.class public final Lcom/swedbank/mobile/data/wallet/j$ak$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ak;->a(Lio/reactivex/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$ak;

.field final synthetic b:Lio/reactivex/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j$ak;Lio/reactivex/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c;",
            ")V"
        }
    .end annotation

    .line 263
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->a:Lcom/swedbank/mobile/data/wallet/j$ak;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->b:Lio/reactivex/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 6
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failure at wallet registration. ErrorMsg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 271
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 272
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->a:Lcom/swedbank/mobile/data/wallet/j$ak;

    iget-object v1, v1, Lcom/swedbank/mobile/data/wallet/j$ak;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v1}, Lcom/swedbank/mobile/data/wallet/j;->f(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/firebase/c;->a(Ljava/lang/Throwable;)V

    .line 273
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->a:Lcom/swedbank/mobile/data/wallet/j$ak;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ak;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    .line 950
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    const/16 v2, 0x7d1

    const/16 v3, 0x7e1

    const/16 v4, 0x386

    if-ne v1, v4, :cond_0

    goto :goto_0

    :cond_0
    const/16 v4, 0x387

    if-ne v1, v4, :cond_1

    goto :goto_0

    :cond_1
    const/16 v4, 0x38d

    if-ne v1, v4, :cond_2

    goto :goto_0

    :cond_2
    const/16 v4, 0x38a

    if-ne v1, v4, :cond_3

    .line 957
    :goto_0
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_4

    :cond_3
    const/16 v4, 0x12f

    if-ne v1, v4, :cond_4

    goto :goto_1

    :cond_4
    const/16 v4, 0x6a

    if-ne v1, v4, :cond_5

    goto :goto_1

    :cond_5
    const/16 v4, 0x132

    if-ne v1, v4, :cond_6

    goto :goto_1

    :cond_6
    const/16 v4, 0x1f7

    if-ne v1, v4, :cond_7

    .line 961
    :goto_1
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_4

    :cond_7
    if-le v2, v1, :cond_8

    goto :goto_3

    :cond_8
    if-lt v3, v1, :cond_9

    packed-switch v1, :pswitch_data_0

    .line 984
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " is not a root reason -- check your code"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 983
    :pswitch_0
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 982
    :pswitch_1
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 981
    :pswitch_2
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 980
    :pswitch_3
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 979
    :pswitch_4
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 978
    :pswitch_5
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 977
    :pswitch_6
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 976
    :pswitch_7
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 975
    :pswitch_8
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 974
    :pswitch_9
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 973
    :pswitch_a
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 972
    :pswitch_b
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 971
    :pswitch_c
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 970
    :pswitch_d
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 969
    :pswitch_e
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 968
    :pswitch_f
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 967
    :pswitch_10
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 962
    :goto_2
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v1, v4

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_4

    .line 986
    :cond_9
    :goto_3
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 946
    :goto_4
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/x;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    .line 273
    invoke-virtual {v0, v4}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 274
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->b:Lio/reactivex/c;

    const-string v1, "emitter"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 989
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    const/16 v4, 0x6d

    if-ne v1, v4, :cond_a

    .line 990
    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_a

    :cond_a
    const/16 v4, 0xca

    if-ne v1, v4, :cond_b

    goto :goto_5

    :cond_b
    const/16 v4, 0xc9

    if-ne v1, v4, :cond_c

    goto :goto_5

    :cond_c
    const/16 v4, 0xcf

    if-ne v1, v4, :cond_d

    .line 991
    :goto_5
    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_a

    :cond_d
    const/16 v4, 0x1fc

    if-ne v1, v4, :cond_e

    goto :goto_6

    :cond_e
    const/16 v4, 0xd0

    if-ne v1, v4, :cond_f

    .line 992
    :goto_6
    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto/16 :goto_a

    :cond_f
    if-le v2, v1, :cond_10

    goto/16 :goto_8

    :cond_10
    if-lt v3, v1, :cond_11

    .line 994
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p1

    packed-switch p1, :pswitch_data_1

    .line 1013
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is not a root reason -- check your code"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 1012
    :pswitch_11
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1011
    :pswitch_12
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1010
    :pswitch_13
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1009
    :pswitch_14
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1008
    :pswitch_15
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1007
    :pswitch_16
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1006
    :pswitch_17
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1005
    :pswitch_18
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1004
    :pswitch_19
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1003
    :pswitch_1a
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1002
    :pswitch_1b
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1001
    :pswitch_1c
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1000
    :pswitch_1d
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 999
    :pswitch_1e
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 998
    :pswitch_1f
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 997
    :pswitch_20
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 996
    :pswitch_21
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 993
    :goto_7
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_a

    :cond_11
    :goto_8
    const/16 p1, 0x25b

    if-ne v1, p1, :cond_12

    goto :goto_9

    :cond_12
    const/16 p1, 0x3f0

    if-ne v1, p1, :cond_13

    goto :goto_9

    :cond_13
    const/16 p1, 0x6b

    if-ne v1, p1, :cond_14

    goto :goto_9

    :cond_14
    const/16 p1, 0x68

    if-ne v1, p1, :cond_15

    goto :goto_9

    :cond_15
    const/16 p1, 0x69

    if-ne v1, p1, :cond_16

    goto :goto_9

    :cond_16
    const/16 p1, 0x66

    if-ne v1, p1, :cond_17

    .line 1025
    :goto_9
    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_a

    :cond_17
    const/16 p1, 0x6f

    if-ne v1, p1, :cond_18

    .line 1026
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    goto :goto_a

    .line 1027
    :cond_18
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    .line 989
    :goto_a
    check-cast p1, Ljava/lang/Throwable;

    invoke-interface {v0, p1}, Lio/reactivex/c;->a(Ljava/lang/Throwable;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7d1
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 263
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ak$1;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->a:Lcom/swedbank/mobile/data/wallet/j$ak;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ak;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->g(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 266
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ak$1;->b:Lio/reactivex/c;

    invoke-interface {v0}, Lio/reactivex/c;->c()V

    return-void
.end method
