.class public final Lcom/swedbank/mobile/data/wallet/j$v;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/z;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/wallet/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/z<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Lcom/swedbank/mobile/business/cards/wallet/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Lcom/swedbank/mobile/business/cards/wallet/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$v;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$v;->b:Lcom/swedbank/mobile/business/cards/wallet/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/x;)V
    .locals 5
    .param p1    # Lio/reactivex/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$v;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    .line 344
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$v;->b:Lcom/swedbank/mobile/business/cards/wallet/a;

    .line 345
    iget-object v2, p0, Lcom/swedbank/mobile/data/wallet/j$v;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v2}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v2

    .line 346
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/a;->a()Ljava/lang/String;

    move-result-object v3

    .line 347
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/a;->b()Ljava/lang/String;

    move-result-object v4

    .line 348
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/a;->c()Ljava/lang/String;

    move-result-object v1

    .line 345
    invoke-interface {v2, v3, v4, v1}, Lcom/swedbank/mobile/data/wallet/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;

    move-result-object v1

    .line 351
    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$v$1;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$v$1;-><init>(Lcom/swedbank/mobile/data/wallet/j$v;Lio/reactivex/x;)V

    check-cast v2, Lcom/meawallet/mtp/MeaInitializeDigitizationListener;

    .line 343
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/data/wallet/e;->a(Lcom/meawallet/mtp/MeaInitializeDigitizationParameters;Lcom/meawallet/mtp/MeaInitializeDigitizationListener;)V

    return-void
.end method
