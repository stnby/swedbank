.class public final Lcom/swedbank/mobile/data/wallet/j$a$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$a;->a(Lio/reactivex/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/c;


# direct methods
.method public constructor <init>(Lio/reactivex/c;)V
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$a$1;->a:Lio/reactivex/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 2
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to set transaction limits, reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 305
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$a$1;->a:Lio/reactivex/c;

    invoke-interface {p1}, Lio/reactivex/c;->c()V

    return-void
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 300
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$a$1;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess()V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$a$1;->a:Lio/reactivex/c;

    invoke-interface {v0}, Lio/reactivex/c;->c()V

    return-void
.end method
