.class final Lcom/swedbank/mobile/data/wallet/j$an;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/e/j;Landroidx/work/o;Lcom/swedbank/mobile/data/wallet/u;Lcom/a/a/a/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$an;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/y;",
            ">;"
        }
    .end annotation

    const-string v0, "registered"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$an;->a:Lcom/swedbank/mobile/data/wallet/j;

    .line 941
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$ao;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/wallet/j$ao;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v0, Lio/reactivex/q;

    invoke-static {v0}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.create { emit\u2026ishListener(listener)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$an;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/wallet/j;->g(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 84
    invoke-virtual {p1, v0, v1}, Lcom/b/c/c;->d(J)Lio/reactivex/o;

    move-result-object p1

    .line 85
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$an$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/wallet/j$an$1;-><init>(Lcom/swedbank/mobile/data/wallet/j$an;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$an;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
