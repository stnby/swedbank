.class public final Lcom/swedbank/mobile/data/wallet/u;
.super Lcom/meawallet/mtp/MeaTransactionReceiver;
.source "WalletTransactionReceiver.kt"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/wallet/s;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/s;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/wallet/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletTransactionBackgroundEventStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Lcom/meawallet/mtp/MeaTransactionReceiver;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/u;->a:Lcom/swedbank/mobile/data/wallet/s;

    return-void
.end method


# virtual methods
.method protected handleOnAuthenticationRequiredIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "cardId"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 69
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Wallet authentication required received with cardId: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/u;->a:Lcom/swedbank/mobile/data/wallet/s;

    new-instance v7, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;

    .line 73
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->a(Lcom/meawallet/mtp/MeaContactlessTransactionData;)D

    move-result-wide v3

    .line 75
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->b(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/util/Currency;

    move-result-object v5

    .line 76
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->d(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v6

    move-object v0, v7

    move-object v1, p2

    .line 71
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v7, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    invoke-virtual {p1, v7}, Lcom/swedbank/mobile/data/wallet/s;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V

    :cond_0
    return-void
.end method

.method protected handleOnTransactionFailureIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaError;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "error"

    invoke-static {p3, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Wallet transaction failure event received with cardId: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". Reason: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/u;->a:Lcom/swedbank/mobile/data/wallet/s;

    if-eqz p2, :cond_0

    :goto_0
    move-object v1, p2

    goto :goto_1

    :cond_0
    const-string p2, ""

    goto :goto_0

    .line 50
    :goto_1
    invoke-static {p4}, Lcom/swedbank/mobile/data/wallet/k;->c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-interface {p3}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string p2, "message"

    invoke-static {v4, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-interface {p3}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v5

    .line 54
    :try_start_0
    invoke-interface {p3}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p2

    const/16 p3, 0x3ed

    if-eq p2, p3, :cond_f

    packed-switch p2, :pswitch_data_0

    packed-switch p2, :pswitch_data_1

    packed-switch p2, :pswitch_data_2

    const/16 p3, 0x386

    if-ne p2, p3, :cond_1

    goto :goto_2

    :cond_1
    const/16 p3, 0x387

    if-ne p2, p3, :cond_2

    goto :goto_2

    :cond_2
    const/16 p3, 0x38d

    if-ne p2, p3, :cond_3

    goto :goto_2

    :cond_3
    const/16 p3, 0x38a

    if-ne p2, p3, :cond_4

    .line 121
    :goto_2
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_7

    :cond_4
    const/16 p3, 0x12f

    if-ne p2, p3, :cond_5

    goto :goto_3

    :cond_5
    const/16 p3, 0x6a

    if-ne p2, p3, :cond_6

    goto :goto_3

    :cond_6
    const/16 p3, 0x132

    if-ne p2, p3, :cond_7

    goto :goto_3

    :cond_7
    const/16 p3, 0x1f7

    if-ne p2, p3, :cond_8

    .line 125
    :goto_3
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_7

    :cond_8
    const/16 p3, 0x7e1

    const/16 p4, 0x7d1

    if-le p4, p2, :cond_9

    goto/16 :goto_6

    :cond_9
    if-lt p3, p2, :cond_a

    packed-switch p2, :pswitch_data_3

    .line 148
    new-instance p3, Ljava/lang/IllegalStateException;

    goto :goto_5

    .line 147
    :pswitch_0
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 146
    :pswitch_1
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 145
    :pswitch_2
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 144
    :pswitch_3
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 143
    :pswitch_4
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 142
    :pswitch_5
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 141
    :pswitch_6
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 140
    :pswitch_7
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 139
    :pswitch_8
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 138
    :pswitch_9
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 137
    :pswitch_a
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 136
    :pswitch_b
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 135
    :pswitch_c
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 134
    :pswitch_d
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 133
    :pswitch_e
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 132
    :pswitch_f
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_4

    .line 131
    :pswitch_10
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 126
    :goto_4
    new-instance p3, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {p3, p2}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p2, p3

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_7

    .line 148
    :goto_5
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " is not a root reason -- check your code"

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p3, Ljava/lang/Throwable;

    throw p3

    .line 150
    :cond_a
    :goto_6
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 153
    :goto_7
    sget-object p3, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_b

    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 154
    :cond_b
    instance-of p3, p2, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    if-eqz p3, :cond_c

    new-instance p3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/wallet/w$a;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object p2

    invoke-direct {p3, p2}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p2, p3

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 155
    :cond_c
    sget-object p3, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_d

    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 156
    :cond_d
    sget-object p3, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_e

    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    :cond_e
    new-instance p2, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p2}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p2

    .line 92
    :pswitch_11
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 104
    :pswitch_12
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 96
    :pswitch_13
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 100
    :pswitch_14
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 91
    :pswitch_15
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 103
    :pswitch_16
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 90
    :pswitch_17
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_8

    .line 105
    :cond_f
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    .line 56
    :catch_0
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    :goto_8
    move-object v3, p2

    .line 48
    new-instance p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;Ljava/lang/String;I)V

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/data/wallet/s;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x44d
        :pswitch_17
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_14
        :pswitch_14
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x456
        :pswitch_12
        :pswitch_14
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x459
        :pswitch_11
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_16
        :pswitch_16
        :pswitch_14
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected handleOnTransactionSubmittedIntent(Landroid/content/Context;Ljava/lang/String;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "cardId"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 27
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Wallet transaction success event received with cardId: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/u;->a:Lcom/swedbank/mobile/data/wallet/s;

    new-instance v7, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    .line 31
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->a(Lcom/meawallet/mtp/MeaContactlessTransactionData;)D

    move-result-wide v3

    .line 33
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->b(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/util/Currency;

    move-result-object v5

    .line 34
    invoke-static {p3}, Lcom/swedbank/mobile/data/wallet/k;->d(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v6

    move-object v0, v7

    move-object v1, p2

    .line 29
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v7, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    invoke-virtual {p1, v7}, Lcom/swedbank/mobile/data/wallet/s;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V

    :cond_0
    return-void
.end method
