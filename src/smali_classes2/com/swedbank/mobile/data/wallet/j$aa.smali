.class final Lcom/swedbank/mobile/data/wallet/j$aa;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->e()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lcom/swedbank/mobile/business/util/l<",
        "+",
        "Lcom/swedbank/mobile/business/e/l;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/wallet/j$aa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$aa;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/wallet/j$aa;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/wallet/j$aa;->a:Lcom/swedbank/mobile/data/wallet/j$aa;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/business/util/l;
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    if-eqz p1, :cond_2

    .line 207
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;->b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    move-result-object v0

    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    if-eqz v0, :cond_2

    .line 208
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;->b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object p1

    .line 210
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    goto :goto_0

    .line 208
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.business.cards.wallet.onboarding.registration.RegistrationError.UnsupportedDeviceReason.Rooted"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 211
    :cond_2
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$aa;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    return-object p1
.end method
