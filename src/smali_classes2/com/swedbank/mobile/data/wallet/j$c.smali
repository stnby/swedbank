.class final Lcom/swedbank/mobile/data/wallet/j$c;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->i(Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$c;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$c;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .line 657
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cancelling scheduled replenishment job for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 658
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$c;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->j(Lcom/swedbank/mobile/data/wallet/j;)Landroidx/work/o;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$c;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/swedbank/mobile/data/wallet/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/work/o;->b(Ljava/lang/String;)Landroidx/work/k;

    return-void
.end method
