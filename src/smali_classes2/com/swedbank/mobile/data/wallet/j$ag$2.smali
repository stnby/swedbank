.class final Lcom/swedbank/mobile/data/wallet/j$ag$2;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ag;->a(Lcom/swedbank/mobile/data/wallet/a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/wallet/j$ag$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$ag$2;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/wallet/j$ag$2;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/wallet/j$ag$2;->a:Lcom/swedbank/mobile/data/wallet/j$ag$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/business/util/l;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;)",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/cards/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 438
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/MeaCard;

    .line 942
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v0, "id"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 943
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getState()Lcom/meawallet/mtp/MeaCardState;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 944
    :cond_0
    sget-object v2, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 957
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 956
    :pswitch_1
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 954
    :pswitch_2
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->e:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 953
    :pswitch_3
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->d:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 952
    :pswitch_4
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->c:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 951
    :pswitch_5
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->b:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 950
    :pswitch_6
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->a:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_1

    .line 949
    :goto_0
    :pswitch_7
    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->h:Lcom/swedbank/mobile/business/cards/t;

    :goto_1
    move-object v2, v0

    .line 959
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->isDefaultForContactlessPayments()Ljava/lang/Boolean;

    move-result-object v0

    const-string v3, "isDefaultForContactlessPayments"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 960
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getNumberOfPaymentTokens()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 962
    new-instance v4, Lcom/swedbank/mobile/business/cards/x$a;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v4, v0}, Lcom/swedbank/mobile/business/cards/x$a;-><init>(I)V

    check-cast v4, Lcom/swedbank/mobile/business/cards/x;

    goto :goto_2

    .line 963
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/business/cards/x$b;->a:Lcom/swedbank/mobile/business/cards/x$b;

    check-cast v0, Lcom/swedbank/mobile/business/cards/x;

    move-object v4, v0

    .line 967
    :goto_2
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/meawallet/mtp/MeaTokenInfo;->getTokenPanSuffix()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 968
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v5, 0x14

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 969
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    rsub-int/lit8 v5, v5, 0x10

    const/4 v6, 0x1

    if-gt v6, v5, :cond_3

    :goto_3
    const/16 v7, 0x2a

    .line 970
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 971
    rem-int/lit8 v7, v6, 0x4

    if-nez v7, :cond_2

    const/16 v7, 0x20

    .line 972
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    if-eq v6, v5, :cond_3

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 975
    :cond_3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 976
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    const-string p1, ""

    :goto_4
    move-object v5, p1

    .line 941
    new-instance p1, Lcom/swedbank/mobile/business/cards/s$a;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/s$a;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/t;ZLcom/swedbank/mobile/business/cards/x;Ljava/lang/String;)V

    .line 438
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    goto :goto_5

    :cond_5
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    :goto_5
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ag$2;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    return-object p1
.end method
