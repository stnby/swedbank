.class public final Lcom/swedbank/mobile/data/wallet/h$a;
.super Ljava/lang/Object;
.source "WalletPushMessageHandler.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/h;->a(Ljava/lang/String;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/h;

.field final synthetic b:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/h;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/h$a;->a:Lcom/swedbank/mobile/data/wallet/h;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/h$a;->b:Ljava/util/Map;

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "ignore"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/h$a;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess()V
    .locals 3

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/h$a;->a:Lcom/swedbank/mobile/data/wallet/h;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/h$a;->b:Ljava/util/Map;

    .line 48
    new-instance v1, Lcom/swedbank/mobile/data/wallet/h$a$1;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/wallet/h$a$1;-><init>(Ljava/util/Map;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object v0

    .line 52
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromCallable\u2026scribeOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    return-void
.end method
