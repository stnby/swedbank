.class public final Lcom/swedbank/mobile/data/wallet/s;
.super Ljava/lang/Object;
.source "WalletTransactionBackgroundEventStreamImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/af;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/e/b;

.field private final c:Lcom/swedbank/mobile/business/firebase/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/firebase/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/s;->b:Lcom/swedbank/mobile/business/e/b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/s;->c:Lcom/swedbank/mobile/business/firebase/c;

    .line 19
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Wall\u2026aymentTransactionEvent>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/s;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/wallet/s;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/s;->b:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/s;->a:Lcom/b/c/c;

    .line 30
    new-instance v1, Lcom/swedbank/mobile/data/wallet/s$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/s$a;-><init>(Lcom/swedbank/mobile/data/wallet/s;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 36
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "eventStream\n      .map {\u2026bserveOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/s;->a:Lcom/b/c/c;

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 23
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    if-eqz v0, :cond_0

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Contactless payment failure. ErrorMsg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/s;->c:Lcom/swedbank/mobile/business/firebase/c;

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/firebase/c;->a(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method
