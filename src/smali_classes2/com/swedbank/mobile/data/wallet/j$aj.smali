.class final Lcom/swedbank/mobile/data/wallet/j$aj;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->f(Ljava/lang/String;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/q<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$aj;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$aj;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/p;)V
    .locals 9
    .param p1    # Lio/reactivex/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$aj;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$aj;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/wallet/e;->a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    if-nez v0, :cond_0

    .line 566
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    .line 567
    iget-object v2, p0, Lcom/swedbank/mobile/data/wallet/j$aj;->b:Ljava/lang/String;

    const-string v3, ""

    .line 569
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    move-object v4, v1

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, v0

    .line 566
    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;Ljava/lang/String;IILkotlin/e/b/g;)V

    invoke-interface {p1, v0}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    .line 570
    invoke-interface {p1}, Lio/reactivex/p;->c()V

    return-void

    .line 573
    :cond_0
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$aj$1;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/wallet/j$aj$1;-><init>(Lcom/meawallet/mtp/MeaCard;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    invoke-interface {p1, v1}, Lio/reactivex/p;->a(Lio/reactivex/b/c;)V

    .line 576
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$aj$2;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$aj$2;-><init>(Lcom/swedbank/mobile/data/wallet/j$aj;Lio/reactivex/p;)V

    check-cast v1, Lcom/meawallet/mtp/MeaContactlessTransactionListener;

    invoke-interface {v0, v1}, Lcom/meawallet/mtp/MeaCard;->setContactlessTransactionListener(Lcom/meawallet/mtp/MeaContactlessTransactionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 619
    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lio/reactivex/p;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
