.class public final Lcom/swedbank/mobile/data/wallet/k;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"


# static fields
.field public static final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 55
    const-class v0, Lcom/meawallet/mtp/MeaHceService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sput-object v0, Lcom/swedbank/mobile/data/wallet/k;->a:Ljava/lang/String;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static final a(Lcom/meawallet/mtp/MeaContactlessTransactionData;)D
    .locals 2
    .param p0    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$amountOr0"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 915
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getAmount()Ljava/lang/Double;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public static final b(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/util/Currency;
    .locals 1
    .param p0    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$currencyOrDefaultLocaleCurrency"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 918
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getCurrency()Ljava/util/Currency;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object p0

    const-string v0, "Currency.getInstance(Locale.getDefault())"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public static final c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    if-eqz p0, :cond_0

    .line 921
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getTransactionIdHexString()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method public static final d(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$merchantAndLocationOrEmpty"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 924
    invoke-interface {p0}, Lcom/meawallet/mtp/MeaContactlessTransactionData;->getMerchantAndLocation()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    return-object p0
.end method
