.class public final Lcom/swedbank/mobile/data/wallet/j$ao;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/wallet/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/q<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ao;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/p;)V
    .locals 2
    .param p1    # Lio/reactivex/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p<",
            "Lcom/swedbank/mobile/business/cards/y;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$ao$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ao$a;-><init>(Lcom/swedbank/mobile/data/wallet/j$ao;Lio/reactivex/p;)V

    .line 148
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$ao$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$ao$1;-><init>(Lcom/swedbank/mobile/data/wallet/j$ao;)V

    check-cast v1, Lio/reactivex/c/f;

    invoke-interface {p1, v1}, Lio/reactivex/p;->a(Lio/reactivex/c/f;)V

    .line 152
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ao;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object p1

    move-object v1, v0

    check-cast v1, Lcom/meawallet/mtp/MeaCardProvisionListener;

    invoke-interface {p1, v1}, Lcom/swedbank/mobile/data/wallet/e;->a(Lcom/meawallet/mtp/MeaCardProvisionListener;)V

    .line 153
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ao;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object p1

    check-cast v0, Lcom/meawallet/mtp/MeaCardReplenishListener;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/data/wallet/e;->a(Lcom/meawallet/mtp/MeaCardReplenishListener;)V

    return-void
.end method
