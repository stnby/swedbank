.class public final Lcom/swedbank/mobile/data/wallet/j$a;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/wallet/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j;

.field final synthetic b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$a;->a:Lcom/swedbank/mobile/data/wallet/j;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$a;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/c;)V
    .locals 5
    .param p1    # Lio/reactivex/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$a;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 941
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 942
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 943
    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/v;

    .line 299
    new-instance v3, Lcom/meawallet/mtp/MeaTransactionLimit;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/wallet/v;->a()Ljava/util/Currency;

    move-result-object v4

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/wallet/v;->b()I

    move-result v2

    invoke-direct {v3, v4, v2}, Lcom/meawallet/mtp/MeaTransactionLimit;-><init>(Ljava/util/Currency;I)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 944
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 300
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$a;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$a$1;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/data/wallet/j$a$1;-><init>(Lio/reactivex/c;)V

    check-cast v2, Lcom/meawallet/mtp/MeaListener;

    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/data/wallet/e;->a(Ljava/util/List;Lcom/meawallet/mtp/MeaListener;)V

    return-void
.end method
