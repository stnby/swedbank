.class public final Lcom/swedbank/mobile/data/wallet/j$k$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$k;->a(Lio/reactivex/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$k;

.field final synthetic b:Lio/reactivex/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j$k;Lio/reactivex/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c;",
            ")V"
        }
    .end annotation

    .line 734
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$k$1;->a:Lcom/swedbank/mobile/data/wallet/j$k;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$k$1;->b:Lio/reactivex/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 2
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 741
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failure at deleting all cards. ErrorMsg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 742
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 743
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$k$1;->a:Lcom/swedbank/mobile/data/wallet/j$k;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$k;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->f(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/firebase/c;->a(Ljava/lang/Throwable;)V

    .line 746
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$k$1;->b:Lio/reactivex/c;

    invoke-interface {p1}, Lio/reactivex/c;->c()V

    return-void
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 734
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$k$1;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 736
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$k$1;->a:Lcom/swedbank/mobile/data/wallet/j$k;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$k;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->h(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 737
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$k$1;->b:Lio/reactivex/c;

    invoke-interface {v0}, Lio/reactivex/c;->c()V

    return-void
.end method
