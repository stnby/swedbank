.class public final Lcom/swedbank/mobile/data/wallet/j$ar$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaCardListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ar;->a(Lio/reactivex/x;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$ar;

.field final synthetic b:Lio/reactivex/x;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j$ar;Lio/reactivex/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x;",
            ")V"
        }
    .end annotation

    .line 516
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->a:Lcom/swedbank/mobile/data/wallet/j$ar;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->b:Lio/reactivex/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 3
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 524
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->a:Lcom/swedbank/mobile/data/wallet/j$ar;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ar;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    .line 949
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p1

    const/16 v1, 0x386

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x387

    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/16 v1, 0x38d

    if-ne p1, v1, :cond_2

    goto :goto_0

    :cond_2
    const/16 v1, 0x38a

    if-ne p1, v1, :cond_3

    .line 956
    :goto_0
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_4

    :cond_3
    const/16 v1, 0x12f

    if-ne p1, v1, :cond_4

    goto :goto_1

    :cond_4
    const/16 v1, 0x6a

    if-ne p1, v1, :cond_5

    goto :goto_1

    :cond_5
    const/16 v1, 0x132

    if-ne p1, v1, :cond_6

    goto :goto_1

    :cond_6
    const/16 v1, 0x1f7

    if-ne p1, v1, :cond_7

    .line 960
    :goto_1
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_4

    :cond_7
    const/16 v1, 0x7e1

    const/16 v2, 0x7d1

    if-le v2, p1, :cond_8

    goto :goto_3

    :cond_8
    if-lt v1, p1, :cond_9

    packed-switch p1, :pswitch_data_0

    .line 983
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is not a root reason -- check your code"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 982
    :pswitch_0
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 981
    :pswitch_1
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 980
    :pswitch_2
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 979
    :pswitch_3
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 978
    :pswitch_4
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 977
    :pswitch_5
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 976
    :pswitch_6
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 975
    :pswitch_7
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 974
    :pswitch_8
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 973
    :pswitch_9
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 972
    :pswitch_a
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 971
    :pswitch_b
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 970
    :pswitch_c
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 969
    :pswitch_d
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 968
    :pswitch_e
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 967
    :pswitch_f
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 966
    :pswitch_10
    sget-object p1, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 961
    :goto_2
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_4

    .line 985
    :cond_9
    :goto_3
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 945
    :goto_4
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/x;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    .line 524
    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 525
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->b:Lio/reactivex/x;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 516
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ar$1;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess(Lcom/meawallet/mtp/MeaCard;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 518
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->b:Lio/reactivex/x;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    .line 519
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Card selected for payment: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->a:Lcom/swedbank/mobile/data/wallet/j$ar;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ar;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 520
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->a:Lcom/swedbank/mobile/data/wallet/j$ar;

    iget-object p1, p1, Lcom/swedbank/mobile/data/wallet/j$ar;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/wallet/j;->i(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/b;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ar$1;->a:Lcom/swedbank/mobile/data/wallet/j$ar;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ar;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method
