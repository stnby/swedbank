.class public final Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;
.super Landroidx/work/RxWorker;
.source "PaymentTokenReplenishWorker.kt"


# instance fields
.field public b:Lcom/swedbank/mobile/data/wallet/j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/WorkerParameters;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "appContext"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workerParams"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1, p2}, Landroidx/work/RxWorker;-><init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V

    .line 26
    invoke-virtual {p2}, Landroidx/work/WorkerParameters;->b()Landroidx/work/e;

    move-result-object p2

    const-string v0, "card_id"

    invoke-virtual {p2, v0}, Landroidx/work/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->c:Ljava/lang/String;

    .line 32
    invoke-static {p1}, Lcom/swedbank/mobile/data/h;->a(Landroid/content/Context;)Lcom/swedbank/mobile/data/g;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/swedbank/mobile/data/g;->a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V

    return-void

    .line 26
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.business.cards.wallet.DigitizedCardId /* = kotlin.String */"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)Ljava/lang/String;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->c:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public b()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->b:Lcom/swedbank/mobile/data/wallet/j;

    if-nez v0, :cond_0

    const-string v1, "walletRepository"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 43
    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/wallet/j;->g()Lio/reactivex/o;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$a;-><init>(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$b;->a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    const-wide/16 v1, 0x5

    .line 52
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3}, Lio/reactivex/b;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b;

    move-result-object v1

    .line 56
    iget-object v2, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->b:Lcom/swedbank/mobile/data/wallet/j;

    if-nez v2, :cond_1

    const-string v3, "walletRepository"

    invoke-static {v3}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/swedbank/mobile/data/wallet/j;->j(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v2

    check-cast v2, Lio/reactivex/aa;

    invoke-virtual {v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v1

    .line 57
    sget-object v2, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;->a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v1

    .line 58
    new-instance v2, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$d;-><init>(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 41
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Observable\n      .merge<\u2026())\n      .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
