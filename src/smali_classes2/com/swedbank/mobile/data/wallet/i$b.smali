.class public final Lcom/swedbank/mobile/data/wallet/i$b;
.super Ljava/lang/Object;
.source "WalletPushMessageInstanceIdHandler.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/wallet/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/i;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/i;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/i$b;->a:Lcom/swedbank/mobile/data/wallet/i;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/i$b;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/i$b;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess()V
    .locals 4

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/i$b;->a:Lcom/swedbank/mobile/data/wallet/i;

    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/i$b;->b:Ljava/lang/String;

    .line 48
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/data/wallet/e;->c()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v2

    .line 56
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->b(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v0

    .line 57
    new-instance v3, Lcom/swedbank/mobile/data/wallet/i$a;

    invoke-direct {v3}, Lcom/swedbank/mobile/data/wallet/i$a;-><init>()V

    check-cast v3, Lcom/meawallet/mtp/MeaListener;

    .line 54
    invoke-interface {v2, v1, v0, v3}, Lcom/swedbank/mobile/data/wallet/e;->b(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    :catch_0
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_0
    return-void
.end method
