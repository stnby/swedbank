.class final Lcom/swedbank/mobile/data/wallet/j$af;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j;->a(Ljava/lang/String;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Lcom/swedbank/mobile/data/wallet/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$af;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/wallet/a;)Z
    .locals 3
    .param p1    # Lcom/swedbank/mobile/data/wallet/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 414
    instance-of v0, p1, Lcom/swedbank/mobile/data/wallet/a$a;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/wallet/a$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/wallet/a$a;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 415
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/wallet/a$a;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/meawallet/mtp/MeaCard;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$af;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 416
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 418
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/data/wallet/a$c;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/data/wallet/a$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/wallet/a$c;->a()Lcom/swedbank/mobile/business/cards/y;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/y;->a()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$af;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 419
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/data/wallet/a$b;->a:Lcom/swedbank/mobile/data/wallet/a$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    :goto_0
    return v1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 66
    check-cast p1, Lcom/swedbank/mobile/data/wallet/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$af;->a(Lcom/swedbank/mobile/data/wallet/a;)Z

    move-result p1

    return p1
.end method
