.class final Lcom/swedbank/mobile/data/wallet/j$ad$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ad;->a(Lcom/swedbank/mobile/data/wallet/a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$ad;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j$ad;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ad$1;->a:Lcom/swedbank/mobile/data/wallet/j$ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 396
    :try_start_0
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ad$1;->a:Lcom/swedbank/mobile/data/wallet/j$ad;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ad;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    .line 397
    invoke-interface {v0}, Lcom/swedbank/mobile/data/wallet/e;->f()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 941
    check-cast v0, Ljava/lang/Iterable;

    .line 942
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 943
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/meawallet/mtp/MeaCard;

    .line 941
    invoke-interface {v3}, Lcom/meawallet/mtp/MeaCard;->getState()Lcom/meawallet/mtp/MeaCardState;

    move-result-object v3

    sget-object v4, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 944
    :cond_2
    check-cast v1, Ljava/util/List;

    goto :goto_2

    .line 941
    :cond_3
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 400
    :catch_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    :goto_2
    return-object v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/j$ad$1;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
