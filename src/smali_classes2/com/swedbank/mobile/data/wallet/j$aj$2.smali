.class public final Lcom/swedbank/mobile/data/wallet/j$aj$2;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaContactlessTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$aj;->a(Lio/reactivex/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$aj;

.field final synthetic b:Lio/reactivex/p;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j$aj;Lio/reactivex/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p;",
            ")V"
        }
    .end annotation

    .line 576
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->a:Lcom/swedbank/mobile/data/wallet/j$aj;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->b:Lio/reactivex/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthenticationRequired(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 8
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "transactionData"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 602
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Contactless authentication required: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 604
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->b:Lio/reactivex/p;

    new-instance v7, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;

    .line 605
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->a:Lcom/swedbank/mobile/data/wallet/j$aj;

    iget-object v1, v0, Lcom/swedbank/mobile/data/wallet/j$aj;->b:Ljava/lang/String;

    .line 606
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v2

    .line 607
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->a(Lcom/meawallet/mtp/MeaContactlessTransactionData;)D

    move-result-wide v3

    .line 608
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->b(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/util/Currency;

    move-result-object v5

    .line 609
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->d(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v6

    move-object v0, v7

    .line 604
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    invoke-interface {p1, v7}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    .line 612
    :try_start_0
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->a:Lcom/swedbank/mobile/data/wallet/j$aj;

    iget-object p1, p1, Lcom/swedbank/mobile/data/wallet/j$aj;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p1}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/data/wallet/e;->j()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 614
    :catch_0
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_0
    return-void
.end method

.method public onContactlessPaymentFailure(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 22
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "card"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "error"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 591
    sget-object v2, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Contactless payment failure: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 592
    iget-object v2, v0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->a:Lcom/swedbank/mobile/data/wallet/j$aj;

    iget-object v2, v2, Lcom/swedbank/mobile/data/wallet/j$aj;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v2}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v2

    .line 953
    invoke-interface/range {p2 .. p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v4

    const/16 v5, 0x7d1

    const/16 v6, 0x7e1

    const/16 v7, 0x1f7

    const/16 v8, 0x132

    const/16 v9, 0x6a

    const/16 v10, 0x12f

    const/16 v11, 0x38a

    const/16 v12, 0x38d

    const/16 v13, 0x387

    const/16 v14, 0x386

    if-ne v4, v14, :cond_0

    goto :goto_0

    :cond_0
    if-ne v4, v13, :cond_1

    goto :goto_0

    :cond_1
    if-ne v4, v12, :cond_2

    goto :goto_0

    :cond_2
    if-ne v4, v11, :cond_3

    .line 960
    :goto_0
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_4

    :cond_3
    if-ne v4, v10, :cond_4

    goto :goto_1

    :cond_4
    if-ne v4, v9, :cond_5

    goto :goto_1

    :cond_5
    if-ne v4, v8, :cond_6

    goto :goto_1

    :cond_6
    if-ne v4, v7, :cond_7

    .line 964
    :goto_1
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_4

    :cond_7
    if-le v5, v4, :cond_8

    goto :goto_3

    :cond_8
    if-lt v6, v4, :cond_9

    packed-switch v4, :pswitch_data_0

    .line 987
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " is not a root reason -- check your code"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 986
    :pswitch_0
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 985
    :pswitch_1
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 984
    :pswitch_2
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 983
    :pswitch_3
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 982
    :pswitch_4
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 981
    :pswitch_5
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 980
    :pswitch_6
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 979
    :pswitch_7
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 978
    :pswitch_8
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 977
    :pswitch_9
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 976
    :pswitch_a
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 975
    :pswitch_b
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 974
    :pswitch_c
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 973
    :pswitch_d
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 972
    :pswitch_e
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 971
    :pswitch_f
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_2

    .line 970
    :pswitch_10
    sget-object v4, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 965
    :goto_2
    new-instance v15, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v15, v4}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v4, v15

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_4

    .line 989
    :cond_9
    :goto_3
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 949
    :goto_4
    new-instance v15, Lcom/swedbank/mobile/business/cards/wallet/x;

    const/4 v6, 0x1

    invoke-direct {v15, v4, v6}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    .line 592
    invoke-virtual {v2, v15}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 593
    iget-object v2, v0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->b:Lio/reactivex/p;

    .line 594
    invoke-interface/range {p1 .. p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "card.id"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 595
    invoke-static/range {p3 .. p3}, Lcom/swedbank/mobile/data/wallet/k;->c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v18

    .line 596
    invoke-interface/range {p2 .. p2}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v6, "error.message"

    invoke-static {v4, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 597
    invoke-interface/range {p2 .. p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v21

    .line 598
    invoke-interface/range {p2 .. p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v1

    const/16 v6, 0x3ed

    if-eq v1, v6, :cond_18

    packed-switch v1, :pswitch_data_1

    packed-switch v1, :pswitch_data_2

    packed-switch v1, :pswitch_data_3

    if-ne v1, v14, :cond_a

    goto :goto_5

    :cond_a
    if-ne v1, v13, :cond_b

    goto :goto_5

    :cond_b
    if-ne v1, v12, :cond_c

    goto :goto_5

    :cond_c
    if-ne v1, v11, :cond_d

    .line 1024
    :goto_5
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_9

    :cond_d
    if-ne v1, v10, :cond_e

    goto :goto_6

    :cond_e
    if-ne v1, v9, :cond_f

    goto :goto_6

    :cond_f
    if-ne v1, v8, :cond_10

    goto :goto_6

    :cond_10
    if-ne v1, v7, :cond_11

    .line 1028
    :goto_6
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_9

    :cond_11
    if-le v5, v1, :cond_12

    goto/16 :goto_8

    :cond_12
    const/16 v5, 0x7e1

    if-lt v5, v1, :cond_13

    packed-switch v1, :pswitch_data_4

    .line 1051
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " is not a root reason -- check your code"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 1050
    :pswitch_11
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1049
    :pswitch_12
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1048
    :pswitch_13
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1047
    :pswitch_14
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1046
    :pswitch_15
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1045
    :pswitch_16
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1044
    :pswitch_17
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1043
    :pswitch_18
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1042
    :pswitch_19
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1041
    :pswitch_1a
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1040
    :pswitch_1b
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1039
    :pswitch_1c
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1038
    :pswitch_1d
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1037
    :pswitch_1e
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1036
    :pswitch_1f
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1035
    :pswitch_20
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_7

    .line 1034
    :pswitch_21
    sget-object v1, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 1029
    :goto_7
    new-instance v5, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v5, v1}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v1, v5

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_9

    .line 1053
    :cond_13
    :goto_8
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 1056
    :goto_9
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1057
    :cond_14
    instance-of v5, v1, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    if-eqz v5, :cond_15

    new-instance v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/w$a;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v1, v5

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1058
    :cond_15
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1059
    :cond_16
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    :cond_17
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 995
    :pswitch_22
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1007
    :pswitch_23
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 999
    :pswitch_24
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1003
    :pswitch_25
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 994
    :pswitch_26
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1006
    :pswitch_27
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 993
    :pswitch_28
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_a

    .line 1008
    :cond_18
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    :goto_a
    move-object/from16 v19, v1

    .line 593
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    move-object/from16 v16, v1

    move-object/from16 v17, v3

    move-object/from16 v20, v4

    invoke-direct/range {v16 .. v21}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;Ljava/lang/String;I)V

    invoke-interface {v2, v1}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x44d
        :pswitch_28
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_26
        :pswitch_25
        :pswitch_25
        :pswitch_24
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x456
        :pswitch_23
        :pswitch_25
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x459
        :pswitch_22
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_27
        :pswitch_27
        :pswitch_25
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x7d1
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch
.end method

.method public onContactlessPaymentSubmitted(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaContactlessTransactionData;)V
    .locals 9
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaContactlessTransactionData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 578
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Contactless payment submitted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 580
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->b:Lio/reactivex/p;

    new-instance v8, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    .line 581
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->a:Lcom/swedbank/mobile/data/wallet/j$aj;

    iget-object v2, v1, Lcom/swedbank/mobile/data/wallet/j$aj;->b:Ljava/lang/String;

    .line 582
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->c(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v3

    .line 583
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->a(Lcom/meawallet/mtp/MeaContactlessTransactionData;)D

    move-result-wide v4

    .line 584
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->b(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/util/Currency;

    move-result-object v6

    .line 585
    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/k;->d(Lcom/meawallet/mtp/MeaContactlessTransactionData;)Ljava/lang/String;

    move-result-object v7

    move-object v1, v8

    .line 580
    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    invoke-interface {v0, v8}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    .line 587
    iget-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$aj$2;->a:Lcom/swedbank/mobile/data/wallet/j$aj;

    iget-object p2, p2, Lcom/swedbank/mobile/data/wallet/j$aj;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/j;->h(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object p2

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method
