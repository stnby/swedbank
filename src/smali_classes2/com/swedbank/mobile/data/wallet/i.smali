.class public final Lcom/swedbank/mobile/data/wallet/i;
.super Ljava/lang/Object;
.source "WalletPushMessageInstanceIdHandler.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/push/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/wallet/e;

.field private final b:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/wallet/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletPlatform"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/i;->a:Lcom/swedbank/mobile/data/wallet/e;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/i;->b:Lcom/swedbank/mobile/business/c/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/i;->a:Lcom/swedbank/mobile/data/wallet/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/i;->b:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_2

    .line 17
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/data/wallet/i;

    .line 48
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/data/wallet/e;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v1

    new-instance v2, Lcom/swedbank/mobile/data/wallet/i$b;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/data/wallet/i$b;-><init>(Lcom/swedbank/mobile/data/wallet/i;Ljava/lang/String;)V

    check-cast v2, Lcom/meawallet/mtp/MeaListener;

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/data/wallet/e;->a(Lcom/meawallet/mtp/MeaListener;)V

    goto :goto_0

    .line 61
    :cond_0
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/data/wallet/e;->c()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 67
    :cond_1
    :try_start_0
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->a(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v1

    .line 69
    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/i;->b(Lcom/swedbank/mobile/data/wallet/i;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v0

    .line 70
    new-instance v2, Lcom/swedbank/mobile/data/wallet/i$a;

    invoke-direct {v2}, Lcom/swedbank/mobile/data/wallet/i$a;-><init>()V

    check-cast v2, Lcom/meawallet/mtp/MeaListener;

    .line 67
    invoke-interface {v1, p1, v0, v2}, Lcom/swedbank/mobile/data/wallet/e;->b(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;Lcom/meawallet/mtp/MeaListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 76
    :catch_0
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :cond_2
    :goto_0
    return-void
.end method
