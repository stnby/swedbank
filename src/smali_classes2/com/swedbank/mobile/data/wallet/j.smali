.class public final Lcom/swedbank/mobile/data/wallet/j;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/ad;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/cards/wallet/x;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/y;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/data/wallet/a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/app/Application;

.field private final j:Lcom/swedbank/mobile/business/firebase/c;

.field private final k:Lcom/swedbank/mobile/data/wallet/e;

.field private final l:Lcom/swedbank/mobile/business/e/j;

.field private final m:Landroidx/work/o;

.field private final n:Lcom/swedbank/mobile/data/wallet/u;

.field private final o:Lcom/a/a/a/f;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/e/j;Landroidx/work/o;Lcom/swedbank/mobile/data/wallet/u;Lcom/a/a/a/f;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/wallet/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/e/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Landroidx/work/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/data/wallet/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPlatform"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalActivityManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletTransactionReceiver"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferences"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->i:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j;->j:Lcom/swedbank/mobile/business/firebase/c;

    iput-object p3, p0, Lcom/swedbank/mobile/data/wallet/j;->k:Lcom/swedbank/mobile/data/wallet/e;

    iput-object p4, p0, Lcom/swedbank/mobile/data/wallet/j;->l:Lcom/swedbank/mobile/business/e/j;

    iput-object p5, p0, Lcom/swedbank/mobile/data/wallet/j;->m:Landroidx/work/o;

    iput-object p6, p0, Lcom/swedbank/mobile/data/wallet/j;->n:Lcom/swedbank/mobile/data/wallet/u;

    iput-object p7, p0, Lcom/swedbank/mobile/data/wallet/j;->o:Lcom/a/a/a/f;

    .line 75
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->o:Lcom/a/a/a/f;

    const-string p2, "isWalletEnabledPreference"

    const/4 p3, 0x0

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object p1

    const-string p2, "appPreferences.getBoolea\u2026IS_WALLET_ENABLED, false)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->a:Lcom/a/a/a/d;

    .line 76
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026DigitizedCardId>>(Absent)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->b:Lcom/b/c/b;

    .line 77
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<WalletGeneralErrorEvent>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->c:Lcom/b/c/c;

    .line 78
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->d:Lcom/b/c/c;

    .line 79
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Optional<MeaCard>>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->e:Lcom/b/c/c;

    .line 80
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/j;->d()Lio/reactivex/w;

    move-result-object p1

    .line 81
    new-instance p2, Lcom/swedbank/mobile/data/wallet/j$an;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/data/wallet/j$an;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 87
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object p1

    const-string p2, "isRegistered()\n      .fl\u2026lers.io())\n      .share()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->f:Lio/reactivex/o;

    .line 89
    new-instance p1, Lcom/swedbank/mobile/data/wallet/j$t;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/wallet/j$t;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast p1, Lio/reactivex/q;

    invoke-static {p1}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object p1

    const-string p2, "Observable.create<MeaCar\u2026nNext)\n    })\n  }.share()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->g:Lio/reactivex/o;

    .line 96
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->e:Lcom/b/c/c;

    sget-object p2, Lcom/swedbank/mobile/data/wallet/j$d;->a:Lcom/swedbank/mobile/data/wallet/j$d;

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lcom/b/c/c;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 97
    iget-object p2, p0, Lcom/swedbank/mobile/data/wallet/j;->g:Lio/reactivex/o;

    sget-object p3, Lcom/swedbank/mobile/data/wallet/j$e;->a:Lcom/swedbank/mobile/data/wallet/j$e;

    check-cast p3, Lio/reactivex/c/h;

    invoke-virtual {p2, p3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p2

    check-cast p2, Lio/reactivex/s;

    .line 98
    iget-object p3, p0, Lcom/swedbank/mobile/data/wallet/j;->f:Lio/reactivex/o;

    .line 100
    sget-object p4, Lcom/swedbank/mobile/data/wallet/j$f;->a:Lcom/swedbank/mobile/data/wallet/j$f;

    check-cast p4, Lio/reactivex/c/k;

    invoke-virtual {p3, p4}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p3

    .line 101
    sget-object p4, Lcom/swedbank/mobile/data/wallet/j$g;->a:Lcom/swedbank/mobile/data/wallet/j$g;

    check-cast p4, Lio/reactivex/c/h;

    invoke-virtual {p3, p4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p3

    check-cast p3, Lio/reactivex/s;

    .line 102
    iget-object p4, p0, Lcom/swedbank/mobile/data/wallet/j;->d:Lcom/b/c/c;

    sget-object p5, Lcom/swedbank/mobile/data/wallet/j$h;->a:Lcom/swedbank/mobile/data/wallet/j$h;

    check-cast p5, Lio/reactivex/c/h;

    invoke-virtual {p4, p5}, Lcom/b/c/c;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p4

    check-cast p4, Lio/reactivex/s;

    .line 95
    invoke-static {p1, p2, p3, p4}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j;->h:Lio/reactivex/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->c:Lcom/b/c/c;

    return-object p0
.end method

.method static synthetic a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x1

    .line 165
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/wallet/j;->a(ZZZ)Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method private final a(ZZZ)Lio/reactivex/b;
    .locals 6

    .line 167
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$w;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/swedbank/mobile/data/wallet/j$w;-><init>(Lcom/swedbank/mobile/data/wallet/j;ZZZ)V

    check-cast v0, Lio/reactivex/e;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object p1

    .line 197
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/b;->b(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p1

    .line 198
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    const-string p1, "Completable\n      .creat\u2026bserveOn(Schedulers.io())"

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    sget-object p1, Lcom/swedbank/mobile/data/wallet/j$x;->a:Lcom/swedbank/mobile/data/wallet/j$x;

    move-object v2, p1

    check-cast v2, Lkotlin/e/a/b;

    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/l/f;->a(Lio/reactivex/b;ILkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->k:Lcom/swedbank/mobile/data/wallet/e;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/wallet/j;)Lcom/a/a/a/d;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->a:Lcom/a/a/a/d;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/wallet/j;)Landroid/app/Application;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->i:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/u;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->n:Lcom/swedbank/mobile/data/wallet/u;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/business/firebase/c;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->j:Lcom/swedbank/mobile/business/firebase/c;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->d:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->e:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/b;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->b:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/data/wallet/j;)Landroidx/work/o;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/data/wallet/j;->m:Landroidx/work/o;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;)Lio/reactivex/b;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pushId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "consumerLanguage"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 258
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 259
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$ak;

    invoke-direct {v1, p0, p1, p2}, Lcom/swedbank/mobile/data/wallet/j$ak;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;)V

    check-cast v1, Lio/reactivex/e;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    .line 278
    new-instance p2, Lcom/swedbank/mobile/data/wallet/j$al;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j;->k:Lcom/swedbank/mobile/data/wallet/e;

    invoke-direct {p2, v0}, Lcom/swedbank/mobile/data/wallet/j$al;-><init>(Lcom/swedbank/mobile/data/wallet/e;)V

    check-cast p2, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/data/wallet/m;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/data/wallet/m;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p2

    check-cast p2, Lio/reactivex/f;

    invoke-virtual {p1, p2}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    .line 279
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p1

    const-string p2, "initializeSdk()\n      .a\u2026bserveOn(Schedulers.io())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/util/List;)Lio/reactivex/b;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 632
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$ap;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ap;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/util/List;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    .line 652
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/wallet/o;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/wallet/o;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object p1

    .line 653
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .fromA\u2026\n      .onErrorComplete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/util/List;I)Lio/reactivex/b;
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/v;",
            ">;I)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "limits"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 282
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 941
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$i;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$i;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/e;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object v1

    .line 950
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v1

    const-string v2, "Completable.create { emi\u2026bserveOn(Schedulers.io())"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/f;

    .line 283
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 951
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$a;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/util/List;)V

    check-cast v1, Lio/reactivex/e;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object p1

    .line 961
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p1

    const-string v1, "Completable.create { emi\u2026bserveOn(Schedulers.io())"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/f;

    .line 284
    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    .line 962
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$au;

    invoke-direct {v0, p0, p2}, Lcom/swedbank/mobile/data/wallet/j$au;-><init>(Lcom/swedbank/mobile/data/wallet/j;I)V

    check-cast v0, Lio/reactivex/e;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object p2

    .line 971
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object p2

    const-string v0, "Completable.create { emi\u2026bserveOn(Schedulers.io())"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lio/reactivex/f;

    .line 285
    invoke-virtual {p1, p2}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string p2, "initializeSdk()\n        \u2026mit(defaultLimitInCents))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Z)Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 158
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$av;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$av;-><init>(Lcom/swedbank/mobile/data/wallet/j;Z)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026tEnabled.set(enabled)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/o;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/cards/s$a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 410
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 411
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j;->h:Lio/reactivex/o;

    .line 412
    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$af;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/data/wallet/j$af;-><init>(Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v1

    .line 422
    new-instance v2, Lcom/swedbank/mobile/data/wallet/a$a;

    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v3, Lcom/swedbank/mobile/business/util/l;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/data/wallet/a$a;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 423
    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$ag;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ag;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 411
    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 440
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->g(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026onErrorReturnItem(Absent)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/a;)Lio/reactivex/w;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/a;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 330
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 973
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$v;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$v;-><init>(Lcom/swedbank/mobile/data/wallet/j;Lcom/swedbank/mobile/business/cards/wallet/a;)V

    check-cast v1, Lio/reactivex/z;

    invoke-static {v1}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object p1

    const-string v1, "Single\n      .create { e\u2026 }\n            })\n      }"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    .line 333
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$r;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$r;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 338
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    .line 339
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$s;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$s;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {p1, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    .line 331
    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "initializeSdk()\n        \u2026Changes.accept(Absent) })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Z
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j;->a:Lcom/a/a/a/d;

    invoke-interface {v0}, Lcom/a/a/a/d;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "isWalletEnabled.get()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public b()Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 215
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j;->h:Lio/reactivex/o;

    .line 217
    sget-object v2, Lcom/swedbank/mobile/data/wallet/j$ah;->a:Lcom/swedbank/mobile/data/wallet/j$ah;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v1

    .line 218
    sget-object v2, Lcom/swedbank/mobile/data/wallet/a$b;->a:Lcom/swedbank/mobile/data/wallet/a$b;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 219
    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$ai;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/data/wallet/j$ai;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 216
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 237
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    .line 238
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->g(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026onErrorReturnItem(Absent)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/w;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 459
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 460
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$at;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$at;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026 false\n        }\n      })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/x;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 240
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j;->c:Lcom/b/c/c;

    .line 241
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "generalErrorStream\n     \u2026bserveOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/w;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    .line 512
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 513
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$ar;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ar;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/z;

    invoke-static {v1}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    .line 533
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "initializeSdk(inPaymentT\u2026bserveOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lio/reactivex/b;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 535
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 536
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$q;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$q;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026m.accept(Absent)\n      })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d()Lio/reactivex/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 249
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 250
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$ab;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$ab;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026 false\n        }\n      })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lio/reactivex/b;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 547
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 548
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$aw;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$aw;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026tion()\n        }\n      })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public e()Lio/reactivex/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object v0, p0

    .line 202
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 203
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 204
    sget-object v1, Lcom/swedbank/mobile/data/wallet/j$aa;->a:Lcom/swedbank/mobile/data/wallet/j$aa;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "initializeSdk(emitErrors\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 321
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$am;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/wallet/j$am;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    .line 327
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction {\u2026scribeOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lio/reactivex/o;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    .line 559
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 560
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$aj;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$aj;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/q;

    invoke-static {v1}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 621
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "initializeSdk(inPaymentT\u2026bserveOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/y;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 387
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j;->f:Lio/reactivex/o;

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lio/reactivex/w;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 979
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 982
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j;->i(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 981
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$m;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$m;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/z;

    invoke-static {v1}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    .line 980
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026bserveOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public h()Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/s$a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 389
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 390
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j;->h:Lio/reactivex/o;

    .line 391
    new-instance v2, Lcom/swedbank/mobile/data/wallet/a$a;

    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v3, Lcom/swedbank/mobile/business/util/l;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/data/wallet/a$a;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 392
    new-instance v2, Lcom/swedbank/mobile/data/wallet/j$ad;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/data/wallet/j$ad;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 390
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 408
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->g(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026orReturnItem(emptyList())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lio/reactivex/w;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 983
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 986
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j;->i(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 985
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$n;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$n;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/z;

    invoke-static {v1}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    .line 984
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026bserveOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final i(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 656
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$c;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    .line 660
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/data/wallet/o;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/wallet/o;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object p1

    .line 661
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .fromA\u2026\n      .onErrorComplete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public i()Lio/reactivex/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 473
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 474
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$y;

    iget-object v2, p0, Lcom/swedbank/mobile/data/wallet/j;->k:Lcom/swedbank/mobile/data/wallet/e;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/data/wallet/j$y;-><init>(Lcom/swedbank/mobile/data/wallet/e;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/data/wallet/q;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/wallet/q;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Ljava/util/concurrent/Callable;

    invoke-static {v2}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026CardholderAuthenticated))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 476
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 477
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$ac;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$ac;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026ithDeviceUnlock)\n      })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final j(Ljava/lang/String;)Lio/reactivex/w;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    .line 671
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 672
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$aq;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/wallet/j$aq;-><init>(Lcom/swedbank/mobile/data/wallet/j;Ljava/lang/String;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "initializeSdk()\n      .a\u2026 false\n        }\n      })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public k()Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 481
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 482
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$ae;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$ae;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/q;

    invoke-static {v1}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 509
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026bserveOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public l()Lio/reactivex/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    .line 623
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 624
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$u;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$u;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "initializeSdk()\n      .a\u2026e defaultCard.id\n      })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public m()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 684
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$z;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/wallet/j$z;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    .line 688
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single\n      .fromCallab\u2026scribeOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public n()Lio/reactivex/w;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 690
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j;->l:Lcom/swedbank/mobile/business/e/j;

    .line 691
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j;->i:Landroid/app/Application;

    .line 975
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.nfc.cardemulation.action.ACTION_CHANGE_DEFAULT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "category"

    const-string v4, "payment"

    .line 976
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "component"

    .line 977
    new-instance v4, Landroid/content/ComponentName;

    check-cast v1, Landroid/content/Context;

    sget-object v5, Lcom/swedbank/mobile/data/wallet/k;->a:Ljava/lang/String;

    invoke-direct {v4, v1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    check-cast v4, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v2

    move v2, v3

    move-object v3, v4

    move v4, v5

    move-object v5, v6

    .line 691
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/e/j$a;->a(Lcom/swedbank/mobile/business/e/j;Landroid/content/Intent;ILandroid/os/Bundle;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    .line 692
    sget-object v1, Lcom/swedbank/mobile/data/wallet/j$as;->a:Lcom/swedbank/mobile/data/wallet/j$as;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/data/wallet/p;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/wallet/p;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "externalActivityManager\n\u2026map(ActivityResult::isOk)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public o()Lio/reactivex/b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, p0

    .line 731
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 732
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/j;->q()Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 733
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$k;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$k;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/e;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 750
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$l;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$l;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    .line 751
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "initializeSdk(emitErrors\u2026bserveOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public p()Lio/reactivex/b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, p0

    .line 753
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;ZZZILjava/lang/Object;)Lio/reactivex/b;

    move-result-object v0

    .line 754
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/j;->q()Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 755
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$o;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$o;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/e;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/e;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const/4 v1, 0x0

    .line 767
    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/data/wallet/j;->a(Z)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 768
    new-instance v1, Lcom/swedbank/mobile/data/wallet/j$p;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/wallet/j$p;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    .line 769
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "initializeSdk(emitErrors\u2026bserveOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final q()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 664
    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/wallet/j$b;-><init>(Lcom/swedbank/mobile/data/wallet/j;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    .line 668
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/data/wallet/o;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/wallet/o;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 669
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable\n      .fromA\u2026\n      .onErrorComplete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
