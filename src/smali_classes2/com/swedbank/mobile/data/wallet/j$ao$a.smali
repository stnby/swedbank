.class public final Lcom/swedbank/mobile/data/wallet/j$ao$a;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaCardProvisionListener;
.implements Lcom/meawallet/mtp/MeaCardReplenishListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ao;->a(Lio/reactivex/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$ao;

.field final synthetic b:Lio/reactivex/p;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/j$ao;Lio/reactivex/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p;",
            ")V"
        }
    .end annotation

    .line 105
    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->a:Lcom/swedbank/mobile/data/wallet/j$ao;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->b:Lio/reactivex/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCardProvisionCompleted(Lcom/meawallet/mtp/MeaCard;)V
    .locals 3
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_1

    .line 108
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object v0

    .line 109
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provision completed for card ***-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaTokenInfo;->getTokenPanSuffix()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->b:Lio/reactivex/p;

    new-instance v1, Lcom/swedbank/mobile/business/cards/y$a;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    const-string v2, "card.id"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/y$a;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 112
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_1
    return-void
.end method

.method public onCardProvisionFailure(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
    .locals 4
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_b

    .line 118
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object v0

    .line 119
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provision failure for card ***-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaTokenInfo;->getTokenPanSuffix()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". ErrorMsg="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "; errorCode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->a:Lcom/swedbank/mobile/data/wallet/j$ao;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ao;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    const/4 v1, 0x0

    .line 958
    invoke-interface {p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p2

    const/16 v2, 0x386

    if-ne p2, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x387

    if-ne p2, v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v2, 0x38d

    if-ne p2, v2, :cond_3

    goto :goto_1

    :cond_3
    const/16 v2, 0x38a

    if-ne p2, v2, :cond_4

    .line 965
    :goto_1
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_5

    :cond_4
    const/16 v2, 0x12f

    if-ne p2, v2, :cond_5

    goto :goto_2

    :cond_5
    const/16 v2, 0x6a

    if-ne p2, v2, :cond_6

    goto :goto_2

    :cond_6
    const/16 v2, 0x132

    if-ne p2, v2, :cond_7

    goto :goto_2

    :cond_7
    const/16 v2, 0x1f7

    if-ne p2, v2, :cond_8

    .line 969
    :goto_2
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_5

    :cond_8
    const/16 v2, 0x7e1

    const/16 v3, 0x7d1

    if-le v3, p2, :cond_9

    goto :goto_4

    :cond_9
    if-lt v2, p2, :cond_a

    packed-switch p2, :pswitch_data_0

    .line 992
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " is not a root reason -- check your code"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 991
    :pswitch_0
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 990
    :pswitch_1
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 989
    :pswitch_2
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 988
    :pswitch_3
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 987
    :pswitch_4
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 986
    :pswitch_5
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 985
    :pswitch_6
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 984
    :pswitch_7
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 983
    :pswitch_8
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 982
    :pswitch_9
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 981
    :pswitch_a
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 980
    :pswitch_b
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 979
    :pswitch_c
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 978
    :pswitch_d
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 977
    :pswitch_e
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 976
    :pswitch_f
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 975
    :pswitch_10
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 970
    :goto_3
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v2, p2}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p2, v2

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_5

    .line 994
    :cond_a
    :goto_4
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 954
    :goto_5
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-direct {v2, p2, v1}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    .line 120
    invoke-virtual {v0, v2}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 121
    iget-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->b:Lio/reactivex/p;

    new-instance v0, Lcom/swedbank/mobile/business/cards/y$b;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    const-string v1, "card.id"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/y$b;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    goto :goto_6

    .line 123
    :cond_b
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onReplenishCompleted(Lcom/meawallet/mtp/MeaCard;I)V
    .locals 3
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_1

    .line 129
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object v0

    .line 130
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Replenish (w/nrOfTransactions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ") completed for card ***-"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaTokenInfo;->getTokenPanSuffix()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 131
    iget-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->b:Lio/reactivex/p;

    new-instance v0, Lcom/swedbank/mobile/business/cards/y$c;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    const-string v1, "card.id"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/y$c;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 133
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_1
    return-void
.end method

.method public onReplenishFailed(Lcom/meawallet/mtp/MeaCard;Lcom/meawallet/mtp/MeaError;)V
    .locals 4
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_b

    .line 139
    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object v0

    .line 140
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Replenish failure for card ***-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/meawallet/mtp/MeaTokenInfo;->getTokenPanSuffix()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". ErrorMsg="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "; errorCode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->a:Lcom/swedbank/mobile/data/wallet/j$ao;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ao;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->a(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    const/4 v1, 0x0

    .line 1018
    invoke-interface {p2}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p2

    const/16 v2, 0x386

    if-ne p2, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x387

    if-ne p2, v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v2, 0x38d

    if-ne p2, v2, :cond_3

    goto :goto_1

    :cond_3
    const/16 v2, 0x38a

    if-ne p2, v2, :cond_4

    .line 1025
    :goto_1
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_5

    :cond_4
    const/16 v2, 0x12f

    if-ne p2, v2, :cond_5

    goto :goto_2

    :cond_5
    const/16 v2, 0x6a

    if-ne p2, v2, :cond_6

    goto :goto_2

    :cond_6
    const/16 v2, 0x132

    if-ne p2, v2, :cond_7

    goto :goto_2

    :cond_7
    const/16 v2, 0x1f7

    if-ne p2, v2, :cond_8

    .line 1029
    :goto_2
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto/16 :goto_5

    :cond_8
    const/16 v2, 0x7e1

    const/16 v3, 0x7d1

    if-le v3, p2, :cond_9

    goto :goto_4

    :cond_9
    if-lt v2, p2, :cond_a

    packed-switch p2, :pswitch_data_0

    .line 1052
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " is not a root reason -- check your code"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 1051
    :pswitch_0
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1050
    :pswitch_1
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1049
    :pswitch_2
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1048
    :pswitch_3
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1047
    :pswitch_4
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1046
    :pswitch_5
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1045
    :pswitch_6
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1044
    :pswitch_7
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1043
    :pswitch_8
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1042
    :pswitch_9
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1041
    :pswitch_a
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1040
    :pswitch_b
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1039
    :pswitch_c
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1038
    :pswitch_d
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1037
    :pswitch_e
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1036
    :pswitch_f
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    goto :goto_3

    .line 1035
    :pswitch_10
    sget-object p2, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    .line 1030
    :goto_3
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-direct {v2, p2}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p2, v2

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_5

    .line 1054
    :cond_a
    :goto_4
    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 1014
    :goto_5
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-direct {v2, p2, v1}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    .line 141
    invoke-virtual {v0, v2}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 142
    iget-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$ao$a;->b:Lio/reactivex/p;

    new-instance v0, Lcom/swedbank/mobile/business/cards/y$d;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object p1

    const-string v1, "card.id"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/y$d;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    goto :goto_6

    .line 144
    :cond_b
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    :goto_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
