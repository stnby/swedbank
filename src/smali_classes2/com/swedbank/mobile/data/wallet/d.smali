.class public final Lcom/swedbank/mobile/data/wallet/d;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaError;


# static fields
.field public static final a:Lcom/swedbank/mobile/data/wallet/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 932
    new-instance v0, Lcom/swedbank/mobile/data/wallet/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/wallet/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/wallet/d;->a:Lcom/swedbank/mobile/data/wallet/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCode()I
    .locals 1

    const/16 v0, -0x29a

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "Unknown MEA error"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 937
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/wallet/d;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteResponseId()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, ""

    return-object v0
.end method
