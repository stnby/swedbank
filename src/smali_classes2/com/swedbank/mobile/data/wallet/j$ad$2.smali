.class final Lcom/swedbank/mobile/data/wallet/j$ad$2;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ad;->a(Lcom/swedbank/mobile/data/wallet/a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/wallet/j$ad$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/wallet/j$ad$2;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/wallet/j$ad$2;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/wallet/j$ad$2;->a:Lcom/swedbank/mobile/data/wallet/j$ad$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$ad$2;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 403
    check-cast p1, Ljava/lang/Iterable;

    .line 941
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 942
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 943
    check-cast v1, Lcom/meawallet/mtp/MeaCard;

    .line 945
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v2, "id"

    invoke-static {v3, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 946
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getState()Lcom/meawallet/mtp/MeaCardState;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 947
    :cond_0
    sget-object v4, Lcom/swedbank/mobile/data/wallet/l;->a:[I

    invoke-virtual {v2}, Lcom/meawallet/mtp/MeaCardState;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 960
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 959
    :pswitch_1
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 957
    :pswitch_2
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->e:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 956
    :pswitch_3
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->d:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 955
    :pswitch_4
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->c:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 954
    :pswitch_5
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->b:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 953
    :pswitch_6
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->a:Lcom/swedbank/mobile/business/cards/t;

    goto :goto_2

    .line 952
    :goto_1
    :pswitch_7
    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->h:Lcom/swedbank/mobile/business/cards/t;

    :goto_2
    move-object v4, v2

    .line 962
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->isDefaultForContactlessPayments()Ljava/lang/Boolean;

    move-result-object v2

    const-string v5, "isDefaultForContactlessPayments"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 963
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getNumberOfPaymentTokens()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 965
    new-instance v6, Lcom/swedbank/mobile/business/cards/x$a;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v6, v2}, Lcom/swedbank/mobile/business/cards/x$a;-><init>(I)V

    check-cast v6, Lcom/swedbank/mobile/business/cards/x;

    goto :goto_3

    .line 966
    :cond_1
    sget-object v2, Lcom/swedbank/mobile/business/cards/x$b;->a:Lcom/swedbank/mobile/business/cards/x$b;

    check-cast v2, Lcom/swedbank/mobile/business/cards/x;

    move-object v6, v2

    .line 970
    :goto_3
    invoke-interface {v1}, Lcom/meawallet/mtp/MeaCard;->getTokenInfo()Lcom/meawallet/mtp/MeaTokenInfo;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/meawallet/mtp/MeaTokenInfo;->getTokenPanSuffix()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 971
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v7, 0x14

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 972
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    rsub-int/lit8 v7, v7, 0x10

    const/4 v8, 0x1

    if-gt v8, v7, :cond_3

    :goto_4
    const/16 v9, 0x2a

    .line 973
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 974
    rem-int/lit8 v9, v8, 0x4

    if-nez v9, :cond_2

    const/16 v9, 0x20

    .line 975
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    if-eq v8, v7, :cond_3

    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 978
    :cond_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 979
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    goto :goto_5

    :cond_4
    const-string v1, ""

    :goto_5
    move-object v7, v1

    .line 944
    new-instance v1, Lcom/swedbank/mobile/business/cards/s$a;

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/business/cards/s$a;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/t;ZLcom/swedbank/mobile/business/cards/x;Ljava/lang/String;)V

    .line 980
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 981
    :cond_5
    check-cast v0, Ljava/util/List;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
