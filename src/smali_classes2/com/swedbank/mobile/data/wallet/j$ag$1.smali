.class final Lcom/swedbank/mobile/data/wallet/j$ag$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$ag;->a(Lcom/swedbank/mobile/data/wallet/a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/z<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$ag;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/j$ag;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$ag$1;->a:Lcom/swedbank/mobile/data/wallet/j$ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/x;)V
    .locals 4
    .param p1    # Lio/reactivex/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/meawallet/mtp/MeaCard;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 427
    :try_start_0
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$ag$1;->a:Lcom/swedbank/mobile/data/wallet/j$ag;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$ag;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->b(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/data/wallet/e;

    move-result-object v0

    .line 428
    iget-object v1, p0, Lcom/swedbank/mobile/data/wallet/j$ag$1;->a:Lcom/swedbank/mobile/data/wallet/j$ag;

    iget-object v1, v1, Lcom/swedbank/mobile/data/wallet/j$ag;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/data/wallet/e;->a(Ljava/lang/String;)Lcom/meawallet/mtp/MeaCard;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 943
    invoke-interface {v0}, Lcom/meawallet/mtp/MeaCard;->getState()Lcom/meawallet/mtp/MeaCardState;

    move-result-object v2

    sget-object v3, Lcom/meawallet/mtp/MeaCardState;->DEACTIVATED:Lcom/meawallet/mtp/MeaCardState;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    move-object v1, v0

    :cond_1
    if-eqz v1, :cond_2

    .line 431
    new-instance v0, Lcom/swedbank/mobile/business/util/n;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/util/n;-><init>(Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 945
    :catch_0
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 435
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    return-void
.end method
