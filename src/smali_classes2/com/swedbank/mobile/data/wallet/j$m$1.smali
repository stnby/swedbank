.class public final Lcom/swedbank/mobile/data/wallet/j$m$1;
.super Ljava/lang/Object;
.source "WalletRepositoryImpl.kt"

# interfaces
.implements Lcom/meawallet/mtp/MeaCardListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/j$m;->a(Lio/reactivex/x;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/j$m;

.field final synthetic b:Lio/reactivex/x;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/wallet/j$m;Lio/reactivex/x;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->a:Lcom/swedbank/mobile/data/wallet/j$m;

    iput-object p2, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->b:Lio/reactivex/x;

    .line 711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/meawallet/mtp/MeaError;)V
    .locals 2
    .param p1    # Lcom/meawallet/mtp/MeaError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 718
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->a:Lcom/swedbank/mobile/data/wallet/j$m;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$m;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->h(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failure at deleting card. ErrMsg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/meawallet/mtp/MeaError;->getCode()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 720
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->a:Lcom/swedbank/mobile/data/wallet/j$m;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$m;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->f(Lcom/swedbank/mobile/data/wallet/j;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/firebase/c;->a(Ljava/lang/Throwable;)V

    .line 721
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->b:Lio/reactivex/x;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 711
    check-cast p1, Lcom/meawallet/mtp/MeaError;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/j$m$1;->a(Lcom/meawallet/mtp/MeaError;)V

    return-void
.end method

.method public onSuccess(Lcom/meawallet/mtp/MeaCard;)V
    .locals 1
    .param p1    # Lcom/meawallet/mtp/MeaCard;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 713
    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->a:Lcom/swedbank/mobile/data/wallet/j$m;

    iget-object v0, v0, Lcom/swedbank/mobile/data/wallet/j$m;->a:Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/j;->h(Lcom/swedbank/mobile/data/wallet/j;)Lcom/b/c/c;

    move-result-object v0

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 714
    iget-object p1, p0, Lcom/swedbank/mobile/data/wallet/j$m$1;->b:Lio/reactivex/x;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    return-void
.end method
