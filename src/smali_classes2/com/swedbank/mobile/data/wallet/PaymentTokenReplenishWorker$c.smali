.class final Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;
.super Ljava/lang/Object;
.source "PaymentTokenReplenishWorker.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->b()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;->a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "replenishSuccessful"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$c;->a(Ljava/lang/Boolean;)Z

    move-result p1

    return p1
.end method
