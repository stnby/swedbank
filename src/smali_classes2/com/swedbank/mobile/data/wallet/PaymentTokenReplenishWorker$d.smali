.class final Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$d;
.super Ljava/lang/Object;
.source "PaymentTokenReplenishWorker.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->b()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$d;->a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Landroidx/work/ListenableWorker$a;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    sget-object p1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Replenish failed, cancelling work for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$d;->a:Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;

    invoke-static {v0}, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;->a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    invoke-static {}, Landroidx/work/ListenableWorker$a;->c()Landroidx/work/ListenableWorker$a;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker$d;->a(Ljava/lang/Boolean;)Landroidx/work/ListenableWorker$a;

    move-result-object p1

    return-object p1
.end method
