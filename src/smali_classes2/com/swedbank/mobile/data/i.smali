.class public final Lcom/swedbank/mobile/data/i;
.super Ljava/lang/Object;
.source "CoreDataModule.kt"


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "app"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/i;->a:Landroid/app/Application;

    .line 49
    iget-object p1, p0, Lcom/swedbank/mobile/data/i;->a:Landroid/app/Application;

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/co;->a(Landroid/app/Application;)Lcom/siimkinks/sqlitemagic/co$a;

    move-result-object p1

    .line 50
    new-instance v0, Landroidx/k/a/a/c;

    invoke-direct {v0}, Landroidx/k/a/a/c;-><init>()V

    check-cast v0, Landroidx/k/a/c$c;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/co$a;->a(Landroidx/k/a/c$c;)Lcom/siimkinks/sqlitemagic/co$a;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/co$a;->a()V

    .line 52
    iget-object p1, p0, Lcom/swedbank/mobile/data/i;->a:Landroid/app/Application;

    invoke-static {p1}, Lcom/b/d/a;->a(Landroid/app/Application;)V

    .line 53
    invoke-static {}, Lcom/swedbank/mobile/data/l/b;->a()V

    .line 155
    sget-object p1, Lcom/swedbank/mobile/data/j$a;->a:Lcom/swedbank/mobile/data/j$a;

    check-cast p1, Lio/reactivex/c/g;

    invoke-static {p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/g;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/a/a/f;
    .locals 2
    .annotation runtime Ljavax/inject/Named;
        value = "app_preferences"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/data/i;->a:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 62
    invoke-static {v0}, Lcom/a/a/a/f;->a(Landroid/content/SharedPreferences;)Lcom/a/a/a/f;

    move-result-object v0

    const-string v1, "RxSharedPreferences.create(preferences)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/k/a;)Ljava/util/Set;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/g/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/k/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/g/f;",
            "Lcom/swedbank/mobile/data/k/a;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_instance_id_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "remoteConfigUpdateStreamImpl"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushInstanceIdWatcher"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 80
    new-array v0, v0, [Lcom/swedbank/mobile/data/push/b;

    .line 81
    check-cast p1, Lcom/swedbank/mobile/data/push/b;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 82
    check-cast p2, Lcom/swedbank/mobile/data/push/b;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 80
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/ordering/i;Lcom/swedbank/mobile/data/push/h;)Ljava/util/Set;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/g/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/ordering/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/g/f;",
            "Lcom/swedbank/mobile/data/ordering/i;",
            "Lcom/swedbank/mobile/data/push/h;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "remoteConfigUpdateStreamImpl"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "systemPushMessageStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPushMessageStreamImpl"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    .line 92
    new-array v0, v0, [Lcom/swedbank/mobile/data/push/a;

    .line 93
    check-cast p1, Lcom/swedbank/mobile/data/push/a;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 94
    check-cast p2, Lcom/swedbank/mobile/data/push/a;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 95
    check-cast p3, Lcom/swedbank/mobile/data/push/a;

    const/4 p1, 0x2

    aput-object p3, v0, p1

    .line 92
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public final b()Landroidx/work/o;
    .locals 2
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 67
    invoke-static {}, Landroidx/work/o;->a()Landroidx/work/o;

    move-result-object v0

    const-string v1, "WorkManager.getInstance()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "app_data_clear_streams"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 72
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
