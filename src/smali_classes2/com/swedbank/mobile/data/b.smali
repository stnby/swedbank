.class public final Lcom/swedbank/mobile/data/b;
.super Ljava/lang/Object;
.source "CardsDataModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/data/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/swedbank/mobile/data/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/b;->a:Lcom/swedbank/mobile/data/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x4

    .line 70
    new-array v0, v0, [Lkotlin/k;

    .line 71
    const-class v1, Lcom/swedbank/mobile/business/cards/CardState;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 72
    const-class v2, Lcom/swedbank/mobile/business/cards/CardState;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 73
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 71
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 74
    const-class v1, Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 75
    const-class v2, Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 76
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 74
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 77
    const-class v1, Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 78
    const-class v2, Lcom/swedbank/mobile/business/cards/CardLimitService;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 79
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardLimitService;->UNRECOGNIZED:Lcom/swedbank/mobile/business/cards/CardLimitService;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 77
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 80
    const-class v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 81
    const-class v2, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 82
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->UNRECOGNIZED:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 80
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 70
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/wallet/ad;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ")",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "app_data_clear_streams"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 42
    new-array v0, v0, [Lio/reactivex/b;

    .line 43
    invoke-interface {p0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->p()Lio/reactivex/b;

    move-result-object p0

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 42
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/data/wallet/h;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/data/wallet/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/wallet/h;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "walletPushMessageHandler"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 62
    new-array v0, v0, [Lcom/swedbank/mobile/data/wallet/h;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/data/wallet/i;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/data/wallet/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/wallet/i;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_instance_id_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "walletPushMessageInstanceIdHandler"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 52
    new-array v0, v0, [Lcom/swedbank/mobile/data/wallet/i;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
