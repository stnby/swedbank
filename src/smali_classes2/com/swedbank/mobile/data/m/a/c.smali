.class public final Lcom/swedbank/mobile/data/m/a/c;
.super Ljava/lang/Object;
.source "WidgetRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/widget/configuration/h;


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/app/AlarmManager;

.field private final d:Landroid/app/PendingIntent;

.field private final e:Lcom/swedbank/mobile/business/a/e;

.field private final f:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/a/e;Lcom/swedbank/mobile/architect/business/f;Lcom/a/a/a/f;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/business/a/e;",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/a/a/a/f;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localAccountRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeAllCards"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferences"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/data/m/a/c;->e:Lcom/swedbank/mobile/business/a/e;

    iput-object p3, p0, Lcom/swedbank/mobile/data/m/a/c;->f:Lcom/swedbank/mobile/architect/business/f;

    const-string p2, "widgetBalanceShownPreference"

    const/4 p3, 0x0

    .line 46
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p4, p2, v0}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object p2

    const-string v0, "appPreferences.getBoolea\u2026GET_BALANCE_SHOWN, false)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/data/m/a/c;->a:Lcom/a/a/a/d;

    const-string p2, "widgetDataLoadingPreference"

    .line 47
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p4, p2, v0}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/a/a/a/d;

    move-result-object p2

    const-string p4, "appPreferences.getBoolea\u2026DGET_DATA_LOADING, false)"

    invoke-static {p2, p4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/data/m/a/c;->b:Lcom/a/a/a/d;

    const-string p2, "alarm"

    .line 48
    invoke-virtual {p1, p2}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Landroid/app/AlarmManager;

    iput-object p2, p0, Lcom/swedbank/mobile/data/m/a/c;->c:Landroid/app/AlarmManager;

    .line 50
    check-cast p1, Landroid/content/Context;

    const p2, 0x74c27b5

    .line 52
    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/data/m/a/c;->b(Z)Landroid/content/Intent;

    move-result-object p3

    const/high16 p4, 0x10000000

    .line 49
    invoke-static {p1, p2, p3, p4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/m/a/c;->d:Landroid/app/PendingIntent;

    return-void

    .line 48
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.app.AlarmManager"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/m/a/c;)Lcom/swedbank/mobile/business/a/e;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/data/m/a/c;->e:Lcom/swedbank/mobile/business/a/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/m/a/c;)Lcom/swedbank/mobile/architect/business/f;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/data/m/a/c;->f:Lcom/swedbank/mobile/architect/business/f;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/m/a/c;)Lcom/a/a/a/d;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/data/m/a/c;->a:Lcom/a/a/a/d;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/util/List;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/swedbank/mobile/data/m/a/c$d;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/data/m/a/c$d;-><init>(Ljava/util/List;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromCallable\u2026ecute()\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Z)Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 116
    new-instance v0, Lcom/swedbank/mobile/data/m/a/c$e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/m/a/c$e;-><init>(Lcom/swedbank/mobile/data/m/a/c;Z)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026own.set(balanceShown)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/siimkinks/sqlitemagic/ed;->a:Lcom/siimkinks/sqlitemagic/ed;

    const-string v2, "WIDGET_ENROLLED_ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object v0

    .line 59
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ab;->b()Lcom/siimkinks/sqlitemagic/al;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM W\u2026nt()\n          .observe()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/al;->a()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM W\u2026ve()\n          .isNotZero"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 129
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "extra_account_shown_status"

    .line 130
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "Intent(AppWidgetManager.\u2026OWN_STATUS, showBalances)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 64
    sget-object v1, Lcom/siimkinks/sqlitemagic/ed;->a:Lcom/siimkinks/sqlitemagic/ed;

    const-string v2, "WIDGET_ENROLLED_ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->b()Lio/reactivex/w;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/swedbank/mobile/data/m/a/c$a;->a:Lcom/swedbank/mobile/data/m/a/c$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM W\u2026AccountData::accountId) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 70
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 71
    sget-object v1, Lcom/siimkinks/sqlitemagic/ed;->a:Lcom/siimkinks/sqlitemagic/ed;

    const-string v2, "WIDGET_ENROLLED_ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object v0

    .line 74
    sget-object v1, Lcom/swedbank/mobile/data/m/a/c$c;->a:Lcom/swedbank/mobile/data/m/a/c$c;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM W\u2026AccountData::accountId) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/swedbank/mobile/data/m/a/c;->b:Lcom/a/a/a/d;

    .line 124
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/a/a/a/d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public d()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/widget/configuration/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 91
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/m/a/c;->b()Lio/reactivex/w;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/swedbank/mobile/data/m/a/c$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/m/a/c$b;-><init>(Lcom/swedbank/mobile/data/m/a/c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "getEnrolledAccountIds()\n\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/swedbank/mobile/data/m/a/c;->a:Lcom/a/a/a/d;

    .line 121
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "widgetBalanceShown\n      .asObservable()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()V
    .locals 5

    .line 132
    iget-object v0, p0, Lcom/swedbank/mobile/data/m/a/c;->c:Landroid/app/AlarmManager;

    .line 134
    invoke-static {}, Lorg/threeten/bp/Instant;->now()Lorg/threeten/bp/Instant;

    move-result-object v1

    const-wide/16 v2, 0xf

    .line 135
    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/Instant;->plusSeconds(J)Lorg/threeten/bp/Instant;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v1

    .line 137
    iget-object v3, p0, Lcom/swedbank/mobile/data/m/a/c;->d:Landroid/app/PendingIntent;

    const/4 v4, 0x1

    .line 132
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public g()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/data/m/a/c;->c:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/swedbank/mobile/data/m/a/c;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public h()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/data/m/a/c;->b:Lcom/a/a/a/d;

    .line 127
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "widgetDataLoading\n      .asObservable()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
