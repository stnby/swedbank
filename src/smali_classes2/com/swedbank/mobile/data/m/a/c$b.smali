.class final Lcom/swedbank/mobile/data/m/a/c$b;
.super Ljava/lang/Object;
.source "WidgetRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/m/a/c;->d()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/m/a/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/m/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/m/a/c$b;->a:Lcom/swedbank/mobile/data/m/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/w;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/widget/configuration/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 95
    iget-object v0, p0, Lcom/swedbank/mobile/data/m/a/c$b;->a:Lcom/swedbank/mobile/data/m/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/data/m/a/c;->a(Lcom/swedbank/mobile/data/m/a/c;)Lcom/swedbank/mobile/business/a/e;

    move-result-object v0

    .line 96
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->a(Ljava/util/List;)Lio/reactivex/o;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    const-string v1, "localAccountRepository\n \u2026          .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/aa;

    .line 98
    iget-object v1, p0, Lcom/swedbank/mobile/data/m/a/c$b;->a:Lcom/swedbank/mobile/data/m/a/c;

    invoke-static {v1}, Lcom/swedbank/mobile/data/m/a/c;->b(Lcom/swedbank/mobile/data/m/a/c;)Lcom/swedbank/mobile/architect/business/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v1

    .line 100
    new-instance v2, Lcom/swedbank/mobile/data/m/a/c$b$1;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/data/m/a/c$b$1;-><init>(Ljava/util/List;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v1, "observeAllCards()\n      \u2026)\n                      }"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/aa;

    .line 141
    new-instance v1, Lcom/swedbank/mobile/data/m/a/c$b$a;

    invoke-direct {v1}, Lcom/swedbank/mobile/data/m/a/c$b$a;-><init>()V

    check-cast v1, Lio/reactivex/c/c;

    invoke-static {v0, p1, v1}, Lio/reactivex/w;->a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/c;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.zip(s1, s2, BiFun\u2026-> zipper.invoke(t, u) })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(emptyList())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/m/a/c$b;->a(Ljava/util/List;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
