.class public final Lcom/swedbank/mobile/data/m/a/a;
.super Ljava/lang/Object;
.source "SqliteMagic_WidgetEnrolledAccountData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/m/a/b;
    .locals 3

    .line 48
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 49
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 50
    new-instance p1, Lcom/swedbank/mobile/data/m/a/b;

    .line 51
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 52
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, v1, p0}, Lcom/swedbank/mobile/data/m/a/b;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/m/a/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/m/a/b;"
        }
    .end annotation

    .line 58
    invoke-virtual {p2, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p2, :cond_1

    .line 60
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_0

    return-object v0

    :cond_0
    const-string p2, "widget_enrolled_account_data"

    .line 65
    :cond_1
    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_3

    .line 67
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 68
    new-instance p2, Lcom/swedbank/mobile/data/m/a/b;

    .line 69
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 70
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, v0, p0}, Lcom/swedbank/mobile/data/m/a/b;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    return-object p2

    .line 72
    :cond_3
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".local_id"

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".account_id"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_6

    .line 77
    new-instance p2, Lcom/swedbank/mobile/data/m/a/b;

    if-eqz p3, :cond_5

    .line 78
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 79
    :cond_5
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, v0, p0}, Lcom/swedbank/mobile/data/m/a/b;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    return-object p2

    .line 75
    :cond_6
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"widget_enrolled_account_data\" required column \"account_id\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/m/a/b;)V
    .locals 1

    .line 32
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 33
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/m/a/b;->a()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p0, v0, p1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    return-void
.end method
