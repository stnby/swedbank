.class public final Lcom/swedbank/mobile/data/m/a/c$b$a;
.super Ljava/lang/Object;
.source "Singles.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/m/a/c$b;->a(Ljava/util/List;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/a/a;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;TR;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)TR;"
        }
    .end annotation

    .line 14
    check-cast p2, Ljava/util/Map;

    check-cast p1, Ljava/util/List;

    const-string v0, "accounts"

    .line 104
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 106
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 107
    check-cast v1, Lcom/swedbank/mobile/business/a/a;

    .line 110
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/a;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    if-eqz v2, :cond_0

    .line 113
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v4

    .line 114
    instance-of v5, v4, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v5, :cond_0

    .line 115
    check-cast v4, Lcom/swedbank/mobile/business/cards/s$a;

    .line 116
    new-instance v3, Lcom/swedbank/mobile/business/widget/configuration/b;

    .line 117
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/s$a;->a()Ljava/lang/String;

    move-result-object v4

    .line 118
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->i()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v2

    .line 116
    invoke-direct {v3, v4, v2}, Lcom/swedbank/mobile/business/widget/configuration/b;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;)V

    .line 108
    :cond_0
    new-instance v2, Lcom/swedbank/mobile/business/widget/configuration/a;

    invoke-direct {v2, v1, v3}, Lcom/swedbank/mobile/business/widget/configuration/a;-><init>(Lcom/swedbank/mobile/business/a/a;Lcom/swedbank/mobile/business/widget/configuration/b;)V

    .line 110
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
