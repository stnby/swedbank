.class final Lcom/swedbank/mobile/data/m/a/c$d;
.super Ljava/lang/Object;
.source "WidgetRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/m/a/c;->a(Ljava/util/List;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/m/a/c$d;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .line 141
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    :try_start_0
    sget-object v1, Lcom/siimkinks/sqlitemagic/ee;->a:Lcom/siimkinks/sqlitemagic/ee;

    .line 144
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dh$b;->a()Lcom/siimkinks/sqlitemagic/dh$b;

    move-result-object v1

    const-string v2, "SqliteMagic_WidgetEnroll\u2026leteTableBuilder.create()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/a/b;

    .line 81
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/a/b;->b()I

    .line 82
    sget-object v1, Lcom/siimkinks/sqlitemagic/ee;->a:Lcom/siimkinks/sqlitemagic/ee;

    .line 83
    iget-object v1, p0, Lcom/swedbank/mobile/data/m/a/c$d;->a:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 145
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 146
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 147
    check-cast v3, Ljava/lang/String;

    .line 84
    new-instance v4, Lcom/swedbank/mobile/data/m/a/b;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v4, v6, v3, v5, v6}, Lcom/swedbank/mobile/data/m/a/b;-><init>(Ljava/lang/Long;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 149
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/dh$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/dh$a;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/dh$a;->a()Z

    .line 150
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/m/a/c$d;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
