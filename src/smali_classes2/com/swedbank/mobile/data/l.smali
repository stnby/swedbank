.class public final Lcom/swedbank/mobile/data/l;
.super Ljava/lang/Object;
.source "CoreDataModule_ProvideAppPreferencesFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/a/a/a/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/i;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/data/l;->a:Lcom/swedbank/mobile/data/i;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/data/l;
    .locals 1

    .line 21
    new-instance v0, Lcom/swedbank/mobile/data/l;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/l;-><init>(Lcom/swedbank/mobile/data/i;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/data/i;)Lcom/a/a/a/f;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/i;->a()Lcom/a/a/a/f;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/a/a/a/f;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/a/a/a/f;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/data/l;->a:Lcom/swedbank/mobile/data/i;

    invoke-static {v0}, Lcom/swedbank/mobile/data/l;->b(Lcom/swedbank/mobile/data/i;)Lcom/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/l;->a()Lcom/a/a/a/f;

    move-result-object v0

    return-object v0
.end method
