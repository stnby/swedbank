.class public final Lcom/swedbank/mobile/data/e/a/a;
.super Ljava/lang/Object;
.source "ShortcutRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/external/shortcut/g;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/d;

.field private final c:Landroid/app/Application;

.field private final d:Lcom/swedbank/mobile/business/f/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/data/e/a/a;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "shortcutManager"

    const-string v4, "getShortcutManager()Landroid/content/pm/ShortcutManager;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/data/e/a/a;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/f/a;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/e/a/a;->c:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/data/e/a/a;->d:Lcom/swedbank/mobile/business/f/a;

    .line 22
    new-instance p1, Lcom/swedbank/mobile/data/e/a/a$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/e/a/a$a;-><init>(Lcom/swedbank/mobile/data/e/a/a;)V

    check-cast p1, Lkotlin/e/a/a;

    invoke-static {p1}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/e/a/a;->b:Lkotlin/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/e/a/a;)Landroid/app/Application;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/data/e/a/a;->c:Landroid/app/Application;

    return-object p0
.end method

.method private final b()Landroid/content/pm/ShortcutManager;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/e/a/a;->b:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/data/e/a/a;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutManager;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/e/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/data/e/a/a;->b()Landroid/content/pm/ShortcutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/pm/ShortcutManager;->reportShortcutUsed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/ShortcutInfo;",
            ">;)V"
        }
    .end annotation

    const-string v0, "shortcuts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/swedbank/mobile/data/e/a/a;->b()Landroid/content/pm/ShortcutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/pm/ShortcutManager;->setDynamicShortcuts(Ljava/util/List;)Z

    :cond_0
    return-void
.end method

.method public a()Z
    .locals 2

    .line 26
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/e/a/a;->d:Lcom/swedbank/mobile/business/f/a;

    const-string v1, "feature_shortcuts"

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
