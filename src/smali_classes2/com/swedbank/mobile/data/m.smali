.class public final Lcom/swedbank/mobile/data/m;
.super Ljava/lang/Object;
.source "CoreDataModule_ProvidePushMessageHandlersFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/swedbank/mobile/data/push/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/i;

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/g/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/ordering/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/push/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static a(Lcom/swedbank/mobile/data/i;Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/ordering/i;Lcom/swedbank/mobile/data/push/h;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/i;",
            "Lcom/swedbank/mobile/data/g/f;",
            "Lcom/swedbank/mobile/data/ordering/i;",
            "Lcom/swedbank/mobile/data/push/h;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .line 48
    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/i;->a(Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/ordering/i;Lcom/swedbank/mobile/data/push/h;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/data/m;->a:Lcom/swedbank/mobile/data/i;

    iget-object v1, p0, Lcom/swedbank/mobile/data/m;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/g/f;

    iget-object v2, p0, Lcom/swedbank/mobile/data/m;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/ordering/i;

    iget-object v3, p0, Lcom/swedbank/mobile/data/m;->d:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/data/push/h;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/m;->a(Lcom/swedbank/mobile/data/i;Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/ordering/i;Lcom/swedbank/mobile/data/push/h;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/m;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
