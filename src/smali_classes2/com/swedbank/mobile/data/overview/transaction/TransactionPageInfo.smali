.class public final Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;
.super Ljava/lang/Object;
.source "TransactionListResponse.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 116
    move-object p1, v0

    check-cast p1, Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 117
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    .line 118
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/f;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/f;

    .line 122
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    :goto_0
    move-object v4, v1

    goto :goto_1

    :cond_0
    const-string v1, ""

    goto :goto_0

    .line 123
    :goto_1
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    :goto_2
    move-object v5, v1

    goto :goto_3

    :cond_1
    const-string v1, ""

    goto :goto_2

    .line 124
    :goto_3
    iget-boolean v6, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c:Z

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    move-object v1, v0

    move-object v3, p1

    .line 120
    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/data/overview/transaction/f;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/e/b/g;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/c;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/c;

    .line 129
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    :goto_0
    move-object v4, v1

    goto :goto_1

    :cond_0
    const-string v1, ""

    goto :goto_0

    .line 130
    :goto_1
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    :goto_2
    move-object v5, v1

    goto :goto_3

    :cond_1
    const-string v1, ""

    goto :goto_2

    .line 131
    :goto_3
    iget-boolean v6, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c:Z

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    move-object v1, v0

    move-object v3, p1

    .line 127
    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/data/overview/transaction/c;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/e/b/g;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 118
    iget-boolean v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c:Z

    return v0
.end method
