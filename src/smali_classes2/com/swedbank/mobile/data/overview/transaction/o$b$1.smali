.class public final Lcom/swedbank/mobile/data/overview/transaction/o$b$1;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/transaction/o$b;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/transaction/o$b;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/o$b;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;->a:Lcom/swedbank/mobile/data/overview/transaction/o$b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lcom/swedbank/mobile/data/overview/transaction/a;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;",
            ">;)",
            "Lcom/swedbank/mobile/data/overview/transaction/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/a;

    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;->a:Lcom/swedbank/mobile/data/overview/transaction/o$b;

    iget-object v1, v1, Lcom/swedbank/mobile/data/overview/transaction/o$b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;->b:Ljava/lang/String;

    const-string v3, "endCursor"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, p1}, Lcom/swedbank/mobile/data/overview/transaction/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/w;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;->a(Lcom/swedbank/mobile/data/network/w;)Lcom/swedbank/mobile/data/overview/transaction/a;

    move-result-object p1

    return-object p1
.end method
