.class public final Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;
.super Ljava/lang/Object;
.source "TransactionListResponse.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "node"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cursor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/data/overview/transaction/e;
    .locals 22
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    .line 55
    iget-object v1, v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    .line 57
    iget-object v4, v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    .line 58
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->a()Ljava/lang/String;

    move-result-object v5

    .line 59
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->b()Ljava/util/Date;

    move-result-object v6

    .line 60
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->c()Ljava/util/Date;

    move-result-object v7

    .line 61
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 62
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 63
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->f()Ljava/lang/String;

    move-result-object v11

    .line 64
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->g()Ljava/lang/String;

    move-result-object v12

    .line 65
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->h()Ljava/lang/String;

    move-result-object v13

    .line 66
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->i()Ljava/lang/String;

    move-result-object v14

    .line 67
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->j()Ljava/lang/String;

    move-result-object v15

    .line 68
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->k()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v16

    .line 69
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->l()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v17

    .line 70
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->m()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v18

    .line 71
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->n()I

    move-result v19

    .line 56
    new-instance v1, Lcom/swedbank/mobile/data/overview/transaction/e;

    move-object v2, v1

    const/4 v3, 0x0

    const/16 v20, 0x1

    const/16 v21, 0x0

    invoke-direct/range {v2 .. v21}, Lcom/swedbank/mobile/data/overview/transaction/e;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;IILkotlin/e/b/g;)V

    return-object v1
.end method

.method public final b()Lcom/swedbank/mobile/data/overview/transaction/b;
    .locals 22
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    .line 75
    iget-object v1, v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    .line 77
    iget-object v4, v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    .line 78
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->a()Ljava/lang/String;

    move-result-object v5

    .line 79
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->b()Ljava/util/Date;

    move-result-object v6

    .line 80
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->c()Ljava/util/Date;

    move-result-object v7

    .line 81
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 82
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 83
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->f()Ljava/lang/String;

    move-result-object v11

    .line 84
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->g()Ljava/lang/String;

    move-result-object v12

    .line 85
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->h()Ljava/lang/String;

    move-result-object v13

    .line 86
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->i()Ljava/lang/String;

    move-result-object v14

    .line 87
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->j()Ljava/lang/String;

    move-result-object v15

    .line 88
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->k()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v16

    .line 89
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->l()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v17

    .line 90
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->m()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v18

    .line 91
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->n()I

    move-result v19

    .line 76
    new-instance v1, Lcom/swedbank/mobile/data/overview/transaction/b;

    move-object v2, v1

    const/4 v3, 0x0

    const/16 v20, 0x1

    const/16 v21, 0x0

    invoke-direct/range {v2 .. v21}, Lcom/swedbank/mobile/data/overview/transaction/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;IILkotlin/e/b/g;)V

    return-object v1
.end method

.method public final c()Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    iget-object v1, p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    iget-object p1, p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransactionEdge(node="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
