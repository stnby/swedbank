.class final Lcom/swedbank/mobile/data/overview/transaction/p$a;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/overview/transaction/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/overview/transaction/p$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/p$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/overview/transaction/p$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/p$a;->a:Lcom/swedbank/mobile/data/overview/transaction/p$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;)I
    .locals 4

    .line 481
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->c()Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    move-result-object p1

    .line 482
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->c()Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    move-result-object p2

    .line 483
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->c()Ljava/util/Date;

    move-result-object v0

    .line 484
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->c()Ljava/util/Date;

    move-result-object v1

    .line 485
    invoke-virtual {v1, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    if-nez v0, :cond_0

    .line 487
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 488
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 491
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->e()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    .line 492
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->e()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    .line 493
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(II)I

    move-result v0

    :cond_0
    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    check-cast p2, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/data/overview/transaction/p$a;->a(Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;)I

    move-result p1

    return p1
.end method
