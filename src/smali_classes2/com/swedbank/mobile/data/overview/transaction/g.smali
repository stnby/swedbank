.class public final Lcom/swedbank/mobile/data/overview/transaction/g;
.super Ljava/lang/Object;
.source "SqliteMagic_CombinedTransactionData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/overview/transaction/b;
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 118
    iget v2, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 119
    iget v3, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v3, v3, 0x10

    iput v3, v1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 120
    new-instance v1, Lcom/swedbank/mobile/data/overview/transaction/b;

    .line 121
    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v5, v3

    goto :goto_1

    :cond_0
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    :goto_1
    add-int/lit8 v3, v2, 0x1

    .line 122
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v3, v2, 0x2

    .line 123
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v3, v2, 0x3

    .line 124
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v8

    add-int/lit8 v3, v2, 0x4

    .line 125
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v9

    add-int/lit8 v3, v2, 0x5

    .line 126
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    add-int/lit8 v3, v2, 0x6

    .line 127
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    add-int/lit8 v3, v2, 0x7

    .line 128
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    add-int/lit8 v3, v2, 0x8

    .line 129
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    add-int/lit8 v3, v2, 0x9

    .line 130
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    add-int/lit8 v3, v2, 0xa

    .line 131
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    add-int/lit8 v3, v2, 0xb

    .line 132
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    add-int/lit8 v3, v2, 0xc

    .line 133
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v18

    add-int/lit8 v3, v2, 0xd

    .line 134
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v19

    add-int/lit8 v3, v2, 0xe

    .line 135
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v20

    add-int/lit8 v2, v2, 0xf

    .line 136
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    move-object v4, v1

    invoke-direct/range {v4 .. v21}, Lcom/swedbank/mobile/data/overview/transaction/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V

    return-object v1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/b;
    .locals 43
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/overview/transaction/b;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 142
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 144
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "combined_transaction_data"

    .line 149
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_3

    .line 151
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 152
    new-instance v2, Lcom/swedbank/mobile/data/overview/transaction/b;

    .line 153
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_0
    move-object v5, v3

    goto :goto_1

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    :goto_1
    add-int/lit8 v3, v1, 0x1

    .line 154
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v3, v1, 0x2

    .line 155
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v3, v1, 0x3

    .line 156
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v8

    add-int/lit8 v3, v1, 0x4

    .line 157
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v9

    add-int/lit8 v3, v1, 0x5

    .line 158
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    add-int/lit8 v3, v1, 0x6

    .line 159
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    add-int/lit8 v3, v1, 0x7

    .line 160
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    add-int/lit8 v3, v1, 0x8

    .line 161
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    add-int/lit8 v3, v1, 0x9

    .line 162
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    add-int/lit8 v3, v1, 0xa

    .line 163
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    add-int/lit8 v3, v1, 0xb

    .line 164
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    add-int/lit8 v3, v1, 0xc

    .line 165
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v18

    add-int/lit8 v3, v1, 0xd

    .line 166
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v19

    add-int/lit8 v3, v1, 0xe

    .line 167
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v20

    add-int/lit8 v1, v1, 0xf

    .line 168
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    move-object v4, v2

    invoke-direct/range {v4 .. v21}, Lcom/swedbank/mobile/data/overview/transaction/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V

    return-object v2

    .line 170
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".local_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 171
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".remote_cursor"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_14

    .line 175
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".account_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_13

    .line 179
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".transaction_date"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_12

    .line 183
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".transaction_bank_date"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_11

    .line 187
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".transaction_number"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_10

    .line 191
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, ".transaction_part"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_f

    .line 195
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, ".amount"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    if-eqz v11, :cond_e

    .line 199
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ".currency"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    if-eqz v12, :cond_d

    .line 203
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, ".counterparty"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    if-eqz v13, :cond_c

    .line 207
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, ".counterparty_account"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    if-eqz v14, :cond_b

    .line 211
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".description"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_a

    .line 215
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v22, v3

    const-string v3, ".type"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_9

    .line 219
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v23, v3

    const-string v3, ".payment_type"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_8

    .line 223
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v24, v3

    const-string v3, ".direction"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_7

    .line 227
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".number_of_transactions"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 231
    new-instance v2, Lcom/swedbank/mobile/data/overview/transaction/b;

    if-eqz v4, :cond_5

    .line 232
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-interface {v0, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-eqz v15, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v26, v4

    goto :goto_3

    :cond_5
    :goto_2
    const/16 v26, 0x0

    .line 233
    :goto_3
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 234
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 235
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v29

    .line 236
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v30

    .line 237
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v31

    .line 238
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 239
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 240
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 241
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 242
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 243
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 244
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/overview/transaction/m;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v39

    .line 245
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/data/overview/transaction/m;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v40

    .line 246
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v41

    .line 247
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v42

    move-object/from16 v25, v2

    invoke-direct/range {v25 .. v42}, Lcom/swedbank/mobile/data/overview/transaction/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V

    return-object v2

    .line 229
    :cond_6
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"number_of_transactions\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_7
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"direction\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_8
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"payment_type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_9
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_a
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"description\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_b
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"counterparty_account\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"counterparty\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_d
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"currency\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_e
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"amount\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_f
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"transaction_part\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_10
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"transaction_number\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_11
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"transaction_bank_date\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_12
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"transaction_date\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_13
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"account_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_14
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"combined_transaction_data\" required column \"remote_cursor\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/overview/transaction/b;)V
    .locals 3

    .line 74
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 75
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 77
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->c()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/util/Date;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 v2, 0x3

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 78
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->d()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/util/Date;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 v2, 0x4

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 79
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->e()J

    move-result-wide v0

    const/4 v2, 0x5

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 80
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->f()I

    move-result v0

    int-to-long v0, v0

    const/4 v2, 0x6

    invoke-interface {p0, v2, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    .line 81
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 82
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->h()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 83
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->i()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->j()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 85
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->k()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->l()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Lcom/swedbank/mobile/business/overview/Transaction$Type;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Lcom/swedbank/mobile/business/overview/Transaction$Direction;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xe

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/b;->o()I

    move-result p1

    int-to-long v0, p1

    const/16 p1, 0xf

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    return-void
.end method
