.class public final Lcom/swedbank/mobile/data/overview/transaction/l$a;
.super Ljava/lang/Object;
.source "TransactionData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/overview/transaction/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/data/overview/transaction/l;)Lcom/swedbank/mobile/business/overview/Transaction;
    .locals 14
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    new-instance v13, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 43
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->b()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->a()Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->c()Ljava/util/Date;

    move-result-object v3

    .line 46
    new-instance v4, Ljava/math/BigDecimal;

    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 47
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->h()Ljava/lang/String;

    move-result-object v5

    .line 48
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->i()Ljava/lang/String;

    move-result-object v6

    .line 49
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->j()Ljava/lang/String;

    move-result-object v7

    .line 50
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->k()Ljava/lang/String;

    move-result-object v8

    .line 51
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->l()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v9

    .line 52
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v10

    .line 53
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v11

    .line 54
    invoke-interface {p0}, Lcom/swedbank/mobile/data/overview/transaction/l;->o()I

    move-result v12

    move-object v0, v13

    .line 42
    invoke-direct/range {v0 .. v12}, Lcom/swedbank/mobile/business/overview/Transaction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V

    return-object v13
.end method
