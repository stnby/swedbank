.class public final Lcom/swedbank/mobile/data/overview/transaction/k;
.super Ljava/lang/Object;
.source "SqliteMagic_TransactionDataView_Dao.java"


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/n;->a:Lcom/siimkinks/sqlitemagic/ae;

    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/k;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/n;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/overview/transaction/n;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "remote_cursor"

    .line 19
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_e

    const-string v3, "account_id"

    .line 23
    invoke-virtual {v1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_d

    const-string v4, "transaction_date"

    .line 27
    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_c

    const-string v5, "transaction_bank_date"

    .line 31
    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_b

    const-string v6, "transaction_number"

    .line 35
    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_a

    const-string v7, "transaction_part"

    .line 39
    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_9

    const-string v8, "amount"

    .line 43
    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_8

    const-string v9, "currency"

    .line 47
    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_7

    const-string v10, "counterparty"

    .line 51
    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_6

    const-string v11, "counterparty_account"

    .line 55
    invoke-virtual {v1, v11}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    if-eqz v11, :cond_5

    const-string v12, "description"

    .line 59
    invoke-virtual {v1, v12}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    if-eqz v12, :cond_4

    const-string v13, "type"

    .line 63
    invoke-virtual {v1, v13}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    if-eqz v13, :cond_3

    const-string v14, "payment_type"

    .line 67
    invoke-virtual {v1, v14}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    if-eqz v14, :cond_2

    const-string v15, "direction"

    .line 71
    invoke-virtual {v1, v15}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    if-eqz v15, :cond_1

    move-object/from16 v16, v15

    const-string v15, "number_of_transactions"

    .line 75
    invoke-virtual {v1, v15}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 79
    new-instance v15, Lcom/swedbank/mobile/data/overview/transaction/n;

    move-object/from16 v17, v15

    .line 80
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 81
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 82
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v20

    .line 83
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/data/l/c;->a(Ljava/lang/Long;)Ljava/util/Date;

    move-result-object v21

    .line 84
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 85
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 86
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 87
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 88
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 89
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 90
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 91
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/data/overview/transaction/m;->b(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v30

    .line 92
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/data/overview/transaction/m;->c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v31

    .line 93
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/data/overview/transaction/m;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v32

    .line 94
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    invoke-direct/range {v17 .. v33}, Lcom/swedbank/mobile/data/overview/transaction/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V

    return-object v15

    .line 77
    :cond_0
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"number_of_transactions\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"direction\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_2
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"payment_type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_3
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"type\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_4
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"description\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_5
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"counterparty_account\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_6
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"counterparty\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_7
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"currency\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_8
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"amount\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_9
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"transaction_part\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_a
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"transaction_number\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_b
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"transaction_bank_date\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"transaction_date\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_d
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"account_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_e
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"transaction_data_view\" required column \"remote_cursor\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
