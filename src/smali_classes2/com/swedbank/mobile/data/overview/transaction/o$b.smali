.class public final Lcom/swedbank/mobile/data/overview/transaction/o$b;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/overview/transaction/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/transaction/o;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/o;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->a:Lcom/swedbank/mobile/data/overview/transaction/o;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/overview/transaction/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "endCursor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xf

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->a:Lcom/swedbank/mobile/data/overview/transaction/o;

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/transaction/o;->a(Lcom/swedbank/mobile/data/overview/transaction/o;)Lcom/swedbank/mobile/data/overview/transaction/r;

    move-result-object v0

    .line 302
    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->b:Ljava/lang/String;

    .line 303
    iget-object v3, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->c:Ljava/lang/String;

    .line 301
    invoke-static {v0, v2, v3, v1}, Lcom/swedbank/mobile/data/overview/transaction/s;->a(Lcom/swedbank/mobile/data/overview/transaction/r;Ljava/lang/String;Ljava/lang/String;I)Lio/reactivex/w;

    move-result-object v0

    goto :goto_1

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->a:Lcom/swedbank/mobile/data/overview/transaction/o;

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/transaction/o;->a(Lcom/swedbank/mobile/data/overview/transaction/o;)Lcom/swedbank/mobile/data/overview/transaction/r;

    move-result-object v0

    .line 307
    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->b:Ljava/lang/String;

    .line 308
    iget-object v3, p0, Lcom/swedbank/mobile/data/overview/transaction/o$b;->c:Ljava/lang/String;

    .line 306
    invoke-static {v0, v2, v3, v1, p1}, Lcom/swedbank/mobile/data/overview/transaction/s;->a(Lcom/swedbank/mobile/data/overview/transaction/r;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 311
    :goto_1
    new-instance v1, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/overview/transaction/o$b$1;-><init>(Lcom/swedbank/mobile/data/overview/transaction/o$b;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/transaction/o$b;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
