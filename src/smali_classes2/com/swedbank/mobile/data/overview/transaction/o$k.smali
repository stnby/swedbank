.class final Lcom/swedbank/mobile/data/overview/transaction/o$k;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/transaction/o;->a(Ljava/lang/String;Ljava/util/List;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "[",
        "Ljava/lang/Object;",
        "TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/transaction/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/o$k;->a:Lcom/swedbank/mobile/data/overview/transaction/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/p;
    .locals 12
    .param p1    # [Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "responses"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 270
    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/data/overview/transaction/a;

    .line 271
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 272
    array-length v3, p1

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_2

    aget-object v6, p1, v5

    if-eqz v6, :cond_1

    .line 273
    check-cast v6, Lcom/swedbank/mobile/data/overview/transaction/a;

    if-nez v1, :cond_0

    .line 274
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/a;->c()Lcom/swedbank/mobile/data/network/w;

    move-result-object v7

    instance-of v7, v7, Lcom/swedbank/mobile/data/network/w$c;

    if-nez v7, :cond_0

    move-object v1, v6

    goto :goto_1

    .line 278
    :cond_0
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/a;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 273
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.data.overview.transaction.AccountTransactionsResponse"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    const/16 p1, 0xa

    if-nez v1, :cond_15

    .line 281
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/o$k;->a:Lcom/swedbank/mobile/data/overview/transaction/o;

    .line 502
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v3, "SqliteMagic.newTransaction()"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 516
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    const/16 v5, 0xf

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 527
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 529
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/data/overview/transaction/a;

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/a;->c()Lcom/swedbank/mobile/data/network/w;

    move-result-object v7

    if-eqz v7, :cond_3

    check-cast v7, Lcom/swedbank/mobile/data/network/w$c;

    .line 530
    invoke-virtual {v7}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v7

    check-cast v7, Ljava/lang/Iterable;

    .line 531
    move-object v8, v3

    check-cast v8, Ljava/util/Collection;

    invoke-static {v8, v7}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_2

    .line 529
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.data.network.NetworkResponse.Success<com.swedbank.mobile.data.overview.transaction.TransactionListResponse>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 533
    :cond_4
    move-object v6, v3

    check-cast v6, Ljava/util/Collection;

    .line 534
    sget-object v6, Lkotlin/s;->a:Lkotlin/s;

    .line 523
    move-object v6, v3

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    const/4 v7, 0x1

    xor-int/2addr v6, v7

    if-eqz v6, :cond_5

    goto :goto_3

    :cond_5
    move-object v3, v0

    :goto_3
    if-eqz v3, :cond_d

    .line 537
    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v3

    .line 544
    invoke-static {}, Lcom/swedbank/mobile/data/overview/transaction/p;->a()Ljava/util/Comparator;

    move-result-object v6

    invoke-static {v3, v6}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object v3

    .line 543
    invoke-static {v3, v5}, Lkotlin/i/f;->a(Lkotlin/i/e;I)Lkotlin/i/e;

    move-result-object v3

    .line 545
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v5, Ljava/util/Map;

    .line 546
    invoke-interface {v3}, Lkotlin/i/e;->a()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 547
    move-object v8, v6

    check-cast v8, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 542
    invoke-virtual {v8}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->c()Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    move-result-object v8

    invoke-virtual {v8}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->a()Ljava/lang/String;

    move-result-object v8

    .line 549
    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_6

    .line 548
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 552
    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    :cond_6
    check-cast v9, Ljava/util/List;

    .line 556
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 559
    :cond_7
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Lkotlin/a/x;->a(I)I

    move-result v6

    invoke-direct {v3, v6}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v3, Ljava/util/Map;

    .line 560
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 561
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 562
    move-object v8, v6

    check-cast v8, Ljava/util/Map$Entry;

    .line 560
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 563
    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_b

    check-cast v9, Lcom/swedbank/mobile/data/overview/transaction/a;

    .line 564
    invoke-virtual {v9}, Lcom/swedbank/mobile/data/overview/transaction/a;->b()Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-nez v10, :cond_8

    const/4 v10, 0x1

    goto :goto_6

    :cond_8
    const/4 v10, 0x0

    .line 565
    :goto_6
    invoke-virtual {v9}, Lcom/swedbank/mobile/data/overview/transaction/a;->c()Lcom/swedbank/mobile/data/network/w;

    move-result-object v9

    instance-of v11, v9, Lcom/swedbank/mobile/data/network/w$c;

    if-nez v11, :cond_9

    move-object v9, v0

    :cond_9
    check-cast v9, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v9, :cond_a

    .line 566
    new-instance v11, Lcom/swedbank/mobile/data/overview/transaction/d;

    invoke-virtual {v9}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    invoke-direct {v11, v6, v10, v9}, Lcom/swedbank/mobile/data/overview/transaction/d;-><init>(Ljava/util/List;ZLcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;)V

    invoke-interface {v3, v8, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_a
    const-string p1, "Required value was null."

    .line 565
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_b
    const-string p1, "Required value was null."

    .line 563
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_c
    move-object v0, v3

    :cond_d
    if-eqz v0, :cond_14

    .line 570
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/overview/transaction/d;

    .line 571
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/d;->a()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/d;->b()Z

    move-result v5

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/d;->c()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    move-result-object v2

    if-eqz v5, :cond_10

    .line 576
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v5

    .line 577
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v2

    .line 580
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v6, v5, :cond_e

    goto :goto_8

    .line 582
    :cond_e
    new-instance v5, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    .line 583
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 584
    invoke-static {v4}, Lkotlin/a/h;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->d()Ljava/lang/String;

    move-result-object v6

    .line 582
    invoke-direct {v5, v2, v6, v7}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v2, v5

    .line 598
    :goto_8
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v5

    const-string v6, "SqliteMagic.newTransaction()"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 601
    :try_start_1
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v6

    .line 605
    sget-object v8, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    const-string v9, "COMBINED_TRANSACTION_DATA"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v6, v8}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v6

    .line 606
    sget-object v8, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/y;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v9, "COMBINED_TRANSACTION_DATA.ACCOUNT_ID"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 607
    invoke-virtual {v8, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v8

    const-string v9, "this.`is`(value)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 608
    invoke-virtual {v6, v8}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v6

    .line 604
    invoke-virtual {v6}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 609
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v6

    .line 613
    sget-object v8, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v9, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v6, v8}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v6

    .line 614
    sget-object v8, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/aa;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v9, "COMBINED_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 615
    invoke-virtual {v8, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v8

    const-string v9, "this.`is`(value)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 616
    invoke-virtual {v6, v8}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v6

    .line 612
    invoke-virtual {v6}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 617
    sget-object v6, Lcom/siimkinks/sqlitemagic/z;->a:Lcom/siimkinks/sqlitemagic/z;

    .line 620
    check-cast v4, Ljava/lang/Iterable;

    .line 621
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v4, p1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 622
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 623
    check-cast v8, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 620
    invoke-virtual {v8}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b()Lcom/swedbank/mobile/data/overview/transaction/b;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 624
    :cond_f
    check-cast v6, Ljava/util/List;

    check-cast v6, Ljava/lang/Iterable;

    .line 625
    invoke-static {v6}, Lcom/siimkinks/sqlitemagic/cv$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cv$a;

    move-result-object v4

    .line 619
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/cv$a;->a()Z

    .line 631
    invoke-virtual {v2, v3}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b(Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/c;

    move-result-object v2

    .line 632
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/cw$a;->a(Lcom/swedbank/mobile/data/overview/transaction/c;)Lcom/siimkinks/sqlitemagic/cw$a;

    move-result-object v2

    .line 629
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/cw$a;->a()J

    .line 634
    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636
    :try_start_2
    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    goto/16 :goto_7

    :catchall_0
    move-exception p1

    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw p1

    .line 640
    :cond_10
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v5

    .line 641
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v2

    .line 643
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v6, v5, :cond_11

    invoke-virtual {v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c()Z

    move-result v2

    goto :goto_a

    :cond_11
    const/4 v2, 0x1

    .line 649
    :goto_a
    invoke-static {v4}, Lkotlin/a/h;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {v5}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->d()Ljava/lang/String;

    move-result-object v5

    .line 652
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v6

    const-string v8, "SqliteMagic.newTransaction()"

    invoke-static {v6, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 655
    :try_start_3
    sget-object v8, Lcom/siimkinks/sqlitemagic/z;->a:Lcom/siimkinks/sqlitemagic/z;

    .line 658
    check-cast v4, Ljava/lang/Iterable;

    .line 659
    new-instance v8, Ljava/util/ArrayList;

    invoke-static {v4, p1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v8, Ljava/util/Collection;

    .line 660
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_12

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 661
    check-cast v9, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 658
    invoke-virtual {v9}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b()Lcom/swedbank/mobile/data/overview/transaction/b;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 662
    :cond_12
    check-cast v8, Ljava/util/List;

    check-cast v8, Ljava/lang/Iterable;

    .line 663
    invoke-static {v8}, Lcom/siimkinks/sqlitemagic/cv$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cv$a;

    move-result-object v4

    .line 657
    invoke-virtual {v4}, Lcom/siimkinks/sqlitemagic/cv$a;->a()Z

    .line 664
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v4

    .line 670
    sget-object v8, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v9, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v4, v8}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v4

    .line 671
    sget-object v8, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/aa;->e:Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v8, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v5

    .line 672
    invoke-virtual {v5}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v5}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v4

    const-string v5, "UPDATE\n        TABLE COM\u2026.END_CURSOR to endCursor)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 673
    sget-object v5, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/aa;->f:Lcom/siimkinks/sqlitemagic/n;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v5, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    .line 674
    invoke-virtual {v2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v2

    const-string v4, "UPDATE\n        TABLE COM\u2026NEXT_PAGE to hasNextPage)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 675
    sget-object v4, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/aa;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "COMBINED_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 676
    invoke-virtual {v4, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 677
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v2

    .line 669
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 679
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 681
    :try_start_4
    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    goto/16 :goto_7

    :catchall_1
    move-exception p1

    invoke-interface {v6}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw p1

    .line 686
    :cond_13
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    .line 688
    :cond_14
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 690
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 284
    sget-object p1, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    goto :goto_e

    :catchall_2
    move-exception p1

    .line 690
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw p1

    .line 286
    :cond_15
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/a;->c()Lcom/swedbank/mobile/data/network/w;

    move-result-object v0

    .line 694
    instance-of v1, v0, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v1, :cond_17

    check-cast v0, Lcom/swedbank/mobile/data/network/w$b;

    .line 695
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 696
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result p1

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 697
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 698
    check-cast v0, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 695
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 699
    :cond_16
    check-cast v1, Ljava/util/List;

    .line 694
    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    .line 286
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_d

    .line 700
    :cond_17
    instance-of p1, v0, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz p1, :cond_18

    check-cast v0, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    .line 286
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 702
    :goto_d
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    :goto_e
    return-object p1

    .line 701
    :cond_18
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/transaction/o$k;->a([Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/p;

    move-result-object p1

    return-object p1
.end method
