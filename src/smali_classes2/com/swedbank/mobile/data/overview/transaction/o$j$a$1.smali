.class public final Lcom/swedbank/mobile/data/overview/transaction/o$j$a$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/transaction/o$j$a;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/transaction/o$j$a;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/o$j$a;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/o$j$a$1;->a:Lcom/swedbank/mobile/data/overview/transaction/o$j$a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/transaction/o$j$a$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/o$j$a$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    .line 236
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 238
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    :try_start_0
    sget-object v2, Lcom/siimkinks/sqlitemagic/ci;->a:Lcom/siimkinks/sqlitemagic/ci;

    .line 244
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 245
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 246
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 247
    check-cast v4, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 244
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a()Lcom/swedbank/mobile/data/overview/transaction/e;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :cond_0
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 249
    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/dc$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/dc$a;

    move-result-object v2

    .line 243
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/dc$a;->a()Z

    .line 250
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v2

    .line 256
    sget-object v3, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v4, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v2

    .line 257
    sget-object v3, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/cj;->e:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v3

    .line 258
    invoke-virtual {v3}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v3}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v2

    const-string v3, "UPDATE\n                 \u2026OR to pageInfo.endCursor)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    sget-object v3, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/cj;->f:Lcom/siimkinks/sqlitemagic/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    const-string v2, "UPDATE\n                 \u2026 to pageInfo.hasNextPage)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/cj;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/swedbank/mobile/data/overview/transaction/o$j$a$1;->a:Lcom/swedbank/mobile/data/overview/transaction/o$j$a;

    iget-object v3, v3, Lcom/swedbank/mobile/data/overview/transaction/o$j$a;->a:Lcom/swedbank/mobile/data/overview/transaction/o$j;

    iget-object v3, v3, Lcom/swedbank/mobile/data/overview/transaction/o$j;->b:Ljava/lang/String;

    .line 262
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 265
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0

    .line 81
    :cond_1
    :goto_1
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/o$j$a$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
