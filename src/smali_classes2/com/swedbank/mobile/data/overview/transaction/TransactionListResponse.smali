.class public final Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;
.super Ljava/lang/Object;
.source "TransactionListResponse.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/d;
    a = "transactions"
.end annotation

.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse$a;
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field public static final a:Ljava/lang/String; = "\nfragment transactionData on TransactionConnection {\n  edges {\n    node {\n      accountId\n      transactionDate\n      transactionBankDate\n      transactionNumber\n      transactionPart\n      amount\n      currency\n      counterPartyName\n      counterPartyAccount\n      description\n      transactionType\n      paymentType\n      direction\n      numberOfTransactions\n    }\n    cursor\n  }\n  pageInfo {\n    startCursor\n    endCursor\n    hasNextPage\n  }\n}"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse$a;


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b:Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse$a;

    const-string v0, "\nfragment transactionData on TransactionConnection {\n  edges {\n    node {\n      accountId\n      transactionDate\n      transactionBankDate\n      transactionNumber\n      transactionPart\n      amount\n      currency\n      counterPartyName\n      counterPartyAccount\n      description\n      transactionType\n      paymentType\n      direction\n      numberOfTransactions\n    }\n    cursor\n  }\n  pageInfo {\n    startCursor\n    endCursor\n    hasNextPage\n  }\n}"

    .line 20
    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "edges"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;",
            ">;",
            "Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;",
            ")V"
        }
    .end annotation

    const-string v0, "transactions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageInfo"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->c:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->d:Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    .line 15
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;-><init>(Ljava/util/List;Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->c:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->d:Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    return-object v0
.end method
