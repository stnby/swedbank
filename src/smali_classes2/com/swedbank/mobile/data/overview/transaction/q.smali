.class public final Lcom/swedbank/mobile/data/overview/transaction/q;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/overview/transaction/o;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/r;",
            ">;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/q;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/overview/transaction/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/r;",
            ">;)",
            "Lcom/swedbank/mobile/data/overview/transaction/q;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/q;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/overview/transaction/q;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/overview/transaction/o;
    .locals 2

    .line 17
    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/o;

    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/transaction/q;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/overview/transaction/r;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/overview/transaction/o;-><init>(Lcom/swedbank/mobile/data/overview/transaction/r;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/q;->a()Lcom/swedbank/mobile/data/overview/transaction/o;

    move-result-object v0

    return-object v0
.end method
