.class final Lcom/swedbank/mobile/data/overview/transaction/o$a;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/transaction/o;->b()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/overview/transaction/o$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/o$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/overview/transaction/o$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/o$a;->a:Lcom/swedbank/mobile/data/overview/transaction/o$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .line 501
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 123
    sget-object v2, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    const-string v3, "SINGLE_TRANSACTION_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 124
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$a;->a()I

    .line 125
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 126
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 127
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$a;->a()I

    .line 504
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 506
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/o$a;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
