.class public final Lcom/swedbank/mobile/data/overview/transaction/n;
.super Ljava/lang/Object;
.source "TransactionData.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/overview/transaction/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/overview/transaction/n$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ae<",
            "Lcom/swedbank/mobile/data/overview/transaction/e;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/overview/transaction/n$a;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/util/Date;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/util/Date;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:J

.field private final h:I

.field private final i:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final k:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final l:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final m:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final n:Lcom/swedbank/mobile/business/overview/Transaction$Type;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final p:Lcom/swedbank/mobile/business/overview/Transaction$Direction;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/n$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/overview/transaction/n$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/n;->b:Lcom/swedbank/mobile/data/overview/transaction/n$a;

    .line 145
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    const/16 v1, 0xf

    .line 147
    new-array v2, v1, [Lcom/siimkinks/sqlitemagic/x;

    .line 148
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "remote_cursor"

    .line 225
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_e

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 149
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "account_id"

    .line 226
    invoke-virtual {v3, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_d

    const/4 v5, 0x1

    aput-object v3, v2, v5

    .line 150
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->e:Lcom/siimkinks/sqlitemagic/ap;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v6, "transaction_date"

    .line 227
    invoke-virtual {v3, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_c

    const/4 v6, 0x2

    aput-object v3, v2, v6

    .line 151
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->f:Lcom/siimkinks/sqlitemagic/ap;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v7, "transaction_bank_date"

    .line 228
    invoke-virtual {v3, v7}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_b

    const/4 v7, 0x3

    aput-object v3, v2, v7

    .line 152
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->g:Lcom/siimkinks/sqlitemagic/bn;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v8, "transaction_number"

    .line 229
    invoke-virtual {v3, v8}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_a

    const/4 v8, 0x4

    aput-object v3, v2, v8

    .line 153
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->h:Lcom/siimkinks/sqlitemagic/bn;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v9, "transaction_part"

    .line 230
    invoke-virtual {v3, v9}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_9

    const/4 v9, 0x5

    aput-object v3, v2, v9

    .line 154
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->i:Lcom/siimkinks/sqlitemagic/x;

    const-string v10, "amount"

    .line 231
    invoke-virtual {v3, v10}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_8

    const/4 v10, 0x6

    aput-object v3, v2, v10

    .line 155
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->j:Lcom/siimkinks/sqlitemagic/x;

    const-string v11, "currency"

    .line 232
    invoke-virtual {v3, v11}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_7

    const/4 v11, 0x7

    aput-object v3, v2, v11

    .line 156
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->k:Lcom/siimkinks/sqlitemagic/x;

    const-string v12, "counterparty"

    .line 233
    invoke-virtual {v3, v12}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_6

    const/16 v12, 0x8

    aput-object v3, v2, v12

    .line 157
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->l:Lcom/siimkinks/sqlitemagic/x;

    const-string v13, "counterparty_account"

    .line 234
    invoke-virtual {v3, v13}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_5

    const/16 v13, 0x9

    aput-object v3, v2, v13

    .line 158
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->m:Lcom/siimkinks/sqlitemagic/x;

    const-string v14, "description"

    .line 235
    invoke-virtual {v3, v14}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_4

    const/16 v14, 0xa

    aput-object v3, v2, v14

    .line 159
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->n:Lcom/siimkinks/sqlitemagic/dq;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v15, "type"

    .line 236
    invoke-virtual {v3, v15}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_3

    const/16 v15, 0xb

    aput-object v3, v2, v15

    .line 160
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->o:Lcom/siimkinks/sqlitemagic/dp;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v15, "payment_type"

    .line 237
    invoke-virtual {v3, v15}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_2

    const/16 v15, 0xc

    aput-object v3, v2, v15

    .line 161
    sget-object v3, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/ch;->p:Lcom/siimkinks/sqlitemagic/do;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v15, "direction"

    .line 238
    invoke-virtual {v3, v15}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_1

    const/16 v15, 0xd

    aput-object v3, v2, v15

    const/16 v3, 0xe

    .line 162
    sget-object v15, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v15, v15, Lcom/siimkinks/sqlitemagic/ch;->q:Lcom/siimkinks/sqlitemagic/bn;

    check-cast v15, Lcom/siimkinks/sqlitemagic/x;

    const-string v14, "number_of_transactions"

    .line 239
    invoke-virtual {v15, v14}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v14

    if-eqz v14, :cond_0

    aput-object v14, v2, v3

    .line 146
    invoke-static {v0, v2}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;

    move-result-object v0

    .line 163
    sget-object v2, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    const-string v3, "SINGLE_TRANSACTION_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    .line 240
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v2, "SELECT\n        COLUMNS\n \u2026M SINGLE_TRANSACTION_DATA"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/siimkinks/sqlitemagic/cd$a;

    .line 164
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v2

    .line 166
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/x;

    .line 167
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v4

    .line 168
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->d:Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v5

    .line 169
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->e:Lcom/siimkinks/sqlitemagic/ap;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v6

    .line 170
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->f:Lcom/siimkinks/sqlitemagic/ap;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v7

    .line 171
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->g:Lcom/siimkinks/sqlitemagic/bn;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v8

    .line 172
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->h:Lcom/siimkinks/sqlitemagic/bn;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v9

    .line 173
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->i:Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v10

    .line 174
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->j:Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v11

    .line 175
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->k:Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v12

    .line 176
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->l:Lcom/siimkinks/sqlitemagic/x;

    aput-object v3, v1, v13

    .line 177
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->m:Lcom/siimkinks/sqlitemagic/x;

    const/16 v4, 0xa

    aput-object v3, v1, v4

    .line 178
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->n:Lcom/siimkinks/sqlitemagic/dq;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const/16 v4, 0xb

    aput-object v3, v1, v4

    .line 179
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->o:Lcom/siimkinks/sqlitemagic/dp;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const/16 v4, 0xc

    aput-object v3, v1, v4

    .line 180
    sget-object v3, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/y;->p:Lcom/siimkinks/sqlitemagic/do;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const/16 v4, 0xd

    aput-object v3, v1, v4

    const/16 v3, 0xe

    .line 181
    sget-object v4, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/y;->q:Lcom/siimkinks/sqlitemagic/bn;

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    aput-object v4, v1, v3

    .line 165
    invoke-static {v2, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;

    move-result-object v1

    .line 182
    sget-object v2, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    const-string v3, "COMBINED_TRANSACTION_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    .line 241
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v1

    const-string v2, "SELECT\n        COLUMNS\n \u2026COMBINED_TRANSACTION_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/cd$a;

    .line 242
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cd$a;->a(Lcom/siimkinks/sqlitemagic/cd$a;)Lcom/siimkinks/sqlitemagic/cb$b;

    move-result-object v0

    const-string v1, "union(select)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$b;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    const-string v1, "(SELECT\n        COLUMNS\n\u2026DATA))\n        .compile()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/data/overview/transaction/n;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void

    .line 239
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_5
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_6
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_7
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_8
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_9
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_a
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_b
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_c
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_d
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_e
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V
    .locals 14
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Date;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/Date;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p13    # Lcom/swedbank/mobile/business/overview/Transaction$Type;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p14    # Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p15    # Lcom/swedbank/mobile/business/overview/Transaction$Direction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    move-object/from16 v11, p14

    move-object/from16 v12, p15

    const-string v13, "remoteCursor"

    invoke-static {p1, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "accountId"

    invoke-static {v2, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "transactionDate"

    invoke-static {v3, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "transactionBankDate"

    invoke-static {v4, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "amount"

    invoke-static {v5, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "currency"

    invoke-static {v6, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "counterparty"

    invoke-static {v7, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "counterpartyAccount"

    invoke-static {v8, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "description"

    invoke-static {v9, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "type"

    invoke-static {v10, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "paymentType"

    invoke-static {v11, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "direction"

    invoke-static {v12, v13}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->c:Ljava/lang/String;

    iput-object v2, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->d:Ljava/lang/String;

    iput-object v3, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->e:Ljava/util/Date;

    iput-object v4, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->f:Ljava/util/Date;

    move-wide/from16 v1, p5

    iput-wide v1, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->g:J

    move/from16 v1, p7

    iput v1, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->h:I

    iput-object v5, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->i:Ljava/lang/String;

    iput-object v6, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->j:Ljava/lang/String;

    iput-object v7, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->k:Ljava/lang/String;

    iput-object v8, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->l:Ljava/lang/String;

    iput-object v9, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->m:Ljava/lang/String;

    iput-object v10, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->n:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    iput-object v11, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->o:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    iput-object v12, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->p:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move/from16 v1, p16

    iput v1, v0, Lcom/swedbank/mobile/data/overview/transaction/n;->q:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/Date;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->e:Ljava/util/Date;

    return-object v0
.end method

.method public d()Ljava/util/Date;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->f:Ljava/util/Date;

    return-object v0
.end method

.method public e()J
    .locals 2

    .line 130
    iget-wide v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->g:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_4

    instance-of v1, p1, Lcom/swedbank/mobile/data/overview/transaction/n;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    check-cast p1, Lcom/swedbank/mobile/data/overview/transaction/n;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->c()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->c()Ljava/util/Date;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->d()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->d()Ljava/util/Date;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->e()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->e()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->f()I

    move-result v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->f()I

    move-result v3

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->i()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->l()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->l()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->o()I

    move-result v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/n;->o()I

    move-result p1

    if-ne v1, p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    return v2

    :cond_4
    :goto_3
    return v0
.end method

.method public f()I
    .locals 1

    .line 131
    iget v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->h:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 133
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->j:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->c()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->d()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->e()J

    move-result-wide v2

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->f()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->h()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->i()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->l()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_a
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->o()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->k:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->l:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->m:Ljava/lang/String;

    return-object v0
.end method

.method public l()Lcom/swedbank/mobile/business/overview/Transaction$Type;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->n:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    return-object v0
.end method

.method public m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->o:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    return-object v0
.end method

.method public n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->p:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    return-object v0
.end method

.method public o()I
    .locals 1

    .line 140
    iget v0, p0, Lcom/swedbank/mobile/data/overview/transaction/n;->q:I

    return v0
.end method

.method public p()Lcom/swedbank/mobile/business/overview/Transaction;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 125
    invoke-static {p0}, Lcom/swedbank/mobile/data/overview/transaction/l$a;->a(Lcom/swedbank/mobile/data/overview/transaction/l;)Lcom/swedbank/mobile/business/overview/Transaction;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransactionDataView(remoteCursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->c()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionBankDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->d()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", transactionPart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", counterparty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", counterpartyAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->l()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->m()Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", direction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->n()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfTransactions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/transaction/n;->o()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
