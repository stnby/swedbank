.class public final Lcom/swedbank/mobile/data/overview/transaction/o;
.super Ljava/lang/Object;
.source "TransactionRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/a/a;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/overview/transaction/r;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/r;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/overview/transaction/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transactionService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/transaction/o;->a:Lcom/swedbank/mobile/data/overview/transaction/r;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/overview/transaction/o;)Lcom/swedbank/mobile/data/overview/transaction/r;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/data/overview/transaction/o;->a:Lcom/swedbank/mobile/data/overview/transaction/r;

    return-object p0
.end method

.method private final e(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 108
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 109
    sget-object v1, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/aa;->f:Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "COMBINED_TRANSACTION_QUERY_METADATA.HAS_NEXT_PAGE"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 110
    sget-object v1, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v2, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 514
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026RANSACTION_QUERY_METADATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    sget-object v1, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/aa;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "COMBINED_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 515
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 516
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 113
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 114
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/o$c;->a:Lcom/swedbank/mobile/data/overview/transaction/o$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          COLUMN\u2026oObservable()\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 63
    sget-object v1, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    const-string v2, "COMBINED_TRANSACTION_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const/4 v1, 0x3

    .line 65
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/cb$f;

    .line 66
    sget-object v2, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/y;->f:Lcom/siimkinks/sqlitemagic/ap;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/ap;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v3, "COMBINED_TRANSACTION_DAT\u2026NSACTION_BANK_DATE.desc()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 67
    sget-object v2, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/y;->g:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bn;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v3, "COMBINED_TRANSACTION_DAT\u2026TRANSACTION_NUMBER.desc()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 68
    sget-object v2, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/y;->h:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bn;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v3, "COMBINED_TRANSACTION_DATA.TRANSACTION_PART.asc()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 504
    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/siimkinks/sqlitemagic/cb$f;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$e;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object v0

    .line 71
    sget-object v1, Lcom/swedbank/mobile/data/overview/transaction/o$h;->a:Lcom/swedbank/mobile/data/overview/transaction/o$h;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM C\u2026ionData::toTransaction) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    const-string v2, "SINGLE_TRANSACTION_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 51
    sget-object v1, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ch;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "SINGLE_TRANSACTION_DATA.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 501
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    const-string v0, "SELECT\n          FROM SI\u2026.ACCOUNT_ID IS accountId)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    .line 53
    new-array v0, v0, [Lcom/siimkinks/sqlitemagic/cb$f;

    .line 54
    sget-object v1, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ch;->f:Lcom/siimkinks/sqlitemagic/ap;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/ap;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "SINGLE_TRANSACTION_DATA.\u2026NSACTION_BANK_DATE.desc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 55
    sget-object v1, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ch;->g:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/bn;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "SINGLE_TRANSACTION_DATA.TRANSACTION_NUMBER.desc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 56
    sget-object v1, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ch;->h:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/bn;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "SINGLE_TRANSACTION_DATA.TRANSACTION_PART.asc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 503
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/siimkinks/sqlitemagic/cb$f;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cb$h;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$e;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 59
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/o$g;->a:Lcom/swedbank/mobile/data/overview/transaction/o$g;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM S\u2026ionData::toTransaction) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/util/List;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    check-cast p1, Ljava/lang/Iterable;

    .line 510
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 511
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 512
    check-cast v1, Ljava/lang/String;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/data/overview/transaction/o;

    .line 100
    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/overview/transaction/o;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 513
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 102
    check-cast v0, Ljava/lang/Iterable;

    sget-object p1, Lcom/swedbank/mobile/data/overview/transaction/o$e;->a:Lcom/swedbank/mobile/data/overview/transaction/o$e;

    check-cast p1, Lio/reactivex/c/h;

    invoke-static {v0, p1}, Lio/reactivex/o;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.combineLatest\u2026it as Boolean }\n        }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountIds\n      .map(::\u2026olean }\n        }\n      }"

    .line 101
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    sget-object v0, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cj;->e:Lcom/siimkinks/sqlitemagic/x;

    const-string v1, "SINGLE_TRANSACTION_QUERY_METADATA.END_CURSOR"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, ""

    .line 590
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v2

    .line 599
    invoke-static {v2, v0}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 600
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    .line 601
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v2, "SELECT\n          COLUMN \u2026RANSACTION_QUERY_METADATA"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 602
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/cj;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 603
    invoke-virtual {v2, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 604
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 598
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 597
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 596
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cg;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "(SELECT\n          COLUMN\u2026ceOrDefault(defaultValue)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    new-instance v1, Lcom/swedbank/mobile/data/overview/transaction/o$j;

    invoke-direct {v1, p0, p1, p2}, Lcom/swedbank/mobile/data/overview/transaction/o$j;-><init>(Lcom/swedbank/mobile/data/overview/transaction/o;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "getTransactionQueryMetad\u2026      }\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)Lio/reactivex/w;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountIds"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    check-cast p2, Ljava/lang/Iterable;

    .line 605
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 606
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 607
    check-cast v1, Ljava/lang/String;

    .line 611
    sget-object v2, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/aa;->e:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "COMBINED_TRANSACTION_QUERY_METADATA.END_CURSOR"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, ""

    .line 614
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v4

    .line 623
    invoke-static {v4, v2}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v2

    .line 624
    sget-object v4, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v5, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    .line 625
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v2

    const-string v4, "SELECT\n          COLUMN \u2026RANSACTION_QUERY_METADATA"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 626
    sget-object v4, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/aa;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "COMBINED_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 627
    invoke-virtual {v4, v1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.`is`(value)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 628
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v2

    .line 622
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v2

    .line 621
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v2

    .line 620
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/cg;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v2

    const-string v3, "(SELECT\n          COLUMN\u2026ceOrDefault(defaultValue)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 610
    new-instance v3, Lcom/swedbank/mobile/data/overview/transaction/o$b;

    invoke-direct {v3, p0, v1, p1}, Lcom/swedbank/mobile/data/overview/transaction/o$b;-><init>(Lcom/swedbank/mobile/data/overview/transaction/o;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v1

    const-string v2, "getCombinedTransactionQu\u2026 endCursor, it) }\n      }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 609
    invoke-static {v1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v1

    .line 266
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 629
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 269
    check-cast v0, Ljava/lang/Iterable;

    new-instance p1, Lcom/swedbank/mobile/data/overview/transaction/o$k;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/overview/transaction/o$k;-><init>(Lcom/swedbank/mobile/data/overview/transaction/o;)V

    check-cast p1, Lio/reactivex/c/h;

    invoke-static {v0, p1}, Lio/reactivex/w;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "Single.zip(queries) { re\u2026or)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "accountIds\n      .map { \u2026      }\n        }\n      }"

    .line 268
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Ljava/lang/String;Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionListResponse"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 526
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 152
    sget-object v2, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    const-string v3, "SINGLE_TRANSACTION_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 153
    sget-object v2, Lcom/siimkinks/sqlitemagic/ch;->a:Lcom/siimkinks/sqlitemagic/ch;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/ch;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "SINGLE_TRANSACTION_DATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 529
    invoke-virtual {v2, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 530
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v1

    .line 154
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 155
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 156
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 157
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/cj;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 531
    invoke-virtual {v2, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 532
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v1

    .line 158
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 160
    sget-object v1, Lcom/siimkinks/sqlitemagic/ci;->a:Lcom/siimkinks/sqlitemagic/ci;

    .line 161
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 533
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 534
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 535
    check-cast v3, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 161
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a()Lcom/swedbank/mobile/data/overview/transaction/e;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 536
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 537
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/dc$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/dc$a;

    move-result-object v1

    .line 162
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/dc$a;->a()Z

    .line 163
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object p2

    .line 164
    invoke-virtual {p2, p1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a(Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/f;

    move-result-object p1

    .line 538
    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/dd$a;->a(Lcom/swedbank/mobile/data/overview/transaction/f;)Lcom/siimkinks/sqlitemagic/dd$a;

    move-result-object p1

    .line 166
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dd$a;->a()J

    .line 539
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    :catchall_0
    move-exception p1

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw p1
.end method

.method public final a(Ljava/util/Map;)V
    .locals 10
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "responses"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 645
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Lkotlin/a/x;->a(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 646
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 647
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 648
    check-cast v1, Ljava/util/Map$Entry;

    .line 646
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 322
    new-instance v3, Lcom/swedbank/mobile/data/overview/transaction/a;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, ""

    new-instance v6, Lcom/swedbank/mobile/data/network/w$c;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v6, v1}, Lcom/swedbank/mobile/data/network/w$c;-><init>(Ljava/lang/Object;)V

    check-cast v6, Lcom/swedbank/mobile/data/network/w;

    invoke-direct {v3, v4, v5, v6}, Lcom/swedbank/mobile/data/overview/transaction/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/network/w;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 652
    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object p1

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 656
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    sget-object v2, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    const-string v3, "COMBINED_TRANSACTION_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$a;->a()I

    .line 657
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    sget-object v2, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v3, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$a;->a()I

    .line 666
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xf

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 677
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 679
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/data/overview/transaction/a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/data/overview/transaction/a;->c()Lcom/swedbank/mobile/data/network/w;

    move-result-object v4

    if-eqz v4, :cond_1

    check-cast v4, Lcom/swedbank/mobile/data/network/w$c;

    .line 680
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    invoke-virtual {v4}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 681
    move-object v5, v1

    check-cast v5, Ljava/util/Collection;

    invoke-static {v5, v4}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 679
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.swedbank.mobile.data.network.NetworkResponse.Success<com.swedbank.mobile.data.overview.transaction.TransactionListResponse>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 683
    :cond_2
    move-object v3, v1

    check-cast v3, Ljava/util/Collection;

    .line 684
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 673
    move-object v3, v1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    const/4 v5, 0x0

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    move-object v1, v5

    :goto_2
    if-eqz v1, :cond_a

    .line 687
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v1

    .line 694
    invoke-static {}, Lcom/swedbank/mobile/data/overview/transaction/p;->a()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object v1

    .line 693
    invoke-static {v1, v2}, Lkotlin/i/f;->a(Lkotlin/i/e;I)Lkotlin/i/e;

    move-result-object v1

    .line 695
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 696
    invoke-interface {v1}, Lkotlin/i/e;->a()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 697
    move-object v6, v3

    check-cast v6, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 692
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->c()Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/TransactionResponse;->a()Ljava/lang/String;

    move-result-object v6

    .line 699
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_4

    .line 698
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 702
    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    :cond_4
    check-cast v7, Ljava/util/List;

    .line 706
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 709
    :cond_5
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Lkotlin/a/x;->a(I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 710
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 711
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 712
    move-object v6, v3

    check-cast v6, Ljava/util/Map$Entry;

    .line 710
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 713
    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_9

    check-cast v7, Lcom/swedbank/mobile/data/overview/transaction/a;

    .line 714
    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/a;->b()Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_6

    const/4 v8, 0x1

    goto :goto_5

    :cond_6
    const/4 v8, 0x0

    .line 715
    :goto_5
    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/a;->c()Lcom/swedbank/mobile/data/network/w;

    move-result-object v7

    instance-of v9, v7, Lcom/swedbank/mobile/data/network/w$c;

    if-nez v9, :cond_7

    move-object v7, v5

    :cond_7
    check-cast v7, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v7, :cond_8

    .line 716
    new-instance v9, Lcom/swedbank/mobile/data/overview/transaction/d;

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    invoke-direct {v9, v3, v8, v7}, Lcom/swedbank/mobile/data/overview/transaction/d;-><init>(Ljava/util/List;ZLcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;)V

    invoke-interface {v1, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_8
    const-string v0, "Required value was null."

    .line 715
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_9
    const-string v0, "Required value was null."

    .line 713
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_a
    move-object v1, v5

    :cond_b
    if-eqz v1, :cond_12

    .line 720
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/overview/transaction/d;

    .line 721
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/d;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/d;->b()Z

    move-result v5

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/d;->c()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    move-result-object v1

    const/16 v6, 0xa

    if-eqz v5, :cond_e

    .line 726
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v5

    .line 727
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v1

    .line 730
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v7, v5, :cond_c

    goto :goto_7

    .line 732
    :cond_c
    new-instance v5, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    .line 733
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    .line 734
    invoke-static {v3}, Lkotlin/a/h;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->d()Ljava/lang/String;

    move-result-object v7

    .line 732
    invoke-direct {v5, v1, v7, v4}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v1, v5

    .line 748
    :goto_7
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v5

    const-string v7, "SqliteMagic.newTransaction()"

    invoke-static {v5, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 751
    :try_start_1
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v7

    .line 755
    sget-object v8, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    const-string v9, "COMBINED_TRANSACTION_DATA"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v7, v8}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v7

    .line 756
    sget-object v8, Lcom/siimkinks/sqlitemagic/y;->a:Lcom/siimkinks/sqlitemagic/y;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/y;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v9, "COMBINED_TRANSACTION_DATA.ACCOUNT_ID"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 757
    invoke-virtual {v8, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v8

    const-string v9, "this.`is`(value)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 758
    invoke-virtual {v7, v8}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v7

    .line 754
    invoke-virtual {v7}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 759
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v7

    .line 763
    sget-object v8, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v9, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v7, v8}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v7

    .line 764
    sget-object v8, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/aa;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v9, "COMBINED_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 765
    invoke-virtual {v8, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v8

    const-string v9, "this.`is`(value)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 766
    invoke-virtual {v7, v8}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v7

    .line 762
    invoke-virtual {v7}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 767
    sget-object v7, Lcom/siimkinks/sqlitemagic/z;->a:Lcom/siimkinks/sqlitemagic/z;

    .line 770
    check-cast v3, Ljava/lang/Iterable;

    .line 771
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v3, v6}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 772
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 773
    check-cast v6, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 770
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b()Lcom/swedbank/mobile/data/overview/transaction/b;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 774
    :cond_d
    check-cast v7, Ljava/util/List;

    check-cast v7, Ljava/lang/Iterable;

    .line 775
    invoke-static {v7}, Lcom/siimkinks/sqlitemagic/cv$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cv$a;

    move-result-object v3

    .line 769
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/cv$a;->a()Z

    .line 781
    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->b(Ljava/lang/String;)Lcom/swedbank/mobile/data/overview/transaction/c;

    move-result-object v1

    .line 782
    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/cw$a;->a(Lcom/swedbank/mobile/data/overview/transaction/c;)Lcom/siimkinks/sqlitemagic/cw$a;

    move-result-object v1

    .line 779
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/cw$a;->a()J

    .line 784
    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 786
    :try_start_2
    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    goto/16 :goto_6

    :catchall_0
    move-exception v0

    invoke-interface {v5}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0

    .line 790
    :cond_e
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v5

    .line 791
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v1

    .line 793
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v7, v5, :cond_f

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c()Z

    move-result v1

    goto :goto_9

    :cond_f
    const/4 v1, 0x1

    .line 799
    :goto_9
    invoke-static {v3}, Lkotlin/a/h;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {v5}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->d()Ljava/lang/String;

    move-result-object v5

    .line 802
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v7

    const-string v8, "SqliteMagic.newTransaction()"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 805
    :try_start_3
    sget-object v8, Lcom/siimkinks/sqlitemagic/z;->a:Lcom/siimkinks/sqlitemagic/z;

    .line 808
    check-cast v3, Ljava/lang/Iterable;

    .line 809
    new-instance v8, Ljava/util/ArrayList;

    invoke-static {v3, v6}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v8, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v8, Ljava/util/Collection;

    .line 810
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 811
    check-cast v6, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 808
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->b()Lcom/swedbank/mobile/data/overview/transaction/b;

    move-result-object v6

    invoke-interface {v8, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 812
    :cond_10
    check-cast v8, Ljava/util/List;

    check-cast v8, Ljava/lang/Iterable;

    .line 813
    invoke-static {v8}, Lcom/siimkinks/sqlitemagic/cv$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cv$a;

    move-result-object v3

    .line 807
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/cv$a;->a()Z

    .line 814
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v3

    .line 820
    sget-object v6, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    const-string v8, "COMBINED_TRANSACTION_QUERY_METADATA"

    invoke-static {v6, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v6}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v3

    .line 821
    sget-object v6, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v6, v6, Lcom/siimkinks/sqlitemagic/aa;->e:Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v6, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v5

    .line 822
    invoke-virtual {v5}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v5}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v3

    const-string v5, "UPDATE\n        TABLE COM\u2026.END_CURSOR to endCursor)"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 823
    sget-object v5, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/aa;->f:Lcom/siimkinks/sqlitemagic/n;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v5, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 824
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v5, v1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v1

    const-string v3, "UPDATE\n        TABLE COM\u2026NEXT_PAGE to hasNextPage)"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 825
    sget-object v3, Lcom/siimkinks/sqlitemagic/aa;->a:Lcom/siimkinks/sqlitemagic/aa;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/aa;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "COMBINED_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 826
    invoke-virtual {v3, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 827
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v1

    .line 819
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 829
    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 831
    :try_start_4
    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    goto/16 :goto_6

    :catchall_1
    move-exception v0

    invoke-interface {v7}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0

    .line 836
    :cond_11
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 838
    :cond_12
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 840
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    :catchall_2
    move-exception v0

    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public b()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 120
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/o$a;->a:Lcom/swedbank/mobile/data/overview/transaction/o$a;

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromCallable\u2026     .execute()\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/siimkinks/sqlitemagic/dn;->a:Lcom/siimkinks/sqlitemagic/dn;

    const-string v2, "TRANSACTION_DATA_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 76
    sget-object v1, Lcom/siimkinks/sqlitemagic/dn;->a:Lcom/siimkinks/sqlitemagic/dn;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/dn;->b:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "TRANSACTION_DATA_VIEW.REMOTE_CURSOR"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 505
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 78
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 79
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/o$f;->a:Lcom/swedbank/mobile/data/overview/transaction/o$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM T\u2026oObservable()\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/transaction/o;->a:Lcom/swedbank/mobile/data/overview/transaction/r;

    const/16 v1, 0xf

    .line 135
    invoke-static {v0, p1, p2, v1}, Lcom/swedbank/mobile/data/overview/transaction/s;->a(Lcom/swedbank/mobile/data/overview/transaction/r;Ljava/lang/String;Ljava/lang/String;I)Lio/reactivex/w;

    move-result-object p2

    .line 139
    invoke-static {p2}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p2

    .line 517
    new-instance v0, Lcom/swedbank/mobile/data/overview/transaction/o$i;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/overview/transaction/o$i;-><init>(Lcom/swedbank/mobile/data/overview/transaction/o;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p2, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final b(Ljava/lang/String;Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionListResponse"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 574
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    :try_start_0
    sget-object v1, Lcom/siimkinks/sqlitemagic/ci;->a:Lcom/siimkinks/sqlitemagic/ci;

    .line 209
    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 577
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 578
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 579
    check-cast v3, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    .line 209
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a()Lcom/swedbank/mobile/data/overview/transaction/e;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 580
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 581
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/dc$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/dc$a;

    move-result-object v1

    .line 210
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/dc$a;->a()Z

    .line 212
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v1

    .line 213
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v1

    .line 214
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/cj;->d:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {v2, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    .line 582
    invoke-virtual {p2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object p2

    const-string v1, "UPDATE\n            TABLE\u2026 to pageInfo.startCursor)"

    invoke-static {p2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    sget-object v1, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/cj;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "SINGLE_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 583
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 584
    invoke-virtual {p2, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 585
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    goto :goto_1

    :catchall_0
    move-exception p1

    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw p1

    :cond_1
    :goto_1
    return-void
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 88
    sget-object v1, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/cj;->f:Lcom/siimkinks/sqlitemagic/n;

    const-string v2, "SINGLE_TRANSACTION_QUERY_METADATA.HAS_NEXT_PAGE"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 89
    sget-object v1, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v2, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 507
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026RANSACTION_QUERY_METADATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v1, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/cj;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "SINGLE_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 508
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 509
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 92
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 93
    sget-object v0, Lcom/swedbank/mobile/data/overview/transaction/o$d;->a:Lcom/swedbank/mobile/data/overview/transaction/o$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          COLUMN\u2026oObservable()\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final d(Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    sget-object v0, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/cj;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v1, "SINGLE_TRANSACTION_QUERY_METADATA.START_CURSOR"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, ""

    .line 544
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v2

    .line 553
    invoke-static {v2, v0}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 554
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    .line 555
    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v2, "SELECT\n          COLUMN \u2026RANSACTION_QUERY_METADATA"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 556
    sget-object v2, Lcom/siimkinks/sqlitemagic/cj;->a:Lcom/siimkinks/sqlitemagic/cj;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/cj;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "SINGLE_TRANSACTION_QUERY_METADATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    invoke-virtual {v2, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v2, "this.`is`(value)"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 552
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 551
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 550
    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/cg;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "(SELECT\n          COLUMN\u2026ceOrDefault(defaultValue)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
