.class public final Lcom/swedbank/mobile/data/overview/transaction/m;
.super Ljava/lang/Object;
.source "TransactionData.kt"


# direct methods
.method public static final a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Direction;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 195
    invoke-static {}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->values()[Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v1

    .line 225
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 196
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v0, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    .line 197
    :cond_2
    sget-object p0, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-object v0, p0

    :cond_3
    :goto_2
    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/overview/Transaction$Direction;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/swedbank/mobile/business/overview/Transaction$Direction;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    if-eqz p0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->name()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    if-eqz p0, :cond_0

    .line 215
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->name()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/overview/Transaction$Type;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/swedbank/mobile/business/overview/Transaction$Type;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    if-eqz p0, :cond_0

    .line 202
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction$Type;->name()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final b(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Type;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 208
    invoke-static {}, Lcom/swedbank/mobile/business/overview/Transaction$Type;->values()[Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v1

    .line 227
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 209
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/overview/Transaction$Type;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v0, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    .line 210
    :cond_2
    sget-object p0, Lcom/swedbank/mobile/business/overview/Transaction$Type;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-object v0, p0

    :cond_3
    :goto_2
    return-object v0
.end method

.method public static final c(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    .line 221
    invoke-static {}, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->values()[Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-result-object v1

    .line 229
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 222
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v0, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    .line 223
    :cond_2
    sget-object p0, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    move-object v0, p0

    :cond_3
    :goto_2
    return-object v0
.end method
