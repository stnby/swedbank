.class final Lcom/swedbank/mobile/data/overview/e$b;
.super Ljava/lang/Object;
.source "OverviewRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/e;->a(Ljava/lang/String;Ljava/util/List;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "[",
        "Ljava/lang/Object;",
        "TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/overview/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/e$b;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/p;
    .locals 8
    .param p1    # [Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "responses"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 82
    check-cast v0, Lcom/swedbank/mobile/data/network/w;

    .line 83
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 84
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 85
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-object v5, p1, v4

    if-eqz v5, :cond_2

    .line 87
    check-cast v5, Lcom/swedbank/mobile/data/network/w;

    if-nez v0, :cond_0

    .line 88
    instance-of v6, v5, Lcom/swedbank/mobile/data/network/w$c;

    if-nez v6, :cond_0

    move-object v0, v5

    goto :goto_1

    :cond_0
    if-eqz v5, :cond_1

    .line 92
    check-cast v5, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v5}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/data/overview/OverviewResponse;

    .line 93
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/overview/OverviewResponse;->b()Ljava/lang/String;

    move-result-object v6

    .line 94
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/overview/OverviewResponse;->a()Lcom/swedbank/mobile/data/account/AccountResponse;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual {v5}, Lcom/swedbank/mobile/data/overview/OverviewResponse;->i()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    move-result-object v5

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 92
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.data.network.NetworkResponse.Success<com.swedbank.mobile.data.overview.OverviewResponse>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 87
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.data.network.NetworkResponse<com.swedbank.mobile.data.overview.OverviewResponse>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_5

    .line 110
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object p1

    const-string v0, "SqliteMagic.newTransaction()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    :try_start_0
    check-cast v1, Ljava/lang/Iterable;

    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/e$b;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/e;->b(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/account/c;

    move-result-object v0

    .line 113
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/data/account/AccountResponse;

    .line 100
    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/data/account/c;->a(Lcom/swedbank/mobile/data/account/AccountResponse;)V

    goto :goto_2

    .line 101
    :cond_4
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/e$b;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/e;->c(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/overview/transaction/o;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/data/overview/transaction/o;->a(Ljava/util/Map;)V

    .line 115
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 103
    sget-object p1, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    goto :goto_5

    :catchall_0
    move-exception v0

    .line 117
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0

    .line 121
    :cond_5
    instance-of p1, v0, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz p1, :cond_7

    check-cast v0, Lcom/swedbank/mobile/data/network/w$b;

    .line 122
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 124
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 125
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 122
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 126
    :cond_6
    check-cast v0, Ljava/util/List;

    .line 121
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    .line 105
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_4

    .line 127
    :cond_7
    instance-of p1, v0, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz p1, :cond_8

    check-cast v0, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    .line 105
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 129
    :goto_4
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    :goto_5
    return-object p1

    .line 128
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/e$b;->a([Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/p;

    move-result-object p1

    return-object p1
.end method
