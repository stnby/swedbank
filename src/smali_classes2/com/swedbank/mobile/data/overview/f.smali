.class public final Lcom/swedbank/mobile/data/overview/f;
.super Ljava/lang/Object;
.source "OverviewRepositoryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/overview/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/g;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/o;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/f;->a:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/f;->b:Ljavax/inject/Provider;

    .line 21
    iput-object p3, p0, Lcom/swedbank/mobile/data/overview/f;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/overview/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/o;",
            ">;)",
            "Lcom/swedbank/mobile/data/overview/f;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/swedbank/mobile/data/overview/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/data/overview/f;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/overview/e;
    .locals 4

    .line 26
    new-instance v0, Lcom/swedbank/mobile/data/overview/e;

    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/f;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/overview/g;

    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/f;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/account/c;

    iget-object v3, p0, Lcom/swedbank/mobile/data/overview/f;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/data/overview/transaction/o;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/overview/e;-><init>(Lcom/swedbank/mobile/data/overview/g;Lcom/swedbank/mobile/data/account/c;Lcom/swedbank/mobile/data/overview/transaction/o;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/f;->a()Lcom/swedbank/mobile/data/overview/e;

    move-result-object v0

    return-object v0
.end method
