.class public final Lcom/swedbank/mobile/data/overview/a;
.super Ljava/lang/Object;
.source "OverviewDataModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/data/overview/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/swedbank/mobile/data/overview/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/overview/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/overview/a;->a:Lcom/swedbank/mobile/data/overview/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "json_adapters"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x2

    .line 28
    new-array v0, v0, [Lkotlin/k;

    .line 29
    const-class v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 30
    const-class v2, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 31
    sget-object v3, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 29
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 32
    const-class v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 33
    const-class v2, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    invoke-static {v2}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Class;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 34
    sget-object v3, Lcom/swedbank/mobile/business/overview/Transaction$Type;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    check-cast v3, Ljava/lang/Enum;

    invoke-virtual {v2, v3}, Lcom/squareup/moshi/adapters/EnumJsonAdapter;->a(Ljava/lang/Enum;)Lcom/squareup/moshi/adapters/EnumJsonAdapter;

    move-result-object v2

    .line 32
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 28
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
