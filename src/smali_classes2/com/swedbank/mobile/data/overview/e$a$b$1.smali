.class public final Lcom/swedbank/mobile/data/overview/e$a$b$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/e$a$b;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/e$a$b;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/e$a$b;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/e$a$b$1;->a:Lcom/swedbank/mobile/data/overview/e$a$b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/e$a$b$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/e$a$b$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/overview/OverviewResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/e$a$b$1;->a:Lcom/swedbank/mobile/data/overview/e$a$b;

    iget-object v2, v2, Lcom/swedbank/mobile/data/overview/e$a$b;->a:Lcom/swedbank/mobile/data/overview/e$a;

    iget-object v2, v2, Lcom/swedbank/mobile/data/overview/e$a;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-static {v2}, Lcom/swedbank/mobile/data/overview/e;->b(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/account/c;

    move-result-object v2

    .line 241
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/OverviewResponse;->a()Lcom/swedbank/mobile/data/account/AccountResponse;

    move-result-object v3

    .line 240
    invoke-virtual {v2, v3}, Lcom/swedbank/mobile/data/account/c;->a(Lcom/swedbank/mobile/data/account/AccountResponse;)V

    .line 242
    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/e$a$b$1;->a:Lcom/swedbank/mobile/data/overview/e$a$b;

    iget-object v2, v2, Lcom/swedbank/mobile/data/overview/e$a$b;->a:Lcom/swedbank/mobile/data/overview/e$a;

    iget-object v2, v2, Lcom/swedbank/mobile/data/overview/e$a;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-static {v2}, Lcom/swedbank/mobile/data/overview/e;->c(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/overview/transaction/o;

    move-result-object v2

    .line 243
    iget-object v3, p0, Lcom/swedbank/mobile/data/overview/e$a$b$1;->a:Lcom/swedbank/mobile/data/overview/e$a$b;

    iget-object v3, v3, Lcom/swedbank/mobile/data/overview/e$a$b;->a:Lcom/swedbank/mobile/data/overview/e$a;

    iget-object v3, v3, Lcom/swedbank/mobile/data/overview/e$a;->b:Ljava/lang/String;

    .line 244
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/OverviewResponse;->i()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    move-result-object v0

    .line 242
    invoke-virtual {v2, v3, v0}, Lcom/swedbank/mobile/data/overview/transaction/o;->b(Ljava/lang/String;Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;)V

    .line 246
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 248
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/overview/e$a$b$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
