.class public final Lcom/swedbank/mobile/data/overview/search/a/a;
.super Ljava/lang/Object;
.source "OverviewSearchHistoryRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/plugins/search/history/d;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "searchKeyword"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 16
    sget-object v1, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    const-string v2, "OVERVIEW_SEARCH_KEYWORD"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 17
    sget-object v1, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bt;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "OVERVIEW_SEARCH_KEYWORD.KEYWORD"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    .line 41
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object v0

    .line 19
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cg;->c()Lio/reactivex/j;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/swedbank/mobile/data/overview/search/a/a$a;->a:Lcom/swedbank/mobile/data/overview/search/a/a$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/data/overview/search/a/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/overview/search/a/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    .line 22
    new-instance v8, Lcom/swedbank/mobile/data/overview/search/a/d;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    move-object v1, v8

    move-object v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/data/overview/search/a/d;-><init>(Ljava/lang/Long;Ljava/lang/String;ILjava/util/Date;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v8}, Lio/reactivex/j;->c(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    .line 23
    sget-object v0, Lcom/swedbank/mobile/data/overview/search/a/a$b;->a:Lcom/swedbank/mobile/data/overview/search/a/a$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM O\u2026noreElement()\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/bt;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "OVERVIEW_SEARCH_KEYWORD.KEYWORD"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 33
    sget-object v1, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    const-string v2, "OVERVIEW_SEARCH_KEYWORD"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 43
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026M OVERVIEW_SEARCH_KEYWORD"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    .line 34
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/cb$f;

    .line 35
    sget-object v2, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bt;->d:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bn;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v3, "OVERVIEW_SEARCH_KEYWORD.SEARCH_COUNT.desc()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 36
    sget-object v2, Lcom/siimkinks/sqlitemagic/bt;->a:Lcom/siimkinks/sqlitemagic/bt;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/bt;->e:Lcom/siimkinks/sqlitemagic/ap;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/ap;->d()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v3, "OVERVIEW_SEARCH_KEYWORD.LAST_SEARCHED.desc()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 44
    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/siimkinks/sqlitemagic/cb$f;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object v0

    const-string v1, "SELECT\n          COLUMN \u2026ORD.LAST_SEARCHED.desc())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x5

    .line 45
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$e;->a(I)Lcom/siimkinks/sqlitemagic/cb$d;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$d;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          COLUMN\u2026e()\n          .runQuery()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
