.class public final Lcom/swedbank/mobile/data/overview/search/a;
.super Ljava/lang/Object;
.source "OverviewSearchRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/search/e;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/overview/search/c;

.field private final b:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/search/c;Lcom/squareup/moshi/n;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/overview/search/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/moshi/n;
        .annotation runtime Ljavax/inject/Named;
            value = "graphql_json_mapper"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "overviewSearchService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "graphQLJsonMapper"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/search/a;->a:Lcom/swedbank/mobile/data/overview/search/c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/search/a;->b:Lcom/squareup/moshi/n;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/overview/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keyword"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/search/a;->a:Lcom/swedbank/mobile/data/overview/search/c;

    .line 35
    new-instance v5, Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;

    invoke-direct {v5, p2}, Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;-><init>(Ljava/lang/String;)V

    .line 36
    iget-object v6, p0, Lcom/swedbank/mobile/data/overview/search/a;->b:Lcom/squareup/moshi/n;

    const/16 v3, 0xf

    move-object v2, p1

    move-object v4, p3

    .line 31
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/data/overview/search/d;->a(Lcom/swedbank/mobile/data/overview/search/c;Ljava/lang/String;ILjava/lang/String;Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;Lcom/squareup/moshi/n;)Lio/reactivex/w;

    move-result-object p1

    .line 37
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 78
    new-instance p2, Lcom/swedbank/mobile/data/overview/search/a$b;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/data/overview/search/a$b;-><init>(Lcom/swedbank/mobile/data/overview/search/a;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026r(it.cause.left()))\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
