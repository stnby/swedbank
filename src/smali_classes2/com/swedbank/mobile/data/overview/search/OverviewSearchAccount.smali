.class public final Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;
.super Ljava/lang/Object;
.source "OverviewSearchNetworkModels.kt"


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;
        .annotation runtime Lcom/squareup/moshi/d;
            a = "transactions"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountTransactionData"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;->a:Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    return-object v0
.end method
