.class public final Lcom/swedbank/mobile/data/overview/search/a$b;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/search/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/search/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/search/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/search/a$b;->a:Lcom/swedbank/mobile/data/overview/search/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 18
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+",
            "Lcom/swedbank/mobile/data/overview/search/OverviewSearchResponse;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/overview/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "it"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    instance-of v1, v0, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v1, :cond_8

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/overview/search/OverviewSearchResponse;

    move-object/from16 v1, p0

    .line 241
    iget-object v2, v1, Lcom/swedbank/mobile/data/overview/search/a$b;->a:Lcom/swedbank/mobile/data/overview/search/a;

    .line 246
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/overview/search/OverviewSearchResponse;->a()Ljava/util/List;

    move-result-object v0

    .line 248
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    if-eqz v2, :cond_7

    .line 249
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xf

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 256
    check-cast v0, Ljava/lang/Iterable;

    .line 257
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 258
    check-cast v6, Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;

    .line 256
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;->a()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a()Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 259
    move-object v7, v2

    check-cast v7, Ljava/util/Collection;

    invoke-static {v7, v6}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 261
    :cond_0
    move-object v5, v2

    check-cast v5, Ljava/util/Collection;

    .line 253
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v3

    const/4 v6, 0x0

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    move-object v2, v6

    :goto_1
    if-eqz v2, :cond_6

    .line 265
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v2

    .line 268
    invoke-static {}, Lcom/swedbank/mobile/data/overview/transaction/p;->a()Ljava/util/Comparator;

    move-result-object v5

    invoke-static {v2, v5}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object v2

    .line 267
    invoke-static {v2, v4}, Lkotlin/i/f;->a(Lkotlin/i/e;I)Lkotlin/i/e;

    move-result-object v2

    .line 270
    invoke-static {v2}, Lkotlin/i/f;->a(Lkotlin/i/e;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {v5}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->d()Ljava/lang/String;

    move-result-object v5

    .line 274
    sget-object v7, Lcom/swedbank/mobile/data/overview/search/a$a;->a:Lcom/swedbank/mobile/data/overview/search/a$a;

    check-cast v7, Lkotlin/e/a/b;

    invoke-static {v2, v7}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v2

    .line 273
    invoke-static {v2}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object v2

    .line 276
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x0

    if-ne v7, v4, :cond_5

    .line 278
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;

    .line 277
    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/search/OverviewSearchAccount;->a()Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;

    move-result-object v7

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->b()Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/swedbank/mobile/data/overview/transaction/TransactionPageInfo;->c()Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_2

    :cond_3
    move-object v4, v6

    :goto_2
    if-eqz v4, :cond_4

    const/4 v0, 0x1

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_5

    goto :goto_4

    :cond_5
    const/4 v3, 0x0

    .line 280
    :goto_4
    new-instance v0, Lcom/swedbank/mobile/business/overview/search/f;

    invoke-direct {v0, v2, v5, v3}, Lcom/swedbank/mobile/business/overview/search/f;-><init>(Ljava/util/List;Ljava/lang/String;Z)V

    goto :goto_5

    .line 286
    :cond_6
    new-instance v0, Lcom/swedbank/mobile/business/overview/search/f;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x7

    const/4 v11, 0x0

    move-object v6, v0

    invoke-direct/range {v6 .. v11}, Lcom/swedbank/mobile/business/overview/search/f;-><init>(Ljava/util/List;Ljava/lang/String;ZILkotlin/e/b/g;)V

    goto :goto_5

    .line 288
    :cond_7
    new-instance v0, Lcom/swedbank/mobile/business/overview/search/f;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x7

    const/16 v17, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v17}, Lcom/swedbank/mobile/business/overview/search/f;-><init>(Ljava/util/List;Ljava/lang/String;ZILkotlin/e/b/g;)V

    .line 291
    :goto_5
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v2, "Single.just(result)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_7

    :cond_8
    move-object/from16 v1, p0

    .line 116
    instance-of v2, v0, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v2, :cond_a

    check-cast v0, Lcom/swedbank/mobile/data/network/w$b;

    .line 236
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 237
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 238
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 239
    check-cast v3, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 236
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 240
    :cond_9
    check-cast v2, Ljava/util/List;

    .line 116
    invoke-static {v2}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    .line 292
    new-instance v0, Lcom/swedbank/mobile/business/overview/search/f;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/business/overview/search/f;-><init>(Ljava/util/List;Ljava/lang/String;ZILkotlin/e/b/g;)V

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v2, "Single.just(onError(it.errorMessages().right()))"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_7

    .line 117
    :cond_a
    instance-of v2, v0, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v2, :cond_b

    check-cast v0, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    .line 293
    new-instance v0, Lcom/swedbank/mobile/business/overview/search/f;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/business/overview/search/f;-><init>(Ljava/util/List;Ljava/lang/String;ZILkotlin/e/b/g;)V

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v2, "Single.just(onError(it.cause.left()))"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_7
    return-object v0

    :cond_b
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/search/a$b;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
