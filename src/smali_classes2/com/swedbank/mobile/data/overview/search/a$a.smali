.class public final Lcom/swedbank/mobile/data/overview/search/a$a;
.super Lkotlin/e/b/k;
.source "OverviewSearchRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/overview/search/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;",
        "Lcom/swedbank/mobile/business/overview/Transaction;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/overview/search/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/overview/search/a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/overview/search/a$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/overview/search/a$a;->a:Lcom/swedbank/mobile/data/overview/search/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;)Lcom/swedbank/mobile/business/overview/Transaction;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;->a()Lcom/swedbank/mobile/data/overview/transaction/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/overview/transaction/e;->p()Lcom/swedbank/mobile/business/overview/Transaction;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/search/a$a;->a(Lcom/swedbank/mobile/data/overview/transaction/TransactionEdge;)Lcom/swedbank/mobile/business/overview/Transaction;

    move-result-object p1

    return-object p1
.end method
