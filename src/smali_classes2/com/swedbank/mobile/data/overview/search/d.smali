.class public final Lcom/swedbank/mobile/data/overview/search/d;
.super Ljava/lang/Object;
.source "OverviewSearchService.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/data/overview/search/c;Ljava/lang/String;ILjava/lang/String;Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;Lcom/squareup/moshi/n;)Lio/reactivex/w;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/data/overview/search/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/overview/search/c;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;",
            "Lcom/squareup/moshi/n;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/overview/search/OverviewSearchResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$searchTransactions"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filter"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n{\n  fetchAccounts(customerId:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\",operation:INFORMATION) {\n    transactions(first:"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0xa

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_0

    .line 26
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, ",after:\""

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x22

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n, filter: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    const-class p2, Lcom/swedbank/mobile/data/overview/search/OverviewSearchFilterData;

    invoke-virtual {p5, p2}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p2

    invoke-virtual {p2, p4}, Lcom/squareup/moshi/JsonAdapter;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 38
    invoke-static {p2, p1}, Lcom/swedbank/mobile/data/network/l;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 39
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "result.toString()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\n    ) {\n      ...transactionData\n    }\n  }\n}\n"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    sget-object p1, Lcom/swedbank/mobile/data/overview/transaction/TransactionListResponse;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    .line 21
    new-instance p3, Lcom/swedbank/mobile/data/network/GraphQLRequest;

    const/4 p4, 0x0

    invoke-direct {p3, p1, p4, p2, p4}, Lcom/swedbank/mobile/data/network/GraphQLRequest;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    invoke-interface {p0, p3}, Lcom/swedbank/mobile/data/overview/search/c;->a(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method
