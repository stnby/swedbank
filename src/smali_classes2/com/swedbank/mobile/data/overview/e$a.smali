.class final Lcom/swedbank/mobile/data/overview/e$a;
.super Ljava/lang/Object;
.source "OverviewRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/overview/e;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/overview/e;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/overview/e;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/e$a;->a:Lcom/swedbank/mobile/data/overview/e;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/e$a;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/overview/e$a;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "startCursor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, v1, :cond_1

    .line 35
    iget-object p1, p0, Lcom/swedbank/mobile/data/overview/e$a;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-static {p1}, Lcom/swedbank/mobile/data/overview/e;->a(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/overview/g;

    move-result-object p1

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/e$a;->b:Ljava/lang/String;

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/e$a;->c:Ljava/lang/String;

    const/16 v2, 0xf

    .line 36
    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/data/overview/h;->a(Lcom/swedbank/mobile/data/overview/g;Ljava/lang/String;Ljava/lang/String;I)Lio/reactivex/w;

    move-result-object p1

    .line 40
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 110
    new-instance v0, Lcom/swedbank/mobile/data/overview/e$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/overview/e$a$a;-><init>(Lcom/swedbank/mobile/data/overview/e$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/e$a;->a:Lcom/swedbank/mobile/data/overview/e;

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/e;->a(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/overview/g;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/swedbank/mobile/data/overview/e$a;->b:Ljava/lang/String;

    .line 53
    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/e$a;->c:Ljava/lang/String;

    .line 51
    invoke-static {v0, v1, v2, p1}, Lcom/swedbank/mobile/data/overview/h;->a(Lcom/swedbank/mobile/data/overview/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 55
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 119
    new-instance v0, Lcom/swedbank/mobile/data/overview/e$a$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/overview/e$a$b;-><init>(Lcom/swedbank/mobile/data/overview/e$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/overview/e$a;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
