.class public final Lcom/swedbank/mobile/data/overview/e;
.super Ljava/lang/Object;
.source "OverviewRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/n;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/overview/g;

.field private final b:Lcom/swedbank/mobile/data/account/c;

.field private final c:Lcom/swedbank/mobile/data/overview/transaction/o;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/overview/g;Lcom/swedbank/mobile/data/account/c;Lcom/swedbank/mobile/data/overview/transaction/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/overview/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/account/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/overview/transaction/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "overviewService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountRepositoryImpl"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionRepositoryImpl"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/overview/e;->a:Lcom/swedbank/mobile/data/overview/g;

    iput-object p2, p0, Lcom/swedbank/mobile/data/overview/e;->b:Lcom/swedbank/mobile/data/account/c;

    iput-object p3, p0, Lcom/swedbank/mobile/data/overview/e;->c:Lcom/swedbank/mobile/data/overview/transaction/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/overview/g;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/overview/e;->a:Lcom/swedbank/mobile/data/overview/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/account/c;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/overview/e;->b:Lcom/swedbank/mobile/data/account/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/overview/e;)Lcom/swedbank/mobile/data/overview/transaction/o;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/data/overview/e;->c:Lcom/swedbank/mobile/data/overview/transaction/o;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/data/overview/e;->c:Lcom/swedbank/mobile/data/overview/transaction/o;

    .line 32
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/data/overview/transaction/o;->d(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/swedbank/mobile/data/overview/e$a;

    invoke-direct {v1, p0, p1, p2}, Lcom/swedbank/mobile/data/overview/e$a;-><init>(Lcom/swedbank/mobile/data/overview/e;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "transactionRepositoryImp\u2026      }\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountIds"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    check-cast p2, Ljava/lang/Iterable;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 111
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 112
    check-cast v1, Ljava/lang/String;

    .line 73
    iget-object v2, p0, Lcom/swedbank/mobile/data/overview/e;->a:Lcom/swedbank/mobile/data/overview/g;

    const/16 v3, 0xf

    .line 74
    invoke-static {v2, v1, p1, v3}, Lcom/swedbank/mobile/data/overview/h;->a(Lcom/swedbank/mobile/data/overview/g;Ljava/lang/String;Ljava/lang/String;I)Lio/reactivex/w;

    move-result-object v1

    .line 78
    invoke-static {v1}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 81
    check-cast v0, Ljava/lang/Iterable;

    new-instance p1, Lcom/swedbank/mobile/data/overview/e$b;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/overview/e$b;-><init>(Lcom/swedbank/mobile/data/overview/e;)V

    check-cast p1, Lio/reactivex/c/h;

    invoke-static {v0, p1}, Lio/reactivex/w;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "Single.zip(queries) { re\u2026or)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "accountIds\n      .map { \u2026      }\n        }\n      }"

    .line 80
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
