.class public final Lcom/swedbank/mobile/data/h/c;
.super Ljava/lang/Object;
.source "NavigationRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/navigation/l;


# instance fields
.field private final a:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/swedbank/mobile/data/h/c;->a:Ljava/util/ArrayDeque;

    .line 19
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object v0

    const-string v1, "BehaviorRelay.create<NavigationItemKey>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/h/c;->b:Lcom/b/c/b;

    .line 20
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<NavigationItemKey>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/data/h/c;->c:Lcom/b/c/c;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/c;->b:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "navigationItemKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/c;->b:Lcom/b/c/b;

    .line 30
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/swedbank/mobile/data/h/c;->c:Lcom/b/c/c;

    invoke-virtual {v1, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 33
    :cond_0
    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/c;->a:Ljava/util/ArrayDeque;

    .line 36
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 37
    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    .line 64
    instance-of v2, v1, Ljava/util/Collection;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 66
    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 37
    invoke-static {v2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v3, v3, 0x1

    if-gez v3, :cond_2

    invoke-static {}, Lkotlin/a/h;->c()V

    goto :goto_0

    :cond_3
    :goto_1
    const/4 v1, 0x1

    if-le v3, v1, :cond_4

    .line 38
    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->removeFirstOccurrence(Ljava/lang/Object;)Z

    .line 40
    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v2

    if-eq v2, v1, :cond_6

    .line 41
    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_2

    .line 44
    :cond_5
    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->removeFirstOccurrence(Ljava/lang/Object;)Z

    .line 45
    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    :cond_6
    :goto_2
    return-void
.end method

.method public b()Lcom/swedbank/mobile/business/util/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/c;->b:Lcom/b/c/b;

    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    .line 26
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/util/l;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/c;->a:Ljava/util/ArrayDeque;

    .line 53
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 54
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    .line 55
    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    :cond_0
    return-object v2
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/c;->c:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method
