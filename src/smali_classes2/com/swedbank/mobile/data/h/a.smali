.class public final Lcom/swedbank/mobile/data/h/a;
.super Ljava/lang/Object;
.source "NavigationItemReselectStreamImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/navigation/i;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/navigation/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/navigation/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/navigation/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "navigationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/h/a;->a:Lcom/swedbank/mobile/business/navigation/l;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/o;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "navigationItemKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/data/h/a;->a:Lcom/swedbank/mobile/business/navigation/l;

    .line 13
    invoke-interface {v0}, Lcom/swedbank/mobile/business/navigation/l;->d()Lio/reactivex/o;

    move-result-object v0

    .line 14
    new-instance v1, Lcom/swedbank/mobile/data/h/a$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/h/a$a;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    .line 15
    sget-object v0, Lcom/swedbank/mobile/data/h/a$b;->a:Lcom/swedbank/mobile/data/h/a$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "navigationRepository\n   \u2026Key }\n      .map { Unit }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
