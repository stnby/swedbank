.class public final Lcom/swedbank/mobile/data/g/h;
.super Ljava/lang/Object;
.source "SqliteMagic_UserPropertyData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/g/i;
    .locals 3

    .line 78
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 79
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 80
    new-instance p1, Lcom/swedbank/mobile/data/g/i;

    .line 81
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    add-int/lit8 v2, v0, 0x1

    .line 82
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x2

    .line 83
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-direct {p1, v1, v2, p0}, Lcom/swedbank/mobile/data/g/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Z)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/g/i;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/g/i;"
        }
    .end annotation

    .line 89
    invoke-virtual {p2, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p2, :cond_1

    .line 91
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_0

    return-object v0

    :cond_0
    const-string p2, "user_property_data"

    .line 96
    :cond_1
    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_3

    .line 98
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 99
    new-instance p2, Lcom/swedbank/mobile/data/g/i;

    .line 100
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    add-int/lit8 p3, p1, 0x1

    .line 101
    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    add-int/lit8 p1, p1, 0x2

    .line 102
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-direct {p2, v0, p3, p0}, Lcom/swedbank/mobile/data/g/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Z)V

    return-object p2

    .line 104
    :cond_3
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".local_id"

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".enabled"

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_6

    .line 113
    new-instance p2, Lcom/swedbank/mobile/data/g/i;

    if-eqz p3, :cond_5

    .line 114
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 115
    :cond_5
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 116
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-direct {p2, v0, p3, p0}, Lcom/swedbank/mobile/data/g/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Z)V

    return-object p2

    .line 111
    :cond_6
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"user_property_data\" required column \"enabled\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 107
    :cond_7
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"user_property_data\" required column \"name\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/swedbank/mobile/data/g/i;)Ljava/lang/Long;
    .locals 0

    .line 73
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/g/i;->b()Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/g/i;)V
    .locals 2

    .line 35
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x337a8b

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "name"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, -0x1

    :goto_1
    packed-switch p2, :pswitch_data_0

    goto :goto_2

    .line 43
    :pswitch_0
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/g/i;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 37
    :pswitch_1
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/g/i;->b()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 40
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/g/i;->b()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    invoke-interface {p0, p1, p2, p3}, Landroidx/k/a/f;->a(IJ)V

    goto :goto_2

    .line 38
    :cond_3
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "local_id column is null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/g/i;)V
    .locals 2

    .line 60
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/g/i;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 62
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/g/i;->d()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v0, p1

    const/4 p1, 0x2

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/g/i;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/g/i;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->b()V

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/g/i;->b()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "local_id"

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/g/i;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "name"

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/g/i;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "enabled"

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/g/i;->d()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-static {p0}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/swedbank/mobile/data/g/i;)Z
    .locals 4

    .line 49
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x337a8b

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "name"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 55
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " is not unique"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    return v3

    .line 51
    :pswitch_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/g/i;->b()Ljava/lang/Long;

    move-result-object p0

    if-nez p0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/g/i;)V
    .locals 2

    .line 67
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 68
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/g/i;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/g/i;->d()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v0, p1

    const/4 p1, 0x2

    invoke-interface {p0, p1, v0, v1}, Landroidx/k/a/f;->a(IJ)V

    return-void
.end method
