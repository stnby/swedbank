.class public final Lcom/swedbank/mobile/data/g/f;
.super Ljava/lang/Object;
.source "RemoteConfigUpdateStreamImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/firebase/e;
.implements Lcom/swedbank/mobile/data/push/a;
.implements Lcom/swedbank/mobile/data/push/b;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/data/g/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/g/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/g/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "firebaseRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/g/f;->a:Lcom/swedbank/mobile/data/g/c;

    .line 40
    invoke-static {p0}, Lcom/swedbank/mobile/data/g/f;->a(Lcom/swedbank/mobile/data/g/f;)Lcom/swedbank/mobile/data/g/c;

    move-result-object p1

    const-string v0, "PUSH_RC"

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/data/g/c;->d(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/g/f;)Lcom/swedbank/mobile/data/g/c;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/data/g/f;->a:Lcom/swedbank/mobile/data/g/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/f;->a:Lcom/swedbank/mobile/data/g/c;

    const/4 v1, 0x0

    .line 21
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/data/g/c;->a(Z)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 38
    invoke-static {p0}, Lcom/swedbank/mobile/data/g/f;->a(Lcom/swedbank/mobile/data/g/f;)Lcom/swedbank/mobile/data/g/c;

    move-result-object p1

    const-string v0, "PUSH_RC"

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/data/g/c;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string p1, "data"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "CONFIG_STATE"

    .line 29
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string p2, "STALE"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/g/f;->b()V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/f;->a:Lcom/swedbank/mobile/data/g/c;

    const/4 v1, 0x1

    .line 24
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/data/g/c;->a(Z)V

    return-void
.end method
