.class public final Lcom/swedbank/mobile/data/g/c;
.super Ljava/lang/Object;
.source "FirebaseRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/firebase/c;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field private final b:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

.field private final c:Lcom/swedbank/mobile/business/e/i;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/e/i;)V
    .locals 2
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/data/g/c;->c:Lcom/swedbank/mobile/business/e/i;

    .line 36
    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "FirebaseAnalytics.getInstance(application)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/g/c;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    .line 39
    invoke-static {}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getInstance()Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    move-result-object p1

    .line 47
    sget p2, Lcom/swedbank/mobile/data/p$a;->data_remote_config_defaults:I

    invoke-virtual {p1, p2}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->setDefaults(I)V

    .line 48
    iget-object p2, p0, Lcom/swedbank/mobile/data/g/c;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v0, "app_version"

    iget-object v1, p0, Lcom/swedbank/mobile/data/g/c;->c:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->setUserProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->activateFetched()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 162
    sget-object p2, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/swedbank/mobile/data/g/c;->b:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/g/c;)Lcom/google/firebase/analytics/FirebaseAnalytics;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/data/g/c;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    return-object p0
.end method


# virtual methods
.method public varargs a([Lcom/swedbank/mobile/business/firebase/f;)Lio/reactivex/b;
    .locals 1
    .param p1    # [Lcom/swedbank/mobile/business/firebase/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userProperties"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/swedbank/mobile/data/g/c$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/g/c$d;-><init>(Lcom/swedbank/mobile/data/g/c;[Lcom/swedbank/mobile/business/firebase/f;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026      }\n      }\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/firebase/f$a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 74
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    const-string v2, "USER_PROPERTY_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 76
    sget-object v1, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ea;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "USER_PROPERTY_DATA.NAME"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "bleeding_edge_user"

    .line 139
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->c(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.isNot(value)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    sget-object v2, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/ea;->d:Lcom/siimkinks/sqlitemagic/n;

    const-string v3, "USER_PROPERTY_DATA.ENABLED"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 140
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v4, "this.`is`(value)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.and(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    new-array v1, v3, [Lcom/siimkinks/sqlitemagic/cb$f;

    .line 78
    sget-object v2, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/ea;->c:Lcom/siimkinks/sqlitemagic/du;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/du;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$h;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$e;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object v0

    .line 81
    sget-object v1, Lcom/swedbank/mobile/data/g/c$c;->a:Lcom/swedbank/mobile/data/g/c$c;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM U\u2026tyData::toUserProperty) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/c;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {v0, p1, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p1}, Lcom/crashlytics/android/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/c;->b:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    if-eqz p1, :cond_0

    const-wide/16 v1, 0x0

    .line 100
    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->fetch(J)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    goto :goto_0

    .line 101
    :cond_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x4

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->fetch(J)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    .line 102
    :goto_0
    new-instance v2, Lcom/swedbank/mobile/data/g/c$b;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/data/g/c$b;-><init>(Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;Lcom/swedbank/mobile/data/g/c;Z)V

    check-cast v2, Lcom/google/android/gms/tasks/OnSuccessListener;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/c;->b:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    .line 113
    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/c;->b:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    .line 116
    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "remoteConfig\n      .getString(key)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b()V
    .locals 5

    .line 84
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 85
    sget-object v1, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    const-string v2, "USER_PROPERTY_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 86
    sget-object v1, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ea;->d:Lcom/siimkinks/sqlitemagic/n;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    .line 143
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/dx$a;->a()I

    .line 88
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 89
    sget-object v1, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/ea;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v2, "USER_PROPERTY_DATA.NAME"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    const-string v2, "USER_PROPERTY_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 144
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "(SELECT\n        COLUMN U\u2026_DATA)\n        .execute()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 145
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 146
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 147
    check-cast v2, Ljava/lang/String;

    .line 92
    iget-object v3, p0, Lcom/swedbank/mobile/data/g/c;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v4, "false"

    invoke-virtual {v3, v2, v4}, Lcom/google/firebase/analytics/FirebaseAnalytics;->setUserProperty(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    :cond_0
    check-cast v1, Ljava/util/List;

    return-void
.end method

.method public c(Ljava/lang/String;)J
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/c;->b:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    .line 119
    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 137
    new-instance v0, Lcom/swedbank/mobile/data/g/c$a;

    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->getInstance()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/g/c$a;-><init>(Lcom/google/firebase/iid/FirebaseInstanceId;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/g/d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/g/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    .line 136
    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction(F\u2026ance()::deleteInstanceId)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "topic"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Subscribing to topic ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    invoke-static {}, Lcom/google/firebase/messaging/FirebaseMessaging;->getInstance()Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/messaging/FirebaseMessaging;->subscribeToTopic(Ljava/lang/String;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method
