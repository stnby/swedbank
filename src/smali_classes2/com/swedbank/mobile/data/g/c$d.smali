.class final Lcom/swedbank/mobile/data/g/c$d;
.super Ljava/lang/Object;
.source "FirebaseRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/g/c;->a([Lcom/swedbank/mobile/business/firebase/f;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/g/c;

.field final synthetic b:[Lcom/swedbank/mobile/business/firebase/f;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/g/c;[Lcom/swedbank/mobile/business/firebase/f;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/g/c$d;->a:Lcom/swedbank/mobile/data/g/c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/g/c$d;->b:[Lcom/swedbank/mobile/business/firebase/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 11

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/data/g/c$d;->b:[Lcom/swedbank/mobile/business/firebase/f;

    .line 139
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 55
    instance-of v4, v3, Lcom/swedbank/mobile/business/firebase/f$c;

    if-eqz v4, :cond_0

    .line 56
    check-cast v3, Lcom/swedbank/mobile/business/firebase/f$c;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/firebase/f$c;->a()Ljava/lang/String;

    move-result-object v3

    .line 57
    invoke-static {v3}, Lcom/crashlytics/android/a;->a(Ljava/lang/String;)V

    .line 58
    iget-object v4, p0, Lcom/swedbank/mobile/data/g/c$d;->a:Lcom/swedbank/mobile/data/g/c;

    invoke-static {v4}, Lcom/swedbank/mobile/data/g/c;->a(Lcom/swedbank/mobile/data/g/c;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/firebase/analytics/FirebaseAnalytics;->setUserId(Ljava/lang/String;)V

    goto :goto_1

    .line 60
    :cond_0
    instance-of v4, v3, Lcom/swedbank/mobile/business/firebase/f$b;

    if-eqz v4, :cond_1

    .line 61
    iget-object v4, p0, Lcom/swedbank/mobile/data/g/c$d;->a:Lcom/swedbank/mobile/data/g/c;

    invoke-static {v4}, Lcom/swedbank/mobile/data/g/c;->a(Lcom/swedbank/mobile/data/g/c;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v4

    move-object v5, v3

    check-cast v5, Lcom/swedbank/mobile/business/firebase/f$b;

    invoke-interface {v5}, Lcom/swedbank/mobile/business/firebase/f$b;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5}, Lcom/swedbank/mobile/business/firebase/f$b;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Lcom/google/firebase/analytics/FirebaseAnalytics;->setUserProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    instance-of v4, v3, Lcom/swedbank/mobile/business/firebase/f$a;

    if-eqz v4, :cond_1

    .line 63
    check-cast v3, Lcom/swedbank/mobile/business/firebase/f$a;

    .line 140
    new-instance v10, Lcom/swedbank/mobile/data/g/i;

    const/4 v5, 0x0

    .line 141
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/firebase/f$a;->c()Ljava/lang/String;

    move-result-object v6

    .line 142
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/firebase/f$a;->a()Z

    move-result v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v4, v10

    .line 140
    invoke-direct/range {v4 .. v9}, Lcom/swedbank/mobile/data/g/i;-><init>(Ljava/lang/Long;Ljava/lang/String;ZILkotlin/e/b/g;)V

    .line 144
    invoke-static {v10}, Lcom/siimkinks/sqlitemagic/dg$a;->a(Lcom/swedbank/mobile/data/g/i;)Lcom/siimkinks/sqlitemagic/dg$a;

    move-result-object v3

    .line 65
    sget-object v4, Lcom/siimkinks/sqlitemagic/ea;->a:Lcom/siimkinks/sqlitemagic/ea;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/ea;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v4, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/dg$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/d;

    move-result-object v3

    .line 66
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/a/d;->b()J

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
