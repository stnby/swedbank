.class public final Lcom/swedbank/mobile/data/biometric/c;
.super Ljava/lang/Object;
.source "BiometricDataModule_ProvideBiometricsRetrofitFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lretrofit2/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/c;->a:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/c;->b:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/c;->c:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/swedbank/mobile/data/biometric/c;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/u;",
            ">;)",
            "Lcom/swedbank/mobile/data/biometric/c;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/swedbank/mobile/data/biometric/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/data/biometric/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/business/c/b;Lcom/squareup/moshi/n;Lokhttp3/x;Lokhttp3/u;)Lretrofit2/r;
    .locals 0

    .line 44
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/biometric/a;->a(Lcom/swedbank/mobile/business/c/b;Lcom/squareup/moshi/n;Lokhttp3/x;Lokhttp3/u;)Lretrofit2/r;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lretrofit2/r;

    return-object p0
.end method


# virtual methods
.method public a()Lretrofit2/r;
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/c/b;

    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/c;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/moshi/n;

    iget-object v2, p0, Lcom/swedbank/mobile/data/biometric/c;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lokhttp3/x;

    iget-object v3, p0, Lcom/swedbank/mobile/data/biometric/c;->d:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokhttp3/u;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/biometric/c;->a(Lcom/swedbank/mobile/business/c/b;Lcom/squareup/moshi/n;Lokhttp3/x;Lokhttp3/u;)Lretrofit2/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/biometric/c;->a()Lretrofit2/r;

    move-result-object v0

    return-object v0
.end method
