.class public final Lcom/swedbank/mobile/data/biometric/a;
.super Ljava/lang/Object;
.source "BiometricDataModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/data/biometric/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    new-instance v0, Lcom/swedbank/mobile/data/biometric/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/biometric/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/biometric/a;->a:Lcom/swedbank/mobile/data/biometric/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lretrofit2/r;)Lcom/swedbank/mobile/data/network/aa;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometrics"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "for_biometrics"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/swedbank/mobile/data/network/aa;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/network/aa;-><init>(Lretrofit2/r;)V

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/data/biometric/a/a;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/data/biometric/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/biometric/a/a;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "externalBiometricAuthenticationRequestStream"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 45
    new-array v0, v0, [Lcom/swedbank/mobile/data/biometric/a/a;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/b;Lcom/squareup/moshi/n;Lokhttp3/x;Lokhttp3/u;)Lretrofit2/r;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lokhttp3/x;
        .annotation runtime Ljavax/inject/Named;
            value = "HTTP_CLIENT"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lokhttp3/u;
        .annotation runtime Ljavax/inject/Named;
            value = "internal_service_requests_interceptor"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "for_biometrics"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "httpClient"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceInterceptor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 58
    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/network/n;->a(Lcom/squareup/moshi/n;Z)Lretrofit2/r$a;

    move-result-object p1

    .line 59
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->f()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lretrofit2/r$a;->a(Ljava/lang/String;)Lretrofit2/r$a;

    move-result-object p0

    const-string p1, "retrofitBuilder(jsonMapp\u2026etricAuthenticationUrl())"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lretrofit2/a/b/c;->a()Lretrofit2/a/b/c;

    move-result-object p1

    const-string v1, "ScalarsConverterFactory.create()"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lretrofit2/f$a;

    .line 108
    invoke-virtual {p0}, Lretrofit2/r$a;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 62
    invoke-virtual {p2}, Lokhttp3/x;->A()Lokhttp3/x$a;

    move-result-object p1

    const/4 p2, 0x0

    .line 63
    invoke-virtual {p1, p2}, Lokhttp3/x$a;->a(Lokhttp3/c;)Lokhttp3/x$a;

    move-result-object p1

    const-string p2, "httpClient\n          .ne\u2026()\n          .cache(null)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p1}, Lokhttp3/x$a;->a()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, v0, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 65
    invoke-virtual {p1}, Lokhttp3/x$a;->b()Lokhttp3/x;

    move-result-object p1

    .line 61
    invoke-virtual {p0, p1}, Lretrofit2/r$a;->a(Lokhttp3/x;)Lretrofit2/r$a;

    move-result-object p0

    .line 66
    invoke-virtual {p0}, Lretrofit2/r$a;->b()Lretrofit2/r;

    move-result-object p0

    const-string p1, "retrofitBuilder(jsonMapp\u2026 .build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final b(Lretrofit2/r;)Lcom/swedbank/mobile/data/biometric/authentication/c;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometrics"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    const-class v0, Lcom/swedbank/mobile/data/biometric/authentication/c;

    invoke-virtual {p0, v0}, Lretrofit2/r;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/biometric/authentication/c;

    return-object p0
.end method
