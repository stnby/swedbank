.class final Lcom/swedbank/mobile/data/biometric/f$e;
.super Ljava/lang/Object;
.source "BiometricRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/f;->d()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/f;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/f;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/f$e;->a:Lcom/swedbank/mobile/data/biometric/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f$e;->a:Lcom/swedbank/mobile/data/biometric/f;

    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/f;->b(Lcom/swedbank/mobile/data/biometric/f;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/f$e;->a:Lcom/swedbank/mobile/data/biometric/f;

    invoke-static {v1}, Lcom/swedbank/mobile/data/biometric/f;->c(Lcom/swedbank/mobile/data/biometric/f;)Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "application.packageName"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/d/a;->c(Ljava/lang/String;)Ljava/security/Key;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/f$e;->a:Lcom/swedbank/mobile/data/biometric/f;

    invoke-static {v1}, Lcom/swedbank/mobile/data/biometric/f;->b(Lcom/swedbank/mobile/data/biometric/f;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/d/a;->a(Ljava/security/Key;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/biometric/f$e;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
