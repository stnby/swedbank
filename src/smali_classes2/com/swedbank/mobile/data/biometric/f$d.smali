.class final synthetic Lcom/swedbank/mobile/data/biometric/f$d;
.super Lkotlin/e/b/i;
.source "BiometricRepositoryImpl.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/f;->f()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/a<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/biometric/c;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/f;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/data/biometric/f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "getAvailableBiometricAuthTypesImpl"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "getAvailableBiometricAuthTypesImpl()Ljava/util/List;"

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/biometric/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f$d;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/data/biometric/f;

    .line 227
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1c

    if-lt v1, v2, :cond_3

    .line 228
    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/f;->c(Lcom/swedbank/mobile/data/biometric/f;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 229
    invoke-static {}, Lcom/swedbank/mobile/data/biometric/g;->a()[Lkotlin/k;

    move-result-object v1

    .line 230
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 239
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_2

    aget-object v5, v1, v4

    .line 238
    invoke-virtual {v5}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/biometric/c;

    .line 241
    invoke-virtual {v0, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    .line 238
    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 245
    :cond_2
    check-cast v2, Ljava/util/List;

    goto :goto_2

    .line 246
    :cond_3
    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/f;->d(Lcom/swedbank/mobile/data/biometric/f;)Landroidx/core/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/core/b/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/swedbank/mobile/business/biometric/c;->a:Lcom/swedbank/mobile/business/biometric/c;

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_2

    .line 247
    :cond_4
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/biometric/f$d;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
