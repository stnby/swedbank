.class public final Lcom/swedbank/mobile/data/biometric/f;
.super Ljava/lang/Object;
.source "BiometricRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/d;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/d;

.field private final c:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/app/Application;

.field private final e:Lcom/swedbank/mobile/business/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/data/biometric/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fingerprintManager"

    const-string v4, "getFingerprintManager()Landroidx/core/hardware/fingerprint/FingerprintManagerCompat;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/data/biometric/f;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/d/a;Lcom/a/a/a/f;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferences"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/f;->d:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/f;->e:Lcom/swedbank/mobile/business/d/a;

    .line 54
    new-instance p1, Lcom/swedbank/mobile/data/biometric/f$c;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/data/biometric/f$c;-><init>(Lcom/swedbank/mobile/data/biometric/f;)V

    check-cast p1, Lkotlin/e/a/a;

    invoke-static {p1}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/f;->b:Lkotlin/d;

    const-string p1, "biometricAuthStatePreference"

    .line 57
    sget-object p2, Lcom/swedbank/mobile/business/biometric/b;->a:Lcom/swedbank/mobile/business/biometric/b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/biometric/b;->a()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/a/a/a/d;

    move-result-object p1

    const-string p2, "appPreferences.getIntege\u2026tricAuthState.UNKNOWN.id)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/f;->c:Lcom/a/a/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/biometric/f;)Lcom/a/a/a/d;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/f;->c:Lcom/a/a/a/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/biometric/f;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/f;->e:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/biometric/f;)Landroid/app/Application;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/f;->d:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/data/biometric/f;)Landroidx/core/b/a/a;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/data/biometric/f;->g()Landroidx/core/b/a/a;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroidx/core/b/a/a;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f;->b:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/data/biometric/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/core/b/a/a;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "biometricAuthState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/swedbank/mobile/data/biometric/f$g;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/biometric/f$g;-><init>(Lcom/swedbank/mobile/data/biometric/f;Lcom/swedbank/mobile/business/biometric/b;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .fromA\u2026tricAuthState.id)\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f;->c:Lcom/a/a/a/d;

    .line 60
    invoke-interface {v0}, Lcom/a/a/a/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/swedbank/mobile/data/biometric/f$f;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/data/biometric/f;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/data/biometric/f$f;-><init>(Lcom/swedbank/mobile/data/biometric/f;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/data/biometric/i;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/biometric/i;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "biometricAuthState\n     \u2026:parseBiometricAuthState)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/security/Signature;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/Signature;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    new-instance v0, Lcom/swedbank/mobile/data/biometric/f$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/biometric/f$b;-><init>(Lcom/swedbank/mobile/data/biometric/f;Ljava/security/Signature;)V

    check-cast v0, Lio/reactivex/q;

    invoke-static {v0}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object p1

    .line 157
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.create<Biomet\u2026scribeOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/security/Signature;Landroid/hardware/biometrics/BiometricPrompt;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/biometrics/BiometricPrompt;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/Signature;",
            "Landroid/hardware/biometrics/BiometricPrompt;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricPrompt"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    new-instance v0, Lcom/swedbank/mobile/data/biometric/f$a;

    invoke-direct {v0, p0, p2, p1}, Lcom/swedbank/mobile/data/biometric/f$a;-><init>(Lcom/swedbank/mobile/data/biometric/f;Landroid/hardware/biometrics/BiometricPrompt;Ljava/security/Signature;)V

    check-cast v0, Lio/reactivex/q;

    invoke-static {v0}, Lio/reactivex/o;->a(Lio/reactivex/q;)Lio/reactivex/o;

    move-result-object p1

    .line 204
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/o;->b(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "Observable.create<Biomet\u2026scribeOn(Schedulers.io())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/biometric/f;->a()Lio/reactivex/o;

    move-result-object v0

    .line 64
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->a:Lcom/swedbank/mobile/business/biometric/b;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "observeBiometricAuthStat\u2026ometricAuthState.UNKNOWN)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Z
    .locals 2

    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 78
    new-instance v0, Lcom/swedbank/mobile/data/biometric/f$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/biometric/f$e;-><init>(Lcom/swedbank/mobile/data/biometric/f;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.fromCallable {\n  \u2026  else -> false\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Z
    .locals 8

    .line 234
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x1c

    if-lt v0, v2, :cond_3

    .line 235
    invoke-static {p0}, Lcom/swedbank/mobile/data/biometric/f;->c(Lcom/swedbank/mobile/data/biometric/f;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 236
    invoke-static {}, Lcom/swedbank/mobile/data/biometric/g;->a()[Lkotlin/k;

    move-result-object v2

    .line 237
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 246
    array-length v4, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v2, v5

    .line 245
    invoke-virtual {v6}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/swedbank/mobile/business/biometric/c;

    .line 248
    invoke-virtual {v0, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    .line 245
    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 252
    :cond_2
    check-cast v3, Ljava/util/List;

    goto :goto_2

    .line 253
    :cond_3
    invoke-static {p0}, Lcom/swedbank/mobile/data/biometric/f;->d(Lcom/swedbank/mobile/data/biometric/f;)Landroidx/core/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/core/b/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/swedbank/mobile/business/biometric/c;->a:Lcom/swedbank/mobile/business/biometric/c;

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto :goto_2

    .line 254
    :cond_4
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v3

    .line 91
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    invoke-static {v3}, Lkotlin/a/h;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/c;

    sget-object v3, Lcom/swedbank/mobile/business/biometric/c;->a:Lcom/swedbank/mobile/business/biometric/c;

    if-ne v0, v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method public f()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/biometric/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 97
    new-instance v0, Lcom/swedbank/mobile/data/biometric/f$d;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/data/biometric/f;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/biometric/f$d;-><init>(Lcom/swedbank/mobile/data/biometric/f;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/data/biometric/j;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/biometric/j;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single\n      .fromCallab\u2026leBiometricAuthTypesImpl)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
