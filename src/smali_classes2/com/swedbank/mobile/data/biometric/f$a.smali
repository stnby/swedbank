.class final Lcom/swedbank/mobile/data/biometric/f$a;
.super Ljava/lang/Object;
.source "BiometricRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/f;->a(Ljava/security/Signature;Landroid/hardware/biometrics/BiometricPrompt;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/q<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/f;

.field final synthetic b:Landroid/hardware/biometrics/BiometricPrompt;

.field final synthetic c:Ljava/security/Signature;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/f;Landroid/hardware/biometrics/BiometricPrompt;Ljava/security/Signature;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/f$a;->a:Lcom/swedbank/mobile/data/biometric/f;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/f$a;->b:Landroid/hardware/biometrics/BiometricPrompt;

    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/f$a;->c:Ljava/security/Signature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/p;)V
    .locals 5
    .param p1    # Lio/reactivex/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    .line 165
    new-instance v1, Lcom/swedbank/mobile/data/biometric/f$a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/biometric/f$a$a;-><init>(Lcom/swedbank/mobile/data/biometric/f$a;Lio/reactivex/p;)V

    .line 198
    iget-object v2, p0, Lcom/swedbank/mobile/data/biometric/f$a;->b:Landroid/hardware/biometrics/BiometricPrompt;

    .line 199
    new-instance v3, Landroid/hardware/biometrics/BiometricPrompt$CryptoObject;

    iget-object v4, p0, Lcom/swedbank/mobile/data/biometric/f$a;->c:Ljava/security/Signature;

    invoke-direct {v3, v4}, Landroid/hardware/biometrics/BiometricPrompt$CryptoObject;-><init>(Ljava/security/Signature;)V

    .line 201
    iget-object v4, p0, Lcom/swedbank/mobile/data/biometric/f$a;->a:Lcom/swedbank/mobile/data/biometric/f;

    invoke-static {v4}, Lcom/swedbank/mobile/data/biometric/f;->c(Lcom/swedbank/mobile/data/biometric/f;)Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Application;->getMainExecutor()Ljava/util/concurrent/Executor;

    move-result-object v4

    .line 202
    check-cast v1, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;

    .line 198
    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/hardware/biometrics/BiometricPrompt;->authenticate(Landroid/hardware/biometrics/BiometricPrompt$CryptoObject;Landroid/os/CancellationSignal;Ljava/util/concurrent/Executor;Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;)V

    .line 203
    new-instance v1, Lcom/swedbank/mobile/data/biometric/f$a$1;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/data/biometric/f$a$1;-><init>(Landroid/os/CancellationSignal;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/data/biometric/h;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/biometric/h;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/f;

    invoke-interface {p1, v0}, Lio/reactivex/p;->a(Lio/reactivex/c/f;)V

    return-void
.end method
