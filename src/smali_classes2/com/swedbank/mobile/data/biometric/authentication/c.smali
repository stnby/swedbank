.class public interface abstract Lcom/swedbank/mobile/data/biometric/authentication/c;
.super Ljava/lang/Object;
.source "BiometricAuthenticationService.kt"


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;",
            ")",
            "Lio/reactivex/w<",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/authentication/finalize"
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Lio/reactivex/w;
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/s;
            a = "customerId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricChallengeResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/f;
        a = "/authentication/last/{customerId}"
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;",
            ")",
            "Lio/reactivex/w<",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/signing/finalize"
    .end annotation
.end method
