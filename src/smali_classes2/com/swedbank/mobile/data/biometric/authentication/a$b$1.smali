.class final Lcom/swedbank/mobile/data/biometric/authentication/a$b$1;
.super Ljava/lang/Object;
.source "BiometricAuthenticationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/authentication/a$b;->a(Lkotlin/k;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b$1;->a:Lcom/swedbank/mobile/business/authentication/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/authentication/m;)Lcom/swedbank/mobile/business/authentication/k;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/m$c;->a:Lcom/swedbank/mobile/business/biometric/authentication/m$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b$1;->a:Lcom/swedbank/mobile/business/authentication/k;

    const-string v0, "authenticationProgress"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 54
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$a;->a()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/authentication/k$b;

    new-instance v2, Lcom/swedbank/mobile/business/authentication/k$d$a;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/business/authentication/k$d$a;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, v2, v1}, Lcom/swedbank/mobile/business/authentication/k$b;-><init>(Ljava/lang/Throwable;Ljava/util/List;ILkotlin/e/b/g;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    goto :goto_0

    .line 55
    :cond_1
    new-instance p1, Lcom/swedbank/mobile/business/authentication/k$b;

    const/4 v0, 0x3

    invoke-direct {p1, v1, v1, v0, v1}, Lcom/swedbank/mobile/business/authentication/k$b;-><init>(Ljava/lang/Throwable;Ljava/util/List;ILkotlin/e/b/g;)V

    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    goto :goto_0

    .line 56
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    if-eqz v0, :cond_3

    .line 57
    new-instance v0, Lcom/swedbank/mobile/business/authentication/k$b;

    new-instance v2, Lcom/swedbank/mobile/business/biometric/authentication/n;

    .line 58
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$b;->a()Ljava/lang/String;

    move-result-object v3

    .line 59
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$b;->b()Ljava/lang/String;

    move-result-object p1

    .line 57
    invoke-direct {v2, v3, p1}, Lcom/swedbank/mobile/business/biometric/authentication/n;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    const/4 p1, 0x2

    invoke-direct {v0, v2, v1, p1, v1}, Lcom/swedbank/mobile/business/authentication/k$b;-><init>(Ljava/lang/Throwable;Ljava/util/List;ILkotlin/e/b/g;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/biometric/authentication/a$b$1;->a(Lcom/swedbank/mobile/business/biometric/authentication/m;)Lcom/swedbank/mobile/business/authentication/k;

    move-result-object p1

    return-object p1
.end method
