.class public final Lcom/swedbank/mobile/data/biometric/authentication/a;
.super Ljava/lang/Object;
.source "BiometricAuthenticationRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/j;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/l;

.field private final b:Lcom/swedbank/mobile/business/biometric/authentication/k;

.field private final c:Lcom/swedbank/mobile/business/customer/k;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/l;Lcom/swedbank/mobile/business/biometric/authentication/k;Lcom/swedbank/mobile/business/customer/k;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/authentication/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/customer/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthorizationRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localCustomerRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/authentication/a;->b:Lcom/swedbank/mobile/business/biometric/authentication/k;

    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/authentication/a;->c:Lcom/swedbank/mobile/business/customer/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/biometric/authentication/a;)Lcom/swedbank/mobile/business/customer/k;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/authentication/a;->c:Lcom/swedbank/mobile/business/customer/k;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/biometric/authentication/a;)Lcom/swedbank/mobile/business/biometric/authentication/k;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/authentication/a;->b:Lcom/swedbank/mobile/business/biometric/authentication/k;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/security/Signature;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mobileAppId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/authentication/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    .line 28
    invoke-interface {v0, p1, p2}, Lcom/swedbank/mobile/business/authentication/l;->c(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 31
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/biometric/authentication/a$a;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 39
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;

    invoke-direct {v0, p0, p2, p3}, Lcom/swedbank/mobile/data/biometric/authentication/a$b;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/a;Ljava/lang/String;Ljava/security/Signature;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "authenticationRepository\u2026ogress)\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
