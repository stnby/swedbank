.class final Lcom/swedbank/mobile/data/biometric/authentication/d$c;
.super Ljava/lang/Object;
.source "BiometricAuthorizationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/authentication/d;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/data/biometric/authentication/d$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/d$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/data/biometric/authentication/d$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/data/biometric/authentication/d$c;->a:Lcom/swedbank/mobile/data/biometric/authentication/d$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lretrofit2/q;)Lcom/swedbank/mobile/business/biometric/authentication/l;
    .locals 1
    .param p1    # Lretrofit2/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricChallengeResponse;",
            ">;)",
            "Lcom/swedbank/mobile/business/biometric/authentication/l;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p1}, Lretrofit2/q;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lretrofit2/q;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 112
    check-cast p1, Lcom/swedbank/mobile/data/biometric/authentication/BiometricChallengeResponse;

    .line 78
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/biometric/authentication/BiometricChallengeResponse;->a()Lcom/swedbank/mobile/business/biometric/authentication/l$b;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/l;

    goto :goto_0

    .line 117
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 79
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/biometric/authentication/l$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/l$a;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/l;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lretrofit2/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/biometric/authentication/d$c;->a(Lretrofit2/q;)Lcom/swedbank/mobile/business/biometric/authentication/l;

    move-result-object p1

    return-object p1
.end method
