.class public final Lcom/swedbank/mobile/data/biometric/authentication/d;
.super Ljava/lang/Object;
.source "BiometricAuthorizationRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/k;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/d/a;

.field private final b:Lcom/swedbank/mobile/data/biometric/authentication/c;

.field private final c:Lcom/swedbank/mobile/data/network/aa;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/data/biometric/authentication/c;Lcom/swedbank/mobile/data/network/aa;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/biometric/authentication/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/data/network/aa;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometrics"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cryptoRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationService"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "responseConverter"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->a:Lcom/swedbank/mobile/business/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->b:Lcom/swedbank/mobile/data/biometric/authentication/c;

    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->c:Lcom/swedbank/mobile/data/network/aa;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/biometric/authentication/d;)Lcom/swedbank/mobile/data/biometric/authentication/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->b:Lcom/swedbank/mobile/data/biometric/authentication/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/data/biometric/authentication/d;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->a:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/data/biometric/authentication/d;)Lcom/swedbank/mobile/data/network/aa;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->c:Lcom/swedbank/mobile/data/network/aa;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/authentication/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d;->b:Lcom/swedbank/mobile/data/biometric/authentication/c;

    .line 73
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/data/biometric/authentication/c;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 74
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 75
    sget-object v0, Lcom/swedbank/mobile/data/biometric/authentication/d$c;->a:Lcom/swedbank/mobile/data/biometric/authentication/d$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "biometricAuthenticationS\u2026t.Error\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/security/Signature;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/authentication/m;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "mobileAppId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challenge"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;

    invoke-direct {v0, p0, p4, p3}, Lcom/swedbank/mobile/data/biometric/authentication/d$d;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/d;Ljava/security/Signature;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p3

    const-string v0, "Single.fromCallable {\n  \u2026etOrHandle(::throwIt)\n  }"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;

    invoke-direct {v0, p0, p2, p4, p1}, Lcom/swedbank/mobile/data/biometric/authentication/d$a;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/d;Ljava/lang/String;Ljava/security/Signature;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p3, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "signWithSignature(\n     \u2026{ it.toResult() }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/security/Signature;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/authentication/m;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "mobileAppId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challenge"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;

    invoke-direct {v0, p0, p4, p3}, Lcom/swedbank/mobile/data/biometric/authentication/d$d;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/d;Ljava/security/Signature;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p3

    const-string v0, "Single.fromCallable {\n  \u2026etOrHandle(::throwIt)\n  }"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/d$b;

    invoke-direct {v0, p0, p2, p4, p1}, Lcom/swedbank/mobile/data/biometric/authentication/d$b;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/d;Ljava/lang/String;Ljava/security/Signature;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p3, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "signWithSignature(\n     \u2026{ it.toResult() }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
