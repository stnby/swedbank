.class final Lcom/swedbank/mobile/data/biometric/authentication/d$a;
.super Ljava/lang/Object;
.source "BiometricAuthorizationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/authentication/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/authentication/d;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/security/Signature;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/authentication/d;Ljava/lang/String;Ljava/security/Signature;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->a:Lcom/swedbank/mobile/data/biometric/authentication/d;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->c:Ljava/security/Signature;

    iput-object p4, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/authentication/m;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "signedChallenge"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->a:Lcom/swedbank/mobile/data/biometric/authentication/d;

    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/authentication/d;->a(Lcom/swedbank/mobile/data/biometric/authentication/d;)Lcom/swedbank/mobile/data/biometric/authentication/c;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->b:Ljava/lang/String;

    .line 45
    iget-object v2, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->c:Ljava/security/Signature;

    .line 113
    :try_start_0
    invoke-virtual {v2}, Ljava/security/Signature;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    const-string v3, "algorithm"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;->valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    :catch_0
    sget-object v2, Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;->SHA512withECDSA:Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;

    .line 46
    :goto_0
    iget-object v3, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->d:Ljava/lang/String;

    .line 42
    new-instance v4, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;

    invoke-direct {v4, v1, p1, v2, v3}, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/ordering/SigningHashAlgorithm;Ljava/lang/String;)V

    invoke-interface {v0, v4}, Lcom/swedbank/mobile/data/biometric/authentication/c;->a(Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationFinalizationInput;)Lio/reactivex/w;

    move-result-object p1

    .line 48
    invoke-static {p1}, Lcom/swedbank/mobile/data/network/ab;->a(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object p1

    .line 49
    new-instance v0, Lcom/swedbank/mobile/data/biometric/authentication/d$a$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/biometric/authentication/d$a$1;-><init>(Lcom/swedbank/mobile/data/biometric/authentication/d$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
