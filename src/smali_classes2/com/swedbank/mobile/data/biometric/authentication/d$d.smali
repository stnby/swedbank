.class public final Lcom/swedbank/mobile/data/biometric/authentication/d$d;
.super Ljava/lang/Object;
.source "BiometricAuthorizationRepositoryImpl.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/data/biometric/authentication/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/authentication/d;

.field final synthetic b:Ljava/security/Signature;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/biometric/authentication/d;Ljava/security/Signature;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->a:Lcom/swedbank/mobile/data/biometric/authentication/d;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->b:Ljava/security/Signature;

    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->a:Lcom/swedbank/mobile/data/biometric/authentication/d;

    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/authentication/d;->b(Lcom/swedbank/mobile/data/biometric/authentication/d;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->b:Ljava/security/Signature;

    iget-object v2, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/d/a;->a(Ljava/security/Signature;Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 114
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    .line 112
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 116
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    .line 112
    check-cast v0, Ljava/lang/Throwable;

    .line 117
    throw v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/biometric/authentication/d$d;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
