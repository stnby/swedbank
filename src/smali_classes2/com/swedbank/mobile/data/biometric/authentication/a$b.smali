.class final Lcom/swedbank/mobile/data/biometric/authentication/a$b;
.super Ljava/lang/Object;
.source "BiometricAuthenticationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/authentication/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/authentication/a;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/security/Signature;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/authentication/a;Ljava/lang/String;Ljava/security/Signature;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->a:Lcom/swedbank/mobile/data/biometric/authentication/a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->c:Ljava/security/Signature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/w;
    .locals 5
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/biometric/authentication/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/l;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    .line 41
    instance-of v1, v0, Lcom/swedbank/mobile/business/biometric/authentication/l$b;

    if-eqz v1, :cond_2

    .line 42
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/k$c;

    if-eqz v1, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/biometric/authentication/l$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/biometric/authentication/l$b;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    check-cast v3, Lcom/swedbank/mobile/business/authentication/k$c;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/authentication/k$c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->a:Lcom/swedbank/mobile/data/biometric/authentication/a;

    invoke-static {v1}, Lcom/swedbank/mobile/data/biometric/authentication/a;->b(Lcom/swedbank/mobile/data/biometric/authentication/a;)Lcom/swedbank/mobile/business/biometric/authentication/k;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->b:Ljava/lang/String;

    .line 47
    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/l$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/authentication/l$b;->a()Ljava/lang/String;

    move-result-object v3

    .line 48
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/authentication/l$b;->b()Ljava/lang/String;

    move-result-object v0

    .line 49
    iget-object v4, p0, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->c:Ljava/security/Signature;

    .line 45
    invoke-interface {v1, v2, v3, v0, v4}, Lcom/swedbank/mobile/business/biometric/authentication/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/swedbank/mobile/data/biometric/authentication/a$b$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/data/biometric/authentication/a$b$1;-><init>(Lcom/swedbank/mobile/business/authentication/k;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "biometricAuthorizationRe\u2026        }\n              }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 65
    :cond_1
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(authenticationProgress)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 43
    :cond_2
    :goto_0
    new-instance p1, Lcom/swedbank/mobile/business/authentication/k$b;

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p1, v1, v1, v0, v1}, Lcom/swedbank/mobile/business/authentication/k$b;-><init>(Ljava/lang/Throwable;Ljava/util/List;ILkotlin/e/b/g;)V

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(AuthenticationProgress.Failed())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/biometric/authentication/a$b;->a(Lkotlin/k;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
