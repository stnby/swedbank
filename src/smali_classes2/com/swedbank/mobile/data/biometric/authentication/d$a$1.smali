.class final Lcom/swedbank/mobile/data/biometric/authentication/d$a$1;
.super Ljava/lang/Object;
.source "BiometricAuthorizationRepositoryImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/authentication/d$a;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/authentication/d$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/authentication/d$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a$1;->a:Lcom/swedbank/mobile/data/biometric/authentication/d$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lretrofit2/q;)Lcom/swedbank/mobile/business/biometric/authentication/m;
    .locals 3
    .param p1    # Lretrofit2/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/q<",
            "Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;",
            ">;)",
            "Lcom/swedbank/mobile/business/biometric/authentication/m;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/authentication/d$a$1;->a:Lcom/swedbank/mobile/data/biometric/authentication/d$a;

    iget-object v0, v0, Lcom/swedbank/mobile/data/biometric/authentication/d$a;->a:Lcom/swedbank/mobile/data/biometric/authentication/d;

    .line 113
    invoke-virtual {p1}, Lretrofit2/q;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    invoke-virtual {p1}, Lretrofit2/q;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;

    .line 120
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x9dc

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    sget-object p1, Lcom/swedbank/mobile/business/biometric/authentication/m$c;->a:Lcom/swedbank/mobile/business/biometric/authentication/m$c;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    goto :goto_1

    .line 122
    :cond_1
    :goto_0
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$a;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    goto :goto_1

    .line 117
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Successful response body is empty"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 125
    :cond_3
    invoke-virtual {p1}, Lretrofit2/q;->a()I

    move-result v1

    const/16 v2, 0x19c

    if-eq v1, v2, :cond_4

    .line 153
    new-instance p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, v1, v0, v1}, Lcom/swedbank/mobile/business/biometric/authentication/m$a;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    goto :goto_1

    .line 126
    :cond_4
    invoke-virtual {p1}, Lretrofit2/q;->f()Lokhttp3/ad;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 135
    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/authentication/d;->c(Lcom/swedbank/mobile/data/biometric/authentication/d;)Lcom/swedbank/mobile/data/network/aa;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/aa;->b()Lretrofit2/r;

    move-result-object v1

    .line 139
    const-class v2, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;

    check-cast v2, Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/aa;->a()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lretrofit2/r;->b(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lretrofit2/f;

    move-result-object v0

    .line 138
    invoke-interface {v0, p1}, Lretrofit2/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 135
    check-cast p1, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;

    .line 148
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    .line 149
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;->a()Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/biometric/authentication/BiometricAuthenticationResponse;->b()Ljava/lang/String;

    move-result-object p1

    .line 148
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    :goto_1
    return-object p1

    .line 145
    :cond_5
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 132
    :cond_6
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Required value was null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lretrofit2/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/biometric/authentication/d$a$1;->a(Lretrofit2/q;)Lcom/swedbank/mobile/business/biometric/authentication/m;

    move-result-object p1

    return-object p1
.end method
