.class public final Lcom/swedbank/mobile/data/biometric/g;
.super Ljava/lang/Object;
.source "BiometricRepositoryImpl.kt"


# static fields
.field private static final a:Ljava/lang/String; = "android.hardware.iris"

.field private static final b:Ljava/lang/String; = "android.hardware.face"

.field private static final c:Ljava/lang/String; = "com.samsung.android.camera.iris"

.field private static final d:[Lkotlin/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/biometric/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    .line 39
    new-array v0, v0, [Lkotlin/k;

    const-string v1, "android.hardware.fingerprint"

    .line 40
    sget-object v2, Lcom/swedbank/mobile/business/biometric/c;->a:Lcom/swedbank/mobile/business/biometric/c;

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 41
    sget-object v1, Lcom/swedbank/mobile/data/biometric/g;->a:Ljava/lang/String;

    sget-object v2, Lcom/swedbank/mobile/business/biometric/c;->b:Lcom/swedbank/mobile/business/biometric/c;

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 42
    sget-object v1, Lcom/swedbank/mobile/data/biometric/g;->b:Ljava/lang/String;

    sget-object v2, Lcom/swedbank/mobile/business/biometric/c;->c:Lcom/swedbank/mobile/business/biometric/c;

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 43
    sget-object v1, Lcom/swedbank/mobile/data/biometric/g;->c:Ljava/lang/String;

    sget-object v2, Lcom/swedbank/mobile/business/biometric/c;->b:Lcom/swedbank/mobile/business/biometric/c;

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 39
    sput-object v0, Lcom/swedbank/mobile/data/biometric/g;->d:[Lkotlin/k;

    return-void
.end method

.method public static final synthetic a()[Lkotlin/k;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/data/biometric/g;->d:[Lkotlin/k;

    return-object v0
.end method
