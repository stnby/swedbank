.class public final Lcom/swedbank/mobile/data/biometric/f$a$a;
.super Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;
.source "BiometricRepositoryImpl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/biometric/f$a;->a(Lio/reactivex/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/biometric/f$a;

.field final synthetic b:Lio/reactivex/p;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/data/biometric/f$a;Lio/reactivex/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/p;",
            ")V"
        }
    .end annotation

    .line 165
    iput-object p1, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->a:Lcom/swedbank/mobile/data/biometric/f$a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    invoke-direct {p0}, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthenticationError(ILjava/lang/CharSequence;)V
    .locals 2
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 189
    invoke-super {p0, p1, p2}, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;->onAuthenticationError(ILjava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    .line 191
    iget-object v1, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->a:Lcom/swedbank/mobile/data/biometric/f$a;

    iget-object v1, v1, Lcom/swedbank/mobile/data/biometric/f$a;->a:Lcom/swedbank/mobile/data/biometric/f;

    packed-switch p1, :pswitch_data_0

    .line 238
    :pswitch_0
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->b:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 237
    :pswitch_1
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->c:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 236
    :pswitch_2
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->g:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 235
    :pswitch_3
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->k:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 234
    :pswitch_4
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->f:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 233
    :pswitch_5
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->l:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 232
    :pswitch_6
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->e:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 231
    :pswitch_7
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->b:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 230
    :pswitch_8
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->h:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 229
    :pswitch_9
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->i:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 228
    :pswitch_a
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->j:Lcom/swedbank/mobile/business/authentication/p$b;

    goto :goto_0

    .line 227
    :pswitch_b
    sget-object p1, Lcom/swedbank/mobile/business/authentication/p$b;->d:Lcom/swedbank/mobile/business/authentication/p$b;

    .line 190
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/biometric/a$a;

    invoke-direct {v1, p1, p2}, Lcom/swedbank/mobile/business/biometric/a$a;-><init>(Lcom/swedbank/mobile/business/authentication/p$b;Ljava/lang/CharSequence;)V

    invoke-interface {v0, v1}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    .line 194
    iget-object p1, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    invoke-interface {p1}, Lio/reactivex/p;->c()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onAuthenticationFailed()V
    .locals 3

    .line 183
    invoke-super {p0}, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;->onAuthenticationFailed()V

    .line 184
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/a$c;

    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v2, Lcom/swedbank/mobile/business/util/l;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/biometric/a$c;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    invoke-interface {v0, v1}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .locals 2
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 175
    invoke-super {p0, p1, p2}, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;->onAuthenticationHelp(ILjava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/a$c;

    .line 177
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 176
    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/biometric/a$c;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    invoke-interface {v0, v1}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public onAuthenticationSucceeded(Landroid/hardware/biometrics/BiometricPrompt$AuthenticationResult;)V
    .locals 2
    .param p1    # Landroid/hardware/biometrics/BiometricPrompt$AuthenticationResult;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-super {p0, p1}, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationCallback;->onAuthenticationSucceeded(Landroid/hardware/biometrics/BiometricPrompt$AuthenticationResult;)V

    .line 169
    iget-object v0, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    invoke-virtual {p1}, Landroid/hardware/biometrics/BiometricPrompt$AuthenticationResult;->getCryptoObject()Landroid/hardware/biometrics/BiometricPrompt$CryptoObject;

    move-result-object p1

    const-string v1, "result.cryptoObject"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/hardware/biometrics/BiometricPrompt$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/biometric/a$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/biometric/a$b;-><init>(Ljava/security/Signature;)V

    invoke-interface {v0, v1}, Lio/reactivex/p;->a(Ljava/lang/Object;)V

    .line 170
    iget-object p1, p0, Lcom/swedbank/mobile/data/biometric/f$a$a;->b:Lio/reactivex/p;

    invoke-interface {p1}, Lio/reactivex/p;->c()V

    return-void

    .line 169
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
