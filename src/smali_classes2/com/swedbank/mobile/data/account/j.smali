.class public final Lcom/swedbank/mobile/data/account/j;
.super Ljava/lang/Object;
.source "LocalAccountRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/a/e;


# instance fields
.field private final a:Lcom/a/a/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/a/a/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/a/a/a/f;)V
    .locals 2
    .param p1    # Lcom/a/a/a/f;
        .annotation runtime Ljavax/inject/Named;
            value = "app_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "sharedAccountIdPreference"

    const-string v1, ""

    .line 29
    invoke-virtual {p1, v0, v1}, Lcom/a/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object p1

    const-string v0, "appPreferences.getString\u2026EF_SHARED_ACCOUNT_ID, \"\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/j;->a:Lcom/a/a/a/d;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/siimkinks/sqlitemagic/d;->a:Lcom/siimkinks/sqlitemagic/d;

    const-string v2, "ACCOUNT_SUMMARY_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$c;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object v0

    .line 73
    sget-object v1, Lcom/swedbank/mobile/data/account/j$b;->a:Lcom/swedbank/mobile/data/account/j$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "(SELECT\n          FROM A\u2026countSummary)\n          }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/util/List;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 58
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 59
    sget-object v1, Lcom/siimkinks/sqlitemagic/e;->a:Lcom/siimkinks/sqlitemagic/e;

    const-string v2, "ACCOUNT_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 60
    sget-object v1, Lcom/siimkinks/sqlitemagic/e;->a:Lcom/siimkinks/sqlitemagic/e;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/e;->b:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_VIEW.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->a(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`in`(values)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 63
    sget-object v0, Lcom/swedbank/mobile/data/account/j$e;->a:Lcom/swedbank/mobile/data/account/j$e;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n        FROM ACC\u2026iew::toAccount)\n        }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Account ids cannot be empty"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 116
    sget-object v1, Lcom/siimkinks/sqlitemagic/a;->a:Lcom/siimkinks/sqlitemagic/a;

    const-string v2, "ACCOUNT_BALANCE_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 117
    sget-object v1, Lcom/siimkinks/sqlitemagic/a;->a:Lcom/siimkinks/sqlitemagic/a;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/a;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_BALANCE_VIEW.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget-object v1, Lcom/siimkinks/sqlitemagic/a;->a:Lcom/siimkinks/sqlitemagic/a;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/a;->b:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_BALANCE_VIEW.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v1, p2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p2

    const-string v1, "this.`is`(value)"

    invoke-static {p2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string p2, "this.and(expr)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 120
    sget-object p2, Lcom/swedbank/mobile/data/account/j$g;->a:Lcom/swedbank/mobile/data/account/j$g;

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "(SELECT\n          FROM A\u2026w::toBalance)\n          }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 33
    sget-object v1, Lcom/siimkinks/sqlitemagic/e;->a:Lcom/siimkinks/sqlitemagic/e;

    const-string v2, "ACCOUNT_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 34
    sget-object v1, Lcom/siimkinks/sqlitemagic/e;->a:Lcom/siimkinks/sqlitemagic/e;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/e;->b:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_VIEW.ACCOUNT_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->b()Lcom/siimkinks/sqlitemagic/ad;

    move-result-object p1

    .line 36
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ad;->b()Lcom/siimkinks/sqlitemagic/cg;

    move-result-object p1

    .line 37
    sget-object v0, Lcom/swedbank/mobile/data/account/j$a;->a:Lcom/swedbank/mobile/data/account/j$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/cg;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM A\u2026oObservable()\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/siimkinks/sqlitemagic/e;->a:Lcom/siimkinks/sqlitemagic/e;

    const-string v2, "ACCOUNT_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/siimkinks/sqlitemagic/e;->a:Lcom/siimkinks/sqlitemagic/e;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/e;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_VIEW.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 50
    sget-object v0, Lcom/swedbank/mobile/data/account/j$d;->a:Lcom/swedbank/mobile/data/account/j$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM A\u2026w::toAccount)\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public e(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 79
    sget-object v1, Lcom/siimkinks/sqlitemagic/d;->a:Lcom/siimkinks/sqlitemagic/d;

    const-string v2, "ACCOUNT_SUMMARY_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/siimkinks/sqlitemagic/d;->a:Lcom/siimkinks/sqlitemagic/d;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/d;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_SUMMARY_VIEW.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 83
    sget-object v0, Lcom/swedbank/mobile/data/account/j$c;->a:Lcom/swedbank/mobile/data/account/j$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM A\u2026countSummary)\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public f(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 89
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v2, "ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_DATA.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->c()Lcom/siimkinks/sqlitemagic/ab;

    move-result-object p1

    .line 92
    invoke-interface {p1}, Lcom/siimkinks/sqlitemagic/ab;->b()Lcom/siimkinks/sqlitemagic/al;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/al;->b()Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM A\u2026e()\n          .runQuery()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public g(Ljava/lang/String;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    .line 106
    sget-object v1, Lcom/siimkinks/sqlitemagic/a;->a:Lcom/siimkinks/sqlitemagic/a;

    const-string v2, "ACCOUNT_BALANCE_VIEW"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    .line 107
    sget-object v1, Lcom/siimkinks/sqlitemagic/a;->a:Lcom/siimkinks/sqlitemagic/a;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/a;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_BALANCE_VIEW.CUSTOMER_ID"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v1, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v1, "this.`is`(value)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/cb$h;->e()Lcom/siimkinks/sqlitemagic/bj;

    move-result-object p1

    .line 109
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/bj;->a()Lio/reactivex/o;

    move-result-object p1

    .line 110
    sget-object v0, Lcom/swedbank/mobile/data/account/j$f;->a:Lcom/swedbank/mobile/data/account/j$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "(SELECT\n          FROM A\u2026w::toBalance)\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
