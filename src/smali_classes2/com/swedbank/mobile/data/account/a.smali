.class public final Lcom/swedbank/mobile/data/account/a;
.super Ljava/lang/Object;
.source "AccountData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/account/a$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ae<",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/account/a$a;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/data/account/i;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/swedbank/mobile/data/account/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/account/a$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/a;->b:Lcom/swedbank/mobile/data/account/a$a;

    .line 131
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    const/4 v1, 0x4

    .line 133
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/x;

    .line 134
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "account_id"

    .line 198
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 135
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "customer_id"

    .line 199
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 136
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->h:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "main_currency"

    .line 200
    invoke-virtual {v2, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const/4 v2, 0x3

    .line 137
    sget-object v6, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    invoke-virtual {v6}, Lcom/siimkinks/sqlitemagic/j;->a()Lcom/siimkinks/sqlitemagic/x;

    move-result-object v6

    aput-object v6, v1, v2

    .line 132
    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;

    move-result-object v0

    .line 138
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v2, "ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 201
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026        FROM ACCOUNT_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    sget-object v1, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v2, "BALANCE_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v6, "ACCOUNT_DATA.ACCOUNT_ID"

    invoke-static {v2, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    sget-object v6, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v6, v6, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 202
    invoke-virtual {v2, v6}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v6, "this.`is`(column)"

    invoke-static {v2, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bi;

    move-result-object v1

    const-string v2, "this.on(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bi;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026BALANCE_DATA.ACCOUNT_ID))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    new-array v1, v5, [Lcom/siimkinks/sqlitemagic/cb$f;

    .line 142
    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "BALANCE_DATA.CURRENCY"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/b;->h:Lcom/siimkinks/sqlitemagic/x;

    .line 205
    invoke-virtual {v2, v5}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v5, "this.`is`(column)"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/bb;->a()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v5, "(BALANCE_DATA.CURRENCY I\u2026ATA.MAIN_CURRENCY).desc()"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v2, v1, v3

    .line 143
    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/x;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v2

    const-string v3, "BALANCE_DATA.CURRENCY.asc()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v2, v1, v4

    .line 206
    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/siimkinks/sqlitemagic/cb$f;

    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$e;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    const-string v1, "(SELECT\n        COLUMNS\n\u2026sc()))\n        .compile()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/a;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void

    .line 200
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/account/i;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/data/account/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainCurrency"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balance"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/a;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/a;->d:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/account/a;->e:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/data/account/a;->f:Lcom/swedbank/mobile/data/account/i;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/a/d;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/a;->f:Lcom/swedbank/mobile/data/account/i;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/data/account/i;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/a/d;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/account/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/account/a;

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/a;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/account/a;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/a;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/account/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/a;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/account/a;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/a;->f:Lcom/swedbank/mobile/data/account/i;

    iget-object p1, p1, Lcom/swedbank/mobile/data/account/a;->f:Lcom/swedbank/mobile/data/account/i;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/a;->c:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/a;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/a;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/a;->f:Lcom/swedbank/mobile/data/account/i;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountBalanceView(accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", customerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mainCurrency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/a;->f:Lcom/swedbank/mobile/data/account/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
