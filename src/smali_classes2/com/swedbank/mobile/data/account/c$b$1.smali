.class public final Lcom/swedbank/mobile/data/account/c$b$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/account/c$b;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/account/c$b;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/account/c$b;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/c$b$1;->a:Lcom/swedbank/mobile/data/account/c$b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/c$b$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c$b$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/account/AccountResponse;

    if-eqz v0, :cond_0

    .line 237
    iget-object v1, p0, Lcom/swedbank/mobile/data/account/c$b$1;->a:Lcom/swedbank/mobile/data/account/c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/data/account/c$b;->a:Lcom/swedbank/mobile/data/account/c;

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/data/account/c;->a(Lcom/swedbank/mobile/data/account/AccountResponse;)V

    goto :goto_0

    .line 240
    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v0

    const-string v1, "SqliteMagic.newTransaction()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 247
    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v3, "BALANCE_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 248
    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "BALANCE_DATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/swedbank/mobile/data/account/c$b$1;->a:Lcom/swedbank/mobile/data/account/c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/data/account/c$b;->b:Ljava/lang/String;

    .line 249
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v1

    .line 246
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 251
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 255
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v3, "ACCOUNT_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 256
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v3, "ACCOUNT_DATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    iget-object v3, p0, Lcom/swedbank/mobile/data/account/c$b$1;->a:Lcom/swedbank/mobile/data/account/c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/data/account/c$b;->b:Ljava/lang/String;

    .line 257
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(value)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 258
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v1

    .line 254
    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 260
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    :goto_0
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v1

    .line 262
    invoke-interface {v0}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/c$b$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
