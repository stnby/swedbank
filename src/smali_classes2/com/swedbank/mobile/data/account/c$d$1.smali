.class public final Lcom/swedbank/mobile/data/account/c$d$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/account/c$d;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/account/c$d;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/account/c$d;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/c$d$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 23
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v1, p0

    .line 80
    iget-object v0, v1, Lcom/swedbank/mobile/data/account/c$d$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/account/AccountBalancesResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v2

    const-string v3, "SqliteMagic.newTransaction()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v3

    .line 245
    sget-object v4, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v5, "ACCOUNT_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v3

    .line 246
    sget-object v4, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/b;->h:Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/AccountBalancesResponse;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v4

    .line 247
    invoke-virtual {v4}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {v4}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object v3

    const-string v4, "UPDATE\n              TAB\u2026CURRENCY to mainCurrency)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    sget-object v4, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "ACCOUNT_DATA.CUSTOMER_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v5, v5, Lcom/swedbank/mobile/data/account/c$d;->b:Ljava/lang/String;

    .line 249
    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.`is`(value)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    sget-object v5, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v6, "ACCOUNT_DATA.ACCOUNT_ID"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    iget-object v6, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v6, v6, Lcom/swedbank/mobile/data/account/c$d;->c:Ljava/lang/String;

    .line 250
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v5

    const-string v6, "this.`is`(value)"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.and(expr)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object v3

    .line 244
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    .line 253
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v3

    .line 258
    sget-object v4, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v5, "BALANCE_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v3

    .line 259
    sget-object v4, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "BALANCE_DATA.ACCOUNT_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v5, v5, Lcom/swedbank/mobile/data/account/c$d;->c:Ljava/lang/String;

    .line 260
    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.`is`(value)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    sget-object v5, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/j;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v6, "BALANCE_DATA.CUSTOMER_ID"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v6, v6, Lcom/swedbank/mobile/data/account/c$d;->b:Ljava/lang/String;

    .line 262
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v5

    const-string v6, "this.`is`(value)"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v4, v5}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.and(expr)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v3

    .line 257
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 265
    sget-object v3, Lcom/siimkinks/sqlitemagic/k;->a:Lcom/siimkinks/sqlitemagic/k;

    .line 273
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/AccountBalancesResponse;->b()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 274
    iget-object v4, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v4, v4, Lcom/swedbank/mobile/data/account/c$d;->a:Lcom/swedbank/mobile/data/account/c;

    .line 276
    iget-object v4, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v4, v4, Lcom/swedbank/mobile/data/account/c$d;->b:Ljava/lang/String;

    .line 277
    iget-object v6, v1, Lcom/swedbank/mobile/data/account/c$d$1;->a:Lcom/swedbank/mobile/data/account/c$d;

    iget-object v15, v6, Lcom/swedbank/mobile/data/account/c$d;->c:Ljava/lang/String;

    .line 278
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/AccountBalancesResponse;->b()Ljava/util/List;

    move-result-object v6

    .line 282
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/AccountBalancesResponse;->a()Ljava/lang/String;

    move-result-object v10

    .line 290
    move-object v0, v6

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v5

    if-eqz v0, :cond_5

    .line 291
    check-cast v6, Ljava/lang/Iterable;

    .line 292
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 293
    move-object v7, v3

    check-cast v7, Ljava/util/List;

    move-object v14, v7

    check-cast v14, Ljava/util/Collection;

    check-cast v6, Lcom/swedbank/mobile/data/account/BalanceResponse;

    .line 294
    new-instance v13, Lcom/swedbank/mobile/data/account/i;

    const/4 v7, 0x0

    .line 297
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->a()Ljava/lang/String;

    move-result-object v10

    .line 298
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->b()Ljava/math/BigDecimal;

    move-result-object v11

    .line 299
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v8

    if-eqz v8, :cond_0

    :goto_1
    move-object v12, v8

    goto :goto_2

    :cond_0
    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v9, "BigDecimal.ZERO"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 300
    :goto_2
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->c()Ljava/math/BigDecimal;

    move-result-object v8

    if-eqz v8, :cond_1

    :goto_3
    move-object/from16 v16, v8

    goto :goto_4

    :cond_1
    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v9, "BigDecimal.ZERO"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 301
    :goto_4
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->e()Ljava/math/BigDecimal;

    move-result-object v8

    if-eqz v8, :cond_2

    :goto_5
    move-object/from16 v17, v8

    goto :goto_6

    :cond_2
    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v9, "BigDecimal.ZERO"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_5

    .line 302
    :goto_6
    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v6

    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v6, v8}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v6

    if-lez v6, :cond_3

    const/16 v18, 0x1

    goto :goto_7

    :cond_3
    const/4 v6, 0x0

    const/16 v18, 0x0

    :goto_7
    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object v6, v13

    move-object v8, v15

    move-object v9, v4

    move-object v5, v13

    move-object/from16 v13, v16

    move-object/from16 v21, v14

    move-object/from16 v14, v17

    move-object/from16 v22, v15

    move/from16 v15, v18

    move/from16 v16, v19

    move-object/from16 v17, v20

    .line 294
    invoke-direct/range {v6 .. v17}, Lcom/swedbank/mobile/data/account/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZILkotlin/e/b/g;)V

    move-object/from16 v7, v21

    .line 302
    invoke-interface {v7, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object/from16 v15, v22

    const/4 v5, 0x1

    goto/16 :goto_0

    .line 303
    :cond_4
    move-object v0, v3

    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    goto :goto_8

    :cond_5
    move-object/from16 v22, v15

    .line 304
    move-object v0, v3

    check-cast v0, Ljava/util/List;

    new-instance v5, Lcom/swedbank/mobile/data/account/i;

    const/4 v7, 0x0

    .line 308
    sget-object v11, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v11, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    sget-object v12, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v12, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    sget-object v13, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v13, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    sget-object v14, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v14, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object v6, v5

    move-object/from16 v8, v22

    move-object v9, v4

    .line 304
    invoke-direct/range {v6 .. v17}, Lcom/swedbank/mobile/data/account/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZILkotlin/e/b/g;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    :goto_8
    check-cast v3, Ljava/lang/Iterable;

    .line 316
    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/cs$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cs$a;

    move-result-object v0

    .line 272
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cs$a;->a()Z

    .line 318
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 320
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/c$d$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
