.class public final Lcom/swedbank/mobile/data/account/c$d;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/account/c;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/account/c;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/account/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/c$d;->a:Lcom/swedbank/mobile/data/account/c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/c$d;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/account/c$d;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/network/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/w<",
            "+TT;>;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$c;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swedbank/mobile/data/account/c$d$1;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/account/c$d$1;-><init>(Lcom/swedbank/mobile/data/account/c$d;Lcom/swedbank/mobile/data/network/w;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.fromCallable {\n  \u2026QueryResult.Success\n    }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 238
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$b;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/data/network/w$b;

    .line 239
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$b;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 241
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 242
    check-cast v1, Lcom/swedbank/mobile/data/network/RequestErrorDetails;

    .line 239
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/network/RequestErrorDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 243
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 238
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    .line 236
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_1

    .line 244
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/data/network/w$a;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/data/network/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/data/network/w$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    .line 236
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 247
    :goto_1
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    .line 245
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Network response is not an error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/data/network/w;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/data/account/c$d;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
