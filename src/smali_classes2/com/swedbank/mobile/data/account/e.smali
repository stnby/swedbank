.class public interface abstract Lcom/swedbank/mobile/data/account/e;
.super Ljava/lang/Object;
.source "AccountService.kt"


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/account/AccountListResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method

.method public abstract c(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/data/network/GraphQLRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation

        .annotation runtime Lretrofit2/b/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/network/GraphQLRequest;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/account/AccountBalancesResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .annotation runtime Lretrofit2/b/o;
        a = "/graphql"
    .end annotation
.end method
