.class public final Lcom/swedbank/mobile/data/account/n;
.super Ljava/lang/Object;
.source "SqliteMagic_AccountData_Dao.java"


# direct methods
.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/b;)Lcom/swedbank/mobile/data/account/b;
    .locals 11

    .line 104
    iget v0, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 105
    iget v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p1, Lcom/siimkinks/sqlitemagic/b/b;->a:I

    .line 106
    new-instance p1, Lcom/swedbank/mobile/data/account/b;

    .line 107
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v3, v2

    goto :goto_0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v3, v1

    :goto_0
    add-int/lit8 v1, v0, 0x1

    .line 108
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v1, v0, 0x2

    .line 109
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v1, v0, 0x3

    .line 110
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v1, v0, 0x4

    .line 111
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v7, v2

    goto :goto_1

    :cond_1
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    :goto_1
    add-int/lit8 v1, v0, 0x5

    .line 112
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    add-int/lit8 v1, v0, 0x6

    .line 113
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v0, v0, 0x7

    .line 114
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v10, v2

    goto :goto_2

    :cond_2
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    move-object v10, p0

    :goto_2
    move-object v2, p1

    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/data/account/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;)V

    return-object p1
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/account/b;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/account/b;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 120
    invoke-virtual/range {p2 .. p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 122
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    return-object v3

    :cond_0
    const-string v2, "account_data"

    .line 127
    :cond_1
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_5

    .line 129
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 130
    new-instance v2, Lcom/swedbank/mobile/data/account/b;

    .line 131
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v5, v3

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v5, v4

    :goto_0
    add-int/lit8 v4, v1, 0x1

    .line 132
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v4, v1, 0x2

    .line 133
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v4, v1, 0x3

    .line 134
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v4, v1, 0x4

    .line 135
    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-eqz v9, :cond_3

    move-object v9, v3

    goto :goto_1

    :cond_3
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v9, v4

    :goto_1
    add-int/lit8 v4, v1, 0x5

    .line 136
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    add-int/lit8 v4, v1, 0x6

    .line 137
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    add-int/lit8 v1, v1, 0x7

    .line 138
    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_2
    move-object v12, v3

    goto :goto_3

    :cond_4
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_2

    :goto_3
    move-object v4, v2

    invoke-direct/range {v4 .. v12}, Lcom/swedbank/mobile/data/account/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;)V

    return-object v2

    .line 140
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".local_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 141
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".account_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    if-eqz v5, :cond_10

    .line 145
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ".customer_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_f

    .line 149
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".iban"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_e

    .line 153
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ".alias"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 154
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".is_default"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    if-eqz v9, :cond_d

    .line 158
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, ".main_currency"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_c

    .line 162
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".ordering"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 163
    new-instance v2, Lcom/swedbank/mobile/data/account/b;

    if-eqz v4, :cond_7

    .line 164
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-interface {v0, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-eqz v11, :cond_6

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v12, v4

    goto :goto_5

    :cond_7
    :goto_4
    move-object v12, v3

    .line 165
    :goto_5
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 166
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 167
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    if-eqz v8, :cond_9

    .line 168
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_8

    goto :goto_6

    :cond_8
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v16, v4

    goto :goto_7

    :cond_9
    :goto_6
    move-object/from16 v16, v3

    .line 169
    :goto_7
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    .line 170
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    if-eqz v1, :cond_b

    .line 171
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_a

    goto :goto_8

    :cond_a
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_b
    :goto_8
    move-object/from16 v19, v3

    move-object v11, v2

    invoke-direct/range {v11 .. v19}, Lcom/swedbank/mobile/data/account/b;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;)V

    return-object v2

    .line 160
    :cond_c
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"account_data\" required column \"main_currency\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_d
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"account_data\" required column \"is_default\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_e
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"account_data\" required column \"iban\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_f
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"account_data\" required column \"customer_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_10
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Selected columns did not contain table \"account_data\" required column \"account_id\""

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/swedbank/mobile/data/account/b;)Ljava/lang/Long;
    .locals 0

    .line 99
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->a()Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroidx/k/a/f;ILjava/lang/String;Lcom/swedbank/mobile/data/account/b;)V
    .locals 2

    .line 44
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x2fe1e393

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "account_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, -0x1

    :goto_1
    packed-switch p2, :pswitch_data_0

    goto :goto_2

    .line 52
    :pswitch_0
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/account/b;->b()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 46
    :pswitch_1
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/account/b;->a()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 49
    invoke-virtual {p3}, Lcom/swedbank/mobile/data/account/b;->a()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    invoke-interface {p0, p1, p2, p3}, Landroidx/k/a/f;->a(IJ)V

    goto :goto_2

    .line 47
    :cond_3
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "local_id column is null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/b;)V
    .locals 3

    .line 69
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 70
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    .line 74
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :cond_0
    const/4 v0, 0x5

    .line 76
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->f()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x6

    .line 77
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->h()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    .line 79
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->h()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    :cond_1
    return-void
.end method

.method public static a(Lcom/swedbank/mobile/data/account/b;Lcom/siimkinks/sqlitemagic/b/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/account/b;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/b/c;->b()V

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->a()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "local_id"

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->a()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "account_id"

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "customer_id"

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "iban"

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "alias"

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v0, "is_default"

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->f()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "main_currency"

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->h()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "ordering"

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/b;->h()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p1, v0, p0}, Lcom/siimkinks/sqlitemagic/b/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/swedbank/mobile/data/account/b;)Z
    .locals 4

    .line 58
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x2fe1e393

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x714bd66f

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "local_id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "account_id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 64
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " is not unique"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    return v3

    .line 60
    :pswitch_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->a()Ljava/lang/Long;

    move-result-object p0

    if-nez p0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Landroidx/k/a/f;Lcom/swedbank/mobile/data/account/b;)V
    .locals 3

    .line 84
    invoke-interface {p0}, Landroidx/k/a/f;->c()V

    .line 85
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p0, v1, v0}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    .line 89
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    :cond_0
    const/4 v0, 0x5

    .line 91
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->f()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/siimkinks/sqlitemagic/c/a;->a(Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    const/4 v0, 0x6

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroidx/k/a/f;->a(ILjava/lang/String;)V

    .line 93
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->h()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    .line 94
    invoke-virtual {p1}, Lcom/swedbank/mobile/data/account/b;->h()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-interface {p0, v0, v1, v2}, Landroidx/k/a/f;->a(IJ)V

    :cond_1
    return-void
.end method
