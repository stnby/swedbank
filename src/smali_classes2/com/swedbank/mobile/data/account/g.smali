.class public final Lcom/swedbank/mobile/data/account/g;
.super Ljava/lang/Object;
.source "AccountData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/account/g$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ae<",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/account/g$a;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final g:Z

.field private final h:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/data/customer/CustomerType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/swedbank/mobile/data/account/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/account/g$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/g;->b:Lcom/swedbank/mobile/data/account/g$a;

    .line 173
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    const/16 v1, 0x8

    .line 175
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/x;

    .line 176
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "account_id"

    .line 198
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 177
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "customer_id"

    .line 199
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 178
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->e:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "iban"

    .line 200
    invoke-virtual {v2, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v5, 0x2

    aput-object v2, v1, v5

    .line 179
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->f:Lcom/siimkinks/sqlitemagic/x;

    const-string v6, "alias"

    .line 201
    invoke-virtual {v2, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_4

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const/4 v2, 0x4

    .line 180
    sget-object v7, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/b;->g:Lcom/siimkinks/sqlitemagic/n;

    check-cast v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v8, "is_default"

    .line 202
    invoke-virtual {v7, v8}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v7

    if-eqz v7, :cond_3

    aput-object v7, v1, v2

    const/4 v2, 0x5

    .line 181
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/am;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v8, "customer_name"

    .line 203
    invoke-virtual {v7, v8}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v7

    if-eqz v7, :cond_2

    aput-object v7, v1, v2

    const/4 v2, 0x6

    .line 182
    sget-object v7, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/am;->f:Lcom/siimkinks/sqlitemagic/ao;

    check-cast v7, Lcom/siimkinks/sqlitemagic/x;

    const-string v8, "customer_type"

    .line 204
    invoke-virtual {v7, v8}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v7

    if-eqz v7, :cond_1

    aput-object v7, v1, v2

    const/4 v2, 0x7

    .line 183
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v7

    .line 186
    new-array v6, v6, [Lcom/siimkinks/sqlitemagic/x;

    sget-object v8, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/j;->f:Lcom/siimkinks/sqlitemagic/l;

    check-cast v8, Lcom/siimkinks/sqlitemagic/x;

    aput-object v8, v6, v3

    const-string v8, " "

    check-cast v8, Ljava/lang/CharSequence;

    .line 205
    invoke-static {v8}, Lcom/siimkinks/sqlitemagic/cb;->a(Ljava/lang/CharSequence;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v8

    const-string v9, "Select.asColumn(this)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v8, v6, v4

    .line 186
    sget-object v8, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v8, v8, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    aput-object v8, v6, v5

    invoke-static {v6}, Lcom/siimkinks/sqlitemagic/cb;->a([Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string v6, ", "

    .line 185
    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/cb;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v5

    const-string v6, "groupConcat(\n           \u2026                    \", \")"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-static {v7, v5}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v5

    .line 188
    sget-object v6, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v7, "BALANCE_DATA"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lcom/siimkinks/sqlitemagic/dl;

    .line 206
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v5

    const-string v6, "SELECT\n                C\u2026        FROM BALANCE_DATA"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    sget-object v6, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v6, v6, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v7, "BALANCE_DATA.ACCOUNT_ID"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v7, Lcom/siimkinks/sqlitemagic/x;

    .line 207
    invoke-virtual {v6, v7}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v6

    const-string v7, "this.`is`(column)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v5

    const-string v6, "balances_summary"

    .line 190
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/cb$h;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;

    move-result-object v5

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    aput-object v5, v1, v2

    .line 174
    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;

    move-result-object v0

    .line 191
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v2, "ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 209
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026        FROM ACCOUNT_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    sget-object v1, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    const-string v2, "CUSTOMER_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "ACCOUNT_DATA.CUSTOMER_ID"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/siimkinks/sqlitemagic/am;->a:Lcom/siimkinks/sqlitemagic/am;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/am;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    .line 210
    invoke-virtual {v2, v5}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v5, "this.`is`(column)"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bi;

    move-result-object v1

    const-string v2, "this.on(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bi;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026STOMER_DATA.CUSTOMER_ID))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/b;->i:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/bn;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "ACCOUNT_DATA.ORDERING.asc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    new-array v2, v4, [Lcom/siimkinks/sqlitemagic/cb$f;

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/cb$c;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$e;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    const-string v1, "(SELECT\n        COLUMNS\n\u2026asc())\n        .compile()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/g;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void

    .line 186
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.siimkinks.sqlitemagic.Column<*, *, *, kotlin.Any, kotlin.Any>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_5
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_6
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_7
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/swedbank/mobile/data/customer/CustomerType;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/data/customer/CustomerType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iban"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerName"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerType"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balancesSummary"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/g;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/g;->d:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/data/account/g;->e:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/data/account/g;->f:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/swedbank/mobile/data/account/g;->g:Z

    iput-object p6, p0, Lcom/swedbank/mobile/data/account/g;->h:Ljava/lang/String;

    iput-object p7, p0, Lcom/swedbank/mobile/data/account/g;->i:Lcom/swedbank/mobile/data/customer/CustomerType;

    iput-object p8, p0, Lcom/swedbank/mobile/data/account/g;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/a/c;
    .locals 10
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 159
    new-instance v9, Lcom/swedbank/mobile/business/a/c;

    .line 160
    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->c:Ljava/lang/String;

    .line 161
    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->d:Ljava/lang/String;

    .line 162
    iget-object v3, p0, Lcom/swedbank/mobile/data/account/g;->e:Ljava/lang/String;

    .line 163
    iget-object v4, p0, Lcom/swedbank/mobile/data/account/g;->f:Ljava/lang/String;

    .line 164
    iget-boolean v5, p0, Lcom/swedbank/mobile/data/account/g;->g:Z

    .line 165
    iget-object v6, p0, Lcom/swedbank/mobile/data/account/g;->h:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/g;->i:Lcom/swedbank/mobile/data/customer/CustomerType;

    sget-object v7, Lcom/swedbank/mobile/data/customer/CustomerType;->COMPANY:Lcom/swedbank/mobile/data/customer/CustomerType;

    if-ne v0, v7, :cond_0

    const/4 v0, 0x1

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 167
    :goto_0
    iget-object v8, p0, Lcom/swedbank/mobile/data/account/g;->j:Ljava/lang/String;

    move-object v0, v9

    .line 159
    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/business/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)V

    return-object v9
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/data/account/g;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/data/account/g;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/g;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/g;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/g;->e:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/g;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/account/g;->g:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/data/account/g;->g:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/g;->h:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->i:Lcom/swedbank/mobile/data/customer/CustomerType;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/g;->i:Lcom/swedbank/mobile/data/customer/CustomerType;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->j:Ljava/lang/String;

    iget-object p1, p1, Lcom/swedbank/mobile/data/account/g;->j:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/g;->c:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/data/account/g;->g:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->h:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->i:Lcom/swedbank/mobile/data/customer/CustomerType;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/g;->j:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountSummaryView(accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", customerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", iban="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", alias="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isDefault="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/data/account/g;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", customerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", customerType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->i:Lcom/swedbank/mobile/data/customer/CustomerType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", balancesSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/g;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
