.class public final Lcom/swedbank/mobile/data/account/l;
.super Ljava/lang/Object;
.source "AccountData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/account/l$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ae<",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/account/l$a;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/math/BigDecimal;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/swedbank/mobile/data/account/l$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/account/l$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/l;->b:Lcom/swedbank/mobile/data/account/l$a;

    .line 106
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    const/4 v1, 0x3

    .line 108
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/x;

    .line 109
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "account_id"

    .line 198
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    .line 110
    sget-object v3, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/j;->i:Lcom/siimkinks/sqlitemagic/l;

    check-cast v3, Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "reserved"

    .line 199
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_1

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 111
    sget-object v3, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "currency"

    .line 200
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v3

    if-eqz v3, :cond_0

    aput-object v3, v1, v2

    .line 107
    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;

    move-result-object v0

    .line 112
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v2, "ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 201
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026        FROM ACCOUNT_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    sget-object v1, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v2, "BALANCE_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v3, "ACCOUNT_DATA.ACCOUNT_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    sget-object v3, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 202
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v3, "this.`is`(column)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bi;

    move-result-object v1

    const-string v2, "this.on(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bi;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026BALANCE_DATA.ACCOUNT_ID))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/b;->h:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_DATA.MAIN_CURRENCY"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    .line 205
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(column)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$h;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    const-string v1, "(SELECT\n        COLUMNS\n\u2026ENCY))\n        .compile()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/l;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void

    .line 200
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/data/account/l;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/data/account/l;

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/l;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/swedbank/mobile/data/account/l;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/l;->d:Ljava/math/BigDecimal;

    iget-object v1, p1, Lcom/swedbank/mobile/data/account/l;->d:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/l;->e:Ljava/lang/String;

    iget-object p1, p1, Lcom/swedbank/mobile/data/account/l;->e:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/l;->c:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/l;->d:Ljava/math/BigDecimal;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/l;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MainBalanceReservedAmountView(accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", reserved="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/l;->d:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
