.class public final Lcom/swedbank/mobile/data/account/p;
.super Ljava/lang/Object;
.source "SqliteMagic_AccountView_Dao.java"


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    sget-object v0, Lcom/swedbank/mobile/data/account/h;->a:Lcom/siimkinks/sqlitemagic/ae;

    sput-object v0, Lcom/swedbank/mobile/data/account/p;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/account/h;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/account/h;"
        }
    .end annotation

    const-string v0, "account_data"

    .line 18
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 21
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/account/n;->a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/account/b;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v0, "balance_data"

    .line 28
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 31
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/account/q;->a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/account/i;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string p2, "account_id"

    .line 38
    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    if-eqz p2, :cond_2

    const-string p3, "customer_id"

    .line 42
    invoke-virtual {p1, p3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_1

    const-string v0, "balance_count"

    .line 46
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    .line 50
    new-instance v0, Lcom/swedbank/mobile/data/account/h;

    .line 53
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 54
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 55
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/data/account/h;-><init>(Lcom/swedbank/mobile/data/account/b;Lcom/swedbank/mobile/data/account/i;Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0

    .line 48
    :cond_0
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_view\" required column \"balance_count\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 44
    :cond_1
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_view\" required column \"customer_id\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 40
    :cond_2
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_view\" required column \"account_id\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 33
    :cond_3
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Complex column mainBalance cannot be instantiated with only id"

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 36
    :cond_4
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_view\" required column \"balance_data\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 23
    :cond_5
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Complex column account cannot be instantiated with only id"

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 26
    :cond_6
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_view\" required column \"account_data\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
