.class public final Lcom/swedbank/mobile/data/account/m;
.super Ljava/lang/Object;
.source "SqliteMagic_AccountBalanceView_Dao.java"


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    sget-object v0, Lcom/swedbank/mobile/data/account/a;->a:Lcom/siimkinks/sqlitemagic/ae;

    sput-object v0, Lcom/swedbank/mobile/data/account/m;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void
.end method

.method public static a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/account/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/siimkinks/sqlitemagic/b/c<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/swedbank/mobile/data/account/a;"
        }
    .end annotation

    const-string v0, "account_id"

    .line 18
    invoke-virtual {p1, v0}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    const-string v1, "customer_id"

    .line 22
    invoke-virtual {p1, v1}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v2, "main_currency"

    .line 26
    invoke-virtual {p1, v2}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    const-string v3, "balance_data"

    .line 30
    invoke-virtual {p1, v3}, Lcom/siimkinks/sqlitemagic/b/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 33
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/data/account/q;->a(Landroid/database/Cursor;Lcom/siimkinks/sqlitemagic/b/c;Lcom/siimkinks/sqlitemagic/b/c;Ljava/lang/String;)Lcom/swedbank/mobile/data/account/i;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 40
    new-instance p2, Lcom/swedbank/mobile/data/account/a;

    .line 41
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-interface {p0, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 42
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, p3, v0, p0, p1}, Lcom/swedbank/mobile/data/account/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/data/account/i;)V

    return-object p2

    .line 35
    :cond_0
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Complex column balance cannot be instantiated with only id"

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 38
    :cond_1
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_balance_view\" required column \"balance_data\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 28
    :cond_2
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_balance_view\" required column \"main_currency\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 24
    :cond_3
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_balance_view\" required column \"customer_id\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 20
    :cond_4
    new-instance p0, Landroid/database/SQLException;

    const-string p1, "Selected columns did not contain table \"account_balance_view\" required column \"account_id\""

    invoke-direct {p0, p1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
