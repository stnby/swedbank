.class public final Lcom/swedbank/mobile/data/account/h;
.super Ljava/lang/Object;
.source "AccountData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/data/account/h$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/siimkinks/sqlitemagic/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/siimkinks/sqlitemagic/ae<",
            "Lcom/swedbank/mobile/data/account/b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final b:Lcom/swedbank/mobile/data/account/h$a;


# instance fields
.field private final c:Lcom/swedbank/mobile/data/account/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/data/account/i;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:J


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/swedbank/mobile/data/account/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/account/h$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/h;->b:Lcom/swedbank/mobile/data/account/h$a;

    .line 73
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v0

    const/4 v1, 0x5

    .line 75
    new-array v1, v1, [Lcom/siimkinks/sqlitemagic/x;

    .line 76
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/b;->a()Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 77
    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/j;->a()Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 78
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "account_id"

    .line 198
    invoke-virtual {v2, v5}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const/4 v2, 0x3

    .line 79
    sget-object v5, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v6, "customer_id"

    .line 199
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/x;

    move-result-object v5

    if-eqz v5, :cond_0

    aput-object v5, v1, v2

    const/4 v2, 0x4

    .line 80
    invoke-static {}, Lcom/siimkinks/sqlitemagic/bz;->a()Lcom/siimkinks/sqlitemagic/cb;

    move-result-object v5

    .line 81
    invoke-static {}, Lcom/siimkinks/sqlitemagic/cb;->a()Lcom/siimkinks/sqlitemagic/bn;

    move-result-object v6

    const-string v7, "count()"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lcom/siimkinks/sqlitemagic/x;

    invoke-static {v5, v6}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$g;

    move-result-object v5

    .line 82
    sget-object v6, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v7, "BALANCE_DATA"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lcom/siimkinks/sqlitemagic/dl;

    .line 200
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/cb$g;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v5

    const-string v6, "SELECT\n                C\u2026        FROM BALANCE_DATA"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object v6, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v6, v6, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v7, "BALANCE_DATA.ACCOUNT_ID"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v7, v7, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v7, Lcom/siimkinks/sqlitemagic/x;

    .line 201
    invoke-virtual {v6, v7}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v6

    const-string v7, "this.`is`(column)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v5

    const-string v6, "balance_count"

    .line 84
    invoke-virtual {v5, v6}, Lcom/siimkinks/sqlitemagic/cb$h;->a(Ljava/lang/String;)Lcom/siimkinks/sqlitemagic/bn;

    move-result-object v5

    check-cast v5, Lcom/siimkinks/sqlitemagic/x;

    aput-object v5, v1, v2

    .line 74
    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/bz;->a(Lcom/siimkinks/sqlitemagic/cb;[Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/cb$a;

    move-result-object v0

    .line 85
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v2, "ACCOUNT_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    .line 203
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$a;->a(Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026        FROM ACCOUNT_DATA"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget-object v1, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v2, "BALANCE_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v5, "ACCOUNT_DATA.ACCOUNT_ID"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/x;

    sget-object v5, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v5, v5, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    .line 204
    invoke-virtual {v2, v5}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v5, "this.`is`(column)"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/dl;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bi;

    move-result-object v1

    const-string v2, "this.on(expr)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bi;)Lcom/siimkinks/sqlitemagic/cb$c;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026BALANCE_DATA.ACCOUNT_ID))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/b;->h:Lcom/siimkinks/sqlitemagic/x;

    const-string v2, "ACCOUNT_DATA.MAIN_CURRENCY"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    .line 207
    invoke-virtual {v1, v2}, Lcom/siimkinks/sqlitemagic/x;->a(Lcom/siimkinks/sqlitemagic/x;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v1

    const-string v2, "this.`is`(column)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v0, v1}, Lcom/siimkinks/sqlitemagic/cb$c;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/cb$h;

    move-result-object v0

    const-string v1, "SELECT\n        COLUMNS\n \u2026IS BALANCE_DATA.CURRENCY)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    sget-object v1, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/b;->i:Lcom/siimkinks/sqlitemagic/bn;

    invoke-virtual {v1}, Lcom/siimkinks/sqlitemagic/bn;->c()Lcom/siimkinks/sqlitemagic/cb$f;

    move-result-object v1

    const-string v2, "ACCOUNT_DATA.ORDERING.asc()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    new-array v2, v4, [Lcom/siimkinks/sqlitemagic/cb$f;

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/siimkinks/sqlitemagic/cb$h;->a([Lcom/siimkinks/sqlitemagic/cb$f;)Lcom/siimkinks/sqlitemagic/cb$e;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cb$e;->a()Lcom/siimkinks/sqlitemagic/ae;

    move-result-object v0

    const-string v1, "(SELECT\n        COLUMNS\n\u2026asc())\n        .compile()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/swedbank/mobile/data/account/h;->a:Lcom/siimkinks/sqlitemagic/ae;

    return-void

    .line 199
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type C"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Lcom/swedbank/mobile/data/account/b;Lcom/swedbank/mobile/data/account/i;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/account/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/account/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "account"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBalance"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/h;->d:Lcom/swedbank/mobile/data/account/i;

    iput-object p3, p0, Lcom/swedbank/mobile/data/account/h;->e:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/data/account/h;->f:Ljava/lang/String;

    iput-wide p5, p0, Lcom/swedbank/mobile/data/account/h;->g:J

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/a/a;
    .locals 11
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    new-instance v8, Lcom/swedbank/mobile/business/a/a;

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/b;->b()Ljava/lang/String;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/b;->d()Ljava/lang/String;

    move-result-object v2

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/b;->f()Z

    move-result v4

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/b;->g()Ljava/lang/String;

    move-result-object v5

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->d:Lcom/swedbank/mobile/data/account/i;

    iget-object v6, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v6}, Lcom/swedbank/mobile/data/account/b;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/swedbank/mobile/data/account/i;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/a/d;

    move-result-object v6

    .line 68
    iget-wide v9, p0, Lcom/swedbank/mobile/data/account/h;->g:J

    long-to-int v7, v9

    move-object v0, v8

    .line 61
    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/business/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/swedbank/mobile/business/a/d;I)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/data/account/h;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/data/account/h;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->d:Lcom/swedbank/mobile/data/account/i;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/h;->d:Lcom/swedbank/mobile/data/account/i;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/h;->e:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/data/account/h;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v3, p0, Lcom/swedbank/mobile/data/account/h;->g:J

    iget-wide v5, p1, Lcom/swedbank/mobile/data/account/h;->g:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 5

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/h;->d:Lcom/swedbank/mobile/data/account/i;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/h;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/data/account/h;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/swedbank/mobile/data/account/h;->g:J

    const/16 v3, 0x20

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountView(account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->c:Lcom/swedbank/mobile/data/account/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mainBalance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->d:Lcom/swedbank/mobile/data/account/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", customerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/data/account/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", balanceCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/swedbank/mobile/data/account/h;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
