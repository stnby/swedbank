.class public final Lcom/swedbank/mobile/data/account/c;
.super Ljava/lang/Object;
.source "AccountRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/a/b;
.implements Lcom/swedbank/mobile/business/a/e;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/account/e;

.field private final synthetic b:Lcom/swedbank/mobile/business/a/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/e;Lcom/swedbank/mobile/data/account/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/account/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "localAccountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/c;->a:Lcom/swedbank/mobile/data/account/e;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/a/e;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->a(Ljava/util/List;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->a:Lcom/swedbank/mobile/data/account/e;

    .line 26
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/account/f;->a(Lcom/swedbank/mobile/data/account/e;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 27
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 237
    new-instance v1, Lcom/swedbank/mobile/data/account/c$b;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/account/c$b;-><init>(Lcom/swedbank/mobile/data/account/c;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->a:Lcom/swedbank/mobile/data/account/e;

    .line 171
    invoke-static {v0, p1, p2}, Lcom/swedbank/mobile/data/account/f;->a(Lcom/swedbank/mobile/data/account/e;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 174
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 425
    new-instance v1, Lcom/swedbank/mobile/data/account/c$d;

    invoke-direct {v1, p0, p1, p2}, Lcom/swedbank/mobile/data/account/c$d;-><init>(Lcom/swedbank/mobile/data/account/c;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/data/account/AccountResponse;)V
    .locals 22
    .param p1    # Lcom/swedbank/mobile/data/account/AccountResponse;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p1

    const-string v1, "account"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/account/AccountResponse;->a()Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/account/AccountResponse;->b()Ljava/lang/String;

    move-result-object v15

    const/4 v3, 0x0

    const/4 v14, 0x1

    .line 118
    invoke-static {v0, v3, v14, v3}, Lcom/swedbank/mobile/data/account/AccountResponse;->a(Lcom/swedbank/mobile/data/account/AccountResponse;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/swedbank/mobile/data/account/b;

    move-result-object v3

    .line 317
    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/cp$b;->a(Lcom/swedbank/mobile/data/account/b;)Lcom/siimkinks/sqlitemagic/cp$b;

    move-result-object v3

    .line 120
    invoke-virtual {v3}, Lcom/siimkinks/sqlitemagic/cp$b;->a()Lcom/siimkinks/sqlitemagic/a/d;

    move-result-object v3

    .line 121
    sget-object v4, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v4, Lcom/siimkinks/sqlitemagic/dt;

    invoke-interface {v3, v4}, Lcom/siimkinks/sqlitemagic/a/d;->b(Lcom/siimkinks/sqlitemagic/dt;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/siimkinks/sqlitemagic/a/d;

    .line 122
    invoke-interface {v3}, Lcom/siimkinks/sqlitemagic/a/d;->b()J

    .line 123
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v3

    .line 124
    sget-object v4, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v5, "BALANCE_DATA"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v3, v4}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v3

    .line 125
    sget-object v4, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "BALANCE_DATA.ACCOUNT_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    invoke-virtual {v4, v2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v4, "this.`is`(value)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v4, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/j;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v5, "BALANCE_DATA.CUSTOMER_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    invoke-virtual {v4, v15}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.`is`(value)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 320
    invoke-virtual {v2, v4}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v2

    const-string v4, "this.and(expr)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    invoke-virtual {v3, v2}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 127
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 128
    sget-object v2, Lcom/siimkinks/sqlitemagic/k;->a:Lcom/siimkinks/sqlitemagic/k;

    .line 129
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/account/AccountResponse;->g()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v14

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 322
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/account/AccountResponse;->g()Ljava/util/List;

    move-result-object v3

    .line 325
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/account/AccountResponse;->a()Ljava/lang/String;

    move-result-object v16

    .line 326
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/data/account/AccountResponse;->f()Ljava/lang/String;

    move-result-object v7

    .line 327
    move-object v0, v3

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v14

    if-eqz v0, :cond_5

    .line 328
    check-cast v3, Ljava/lang/Iterable;

    .line 329
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 330
    move-object v4, v2

    check-cast v4, Ljava/util/List;

    move-object v13, v4

    check-cast v13, Ljava/util/Collection;

    check-cast v3, Lcom/swedbank/mobile/data/account/BalanceResponse;

    .line 331
    new-instance v12, Lcom/swedbank/mobile/data/account/i;

    const/4 v4, 0x0

    .line 334
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->a()Ljava/lang/String;

    move-result-object v7

    .line 335
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->b()Ljava/math/BigDecimal;

    move-result-object v8

    .line 336
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v5

    if-eqz v5, :cond_0

    :goto_1
    move-object v9, v5

    goto :goto_2

    :cond_0
    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 337
    :goto_2
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->c()Ljava/math/BigDecimal;

    move-result-object v5

    if-eqz v5, :cond_1

    :goto_3
    move-object v10, v5

    goto :goto_4

    :cond_1
    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 338
    :goto_4
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->e()Ljava/math/BigDecimal;

    move-result-object v5

    if-eqz v5, :cond_2

    :goto_5
    move-object v11, v5

    goto :goto_6

    :cond_2
    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v6, "BigDecimal.ZERO"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_5

    .line 339
    :goto_6
    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v3, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-lez v3, :cond_3

    const/16 v17, 0x1

    goto :goto_7

    :cond_3
    const/4 v3, 0x0

    const/16 v17, 0x0

    :goto_7
    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object v3, v12

    move-object/from16 v5, v16

    move-object v6, v15

    move-object/from16 v20, v12

    move/from16 v12, v17

    move-object/from16 v21, v13

    move/from16 v13, v18

    const/16 v17, 0x1

    move-object/from16 v14, v19

    .line 331
    invoke-direct/range {v3 .. v14}, Lcom/swedbank/mobile/data/account/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZILkotlin/e/b/g;)V

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    .line 339
    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const/4 v14, 0x1

    goto/16 :goto_0

    .line 340
    :cond_4
    move-object v0, v2

    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    goto :goto_8

    .line 341
    :cond_5
    move-object v0, v2

    check-cast v0, Ljava/util/List;

    new-instance v14, Lcom/swedbank/mobile/data/account/i;

    const/4 v4, 0x0

    .line 345
    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v3, "BigDecimal.ZERO"

    invoke-static {v8, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    sget-object v9, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v3, "BigDecimal.ZERO"

    invoke-static {v9, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 347
    sget-object v10, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v3, "BigDecimal.ZERO"

    invoke-static {v10, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 348
    sget-object v11, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v3, "BigDecimal.ZERO"

    invoke-static {v11, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/16 v17, 0x0

    move-object v3, v14

    move-object/from16 v5, v16

    move-object v6, v15

    move-object v15, v14

    move-object/from16 v14, v17

    .line 341
    invoke-direct/range {v3 .. v14}, Lcom/swedbank/mobile/data/account/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZILkotlin/e/b/g;)V

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :goto_8
    check-cast v2, Ljava/lang/Iterable;

    .line 352
    invoke-static {v2}, Lcom/siimkinks/sqlitemagic/cs$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cs$a;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cs$a;->a()Z

    .line 353
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/math/BigDecimal;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableFunds"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-static {}, Lcom/siimkinks/sqlitemagic/dz;->a()Lcom/siimkinks/sqlitemagic/dx;

    move-result-object v0

    .line 230
    sget-object v1, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v2, "BALANCE_DATA"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v0, v1}, Lcom/siimkinks/sqlitemagic/dz;->a(Lcom/siimkinks/sqlitemagic/dx;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/dx$b;

    move-result-object v0

    .line 231
    sget-object v1, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v1, v1, Lcom/siimkinks/sqlitemagic/j;->f:Lcom/siimkinks/sqlitemagic/l;

    invoke-static {v1, p3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p3

    .line 443
    invoke-virtual {p3}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/siimkinks/sqlitemagic/x;

    invoke-virtual {p3}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {v0, v1, p3}, Lcom/siimkinks/sqlitemagic/dx$b;->a(Lcom/siimkinks/sqlitemagic/x;Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/dx$a;

    move-result-object p3

    const-string v0, "UPDATE\n        TABLE BAL\u2026_FUNDS to availableFunds)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    sget-object v0, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/j;->c:Lcom/siimkinks/sqlitemagic/x;

    const-string v1, "BALANCE_DATA.ACCOUNT_ID"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    invoke-virtual {v0, p1}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string v0, "this.`is`(value)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    sget-object v0, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v0, v0, Lcom/siimkinks/sqlitemagic/j;->e:Lcom/siimkinks/sqlitemagic/x;

    const-string v1, "BALANCE_DATA.CURRENCY"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 445
    invoke-virtual {v0, p2}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p2

    const-string v0, "this.`is`(value)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 446
    invoke-virtual {p1, p2}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object p1

    const-string p2, "this.and(expr)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 447
    invoke-virtual {p3, p1}, Lcom/siimkinks/sqlitemagic/dx$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/dx$d;

    move-result-object p1

    .line 234
    invoke-virtual {p1}, Lcom/siimkinks/sqlitemagic/dx$d;->a()I

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 24
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/data/account/AccountResponse;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "customerId"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "allAccounts"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 358
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v13

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v13, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    :try_start_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    .line 139
    sget-object v3, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    const-string v4, "BALANCE_DATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 140
    sget-object v3, Lcom/siimkinks/sqlitemagic/j;->a:Lcom/siimkinks/sqlitemagic/j;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/j;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "BALANCE_DATA.CUSTOMER_ID"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    invoke-virtual {v3, v0}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 141
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 142
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v1

    .line 144
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v3, "ACCOUNT_DATA"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v1, v2}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v1

    .line 145
    sget-object v2, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v2, v2, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v3, "ACCOUNT_DATA.CUSTOMER_ID"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    invoke-virtual {v2, v0}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v0

    const-string v2, "this.`is`(value)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    invoke-virtual {v1, v0}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    goto/16 :goto_c

    .line 148
    :cond_0
    invoke-static {}, Lcom/siimkinks/sqlitemagic/ax;->a()Lcom/siimkinks/sqlitemagic/av;

    move-result-object v2

    .line 149
    sget-object v3, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    const-string v4, "ACCOUNT_DATA"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/siimkinks/sqlitemagic/dl;

    invoke-static {v2, v3}, Lcom/siimkinks/sqlitemagic/ax;->a(Lcom/siimkinks/sqlitemagic/av;Lcom/siimkinks/sqlitemagic/dl;)Lcom/siimkinks/sqlitemagic/av$a;

    move-result-object v2

    .line 150
    sget-object v3, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/b;->d:Lcom/siimkinks/sqlitemagic/x;

    const-string v4, "ACCOUNT_DATA.CUSTOMER_ID"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 365
    invoke-virtual {v3, v0}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/lang/Object;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.`is`(value)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    sget-object v4, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v4, v4, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    const-string v5, "ACCOUNT_DATA.ACCOUNT_ID"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/siimkinks/sqlitemagic/x;

    move-object v5, v1

    check-cast v5, Ljava/lang/Iterable;

    .line 366
    new-instance v6, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v5, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 367
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 368
    check-cast v8, Lcom/swedbank/mobile/data/account/AccountResponse;

    .line 151
    invoke-virtual {v8}, Lcom/swedbank/mobile/data/account/AccountResponse;->a()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 369
    :cond_1
    check-cast v6, Ljava/util/List;

    check-cast v6, Ljava/util/Collection;

    .line 370
    invoke-virtual {v4, v6}, Lcom/siimkinks/sqlitemagic/x;->b(Ljava/util/Collection;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v4

    const-string v5, "this.notIn(values)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    invoke-virtual {v3, v4}, Lcom/siimkinks/sqlitemagic/bb;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/bb;

    move-result-object v3

    const-string v4, "this.and(expr)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 372
    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/av$a;->a(Lcom/siimkinks/sqlitemagic/bb;)Lcom/siimkinks/sqlitemagic/av$b;

    move-result-object v2

    .line 152
    invoke-virtual {v2}, Lcom/siimkinks/sqlitemagic/av$b;->a()I

    .line 153
    sget-object v2, Lcom/siimkinks/sqlitemagic/c;->a:Lcom/siimkinks/sqlitemagic/c;

    .line 373
    move-object v2, v1

    check-cast v2, Ljava/lang/Iterable;

    .line 377
    new-instance v3, Lcom/swedbank/mobile/data/account/c$a;

    invoke-direct {v3}, Lcom/swedbank/mobile/data/account/c$a;-><init>()V

    check-cast v3, Ljava/util/Comparator;

    invoke-static {v2, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 378
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 380
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v4, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    add-int/lit8 v6, v4, 0x1

    if-gez v4, :cond_2

    .line 381
    invoke-static {}, Lkotlin/a/h;->b()V

    :cond_2
    check-cast v5, Lcom/swedbank/mobile/data/account/AccountResponse;

    .line 383
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 382
    invoke-virtual {v5, v4}, Lcom/swedbank/mobile/data/account/AccountResponse;->a(Ljava/lang/Integer;)Lcom/swedbank/mobile/data/account/b;

    move-result-object v4

    .line 383
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v4, v6

    goto :goto_1

    .line 384
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 385
    check-cast v3, Ljava/lang/Iterable;

    .line 386
    invoke-static {v3}, Lcom/siimkinks/sqlitemagic/cp$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cp$a;

    move-result-object v2

    .line 157
    sget-object v3, Lcom/siimkinks/sqlitemagic/b;->a:Lcom/siimkinks/sqlitemagic/b;

    iget-object v3, v3, Lcom/siimkinks/sqlitemagic/b;->c:Lcom/siimkinks/sqlitemagic/du;

    check-cast v3, Lcom/siimkinks/sqlitemagic/dt;

    invoke-virtual {v2, v3}, Lcom/siimkinks/sqlitemagic/cp$a;->a(Lcom/siimkinks/sqlitemagic/dt;)Lcom/siimkinks/sqlitemagic/a/a;

    move-result-object v2

    .line 158
    invoke-interface {v2}, Lcom/siimkinks/sqlitemagic/a/a;->b()Z

    .line 159
    sget-object v2, Lcom/siimkinks/sqlitemagic/k;->a:Lcom/siimkinks/sqlitemagic/k;

    .line 160
    new-instance v15, Ljava/util/ArrayList;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    check-cast v1, Ljava/lang/Iterable;

    .line 387
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/account/AccountResponse;

    .line 162
    move-object v12, v15

    check-cast v12, Ljava/util/List;

    .line 388
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/AccountResponse;->g()Ljava/util/List;

    move-result-object v2

    .line 391
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/AccountResponse;->a()Ljava/lang/String;

    move-result-object v17

    .line 392
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/AccountResponse;->f()Ljava/lang/String;

    move-result-object v5

    .line 393
    move-object v1, v2

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/16 v18, 0x1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_9

    .line 394
    check-cast v2, Ljava/lang/Iterable;

    .line 395
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 396
    move-object v11, v12

    check-cast v11, Ljava/util/Collection;

    check-cast v1, Lcom/swedbank/mobile/data/account/BalanceResponse;

    .line 397
    new-instance v10, Lcom/swedbank/mobile/data/account/i;

    const/4 v2, 0x0

    .line 400
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->a()Ljava/lang/String;

    move-result-object v5

    .line 401
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->b()Ljava/math/BigDecimal;

    move-result-object v6

    .line 402
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_4

    :goto_4
    move-object v7, v3

    goto :goto_5

    :cond_4
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v4, "BigDecimal.ZERO"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    .line 403
    :goto_5
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->c()Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_5

    :goto_6
    move-object v8, v3

    goto :goto_7

    :cond_5
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v4, "BigDecimal.ZERO"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 404
    :goto_7
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->e()Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_6

    :goto_8
    move-object v9, v3

    goto :goto_9

    :cond_6
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v4, "BigDecimal.ZERO"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_8

    .line 405
    :goto_9
    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v1}, Lcom/swedbank/mobile/data/account/BalanceResponse;->d()Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-lez v1, :cond_7

    const/16 v20, 0x1

    goto :goto_a

    :cond_7
    const/16 v20, 0x0

    :goto_a
    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object v1, v10

    move-object/from16 v3, v17

    move-object/from16 v4, p1

    move-object v14, v10

    move/from16 v10, v20

    move-object/from16 v23, v11

    move/from16 v11, v21

    move-object v0, v12

    move-object/from16 v12, v22

    .line 397
    invoke-direct/range {v1 .. v12}, Lcom/swedbank/mobile/data/account/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZILkotlin/e/b/g;)V

    move-object/from16 v12, v23

    .line 405
    invoke-interface {v12, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v12, v0

    move-object/from16 v0, p1

    goto :goto_3

    :cond_8
    move-object v0, v12

    .line 406
    move-object v12, v0

    check-cast v12, Ljava/util/Collection;

    goto :goto_b

    :cond_9
    move-object v0, v12

    .line 407
    new-instance v14, Lcom/swedbank/mobile/data/account/i;

    const/4 v2, 0x0

    .line 411
    sget-object v6, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "BigDecimal.ZERO"

    invoke-static {v6, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 412
    sget-object v7, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "BigDecimal.ZERO"

    invoke-static {v7, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 413
    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "BigDecimal.ZERO"

    invoke-static {v8, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 414
    sget-object v9, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "BigDecimal.ZERO"

    invoke-static {v9, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    move-object v1, v14

    move-object/from16 v3, v17

    move-object/from16 v4, p1

    .line 407
    invoke-direct/range {v1 .. v12}, Lcom/swedbank/mobile/data/account/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZILkotlin/e/b/g;)V

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_b
    move-object/from16 v0, p1

    goto/16 :goto_2

    .line 160
    :cond_a
    check-cast v15, Ljava/lang/Iterable;

    .line 419
    invoke-static {v15}, Lcom/siimkinks/sqlitemagic/cs$a;->a(Ljava/lang/Iterable;)Lcom/siimkinks/sqlitemagic/cs$a;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/siimkinks/sqlitemagic/cs$a;->a()Z

    .line 420
    :goto_c
    invoke-interface {v13}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    invoke-interface {v13}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v13}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1, p2}, Lcom/swedbank/mobile/business/a/e;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->a:Lcom/swedbank/mobile/data/account/e;

    .line 46
    invoke-static {v0, p1}, Lcom/swedbank/mobile/data/account/f;->b(Lcom/swedbank/mobile/data/account/e;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 246
    new-instance v1, Lcom/swedbank/mobile/data/account/c$c;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/data/account/c$c;-><init>(Lcom/swedbank/mobile/data/account/c;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->c(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->d(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public e(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public f(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->f(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/String;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/data/account/c;->b:Lcom/swedbank/mobile/business/a/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/a/e;->g(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
