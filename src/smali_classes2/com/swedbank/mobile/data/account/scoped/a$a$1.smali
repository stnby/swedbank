.class public final Lcom/swedbank/mobile/data/account/scoped/a$a$1;
.super Ljava/lang/Object;
.source "NetworkModels.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/data/account/scoped/a$a;->a(Lcom/swedbank/mobile/data/network/w;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/data/account/scoped/a$a;

.field final synthetic b:Lcom/swedbank/mobile/data/network/w;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/account/scoped/a$a;Lcom/swedbank/mobile/data/network/w;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/scoped/a$a$1;->a:Lcom/swedbank/mobile/data/account/scoped/a$a;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/scoped/a$a$1;->b:Lcom/swedbank/mobile/data/network/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/p$b;
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/scoped/a$a$1;->b:Lcom/swedbank/mobile/data/network/w;

    check-cast v0, Lcom/swedbank/mobile/data/network/w$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/data/network/w$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/account/scoped/ScopedAccountListResponse;

    .line 237
    invoke-static {}, Lcom/siimkinks/sqlitemagic/co;->a()Lcom/siimkinks/sqlitemagic/dm;

    move-result-object v1

    const-string v2, "SqliteMagic.newTransaction()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    :try_start_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/data/account/scoped/ScopedAccountListResponse;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 241
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/account/scoped/ScopedAccountResponse;

    .line 242
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/account/scoped/ScopedAccountResponse;->b()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 243
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/data/account/scoped/ScopedBalanceResponse;

    .line 244
    iget-object v5, p0, Lcom/swedbank/mobile/data/account/scoped/a$a$1;->a:Lcom/swedbank/mobile/data/account/scoped/a$a;

    iget-object v5, v5, Lcom/swedbank/mobile/data/account/scoped/a$a;->a:Lcom/swedbank/mobile/data/account/scoped/a;

    invoke-static {v5}, Lcom/swedbank/mobile/data/account/scoped/a;->a(Lcom/swedbank/mobile/data/account/scoped/a;)Lcom/swedbank/mobile/business/a/b;

    move-result-object v5

    .line 245
    invoke-virtual {v2}, Lcom/swedbank/mobile/data/account/scoped/ScopedAccountResponse;->a()Ljava/lang/String;

    move-result-object v6

    .line 246
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/account/scoped/ScopedBalanceResponse;->a()Ljava/lang/String;

    move-result-object v7

    .line 247
    invoke-virtual {v4}, Lcom/swedbank/mobile/data/account/scoped/ScopedBalanceResponse;->b()Ljava/math/BigDecimal;

    move-result-object v4

    .line 244
    invoke-interface {v5, v6, v7, v4}, Lcom/swedbank/mobile/business/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V

    goto :goto_0

    .line 254
    :cond_1
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    .line 81
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    return-object v0

    :catchall_0
    move-exception v0

    .line 256
    invoke-interface {v1}, Lcom/siimkinks/sqlitemagic/dm;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/data/account/scoped/a$a$1;->a()Lcom/swedbank/mobile/business/util/p$b;

    move-result-object v0

    return-object v0
.end method
