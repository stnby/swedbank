.class public final Lcom/swedbank/mobile/data/account/scoped/a;
.super Ljava/lang/Object;
.source "ScopedAccountRepositoryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/a/a/a;


# instance fields
.field private final a:Lcom/swedbank/mobile/data/account/scoped/c;

.field private final b:Lcom/swedbank/mobile/business/a/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/account/scoped/c;Lcom/swedbank/mobile/business/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/account/scoped/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopedAccountService"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/data/account/scoped/a;->a:Lcom/swedbank/mobile/data/account/scoped/c;

    iput-object p2, p0, Lcom/swedbank/mobile/data/account/scoped/a;->b:Lcom/swedbank/mobile/business/a/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/data/account/scoped/a;)Lcom/swedbank/mobile/business/a/b;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/data/account/scoped/a;->b:Lcom/swedbank/mobile/business/a/b;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/data/account/scoped/a;->a:Lcom/swedbank/mobile/data/account/scoped/c;

    .line 17
    invoke-static {v0}, Lcom/swedbank/mobile/data/account/scoped/d;->a(Lcom/swedbank/mobile/data/account/scoped/c;)Lio/reactivex/w;

    move-result-object v0

    .line 18
    invoke-static {v0}, Lcom/swedbank/mobile/data/network/ab;->b(Lio/reactivex/w;)Lio/reactivex/w;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/swedbank/mobile/data/account/scoped/a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/data/account/scoped/a$a;-><init>(Lcom/swedbank/mobile/data/account/scoped/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "flatMap {\n  when (it) {\n\u2026etworkErrorSingle()\n  }\n}"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
