.class public final Lcom/swedbank/mobile/data/account/scoped/d;
.super Ljava/lang/Object;
.source "ScopedAccountService.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/data/account/scoped/c;)Lio/reactivex/w;
    .locals 4
    .param p0    # Lcom/swedbank/mobile/data/account/scoped/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/account/scoped/c;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/data/network/w<",
            "Lcom/swedbank/mobile/data/account/scoped/ScopedAccountListResponse;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$allAccounts"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/swedbank/mobile/data/network/GraphQLRequest;

    const-string v1, "\n{\n  accountsForScoped {\n    id\n    balances {\n      currency\n      availableFunds\n    }\n  }\n}\n"

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/swedbank/mobile/data/network/GraphQLRequest;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    invoke-interface {p0, v0}, Lcom/swedbank/mobile/data/account/scoped/c;->a(Lcom/swedbank/mobile/data/network/GraphQLRequest;)Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method
