.class public final Lcom/swedbank/mobile/util/a;
.super Ljava/lang/Object;
.source "Transactions.kt"


# direct methods
.method public static final a(Landroid/content/Context;Ljava/math/BigDecimal;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Direction;)Landroid/text/SpannedString;
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/math/BigDecimal;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/Transaction$Direction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$buildUserDisplayedAmountString"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 20
    invoke-static {p1, v0, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 21
    new-instance v0, Lcom/swedbank/mobile/util/a$a;

    invoke-direct {v0, p1, p2}, Lcom/swedbank/mobile/util/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    new-instance p1, Landroid/text/SpannableStringBuilder;

    invoke-direct {p1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 27
    sget-object p2, Lcom/swedbank/mobile/util/b;->a:[I

    invoke-virtual {p3}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->ordinal()I

    move-result p3

    aget p2, p2, p3

    const/16 p3, 0x11

    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    .line 32
    sget p2, Lcom/swedbank/mobile/app/overview/i$a;->text_color_primary:I

    .line 49
    invoke-static {p0, p2}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p0

    .line 50
    new-instance p2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {p2, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 51
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p0

    const/16 v1, 0x2d

    .line 33
    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 34
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/util/a$a;->a(Landroid/text/SpannableStringBuilder;)V

    .line 53
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {p1, p2, p0, v0, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 28
    :cond_0
    sget p2, Lcom/swedbank/mobile/app/overview/i$a;->good_green:I

    .line 42
    invoke-static {p0, p2}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p0

    .line 43
    new-instance p2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {p2, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 44
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p0

    const/16 v1, 0x2b

    .line 29
    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 30
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/util/a$a;->a(Landroid/text/SpannableStringBuilder;)V

    .line 46
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {p1, p2, p0, v0, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 55
    :goto_0
    new-instance p0, Landroid/text/SpannedString;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    return-object p0
.end method
