.class public final Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;
.super Landroidx/recyclerview/widget/LinearLayoutManager;
.source "CenterZoomLinearLayoutManager.kt"


# instance fields
.field private final a:F

.field private final b:F

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const p1, 0x3e19999a    # 0.15f

    .line 16
    iput p1, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->a:F

    const p1, 0x3f666666    # 0.9f

    .line 17
    iput p1, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->b:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const p1, 0x3e19999a    # 0.15f

    .line 16
    iput p1, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->a:F

    const p1, 0x3f666666    # 0.9f

    .line 17
    iput p1, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->b:F

    return-void
.end method


# virtual methods
.method public a(ILandroidx/recyclerview/widget/RecyclerView$p;Landroidx/recyclerview/widget/RecyclerView$u;)I
    .locals 10
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$p;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroidx/recyclerview/widget/RecyclerView$u;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->g()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->c:Ljava/lang/Integer;

    .line 28
    invoke-super {p0, p1, p2, p3}, Landroidx/recyclerview/widget/LinearLayoutManager;->a(ILandroidx/recyclerview/widget/RecyclerView$p;Landroidx/recyclerview/widget/RecyclerView$u;)I

    move-result p1

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->A()I

    move-result p2

    int-to-float p2, p2

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p2, p3

    .line 31
    iget v2, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->b:F

    mul-float v2, v2, p2

    .line 33
    iget v3, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->a:F

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v3, v4, v3

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->x()I

    move-result v5

    :goto_0
    if-ge v1, v5, :cond_3

    .line 35
    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->i(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 36
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v7

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, p3

    sub-float v7, p2, v7

    .line 37
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v2, v7}, Ljava/lang/Math;->min(FF)F

    move-result v7

    sub-float v8, v3, v4

    const/4 v9, 0x0

    sub-float/2addr v7, v9

    mul-float v8, v8, v7

    sub-float v7, v2, v9

    div-float/2addr v8, v7

    add-float/2addr v8, v4

    .line 40
    instance-of v7, v6, Landroid/view/ViewGroup;

    if-eqz v7, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const-string v7, "child.findViewById<View>(zoomedViewId)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getTranslationY()F

    move-result v7

    cmpg-float v7, v7, v9

    if-eqz v7, :cond_1

    goto :goto_1

    .line 46
    :cond_1
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, p3

    invoke-virtual {v6, v7}, Landroid/view/View;->setPivotX(F)V

    .line 47
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, p3

    invoke-virtual {v6, v7}, Landroid/view/View;->setPivotY(F)V

    .line 48
    invoke-virtual {v6, v8}, Landroid/view/View;->setScaleX(F)V

    .line 49
    invoke-virtual {v6, v8}, Landroid/view/View;->setScaleY(F)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_3
    return p1

    :cond_4
    return v1
.end method

.method public final a(I)V
    .locals 0

    .line 22
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->c:Ljava/lang/Integer;

    return-void
.end method

.method public c(Landroidx/recyclerview/widget/RecyclerView$p;Landroidx/recyclerview/widget/RecyclerView$u;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$p;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/RecyclerView$u;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 58
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->c(Landroidx/recyclerview/widget/RecyclerView$p;Landroidx/recyclerview/widget/RecyclerView$u;)V

    const/4 v0, 0x0

    .line 59
    invoke-virtual {p0, v0, p1, p2}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->a(ILandroidx/recyclerview/widget/RecyclerView$p;Landroidx/recyclerview/widget/RecyclerView$u;)I

    return-void
.end method
