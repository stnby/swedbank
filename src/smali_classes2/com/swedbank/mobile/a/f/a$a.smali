.class public interface abstract Lcom/swedbank/mobile/a/f/a$a;
.super Ljava/lang/Object;
.source "ChallengeComponent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/f/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/challenge/a;)Lcom/swedbank/mobile/a/f/a$a;
    .param p1    # Lcom/swedbank/mobile/business/challenge/a;
        .annotation runtime Ljavax/inject/Named;
            value = "challenge_info"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/challenge/d;)Lcom/swedbank/mobile/a/f/a$a;
    .param p1    # Lcom/swedbank/mobile/business/challenge/d;
        .annotation runtime Ljavax/inject/Named;
            value = "challenge_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
