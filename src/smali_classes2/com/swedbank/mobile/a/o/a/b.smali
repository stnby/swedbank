.class public final Lcom/swedbank/mobile/a/o/a/b;
.super Ljava/lang/Object;
.source "MessageCenterAccessModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/o/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Lcom/swedbank/mobile/a/o/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/o/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/o/a/b;->a:Lcom/swedbank/mobile/a/o/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 1
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_message_center_access"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_message_center_access"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_message_center_access"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/architect/a/b/f$a;-><init>(Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method
