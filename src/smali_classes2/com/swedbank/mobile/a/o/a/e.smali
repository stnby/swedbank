.class public final Lcom/swedbank/mobile/a/o/a/e;
.super Ljava/lang/Object;
.source "MessageCenterAccessModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/o/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/swedbank/mobile/a/o/a/e;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/o/a/e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/o/a/e;->a:Lcom/swedbank/mobile/a/o/a/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/a/o/a/d;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/a/o/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lcom/swedbank/mobile/a/o/a/d;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_logged_in_navigation_item_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "plugin"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feature_messaging_center"

    .line 40
    invoke-interface {p0, v0}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    goto :goto_0

    .line 41
    :cond_0
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object p0

    :goto_0
    return-object p0
.end method
