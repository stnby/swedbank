.class public final Lcom/swedbank/mobile/a/c/b;
.super Ljava/lang/Object;
.source "AuthenticationDataModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/swedbank/mobile/a/c/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/c/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/c/b;->a:Lcom/swedbank/mobile/a/c/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lretrofit2/r;)Lcom/swedbank/mobile/data/network/aa;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "for_authentication"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/swedbank/mobile/data/network/aa;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/data/network/aa;-><init>(Lretrofit2/r;)V

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/b;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "authorization_client_id"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->b()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/b;Lcom/squareup/moshi/n;Lokhttp3/x;Lokhttp3/u;)Lretrofit2/r;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lokhttp3/x;
        .annotation runtime Ljavax/inject/Named;
            value = "HTTP_CLIENT"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lokhttp3/u;
        .annotation runtime Ljavax/inject/Named;
            value = "internal_service_requests_interceptor"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "for_authentication"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "httpClient"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceInterceptor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 68
    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/network/n;->a(Lcom/squareup/moshi/n;Z)Lretrofit2/r$a;

    move-result-object p1

    .line 69
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lretrofit2/r$a;->a(Ljava/lang/String;)Lretrofit2/r$a;

    move-result-object p0

    .line 71
    invoke-virtual {p2}, Lokhttp3/x;->A()Lokhttp3/x$a;

    move-result-object p1

    .line 72
    new-instance p2, Lcom/swedbank/mobile/data/authentication/a;

    invoke-direct {p2}, Lcom/swedbank/mobile/data/authentication/a;-><init>()V

    check-cast p2, Lokhttp3/m;

    invoke-virtual {p1, p2}, Lokhttp3/x$a;->a(Lokhttp3/m;)Lokhttp3/x$a;

    move-result-object p1

    .line 73
    invoke-virtual {p1, v0}, Lokhttp3/x$a;->b(Z)Lokhttp3/x$a;

    move-result-object p1

    .line 74
    invoke-virtual {p1, v0}, Lokhttp3/x$a;->a(Z)Lokhttp3/x$a;

    move-result-object p1

    const/4 p2, 0x0

    .line 75
    invoke-virtual {p1, p2}, Lokhttp3/x$a;->a(Lokhttp3/c;)Lokhttp3/x$a;

    move-result-object p1

    const-string p2, "httpClient\n          .ne\u2026e)\n          .cache(null)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lokhttp3/x$a;->a()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, v0, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lokhttp3/x$a;->b()Lokhttp3/x;

    move-result-object p1

    .line 70
    invoke-virtual {p0, p1}, Lretrofit2/r$a;->a(Lokhttp3/x;)Lretrofit2/r$a;

    move-result-object p0

    .line 78
    invoke-virtual {p0}, Lretrofit2/r$a;->b()Lretrofit2/r;

    move-result-object p0

    const-string p1, "retrofitBuilder(jsonMapp\u2026 .build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final b(Lretrofit2/r;)Lcom/swedbank/mobile/data/authentication/g;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    const-class v0, Lcom/swedbank/mobile/data/authentication/g;

    invoke-virtual {p0, v0}, Lretrofit2/r;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/authentication/g;

    return-object p0
.end method

.method public static final b(Lcom/swedbank/mobile/business/c/b;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "authorization_client_secret"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->c()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Lcom/swedbank/mobile/business/c/b;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "authorization_scope"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->d()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final d(Lcom/swedbank/mobile/business/c/b;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "scoped_authorization_client_id"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->h()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final e(Lcom/swedbank/mobile/business/c/b;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "scoped_authorization_scope"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/b;->i()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
