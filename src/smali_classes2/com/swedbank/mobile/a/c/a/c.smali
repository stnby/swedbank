.class public final Lcom/swedbank/mobile/a/c/a/c;
.super Ljava/lang/Object;
.source "AuthenticatedModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/c/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    new-instance v0, Lcom/swedbank/mobile/a/c/a/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/c/a/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/c/a/c;->a:Lcom/swedbank/mobile/a/c/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;
    .locals 1
    .param p0    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_authenticated_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/i/d;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_authenticated_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/swedbank/mobile/business/i/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/i/d;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/authentication/login/g;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "authenticated_user_id"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginCredentialsRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-interface {p0}, Lcom/swedbank/mobile/business/authentication/login/g;->a()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_authenticated_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
