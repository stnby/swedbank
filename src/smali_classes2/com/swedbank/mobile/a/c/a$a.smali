.class public interface abstract Lcom/swedbank/mobile/a/c/a$a;
.super Ljava/lang/Object;
.source "AuthenticationComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/c/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/a/c/a$a;
    .param p1    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_credentials"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/a/c/a$a;
    .param p1    # Lcom/swedbank/mobile/business/authentication/j;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/a/c/a$a;
    .param p1    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
