.class public final Lcom/swedbank/mobile/a/c/b/i;
.super Ljava/lang/Object;
.source "LoginModule_ProvideViewDetailsFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/architect/a/b/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/a/c/b/i;->a:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/swedbank/mobile/a/c/b/i;->b:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/swedbank/mobile/a/c/b/i;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;)",
            "Lcom/swedbank/mobile/a/c/b/i;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/swedbank/mobile/a/c/b/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/a/c/b/i;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;Ljavax/inject/Provider;Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ")",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .line 42
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/a/c/b/g;->a(Ljava/util/Map;Ljavax/inject/Provider;Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/a/b/f;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/b/i;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iget-object v1, p0, Lcom/swedbank/mobile/a/c/b/i;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/c/b/i;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/c;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/a/c/b/i;->a(Ljava/util/Map;Ljavax/inject/Provider;Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/c/b/i;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    return-object v0
.end method
