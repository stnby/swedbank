.class public final Lcom/swedbank/mobile/a/c/b/f;
.super Ljava/lang/Object;
.source "LoginMethodsModule_ProvideViewsFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/swedbank/mobile/architect/a/b/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/p;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/c/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/p;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/b/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/c/f;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/swedbank/mobile/a/c/b/f;->a:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/swedbank/mobile/a/c/b/f;->b:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/swedbank/mobile/a/c/b/f;->c:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/swedbank/mobile/a/c/b/f;->d:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/swedbank/mobile/a/c/b/f;->e:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/swedbank/mobile/a/c/b/f;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/p;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/b/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/c/f;",
            ">;)",
            "Lcom/swedbank/mobile/a/c/b/f;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/swedbank/mobile/a/c/b/f;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/a/c/b/f;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static a(Ljava/util/List;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/p;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/b/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/c/f;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;"
        }
    .end annotation

    .line 63
    invoke-static/range {p0 .. p5}, Lcom/swedbank/mobile/a/c/b/b;->a(Ljava/util/List;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/b/f;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/swedbank/mobile/a/c/b/f;->b:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/c/b/f;->c:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/c/b/f;->d:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/c/b/f;->e:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/c/b/f;->f:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/a/c/b/f;->a(Ljava/util/List;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/c/b/f;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
