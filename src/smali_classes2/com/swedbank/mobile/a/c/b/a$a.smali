.class public interface abstract Lcom/swedbank/mobile/a/c/b/a$a;
.super Ljava/lang/Object;
.source "LoginComponent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/c/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/c/b/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/a/c/b/a$a;
    .param p1    # Lcom/swedbank/mobile/architect/a/b/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/a/c/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/l;
        .annotation runtime Ljavax/inject/Named;
            value = "login_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "logged_in_customer_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/c/b/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;)",
            "Lcom/swedbank/mobile/a/c/b/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
