.class public final Lcom/swedbank/mobile/a/c/b/g;
.super Ljava/lang/Object;
.source "LoginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/c/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    new-instance v0, Lcom/swedbank/mobile/a/c/b/g;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/c/b/g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/c/b/g;->a:Lcom/swedbank/mobile/a/c/b/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/d;)Landroid/text/InputFilter;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "personal_code_formatter"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "countryConfiguration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-interface {p0}, Lcom/swedbank/mobile/business/c/d;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object p0

    sget-object v0, Lcom/swedbank/mobile/business/c/c;->b:Lcom/swedbank/mobile/business/c/c;

    if-ne p0, v0, :cond_0

    new-instance p0, Lcom/swedbank/mobile/app/b/b/d/b;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/b;-><init>()V

    :goto_0
    check-cast p0, Landroid/text/InputFilter;

    goto :goto_1

    :cond_0
    new-instance p0, Lcom/swedbank/mobile/app/b/b/d/a;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/a;-><init>()V

    goto :goto_0

    :goto_1
    return-object p0
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 7
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ")",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_login"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priority"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    .line 110
    new-instance v0, Lcom/swedbank/mobile/core/ui/a/i;

    invoke-direct {v0}, Lcom/swedbank/mobile/core/ui/a/i;-><init>()V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 111
    new-instance v1, Lcom/swedbank/mobile/core/ui/a/k;

    const v2, 0x7f0a0187

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/core/ui/a/k;-><init>(I)V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 109
    invoke-direct {v3, v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 56
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    const v2, 0x7f0d0087

    move-object v1, v0

    move-object v4, p2

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method
