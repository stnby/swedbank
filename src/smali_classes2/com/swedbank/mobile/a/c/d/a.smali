.class public final Lcom/swedbank/mobile/a/c/d/a;
.super Ljava/lang/Object;
.source "LastUsedLoginMethodRegistry.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/c/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/c/d/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/c/d/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/c/d/a;->a:Lcom/swedbank/mobile/a/c/d/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/a/d/b/a/d;Lcom/swedbank/mobile/a/c/d/b/e;Lcom/swedbank/mobile/a/c/d/a/d;Lcom/swedbank/mobile/architect/business/g;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/a/d/b/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/a/c/d/b/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/a/c/d/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "observeLastUsedLoginMethodUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/d/b/a/d;",
            "Lcom/swedbank/mobile/a/c/d/b/e;",
            "Lcom/swedbank/mobile/a/c/d/a/d;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;)",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_recurring_login_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "biometricLoginMethodPlugin"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pollingLoginMethodPlugin"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinCalculatorLoginMethodPlugin"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeLastUsedLoginMethod"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-interface {p3}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lio/reactivex/o;

    invoke-virtual {p3}, Lio/reactivex/o;->g()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/swedbank/mobile/business/authentication/login/c;

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/swedbank/mobile/a/c/d/b;->a:[I

    invoke-virtual {p3}, Lcom/swedbank/mobile/business/authentication/login/c;->ordinal()I

    move-result p3

    aget p3, v0, p3

    packed-switch p3, :pswitch_data_0

    .line 39
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    invoke-static {p2}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    goto :goto_1

    .line 38
    :pswitch_1
    invoke-static {p1}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    goto :goto_1

    .line 36
    :pswitch_2
    invoke-static {p0}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    goto :goto_1

    .line 35
    :goto_0
    :pswitch_3
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object p0

    :goto_1
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
