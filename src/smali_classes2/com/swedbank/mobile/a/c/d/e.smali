.class public final Lcom/swedbank/mobile/a/c/d/e;
.super Ljava/lang/Object;
.source "RecurringLoginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/c/d/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/swedbank/mobile/a/c/d/e;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/c/d/e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/c/d/e;->a:Lcom/swedbank/mobile/a/c/d/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 8
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_recurring_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_recurring_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_recurring_login"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$b;

    .line 42
    sget v2, Lcom/swedbank/mobile/core/a$e;->view_recurring_login:I

    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v1, v0

    move-object v4, p0

    move-object v5, p1

    .line 41
    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/architect/a/b/f$b;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method

.method public static final a(Ljava/util/Set;Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;
    .locals 1
    .param p0    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_out_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_recurring_login_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/i/d;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_logged_out_navigation_item_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loggedOutNavigationItemPlugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurringLoginPlugins"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/swedbank/mobile/business/i/d;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p0, p1}, Lkotlin/a/ac;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/i/d;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_logged_out_navigation_item_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 58
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_recurring_login_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 64
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
