.class public final Lcom/swedbank/mobile/a/c/d/c;
.super Ljava/lang/Object;
.source "LastUsedLoginMethodRegistry_ProvideLastUsedLoginMethodPluginsFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/swedbank/mobile/business/i/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/b/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/b/a/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/b/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/a/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/a/c/d/c;->a:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/swedbank/mobile/a/c/d/c;->b:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/swedbank/mobile/a/c/d/c;->c:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/swedbank/mobile/a/c/d/c;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/b/a/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/b/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/a/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/a/c/d/c;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/swedbank/mobile/a/c/d/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/a/c/d/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/a/d/b/a/d;Lcom/swedbank/mobile/a/c/d/b/e;Lcom/swedbank/mobile/a/c/d/a/d;Lcom/swedbank/mobile/architect/business/g;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/d/b/a/d;",
            "Lcom/swedbank/mobile/a/c/d/b/e;",
            "Lcom/swedbank/mobile/a/c/d/a/d;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;)",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/c/d/a;->a(Lcom/swedbank/mobile/a/d/b/a/d;Lcom/swedbank/mobile/a/c/d/b/e;Lcom/swedbank/mobile/a/c/d/a/d;Lcom/swedbank/mobile/architect/business/g;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/d/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/a/d/b/a/d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/c/d/c;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/a/c/d/b/e;

    iget-object v2, p0, Lcom/swedbank/mobile/a/c/d/c;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/a/c/d/a/d;

    iget-object v3, p0, Lcom/swedbank/mobile/a/c/d/c;->d:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/business/g;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/a/c/d/c;->a(Lcom/swedbank/mobile/a/d/b/a/d;Lcom/swedbank/mobile/a/c/d/b/e;Lcom/swedbank/mobile/a/c/d/a/d;Lcom/swedbank/mobile/architect/business/g;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/c/d/c;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
