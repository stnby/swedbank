.class public final Lcom/swedbank/mobile/a/c/e/b;
.super Ljava/lang/Object;
.source "SessionModule.kt"


# instance fields
.field private final a:Lretrofit2/r;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/authentication/b/t;Lretrofit2/r;Lokhttp3/x;Lokhttp3/u;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/authentication/b/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "not_authenticated"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lokhttp3/x;
        .annotation runtime Ljavax/inject/Named;
            value = "SWED_HTTP_CLIENT"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lokhttp3/u;
        .annotation runtime Ljavax/inject/Named;
            value = "session_requests_interceptor"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "logOutUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "refreshSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/authentication/b/t;",
            "Lretrofit2/r;",
            "Lokhttp3/x;",
            "Lokhttp3/u;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tokenAuthenticator"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notAuthenticatedRetrofit"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swedHttpClient"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionRequestsInterceptor"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logOut"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refreshSession"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lcom/swedbank/mobile/a/c/e/b;->b:Lcom/swedbank/mobile/architect/business/g;

    iput-object p6, p0, Lcom/swedbank/mobile/a/c/e/b;->c:Lcom/swedbank/mobile/architect/business/b;

    .line 28
    invoke-virtual {p2}, Lretrofit2/r;->a()Lretrofit2/r$a;

    move-result-object p2

    .line 30
    invoke-virtual {p3}, Lokhttp3/x;->A()Lokhttp3/x$a;

    move-result-object p3

    .line 31
    invoke-virtual {p3, p4}, Lokhttp3/x$a;->a(Lokhttp3/u;)Lokhttp3/x$a;

    move-result-object p3

    .line 32
    check-cast p1, Lokhttp3/b;

    invoke-virtual {p3, p1}, Lokhttp3/x$a;->a(Lokhttp3/b;)Lokhttp3/x$a;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lokhttp3/x$a;->b()Lokhttp3/x;

    move-result-object p1

    .line 29
    invoke-virtual {p2, p1}, Lretrofit2/r$a;->a(Lokhttp3/x;)Lretrofit2/r$a;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lretrofit2/r$a;->b()Lretrofit2/r;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/c/e/b;->a:Lretrofit2/r;

    return-void
.end method


# virtual methods
.method public final a()Lretrofit2/r;
    .locals 2
    .annotation runtime Ljavax/inject/Named;
        value = "session_authenticated"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/e/b;->a:Lretrofit2/r;

    const-string v1, "sessionAuthenticatedRetrofit"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "logOutUseCase"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/e/b;->b:Lcom/swedbank/mobile/architect/business/g;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/architect/business/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "refreshSessionUseCase"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/e/b;->c:Lcom/swedbank/mobile/architect/business/b;

    return-object v0
.end method
