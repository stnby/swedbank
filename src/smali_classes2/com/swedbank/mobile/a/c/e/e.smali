.class public final Lcom/swedbank/mobile/a/c/e/e;
.super Ljava/lang/Object;
.source "SessionModule_ProvideRefreshSessionUseCaseFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/c/e/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/e/b;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/a/c/e/e;->a:Lcom/swedbank/mobile/a/c/e/b;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/a/c/e/e;
    .locals 1

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/c/e/e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/c/e/e;-><init>(Lcom/swedbank/mobile/a/c/e/b;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/architect/business/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/c/e/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/c/e/b;->c()Lcom/swedbank/mobile/architect/business/b;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/business/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/a/c/e/e;->a:Lcom/swedbank/mobile/a/c/e/b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/e/e;->b(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/c/e/e;->a()Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    return-object v0
.end method
