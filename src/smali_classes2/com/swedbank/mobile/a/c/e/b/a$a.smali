.class public interface abstract Lcom/swedbank/mobile/a/c/e/b/a$a;
.super Ljava/lang/Object;
.source "ScopedSessionComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/c/e/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/c/e/b/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/a/c/e/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation runtime Ljavax/inject/Named;
            value = "session_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/c/e/b/a$a;
    .param p1    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "session_established_callback"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/s;",
            ">;)",
            "Lcom/swedbank/mobile/a/c/e/b/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
