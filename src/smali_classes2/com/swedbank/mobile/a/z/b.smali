.class public final Lcom/swedbank/mobile/a/z/b;
.super Ljava/lang/Object;
.source "RootModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/z/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 69
    new-instance v0, Lcom/swedbank/mobile/a/z/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/z/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/z/b;->a:Lcom/swedbank/mobile/a/z/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;
    .locals 1
    .param p0    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_root_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/i/d;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_root_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/swedbank/mobile/business/i/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/i/d;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_root_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 81
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
