.class public final Lcom/swedbank/mobile/a/x/a$a;
.super Ljava/lang/Object;
.source "PushOnboardingPluginModule.kt"

# interfaces
.implements Lcom/swedbank/mobile/a/s/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/a/x/a;->a(Lcom/swedbank/mobile/app/r/b/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/r/b/a;

.field final synthetic b:Lcom/swedbank/mobile/business/onboarding/f;

.field final synthetic c:Lcom/swedbank/mobile/architect/business/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/r/b/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/swedbank/mobile/a/x/a$a;->a:Lcom/swedbank/mobile/app/r/b/a;

    iput-object p2, p0, Lcom/swedbank/mobile/a/x/a$a;->b:Lcom/swedbank/mobile/business/onboarding/f;

    iput-object p3, p0, Lcom/swedbank/mobile/a/x/a$a;->c:Lcom/swedbank/mobile/architect/business/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "feature_push"

    return-object v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0xf

    return v0
.end method

.method public c()Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/a/x/a$a;->c:Lcom/swedbank/mobile/architect/business/g;

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/app/onboarding/i;
    .locals 10
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 51
    new-instance v9, Lcom/swedbank/mobile/app/onboarding/i;

    const-string v1, "feature_push"

    const/16 v2, 0xc8

    const v3, 0x7f11017b

    const v4, 0x7f11017a

    const v5, 0x7f100002

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/onboarding/i;-><init>(Ljava/lang/String;IIIIZILkotlin/e/b/g;)V

    return-object v9
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    const-class v0, Lcom/swedbank/mobile/business/push/onboarding/f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    new-instance v0, Lcom/swedbank/mobile/a/x/a$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/x/a$a$a;-><init>(Lcom/swedbank/mobile/a/x/a$a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 39
    invoke-static {p0}, Lcom/swedbank/mobile/a/s/f$a;->b(Lcom/swedbank/mobile/a/s/f;)Z

    move-result v0

    return v0
.end method
