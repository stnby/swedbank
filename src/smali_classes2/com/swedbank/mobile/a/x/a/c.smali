.class public final Lcom/swedbank/mobile/a/x/a/c;
.super Ljava/lang/Object;
.source "PushMessageHandlingModule_ProvideNotificationInformationProvidersFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/swedbank/mobile/business/push/handling/d;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/swedbank/mobile/a/x/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/swedbank/mobile/a/x/a/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/x/a/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/x/a/c;->a:Lcom/swedbank/mobile/a/x/a/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()Lcom/swedbank/mobile/a/x/a/c;
    .locals 1

    .line 18
    sget-object v0, Lcom/swedbank/mobile/a/x/a/c;->a:Lcom/swedbank/mobile/a/x/a/c;

    return-object v0
.end method

.method public static c()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;"
        }
    .end annotation

    .line 22
    invoke-static {}, Lcom/swedbank/mobile/a/x/a/b;->a()Ljava/util/Set;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;"
        }
    .end annotation

    .line 14
    invoke-static {}, Lcom/swedbank/mobile/a/x/a/c;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/x/a/c;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
