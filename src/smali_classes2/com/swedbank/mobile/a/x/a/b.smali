.class public final Lcom/swedbank/mobile/a/x/a/b;
.super Ljava/lang/Object;
.source "PushMessageHandlingModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/x/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/x/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/x/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/x/a/b;->a:Lcom/swedbank/mobile/a/x/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_notification_information_providers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
