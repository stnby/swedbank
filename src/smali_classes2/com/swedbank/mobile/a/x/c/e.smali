.class public final Lcom/swedbank/mobile/a/x/c/e;
.super Ljava/lang/Object;
.source "PushPreferencesModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/x/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/swedbank/mobile/a/x/c/e;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/x/c/e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/x/c/e;->a:Lcom/swedbank/mobile/a/x/c/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/a/x/c/d;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/a/x/c/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/x/c/d;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_preferences_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugin"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p0}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
