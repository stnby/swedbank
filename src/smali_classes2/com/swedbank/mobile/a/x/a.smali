.class public final Lcom/swedbank/mobile/a/x/a;
.super Ljava/lang/Object;
.source "PushOnboardingPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/x/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/swedbank/mobile/a/x/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/x/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/x/a;->a:Lcom/swedbank/mobile/a/x/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/r/b/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/r/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/business/onboarding/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isPushOnboardingNeededUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/r/b/a;",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/s/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_onboarding_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "builder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isOnboardingNeeded"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/swedbank/mobile/a/x/a$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/a/x/a$a;-><init>(Lcom/swedbank/mobile/app/r/b/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)V

    check-cast v0, Lcom/swedbank/mobile/a/s/f;

    return-object v0
.end method
