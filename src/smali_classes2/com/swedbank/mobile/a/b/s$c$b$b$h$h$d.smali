.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/k;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/overview/currencies/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            ")V"
        }
    .end annotation

    .line 5304
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5306
    invoke-direct {p0, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/overview/currencies/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/overview/currencies/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5280
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/overview/currencies/c;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/overview/currencies/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            ")V"
        }
    .end annotation

    .line 5313
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->b:Ljavax/inject/Provider;

    .line 5314
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->c:Ljavax/inject/Provider;

    .line 5315
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->e(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {p1, p3, v0, v1, v2}, Lcom/swedbank/mobile/business/overview/currencies/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/currencies/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->d:Ljavax/inject/Provider;

    .line 5316
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->d:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/currencies/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/currencies/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->e:Ljavax/inject/Provider;

    .line 5317
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->e:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->f:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 5318
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p3

    const-class v0, Lcom/swedbank/mobile/app/overview/currencies/k;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->f:Ljavax/inject/Provider;

    invoke-virtual {p3, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/f$a;->a()La/a/f;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->g:Ljavax/inject/Provider;

    .line 5319
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->h:Ljavax/inject/Provider;

    .line 5320
    invoke-static {}, Lcom/swedbank/mobile/a/b/l;->b()Lcom/swedbank/mobile/a/b/l;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/swedbank/mobile/app/overview/currencies/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/currencies/l;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->i:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 5321
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->i:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->j:Ljavax/inject/Provider;

    .line 5322
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->g:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->j:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/u/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/c/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->k:Ljavax/inject/Provider;

    .line 5323
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->d:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->k:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p2, p3, v0, v1}, Lcom/swedbank/mobile/app/overview/currencies/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/currencies/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->l:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5280
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->b()Lcom/swedbank/mobile/app/overview/currencies/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/currencies/h;
    .locals 1

    .line 5328
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;->l:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/currencies/h;

    return-object v0
.end method
