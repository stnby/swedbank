.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/c/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

.field private b:Ljava/lang/String;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/business/overview/currencies/c;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)V
    .locals 0

    .line 5246
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5246
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/currencies/c;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;
    .locals 0

    .line 5267
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/overview/currencies/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->d:Lcom/swedbank/mobile/business/overview/currencies/c;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;"
        }
    .end annotation

    .line 5261
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->c:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;
    .locals 0

    .line 5255
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/u/c/a;
    .locals 8

    .line 5273
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->b:Ljava/lang/String;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5274
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->c:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5275
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->d:Lcom/swedbank/mobile/business/overview/currencies/c;

    const-class v1, Lcom/swedbank/mobile/business/overview/currencies/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5276
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->c:Lcom/swedbank/mobile/business/util/l;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->d:Lcom/swedbank/mobile/business/overview/currencies/c;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/overview/currencies/c;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/overview/currencies/c;)Lcom/swedbank/mobile/a/u/c/a$a;
    .locals 0

    .line 5246
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->a(Lcom/swedbank/mobile/business/overview/currencies/c;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/c/a$a;
    .locals 0

    .line 5246
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/swedbank/mobile/a/u/c/a$a;
    .locals 0

    .line 5246
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;->a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;

    move-result-object p1

    return-object p1
.end method
