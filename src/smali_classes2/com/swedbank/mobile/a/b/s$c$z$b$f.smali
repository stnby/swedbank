.class final Lcom/swedbank/mobile/a/b/s$c$z$b$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/b/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$z$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$z$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/r;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/d/k;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/d/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;)V"
        }
    .end annotation

    .line 14468
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14470
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14439
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$z$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;)V"
        }
    .end annotation

    .line 14478
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->b:Ljavax/inject/Provider;

    .line 14479
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/cards/details/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/s;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->c:Ljavax/inject/Provider;

    .line 14480
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->d:Ljavax/inject/Provider;

    .line 14481
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->e:Ljavax/inject/Provider;

    .line 14482
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->f:Ljavax/inject/Provider;

    .line 14483
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->b:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->c:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->d:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->e:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->f:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, p4, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->g:Ljavax/inject/Provider;

    .line 14484
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->g:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/b/d/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/d/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->h:Ljavax/inject/Provider;

    .line 14485
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->h:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->i:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 14486
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class p3, Lcom/swedbank/mobile/app/cards/f/b/d/k;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->i:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->j:Ljavax/inject/Provider;

    .line 14487
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p2}, Lcom/swedbank/mobile/app/cards/f/b/d/m;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/d/m;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->k:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 14488
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->k:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->l:Ljavax/inject/Provider;

    .line 14489
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->j:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->l:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/e/g/b/d/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/g/b/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->m:Ljavax/inject/Provider;

    .line 14490
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->g:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->m:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/app/cards/f/b/d/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/d/h;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->n:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14439
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->b()Lcom/swedbank/mobile/app/cards/f/b/d/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/b/d/g;
    .locals 1

    .line 14495
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;->n:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/d/g;

    return-object v0
.end method
