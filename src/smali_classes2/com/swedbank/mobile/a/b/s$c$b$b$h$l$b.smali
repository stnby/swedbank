.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/g/c/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/search/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/c/a/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;)V
    .locals 0

    .line 6064
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6066
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6057
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;)V

    return-void
.end method

.method private c()V
    .locals 2

    .line 6071
    invoke-static {}, Lcom/swedbank/mobile/data/overview/search/a/c;->b()Lcom/swedbank/mobile/data/overview/search/a/c;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->b:Ljavax/inject/Provider;

    .line 6072
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/history/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/plugins/search/history/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->c:Ljavax/inject/Provider;

    .line 6073
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/overview/d/c/a/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/c/a/e;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6057
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->b()Lcom/swedbank/mobile/app/overview/d/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/d/c/a/d;
    .locals 1

    .line 6078
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/c/a/d;

    return-object v0
.end method
