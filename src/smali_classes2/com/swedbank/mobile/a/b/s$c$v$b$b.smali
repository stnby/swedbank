.class final Lcom/swedbank/mobile/a/b/s$c$v$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$v$b$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$b$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/f/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/j;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/q;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;)V
    .locals 0

    .line 3278
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3280
    invoke-direct {p0, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a(Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3255
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$v$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;)V
    .locals 7

    .line 3287
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->b:Ljavax/inject/Provider;

    .line 3292
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->c:Ljavax/inject/Provider;

    .line 3293
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->d:Ljavax/inject/Provider;

    .line 3298
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->e:Ljavax/inject/Provider;

    .line 3299
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->f:Ljavax/inject/Provider;

    .line 3300
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->g:Ljavax/inject/Provider;

    .line 3301
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->h:Ljavax/inject/Provider;

    .line 3302
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/login/r;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/login/r;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->i:Ljavax/inject/Provider;

    .line 3303
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->f:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->h:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->a(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->i:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->b(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/business/authentication/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/h;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->j:Ljavax/inject/Provider;

    .line 3304
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->j:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->j:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->j:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/b/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->k:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3255
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->b()Lcom/swedbank/mobile/app/b/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/c;
    .locals 1

    .line 3309
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;->k:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/c;

    return-object v0
.end method
