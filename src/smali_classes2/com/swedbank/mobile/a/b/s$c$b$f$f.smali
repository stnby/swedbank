.class final Lcom/swedbank/mobile/a/b/s$c$b$f$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/s/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/b/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ")V"
        }
    .end annotation

    .line 11075
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11077
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a(Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11039
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$f$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V

    return-void
.end method

.method private a(Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ")V"
        }
    .end annotation

    .line 11083
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->b:Ljavax/inject/Provider;

    .line 11088
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/onboarding/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->c:Ljavax/inject/Provider;

    .line 11089
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->d:Ljavax/inject/Provider;

    .line 11090
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$f;->a(Lcom/swedbank/mobile/a/b/s$c$b$f;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->d:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/s/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/b/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->e:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 11091
    invoke-static {p1, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/a/s/b/c;->b()Lcom/swedbank/mobile/a/s/b/c;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/a/ae/f;->b()Lcom/swedbank/mobile/a/ae/f;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->f:Ljavax/inject/Provider;

    .line 11092
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/s/b/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->g:Ljavax/inject/Provider;

    .line 11093
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->h:Ljavax/inject/Provider;

    .line 11094
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->e:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/s/b/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/b/g;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->i:Ljavax/inject/Provider;

    .line 11095
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->f:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/s/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/b/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->j:Ljavax/inject/Provider;

    .line 11096
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->i:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->j:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->h:Ljavax/inject/Provider;

    invoke-static {p2, v0, v1, v2}, Lcom/swedbank/mobile/business/onboarding/intro/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/onboarding/intro/b;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->k:Ljavax/inject/Provider;

    .line 11097
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->k:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/onboarding/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/b/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->l:Ljavax/inject/Provider;

    .line 11098
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->l:Ljavax/inject/Provider;

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->m:Ljavax/inject/Provider;

    .line 11099
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/onboarding/b/h;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->m:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->n:Ljavax/inject/Provider;

    .line 11100
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p2}, Lcom/swedbank/mobile/app/onboarding/b/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/b/i;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->o:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 11101
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->o:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->p:Ljavax/inject/Provider;

    .line 11102
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->n:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->p:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/s/b/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/b/h;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->q:Ljavax/inject/Provider;

    .line 11103
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->h:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->k:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->q:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/onboarding/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/b/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->r:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11039
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->b()Lcom/swedbank/mobile/app/onboarding/b/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/onboarding/b/e;
    .locals 1

    .line 11108
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->r:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/onboarding/b/e;

    return-object v0
.end method
