.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/e/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "p"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/plugins/c/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V
    .locals 0

    .line 7380
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7382
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7375
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    return-void
.end method

.method private c()V
    .locals 4

    .line 7387
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->e(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/plugins/services/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/plugins/services/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->b:Ljavax/inject/Provider;

    .line 7388
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/transfer/plugins/c/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/plugins/c/k;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->c:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7375
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->b()Lcom/swedbank/mobile/app/transfer/plugins/c/j;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/plugins/c/j;
    .locals 1

    .line 7393
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/plugins/c/j;

    return-object v0
.end method
