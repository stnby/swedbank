.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/e/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/d/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;Lcom/swedbank/mobile/business/cards/order/b;)V
    .locals 0

    .line 7898
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7900
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a(Lcom/swedbank/mobile/business/cards/order/b;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;Lcom/swedbank/mobile/business/cards/order/b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7887
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;Lcom/swedbank/mobile/business/cards/order/b;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/cards/order/b;)V
    .locals 4

    .line 7905
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->b:Ljavax/inject/Provider;

    .line 7910
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->c:Ljavax/inject/Provider;

    .line 7911
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->d:Ljavax/inject/Provider;

    .line 7912
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->b(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->d:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/cards/order/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/order/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->e:Ljavax/inject/Provider;

    .line 7913
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->e:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {p1, v0, v1, v2, v3}, Lcom/swedbank/mobile/app/cards/d/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/d/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->f:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7887
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->b()Lcom/swedbank/mobile/app/cards/d/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/d/d;
    .locals 1

    .line 7918
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/d/d;

    return-object v0
.end method
