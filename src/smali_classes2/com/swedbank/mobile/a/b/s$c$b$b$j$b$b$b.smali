.class final Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/d/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 0

    .line 9929
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9931
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->a(Lcom/swedbank/mobile/business/preferences/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 9916
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 6

    .line 9936
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->b:Ljavax/inject/Provider;

    .line 9941
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->c:Ljavax/inject/Provider;

    .line 9942
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/onboarding/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->d:Ljavax/inject/Provider;

    .line 9943
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->e:Ljavax/inject/Provider;

    .line 9944
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->e:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/biometric/preference/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/preference/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->f:Ljavax/inject/Provider;

    .line 9945
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->d:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->f:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->f:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->f:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/c/d/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/d/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->g:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 9916
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->b()Lcom/swedbank/mobile/app/c/d/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/c/d/e;
    .locals 1

    .line 9950
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/c/d/e;

    return-object v0
.end method
