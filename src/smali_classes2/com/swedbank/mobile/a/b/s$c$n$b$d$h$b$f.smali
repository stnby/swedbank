.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;
.super Lcom/swedbank/mobile/a/e/g/a/e/a;
.source "DaggerApplicationComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/h;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V
    .locals 0

    .line 12542
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    invoke-direct {p0}, Lcom/swedbank/mobile/a/e/g/a/e/a;-><init>()V

    .line 12544
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->f()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12525
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V

    return-void
.end method

.method private f()V
    .locals 4

    .line 12549
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->V(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->b(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->b:Ljavax/inject/Provider;

    .line 12550
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/e/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/e/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->c:Ljavax/inject/Provider;

    .line 12551
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->c:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->d:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 12552
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->d:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->e:Ljavax/inject/Provider;

    .line 12553
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/a/e/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/e/i;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->f:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 12554
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->f:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->g:Ljavax/inject/Provider;

    .line 12555
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->e:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->g:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/e/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/g/a/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->h:Ljavax/inject/Provider;

    .line 12556
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/a/e/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/e/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12525
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->b()Lcom/swedbank/mobile/app/cards/f/a/e/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/e/e;
    .locals 1

    .line 12561
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/e/e;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;
    .locals 1

    .line 12565
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;

    return-object v0
.end method

.method public synthetic d()Lcom/swedbank/mobile/business/cards/wallet/onboarding/l;
    .locals 1

    .line 12525
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;->c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;

    move-result-object v0

    return-object v0
.end method
