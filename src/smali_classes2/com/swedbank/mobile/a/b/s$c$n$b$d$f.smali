.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/notauth/empty/NotAuthenticatedEmptyCardsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)V
    .locals 0

    .line 12695
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12697
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12676
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)V

    return-void
.end method

.method private c()V
    .locals 4

    const/4 v0, 0x1

    .line 12702
    invoke-static {v0, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$n$b;->a(Lcom/swedbank/mobile/a/b/s$c$n$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/a/e/d/a/d;->b()Lcom/swedbank/mobile/a/e/d/a/d;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->b:Ljavax/inject/Provider;

    .line 12703
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->b:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/e/d/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/a/c;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->c:Ljavax/inject/Provider;

    .line 12704
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->c:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/notauth/empty/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/notauth/empty/b;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->d:Ljavax/inject/Provider;

    .line 12705
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->d:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/c/a/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->e:Ljavax/inject/Provider;

    .line 12706
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->e:Ljavax/inject/Provider;

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->f:Ljavax/inject/Provider;

    .line 12707
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/cards/c/a/h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->f:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->g:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 12708
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/app/cards/c/a/i;->b()Lcom/swedbank/mobile/app/cards/c/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->h:Ljavax/inject/Provider;

    .line 12709
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->h:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/e/d/a/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->i:Ljavax/inject/Provider;

    .line 12710
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->d:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->i:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/cards/c/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/a/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12676
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->b()Lcom/swedbank/mobile/app/cards/c/a/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/c/a/e;
    .locals 1

    .line 12715
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$f;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/c/a/e;

    return-object v0
.end method
