.class public final Lcom/swedbank/mobile/a/b/a;
.super Ljava/lang/Object;
.source "AppModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 74
    new-instance v0, Lcom/swedbank/mobile/a/b/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/b/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/b/a;->a:Lcom/swedbank/mobile/a/b/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Landroid/app/Application;)Lcom/swedbank/mobile/a/c/a/b;
    .locals 1
    .param p0    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "app"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    check-cast p0, Lcom/swedbank/mobile/a/c/a/b;

    return-object p0
.end method

.method public static final a()Lcom/swedbank/mobile/architect/a/b/g;
    .locals 1
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 83
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/g;

    invoke-direct {v0}, Lcom/swedbank/mobile/architect/a/b/g;-><init>()V

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/c/f;)Lcom/swedbank/mobile/business/c/b;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/business/c/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "countryConfiguration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endpointConfiguration"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/swedbank/mobile/business/c/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/c/b;-><init>(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/c/f;)V

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/app/g/h;)Lio/reactivex/o;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/g/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/g/h;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "backManager"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/g/h;->c()Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/app/r/a;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/app/r/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/r/a;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "intent_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "genericPushNotificationIntentHandler"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 122
    new-array v0, v0, [Lcom/swedbank/mobile/app/r/a;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/app/customer/g;Lcom/swedbank/mobile/app/customer/i;)Lkotlin/e/a/b;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/app/customer/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/customer/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/c/a;",
            "Lcom/swedbank/mobile/app/customer/g;",
            "Lcom/swedbank/mobile/app/customer/i;",
            ")",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ltImpl"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lvImpl"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-interface {p0}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object p0

    sget-object v0, Lcom/swedbank/mobile/a/b/b;->a:[I

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/g;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 148
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/swedbank/mobile/a/b/a$a;->a:Lcom/swedbank/mobile/a/b/a$a;

    check-cast p0, Lkotlin/e/a/b;

    goto :goto_0

    .line 144
    :pswitch_1
    move-object p0, p2

    check-cast p0, Lkotlin/e/a/b;

    goto :goto_0

    .line 143
    :pswitch_2
    move-object p0, p1

    check-cast p0, Lkotlin/e/a/b;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static final b(Landroid/app/Application;)Landroid/app/KeyguardManager;
    .locals 1
    .param p0    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "app"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keyguard"

    .line 99
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/app/KeyguardManager;

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.app.KeyguardManager"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final b()Lcom/swedbank/mobile/architect/business/a;
    .locals 3
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    new-instance v0, Lcom/swedbank/mobile/architect/business/a;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v1}, Lcom/swedbank/mobile/architect/business/a;-><init>(Lio/reactivex/v;ILkotlin/e/b/g;)V

    return-object v0
.end method

.method public static final b(Lcom/swedbank/mobile/app/r/a;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/app/r/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/r/a;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "push_message_notification_information_providers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "genericPushNotificationProvider"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 130
    new-array v0, v0, [Lcom/swedbank/mobile/app/r/a;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Landroid/app/Application;)Landroid/content/ClipboardManager;
    .locals 1
    .param p0    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "app"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    .line 104
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/content/ClipboardManager;

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.content.ClipboardManager"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final c()Lcom/swedbank/mobile/core/ui/aj;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 134
    new-instance v0, Lcom/swedbank/mobile/core/ui/ak;

    invoke-direct {v0}, Lcom/swedbank/mobile/core/ui/ak;-><init>()V

    check-cast v0, Lcom/swedbank/mobile/core/ui/aj;

    return-object v0
.end method
