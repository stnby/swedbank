.class final Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/c/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$d$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$d$f;

.field private b:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)V
    .locals 0

    .line 13340
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 13340
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;
    .locals 0

    .line 13348
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->b:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;"
        }
    .end annotation

    .line 13355
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->c:Ljava/util/List;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/e/g/c/a;
    .locals 5

    .line 13361
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->b:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 13362
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->c:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 13363
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->b:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->c:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/a/e/g/c/a$a;
    .locals 0

    .line 13340
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/c/a$a;
    .locals 0

    .line 13340
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;->a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;

    move-result-object p1

    return-object p1
.end method
