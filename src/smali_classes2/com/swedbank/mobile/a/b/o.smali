.class public final Lcom/swedbank/mobile/a/b/o;
.super Ljava/lang/Object;
.source "BuildTypeModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/b/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/a/b/o;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/b/o;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/b/o;->a:Lcom/swedbank/mobile/a/b/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Lcom/swedbank/mobile/business/c/d;
    .locals 1
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    sget-object v0, Lcom/swedbank/mobile/data/b/d;->a:Lcom/swedbank/mobile/data/b/d;

    check-cast v0, Lcom/swedbank/mobile/business/c/d;

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/f/a;)Lcom/swedbank/mobile/business/c/f;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "countryConfiguration"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/swedbank/mobile/data/b/e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/data/b/e;-><init>(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/f/a;)V

    check-cast v0, Lcom/swedbank/mobile/business/c/f;

    return-object v0
.end method

.method public static final b()Lcom/swedbank/mobile/business/general/a;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    new-instance v0, Lcom/swedbank/mobile/business/general/a;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;ILkotlin/e/b/g;)V

    return-object v0
.end method
