.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/g/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/statement/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/search/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/d/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V
    .locals 0

    .line 5640
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5642
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5631
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V

    return-void
.end method

.method private c()V
    .locals 7

    .line 5647
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->d(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/statement/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/statement/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->b:Ljavax/inject/Provider;

    .line 5648
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->e(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/search/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/search/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->c:Ljavax/inject/Provider;

    .line 5649
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->f(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->g(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {}, Lcom/swedbank/mobile/a/u/a/c;->b()Lcom/swedbank/mobile/a/u/a/c;

    move-result-object v5

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/plugins/statement/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->d:Ljavax/inject/Provider;

    .line 5650
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->d:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->d:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->d:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/overview/d/d/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/d/k;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->e:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5631
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->b()Lcom/swedbank/mobile/app/overview/d/d/j;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/d/d/j;
    .locals 1

    .line 5655
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/d/j;

    return-object v0
.end method
