.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/h/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/c/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V
    .locals 0

    .line 7115
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7117
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7110
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V

    return-void
.end method

.method private c()V
    .locals 2

    .line 7122
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/transfer/search/remote/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/search/remote/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->b:Ljavax/inject/Provider;

    .line 7123
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/search/c/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/c/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->c:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7110
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->b()Lcom/swedbank/mobile/app/transfer/search/c/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/search/c/c;
    .locals 1

    .line 7128
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/search/c/c;

    return-object v0
.end method
