.class public interface abstract Lcom/swedbank/mobile/a/b/n;
.super Ljava/lang/Object;
.source "ApplicationComponent.kt"

# interfaces
.implements Lcom/swedbank/mobile/a/ae/c;
.implements Lcom/swedbank/mobile/core/a/d;
.implements Lcom/swedbank/mobile/data/a;
.implements Lcom/swedbank/mobile/data/device/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/n$a;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/architect/a/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a(Lcom/swedbank/mobile/app/MainActivity;)V
    .param p1    # Lcom/swedbank/mobile/app/MainActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/swedbank/mobile/data/push/PushMessageListenerService;)V
    .param p1    # Lcom/swedbank/mobile/data/push/PushMessageListenerService;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract b()Lcom/swedbank/mobile/architect/business/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
