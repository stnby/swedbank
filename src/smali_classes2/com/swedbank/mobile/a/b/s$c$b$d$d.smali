.class final Lcom/swedbank/mobile/a/b/s$c$b$d$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/y/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/retry/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/retry/RetryInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/s/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/s/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/s/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$d;Lcom/swedbank/mobile/business/general/retry/c;Lcom/swedbank/mobile/app/s/c;)V
    .locals 0

    .line 4722
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4724
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->a(Lcom/swedbank/mobile/business/general/retry/c;Lcom/swedbank/mobile/app/s/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$d;Lcom/swedbank/mobile/business/general/retry/c;Lcom/swedbank/mobile/app/s/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 4703
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$d$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$d;Lcom/swedbank/mobile/business/general/retry/c;Lcom/swedbank/mobile/app/s/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/general/retry/c;Lcom/swedbank/mobile/app/s/c;)V
    .locals 2

    .line 4730
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->b:Ljavax/inject/Provider;

    .line 4731
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/general/retry/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/general/retry/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->c:Ljavax/inject/Provider;

    .line 4732
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->d:Ljavax/inject/Provider;

    .line 4733
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->d:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/s/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/s/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->e:Ljavax/inject/Provider;

    .line 4734
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->e:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->f:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 4735
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/s/i;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->f:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->g:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 4736
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/app/s/j;->b()Lcom/swedbank/mobile/app/s/j;

    move-result-object p2

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->h:Ljavax/inject/Provider;

    .line 4737
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->g:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->h:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/y/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/y/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->i:Ljavax/inject/Provider;

    .line 4738
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->i:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/s/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/s/g;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 4703
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->b()Lcom/swedbank/mobile/app/s/f;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/s/f;
    .locals 1

    .line 4743
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$d;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/s/f;

    return-object v0
.end method
