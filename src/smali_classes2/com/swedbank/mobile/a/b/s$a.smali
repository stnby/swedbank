.class final Lcom/swedbank/mobile/a/b/s$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/b/n$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/app/Application;

.field private b:Lcom/swedbank/mobile/data/i;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2481
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$a;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/app/Application;)Lcom/swedbank/mobile/a/b/n$a;
    .locals 0

    .line 2481
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$a;->b(Landroid/app/Application;)Lcom/swedbank/mobile/a/b/s$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/a/b/n$a;
    .locals 0

    .line 2481
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$a;->b(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/a/b/s$a;

    move-result-object p1

    return-object p1
.end method

.method public a()Lcom/swedbank/mobile/a/b/n;
    .locals 4

    .line 2500
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$a;->a:Landroid/app/Application;

    const-class v1, Landroid/app/Application;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 2501
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$a;->b:Lcom/swedbank/mobile/data/i;

    const-class v1, Lcom/swedbank/mobile/data/i;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 2502
    new-instance v0, Lcom/swedbank/mobile/a/b/s;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$a;->b:Lcom/swedbank/mobile/data/i;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$a;->a:Landroid/app/Application;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/a/b/s;-><init>(Lcom/swedbank/mobile/data/i;Landroid/app/Application;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Landroid/app/Application;)Lcom/swedbank/mobile/a/b/s$a;
    .locals 0

    .line 2488
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$a;->a:Landroid/app/Application;

    return-object p0
.end method

.method public b(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/a/b/s$a;
    .locals 0

    .line 2494
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/data/i;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$a;->b:Lcom/swedbank/mobile/data/i;

    return-object p0
.end method
