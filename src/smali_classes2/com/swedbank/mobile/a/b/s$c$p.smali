.class final Lcom/swedbank/mobile/a/b/s$c$p;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/q/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "p"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$p$b;,
        Lcom/swedbank/mobile/a/b/s$c$p$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/notify/NotifyUserInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/n/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c;)V
    .locals 0

    .line 14591
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$p;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14593
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$p;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14582
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$p;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    return-void
.end method

.method private c()V
    .locals 4

    .line 14598
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$p$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$p$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$p;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->b:Ljavax/inject/Provider;

    .line 14603
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->c:Ljavax/inject/Provider;

    .line 14604
    invoke-static {}, Lcom/swedbank/mobile/business/notify/e;->b()Lcom/swedbank/mobile/business/notify/e;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->d:Ljavax/inject/Provider;

    .line 14605
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$p;->d:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$p;->d:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$p;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/n/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/n/e;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->e:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14582
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$p;->b()Lcom/swedbank/mobile/app/n/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/n/d;
    .locals 1

    .line 14610
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/n/d;

    return-object v0
.end method
