.class final Lcom/swedbank/mobile/a/b/s$c$b$j;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ae/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "j"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/e;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/customer/g;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/customer/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/a/k;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 0

    .line 11679
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11681
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$j;->a(Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11652
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$j;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/i/c;)V
    .locals 6

    .line 11686
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/a/b;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->b:Ljavax/inject/Provider;

    .line 11687
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->Q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->b:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/data/account/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/account/d;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->c:Ljavax/inject/Provider;

    .line 11688
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/i/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/i/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->d:Ljavax/inject/Provider;

    .line 11689
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c;->b(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->c:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/data/customer/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/customer/f;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->e:Ljavax/inject/Provider;

    .line 11690
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->f:Ljavax/inject/Provider;

    .line 11691
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->Z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->e:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->f:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/widget/configuration/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/widget/configuration/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->g:Ljavax/inject/Provider;

    .line 11692
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->g:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/widget/a/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/a/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->h:Ljavax/inject/Provider;

    .line 11693
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->h:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->i:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 11694
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/widget/a/k;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->i:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->j:Ljavax/inject/Provider;

    .line 11695
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/widget/a/l;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->k:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 11696
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->k:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->l:Ljavax/inject/Provider;

    .line 11697
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->j:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->l:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/ae/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ae/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->m:Ljavax/inject/Provider;

    .line 11698
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->m:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/widget/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/a/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->n:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11652
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$j;->b()Lcom/swedbank/mobile/app/widget/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/widget/a/h;
    .locals 1

    .line 11703
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$j;->n:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/widget/a/h;

    return-object v0
.end method
