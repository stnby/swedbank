.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/e/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "l"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/g;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/plugins/predefined/TransferPredefinedInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/plugins/b/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/g;)V
    .locals 0

    .line 7499
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7501
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->a(Lcom/swedbank/mobile/business/transfer/g;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/g;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7492
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/g;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/transfer/g;)V
    .locals 2

    .line 7506
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->b:Ljavax/inject/Provider;

    .line 7507
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->b:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/plugins/predefined/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/plugins/predefined/c;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->c:Ljavax/inject/Provider;

    .line 7508
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/transfer/plugins/b/m;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/plugins/b/m;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7492
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->b()Lcom/swedbank/mobile/app/transfer/plugins/b/l;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/plugins/b/l;
    .locals 1

    .line 7513
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/plugins/b/l;

    return-object v0
.end method
