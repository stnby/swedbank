.class final Lcom/swedbank/mobile/a/b/s$c$b$f$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/x/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$f$h$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$h$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/push/onboarding/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/b/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/business/util/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/push/onboarding/e;",
            ">;)V"
        }
    .end annotation

    .line 11536
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11538
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a(Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11512
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$f$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/util/e;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/push/onboarding/e;",
            ">;)V"
        }
    .end annotation

    .line 11544
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$h$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$h$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->b:Ljavax/inject/Provider;

    .line 11549
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/onboarding/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->c:Ljavax/inject/Provider;

    .line 11550
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b;->g(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b;->d(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/push/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->d:Ljavax/inject/Provider;

    .line 11551
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->e:Ljavax/inject/Provider;

    .line 11552
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->e:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/push/onboarding/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/onboarding/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->f:Ljavax/inject/Provider;

    .line 11553
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->f:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/r/b/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/b/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->g:Ljavax/inject/Provider;

    .line 11554
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->g:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->h:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 11555
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/r/b/k;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->h:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->i:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 11556
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/app/r/b/l;->b()Lcom/swedbank/mobile/app/r/b/l;

    move-result-object v0

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->j:Ljavax/inject/Provider;

    .line 11557
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->i:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->j:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/x/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/x/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->k:Ljavax/inject/Provider;

    .line 11558
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->f:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->f:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->k:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {p1, v0, v1, v2, v3}, Lcom/swedbank/mobile/app/r/b/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/b/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->l:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11512
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->b()Lcom/swedbank/mobile/app/r/b/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/r/b/h;
    .locals 1

    .line 11563
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$h;->l:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/r/b/h;

    return-object v0
.end method
