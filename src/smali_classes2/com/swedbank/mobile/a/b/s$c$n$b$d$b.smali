.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/g/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/j;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/app/f/c;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .line 12641
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12643
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->a(Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/app/f/c;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/app/f/c;Ljava/lang/Boolean;Ljava/lang/String;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12614
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/app/f/c;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/app/f/c;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 1

    .line 12650
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->b:Ljavax/inject/Provider;

    .line 12651
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->c:Ljavax/inject/Provider;

    .line 12652
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->b:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->c:Ljavax/inject/Provider;

    invoke-static {p1, p4}, Lcom/swedbank/mobile/business/general/confirmation/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/general/confirmation/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->d:Ljavax/inject/Provider;

    .line 12653
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->e:Ljavax/inject/Provider;

    .line 12654
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->d:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->e:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/f/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->f:Ljavax/inject/Provider;

    .line 12655
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->f:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->g:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 12656
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class p4, Lcom/swedbank/mobile/app/f/j;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->g:Ljavax/inject/Provider;

    invoke-virtual {p2, p4, v0}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->h:Ljavax/inject/Provider;

    .line 12657
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->i:Ljavax/inject/Provider;

    .line 12658
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->i:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/f/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/k;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->j:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 12659
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->j:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->k:Ljavax/inject/Provider;

    .line 12660
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->h:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->k:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/g/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/g/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->l:Ljavax/inject/Provider;

    .line 12661
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->d:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->l:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/app/f/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/h;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->m:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12614
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->b()Lcom/swedbank/mobile/app/f/f;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/f/f;
    .locals 1

    .line 12666
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$b;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/f/f;

    return-object v0
.end method
