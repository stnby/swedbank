.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$k;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$j;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$i;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/detailed/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/j/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/e;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/g;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/l/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/k/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/g;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/r;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/transaction/o;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V
    .locals 0

    .line 5018
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5020
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 4975
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->n:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->k:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->p:Ljavax/inject/Provider;

    return-object p0
.end method

.method private c()V
    .locals 9

    .line 5025
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b:Ljavax/inject/Provider;

    .line 5030
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/detailed/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/detailed/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->c:Ljavax/inject/Provider;

    .line 5031
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->d:Ljavax/inject/Provider;

    .line 5036
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->e:Ljavax/inject/Provider;

    .line 5037
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->f:Ljavax/inject/Provider;

    .line 5042
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->g:Ljavax/inject/Provider;

    .line 5043
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$4;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->h:Ljavax/inject/Provider;

    .line 5048
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->h:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->i:Ljavax/inject/Provider;

    .line 5049
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/a/b;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->j:Ljavax/inject/Provider;

    .line 5050
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->Q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->j:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/data/account/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/account/d;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->k:Ljavax/inject/Provider;

    .line 5051
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->R(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/overview/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/j;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->l:Ljavax/inject/Provider;

    .line 5052
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->e:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->g:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->i:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->l:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->l:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/overview/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/h;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->m:Ljavax/inject/Provider;

    .line 5053
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$5;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->n:Ljavax/inject/Provider;

    const/4 v0, 0x1

    const/4 v1, 0x2

    .line 5058
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->c(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->b(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/a/u/e;->b()Lcom/swedbank/mobile/a/u/e;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->o:Ljavax/inject/Provider;

    .line 5059
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->p:Ljavax/inject/Provider;

    .line 5060
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$6;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->q:Ljavax/inject/Provider;

    .line 5065
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$7;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$7;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->r:Ljavax/inject/Provider;

    .line 5070
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->s:Ljavax/inject/Provider;

    .line 5071
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/m/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/m/b;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->t:Ljavax/inject/Provider;

    .line 5072
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->t:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/data/overview/transaction/q;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/overview/transaction/q;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->u:Ljavax/inject/Provider;

    .line 5073
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->s:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->k:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->u:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/data/overview/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/overview/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->v:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->q:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->r:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic f(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->v:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic g(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->u:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic h(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;
    .locals 0

    .line 4975
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->c:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 4975
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b()Lcom/swedbank/mobile/app/overview/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/g;
    .locals 1

    .line 5078
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/g;

    return-object v0
.end method
