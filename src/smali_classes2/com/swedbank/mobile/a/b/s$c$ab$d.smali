.class final Lcom/swedbank/mobile/a/b/s$c$ab$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ae/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$ab;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/scoped/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/scoped/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/d/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lcom/swedbank/mobile/a/c/e/b;Lcom/swedbank/mobile/business/widget/update/e;Ljava/lang/Boolean;)V
    .locals 0

    .line 15101
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15103
    invoke-direct {p0, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$ab$d;->a(Lcom/swedbank/mobile/a/c/e/b;Lcom/swedbank/mobile/business/widget/update/e;Ljava/lang/Boolean;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lcom/swedbank/mobile/a/c/e/b;Lcom/swedbank/mobile/business/widget/update/e;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 15081
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$ab$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lcom/swedbank/mobile/a/c/e/b;Lcom/swedbank/mobile/business/widget/update/e;Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/a/c/e/b;Lcom/swedbank/mobile/business/widget/update/e;Ljava/lang/Boolean;)V
    .locals 1

    .line 15109
    invoke-static {p1}, Lcom/swedbank/mobile/a/c/e/f;->a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/a/c/e/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->b:Ljavax/inject/Provider;

    .line 15110
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/a/a/b;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->c:Ljavax/inject/Provider;

    .line 15111
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/a/b;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->d:Ljavax/inject/Provider;

    .line 15112
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->Q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->d:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/account/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/account/d;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->e:Ljavax/inject/Provider;

    .line 15113
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->e:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/account/scoped/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/account/scoped/b;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->f:Ljavax/inject/Provider;

    .line 15114
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->g:Ljavax/inject/Provider;

    .line 15115
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->h:Ljavax/inject/Provider;

    .line 15116
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->Z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->f:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->g:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->h:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, v0}, Lcom/swedbank/mobile/business/widget/update/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/widget/update/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->i:Ljavax/inject/Provider;

    .line 15117
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->i:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/widget/d/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/d/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 15081
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$ab$d;->b()Lcom/swedbank/mobile/app/widget/d/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/widget/d/c;
    .locals 1

    .line 15122
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$d;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/widget/d/c;

    return-object v0
.end method
