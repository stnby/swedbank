.class final Lcom/swedbank/mobile/a/b/s$c$v$b$e;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/b/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b;

.field private b:Lcom/swedbank/mobile/business/authentication/login/l;

.field private c:Lcom/swedbank/mobile/architect/a/b/c;

.field private d:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V
    .locals 0

    .line 2966
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2966
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$e;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;
    .locals 0

    .line 2983
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/architect/a/b/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->c:Lcom/swedbank/mobile/architect/a/b/c;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;
    .locals 0

    .line 2977
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->b:Lcom/swedbank/mobile/business/authentication/login/l;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$v$b$e;"
        }
    .end annotation

    .line 2990
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->d:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/c/b/a;
    .locals 9

    .line 3002
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->b:Lcom/swedbank/mobile/business/authentication/login/l;

    const-class v1, Lcom/swedbank/mobile/business/authentication/login/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3003
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->c:Lcom/swedbank/mobile/architect/a/b/c;

    const-class v1, Lcom/swedbank/mobile/architect/a/b/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3004
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->d:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3005
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->e:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3006
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->b:Lcom/swedbank/mobile/business/authentication/login/l;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->c:Lcom/swedbank/mobile/architect/a/b/c;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->d:Lcom/swedbank/mobile/business/util/l;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->e:Lcom/swedbank/mobile/business/util/l;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$v$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/a/b/c;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$v$b$e;"
        }
    .end annotation

    .line 2996
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->e:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/a/c/b/a$a;
    .locals 0

    .line 2966
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->a(Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/a/c/b/a$a;
    .locals 0

    .line 2966
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->a(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a$a;
    .locals 0

    .line 2966
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a$a;
    .locals 0

    .line 2966
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$e;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$e;

    move-result-object p1

    return-object p1
.end method
