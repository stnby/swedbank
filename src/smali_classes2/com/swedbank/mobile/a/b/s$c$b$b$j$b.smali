.class final Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/i/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/v/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/selection/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/customer/selection/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/customer/selection/j;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/customer/selection/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j;Lcom/swedbank/mobile/business/customer/selection/c;)V
    .locals 0

    .line 9633
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9635
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a(Lcom/swedbank/mobile/business/customer/selection/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j;Lcom/swedbank/mobile/business/customer/selection/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 9608
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j;Lcom/swedbank/mobile/business/customer/selection/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/customer/selection/c;)V
    .locals 7

    .line 9640
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->b:Ljavax/inject/Provider;

    .line 9645
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/p/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->c:Ljavax/inject/Provider;

    .line 9646
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/a/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->d:Ljavax/inject/Provider;

    .line 9647
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->e:Ljavax/inject/Provider;

    .line 9648
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->h(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->d:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->e:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->f(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->b(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/customer/selection/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/customer/selection/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->f:Ljavax/inject/Provider;

    .line 9649
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->f:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/customer/selection/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/customer/selection/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->g:Ljavax/inject/Provider;

    .line 9650
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->g:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->h:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 9651
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/customer/selection/j;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->h:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->i:Ljavax/inject/Provider;

    .line 9652
    invoke-static {}, Lcom/swedbank/mobile/a/b/l;->b()Lcom/swedbank/mobile/a/b/l;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/customer/selection/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/customer/selection/l;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->j:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 9653
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->j:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->k:Ljavax/inject/Provider;

    .line 9654
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->i:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->k:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/i/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/i/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->l:Ljavax/inject/Provider;

    .line 9655
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->f:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->f:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->l:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/customer/selection/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/customer/selection/h;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->m:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 9608
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->b()Lcom/swedbank/mobile/app/customer/selection/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/customer/selection/g;
    .locals 1

    .line 9660
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/customer/selection/g;

    return-object v0
.end method
