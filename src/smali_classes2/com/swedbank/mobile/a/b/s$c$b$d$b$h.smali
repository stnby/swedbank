.class final Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$d$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$d$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/loading/LoadingTransferInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/p;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/loading/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$d$b;)V
    .locals 0

    .line 4498
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4500
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$d$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 4479
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$d$b;)V

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 4505
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/ac/b/e;->b()Lcom/swedbank/mobile/a/ac/b/e;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->b:Ljavax/inject/Provider;

    .line 4506
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->b:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/ac/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/b/d;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->c:Ljavax/inject/Provider;

    .line 4507
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->c:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/business/transfer/loading/a;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/loading/a;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->d:Ljavax/inject/Provider;

    .line 4508
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/loading/d;->b()Lcom/swedbank/mobile/app/transfer/loading/d;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->e:Ljavax/inject/Provider;

    .line 4509
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/transfer/p;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->f:Ljavax/inject/Provider;

    .line 4510
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$d$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$d$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$d$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/ac/b/c;->b()Lcom/swedbank/mobile/a/ac/b/c;

    move-result-object v3

    invoke-static {}, Lcom/swedbank/mobile/a/ac/b/f;->b()Lcom/swedbank/mobile/a/ac/b/f;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/swedbank/mobile/app/transfer/r;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/r;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->g:Ljavax/inject/Provider;

    .line 4511
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->g:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->h:Ljavax/inject/Provider;

    .line 4512
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->f:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->h:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/ac/b/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->i:Ljavax/inject/Provider;

    .line 4513
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->i:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/transfer/loading/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/loading/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 4479
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->b()Lcom/swedbank/mobile/app/transfer/loading/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/loading/e;
    .locals 1

    .line 4518
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$d$b$h;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/loading/e;

    return-object v0
.end method
