.class final Lcom/swedbank/mobile/a/b/s$c$v$b$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/o;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/u;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/p;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/text/InputFilter;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/a/b/c;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 3063
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3065
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a(Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/a/b/c;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/a/b/c;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3010
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$v$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/a/b/c;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/a/b/c;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 3072
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->b:Ljavax/inject/Provider;

    .line 3077
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/a/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->c:Ljavax/inject/Provider;

    .line 3078
    invoke-static {}, Lcom/swedbank/mobile/data/authentication/i;->b()Lcom/swedbank/mobile/data/authentication/i;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->d:Ljavax/inject/Provider;

    .line 3079
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->e:Ljavax/inject/Provider;

    .line 3080
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->f:Ljavax/inject/Provider;

    .line 3081
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/login/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/login/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->g:Ljavax/inject/Provider;

    .line 3082
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->A(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p4}, Lcom/swedbank/mobile/business/authentication/login/p;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/login/p;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->h:Ljavax/inject/Provider;

    .line 3083
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->f:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->d:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->C(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->g:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->h:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/business/authentication/login/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/login/j;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    .line 3084
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->A(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p4}, Lcom/swedbank/mobile/app/b/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->j:Ljavax/inject/Provider;

    .line 3085
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->j:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/c/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->k:Ljavax/inject/Provider;

    .line 3086
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/b/l;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/l;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->l:Ljavax/inject/Provider;

    .line 3087
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/c/a/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->m:Ljavax/inject/Provider;

    .line 3088
    invoke-static {}, Lcom/swedbank/mobile/app/b/b/v;->b()Lcom/swedbank/mobile/app/b/b/v;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->n:Ljavax/inject/Provider;

    .line 3089
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->n:Ljavax/inject/Provider;

    invoke-static {p1, p4}, Lcom/swedbank/mobile/app/b/b/d/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/d/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->o:Ljavax/inject/Provider;

    .line 3090
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->n:Ljavax/inject/Provider;

    invoke-static {p1, p4}, Lcom/swedbank/mobile/app/b/b/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/b/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->p:Ljavax/inject/Provider;

    .line 3091
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->n:Ljavax/inject/Provider;

    invoke-static {p1, p4}, Lcom/swedbank/mobile/app/b/b/c/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/c/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->q:Ljavax/inject/Provider;

    .line 3092
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->l:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->m:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->o:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->p:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->q:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/a/c/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->r:Ljavax/inject/Provider;

    .line 3093
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->s:Ljavax/inject/Provider;

    .line 3094
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->E(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->s:Ljavax/inject/Provider;

    invoke-static {p1, p3, p4, v0, v1}, Lcom/swedbank/mobile/app/b/b/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/s;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->t:Ljavax/inject/Provider;

    .line 3095
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->F(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/a/c/b/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/h;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->u:Ljavax/inject/Provider;

    .line 3096
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->u:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/b/d/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/d/k;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->v:Ljavax/inject/Provider;

    .line 3097
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->t:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/app/c/a/b/g;->b()Lcom/swedbank/mobile/app/c/a/b/g;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->v:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/app/b/b/b/i;->b()Lcom/swedbank/mobile/app/b/b/b/i;

    move-result-object v4

    invoke-static {}, Lcom/swedbank/mobile/app/b/b/c/h;->b()Lcom/swedbank/mobile/app/b/b/c/h;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/a/c/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->w:Ljavax/inject/Provider;

    .line 3098
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->x:Ljavax/inject/Provider;

    .line 3099
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->r:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->w:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->x:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/a/c/b/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/b/i;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->y:Ljavax/inject/Provider;

    .line 3100
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v$b;->a(Lcom/swedbank/mobile/a/b/s$c$v$b;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v$b;->b(Lcom/swedbank/mobile/a/b/s$c$v$b;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->d:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->i:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->y:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/b/b/n;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/n;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->z:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3010
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->b()Lcom/swedbank/mobile/app/b/b/m;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/b/m;
    .locals 1

    .line 3105
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->z:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/m;

    return-object v0
.end method
