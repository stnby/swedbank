.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/a/f/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$e;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/b/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 12378
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12380
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a(Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12351
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)Ljavax/inject/Provider;
    .locals 0

    .line 12351
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->j:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 12388
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->b:Ljavax/inject/Provider;

    .line 12393
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/d/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->c:Ljavax/inject/Provider;

    .line 12394
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$2;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->d:Ljavax/inject/Provider;

    .line 12399
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->d:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->e:Ljavax/inject/Provider;

    .line 12400
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$3;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->f:Ljavax/inject/Provider;

    .line 12405
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->f:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/e/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/e/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->g:Ljavax/inject/Provider;

    .line 12406
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->e:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->g:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/cards/f/a/f/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/f/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->h:Ljavax/inject/Provider;

    .line 12407
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->i:Ljavax/inject/Provider;

    .line 12408
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->j:Ljavax/inject/Provider;

    .line 12409
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->i:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->j:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->b(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->k:Ljavax/inject/Provider;

    .line 12410
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->k:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->l:Ljavax/inject/Provider;

    .line 12411
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->l:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->l:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p2, p3, p4}, Lcom/swedbank/mobile/app/cards/f/a/f/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/f/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->m:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)Ljavax/inject/Provider;
    .locals 0

    .line 12351
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->l:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12351
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->b()Lcom/swedbank/mobile/app/cards/f/a/f/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/f/e;
    .locals 1

    .line 12416
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/f/e;

    return-object v0
.end method
