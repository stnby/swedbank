.class final Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/aa/c/a/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/a/a/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/a/a/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/a/a/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;)V
    .locals 0

    .line 9336
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9338
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->a(Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 9317
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;)V
    .locals 3

    .line 9343
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->b:Ljavax/inject/Provider;

    .line 9344
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->c:Ljavax/inject/Provider;

    .line 9345
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->c:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/services/c/a/a/a/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/a/a/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->d:Ljavax/inject/Provider;

    .line 9346
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->d:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->e:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 9347
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/services/c/a/a/a/h;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->e:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->f:Ljavax/inject/Provider;

    .line 9348
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/services/c/a/a/a/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/a/a/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->g:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 9349
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->g:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->h:Ljavax/inject/Provider;

    .line 9350
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->h:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/aa/c/a/a/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/a/a/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->i:Ljavax/inject/Provider;

    .line 9351
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->i:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/services/c/a/a/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/a/a/a/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 9317
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->b()Lcom/swedbank/mobile/app/services/c/a/a/a/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/services/c/a/a/a/e;
    .locals 1

    .line 9356
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/services/c/a/a/a/e;

    return-object v0
.end method
