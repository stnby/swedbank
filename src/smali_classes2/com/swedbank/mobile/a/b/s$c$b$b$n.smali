.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "n"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$l;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$k;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$j;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$i;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$r;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$q;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$p;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$o;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$m;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/c;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/h;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/p;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private N:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/m;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/suggested/g;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/suggested/d;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/predefined/e;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/predefined/b;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/d/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/g/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/e/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/plugins/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/e/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/plugins/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/e/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/plugins/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/e/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/plugins/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V
    .locals 0

    .line 6179
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6181
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6090
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)Ljavax/inject/Provider;
    .locals 0

    .line 6090
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->A:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)Ljavax/inject/Provider;
    .locals 0

    .line 6090
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->Q:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)Ljavax/inject/Provider;
    .locals 0

    .line 6090
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->S:Ljavax/inject/Provider;

    return-object p0
.end method

.method private c()V
    .locals 15

    .line 6186
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->b:Ljavax/inject/Provider;

    .line 6191
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/form/k;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->c:Ljavax/inject/Provider;

    .line 6192
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->d:Ljavax/inject/Provider;

    .line 6197
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/search/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->e:Ljavax/inject/Provider;

    .line 6198
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->f:Ljavax/inject/Provider;

    .line 6203
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->g:Ljavax/inject/Provider;

    .line 6204
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$4;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->h:Ljavax/inject/Provider;

    .line 6209
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->h:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->i:Ljavax/inject/Provider;

    .line 6210
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$5;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->j:Ljavax/inject/Provider;

    .line 6215
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->j:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/c/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->k:Ljavax/inject/Provider;

    .line 6216
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$6;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->l:Ljavax/inject/Provider;

    .line 6221
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->l:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/plugins/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/plugins/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->m:Ljavax/inject/Provider;

    .line 6222
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->m:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/e/c/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->n:Ljavax/inject/Provider;

    .line 6223
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$7;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$7;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->o:Ljavax/inject/Provider;

    .line 6228
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/plugins/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/plugins/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->p:Ljavax/inject/Provider;

    .line 6229
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->p:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/e/d/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/d/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->q:Ljavax/inject/Provider;

    .line 6230
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$8;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$8;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->r:Ljavax/inject/Provider;

    .line 6235
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->r:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/plugins/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/plugins/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->s:Ljavax/inject/Provider;

    .line 6236
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->s:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/e/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->t:Ljavax/inject/Provider;

    .line 6237
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$9;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$9;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->u:Ljavax/inject/Provider;

    .line 6242
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->u:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/plugins/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/plugins/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->v:Ljavax/inject/Provider;

    .line 6243
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->v:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/e/b/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->w:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x4

    .line 6244
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->n:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->q:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->t:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->w:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->x:Ljavax/inject/Provider;

    .line 6245
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->x:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/a/ac/e/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/c;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->y:Ljavax/inject/Provider;

    .line 6246
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/a/b;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->z:Ljavax/inject/Provider;

    .line 6247
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->Q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->z:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/data/account/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/account/d;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->A:Ljavax/inject/Provider;

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 6248
    invoke-static {v1, v2}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->c(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->b(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/ac/g;->b()Lcom/swedbank/mobile/a/ac/g;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->B:Ljavax/inject/Provider;

    .line 6249
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->B:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/ac/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->C:Ljavax/inject/Provider;

    .line 6250
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->y:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/ac/e/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/e;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->D:Ljavax/inject/Provider;

    .line 6251
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->A:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->C:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->D:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/swedbank/mobile/business/transfer/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/e;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    .line 6252
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->y:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/ac/e/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/b;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->F:Ljavax/inject/Provider;

    .line 6253
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->F:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4}, Lcom/swedbank/mobile/app/transfer/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/l;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->G:Ljavax/inject/Provider;

    .line 6254
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->G:Ljavax/inject/Provider;

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->H:Ljavax/inject/Provider;

    .line 6255
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/transfer/p;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->H:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->I:Ljavax/inject/Provider;

    .line 6256
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/swedbank/mobile/a/ac/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->J:Ljavax/inject/Provider;

    .line 6257
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->y:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/ac/e/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/d;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->K:Ljavax/inject/Provider;

    .line 6258
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->d(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->J:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->K:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4}, Lcom/swedbank/mobile/app/transfer/r;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/r;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->L:Ljavax/inject/Provider;

    .line 6259
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->L:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->M:Ljavax/inject/Provider;

    .line 6260
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->I:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->M:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/ac/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->N:Ljavax/inject/Provider;

    .line 6261
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->e:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->g:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->i:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->k:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->y:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    iget-object v11, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    iget-object v12, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->E:Ljavax/inject/Provider;

    iget-object v13, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->N:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v14

    invoke-static/range {v1 .. v14}, Lcom/swedbank/mobile/app/transfer/n;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/n;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->O:Ljavax/inject/Provider;

    .line 6262
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/e/d/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/d/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->P:Ljavax/inject/Provider;

    .line 6263
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->P:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/data/transfer/suggested/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/transfer/suggested/f;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->Q:Ljavax/inject/Provider;

    .line 6264
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/e/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/e/b/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->R:Ljavax/inject/Provider;

    .line 6265
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->R:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/data/transfer/predefined/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/transfer/predefined/d;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->S:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6090
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->b()Lcom/swedbank/mobile/app/transfer/m;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/m;
    .locals 1

    .line 6270
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->O:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/m;

    return-object v0
.end method
