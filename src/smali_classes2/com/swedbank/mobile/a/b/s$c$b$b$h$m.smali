.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/l/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "m"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

.field private b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/business/overview/statement/details/f;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V
    .locals 0

    .line 5843
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5843
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;
    .locals 0

    .line 5856
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/overview/statement/details/f;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->c:Lcom/swedbank/mobile/business/overview/statement/details/f;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;"
        }
    .end annotation

    .line 5850
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->b:Lcom/swedbank/mobile/business/util/e;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/u/l/a/a;
    .locals 5

    .line 5862
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->b:Lcom/swedbank/mobile/business/util/e;

    const-class v1, Lcom/swedbank/mobile/business/util/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5863
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->c:Lcom/swedbank/mobile/business/overview/statement/details/f;

    const-class v1, Lcom/swedbank/mobile/business/overview/statement/details/f;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5864
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->b:Lcom/swedbank/mobile/business/util/e;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->c:Lcom/swedbank/mobile/business/overview/statement/details/f;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/a/u/l/a/a$a;
    .locals 0

    .line 5843
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->a(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/u/l/a/a$a;
    .locals 0

    .line 5843
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;->a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$m;

    move-result-object p1

    return-object p1
.end method
