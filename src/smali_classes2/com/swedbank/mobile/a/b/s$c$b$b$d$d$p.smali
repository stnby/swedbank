.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;
.super Lcom/swedbank/mobile/a/e/g/a/h/a;
.source "DaggerApplicationComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "p"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/h/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V
    .locals 0

    .line 8344
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-direct {p0}, Lcom/swedbank/mobile/a/e/g/a/h/a;-><init>()V

    .line 8346
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->f()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 8339
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    return-void
.end method

.method private f()V
    .locals 3

    .line 8351
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->b:Ljavax/inject/Provider;

    .line 8352
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->U(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/a/h/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/h/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->c:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 8339
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->b()Lcom/swedbank/mobile/app/cards/f/a/h/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/h/c;
    .locals 1

    .line 8357
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/h/c;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/a;
    .locals 1

    .line 8361
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/a;

    return-object v0
.end method

.method public synthetic d()Lcom/swedbank/mobile/business/cards/wallet/onboarding/l;
    .locals 1

    .line 8339
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;->c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/a;

    move-result-object v0

    return-object v0
.end method
