.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/d/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/e;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/n;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/v;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 6348
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6350
    invoke-direct {p0, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a(Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6306
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 6356
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->b:Ljavax/inject/Provider;

    .line 6361
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->c:Ljavax/inject/Provider;

    .line 6362
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->d:Ljavax/inject/Provider;

    .line 6367
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->e:Ljavax/inject/Provider;

    .line 6368
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->f:Ljavax/inject/Provider;

    .line 6373
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->g:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 6374
    invoke-static {v0, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->b(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/a/ac/d/b/d;->b()Lcom/swedbank/mobile/a/ac/d/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->h:Ljavax/inject/Provider;

    .line 6375
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->h:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/ac/d/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/d/b/c;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->i:Ljavax/inject/Provider;

    .line 6376
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->j:Ljavax/inject/Provider;

    .line 6377
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->k:Ljavax/inject/Provider;

    .line 6378
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->c(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->i:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->j:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->k:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/transfer/payment/form/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/form/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->l:Ljavax/inject/Provider;

    .line 6379
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/form/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->m:Ljavax/inject/Provider;

    .line 6380
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->m:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->l:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/r;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/form/r;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->n:Ljavax/inject/Provider;

    .line 6381
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->n:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->o:Ljavax/inject/Provider;

    .line 6382
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object p1

    const-class p2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->o:Ljavax/inject/Provider;

    invoke-virtual {p1, p2, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/f$a;->a()La/a/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->p:Ljavax/inject/Provider;

    .line 6383
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ab;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/form/ab;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->q:Ljavax/inject/Provider;

    const/4 p1, 0x0

    .line 6384
    invoke-static {v0, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->q:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->r:Ljavax/inject/Provider;

    .line 6385
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->s:Ljavax/inject/Provider;

    .line 6386
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->p:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->r:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->s:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/a/ac/d/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/d/b/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->t:Ljavax/inject/Provider;

    .line 6387
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->l:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->l:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->l:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->l:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->t:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/transfer/payment/form/t;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/form/t;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->u:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6306
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->b()Lcom/swedbank/mobile/app/transfer/payment/form/s;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/payment/form/s;
    .locals 1

    .line 6392
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->u:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/s;

    return-object v0
.end method
