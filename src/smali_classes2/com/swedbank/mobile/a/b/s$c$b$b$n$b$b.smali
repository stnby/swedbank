.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/g/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/BottomSheetDialogInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V
    .locals 0

    .line 6812
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6814
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->a(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6792
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V
    .locals 2

    .line 6820
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->b:Ljavax/inject/Provider;

    .line 6821
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/general/confirmation/bottom/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/general/confirmation/bottom/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->c:Ljavax/inject/Provider;

    .line 6822
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->d:Ljavax/inject/Provider;

    .line 6823
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->d:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/f/a/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->e:Ljavax/inject/Provider;

    .line 6824
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->e:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->f:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 6825
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/f/a/k;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->f:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->g:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 6826
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/app/f/a/l;->b()Lcom/swedbank/mobile/app/f/a/l;

    move-result-object p2

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->h:Ljavax/inject/Provider;

    .line 6827
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->g:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->h:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/g/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/g/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->i:Ljavax/inject/Provider;

    .line 6828
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->i:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/f/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6792
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->b()Lcom/swedbank/mobile/app/f/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/f/a/h;
    .locals 1

    .line 6833
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$b;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/f/a/h;

    return-object v0
.end method
