.class final Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/i;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/r;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/app/c/a/d;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 3187
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3189
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3152
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/app/c/a/d;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 3197
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->b:Ljavax/inject/Provider;

    .line 3198
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->c:Ljavax/inject/Provider;

    .line 3199
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c;->c(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->b:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->c:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/business/biometric/authentication/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/authentication/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->d:Ljavax/inject/Provider;

    .line 3200
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->e:Ljavax/inject/Provider;

    .line 3201
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->f:Ljavax/inject/Provider;

    .line 3202
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->d:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->f:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/c/a/m;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/m;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->g:Ljavax/inject/Provider;

    .line 3203
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->g:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->h:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 3204
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class p3, Lcom/swedbank/mobile/app/c/a/p;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->h:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->i:Ljavax/inject/Provider;

    .line 3205
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->e:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/app/c/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->j:Ljavax/inject/Provider;

    .line 3206
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/swedbank/mobile/app/c/a/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/s;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->k:Ljavax/inject/Provider;

    .line 3207
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p2}, Lcom/swedbank/mobile/app/c/a/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->l:Ljavax/inject/Provider;

    .line 3208
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->j:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->k:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->l:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->e:Ljavax/inject/Provider;

    invoke-static {p2, p3, p4, v0, v1}, Lcom/swedbank/mobile/a/d/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/a/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->m:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 3209
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->m:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->n:Ljavax/inject/Provider;

    .line 3210
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->e:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->i:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->n:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, p4}, Lcom/swedbank/mobile/a/d/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->o:Ljavax/inject/Provider;

    .line 3211
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->d:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->o:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p2, p3, p4}, Lcom/swedbank/mobile/app/c/a/o;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/o;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->p:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3152
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->b()Lcom/swedbank/mobile/app/c/a/n;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/c/a/n;
    .locals 1

    .line 3216
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;->p:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/c/a/n;

    return-object v0
.end method
