.class final Lcom/swedbank/mobile/a/b/s$c$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/p/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$j;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$i;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$k;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$m;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$g;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/c;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/k;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/h;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/o/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/m/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/o/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private O:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/i/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/customer/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/i/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/h/a;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/services/a;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/a;",
            ">;"
        }
    .end annotation
.end field

.field private U:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private V:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/b;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/d;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/i;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/b;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/h/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/contact/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/a/p/a/b;Lcom/swedbank/mobile/a/i/d;Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/a/ac/i;Lcom/swedbank/mobile/a/e/d;Lcom/swedbank/mobile/a/aa/f;Lcom/swedbank/mobile/a/h/a/g;)V
    .locals 9

    move-object v8, p0

    move-object v0, p1

    .line 4874
    iput-object v0, v8, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    .line 4876
    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/p/a/b;Lcom/swedbank/mobile/a/i/d;Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/a/ac/i;Lcom/swedbank/mobile/a/e/d;Lcom/swedbank/mobile/a/aa/f;Lcom/swedbank/mobile/a/h/a/g;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/a/p/a/b;Lcom/swedbank/mobile/a/i/d;Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/a/ac/i;Lcom/swedbank/mobile/a/e/d;Lcom/swedbank/mobile/a/aa/f;Lcom/swedbank/mobile/a/h/a/g;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 4773
    invoke-direct/range {p0 .. p8}, Lcom/swedbank/mobile/a/b/s$c$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/a/p/a/b;Lcom/swedbank/mobile/a/i/d;Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/a/ac/i;Lcom/swedbank/mobile/a/e/d;Lcom/swedbank/mobile/a/aa/f;Lcom/swedbank/mobile/a/h/a/g;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->J:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/a/p/a/b;Lcom/swedbank/mobile/a/i/d;Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/a/ac/i;Lcom/swedbank/mobile/a/e/d;Lcom/swedbank/mobile/a/aa/f;Lcom/swedbank/mobile/a/h/a/g;)V
    .locals 4

    .line 4887
    invoke-static {p1}, Lcom/swedbank/mobile/a/p/a/c;->a(Lcom/swedbank/mobile/a/p/a/b;)Lcom/swedbank/mobile/a/p/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->b:Ljavax/inject/Provider;

    .line 4888
    invoke-static {p3}, Lcom/swedbank/mobile/a/u/g;->a(Lcom/swedbank/mobile/a/u/f;)Lcom/swedbank/mobile/a/u/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->c:Ljavax/inject/Provider;

    .line 4889
    invoke-static {p4}, Lcom/swedbank/mobile/a/ac/j;->a(Lcom/swedbank/mobile/a/ac/i;)Lcom/swedbank/mobile/a/ac/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->d:Ljavax/inject/Provider;

    .line 4890
    invoke-static {p5}, Lcom/swedbank/mobile/a/e/e;->a(Lcom/swedbank/mobile/a/e/d;)Lcom/swedbank/mobile/a/e/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->e:Ljavax/inject/Provider;

    .line 4891
    invoke-static {p6}, Lcom/swedbank/mobile/a/aa/g;->a(Lcom/swedbank/mobile/a/aa/f;)Lcom/swedbank/mobile/a/aa/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->f:Ljavax/inject/Provider;

    .line 4892
    invoke-static {p7}, Lcom/swedbank/mobile/a/h/a/h;->a(Lcom/swedbank/mobile/a/h/a/g;)Lcom/swedbank/mobile/a/h/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->g:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x5

    .line 4893
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->c:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->d:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->f:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->g:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->h:Ljavax/inject/Provider;

    .line 4894
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->h:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/swedbank/mobile/a/p/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/e;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->i:Ljavax/inject/Provider;

    .line 4895
    new-instance v2, Lcom/swedbank/mobile/a/b/s$c$b$b$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->j:Ljavax/inject/Provider;

    .line 4900
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->j:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/overview/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->k:Ljavax/inject/Provider;

    .line 4901
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->k:Ljavax/inject/Provider;

    invoke-static {p3, v2}, Lcom/swedbank/mobile/a/u/h;->a(Lcom/swedbank/mobile/a/u/f;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/h;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->l:Ljavax/inject/Provider;

    .line 4902
    new-instance p3, Lcom/swedbank/mobile/a/b/s$c$b$b$2;

    invoke-direct {p3, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->m:Ljavax/inject/Provider;

    .line 4907
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->m:Ljavax/inject/Provider;

    invoke-static {p3}, Lcom/swedbank/mobile/app/transfer/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/e;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->n:Ljavax/inject/Provider;

    .line 4908
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->n:Ljavax/inject/Provider;

    invoke-static {p4, p3}, Lcom/swedbank/mobile/a/ac/k;->a(Lcom/swedbank/mobile/a/ac/i;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/k;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->o:Ljavax/inject/Provider;

    .line 4909
    new-instance p3, Lcom/swedbank/mobile/a/b/s$c$b$b$3;

    invoke-direct {p3, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->p:Ljavax/inject/Provider;

    .line 4914
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->p:Ljavax/inject/Provider;

    invoke-static {p3}, Lcom/swedbank/mobile/app/cards/j;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/j;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->q:Ljavax/inject/Provider;

    .line 4915
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->q:Ljavax/inject/Provider;

    invoke-static {p5, p3}, Lcom/swedbank/mobile/a/e/f;->a(Lcom/swedbank/mobile/a/e/d;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/f;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->r:Ljavax/inject/Provider;

    .line 4916
    new-instance p3, Lcom/swedbank/mobile/a/b/s$c$b$b$4;

    invoke-direct {p3, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->s:Ljavax/inject/Provider;

    .line 4921
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->s:Ljavax/inject/Provider;

    invoke-static {p3}, Lcom/swedbank/mobile/app/services/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->t:Ljavax/inject/Provider;

    .line 4922
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->t:Ljavax/inject/Provider;

    invoke-static {p6, p3}, Lcom/swedbank/mobile/a/aa/h;->a(Lcom/swedbank/mobile/a/aa/f;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/h;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->u:Ljavax/inject/Provider;

    .line 4923
    new-instance p3, Lcom/swedbank/mobile/a/b/s$c$b$b$5;

    invoke-direct {p3, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->v:Ljavax/inject/Provider;

    .line 4928
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->v:Ljavax/inject/Provider;

    invoke-static {p3}, Lcom/swedbank/mobile/app/contact/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/contact/a/b;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->w:Ljavax/inject/Provider;

    .line 4929
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->w:Ljavax/inject/Provider;

    invoke-static {p7, p3}, Lcom/swedbank/mobile/a/h/a/i;->a(Lcom/swedbank/mobile/a/h/a/g;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/h/a/i;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->x:Ljavax/inject/Provider;

    .line 4930
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p3

    const-string p4, "feature_overview"

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->l:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    const-string p4, "feature_transfer"

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->o:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    const-string p4, "feature_cards"

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->r:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    const-string p4, "feature_services"

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->u:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    const-string p4, "feature_contact"

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->x:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/f$a;->a()La/a/f;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->y:Ljavax/inject/Provider;

    .line 4931
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->i:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->y:Ljavax/inject/Provider;

    invoke-static {p3, p4}, Lcom/swedbank/mobile/a/p/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/f;

    move-result-object p3

    invoke-static {p3}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->z:Ljavax/inject/Provider;

    .line 4932
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s$c;->e(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->z:Ljavax/inject/Provider;

    invoke-static {p3, p4}, Lcom/swedbank/mobile/business/navigation/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/navigation/h;

    move-result-object p3

    invoke-static {p3}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->A:Ljavax/inject/Provider;

    .line 4933
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->i:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->A:Ljavax/inject/Provider;

    invoke-static {p3, p4}, Lcom/swedbank/mobile/app/navigation/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/g;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->B:Ljavax/inject/Provider;

    .line 4934
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->B:Ljavax/inject/Provider;

    invoke-static {p3}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->C:Ljavax/inject/Provider;

    const/4 p3, 0x1

    .line 4935
    invoke-static {p3}, La/a/f;->a(I)La/a/f$a;

    move-result-object p4

    const-class p5, Lcom/swedbank/mobile/app/navigation/k;

    iget-object p6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->C:Ljavax/inject/Provider;

    invoke-virtual {p4, p5, p6}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p4

    invoke-virtual {p4}, La/a/f$a;->a()La/a/f;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->D:Ljavax/inject/Provider;

    .line 4936
    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p5, p5, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p5, p5, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p5}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p5

    invoke-static {p4, p5}, Lcom/swedbank/mobile/app/navigation/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/l;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->E:Ljavax/inject/Provider;

    .line 4937
    invoke-static {p3, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p3

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->E:Ljavax/inject/Provider;

    invoke-virtual {p3, p4}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/j$a;->a()La/a/j;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->F:Ljavax/inject/Provider;

    .line 4938
    invoke-static {p1}, Lcom/swedbank/mobile/a/p/a/d;->a(Lcom/swedbank/mobile/a/p/a/b;)Lcom/swedbank/mobile/a/p/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->G:Ljavax/inject/Provider;

    .line 4939
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->D:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->F:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->G:Ljavax/inject/Provider;

    invoke-static {p1, p3, p4}, Lcom/swedbank/mobile/a/p/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->H:Ljavax/inject/Provider;

    .line 4940
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->j(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->b:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->A:Ljavax/inject/Provider;

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->H:Ljavax/inject/Provider;

    iget-object p6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p6, p6, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p6, p6, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p6}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p6

    invoke-static {p1, p3, p4, p5, p6}, Lcom/swedbank/mobile/app/navigation/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->I:Ljavax/inject/Provider;

    .line 4941
    invoke-static {p2}, Lcom/swedbank/mobile/a/i/g;->a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/i/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->J:Ljavax/inject/Provider;

    .line 4942
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$6;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->K:Ljavax/inject/Provider;

    .line 4947
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->K:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/m/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/m/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->L:Ljavax/inject/Provider;

    .line 4948
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->L:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/o/a/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/o/a/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->M:Ljavax/inject/Provider;

    .line 4949
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->M:Ljavax/inject/Provider;

    invoke-static {p1, p3}, Lcom/swedbank/mobile/a/o/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/o/a/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->N:Ljavax/inject/Provider;

    .line 4950
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$7;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$7;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->O:Ljavax/inject/Provider;

    .line 4955
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->O:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/customer/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/customer/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->P:Ljavax/inject/Provider;

    .line 4956
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->P:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/i/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/i/a/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->Q:Ljavax/inject/Provider;

    .line 4957
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c;->e(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/h/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/h/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->R:Ljavax/inject/Provider;

    .line 4958
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/services/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/services/c;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->S:Ljavax/inject/Provider;

    .line 4959
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->S:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->J:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s$c$b;->b(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p3, p4}, Lcom/swedbank/mobile/business/services/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/services/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->T:Ljavax/inject/Provider;

    .line 4960
    invoke-static {p2}, Lcom/swedbank/mobile/a/i/f;->a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/i/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->U:Ljavax/inject/Provider;

    .line 4961
    invoke-static {p2}, Lcom/swedbank/mobile/a/i/e;->a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/i/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->V:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->Q:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->N:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->R:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->T:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic f(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->U:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic g(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->S:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic h(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 4773
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->V:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 4773
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b;->b()Lcom/swedbank/mobile/app/navigation/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/navigation/h;
    .locals 1

    .line 4966
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b;->I:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/navigation/h;

    return-object v0
.end method
