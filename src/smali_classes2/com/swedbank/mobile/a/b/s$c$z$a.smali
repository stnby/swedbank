.class final Lcom/swedbank/mobile/a/b/s$c$z$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/b/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$z;

.field private b:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

.field private c:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z;)V
    .locals 0

    .line 14252
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14252
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$z;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/a/b/s$c$z$a;
    .locals 0

    .line 14261
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/b/s$c$z$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$z$a;"
        }
    .end annotation

    .line 14267
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->c:Lcom/swedbank/mobile/business/util/e;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$z$a;
    .locals 0

    .line 14273
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->d:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/e/g/b/a;
    .locals 8

    .line 14279
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14280
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->c:Lcom/swedbank/mobile/business/util/e;

    const-class v1, Lcom/swedbank/mobile/business/util/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14281
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->d:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14282
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->c:Lcom/swedbank/mobile/business/util/e;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$z$a;->d:Ljava/lang/Boolean;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$z$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$z;Lcom/swedbank/mobile/business/cards/wallet/payment/k;Lcom/swedbank/mobile/business/util/e;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/a/e/g/b/a$a;
    .locals 0

    .line 14252
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$a;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/a/b/s$c$z$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/e/g/b/a$a;
    .locals 0

    .line 14252
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$a;->a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/b/s$c$z$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Z)Lcom/swedbank/mobile/a/e/g/b/a$a;
    .locals 0

    .line 14252
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$a;->a(Z)Lcom/swedbank/mobile/a/b/s$c$z$a;

    move-result-object p1

    return-object p1
.end method
