.class final Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/v/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 0

    .line 10577
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10579
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a(Lcom/swedbank/mobile/business/preferences/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 10562
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 3

    .line 10584
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->b:Ljavax/inject/Provider;

    .line 10589
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->c:Ljavax/inject/Provider;

    .line 10590
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->d:Ljavax/inject/Provider;

    const/4 p1, 0x0

    const/4 v0, 0x2

    .line 10591
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->t(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->s(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->e:Ljavax/inject/Provider;

    .line 10592
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->e:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->u(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/e/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/e/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->f:Ljavax/inject/Provider;

    .line 10593
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->d:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->f:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/preferences/cleardata/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/preferences/cleardata/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->g:Ljavax/inject/Provider;

    .line 10594
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->g:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/p/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/a/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->h:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 10562
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->b()Lcom/swedbank/mobile/app/p/a/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/p/a/e;
    .locals 1

    .line 10599
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/p/a/e;

    return-object v0
.end method
