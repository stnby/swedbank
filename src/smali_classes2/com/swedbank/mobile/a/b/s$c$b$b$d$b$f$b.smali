.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/g/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/BottomSheetDialogInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V
    .locals 0

    .line 7965
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7967
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->a(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7945
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/app/f/a/d;)V
    .locals 2

    .line 7973
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->b:Ljavax/inject/Provider;

    .line 7974
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/general/confirmation/bottom/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/general/confirmation/bottom/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->c:Ljavax/inject/Provider;

    .line 7975
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->d:Ljavax/inject/Provider;

    .line 7976
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->d:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/f/a/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->e:Ljavax/inject/Provider;

    .line 7977
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->e:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->f:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 7978
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/f/a/k;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->f:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->g:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 7979
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/app/f/a/l;->b()Lcom/swedbank/mobile/app/f/a/l;

    move-result-object p2

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->h:Ljavax/inject/Provider;

    .line 7980
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->g:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->h:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/g/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/g/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->i:Ljavax/inject/Provider;

    .line 7981
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->i:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/f/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7945
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->b()Lcom/swedbank/mobile/app/f/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/f/a/h;
    .locals 1

    .line 7986
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f$b;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/f/a/h;

    return-object v0
.end method
