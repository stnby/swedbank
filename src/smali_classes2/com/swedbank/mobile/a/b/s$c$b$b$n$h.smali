.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/detailed/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/k;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Ljava/lang/Integer;Lcom/swedbank/mobile/app/plugins/list/c;Lcom/swedbank/mobile/business/transfer/detailed/c;)V
    .locals 0

    .line 7196
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7198
    invoke-direct {p0, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->a(Ljava/lang/Integer;Lcom/swedbank/mobile/app/plugins/list/c;Lcom/swedbank/mobile/business/transfer/detailed/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Ljava/lang/Integer;Lcom/swedbank/mobile/app/plugins/list/c;Lcom/swedbank/mobile/business/transfer/detailed/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7166
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Ljava/lang/Integer;Lcom/swedbank/mobile/app/plugins/list/c;Lcom/swedbank/mobile/business/transfer/detailed/c;)V

    return-void
.end method

.method private a(Ljava/lang/Integer;Lcom/swedbank/mobile/app/plugins/list/c;Lcom/swedbank/mobile/business/transfer/detailed/c;)V
    .locals 2

    .line 7205
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->b:Ljavax/inject/Provider;

    .line 7206
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->c:Ljavax/inject/Provider;

    .line 7207
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/ac/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/a/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->d:Ljavax/inject/Provider;

    .line 7208
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->b:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->d:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/business/transfer/detailed/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/detailed/b;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->e:Ljavax/inject/Provider;

    .line 7209
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/ac/a/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->f:Ljavax/inject/Provider;

    .line 7210
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->f:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->e:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/app/transfer/a/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/a/g;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->g:Ljavax/inject/Provider;

    .line 7211
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->g:Ljavax/inject/Provider;

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->h:Ljavax/inject/Provider;

    const/4 p2, 0x1

    .line 7212
    invoke-static {p2}, La/a/f;->a(I)La/a/f$a;

    move-result-object p3

    const-class v0, Lcom/swedbank/mobile/app/transfer/a/k;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->h:Ljavax/inject/Provider;

    invoke-virtual {p3, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/f$a;->a()La/a/f;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->i:Ljavax/inject/Provider;

    .line 7213
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->j:Ljavax/inject/Provider;

    .line 7214
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->c:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/ac/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/a/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->k:Ljavax/inject/Provider;

    .line 7215
    invoke-static {}, Lcom/swedbank/mobile/a/b/l;->b()Lcom/swedbank/mobile/a/b/l;

    move-result-object p1

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->j:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p3, v0, v1}, Lcom/swedbank/mobile/app/transfer/a/m;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/a/m;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->l:Ljavax/inject/Provider;

    const/4 p1, 0x0

    .line 7216
    invoke-static {p2, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->l:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->m:Ljavax/inject/Provider;

    .line 7217
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->i:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->m:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/ac/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/a/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->n:Ljavax/inject/Provider;

    .line 7218
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->e:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->n:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/app/transfer/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/a/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->o:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7166
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->b()Lcom/swedbank/mobile/app/transfer/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/a/h;
    .locals 1

    .line 7223
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;->o:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/a/h;

    return-object v0
.end method
