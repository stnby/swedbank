.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$g;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$c;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/detailed/p;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/g/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/g/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/d/e;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/detailed/j;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/detailed/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 5185
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5187
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5124
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)Ljavax/inject/Provider;
    .locals 0

    .line 5124
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->D:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 5195
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->b:Ljavax/inject/Provider;

    .line 5196
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->c:Ljavax/inject/Provider;

    .line 5201
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->c:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/currencies/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/currencies/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->d:Ljavax/inject/Provider;

    .line 5202
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->e:Ljavax/inject/Provider;

    .line 5207
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->e:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->f:Ljavax/inject/Provider;

    .line 5208
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->g:Ljavax/inject/Provider;

    .line 5209
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->h:Ljavax/inject/Provider;

    .line 5210
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$3;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->i:Ljavax/inject/Provider;

    .line 5215
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->i:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/d/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->j:Ljavax/inject/Provider;

    .line 5216
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->j:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/u/g/b/d;->b()Lcom/swedbank/mobile/a/u/g/b/d;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/swedbank/mobile/a/u/g/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->k:Ljavax/inject/Provider;

    .line 5217
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$4;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->l:Ljavax/inject/Provider;

    .line 5222
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->l:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/d/d/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/d/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->m:Ljavax/inject/Provider;

    .line 5223
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->m:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/u/g/d/d;->b()Lcom/swedbank/mobile/a/u/g/d/d;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/swedbank/mobile/a/u/g/d/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->n:Ljavax/inject/Provider;

    const/4 p1, 0x0

    const/4 p3, 0x2

    .line 5224
    invoke-static {p3, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object p3

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->k:Ljavax/inject/Provider;

    invoke-virtual {p3, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p3

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->n:Ljavax/inject/Provider;

    invoke-virtual {p3, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/j$a;->a()La/a/j;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->o:Ljavax/inject/Provider;

    .line 5225
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->o:Ljavax/inject/Provider;

    invoke-static {p3, v0}, Lcom/swedbank/mobile/a/u/g/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/c;

    move-result-object p3

    invoke-static {p3}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->p:Ljavax/inject/Provider;

    .line 5226
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->p:Ljavax/inject/Provider;

    invoke-static {p3}, Lcom/swedbank/mobile/a/u/g/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/e;

    move-result-object p3

    invoke-static {p3}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->q:Ljavax/inject/Provider;

    .line 5227
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->h:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->q:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/business/overview/detailed/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/detailed/d;

    move-result-object p3

    invoke-static {p3}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->r:Ljavax/inject/Provider;

    .line 5228
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->p:Ljavax/inject/Provider;

    invoke-static {p3}, Lcom/swedbank/mobile/a/u/g/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/b;

    move-result-object p3

    invoke-static {p3}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->s:Ljavax/inject/Provider;

    .line 5229
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->s:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->r:Ljavax/inject/Provider;

    invoke-static {p3, v0, v1}, Lcom/swedbank/mobile/app/overview/detailed/o;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/detailed/o;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->t:Ljavax/inject/Provider;

    .line 5230
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->t:Ljavax/inject/Provider;

    invoke-static {p3}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->u:Ljavax/inject/Provider;

    const/4 p3, 0x1

    .line 5231
    invoke-static {p3}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/overview/detailed/s;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->u:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->v:Ljavax/inject/Provider;

    .line 5232
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->w:Ljavax/inject/Provider;

    .line 5233
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->x:Ljavax/inject/Provider;

    .line 5234
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->p:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/u/g/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/d;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->y:Ljavax/inject/Provider;

    .line 5235
    invoke-static {}, Lcom/swedbank/mobile/a/b/l;->b()Lcom/swedbank/mobile/a/b/l;

    move-result-object v0

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->d(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->w:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->x:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->y:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/detailed/w;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/detailed/w;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->z:Ljavax/inject/Provider;

    .line 5236
    invoke-static {p3, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->z:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->A:Ljavax/inject/Provider;

    .line 5237
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->v:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->A:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->w:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/a/u/d/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/d/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->B:Ljavax/inject/Provider;

    .line 5238
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->S(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->b:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->d:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->f:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->r:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->r:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->r:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->r:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->B:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v10

    invoke-static/range {v0 .. v10}, Lcom/swedbank/mobile/app/overview/detailed/q;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/detailed/q;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->C:Ljavax/inject/Provider;

    .line 5239
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->h:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/u/d/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->D:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5124
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->b()Lcom/swedbank/mobile/app/overview/detailed/p;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/detailed/p;
    .locals 1

    .line 5244
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;->C:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/detailed/p;

    return-object v0
.end method
