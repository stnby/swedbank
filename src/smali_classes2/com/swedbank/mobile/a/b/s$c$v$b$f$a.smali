.class final Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

.field private b:Lcom/swedbank/mobile/business/biometric/authentication/h;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/app/c/a/d;

.field private e:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;)V
    .locals 0

    .line 3107
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3107
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;
    .locals 0

    .line 3132
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/c/a/d;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->d:Lcom/swedbank/mobile/app/c/a/d;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;
    .locals 0

    .line 3119
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/h;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->b:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;"
        }
    .end annotation

    .line 3125
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->c:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/d/a/a;
    .locals 9

    .line 3144
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->b:Lcom/swedbank/mobile/business/biometric/authentication/h;

    const-class v1, Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3145
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->c:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3146
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->d:Lcom/swedbank/mobile/app/c/a/d;

    const-class v1, Lcom/swedbank/mobile/app/c/a/d;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3147
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->e:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3148
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->a:Lcom/swedbank/mobile/a/b/s$c$v$b$f;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->b:Lcom/swedbank/mobile/business/biometric/authentication/h;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->c:Lcom/swedbank/mobile/business/util/l;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->d:Lcom/swedbank/mobile/app/c/a/d;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->e:Lcom/swedbank/mobile/business/util/l;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b$f;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;"
        }
    .end annotation

    .line 3138
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->e:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 3107
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 3107
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 3107
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 3107
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$v$b$f$a;

    move-result-object p1

    return-object p1
.end method
