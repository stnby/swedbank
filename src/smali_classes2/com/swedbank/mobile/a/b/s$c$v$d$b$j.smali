.class final Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/h/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$d$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "j"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/contact/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/contact/notauth/NotAuthenticatedContactInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/contact/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/contact/h;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/contact/b/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)V
    .locals 0

    .line 3985
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3987
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3958
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)V

    return-void
.end method

.method private c()V
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3992
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/h/b/d;->b()Lcom/swedbank/mobile/a/h/b/d;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->b:Ljavax/inject/Provider;

    .line 3993
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->b:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/h/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/h/b/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->c:Ljavax/inject/Provider;

    .line 3994
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/data/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/a/c;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->d:Ljavax/inject/Provider;

    .line 3995
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->d:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/business/b/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/b/e;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->e:Ljavax/inject/Provider;

    .line 3996
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$v$d;->a(Lcom/swedbank/mobile/a/b/s$c$v$d;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/business/contact/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/contact/d;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->f:Ljavax/inject/Provider;

    .line 3997
    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->c:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->e:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->f:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/contact/notauth/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/contact/notauth/b;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->g:Ljavax/inject/Provider;

    .line 3998
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->g:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/swedbank/mobile/app/contact/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/contact/b/e;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->h:Ljavax/inject/Provider;

    .line 3999
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->h:Ljavax/inject/Provider;

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->i:Ljavax/inject/Provider;

    .line 4000
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/contact/h;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->i:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->j:Ljavax/inject/Provider;

    .line 4001
    invoke-static {}, Lcom/swedbank/mobile/a/h/b/e;->b()Lcom/swedbank/mobile/a/h/b/e;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/app/contact/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/contact/i;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->k:Ljavax/inject/Provider;

    .line 4002
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->k:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->l:Ljavax/inject/Provider;

    .line 4003
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->j:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->l:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/h/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/h/b/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->m:Ljavax/inject/Provider;

    .line 4004
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->m:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v4}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/contact/b/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/contact/b/g;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->n:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3958
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->b()Lcom/swedbank/mobile/app/contact/b/f;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/contact/b/f;
    .locals 1

    .line 4009
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$j;->n:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/contact/b/f;

    return-object v0
.end method
