.class final Lcom/swedbank/mobile/a/b/s$c$p$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/g/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$p;

.field private b:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private c:Lcom/swedbank/mobile/app/f/c;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$p;)V
    .locals 0

    .line 14612
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->a:Lcom/swedbank/mobile/a/b/s$c$p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$p;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14612
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$p$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$p;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/a/b/s$c$p$a;
    .locals 0

    .line 14629
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/f/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->c:Lcom/swedbank/mobile/app/f/c;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/a/b/s$c$p$a;
    .locals 0

    .line 14623
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$p$a;
    .locals 0

    .line 14641
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$p$a;
    .locals 0

    .line 14635
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->d:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/g/a;
    .locals 9

    .line 14647
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    const-class v1, Lcom/swedbank/mobile/business/general/confirmation/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14648
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->c:Lcom/swedbank/mobile/app/f/c;

    const-class v1, Lcom/swedbank/mobile/app/f/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14649
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->d:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14650
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->e:Ljava/lang/String;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14651
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$p$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->a:Lcom/swedbank/mobile/a/b/s$c$p;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->c:Lcom/swedbank/mobile/app/f/c;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->d:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$p$a;->e:Ljava/lang/String;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$p$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$p;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/app/f/c;Ljava/lang/Boolean;Ljava/lang/String;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/a/g/a$a;
    .locals 0

    .line 14612
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$p$a;->a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/a/b/s$c$p$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/a/g/a$a;
    .locals 0

    .line 14612
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$p$a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/a/b/s$c$p$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/swedbank/mobile/a/g/a$a;
    .locals 0

    .line 14612
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$p$a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$p$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Z)Lcom/swedbank/mobile/a/g/a$a;
    .locals 0

    .line 14612
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$p$a;->a(Z)Lcom/swedbank/mobile/a/b/s$c$p$a;

    move-result-object p1

    return-object p1
.end method
