.class final Lcom/swedbank/mobile/a/b/s$c$n$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/p/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$n$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$l;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$k;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$h;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$g;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$j;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$i;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$e;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/c;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/k;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/h;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/n/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/l/a;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/n/d;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/g/d;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/f/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/h/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/contact/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n;Lcom/swedbank/mobile/a/p/c/b;Lcom/swedbank/mobile/a/u/f/f;Lcom/swedbank/mobile/a/ac/c/f;Lcom/swedbank/mobile/a/e/d/h;Lcom/swedbank/mobile/a/aa/b/f;Lcom/swedbank/mobile/a/h/b/g;)V
    .locals 7

    .line 11896
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 11898
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/a/b/s$c$n$b;->a(Lcom/swedbank/mobile/a/p/c/b;Lcom/swedbank/mobile/a/u/f/f;Lcom/swedbank/mobile/a/ac/c/f;Lcom/swedbank/mobile/a/e/d/h;Lcom/swedbank/mobile/a/aa/b/f;Lcom/swedbank/mobile/a/h/b/g;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n;Lcom/swedbank/mobile/a/p/c/b;Lcom/swedbank/mobile/a/u/f/f;Lcom/swedbank/mobile/a/ac/c/f;Lcom/swedbank/mobile/a/e/d/h;Lcom/swedbank/mobile/a/aa/b/f;Lcom/swedbank/mobile/a/h/b/g;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11814
    invoke-direct/range {p0 .. p7}, Lcom/swedbank/mobile/a/b/s$c$n$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$n;Lcom/swedbank/mobile/a/p/c/b;Lcom/swedbank/mobile/a/u/f/f;Lcom/swedbank/mobile/a/ac/c/f;Lcom/swedbank/mobile/a/e/d/h;Lcom/swedbank/mobile/a/aa/b/f;Lcom/swedbank/mobile/a/h/b/g;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$n$b;)Ljavax/inject/Provider;
    .locals 0

    .line 11814
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->L:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/a/p/c/b;Lcom/swedbank/mobile/a/u/f/f;Lcom/swedbank/mobile/a/ac/c/f;Lcom/swedbank/mobile/a/e/d/h;Lcom/swedbank/mobile/a/aa/b/f;Lcom/swedbank/mobile/a/h/b/g;)V
    .locals 4

    .line 11908
    invoke-static {p1}, Lcom/swedbank/mobile/a/p/c/c;->a(Lcom/swedbank/mobile/a/p/c/b;)Lcom/swedbank/mobile/a/p/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->b:Ljavax/inject/Provider;

    .line 11909
    invoke-static {p2}, Lcom/swedbank/mobile/a/u/f/g;->a(Lcom/swedbank/mobile/a/u/f/f;)Lcom/swedbank/mobile/a/u/f/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->c:Ljavax/inject/Provider;

    .line 11910
    invoke-static {p3}, Lcom/swedbank/mobile/a/ac/c/g;->a(Lcom/swedbank/mobile/a/ac/c/f;)Lcom/swedbank/mobile/a/ac/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->d:Ljavax/inject/Provider;

    .line 11911
    invoke-static {p4}, Lcom/swedbank/mobile/a/e/d/i;->a(Lcom/swedbank/mobile/a/e/d/h;)Lcom/swedbank/mobile/a/e/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->e:Ljavax/inject/Provider;

    .line 11912
    invoke-static {p5}, Lcom/swedbank/mobile/a/aa/b/g;->a(Lcom/swedbank/mobile/a/aa/b/f;)Lcom/swedbank/mobile/a/aa/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->f:Ljavax/inject/Provider;

    .line 11913
    invoke-static {p6}, Lcom/swedbank/mobile/a/h/b/h;->a(Lcom/swedbank/mobile/a/h/b/g;)Lcom/swedbank/mobile/a/h/b/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->g:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x5

    .line 11914
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->c:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->d:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->f:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->g:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->h:Ljavax/inject/Provider;

    .line 11915
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->h:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/swedbank/mobile/a/p/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/e;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->i:Ljavax/inject/Provider;

    .line 11916
    new-instance v2, Lcom/swedbank/mobile/a/b/s$c$n$b$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b;)V

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->j:Ljavax/inject/Provider;

    .line 11921
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->j:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/overview/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/c/b;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->k:Ljavax/inject/Provider;

    .line 11922
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->k:Ljavax/inject/Provider;

    invoke-static {p2, v2}, Lcom/swedbank/mobile/a/u/f/h;->a(Lcom/swedbank/mobile/a/u/f/f;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/f/h;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->l:Ljavax/inject/Provider;

    .line 11923
    new-instance p2, Lcom/swedbank/mobile/a/b/s$c$n$b$2;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b;)V

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->m:Ljavax/inject/Provider;

    .line 11928
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->m:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/transfer/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/b/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->n:Ljavax/inject/Provider;

    .line 11929
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->n:Ljavax/inject/Provider;

    invoke-static {p3, p2}, Lcom/swedbank/mobile/a/ac/c/h;->a(Lcom/swedbank/mobile/a/ac/c/f;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/c/h;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->o:Ljavax/inject/Provider;

    .line 11930
    new-instance p2, Lcom/swedbank/mobile/a/b/s$c$n$b$3;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b;)V

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->p:Ljavax/inject/Provider;

    .line 11935
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->p:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/cards/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->q:Ljavax/inject/Provider;

    .line 11936
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->q:Ljavax/inject/Provider;

    invoke-static {p4, p2}, Lcom/swedbank/mobile/a/e/d/j;->a(Lcom/swedbank/mobile/a/e/d/h;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/j;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->r:Ljavax/inject/Provider;

    .line 11937
    new-instance p2, Lcom/swedbank/mobile/a/b/s$c$n$b$4;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b;)V

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->s:Ljavax/inject/Provider;

    .line 11942
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->s:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/services/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/b/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->t:Ljavax/inject/Provider;

    .line 11943
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->t:Ljavax/inject/Provider;

    invoke-static {p5, p2}, Lcom/swedbank/mobile/a/aa/b/h;->a(Lcom/swedbank/mobile/a/aa/b/f;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/b/h;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->u:Ljavax/inject/Provider;

    .line 11944
    new-instance p2, Lcom/swedbank/mobile/a/b/s$c$n$b$5;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b;)V

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->v:Ljavax/inject/Provider;

    .line 11949
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->v:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/contact/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/contact/b/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->w:Ljavax/inject/Provider;

    .line 11950
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->w:Ljavax/inject/Provider;

    invoke-static {p6, p2}, Lcom/swedbank/mobile/a/h/b/i;->a(Lcom/swedbank/mobile/a/h/b/g;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/h/b/i;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->x:Ljavax/inject/Provider;

    .line 11951
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_overview"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->l:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_transfer"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->o:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_cards"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->r:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_services"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->u:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_contact"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->x:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->y:Ljavax/inject/Provider;

    .line 11952
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->i:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->y:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/a/p/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/f;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->z:Ljavax/inject/Provider;

    .line 11953
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s$c;->e(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->z:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/business/navigation/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/navigation/h;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->A:Ljavax/inject/Provider;

    .line 11954
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->i:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->A:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/app/navigation/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/g;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->B:Ljavax/inject/Provider;

    .line 11955
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->B:Ljavax/inject/Provider;

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->C:Ljavax/inject/Provider;

    const/4 p2, 0x1

    .line 11956
    invoke-static {p2}, La/a/f;->a(I)La/a/f$a;

    move-result-object p3

    const-class p4, Lcom/swedbank/mobile/app/navigation/k;

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->C:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/f$a;->a()La/a/f;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->D:Ljavax/inject/Provider;

    .line 11957
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/swedbank/mobile/app/navigation/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/l;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->E:Ljavax/inject/Provider;

    .line 11958
    invoke-static {p2, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->E:Ljavax/inject/Provider;

    invoke-virtual {p2, p3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/j$a;->a()La/a/j;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->F:Ljavax/inject/Provider;

    .line 11959
    invoke-static {p1}, Lcom/swedbank/mobile/a/p/c/d;->a(Lcom/swedbank/mobile/a/p/c/b;)Lcom/swedbank/mobile/a/p/c/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->G:Ljavax/inject/Provider;

    .line 11960
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->D:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->F:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->G:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/a/p/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->H:Ljavax/inject/Provider;

    .line 11961
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->j(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->b:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->A:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->H:Ljavax/inject/Provider;

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p5, p5, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p5, p5, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p5}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p5

    invoke-static {p1, p2, p3, p4, p5}, Lcom/swedbank/mobile/app/navigation/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->I:Ljavax/inject/Provider;

    .line 11962
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$b$6;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->J:Ljavax/inject/Provider;

    .line 11967
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->J:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/l/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/l/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->K:Ljavax/inject/Provider;

    .line 11968
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->K:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/n/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/n/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->L:Ljavax/inject/Provider;

    .line 11969
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->I(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/g/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/g/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->M:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$n$b;)Ljavax/inject/Provider;
    .locals 0

    .line 11814
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->M:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11814
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b;->b()Lcom/swedbank/mobile/app/navigation/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/navigation/h;
    .locals 1

    .line 11974
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b;->I:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/navigation/h;

    return-object v0
.end method
