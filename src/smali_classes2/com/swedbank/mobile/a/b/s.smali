.class public final Lcom/swedbank/mobile/a/b/s;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/b/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c;,
        Lcom/swedbank/mobile/a/b/s$b;,
        Lcom/swedbank/mobile/a/b/s$a;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/notauth/list/a;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/e;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/a;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/y;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/cards/m;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/external/shortcut/a;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/statement/details/a;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/a;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;>;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/a;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/h/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/account/j;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/v;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/m/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/h;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private T:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private U:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private V:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private W:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/c;",
            ">;"
        }
    .end annotation
.end field

.field private X:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private Y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private Z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/app/Application;

.field private aA:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;"
        }
    .end annotation
.end field

.field private aB:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/biometric/authentication/c;",
            ">;"
        }
    .end annotation
.end field

.field private aC:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/network/aa;",
            ">;"
        }
    .end annotation
.end field

.field private aD:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/biometric/authentication/d;",
            ">;"
        }
    .end annotation
.end field

.field private aE:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/s;",
            ">;"
        }
    .end annotation
.end field

.field private aF:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/m;",
            ">;"
        }
    .end annotation
.end field

.field private aG:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private aH:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/overview/c;",
            ">;"
        }
    .end annotation
.end field

.field private aI:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/ClipboardManager;",
            ">;"
        }
    .end annotation
.end field

.field private aJ:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private aK:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/n;",
            ">;"
        }
    .end annotation
.end field

.field private aL:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/g/a;",
            ">;"
        }
    .end annotation
.end field

.field private aM:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/privacy/c;",
            ">;"
        }
    .end annotation
.end field

.field private aN:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/e;",
            ">;"
        }
    .end annotation
.end field

.field private aO:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/privacy/a;",
            ">;"
        }
    .end annotation
.end field

.field private aP:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/e/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private aQ:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/external/shortcut/i;",
            ">;"
        }
    .end annotation
.end field

.field private aR:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/i;",
            ">;"
        }
    .end annotation
.end field

.field private aS:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/root/a;",
            ">;"
        }
    .end annotation
.end field

.field private aT:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private aU:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/q;",
            ">;"
        }
    .end annotation
.end field

.field private ab:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/g/f;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/push/e;",
            ">;"
        }
    .end annotation
.end field

.field private ad:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/k/a;",
            ">;"
        }
    .end annotation
.end field

.field private ae:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/ordering/i;",
            ">;"
        }
    .end annotation
.end field

.field private af:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/push/h;",
            ">;"
        }
    .end annotation
.end field

.field private ag:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/biometric/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private ah:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private ai:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/i/a;",
            ">;"
        }
    .end annotation
.end field

.field private aj:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/network/j;",
            ">;"
        }
    .end annotation
.end field

.field private ak:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/network/a;",
            ">;"
        }
    .end annotation
.end field

.field private al:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;"
        }
    .end annotation
.end field

.field private am:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/j;",
            ">;"
        }
    .end annotation
.end field

.field private an:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private ao:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/j/a;",
            ">;"
        }
    .end annotation
.end field

.field private ap:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private aq:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private ar:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/network/ac;",
            ">;"
        }
    .end annotation
.end field

.field private as:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/x;",
            ">;"
        }
    .end annotation
.end field

.field private at:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;"
        }
    .end annotation
.end field

.field private au:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            ">;"
        }
    .end annotation
.end field

.field private av:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private aw:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/biometric/f;",
            ">;"
        }
    .end annotation
.end field

.field private ax:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/g;",
            ">;"
        }
    .end annotation
.end field

.field private ay:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private az:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/data/i;

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/KeyguardManager;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/a/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/b;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/k;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroidx/work/o;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/g/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/s;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/u;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/wallet/j;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lkotlin/k<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/squareup/moshi/JsonAdapter<",
            "*>;>;>;>;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/moshi/JsonAdapter$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/data/i;Landroid/app/Application;)V
    .locals 0

    .line 2252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2253
    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->a:Landroid/app/Application;

    .line 2254
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->b:Lcom/swedbank/mobile/data/i;

    .line 2255
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/data/i;Landroid/app/Application;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/data/i;Landroid/app/Application;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2053
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s;-><init>(Lcom/swedbank/mobile/data/i;Landroid/app/Application;)V

    return-void
.end method

.method static synthetic A(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ax:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic C(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->au:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ay:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic E(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->az:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic F(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->p:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aw:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic H(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aD:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic I(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->r:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic J(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->o:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic K(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->L:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic L(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aF:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic M(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ae:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic N(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aG:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic O(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->j:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ac:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic Q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->N:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic R(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aH:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic S(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aJ:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic T(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->O:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic U(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aK:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic V(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->i:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic W(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aL:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic X(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aM:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic Y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aN:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic Z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->n:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/data/i;Landroid/app/Application;)V
    .locals 11

    .line 2318
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    .line 2319
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/j;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/j;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->d:Ljavax/inject/Provider;

    .line 2320
    invoke-static {p1}, Lcom/swedbank/mobile/data/l;->a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/data/l;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    .line 2321
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p2, v0, v1}, Lcom/swedbank/mobile/data/device/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/device/g;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    .line 2322
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/data/g/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/g/e;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    .line 2323
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/wallet/g;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->h:Ljavax/inject/Provider;

    .line 2324
    invoke-static {}, Lcom/swedbank/mobile/app/g/l;->b()Lcom/swedbank/mobile/app/g/l;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->i:Ljavax/inject/Provider;

    .line 2325
    invoke-static {p1}, Lcom/swedbank/mobile/data/o;->a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/data/o;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->j:Ljavax/inject/Provider;

    .line 2326
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/g/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/g/b;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->k:Ljavax/inject/Provider;

    .line 2327
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->k:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/data/wallet/t;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/wallet/t;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->l:Ljavax/inject/Provider;

    .line 2328
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->l:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/wallet/v;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/wallet/v;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->m:Ljavax/inject/Provider;

    .line 2329
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->h:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->i:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s;->j:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s;->m:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/data/wallet/r;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/wallet/r;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->n:Ljavax/inject/Provider;

    .line 2330
    invoke-static {}, Lcom/swedbank/mobile/a/b/f;->b()Lcom/swedbank/mobile/a/b/f;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->o:Ljavax/inject/Provider;

    .line 2331
    invoke-static {}, Lcom/swedbank/mobile/a/b/p;->b()Lcom/swedbank/mobile/a/b/p;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->p:Ljavax/inject/Provider;

    .line 2332
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/data/f/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/f/b;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    .line 2333
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->p:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/a/b/q;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/q;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->r:Ljavax/inject/Provider;

    .line 2334
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->p:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->r:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/a/b/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/h;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    .line 2335
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p2, v0, v1}, Lcom/swedbank/mobile/data/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/b/c;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    const/4 p2, 0x0

    const/4 v0, 0x1

    .line 2336
    invoke-static {p2, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/data/network/r;->b()Lcom/swedbank/mobile/data/network/r;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s;->u:Ljavax/inject/Provider;

    const/4 v0, 0x4

    .line 2337
    invoke-static {p2, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/data/network/v;->b()Lcom/swedbank/mobile/data/network/v;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/data/overview/b;->b()Lcom/swedbank/mobile/data/overview/b;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/data/f;->b()Lcom/swedbank/mobile/data/f;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/data/transfer/c;->b()Lcom/swedbank/mobile/data/transfer/c;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->v:Ljavax/inject/Provider;

    const/4 v1, 0x2

    .line 2338
    invoke-static {p2, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/data/network/q;->b()Lcom/swedbank/mobile/data/network/q;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/data/transfer/b;->b()Lcom/swedbank/mobile/data/transfer/b;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->w:Ljavax/inject/Provider;

    .line 2339
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->u:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->v:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->w:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/swedbank/mobile/data/network/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/s;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    .line 2340
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/a/c;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->y:Ljavax/inject/Provider;

    .line 2341
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->o:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/swedbank/mobile/a/b/s;->y:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v10}, Lcom/swedbank/mobile/app/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/a/i;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->z:Ljavax/inject/Provider;

    .line 2342
    invoke-static {}, Lcom/swedbank/mobile/business/cards/list/c;->b()Lcom/swedbank/mobile/business/cards/list/c;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/notauth/list/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/notauth/list/c;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->A:Ljavax/inject/Provider;

    .line 2343
    invoke-static {}, Lcom/swedbank/mobile/business/cards/list/c;->b()Lcom/swedbank/mobile/business/cards/list/c;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/details/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/g;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->B:Ljavax/inject/Provider;

    .line 2344
    invoke-static {}, Lcom/swedbank/mobile/business/cards/list/c;->b()Lcom/swedbank/mobile/business/cards/list/c;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/c;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->C:Ljavax/inject/Provider;

    .line 2345
    invoke-static {}, Lcom/swedbank/mobile/business/cards/list/c;->b()Lcom/swedbank/mobile/business/cards/list/c;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/aa;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/aa;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->D:Ljavax/inject/Provider;

    .line 2346
    invoke-static {}, Lcom/swedbank/mobile/data/cards/o;->b()Lcom/swedbank/mobile/data/cards/o;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->E:Ljavax/inject/Provider;

    .line 2347
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->E:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/business/cards/wallet/payment/f;->b()Lcom/swedbank/mobile/business/cards/wallet/payment/f;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/business/cards/list/c;->b()Lcom/swedbank/mobile/business/cards/list/c;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/swedbank/mobile/business/external/shortcut/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/external/shortcut/d;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->F:Ljavax/inject/Provider;

    .line 2348
    invoke-static {}, Lcom/swedbank/mobile/business/overview/g;->b()Lcom/swedbank/mobile/business/overview/g;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/overview/statement/details/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/statement/details/c;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->G:Ljavax/inject/Provider;

    .line 2349
    invoke-static {}, Lcom/swedbank/mobile/business/transfer/c;->b()Lcom/swedbank/mobile/business/transfer/c;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/form/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/form/c;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->H:Ljavax/inject/Provider;

    const/16 v1, 0x14

    .line 2350
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/navigation/b;

    invoke-static {}, Lcom/swedbank/mobile/business/navigation/c;->b()Lcom/swedbank/mobile/business/navigation/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/cards/list/b;

    invoke-static {}, Lcom/swedbank/mobile/business/cards/list/c;->b()Lcom/swedbank/mobile/business/cards/list/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/cards/notauth/list/b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->A:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/cards/details/f;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->B:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->C:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/cards/wallet/payment/e;

    invoke-static {}, Lcom/swedbank/mobile/business/cards/wallet/payment/f;->b()Lcom/swedbank/mobile/business/cards/wallet/payment/f;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/cards/wallet/z;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->D:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/external/shortcut/c;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->F:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/overview/f;

    invoke-static {}, Lcom/swedbank/mobile/business/overview/g;->b()Lcom/swedbank/mobile/business/overview/g;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/overview/statement/details/b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->G:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/transfer/b;

    invoke-static {}, Lcom/swedbank/mobile/business/transfer/c;->b()Lcom/swedbank/mobile/business/transfer/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/transfer/payment/form/b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->H:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/transfer/request/opening/b;

    invoke-static {}, Lcom/swedbank/mobile/business/transfer/request/opening/c;->b()Lcom/swedbank/mobile/business/transfer/request/opening/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/widget/b;

    invoke-static {}, Lcom/swedbank/mobile/business/widget/c;->b()Lcom/swedbank/mobile/business/widget/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/widget/update/b;

    invoke-static {}, Lcom/swedbank/mobile/business/widget/update/c;->b()Lcom/swedbank/mobile/business/widget/update/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/services/e;

    invoke-static {}, Lcom/swedbank/mobile/business/services/f;->b()Lcom/swedbank/mobile/business/services/f;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/navigation/e;

    invoke-static {}, Lcom/swedbank/mobile/business/navigation/f;->b()Lcom/swedbank/mobile/business/navigation/f;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/notify/c;

    invoke-static {}, Lcom/swedbank/mobile/business/notify/d;->b()Lcom/swedbank/mobile/business/notify/d;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/j/b;

    invoke-static {}, Lcom/swedbank/mobile/app/j/c;->b()Lcom/swedbank/mobile/app/j/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/business/biometric/authentication/external/b;

    invoke-static {}, Lcom/swedbank/mobile/business/biometric/authentication/external/c;->b()Lcom/swedbank/mobile/business/biometric/authentication/external/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->I:Ljavax/inject/Provider;

    .line 2351
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->I:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    .line 2352
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/swedbank/mobile/app/r/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/b;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->K:Ljavax/inject/Provider;

    .line 2353
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/swedbank/mobile/app/transfer/payment/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/a/f;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->L:Ljavax/inject/Provider;

    .line 2354
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->z:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/h/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/h/a/d;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->M:Ljavax/inject/Provider;

    .line 2355
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/data/account/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/account/k;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->N:Ljavax/inject/Provider;

    .line 2356
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->E:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->n:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/business/cards/w;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/w;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->O:Ljavax/inject/Provider;

    .line 2357
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->N:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->O:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/swedbank/mobile/data/m/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/m/a/d;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    .line 2358
    invoke-static {}, Lcom/swedbank/mobile/a/b/m;->b()Lcom/swedbank/mobile/a/b/m;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->Q:Ljavax/inject/Provider;

    .line 2359
    invoke-static {}, Lcom/swedbank/mobile/app/g/i;->b()Lcom/swedbank/mobile/app/g/i;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->R:Ljavax/inject/Provider;

    .line 2360
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->K:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/i;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->S:Ljavax/inject/Provider;

    .line 2361
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/app/transfer/c/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/c/e;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->T:Ljavax/inject/Provider;

    .line 2362
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->L:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->T:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/a/ac/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/b;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->U:Ljavax/inject/Provider;

    .line 2363
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->M:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/k/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/k/b;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->V:Ljavax/inject/Provider;

    .line 2364
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->z:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/app/widget/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/d;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->W:Ljavax/inject/Provider;

    .line 2365
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->W:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/ae/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ae/b;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s;->X:Ljavax/inject/Provider;

    .line 2366
    invoke-static {p2, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->S:Ljavax/inject/Provider;

    invoke-virtual {p2, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->U:Ljavax/inject/Provider;

    invoke-virtual {p2, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->V:Ljavax/inject/Provider;

    invoke-virtual {p2, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->X:Ljavax/inject/Provider;

    invoke-virtual {p2, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/j$a;->a()La/a/j;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->Y:Ljavax/inject/Provider;

    .line 2367
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->Y:Ljavax/inject/Provider;

    invoke-static {p2, v0, v1}, Lcom/swedbank/mobile/app/g/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/g/e;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->Z:Ljavax/inject/Provider;

    .line 2368
    invoke-static {}, Lcom/swedbank/mobile/app/g/r;->b()Lcom/swedbank/mobile/app/g/r;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->aa:Ljavax/inject/Provider;

    .line 2369
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/g/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/g/g;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ab:Ljavax/inject/Provider;

    .line 2370
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/data/push/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/push/g;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ac:Ljavax/inject/Provider;

    .line 2371
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ac:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/k/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/k/b;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ad:Ljavax/inject/Provider;

    .line 2372
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/ordering/j;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/ordering/j;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ae:Ljavax/inject/Provider;

    .line 2373
    invoke-static {}, Lcom/swedbank/mobile/data/push/i;->b()Lcom/swedbank/mobile/data/push/i;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->af:Ljavax/inject/Provider;

    .line 2374
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/data/biometric/a/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/a/b;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ag:Ljavax/inject/Provider;

    .line 2375
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ah:Ljavax/inject/Provider;

    .line 2376
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/i/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/i/b;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ai:Ljavax/inject/Provider;

    .line 2377
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/network/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/k;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->aj:Ljavax/inject/Provider;

    .line 2378
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->aj:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/network/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ak:Ljavax/inject/Provider;

    .line 2379
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->ak:Ljavax/inject/Provider;

    invoke-static {p2, v0, v1, v2}, Lcom/swedbank/mobile/data/network/o;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/o;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->al:Ljavax/inject/Provider;

    .line 2380
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/authentication/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/k;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->am:Ljavax/inject/Provider;

    .line 2381
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    invoke-static {p2, v0}, Lcom/swedbank/mobile/data/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/c/c;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->an:Ljavax/inject/Provider;

    .line 2382
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/data/j/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/j/b;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ao:Ljavax/inject/Provider;

    .line 2383
    invoke-static {p1}, Lcom/swedbank/mobile/data/k;->a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/data/k;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->ap:Ljavax/inject/Provider;

    .line 2384
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->n:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aq:Ljavax/inject/Provider;

    .line 2385
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/data/network/ad;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/ad;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->ar:Ljavax/inject/Provider;

    .line 2386
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->al:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->ar:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/data/network/u;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/u;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->as:Ljavax/inject/Provider;

    .line 2387
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->as:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/data/network/t;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/t;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->at:Ljavax/inject/Provider;

    .line 2388
    invoke-static {}, Lcom/swedbank/mobile/data/biometric/b/a/b;->b()Lcom/swedbank/mobile/data/biometric/b/a/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->au:Ljavax/inject/Provider;

    .line 2389
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/authentication/a/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/a/d;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->av:Ljavax/inject/Provider;

    .line 2390
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->an:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/data/biometric/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/k;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aw:Ljavax/inject/Provider;

    .line 2391
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aw:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/h;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->ax:Ljavax/inject/Provider;

    .line 2392
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->R:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->ay:Ljavax/inject/Provider;

    .line 2393
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/app/customer/h;->b()Lcom/swedbank/mobile/app/customer/h;

    move-result-object p2

    invoke-static {}, Lcom/swedbank/mobile/app/customer/j;->b()Lcom/swedbank/mobile/app/customer/j;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/a/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->az:Ljavax/inject/Provider;

    .line 2394
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->al:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->am:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0, v1}, Lcom/swedbank/mobile/data/biometric/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/c;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aA:Ljavax/inject/Provider;

    .line 2395
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aA:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/biometric/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aB:Ljavax/inject/Provider;

    .line 2396
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aA:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/biometric/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aC:Ljavax/inject/Provider;

    .line 2397
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->an:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->aB:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->aC:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/data/biometric/authentication/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/authentication/e;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aD:Ljavax/inject/Provider;

    .line 2398
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/g/t;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/g/t;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aE:Ljavax/inject/Provider;

    .line 2399
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->aE:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/data/device/n;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/device/n;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aF:Ljavax/inject/Provider;

    .line 2400
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/network/p;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/network/p;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aG:Ljavax/inject/Provider;

    .line 2401
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/overview/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/overview/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aH:Ljavax/inject/Provider;

    .line 2402
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/g;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aI:Ljavax/inject/Provider;

    .line 2403
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->aI:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/e/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/e/c;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aJ:Ljavax/inject/Provider;

    .line 2404
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->i:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0, v1}, Lcom/swedbank/mobile/app/g/p;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/g/p;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aK:Ljavax/inject/Provider;

    .line 2405
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/data/g/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/g/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aL:Ljavax/inject/Provider;

    .line 2406
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->ao:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/privacy/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/privacy/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aM:Ljavax/inject/Provider;

    .line 2407
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aw:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aN:Ljavax/inject/Provider;

    .line 2408
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->ao:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/privacy/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/privacy/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aO:Ljavax/inject/Provider;

    .line 2409
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/data/e/a/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/e/a/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aP:Ljavax/inject/Provider;

    .line 2410
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->M:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->aP:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/external/shortcut/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/external/shortcut/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aQ:Ljavax/inject/Provider;

    .line 2411
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/widget/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aR:Ljavax/inject/Provider;

    .line 2412
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/business/widget/root/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/widget/root/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aS:Ljavax/inject/Provider;

    .line 2413
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s;->K:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/k;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/k;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aT:Ljavax/inject/Provider;

    .line 2414
    invoke-static {}, Lcom/swedbank/mobile/data/d/b;->b()Lcom/swedbank/mobile/data/d/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s;->aU:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic aa(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ab:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ab(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aO:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ac(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aQ:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ad(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->l:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ae(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->M:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic af(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aP:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ag(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aS:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ah(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aR:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ai(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aT:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic aj(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->af:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic ak(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aU:Ljavax/inject/Provider;

    return-object p0
.end method

.method private b(Lcom/swedbank/mobile/app/MainActivity;)Lcom/swedbank/mobile/app/MainActivity;
    .locals 1

    .line 2464
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->Q:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/g;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/b;->a(Lcom/swedbank/mobile/app/MainActivity;Lcom/swedbank/mobile/architect/a/b/g;)V

    .line 2465
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->R:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/g/h;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/b;->a(Lcom/swedbank/mobile/app/MainActivity;Lcom/swedbank/mobile/app/g/h;)V

    .line 2466
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->Z:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/g/c;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/b;->a(Lcom/swedbank/mobile/app/MainActivity;Lcom/swedbank/mobile/app/g/c;)V

    .line 2467
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->aa:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/g/q;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/b;->a(Lcom/swedbank/mobile/app/MainActivity;Lcom/swedbank/mobile/app/g/q;)V

    .line 2468
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/g/k;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/b;->a(Lcom/swedbank/mobile/app/MainActivity;Lcom/swedbank/mobile/app/g/k;)V

    .line 2469
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/c/a;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/b;->a(Lcom/swedbank/mobile/app/MainActivity;Lcom/swedbank/mobile/business/c/a;)V

    return-object p1
.end method

.method private b(Lcom/swedbank/mobile/data/push/PushMessageListenerService;)Lcom/swedbank/mobile/data/push/PushMessageListenerService;
    .locals 1

    .line 2475
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->t()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/push/c;->a(Lcom/swedbank/mobile/data/push/PushMessageListenerService;Ljava/util/Set;)V

    .line 2476
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->y()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/push/c;->b(Lcom/swedbank/mobile/data/push/PushMessageListenerService;Ljava/util/Set;)V

    .line 2477
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/firebase/c;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/push/c;->a(Lcom/swedbank/mobile/data/push/PushMessageListenerService;Lcom/swedbank/mobile/business/firebase/c;)V

    return-object p1
.end method

.method private b(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;
    .locals 1

    .line 2459
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->n:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/wallet/j;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/wallet/c;->a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;Lcom/swedbank/mobile/data/wallet/j;)V

    return-object p1
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->E:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ai:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->c:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static h()Lcom/swedbank/mobile/a/b/n$a;
    .locals 2

    .line 2259
    new-instance v0, Lcom/swedbank/mobile/a/b/s$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/a/b/s$a;-><init>(Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method static synthetic h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->Z:Ljavax/inject/Provider;

    return-object p0
.end method

.method private i()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 2263
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->K:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/r/a;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/i;->a(Lcom/swedbank/mobile/app/r/a;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ah:Ljavax/inject/Provider;

    return-object p0
.end method

.method private j()Lcom/swedbank/mobile/app/transfer/c/d;
    .locals 3

    .line 2266
    new-instance v0, Lcom/swedbank/mobile/app/transfer/c/d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/c;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->q:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/f/a;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/transfer/c/d;-><init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/f/a;)V

    return-object v0
.end method

.method static synthetic j(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->R:Ljavax/inject/Provider;

    return-object p0
.end method

.method private k()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 2269
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->L:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/d;

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->j()Lcom/swedbank/mobile/app/transfer/c/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/ac/b;->a(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/app/transfer/c/d;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->z:Ljavax/inject/Provider;

    return-object p0
.end method

.method private l()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 2272
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->M:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/h/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/a/k/b;->a(Lcom/swedbank/mobile/app/h/a/c;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->Q:Ljavax/inject/Provider;

    return-object p0
.end method

.method private m()Lcom/swedbank/mobile/app/widget/c;
    .locals 3

    .line 2275
    new-instance v0, Lcom/swedbank/mobile/app/widget/c;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->J:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/c;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->z:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/core/a/c;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/widget/c;-><init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/core/a/c;)V

    return-object v0
.end method

.method static synthetic m(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->e:Ljavax/inject/Provider;

    return-object p0
.end method

.method private n()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 2278
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->m()Lcom/swedbank/mobile/app/widget/c;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/ae/b;->a(Lcom/swedbank/mobile/app/widget/c;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->s:Ljavax/inject/Provider;

    return-object p0
.end method

.method private o()Lcom/swedbank/mobile/app/widget/i;
    .locals 4

    .line 2281
    new-instance v0, Lcom/swedbank/mobile/app/widget/i;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->a:Landroid/app/Application;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->f:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/e/i;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/widget/configuration/h;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/widget/i;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/widget/configuration/h;)V

    return-object v0
.end method

.method static synthetic o(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->x:Ljavax/inject/Provider;

    return-object p0
.end method

.method private p()Lcom/swedbank/mobile/business/widget/root/a;
    .locals 2

    .line 2284
    new-instance v0, Lcom/swedbank/mobile/business/widget/root/a;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/widget/configuration/h;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/widget/root/a;-><init>(Lcom/swedbank/mobile/business/widget/configuration/h;)V

    return-object v0
.end method

.method static synthetic p(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->al:Ljavax/inject/Provider;

    return-object p0
.end method

.method private q()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/b;",
            ">;"
        }
    .end annotation

    .line 2287
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->b:Lcom/swedbank/mobile/data/i;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->ab:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/g/f;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->ad:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/k/a;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/data/n;->a(Lcom/swedbank/mobile/data/i;Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/k/a;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->am:Ljavax/inject/Provider;

    return-object p0
.end method

.method private r()Lcom/swedbank/mobile/data/wallet/i;
    .locals 3

    .line 2290
    new-instance v0, Lcom/swedbank/mobile/data/wallet/i;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->h:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/wallet/e;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->t:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/c/a;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/data/wallet/i;-><init>(Lcom/swedbank/mobile/data/wallet/e;Lcom/swedbank/mobile/business/c/a;)V

    return-object v0
.end method

.method static synthetic r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->an:Ljavax/inject/Provider;

    return-object p0
.end method

.method private s()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/b;",
            ">;"
        }
    .end annotation

    .line 2293
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->r()Lcom/swedbank/mobile/data/wallet/i;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/e;->a(Lcom/swedbank/mobile/data/wallet/i;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->aq:Ljavax/inject/Provider;

    return-object p0
.end method

.method private t()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/b;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    .line 2296
    invoke-static {v0}, La/a/i;->a(I)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->q()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->s()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-virtual {v0}, La/a/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ap:Ljavax/inject/Provider;

    return-object p0
.end method

.method private u()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .line 2299
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->b:Lcom/swedbank/mobile/data/i;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->ab:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/g/f;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s;->ae:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/data/ordering/i;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s;->af:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/data/push/h;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/m;->a(Lcom/swedbank/mobile/data/i;Lcom/swedbank/mobile/data/g/f;Lcom/swedbank/mobile/data/ordering/i;Lcom/swedbank/mobile/data/push/h;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic u(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->g:Ljavax/inject/Provider;

    return-object p0
.end method

.method private v()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .line 2302
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->ag:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/biometric/a/a;

    invoke-static {v0}, Lcom/swedbank/mobile/data/biometric/e;->a(Lcom/swedbank/mobile/data/biometric/a/a;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->ao:Ljavax/inject/Provider;

    return-object p0
.end method

.method private w()Lcom/swedbank/mobile/data/wallet/h;
    .locals 2

    .line 2305
    new-instance v0, Lcom/swedbank/mobile/data/wallet/h;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s;->a:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/data/wallet/h;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method static synthetic w(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->at:Ljavax/inject/Provider;

    return-object p0
.end method

.method private x()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    .line 2308
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->w()Lcom/swedbank/mobile/data/wallet/h;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/d;->a(Lcom/swedbank/mobile/data/wallet/h;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->as:Ljavax/inject/Provider;

    return-object p0
.end method

.method private y()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/push/a;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    .line 2311
    invoke-static {v0}, La/a/i;->a(I)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->u()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->v()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->x()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-virtual {v0}, La/a/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->k:Ljavax/inject/Provider;

    return-object p0
.end method

.method private z()Lcom/swedbank/mobile/app/t/a;
    .locals 3

    .line 2314
    new-instance v0, Lcom/swedbank/mobile/app/t/a;

    new-instance v1, Lcom/swedbank/mobile/a/b/s$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/swedbank/mobile/a/b/s$b;-><init>(Lcom/swedbank/mobile/a/b/s;Lcom/swedbank/mobile/a/b/s$1;)V

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/t/a;-><init>(Lcom/swedbank/mobile/a/z/a$a;)V

    return-object v0
.end method

.method static synthetic z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;
    .locals 0

    .line 2053
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s;->av:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/a/c;
    .locals 1

    .line 2451
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->z()Lcom/swedbank/mobile/app/t/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/MainActivity;)V
    .locals 0

    .line 2443
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/app/MainActivity;)Lcom/swedbank/mobile/app/MainActivity;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/data/push/PushMessageListenerService;)V
    .locals 0

    .line 2447
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/data/push/PushMessageListenerService;)Lcom/swedbank/mobile/data/push/PushMessageListenerService;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V
    .locals 0

    .line 2419
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;

    return-void
.end method

.method public b()Lcom/swedbank/mobile/architect/business/a;
    .locals 1

    .line 2455
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->o:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/business/a;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/core/a/c;
    .locals 1

    .line 2423
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->z:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/a/c;

    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    .line 2427
    invoke-static {v0}, La/a/i;->a(I)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->i()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->k()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->l()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->n()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/i;->a(Ljava/util/Collection;)La/a/i;

    move-result-object v0

    invoke-virtual {v0}, La/a/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/widget/root/g;",
            ">;>;"
        }
    .end annotation

    .line 2439
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->p()Lcom/swedbank/mobile/business/widget/root/a;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/swedbank/mobile/app/widget/h;
    .locals 1

    .line 2431
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s;->o()Lcom/swedbank/mobile/app/widget/i;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/swedbank/mobile/business/widget/configuration/h;
    .locals 1

    .line 2435
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s;->P:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/widget/configuration/h;

    return-object v0
.end method
