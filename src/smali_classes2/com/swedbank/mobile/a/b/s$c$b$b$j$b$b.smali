.class final Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/v/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$l;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$k;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$g;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$p;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$o;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$m;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$r;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$q;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$j;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$i;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$e;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/v/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/l/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/i/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/bleedingedge/d;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/i/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/i;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private N:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/o;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private P:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/p/l;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/n/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/l/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/preference/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/h/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/e/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/e/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ae/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ae/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/x/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/x/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/w/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/q/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/privacy/preference/a;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/w/b/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;Lcom/swedbank/mobile/business/preferences/d;)V
    .locals 0

    .line 9765
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9767
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a(Lcom/swedbank/mobile/business/preferences/d;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;Lcom/swedbank/mobile/business/preferences/d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 9678
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;Lcom/swedbank/mobile/business/preferences/d;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)Ljavax/inject/Provider;
    .locals 0

    .line 9678
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->R:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/preferences/d;)V
    .locals 4

    .line 9772
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->b:Ljavax/inject/Provider;

    .line 9777
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/l/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/l/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->c:Ljavax/inject/Provider;

    .line 9778
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->d:Ljavax/inject/Provider;

    .line 9779
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->W(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->X(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/preferences/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/preferences/c;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->e:Ljavax/inject/Provider;

    .line 9780
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$2;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->f:Ljavax/inject/Provider;

    .line 9785
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->f:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/c/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/d/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->g:Ljavax/inject/Provider;

    .line 9786
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/preference/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/preference/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->h:Ljavax/inject/Provider;

    .line 9787
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->g:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->h:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->Y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/c/d/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/d/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->i:Ljavax/inject/Provider;

    .line 9788
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->i:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/d/d/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->j:Ljavax/inject/Provider;

    .line 9789
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$3;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->k:Ljavax/inject/Provider;

    .line 9794
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->k:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/e/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/e/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->l:Ljavax/inject/Provider;

    .line 9795
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->l:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->R(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/overview/e/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/e/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->m:Ljavax/inject/Provider;

    .line 9796
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->m:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/u/h/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/h/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->n:Ljavax/inject/Provider;

    .line 9797
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$4;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->o:Ljavax/inject/Provider;

    .line 9802
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->o:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/widget/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->p:Ljavax/inject/Provider;

    .line 9803
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->p:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/ae/b/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ae/b/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->q:Ljavax/inject/Provider;

    .line 9804
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->q:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/ae/b/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ae/b/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->r:Ljavax/inject/Provider;

    .line 9805
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$5;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->s:Ljavax/inject/Provider;

    .line 9810
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->s:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/r/c/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/c/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->t:Ljavax/inject/Provider;

    .line 9811
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->t:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/x/c/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/x/c/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->u:Ljavax/inject/Provider;

    .line 9812
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->u:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/x/c/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/x/c/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->v:Ljavax/inject/Provider;

    .line 9813
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$6;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->w:Ljavax/inject/Provider;

    .line 9818
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->w:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/q/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/q/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->x:Ljavax/inject/Provider;

    .line 9819
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->v(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/privacy/preference/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/privacy/preference/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->y:Ljavax/inject/Provider;

    .line 9820
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->x:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->y:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/w/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/w/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->z:Ljavax/inject/Provider;

    .line 9821
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$7;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$7;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->A:Ljavax/inject/Provider;

    .line 9826
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->A:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/p/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->B:Ljavax/inject/Provider;

    .line 9827
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->B:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/p/a/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->C:Ljavax/inject/Provider;

    .line 9828
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$8;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$8;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->D:Ljavax/inject/Provider;

    .line 9833
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->D:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/i/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/i/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->E:Ljavax/inject/Provider;

    .line 9834
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->W(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/firebase/bleedingedge/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/firebase/bleedingedge/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->F:Ljavax/inject/Provider;

    .line 9835
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->E:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->F:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/i/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/i/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->G:Ljavax/inject/Provider;

    const/4 p1, 0x3

    const/4 v0, 0x5

    .line 9836
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/a/v/b;->b()Lcom/swedbank/mobile/a/v/b;

    move-result-object v0

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->j:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->n:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->r:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->v:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->z:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->C:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->G:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->H:Ljavax/inject/Provider;

    .line 9837
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->H:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/v/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/v/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->I:Ljavax/inject/Provider;

    const/4 p1, 0x0

    const/4 v0, 0x1

    .line 9838
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->I:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->J:Ljavax/inject/Provider;

    .line 9839
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->J:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->e:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/app/p/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/k;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->K:Ljavax/inject/Provider;

    .line 9840
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->K:Ljavax/inject/Provider;

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->L:Ljavax/inject/Provider;

    .line 9841
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/p/o;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->L:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->M:Ljavax/inject/Provider;

    .line 9842
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/p/p;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/p;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->N:Ljavax/inject/Provider;

    .line 9843
    invoke-static {v0, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->N:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->O:Ljavax/inject/Provider;

    .line 9844
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->M:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->O:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/v/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/v/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->P:Ljavax/inject/Provider;

    .line 9845
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->e:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->P:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {p1, v0, v1, v2, v3}, Lcom/swedbank/mobile/app/p/m;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/p/m;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->Q:Ljavax/inject/Provider;

    .line 9846
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$9;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$9;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->R:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 9678
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->b()Lcom/swedbank/mobile/app/p/l;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/p/l;
    .locals 1

    .line 9855
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->Q:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/p/l;

    return-object v0
.end method
