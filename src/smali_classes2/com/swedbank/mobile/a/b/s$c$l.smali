.class final Lcom/swedbank/mobile/a/b/s$c$l;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ad/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "l"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/v/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/v/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/i/c;)V
    .locals 0

    .line 14772
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14774
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$l;->a(Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14755
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$l;-><init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/i/c;)V
    .locals 3

    .line 14779
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->b:Ljavax/inject/Provider;

    .line 14780
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$l;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->u(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->b:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/update/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/update/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->c:Ljavax/inject/Provider;

    .line 14781
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->c:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/v/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/v/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->d:Ljavax/inject/Provider;

    .line 14782
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->d:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->e:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 14783
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/v/h;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$l;->e:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$l;->f:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 14784
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/app/v/i;->b()Lcom/swedbank/mobile/app/v/i;

    move-result-object v0

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->g:Ljavax/inject/Provider;

    .line 14785
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$l;->g:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/ad/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ad/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->h:Ljavax/inject/Provider;

    .line 14786
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$l;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$l;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/v/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/v/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$l;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14755
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$l;->b()Lcom/swedbank/mobile/app/v/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/v/e;
    .locals 1

    .line 14791
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$l;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/v/e;

    return-object v0
.end method
