.class final Lcom/swedbank/mobile/a/b/s$c$z;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/b/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "z"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$z$b;,
        Lcom/swedbank/mobile/a/b/s$c$z$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/s;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/f;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/b/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/cards/wallet/payment/a;)V
    .locals 0

    .line 14228
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14230
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$z;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/cards/wallet/payment/a;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14213
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$z;-><init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/cards/wallet/payment/a;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/cards/wallet/payment/a;)V
    .locals 9

    .line 14235
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$z$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$z$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$z;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->b:Ljavax/inject/Provider;

    .line 14240
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->c:Ljavax/inject/Provider;

    .line 14241
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->ac(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->k(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/cards/t;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/t;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->d:Ljavax/inject/Provider;

    .line 14242
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->e:Ljavax/inject/Provider;

    .line 14243
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->f:Ljavax/inject/Provider;

    .line 14244
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->ad(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$z;->e:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c;->g(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$z;->f:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->T(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->g:Ljavax/inject/Provider;

    .line 14245
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$z;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$z;->d:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$z;->g:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$z;->g:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/cards/f/b/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/b/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z;->h:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14213
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$z;->b()Lcom/swedbank/mobile/app/cards/f/b/b/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/b/b/d;
    .locals 1

    .line 14250
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/b/d;

    return-object v0
.end method
