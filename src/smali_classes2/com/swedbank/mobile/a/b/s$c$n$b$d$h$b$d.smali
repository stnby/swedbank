.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;
.super Lcom/swedbank/mobile/a/e/g/a/d/a;
.source "DaggerApplicationComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/h;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V
    .locals 0

    .line 12442
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    invoke-direct {p0}, Lcom/swedbank/mobile/a/e/g/a/d/a;-><init>()V

    .line 12444
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->f()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12425
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)V

    return-void
.end method

.method private f()V
    .locals 4

    .line 12449
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->b(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->b:Ljavax/inject/Provider;

    .line 12450
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/d/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->c:Ljavax/inject/Provider;

    .line 12451
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->c:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->d:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 12452
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/d/h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->d:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->e:Ljavax/inject/Provider;

    .line 12453
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/a/d/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/d/i;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->f:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 12454
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->f:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->g:Ljavax/inject/Provider;

    .line 12455
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->e:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->g:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/d/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/g/a/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->h:Ljavax/inject/Provider;

    .line 12456
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->U(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->h:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/cards/f/a/d/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/d/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12425
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->b()Lcom/swedbank/mobile/app/cards/f/a/d/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/d/e;
    .locals 1

    .line 12461
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/d/e;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;
    .locals 1

    .line 12465
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;

    return-object v0
.end method

.method public synthetic d()Lcom/swedbank/mobile/business/cards/wallet/onboarding/l;
    .locals 1

    .line 12425
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b$d;->c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;

    move-result-object v0

    return-object v0
.end method
