.class final Lcom/swedbank/mobile/a/b/s$c$v$b$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/biometric/authentication/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/b/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/p;Lcom/swedbank/mobile/business/authentication/login/l;)V
    .locals 0

    .line 3520
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3522
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a(Lcom/swedbank/mobile/business/authentication/p;Lcom/swedbank/mobile/business/authentication/login/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/p;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3506
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$v$b$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/p;Lcom/swedbank/mobile/business/authentication/login/l;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/authentication/p;Lcom/swedbank/mobile/business/authentication/login/l;)V
    .locals 6

    .line 3528
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->b:Ljavax/inject/Provider;

    .line 3529
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->c:Ljavax/inject/Provider;

    .line 3530
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->c(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->H(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c;->b(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/data/biometric/authentication/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/biometric/authentication/b;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->d:Ljavax/inject/Provider;

    .line 3531
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c;->d(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->c(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->d:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->d(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->e(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/biometric/authentication/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/authentication/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->e:Ljavax/inject/Provider;

    .line 3532
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->c:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->f(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->C(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->e:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/biometric/login/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/login/c;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->f:Ljavax/inject/Provider;

    .line 3533
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->k(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->f:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lcom/swedbank/mobile/app/c/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/b/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->g:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3506
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->b()Lcom/swedbank/mobile/app/c/b/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/c/b/d;
    .locals 1

    .line 3538
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$d;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/c/b/d;

    return-object v0
.end method
