.class final Lcom/swedbank/mobile/a/b/s$c$b$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/s/e/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$f$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$g;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$j;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$i;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/w/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/q/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/privacy/onboarding/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/f;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/f;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/x/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/f;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/s/f;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/a/s/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/a/s/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;>;>;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/e/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 0

    .line 10885
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10887
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$f;->a(Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 10838
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$f;)Ljavax/inject/Provider;
    .locals 0

    .line 10838
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->u:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/i/c;)V
    .locals 6

    .line 10892
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->b:Ljavax/inject/Provider;

    .line 10897
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/onboarding/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->c:Ljavax/inject/Provider;

    .line 10898
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->d:Ljavax/inject/Provider;

    .line 10903
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/onboarding/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->e:Ljavax/inject/Provider;

    .line 10904
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->f:Ljavax/inject/Provider;

    .line 10905
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$f$3;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->g:Ljavax/inject/Provider;

    .line 10910
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->g:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/q/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/q/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->h:Ljavax/inject/Provider;

    .line 10911
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->v(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/privacy/onboarding/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/privacy/onboarding/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->i:Ljavax/inject/Provider;

    .line 10912
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->i:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/w/a/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/w/a/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->j:Ljavax/inject/Provider;

    .line 10913
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$f$4;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->k:Ljavax/inject/Provider;

    .line 10918
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->k:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/c/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/c/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->l:Ljavax/inject/Provider;

    .line 10919
    invoke-static {}, Lcom/swedbank/mobile/app/onboarding/h;->b()Lcom/swedbank/mobile/app/onboarding/h;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->m:Ljavax/inject/Provider;

    .line 10920
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->ab(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->Y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/biometric/onboarding/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/onboarding/h;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->n:Ljavax/inject/Provider;

    .line 10921
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->l:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->m:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->n:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/a/d/c/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/c/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->o:Ljavax/inject/Provider;

    .line 10922
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$f$5;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->p:Ljavax/inject/Provider;

    .line 10927
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->p:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/r/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->q:Ljavax/inject/Provider;

    .line 10928
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->ab(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/push/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->r:Ljavax/inject/Provider;

    .line 10929
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->q:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->m:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->r:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/a/x/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/x/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->s:Ljavax/inject/Provider;

    const/4 p1, 0x3

    const/4 v0, 0x0

    .line 10930
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->j:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->o:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->s:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->t:Ljavax/inject/Provider;

    .line 10931
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->t:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/s/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/h;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->u:Ljavax/inject/Provider;

    .line 10932
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->u:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/s/e/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/e/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->v:Ljavax/inject/Provider;

    .line 10933
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->f:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->v:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/onboarding/watcher/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/onboarding/watcher/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->w:Ljavax/inject/Provider;

    .line 10934
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->w:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->w:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->w:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/onboarding/e/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/e/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->x:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$f;)Ljavax/inject/Provider;
    .locals 0

    .line 10838
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->m:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 10838
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f;->b()Lcom/swedbank/mobile/app/onboarding/e/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/onboarding/e/c;
    .locals 1

    .line 10939
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f;->x:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/onboarding/e/c;

    return-object v0
.end method
