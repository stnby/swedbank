.class final Lcom/swedbank/mobile/a/b/s$c$b$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ab/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/u/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 0

    .line 11727
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11729
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$h;->a(Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11722
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/i/c;)V
    .locals 1

    .line 11734
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->i(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/sync/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/sync/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->b:Ljavax/inject/Provider;

    .line 11735
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->b:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/u/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/u/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->c:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11722
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$h;->b()Lcom/swedbank/mobile/app/u/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/u/c;
    .locals 1

    .line 11740
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$h;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/u/c;

    return-object v0
.end method
