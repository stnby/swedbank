.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/l/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "n"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/statement/details/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/statement/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/statement/a/j;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/statement/a/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;",
            "Lcom/swedbank/mobile/business/overview/statement/details/f;",
            ")V"
        }
    .end annotation

    .line 5890
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5892
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a(Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5868
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;",
            "Lcom/swedbank/mobile/business/overview/statement/details/f;",
            ")V"
        }
    .end annotation

    .line 5898
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->b:Ljavax/inject/Provider;

    .line 5899
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->c:Ljavax/inject/Provider;

    .line 5900
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->b:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->g(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lcom/swedbank/mobile/business/overview/statement/details/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/statement/details/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->d:Ljavax/inject/Provider;

    .line 5901
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->d:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/statement/a/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/statement/a/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->e:Ljavax/inject/Provider;

    .line 5902
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->e:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->f:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 5903
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/overview/statement/a/j;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->f:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->g:Ljavax/inject/Provider;

    .line 5904
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p2}, Lcom/swedbank/mobile/app/overview/statement/a/m;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/statement/a/m;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->h:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 5905
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->h:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->i:Ljavax/inject/Provider;

    .line 5906
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->g:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->i:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/u/l/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/l/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->j:Ljavax/inject/Provider;

    .line 5907
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->d:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->j:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lcom/swedbank/mobile/app/overview/statement/a/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/statement/a/h;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->k:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5868
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->b()Lcom/swedbank/mobile/app/overview/statement/a/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/statement/a/g;
    .locals 1

    .line 5912
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$n;->k:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/statement/a/g;

    return-object v0
.end method
