.class final Lcom/swedbank/mobile/a/b/s$c$v;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/e/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "v"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$v$d;,
        Lcom/swedbank/mobile/a/b/s$c$v$c;,
        Lcom/swedbank/mobile/a/b/s$c$v$b;,
        Lcom/swedbank/mobile/a/b/s$c$v$a;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/c;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/j;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/scoped/a;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/a;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/e/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/g;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/network/aa;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/c;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/f;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/e;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/b;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/b;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/t;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/p;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/t;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)V"
        }
    .end annotation

    .line 2828
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2830
    invoke-direct {p0, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$v;->a(Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2763
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/a/b/s$c$v;-><init>(Lcom/swedbank/mobile/a/b/s$c;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->F:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/util/l;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)V"
        }
    .end annotation

    .line 2837
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$v;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->b:Ljavax/inject/Provider;

    .line 2842
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->c:Ljavax/inject/Provider;

    .line 2843
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$v;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->d:Ljavax/inject/Provider;

    .line 2848
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/e/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/e/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->e:Ljavax/inject/Provider;

    .line 2849
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->o(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->p(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/a/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/c;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->f:Ljavax/inject/Provider;

    .line 2850
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->g:Ljavax/inject/Provider;

    .line 2851
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->h:Ljavax/inject/Provider;

    .line 2852
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->i:Ljavax/inject/Provider;

    .line 2853
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->j:Ljavax/inject/Provider;

    .line 2854
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->k:Ljavax/inject/Provider;

    .line 2855
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/j;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->l:Ljavax/inject/Provider;

    .line 2856
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/h;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->m:Ljavax/inject/Provider;

    .line 2857
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->g:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->h:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v;->i:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v;->j:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v;->k:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v;->l:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$v;->m:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/data/authentication/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/e;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->n:Ljavax/inject/Provider;

    .line 2858
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c;->a(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/data/authentication/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    .line 2859
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/g;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->p:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 2860
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->t(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->s(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->q:Ljavax/inject/Provider;

    .line 2861
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->q:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->u(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/e/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/e/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->r:Ljavax/inject/Provider;

    .line 2862
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->v(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->r:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/session/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->s:Ljavax/inject/Provider;

    .line 2863
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->n:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->p:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v;->s:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/session/refresh/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/refresh/c;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->t:Ljavax/inject/Provider;

    .line 2864
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->t:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/u;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/u;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->u:Ljavax/inject/Provider;

    .line 2865
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/q;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/q;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->v:Ljavax/inject/Provider;

    .line 2866
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->n:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->s:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/u;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/u;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->w:Ljavax/inject/Provider;

    .line 2867
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->u:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->w(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->x(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v;->v:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v;->w:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v;->t:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/a/c/e/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->x:Ljavax/inject/Provider;

    .line 2868
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->y:Ljavax/inject/Provider;

    .line 2869
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->z:Ljavax/inject/Provider;

    .line 2870
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->o:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->z:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/business/authentication/session/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/j;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->A:Ljavax/inject/Provider;

    .line 2871
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->B:Ljavax/inject/Provider;

    .line 2872
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->x:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v;->y:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v;->A:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v;->A:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v;->B:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$v;->A:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/b/e/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/e/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->C:Ljavax/inject/Provider;

    .line 2873
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/data/authentication/b/m;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/m;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->D:Ljavax/inject/Provider;

    .line 2874
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->n:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v;->D:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/authentication/session/scoped/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/scoped/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->E:Ljavax/inject/Provider;

    .line 2875
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->n:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/b/r;->b()Lcom/swedbank/mobile/a/b/r;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$v;->p:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->E:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, v0}, Lcom/swedbank/mobile/business/authentication/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v;->F:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->r:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->n:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->p:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->E:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic f(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->w:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic g(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;
    .locals 0

    .line 2763
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->t:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 2763
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v;->b()Lcom/swedbank/mobile/app/b/e/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/e/c;
    .locals 1

    .line 2880
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v;->C:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/c;

    return-object v0
.end method
