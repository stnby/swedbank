.class final Lcom/swedbank/mobile/a/b/s$c$z$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/b/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$z$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$z$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/a/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V
    .locals 0

    .line 14379
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14381
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14372
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$z$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V
    .locals 2

    .line 14386
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->b:Ljavax/inject/Provider;

    .line 14387
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->b:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->c:Ljavax/inject/Provider;

    .line 14388
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->U(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/a/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/a/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14372
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->b()Lcom/swedbank/mobile/app/cards/f/b/a/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/b/a/d;
    .locals 1

    .line 14393
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/a/d;

    return-object v0
.end method
