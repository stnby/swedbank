.class final Lcom/swedbank/mobile/a/b/s$c$b$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/p/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b;

.field private b:Lcom/swedbank/mobile/a/i/d;

.field private c:Lcom/swedbank/mobile/a/p/a/b;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;)V
    .locals 0

    .line 4747
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 4747
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/b/s$c$b$a;
    .locals 0

    .line 4754
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/a/i/d;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->b:Lcom/swedbank/mobile/a/i/d;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/a/p/a/b;)Lcom/swedbank/mobile/a/b/s$c$b$a;
    .locals 0

    .line 4761
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/a/p/a/b;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->c:Lcom/swedbank/mobile/a/p/a/b;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/p/a/a;
    .locals 12

    .line 4767
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->b:Lcom/swedbank/mobile/a/i/d;

    const-class v1, Lcom/swedbank/mobile/a/i/d;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 4768
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->c:Lcom/swedbank/mobile/a/p/a/b;

    const-class v1, Lcom/swedbank/mobile/a/p/a/b;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 4769
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->c:Lcom/swedbank/mobile/a/p/a/b;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$a;->b:Lcom/swedbank/mobile/a/i/d;

    new-instance v6, Lcom/swedbank/mobile/a/u/f;

    invoke-direct {v6}, Lcom/swedbank/mobile/a/u/f;-><init>()V

    new-instance v7, Lcom/swedbank/mobile/a/ac/i;

    invoke-direct {v7}, Lcom/swedbank/mobile/a/ac/i;-><init>()V

    new-instance v8, Lcom/swedbank/mobile/a/e/d;

    invoke-direct {v8}, Lcom/swedbank/mobile/a/e/d;-><init>()V

    new-instance v9, Lcom/swedbank/mobile/a/aa/f;

    invoke-direct {v9}, Lcom/swedbank/mobile/a/aa/f;-><init>()V

    new-instance v10, Lcom/swedbank/mobile/a/h/a/g;

    invoke-direct {v10}, Lcom/swedbank/mobile/a/h/a/g;-><init>()V

    const/4 v11, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/swedbank/mobile/a/b/s$c$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/a/p/a/b;Lcom/swedbank/mobile/a/i/d;Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/a/ac/i;Lcom/swedbank/mobile/a/e/d;Lcom/swedbank/mobile/a/aa/f;Lcom/swedbank/mobile/a/h/a/g;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/p/a/a$a;
    .locals 0

    .line 4747
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$a;->a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/b/s$c$b$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/a/p/a/b;)Lcom/swedbank/mobile/a/p/a/a$a;
    .locals 0

    .line 4747
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$a;->a(Lcom/swedbank/mobile/a/p/a/b;)Lcom/swedbank/mobile/a/b/s$c$b$a;

    move-result-object p1

    return-object p1
.end method
