.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/f/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/d/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ad;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/w;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/aa;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/a/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;)V"
        }
    .end annotation

    .line 6472
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6474
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6437
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 6481
    new-instance v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;)V

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->b:Ljavax/inject/Provider;

    .line 6486
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->b:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/d/b;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->c:Ljavax/inject/Provider;

    .line 6487
    new-instance v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$2;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;)V

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->d:Ljavax/inject/Provider;

    .line 6492
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->d:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/payment/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/b/b;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->e:Ljavax/inject/Provider;

    .line 6493
    new-instance v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$3;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;)V

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->f:Ljavax/inject/Provider;

    .line 6498
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->f:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/c/a/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/h;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->g:Ljavax/inject/Provider;

    .line 6499
    invoke-static/range {p2 .. p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->h:Ljavax/inject/Provider;

    .line 6500
    invoke-static/range {p3 .. p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->i:Ljavax/inject/Provider;

    .line 6501
    invoke-static/range {p4 .. p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->j:Ljavax/inject/Provider;

    .line 6502
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->k:Ljavax/inject/Provider;

    .line 6503
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->c(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ae;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/ae;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->l:Ljavax/inject/Provider;

    .line 6504
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->c(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/z;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/z;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->m:Ljavax/inject/Provider;

    .line 6505
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->c(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ab;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/ab;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->n:Ljavax/inject/Provider;

    .line 6506
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->H(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b;->d(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/swedbank/mobile/business/biometric/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->o:Ljavax/inject/Provider;

    .line 6507
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->c(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->h:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->i:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->j:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->k:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->l:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->m:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/ac/d/f;->b()Lcom/swedbank/mobile/a/ac/d/f;

    move-result-object v9

    invoke-static {}, Lcom/swedbank/mobile/a/ac/d/b;->b()Lcom/swedbank/mobile/a/ac/d/b;

    move-result-object v10

    iget-object v11, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->n:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->o:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->e(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v13

    invoke-static/range {v2 .. v13}, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/k;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->p:Ljavax/inject/Provider;

    .line 6508
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->c:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->e:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->g:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->p:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->p:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->p:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/app/transfer/payment/a/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/payment/a/h;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->q:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6437
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->b()Lcom/swedbank/mobile/app/transfer/payment/a/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/payment/a/g;
    .locals 1

    .line 6513
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;->q:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/g;

    return-object v0
.end method
