.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/g/c/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/statement/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/c/b/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;)V
    .locals 0

    .line 6033
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6035
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6026
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;)V

    return-void
.end method

.method private c()V
    .locals 4

    .line 6040
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->d(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/statement/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/statement/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->b:Ljavax/inject/Provider;

    .line 6041
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/plugins/search/remote/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->c:Ljavax/inject/Provider;

    .line 6042
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/overview/d/c/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/c/b/e;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6026
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->b()Lcom/swedbank/mobile/app/overview/d/c/b/d;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/d/c/b/d;
    .locals 1

    .line 6047
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$l$d;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/c/b/d;

    return-object v0
.end method
