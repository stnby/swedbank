.class final Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/aa/c/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/c/a/a/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/a/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/services/agreements/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/services/agreements/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/a/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l;)V
    .locals 0

    .line 9272
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9274
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 9255
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l;)V

    return-void
.end method

.method private c()V
    .locals 8

    .line 9279
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->b:Ljavax/inject/Provider;

    .line 9284
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/services/c/a/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->c:Ljavax/inject/Provider;

    .line 9285
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->d:Ljavax/inject/Provider;

    .line 9290
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->e:Ljavax/inject/Provider;

    .line 9291
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/aa/c/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/a/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->f:Ljavax/inject/Provider;

    .line 9292
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->f:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->o(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/data/services/agreements/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/services/agreements/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->g:Ljavax/inject/Provider;

    .line 9293
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/services/plugins/agreements/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/services/plugins/agreements/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->h:Ljavax/inject/Provider;

    .line 9294
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->e:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->h:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->h:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$l;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/services/c/a/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/a/j;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 9255
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->b()Lcom/swedbank/mobile/app/services/c/a/i;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/services/c/a/i;
    .locals 1

    .line 9299
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/services/c/a/i;

    return-object v0
.end method
