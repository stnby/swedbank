.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/d/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

.field private b:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

.field private c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field private d:Ljava/lang/String;

.field private e:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;)V
    .locals 0

    .line 6394
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6394
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/ac/d/a/a;
    .locals 9

    .line 6429
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    const-class v1, Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6430
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    const-class v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6431
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->d:Ljava/lang/String;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6432
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->e:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6433
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->d:Ljava/lang/String;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->e:Lcom/swedbank/mobile/business/util/l;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;
    .locals 0

    .line 6411
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;
    .locals 0

    .line 6405
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;"
        }
    .end annotation

    .line 6423
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->e:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;
    .locals 0

    .line 6417
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->d:Ljava/lang/String;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/a/ac/d/a/a$a;
    .locals 0

    .line 6394
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)Lcom/swedbank/mobile/a/ac/d/a/a$a;
    .locals 0

    .line 6394
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/ac/d/a/a$a;
    .locals 0

    .line 6394
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/swedbank/mobile/a/ac/d/a/a$a;
    .locals 0

    .line 6394
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;->a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$b$e;

    move-result-object p1

    return-object p1
.end method
