.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/h/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "n"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ac/h/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/transfer/search/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/d;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/o;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/l;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/search/c;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/search/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/search/c;)V
    .locals 0

    .line 6997
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6999
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a(Lcom/swedbank/mobile/business/transfer/search/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/search/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6952
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/search/c;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)Ljavax/inject/Provider;
    .locals 0

    .line 6952
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->w:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/transfer/search/c;)V
    .locals 4

    .line 7004
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->b:Ljavax/inject/Provider;

    .line 7005
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->c:Ljavax/inject/Provider;

    .line 7010
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->c:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/search/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->d:Ljavax/inject/Provider;

    .line 7011
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->d:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/ac/h/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->e:Ljavax/inject/Provider;

    .line 7012
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$2;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->f:Ljavax/inject/Provider;

    .line 7017
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->f:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/search/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->g:Ljavax/inject/Provider;

    .line 7018
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->g:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/ac/h/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h/b/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->h:Ljavax/inject/Provider;

    .line 7019
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$3;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->i:Ljavax/inject/Provider;

    .line 7024
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->i:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/search/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/c/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->j:Ljavax/inject/Provider;

    .line 7025
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->j:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/ac/h/c/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h/c/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->k:Ljavax/inject/Provider;

    const/4 p1, 0x0

    const/4 v0, 0x3

    .line 7026
    invoke-static {v0, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->e:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->h:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->k:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->l:Ljavax/inject/Provider;

    .line 7027
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->l:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/ac/h/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->m:Ljavax/inject/Provider;

    .line 7028
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->m:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/transfer/search/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/search/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->n:Ljavax/inject/Provider;

    .line 7029
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->n:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/search/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->o:Ljavax/inject/Provider;

    .line 7030
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->o:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->p:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 7031
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/o;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->p:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->q:Ljavax/inject/Provider;

    .line 7032
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/search/r;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/r;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->r:Ljavax/inject/Provider;

    .line 7033
    invoke-static {v0, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->r:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->s:Ljavax/inject/Provider;

    .line 7034
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->q:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->s:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/ac/h/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->t:Ljavax/inject/Provider;

    .line 7035
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->n:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->t:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/transfer/search/m;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/m;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->u:Ljavax/inject/Provider;

    .line 7036
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->a(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/a/ac/h/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/h/c;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->v:Ljavax/inject/Provider;

    .line 7037
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->v:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->N(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/data/transfer/search/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/transfer/search/b;

    move-result-object p1

    invoke-static {p1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->w:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 6952
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->b()Lcom/swedbank/mobile/app/transfer/search/l;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/search/l;
    .locals 1

    .line 7042
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->u:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/search/l;

    return-object v0
.end method
