.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/a/f/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/b/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 8923
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8925
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a(Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 8896
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8896
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->j:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 8933
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->b:Ljavax/inject/Provider;

    .line 8938
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/d/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->c:Ljavax/inject/Provider;

    .line 8939
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$2;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->d:Ljavax/inject/Provider;

    .line 8944
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->d:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->e:Ljavax/inject/Provider;

    .line 8945
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$3;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->f:Ljavax/inject/Provider;

    .line 8950
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->f:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/e/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/e/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->g:Ljavax/inject/Provider;

    .line 8951
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->e:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->g:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/cards/f/a/f/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/f/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->h:Ljavax/inject/Provider;

    .line 8952
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->i:Ljavax/inject/Provider;

    .line 8953
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->j:Ljavax/inject/Provider;

    .line 8954
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->i:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->j:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->f(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->g(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->k:Ljavax/inject/Provider;

    .line 8955
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->k:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->l:Ljavax/inject/Provider;

    .line 8956
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->l:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->l:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p2, p3, p4}, Lcom/swedbank/mobile/app/cards/f/a/f/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/f/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->m:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8896
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->l:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 8896
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->b()Lcom/swedbank/mobile/app/cards/f/a/f/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/f/e;
    .locals 1

    .line 8961
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/f/e;

    return-object v0
.end method
