.class final Lcom/swedbank/mobile/a/b/s$c$n$d$j;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "j"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/q;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V
    .locals 0

    .line 14171
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14173
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14152
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$d$j;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    return-void
.end method

.method private c()V
    .locals 5

    .line 14178
    invoke-static {}, Lcom/swedbank/mobile/data/authentication/i;->b()Lcom/swedbank/mobile/data/authentication/i;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->b:Ljavax/inject/Provider;

    .line 14179
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->z(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/s;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/s;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->c:Ljavax/inject/Provider;

    .line 14180
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->b:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->d:Ljavax/inject/Provider;

    .line 14181
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/d/a/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->e:Ljavax/inject/Provider;

    .line 14182
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->e:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->f:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 14183
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->f:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->g:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 14184
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/app/b/d/a/l;->b()Lcom/swedbank/mobile/app/b/d/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->h:Ljavax/inject/Provider;

    .line 14185
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->h:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/c/d/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->i:Ljavax/inject/Provider;

    .line 14186
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->d:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->i:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v4, v4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v4}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/b/d/a/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/a/h;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14152
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->b()Lcom/swedbank/mobile/app/b/d/a/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/d/a/g;
    .locals 1

    .line 14191
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$j;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/d/a/g;

    return-object v0
.end method
