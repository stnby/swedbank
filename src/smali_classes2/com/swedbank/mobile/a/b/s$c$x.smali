.class final Lcom/swedbank/mobile/a/b/s$c$x;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/k/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "x"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/h/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/i/c;)V
    .locals 0

    .line 14875
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14877
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$x;->a(Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14868
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$x;-><init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/business/i/c;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/i/c;)V
    .locals 3

    .line 14882
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->b:Ljavax/inject/Provider;

    .line 14883
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->ae(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$x;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->af(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->b:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$x;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->T(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/business/external/shortcut/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/external/shortcut/e;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->c:Ljavax/inject/Provider;

    .line 14884
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$x;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/h/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/h/a/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$x;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14868
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$x;->b()Lcom/swedbank/mobile/app/h/a/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/h/a/e;
    .locals 1

    .line 14889
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$x;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/h/a/e;

    return-object v0
.end method
