.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/e/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/loading/LoadingOverviewInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/b/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V
    .locals 0

    .line 5683
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5685
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5666
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 5690
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/u/e/d;->b()Lcom/swedbank/mobile/a/u/e/d;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->b:Ljavax/inject/Provider;

    .line 5691
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->b:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/u/e/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/e/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->c:Ljavax/inject/Provider;

    .line 5692
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->c:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/business/overview/loading/a;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/loading/a;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->d:Ljavax/inject/Provider;

    .line 5693
    invoke-static {}, Lcom/swedbank/mobile/app/overview/b/d;->b()Lcom/swedbank/mobile/app/overview/b/d;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->e:Ljavax/inject/Provider;

    .line 5694
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/overview/b/h;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->f:Ljavax/inject/Provider;

    .line 5695
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/app/overview/b/i;->b()Lcom/swedbank/mobile/app/overview/b/i;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->g:Ljavax/inject/Provider;

    .line 5696
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->f:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->g:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/u/e/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/e/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->h:Ljavax/inject/Provider;

    .line 5697
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/overview/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/b/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5666
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->b()Lcom/swedbank/mobile/app/overview/b/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/b/e;
    .locals 1

    .line 5702
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$d;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/b/e;

    return-object v0
.end method
