.class final Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$d$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/loading/LoadingCardsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/b/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)V
    .locals 0

    .line 3880
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3882
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3863
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)V

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3887
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/e/c/d;->b()Lcom/swedbank/mobile/a/e/c/d;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->b:Ljavax/inject/Provider;

    .line 3888
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->b:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/e/c/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/c/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->c:Ljavax/inject/Provider;

    .line 3889
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->c:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/business/cards/loading/a;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/loading/a;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->d:Ljavax/inject/Provider;

    .line 3890
    invoke-static {}, Lcom/swedbank/mobile/app/cards/b/d;->b()Lcom/swedbank/mobile/app/cards/b/d;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->e:Ljavax/inject/Provider;

    .line 3891
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/cards/list/m;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->f:Ljavax/inject/Provider;

    .line 3892
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/app/cards/list/q;->b()Lcom/swedbank/mobile/app/cards/list/q;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->g:Ljavax/inject/Provider;

    .line 3893
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->f:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->g:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/e/c/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->h:Ljavax/inject/Provider;

    .line 3894
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/cards/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/b/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3863
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->b()Lcom/swedbank/mobile/app/cards/b/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/b/e;
    .locals 1

    .line 3899
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$b;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/b/e;

    return-object v0
.end method
