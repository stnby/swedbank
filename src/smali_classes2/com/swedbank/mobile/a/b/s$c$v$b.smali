.class final Lcom/swedbank/mobile/a/b/s$c$v$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/b/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$v$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$a;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$v$b$e;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/handling/b;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/b/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v;Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/handling/b;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)V"
        }
    .end annotation

    .line 2930
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2932
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$v$b;->a(Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v;Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2908
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$v$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$v;Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$v$b;)Ljavax/inject/Provider;
    .locals 0

    .line 2908
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->e:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/handling/b;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)V"
        }
    .end annotation

    .line 2938
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->b:Ljavax/inject/Provider;

    .line 2943
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->c:Ljavax/inject/Provider;

    .line 2944
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->d:Ljavax/inject/Provider;

    .line 2949
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->e:Ljavax/inject/Provider;

    .line 2950
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->f:Ljavax/inject/Provider;

    .line 2955
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->g:Ljavax/inject/Provider;

    .line 2956
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->h:Ljavax/inject/Provider;

    .line 2957
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->i:Ljavax/inject/Provider;

    .line 2958
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c;->b(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->i:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/business/authentication/login/handling/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/login/handling/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->j:Ljavax/inject/Provider;

    .line 2959
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->j:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->j:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->j:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/b/b/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/a/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->k:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$v$b;)Ljavax/inject/Provider;
    .locals 0

    .line 2908
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->g:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 2908
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$b;->b()Lcom/swedbank/mobile/app/b/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/b/a/c;
    .locals 1

    .line 2964
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b;->k:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/a/c;

    return-object v0
.end method
