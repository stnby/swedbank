.class public final Lcom/swedbank/mobile/a/b/d;
.super Ljava/lang/Object;
.source "AppModule_ProvideAuthenticatedComponentHandlerFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/a/c/a/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/d;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/d;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/b/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/d;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Landroid/app/Application;)Lcom/swedbank/mobile/a/c/a/b;
    .locals 1

    .line 29
    invoke-static {p0}, Lcom/swedbank/mobile/a/b/a;->a(Landroid/app/Application;)Lcom/swedbank/mobile/a/c/a/b;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/a/c/a/b;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/c/a/b;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/d;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/d;->a(Landroid/app/Application;)Lcom/swedbank/mobile/a/c/a/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/d;->a()Lcom/swedbank/mobile/a/c/a/b;

    move-result-object v0

    return-object v0
.end method
