.class public final Lcom/swedbank/mobile/a/b/j;
.super Ljava/lang/Object;
.source "AppModule_ProvideKeyguardManagerFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Landroid/app/KeyguardManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/j;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Landroid/app/Application;)Landroid/app/KeyguardManager;
    .locals 1

    .line 27
    invoke-static {p0}, Lcom/swedbank/mobile/a/b/a;->b(Landroid/app/Application;)Landroid/app/KeyguardManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/KeyguardManager;

    return-object p0
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/j;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/swedbank/mobile/a/b/j;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/j;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Landroid/app/KeyguardManager;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/j;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/j;->a(Landroid/app/Application;)Landroid/app/KeyguardManager;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/j;->a()Landroid/app/KeyguardManager;

    move-result-object v0

    return-object v0
.end method
