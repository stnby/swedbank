.class final Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;
.super Lcom/swedbank/mobile/a/e/g/a/b/c;
.source "DaggerApplicationComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;)V
    .locals 0

    .line 13591
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;

    invoke-direct {p0}, Lcom/swedbank/mobile/a/e/g/a/b/c;-><init>()V

    .line 13593
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->f()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 13574
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;)V

    return-void
.end method

.method private f()V
    .locals 4

    .line 13598
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;->b(Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->b:Ljavax/inject/Provider;

    .line 13599
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->c:Ljavax/inject/Provider;

    .line 13600
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->c:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->d:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 13601
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/b/h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->d:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->e:Ljavax/inject/Provider;

    .line 13602
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/a/b/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/b/i;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->f:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 13603
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->f:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->g:Ljavax/inject/Provider;

    .line 13604
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->e:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->g:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/b/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/g/a/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->h:Ljavax/inject/Provider;

    .line 13605
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->h:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$d$f;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/a/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/b/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->i:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 13574
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->b()Lcom/swedbank/mobile/app/cards/f/a/b/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/b/e;
    .locals 1

    .line 13610
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/b/e;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/a;
    .locals 1

    .line 13614
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/a;

    return-object v0
.end method

.method public synthetic d()Lcom/swedbank/mobile/business/cards/wallet/onboarding/l;
    .locals 1

    .line 13574
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$h$b$b;->c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/a;

    move-result-object v0

    return-object v0
.end method
