.class final Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/s/d/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/business/onboarding/g;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;)V
    .locals 0

    .line 11110
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11110
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;
    .locals 0

    .line 11123
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/onboarding/g;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->c:Lcom/swedbank/mobile/business/onboarding/g;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;"
        }
    .end annotation

    .line 11117
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->b:Ljava/util/List;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/s/d/a;
    .locals 5

    .line 11129
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->b:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 11130
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->c:Lcom/swedbank/mobile/business/onboarding/g;

    const-class v1, Lcom/swedbank/mobile/business/onboarding/g;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 11131
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->b:Ljava/util/List;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->c:Lcom/swedbank/mobile/business/onboarding/g;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/a/s/d/a$a;
    .locals 0

    .line 11110
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->a(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/util/List;)Lcom/swedbank/mobile/a/s/d/a$a;
    .locals 0

    .line 11110
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;->a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$b$f$f$a;

    move-result-object p1

    return-object p1
.end method
