.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/a/f/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private e:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;)V
    .locals 0

    .line 8852
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 8852
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;
    .locals 0

    .line 8876
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;"
        }
    .end annotation

    .line 8869
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->c:Ljava/util/List;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;
    .locals 0

    .line 8863
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->b:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/e/g/a/f/a;
    .locals 9

    .line 8888
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->b:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8889
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->c:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8890
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8891
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->e:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8892
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->b:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->c:Ljava/util/List;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->e:Ljava/lang/Boolean;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$f;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;
    .locals 0

    .line 8882
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->e:Ljava/lang/Boolean;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 8852
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 8852
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Z)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 8852
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->b(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Z)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 8852
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;->a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$f$c;

    move-result-object p1

    return-object p1
.end method
