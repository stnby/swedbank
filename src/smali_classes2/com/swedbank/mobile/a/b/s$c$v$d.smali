.class final Lcom/swedbank/mobile/a/b/s$c$v$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$v$d$b;,
        Lcom/swedbank/mobile/a/b/s$c$v$d$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/p/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/g/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v;Lcom/swedbank/mobile/business/authentication/session/refresh/e;)V
    .locals 0

    .line 3571
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3573
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$v$d;->a(Lcom/swedbank/mobile/business/authentication/session/refresh/e;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v;Lcom/swedbank/mobile/business/authentication/session/refresh/e;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3558
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$v$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$v;Lcom/swedbank/mobile/business/authentication/session/refresh/e;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$v$d;)Ljavax/inject/Provider;
    .locals 0

    .line 3558
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->g:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/authentication/session/refresh/e;)V
    .locals 2

    .line 3578
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$d$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->b:Ljavax/inject/Provider;

    .line 3583
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/navigation/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->c:Ljavax/inject/Provider;

    .line 3584
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->d:Ljavax/inject/Provider;

    .line 3585
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$v;->g(Lcom/swedbank/mobile/a/b/s$c$v;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->d:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/authentication/session/refresh/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/refresh/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->e:Ljavax/inject/Provider;

    .line 3586
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->e:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/b/e/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/e/a/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->f:Ljavax/inject/Provider;

    .line 3587
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->I(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/g/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/g/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->g:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3558
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d;->b()Lcom/swedbank/mobile/app/b/e/a/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/e/a/c;
    .locals 1

    .line 3592
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/a/c;

    return-object v0
.end method
