.class final Lcom/swedbank/mobile/a/b/s$c$b$f$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$f$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$f$b$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/c/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/business/util/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
            ">;)V"
        }
    .end annotation

    .line 11272
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11274
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a(Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11246
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/util/e;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
            ">;)V"
        }
    .end annotation

    .line 11280
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->b:Ljavax/inject/Provider;

    .line 11285
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/a/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->c:Ljavax/inject/Provider;

    .line 11286
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$b;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->d:Ljavax/inject/Provider;

    .line 11291
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/c/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->e:Ljavax/inject/Provider;

    .line 11292
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->f:Ljavax/inject/Provider;

    .line 11293
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->G(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->g(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->d(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->f:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->h(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/biometric/onboarding/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/onboarding/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->g:Ljavax/inject/Provider;

    .line 11294
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->g:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/c/c/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/c/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->h:Ljavax/inject/Provider;

    .line 11295
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->h:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->i:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 11296
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/c/c/k;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->i:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->j:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 11297
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    invoke-static {}, Lcom/swedbank/mobile/app/c/c/l;->b()Lcom/swedbank/mobile/app/c/c/l;

    move-result-object v0

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->k:Ljavax/inject/Provider;

    .line 11298
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->j:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->k:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/d/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/c/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->l:Ljavax/inject/Provider;

    .line 11299
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->g:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->g:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->l:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/c/c/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/c/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->m:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11246
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->b()Lcom/swedbank/mobile/app/c/c/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/c/c/h;
    .locals 1

    .line 11304
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/c/c/h;

    return-object v0
.end method
