.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/k;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/r;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/t;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/o;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/o;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/details/k;)V
    .locals 0

    .line 7742
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7744
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/details/k;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/details/k;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7710
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/details/k;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/details/k;)V
    .locals 12

    .line 7750
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->b:Ljavax/inject/Provider;

    .line 7751
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/swedbank/mobile/business/cards/details/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/s;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->c:Ljavax/inject/Provider;

    .line 7752
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->d:Ljavax/inject/Provider;

    .line 7753
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/details/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->e:Ljavax/inject/Provider;

    .line 7754
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/details/u;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/u;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->f:Ljavax/inject/Provider;

    .line 7755
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/details/p;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/p;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->g:Ljavax/inject/Provider;

    .line 7756
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->d:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->e:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->f:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->g:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b;->b(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v10

    invoke-static/range {v0 .. v10}, Lcom/swedbank/mobile/business/cards/details/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/j;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    .line 7757
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/e/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->i:Ljavax/inject/Provider;

    .line 7758
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/app/cards/a/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->j:Ljavax/inject/Provider;

    .line 7759
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->j:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->k:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 7760
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/cards/a/o;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->k:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->l:Ljavax/inject/Provider;

    .line 7761
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/swedbank/mobile/app/cards/a/r;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/a/r;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->m:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 7762
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->m:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->n:Ljavax/inject/Provider;

    .line 7763
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->l:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->n:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/e/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->o:Ljavax/inject/Provider;

    .line 7764
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->b(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->d(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->i:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->h:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->o:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v11

    invoke-static/range {v0 .. v11}, Lcom/swedbank/mobile/app/cards/a/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/a/l;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->p:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7710
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->b()Lcom/swedbank/mobile/app/cards/a/k;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/a/k;
    .locals 1

    .line 7769
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;->p:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/a/k;

    return-object v0
.end method
