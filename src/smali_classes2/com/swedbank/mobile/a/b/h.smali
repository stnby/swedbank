.class public final Lcom/swedbank/mobile/a/b/h;
.super Ljava/lang/Object;
.source "AppModule_ProvideConfigurationFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/c/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/h;->a:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/swedbank/mobile/a/b/h;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/h;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/swedbank/mobile/a/b/h;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/b/h;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/c/f;)Lcom/swedbank/mobile/business/c/b;
    .locals 0

    .line 36
    invoke-static {p0, p1}, Lcom/swedbank/mobile/a/b/a;->a(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/c/f;)Lcom/swedbank/mobile/business/c/b;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/c/b;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/c/b;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/h;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/c/d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/h;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/c/f;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/b/h;->a(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/c/f;)Lcom/swedbank/mobile/business/c/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/h;->a()Lcom/swedbank/mobile/business/c/b;

    move-result-object v0

    return-object v0
.end method
