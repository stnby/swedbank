.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Ljava/lang/Integer;

.field private c:Lcom/swedbank/mobile/app/plugins/list/c;

.field private d:Lcom/swedbank/mobile/business/transfer/detailed/c;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V
    .locals 0

    .line 7132
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7132
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/ac/a/a;
    .locals 8

    .line 7159
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->b:Ljava/lang/Integer;

    const-class v1, Ljava/lang/Integer;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 7160
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->c:Lcom/swedbank/mobile/app/plugins/list/c;

    const-class v1, Lcom/swedbank/mobile/app/plugins/list/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 7161
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->d:Lcom/swedbank/mobile/business/transfer/detailed/c;

    const-class v1, Lcom/swedbank/mobile/business/transfer/detailed/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 7162
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->b:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->c:Lcom/swedbank/mobile/app/plugins/list/c;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->d:Lcom/swedbank/mobile/business/transfer/detailed/c;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Ljava/lang/Integer;Lcom/swedbank/mobile/app/plugins/list/c;Lcom/swedbank/mobile/business/transfer/detailed/c;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public a(I)Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;
    .locals 0

    .line 7141
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->b:Ljava/lang/Integer;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;
    .locals 0

    .line 7147
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->c:Lcom/swedbank/mobile/app/plugins/list/c;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;
    .locals 0

    .line 7153
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/detailed/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->d:Lcom/swedbank/mobile/business/transfer/detailed/c;

    return-object p0
.end method

.method public synthetic b(I)Lcom/swedbank/mobile/a/ac/a/a$a;
    .locals 0

    .line 7132
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->a(I)Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/a/ac/a/a$a;
    .locals 0

    .line 7132
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/a/ac/a/a$a;
    .locals 0

    .line 7132
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;->a(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$g;

    move-result-object p1

    return-object p1
.end method
