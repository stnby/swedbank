.class final Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/s/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/d/h;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/onboarding/d/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ")V"
        }
    .end annotation

    .line 11159
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11161
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->a(Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11135
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$f;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V

    return-void
.end method

.method private a(Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ")V"
        }
    .end annotation

    .line 11167
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->b:Ljavax/inject/Provider;

    .line 11168
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->b:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/s/d/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->c:Ljavax/inject/Provider;

    .line 11169
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->d:Ljavax/inject/Provider;

    .line 11170
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->c:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->d:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/onboarding/tour/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/onboarding/tour/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->e:Ljavax/inject/Provider;

    .line 11171
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->e:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/onboarding/d/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/d/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->f:Ljavax/inject/Provider;

    .line 11172
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->f:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->g:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 11173
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/onboarding/d/h;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->g:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->h:Ljavax/inject/Provider;

    .line 11174
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->b:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/swedbank/mobile/app/onboarding/d/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/d/i;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->i:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 11175
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->i:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->j:Ljavax/inject/Provider;

    .line 11176
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->h:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->j:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/s/d/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/d/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->k:Ljavax/inject/Provider;

    .line 11177
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->e:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->k:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$f$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/onboarding/d/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/d/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->l:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11135
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->b()Lcom/swedbank/mobile/app/onboarding/d/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/onboarding/d/e;
    .locals 1

    .line 11182
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$f$b;->l:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/onboarding/d/e;

    return-object v0
.end method
