.class final Lcom/swedbank/mobile/a/b/s$c$n$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/p/c/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$n$d$j;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$i;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$l;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$k;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$b;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$a;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$d;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$c;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$h;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$g;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$e;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$n;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$m;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/k;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/h;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lio/reactivex/b/c;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/n/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/l/a;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/n/d;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/b/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private T:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private U:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/g/d;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/d/d$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/h/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/contact/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/c;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n;Lcom/swedbank/mobile/a/p/c/a/b;Lcom/swedbank/mobile/a/u/i/a;Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/a/aa/d/a;Lcom/swedbank/mobile/a/h/b/g;Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/b;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/p/c/a/b;",
            "Lcom/swedbank/mobile/a/u/i/a;",
            "Lcom/swedbank/mobile/a/ac/f/a;",
            "Lcom/swedbank/mobile/a/e/f/a;",
            "Lcom/swedbank/mobile/a/aa/d/a;",
            "Lcom/swedbank/mobile/a/h/b/g;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/e/a/b<",
            "Lio/reactivex/b/c;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    move-object v9, p0

    move-object v0, p1

    .line 13011
    iput-object v0, v9, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    .line 13013
    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/a/b/s$c$n$d;->a(Lcom/swedbank/mobile/a/p/c/a/b;Lcom/swedbank/mobile/a/u/i/a;Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/a/aa/d/a;Lcom/swedbank/mobile/a/h/b/g;Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/b;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n;Lcom/swedbank/mobile/a/p/c/a/b;Lcom/swedbank/mobile/a/u/i/a;Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/a/aa/d/a;Lcom/swedbank/mobile/a/h/b/g;Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12910
    invoke-direct/range {p0 .. p9}, Lcom/swedbank/mobile/a/b/s$c$n$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$n;Lcom/swedbank/mobile/a/p/c/a/b;Lcom/swedbank/mobile/a/u/i/a;Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/a/aa/d/a;Lcom/swedbank/mobile/a/h/b/g;Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/b;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;
    .locals 0

    .line 12910
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->I:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/a/p/c/a/b;Lcom/swedbank/mobile/a/u/i/a;Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/a/aa/d/a;Lcom/swedbank/mobile/a/h/b/g;Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/p/c/a/b;",
            "Lcom/swedbank/mobile/a/u/i/a;",
            "Lcom/swedbank/mobile/a/ac/f/a;",
            "Lcom/swedbank/mobile/a/e/f/a;",
            "Lcom/swedbank/mobile/a/aa/d/a;",
            "Lcom/swedbank/mobile/a/h/b/g;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/e/a/b<",
            "Lio/reactivex/b/c;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .line 13026
    invoke-static {p1}, Lcom/swedbank/mobile/a/p/c/a/c;->a(Lcom/swedbank/mobile/a/p/c/a/b;)Lcom/swedbank/mobile/a/p/c/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->b:Ljavax/inject/Provider;

    .line 13027
    invoke-static {p2}, Lcom/swedbank/mobile/a/u/i/b;->a(Lcom/swedbank/mobile/a/u/i/a;)Lcom/swedbank/mobile/a/u/i/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->c:Ljavax/inject/Provider;

    .line 13028
    invoke-static {p3}, Lcom/swedbank/mobile/a/ac/f/b;->a(Lcom/swedbank/mobile/a/ac/f/a;)Lcom/swedbank/mobile/a/ac/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->d:Ljavax/inject/Provider;

    .line 13029
    invoke-static {p4}, Lcom/swedbank/mobile/a/e/f/b;->a(Lcom/swedbank/mobile/a/e/f/a;)Lcom/swedbank/mobile/a/e/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->e:Ljavax/inject/Provider;

    .line 13030
    invoke-static {p5}, Lcom/swedbank/mobile/a/aa/d/b;->a(Lcom/swedbank/mobile/a/aa/d/a;)Lcom/swedbank/mobile/a/aa/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->f:Ljavax/inject/Provider;

    .line 13031
    invoke-static {p6}, Lcom/swedbank/mobile/a/h/b/h;->a(Lcom/swedbank/mobile/a/h/b/g;)Lcom/swedbank/mobile/a/h/b/h;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->g:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x5

    .line 13032
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->c:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->d:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->f:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->g:Ljavax/inject/Provider;

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->h:Ljavax/inject/Provider;

    .line 13033
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->h:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/swedbank/mobile/a/p/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/e;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->i:Ljavax/inject/Provider;

    .line 13034
    new-instance v2, Lcom/swedbank/mobile/a/b/s$c$n$d$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->j:Ljavax/inject/Provider;

    .line 13039
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->j:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/b/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/b;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->k:Ljavax/inject/Provider;

    .line 13040
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->k:Ljavax/inject/Provider;

    invoke-static {p2, v2}, Lcom/swedbank/mobile/a/u/i/c;->a(Lcom/swedbank/mobile/a/u/i/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/i/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->l:Ljavax/inject/Provider;

    .line 13041
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->k:Ljavax/inject/Provider;

    invoke-static {p3, p2}, Lcom/swedbank/mobile/a/ac/f/c;->a(Lcom/swedbank/mobile/a/ac/f/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->m:Ljavax/inject/Provider;

    .line 13042
    new-instance p2, Lcom/swedbank/mobile/a/b/s$c$n$d$2;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->n:Ljavax/inject/Provider;

    .line 13047
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->n:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/cards/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->o:Ljavax/inject/Provider;

    .line 13048
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->k:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->o:Ljavax/inject/Provider;

    invoke-static {p4, p2, p3}, Lcom/swedbank/mobile/a/e/f/c;->a(Lcom/swedbank/mobile/a/e/f/a;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->p:Ljavax/inject/Provider;

    .line 13049
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->k:Ljavax/inject/Provider;

    invoke-static {p5, p2}, Lcom/swedbank/mobile/a/aa/d/c;->a(Lcom/swedbank/mobile/a/aa/d/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/d/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->q:Ljavax/inject/Provider;

    .line 13050
    new-instance p2, Lcom/swedbank/mobile/a/b/s$c$n$d$3;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->r:Ljavax/inject/Provider;

    .line 13055
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->r:Ljavax/inject/Provider;

    invoke-static {p2}, Lcom/swedbank/mobile/app/contact/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/contact/b/b;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->s:Ljavax/inject/Provider;

    .line 13056
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->s:Ljavax/inject/Provider;

    invoke-static {p6, p2}, Lcom/swedbank/mobile/a/h/b/i;->a(Lcom/swedbank/mobile/a/h/b/g;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/h/b/i;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->t:Ljavax/inject/Provider;

    .line 13057
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_overview"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->l:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_transfer"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->m:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_cards"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->p:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_services"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->q:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    const-string p3, "feature_contact"

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->t:Ljavax/inject/Provider;

    invoke-virtual {p2, p3, p4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->u:Ljavax/inject/Provider;

    .line 13058
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->i:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->u:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/a/p/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/f;

    move-result-object p2

    invoke-static {p2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->v:Ljavax/inject/Provider;

    .line 13059
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s$c;->e(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->v:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/business/navigation/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/navigation/h;

    move-result-object p2

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->w:Ljavax/inject/Provider;

    .line 13060
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->i:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->w:Ljavax/inject/Provider;

    invoke-static {p2, p3}, Lcom/swedbank/mobile/app/navigation/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/g;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->x:Ljavax/inject/Provider;

    .line 13061
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->x:Ljavax/inject/Provider;

    invoke-static {p2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->y:Ljavax/inject/Provider;

    const/4 p2, 0x1

    .line 13062
    invoke-static {p2}, La/a/f;->a(I)La/a/f$a;

    move-result-object p3

    const-class p4, Lcom/swedbank/mobile/app/navigation/k;

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->y:Ljavax/inject/Provider;

    invoke-virtual {p3, p4, p5}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p3

    invoke-virtual {p3}, La/a/f$a;->a()La/a/f;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->z:Ljavax/inject/Provider;

    .line 13063
    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p3, p3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p3}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p3

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/swedbank/mobile/app/navigation/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/l;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->A:Ljavax/inject/Provider;

    .line 13064
    invoke-static {p2, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p2

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->A:Ljavax/inject/Provider;

    invoke-virtual {p2, p3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/j$a;->a()La/a/j;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->B:Ljavax/inject/Provider;

    .line 13065
    invoke-static {p1}, Lcom/swedbank/mobile/a/p/c/a/d;->a(Lcom/swedbank/mobile/a/p/c/a/b;)Lcom/swedbank/mobile/a/p/c/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->C:Ljavax/inject/Provider;

    .line 13066
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->z:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->B:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->C:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/swedbank/mobile/a/p/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->D:Ljavax/inject/Provider;

    .line 13067
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->j(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->b:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->w:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->D:Ljavax/inject/Provider;

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p5, p5, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p5, p5, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p5}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p5

    invoke-static {p1, p2, p3, p4, p5}, Lcom/swedbank/mobile/app/navigation/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/navigation/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->E:Ljavax/inject/Provider;

    .line 13068
    invoke-static {p8}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->F:Ljavax/inject/Provider;

    .line 13069
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$d$4;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->G:Ljavax/inject/Provider;

    .line 13074
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->G:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/l/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/l/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->H:Ljavax/inject/Provider;

    .line 13075
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->H:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/n/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/n/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->I:Ljavax/inject/Provider;

    .line 13076
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$d$5;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->J:Ljavax/inject/Provider;

    .line 13081
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->J:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/c/b/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/b/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->K:Ljavax/inject/Provider;

    .line 13082
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->K:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/d/b/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/b/a/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->L:Ljavax/inject/Provider;

    .line 13083
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$d$6;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->M:Ljavax/inject/Provider;

    .line 13088
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->M:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/d/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/b/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->N:Ljavax/inject/Provider;

    .line 13089
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->N:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/c/d/b/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/b/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->O:Ljavax/inject/Provider;

    .line 13090
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$d$7;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$7;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->P:Ljavax/inject/Provider;

    .line 13095
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->P:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/d/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->Q:Ljavax/inject/Provider;

    .line 13096
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->Q:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/c/d/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/a/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->R:Ljavax/inject/Provider;

    .line 13097
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->L:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->O:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->R:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s$c$n;->a(Lcom/swedbank/mobile/a/b/s$c$n;)Ljavax/inject/Provider;

    move-result-object p4

    invoke-static {p1, p2, p3, p4}, Lcom/swedbank/mobile/a/c/d/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->S:Ljavax/inject/Provider;

    .line 13098
    invoke-static {p7}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->T:Ljavax/inject/Provider;

    .line 13099
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->I(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/g/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/g/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->U:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;
    .locals 0

    .line 12910
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->S:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;
    .locals 0

    .line 12910
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->T:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;
    .locals 0

    .line 12910
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->F:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;
    .locals 0

    .line 12910
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->U:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12910
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d;->b()Lcom/swedbank/mobile/app/navigation/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/navigation/h;
    .locals 1

    .line 13104
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d;->E:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/navigation/h;

    return-object v0
.end method
