.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/a/f/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private e:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;)V
    .locals 0

    .line 12307
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12307
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;
    .locals 0

    .line 12331
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;"
        }
    .end annotation

    .line 12324
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->c:Ljava/util/List;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;
    .locals 0

    .line 12318
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->b:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/e/g/a/f/a;
    .locals 9

    .line 12343
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->b:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 12344
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->c:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 12345
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 12346
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->e:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 12347
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->b:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->c:Ljava/util/List;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->e:Ljava/lang/Boolean;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Z)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;
    .locals 0

    .line 12337
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->e:Ljava/lang/Boolean;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 12307
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 12307
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Z)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 12307
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->b(Z)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Z)Lcom/swedbank/mobile/a/e/g/a/f/a$a;
    .locals 0

    .line 12307
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;->a(Z)Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;

    move-result-object p1

    return-object p1
.end method
