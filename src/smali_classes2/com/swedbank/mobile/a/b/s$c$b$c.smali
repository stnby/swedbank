.class final Lcom/swedbank/mobile/a/b/s$c$b$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/i/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b;

.field private b:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;)V
    .locals 0

    .line 4169
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 4169
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$c;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;)V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/b/s$c$b$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$c;"
        }
    .end annotation

    .line 4177
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/e/a/b;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->b:Lkotlin/e/a/b;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/i/a;
    .locals 5

    .line 4190
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->b:Lkotlin/e/a/b;

    const-class v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 4191
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->c:Lkotlin/e/a/b;

    const-class v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 4192
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->b:Lkotlin/e/a/b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->c:Lkotlin/e/a/b;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/a/b/s$c$b$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b;Lkotlin/e/a/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/b/s$c$b$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$c;"
        }
    .end annotation

    .line 4184
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/e/a/b;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$c;->c:Lkotlin/e/a/b;

    return-object p0
.end method

.method public synthetic c(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/i/a$a;
    .locals 0

    .line 4169
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$c;->b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/b/s$c$b$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/i/a$a;
    .locals 0

    .line 4169
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$c;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/b/s$c$b$c;

    move-result-object p1

    return-object p1
.end method
