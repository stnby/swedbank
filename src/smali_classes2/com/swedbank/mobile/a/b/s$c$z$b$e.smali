.class final Lcom/swedbank/mobile/a/b/s$c$z$b$e;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/b/d/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$z$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$z$b;

.field private b:Ljava/lang/String;

.field private c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

.field private d:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

.field private e:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;)V
    .locals 0

    .line 14396
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14396
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$b$e;-><init>(Lcom/swedbank/mobile/a/b/s$c$z$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;
    .locals 0

    .line 14413
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;
    .locals 0

    .line 14419
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    return-object p0
.end method

.method public a(Lio/reactivex/o;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$z$b$e;"
        }
    .end annotation

    .line 14425
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/o;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->e:Lio/reactivex/o;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;
    .locals 0

    .line 14407
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/e/g/b/d/a;
    .locals 9

    .line 14431
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->b:Ljava/lang/String;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14432
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14433
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14434
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->e:Lio/reactivex/o;

    const-class v1, Lio/reactivex/o;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 14435
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$z$b$f;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->e:Lio/reactivex/o;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$z$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .locals 0

    .line 14396
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .locals 0

    .line 14396
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lio/reactivex/o;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .locals 0

    .line 14396
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->a(Lio/reactivex/o;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .locals 0

    .line 14396
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$z$b$e;->a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$z$b$e;

    move-result-object p1

    return-object p1
.end method
