.class final Lcom/swedbank/mobile/a/b/s$c$v$b$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$b;

.field private b:Lcom/swedbank/mobile/business/authentication/e;

.field private c:Lcom/swedbank/mobile/business/authentication/o;

.field private d:Lcom/swedbank/mobile/business/authentication/j;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V
    .locals 0

    .line 3220
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3220
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/a/b/s$c$v$b$a;
    .locals 0

    .line 3229
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/a/b/s$c$v$b$a;
    .locals 0

    .line 3242
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/j;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->d:Lcom/swedbank/mobile/business/authentication/j;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/a/b/s$c$v$b$a;
    .locals 0

    .line 3236
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/o;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->c:Lcom/swedbank/mobile/business/authentication/o;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/c/a;
    .locals 8

    .line 3248
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    const-class v1, Lcom/swedbank/mobile/business/authentication/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3249
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->c:Lcom/swedbank/mobile/business/authentication/o;

    const-class v1, Lcom/swedbank/mobile/business/authentication/o;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3250
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->d:Lcom/swedbank/mobile/business/authentication/j;

    const-class v1, Lcom/swedbank/mobile/business/authentication/j;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 3251
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v$b$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->a:Lcom/swedbank/mobile/a/b/s$c$v$b;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->c:Lcom/swedbank/mobile/business/authentication/o;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->d:Lcom/swedbank/mobile/business/authentication/j;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$v$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$b;Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/a/c/a$a;
    .locals 0

    .line 3220
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->a(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/a/b/s$c$v$b$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/a/c/a$a;
    .locals 0

    .line 3220
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->a(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/a/b/s$c$v$b$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/a/c/a$a;
    .locals 0

    .line 3220
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$b$a;->a(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/a/b/s$c$v$b$a;

    move-result-object p1

    return-object p1
.end method
