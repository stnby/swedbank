.class final Lcom/swedbank/mobile/a/b/s$c$n$d$n;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/d/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "n"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/g;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/recurring/RecurringLoginInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/app/b/d/g;)V
    .locals 0

    .line 13147
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13149
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a(Lcom/swedbank/mobile/app/b/d/g;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/app/b/d/g;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 13122
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$n$d$n;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/app/b/d/g;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/app/b/d/g;)V
    .locals 4

    .line 13154
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->b:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 13155
    invoke-static {p1, p1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$n$d;->a(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/a/c/d/f;->b()Lcom/swedbank/mobile/a/c/d/f;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->c:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 13156
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$n$d;->b(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/a/c/d/h;->b()Lcom/swedbank/mobile/a/c/d/h;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->d:Ljavax/inject/Provider;

    .line 13157
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->d:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/a/c/d/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/g;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->e:Ljavax/inject/Provider;

    .line 13158
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->e:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/business/authentication/recurring/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/recurring/b;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->f:Ljavax/inject/Provider;

    .line 13159
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->f:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/d;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->g:Ljavax/inject/Provider;

    .line 13160
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->g:Ljavax/inject/Provider;

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->h:Ljavax/inject/Provider;

    .line 13161
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/b/d/i;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->h:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->i:Ljavax/inject/Provider;

    .line 13162
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->E(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->b:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$n$d;->c(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/swedbank/mobile/app/b/d/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->j:Ljavax/inject/Provider;

    .line 13163
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->j:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->k:Ljavax/inject/Provider;

    .line 13164
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->i:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->k:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/c/d/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d/i;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->l:Ljavax/inject/Provider;

    .line 13165
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$n$d;->d(Lcom/swedbank/mobile/a/b/s$c$n$d;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->f:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->l:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {p1, v0, v1, v2, v3}, Lcom/swedbank/mobile/app/b/d/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/d/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->m:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 13122
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->b()Lcom/swedbank/mobile/app/b/d/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/d/e;
    .locals 1

    .line 13170
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$n;->m:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/d/e;

    return-object v0
.end method
