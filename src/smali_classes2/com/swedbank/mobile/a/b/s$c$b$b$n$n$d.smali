.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/h/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V
    .locals 0

    .line 7087
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7089
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7082
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V

    return-void
.end method

.method private c()V
    .locals 2

    .line 7094
    invoke-static {}, Lcom/swedbank/mobile/business/transfer/search/prediction/e;->b()Lcom/swedbank/mobile/business/transfer/search/prediction/e;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->b:Ljavax/inject/Provider;

    .line 7095
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/search/b/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/b/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->c:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7082
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->b()Lcom/swedbank/mobile/app/transfer/search/b/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/search/b/c;
    .locals 1

    .line 7100
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$d;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/search/b/c;

    return-object v0
.end method
