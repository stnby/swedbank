.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/g/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/g/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/u/g/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/d/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/a/k;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V
    .locals 0

    .line 5518
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5520
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5473
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    return-void
.end method

.method private c()V
    .locals 5

    .line 5525
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->b:Ljavax/inject/Provider;

    .line 5530
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/d/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->c:Ljavax/inject/Provider;

    .line 5531
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->c:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/u/g/a/d;->b()Lcom/swedbank/mobile/a/u/g/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/u/g/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->d:Ljavax/inject/Provider;

    .line 5532
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->e:Ljavax/inject/Provider;

    .line 5537
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->e:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/d/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->f:Ljavax/inject/Provider;

    .line 5538
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->f:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/u/g/b/d;->b()Lcom/swedbank/mobile/a/u/g/b/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/u/g/b/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->g:Ljavax/inject/Provider;

    .line 5539
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->h:Ljavax/inject/Provider;

    .line 5544
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->h:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/d/d/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->i:Ljavax/inject/Provider;

    .line 5545
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->i:Ljavax/inject/Provider;

    invoke-static {}, Lcom/swedbank/mobile/a/u/g/d/d;->b()Lcom/swedbank/mobile/a/u/g/d/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/u/g/d/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->j:Ljavax/inject/Provider;

    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 5546
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->d:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->g:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->j:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->k:Ljavax/inject/Provider;

    .line 5547
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->k:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/a/u/g/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/c;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->l:Ljavax/inject/Provider;

    .line 5548
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->l:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/u/g/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/e;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->m:Ljavax/inject/Provider;

    .line 5549
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->m:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/business/overview/combined/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/combined/b;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->n:Ljavax/inject/Provider;

    .line 5550
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->l:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/a/u/g/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/b;

    move-result-object v1

    invoke-static {v1}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->o:Ljavax/inject/Provider;

    .line 5551
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->o:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->n:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/app/overview/a/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/a/g;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->p:Ljavax/inject/Provider;

    .line 5552
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->p:Ljavax/inject/Provider;

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->q:Ljavax/inject/Provider;

    const/4 v1, 0x1

    .line 5553
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/overview/a/k;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->q:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->r:Ljavax/inject/Provider;

    .line 5554
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->l:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/u/g/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/g/d;

    move-result-object v2

    invoke-static {v2}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->s:Ljavax/inject/Provider;

    .line 5555
    invoke-static {}, Lcom/swedbank/mobile/a/b/l;->b()Lcom/swedbank/mobile/a/b/l;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->d(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->s:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4}, Lcom/swedbank/mobile/app/overview/a/m;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/a/m;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->t:Ljavax/inject/Provider;

    .line 5556
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->t:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->u:Ljavax/inject/Provider;

    .line 5557
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->r:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->u:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/u/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->v:Ljavax/inject/Provider;

    .line 5558
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->n:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->v:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/overview/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/a/i;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->w:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5473
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->b()Lcom/swedbank/mobile/app/overview/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/a/h;
    .locals 1

    .line 5563
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->w:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/a/h;

    return-object v0
.end method
