.class final Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;

.field private b:Lcom/swedbank/mobile/business/biometric/authentication/h;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/app/c/a/d;

.field private e:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;)V
    .locals 0

    .line 10029
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 10029
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;
    .locals 0

    .line 10055
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/c/a/d;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->d:Lcom/swedbank/mobile/app/c/a/d;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;
    .locals 0

    .line 10041
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/h;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->b:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;"
        }
    .end annotation

    .line 10048
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->c:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/d/a/a;
    .locals 9

    .line 10068
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->b:Lcom/swedbank/mobile/business/biometric/authentication/h;

    const-class v1, Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 10069
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->c:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 10070
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->d:Lcom/swedbank/mobile/app/c/a/d;

    const-class v1, Lcom/swedbank/mobile/app/c/a/d;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 10071
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->e:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 10072
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->b:Lcom/swedbank/mobile/business/biometric/authentication/h;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->c:Lcom/swedbank/mobile/business/util/l;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->d:Lcom/swedbank/mobile/app/c/a/d;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->e:Lcom/swedbank/mobile/business/util/l;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/c/a/d;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;"
        }
    .end annotation

    .line 10062
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->e:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 10029
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 10029
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 10029
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;
    .locals 0

    .line 10029
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$b$b$a;

    move-result-object p1

    return-object p1
.end method
