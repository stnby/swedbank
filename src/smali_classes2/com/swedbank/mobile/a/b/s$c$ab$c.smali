.class final Lcom/swedbank/mobile/a/b/s$c$ab$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ae/d/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$ab;

.field private b:Lcom/swedbank/mobile/a/c/e/b;

.field private c:Lcom/swedbank/mobile/business/widget/update/e;

.field private d:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$ab;)V
    .locals 0

    .line 15047
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 15047
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$ab$c;-><init>(Lcom/swedbank/mobile/a/b/s$c$ab;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/ae/d/a;
    .locals 8

    .line 15074
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->b:Lcom/swedbank/mobile/a/c/e/b;

    const-class v1, Lcom/swedbank/mobile/a/c/e/b;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 15075
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->c:Lcom/swedbank/mobile/business/widget/update/e;

    const-class v1, Lcom/swedbank/mobile/business/widget/update/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 15076
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->d:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 15077
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$ab$d;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->b:Lcom/swedbank/mobile/a/c/e/b;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->c:Lcom/swedbank/mobile/business/widget/update/e;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->d:Ljava/lang/Boolean;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$ab$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lcom/swedbank/mobile/a/c/e/b;Lcom/swedbank/mobile/business/widget/update/e;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/a/b/s$c$ab$c;
    .locals 0

    .line 15056
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/a/c/e/b;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->b:Lcom/swedbank/mobile/a/c/e/b;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/widget/update/e;)Lcom/swedbank/mobile/a/b/s$c$ab$c;
    .locals 0

    .line 15062
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/widget/update/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->c:Lcom/swedbank/mobile/business/widget/update/e;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$ab$c;
    .locals 0

    .line 15068
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$c;->d:Ljava/lang/Boolean;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/a/ae/d/a$a;
    .locals 0

    .line 15047
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$ab$c;->a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/a/b/s$c$ab$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/widget/update/e;)Lcom/swedbank/mobile/a/ae/d/a$a;
    .locals 0

    .line 15047
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$ab$c;->a(Lcom/swedbank/mobile/business/widget/update/e;)Lcom/swedbank/mobile/a/b/s$c$ab$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Z)Lcom/swedbank/mobile/a/ae/d/a$a;
    .locals 0

    .line 15047
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$ab$c;->a(Z)Lcom/swedbank/mobile/a/b/s$c$ab$c;

    move-result-object p1

    return-object p1
.end method
