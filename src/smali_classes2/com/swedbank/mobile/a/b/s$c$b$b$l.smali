.class final Lcom/swedbank/mobile/a/b/s$c$b$b$l;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/aa/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "l"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$a;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$l$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/c/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/c/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/ServicesInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/e;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/m;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V
    .locals 0

    .line 9174
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9176
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 9123
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$l;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b;)V

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 9181
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->c(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->b(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/aa/d;->b()Lcom/swedbank/mobile/a/aa/d;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->b:Ljavax/inject/Provider;

    .line 9182
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->b:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/aa/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->c:Ljavax/inject/Provider;

    .line 9183
    new-instance v2, Lcom/swedbank/mobile/a/b/s$c$b$b$l$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l;)V

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->d:Ljavax/inject/Provider;

    .line 9188
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->d:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/services/c/b/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/b/h;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->e:Ljavax/inject/Provider;

    .line 9189
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->B(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v2}, Lcom/swedbank/mobile/a/aa/c/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/b/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->f:Ljavax/inject/Provider;

    .line 9190
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->f:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/services/c/b/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/b/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->g:Ljavax/inject/Provider;

    .line 9191
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->f:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/services/c/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/b/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->h:Ljavax/inject/Provider;

    .line 9192
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->h:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/swedbank/mobile/a/aa/c/b/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/b/e;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->i:Ljavax/inject/Provider;

    .line 9193
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->e:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->i:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/swedbank/mobile/a/aa/c/b/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/b/d;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->j:Ljavax/inject/Provider;

    .line 9194
    new-instance v2, Lcom/swedbank/mobile/a/b/s$c$b$b$l$2;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$l;)V

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->k:Ljavax/inject/Provider;

    .line 9199
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->k:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/services/c/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c/a/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->l:Ljavax/inject/Provider;

    .line 9200
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->l:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/aa/c/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/a/e;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->m:Ljavax/inject/Provider;

    const/4 v2, 0x0

    .line 9201
    invoke-static {v0, v2}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->j:Ljavax/inject/Provider;

    invoke-virtual {v0, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->m:Ljavax/inject/Provider;

    invoke-virtual {v0, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->n:Ljavax/inject/Provider;

    .line 9202
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->n:Ljavax/inject/Provider;

    invoke-static {v0, v3}, Lcom/swedbank/mobile/a/aa/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->o:Ljavax/inject/Provider;

    .line 9203
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/aa/c/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/e;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->p:Ljavax/inject/Provider;

    .line 9204
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->c:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->p:Ljavax/inject/Provider;

    invoke-static {v0, v3}, Lcom/swedbank/mobile/business/services/h;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/services/h;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->q:Ljavax/inject/Provider;

    .line 9205
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/aa/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/b;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->r:Ljavax/inject/Provider;

    .line 9206
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->r:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->q:Ljavax/inject/Provider;

    invoke-static {v0, v3}, Lcom/swedbank/mobile/app/services/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/i;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->s:Ljavax/inject/Provider;

    .line 9207
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->s:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->t:Ljavax/inject/Provider;

    .line 9208
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v3, Lcom/swedbank/mobile/app/services/m;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->t:Ljavax/inject/Provider;

    invoke-virtual {v0, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->u:Ljavax/inject/Provider;

    .line 9209
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->o:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/aa/c/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/c/d;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->v:Ljavax/inject/Provider;

    .line 9210
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b;->d(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->v:Ljavax/inject/Provider;

    invoke-static {v0, v3}, Lcom/swedbank/mobile/app/services/o;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/o;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->w:Ljavax/inject/Provider;

    .line 9211
    invoke-static {v1, v2}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->w:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->x:Ljavax/inject/Provider;

    .line 9212
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->u:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->x:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/aa/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->y:Ljavax/inject/Provider;

    .line 9213
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->q:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->y:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/services/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/k;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->z:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 9123
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->b()Lcom/swedbank/mobile/app/services/j;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/services/j;
    .locals 1

    .line 9218
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$l;->z:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/services/j;

    return-object v0
.end method
