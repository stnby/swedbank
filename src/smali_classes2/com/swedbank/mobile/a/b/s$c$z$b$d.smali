.class final Lcom/swedbank/mobile/a/b/s$c$z$b$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/b/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$z$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$z$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/r;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/c/j;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/c/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V
    .locals 0

    .line 14547
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14549
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14523
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$z$b$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$z$b;Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V
    .locals 3

    .line 14555
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/cards/details/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/s;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->b:Ljavax/inject/Provider;

    .line 14556
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->c:Ljavax/inject/Provider;

    .line 14557
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->d:Ljavax/inject/Provider;

    .line 14558
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->y(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->d:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/result/c;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->e:Ljavax/inject/Provider;

    .line 14559
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->e:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/b/c/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/c/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->f:Ljavax/inject/Provider;

    .line 14560
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->f:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->g:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 14561
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object p2

    const-class v0, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->g:Ljavax/inject/Provider;

    invoke-virtual {p2, v0, v1}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object p2

    invoke-virtual {p2}, La/a/f$a;->a()La/a/f;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->h:Ljavax/inject/Provider;

    .line 14562
    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p2, p2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p2}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p2

    invoke-static {p2}, Lcom/swedbank/mobile/app/cards/f/b/c/l;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/c/l;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->i:Ljavax/inject/Provider;

    const/4 p2, 0x0

    .line 14563
    invoke-static {p1, p2}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->i:Ljavax/inject/Provider;

    invoke-virtual {p1, p2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->j:Ljavax/inject/Provider;

    .line 14564
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->h:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->j:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/a/e/g/b/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/g/b/c/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->k:Ljavax/inject/Provider;

    .line 14565
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->e:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->k:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$z$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$z$b;->a:Lcom/swedbank/mobile/a/b/s$c$z;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$z;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/cards/f/b/c/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/c/g;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->l:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14523
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->b()Lcom/swedbank/mobile/app/cards/f/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/b/c/f;
    .locals 1

    .line 14570
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$z$b$d;->l:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/c/f;

    return-object v0
.end method
