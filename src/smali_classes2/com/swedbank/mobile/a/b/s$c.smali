.class final Lcom/swedbank/mobile/a/b/s$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/z/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$r;,
        Lcom/swedbank/mobile/a/b/s$c$q;,
        Lcom/swedbank/mobile/a/b/s$c$f;,
        Lcom/swedbank/mobile/a/b/s$c$e;,
        Lcom/swedbank/mobile/a/b/s$c$d;,
        Lcom/swedbank/mobile/a/b/s$c$c;,
        Lcom/swedbank/mobile/a/b/s$c$t;,
        Lcom/swedbank/mobile/a/b/s$c$s;,
        Lcom/swedbank/mobile/a/b/s$c$ab;,
        Lcom/swedbank/mobile/a/b/s$c$aa;,
        Lcom/swedbank/mobile/a/b/s$c$x;,
        Lcom/swedbank/mobile/a/b/s$c$w;,
        Lcom/swedbank/mobile/a/b/s$c$j;,
        Lcom/swedbank/mobile/a/b/s$c$i;,
        Lcom/swedbank/mobile/a/b/s$c$l;,
        Lcom/swedbank/mobile/a/b/s$c$k;,
        Lcom/swedbank/mobile/a/b/s$c$h;,
        Lcom/swedbank/mobile/a/b/s$c$g;,
        Lcom/swedbank/mobile/a/b/s$c$p;,
        Lcom/swedbank/mobile/a/b/s$c$o;,
        Lcom/swedbank/mobile/a/b/s$c$z;,
        Lcom/swedbank/mobile/a/b/s$c$y;,
        Lcom/swedbank/mobile/a/b/s$c$n;,
        Lcom/swedbank/mobile/a/b/s$c$m;,
        Lcom/swedbank/mobile/a/b/s$c$b;,
        Lcom/swedbank/mobile/a/b/s$c$a;,
        Lcom/swedbank/mobile/a/b/s$c$v;,
        Lcom/swedbank/mobile/a/b/s$c$u;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ae/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/x/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/x/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/a/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/d/a/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/j/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/discovery/a;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/j/d;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/r/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/o/a;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/r/b;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private O:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/h;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/p;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/k;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/root/RootInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/t/b;",
            ">;"
        }
    .end annotation
.end field

.field private U:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/customer/i;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/ordering/a;",
            ">;"
        }
    .end annotation
.end field

.field private W:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/h/c;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/b/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/b/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/q/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/n/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/l/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/i/a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/l/b;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ad/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/v/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ad/d;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/m/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/k/a;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/k/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/h/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/k/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/ae/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/c/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s;)V
    .locals 0

    .line 2610
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2513
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c;-><init>(Lcom/swedbank/mobile/a/b/s;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->l:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->U:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->R:Ljavax/inject/Provider;

    return-object p0
.end method

.method private c()V
    .locals 15

    .line 2617
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$1;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->b:Ljavax/inject/Provider;

    .line 2622
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/e/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/e/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->c:Ljavax/inject/Provider;

    .line 2623
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$7;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$7;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->d:Ljavax/inject/Provider;

    .line 2628
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->e:Ljavax/inject/Provider;

    .line 2629
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$8;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$8;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->f:Ljavax/inject/Provider;

    .line 2634
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/c/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->g:Ljavax/inject/Provider;

    .line 2635
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$9;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$9;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->h:Ljavax/inject/Provider;

    .line 2640
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->h:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/b/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->i:Ljavax/inject/Provider;

    .line 2641
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$10;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$10;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->j:Ljavax/inject/Provider;

    .line 2646
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->j:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/n/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/n/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->k:Ljavax/inject/Provider;

    .line 2647
    invoke-static {}, Lcom/swedbank/mobile/data/authentication/b/i;->b()Lcom/swedbank/mobile/data/authentication/b/i;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->l:Ljavax/inject/Provider;

    .line 2648
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$11;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$11;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->m:Ljavax/inject/Provider;

    .line 2653
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->m:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/i/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/i/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->n:Ljavax/inject/Provider;

    .line 2654
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->n:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/l/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/l/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->o:Ljavax/inject/Provider;

    .line 2655
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$12;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$12;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->p:Ljavax/inject/Provider;

    .line 2660
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->p:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/v/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/v/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->q:Ljavax/inject/Provider;

    .line 2661
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->q:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ad/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ad/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->r:Ljavax/inject/Provider;

    .line 2662
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$13;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$13;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->s:Ljavax/inject/Provider;

    .line 2667
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->s:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/k/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/k/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->t:Ljavax/inject/Provider;

    .line 2668
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->t:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/m/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/m/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->u:Ljavax/inject/Provider;

    .line 2669
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$14;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$14;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->v:Ljavax/inject/Provider;

    .line 2674
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->v:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/h/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/h/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->w:Ljavax/inject/Provider;

    .line 2675
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->w:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/k/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->x:Ljavax/inject/Provider;

    .line 2676
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$2;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->y:Ljavax/inject/Provider;

    .line 2681
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->y:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/widget/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->z:Ljavax/inject/Provider;

    .line 2682
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->z:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ae/c/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ae/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->A:Ljavax/inject/Provider;

    .line 2683
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$3;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->B:Ljavax/inject/Provider;

    .line 2688
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->B:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/r/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->C:Ljavax/inject/Provider;

    .line 2689
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->C:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/x/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/x/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->D:Ljavax/inject/Provider;

    .line 2690
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$4;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$4;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->E:Ljavax/inject/Provider;

    .line 2695
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->E:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->F:Ljavax/inject/Provider;

    .line 2696
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->F:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/d/a/a/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->G:Ljavax/inject/Provider;

    .line 2697
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$5;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$5;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->H:Ljavax/inject/Provider;

    .line 2702
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->H:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/discovery/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/discovery/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->I:Ljavax/inject/Provider;

    .line 2703
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->I:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/j/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/j/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->J:Ljavax/inject/Provider;

    .line 2704
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$6;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$6;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->K:Ljavax/inject/Provider;

    .line 2709
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->K:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/o/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/o/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->L:Ljavax/inject/Provider;

    .line 2710
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->L:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/r/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/r/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->M:Ljavax/inject/Provider;

    const/16 v0, 0x9

    const/4 v1, 0x1

    .line 2711
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/a/z/d;->b()Lcom/swedbank/mobile/a/z/d;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->o:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->r:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->u:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->x:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->A:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->D:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->G:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->J:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->M:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->N:Ljavax/inject/Provider;

    .line 2712
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->N:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/z/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/z/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->O:Ljavax/inject/Provider;

    .line 2713
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/k;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->P:Ljavax/inject/Provider;

    .line 2714
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->e(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c;->P:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/q;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/q;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->Q:Ljavax/inject/Provider;

    .line 2715
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/data/device/l;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/device/l;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->R:Ljavax/inject/Provider;

    .line 2716
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c;->l:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c;->O:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c;->Q:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c;->R:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/business/root/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/root/a;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->S:Ljavax/inject/Provider;

    .line 2717
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c;->c:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c;->e:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c;->g:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c;->i:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c;->k:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->i(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->j(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->k(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v11, p0, Lcom/swedbank/mobile/a/b/s$c;->S:Ljavax/inject/Provider;

    iget-object v12, p0, Lcom/swedbank/mobile/a/b/s$c;->S:Ljavax/inject/Provider;

    iget-object v13, p0, Lcom/swedbank/mobile/a/b/s$c;->S:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v14

    invoke-static/range {v1 .. v14}, Lcom/swedbank/mobile/app/t/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/t/i;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->T:Ljavax/inject/Provider;

    .line 2718
    invoke-static {}, Lcom/swedbank/mobile/data/customer/j;->b()Lcom/swedbank/mobile/data/customer/j;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->U:Ljavax/inject/Provider;

    .line 2719
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->m(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/ordering/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/ordering/c;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->V:Ljavax/inject/Provider;

    .line 2720
    invoke-static {}, Lcom/swedbank/mobile/data/h/d;->b()Lcom/swedbank/mobile/data/h/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->W:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->V:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->W:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic f(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->Q:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic g(Lcom/swedbank/mobile/a/b/s$c;)Ljavax/inject/Provider;
    .locals 0

    .line 2513
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c;->P:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 2513
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c;->b()Lcom/swedbank/mobile/app/t/b;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/t/b;
    .locals 1

    .line 2725
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c;->T:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/t/b;

    return-object v0
.end method
