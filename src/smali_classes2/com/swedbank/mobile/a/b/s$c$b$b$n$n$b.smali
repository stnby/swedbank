.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/h/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/search/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V
    .locals 0

    .line 7058
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7060
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7051
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;)V

    return-void
.end method

.method private c()V
    .locals 2

    .line 7065
    invoke-static {}, Lcom/swedbank/mobile/data/transfer/search/a/d;->b()Lcom/swedbank/mobile/data/transfer/search/a/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->b:Ljavax/inject/Provider;

    .line 7066
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/search/history/a;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/search/history/a;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->c:Ljavax/inject/Provider;

    .line 7067
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/search/a/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/search/a/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7051
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->b()Lcom/swedbank/mobile/app/transfer/search/a/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/transfer/search/a/c;
    .locals 1

    .line 7072
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$n$b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/search/a/c;

    return-object v0
.end method
