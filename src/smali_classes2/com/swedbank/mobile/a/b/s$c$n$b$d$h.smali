.class final Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$b;,
        Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/f/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/c/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)V"
        }
    .end annotation

    .line 12283
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12285
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 12268
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)V"
        }
    .end annotation

    .line 12291
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->b:Ljavax/inject/Provider;

    .line 12296
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/f/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/f/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->c:Ljavax/inject/Provider;

    .line 12297
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->d:Ljavax/inject/Provider;

    .line 12298
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->e:Ljavax/inject/Provider;

    .line 12299
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->d:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->e:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->b(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/a;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/preconditions/a;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->f:Ljavax/inject/Provider;

    .line 12300
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$n$b$d;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->f:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->f:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->f:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$n$b$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$n$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$b;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/cards/f/c/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/c/f;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->g:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 12268
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->b()Lcom/swedbank/mobile/app/cards/f/c/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/c/e;
    .locals 1

    .line 12305
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$b$d$h;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/c/e;

    return-object v0
.end method
