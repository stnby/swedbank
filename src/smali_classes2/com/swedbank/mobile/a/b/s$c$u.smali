.class final Lcom/swedbank/mobile/a/b/s$c$u;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/e/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "u"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c;

.field private b:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/business/authentication/session/l;

.field private d:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c;)V
    .locals 0

    .line 2727
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$u;->a:Lcom/swedbank/mobile/a/b/s$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 2727
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$u;-><init>(Lcom/swedbank/mobile/a/b/s$c;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/a/b/s$c$u;
    .locals 0

    .line 2743
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$u;->c:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$u;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$u;"
        }
    .end annotation

    .line 2750
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$u;->d:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/b/s$c$u;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$u;"
        }
    .end annotation

    .line 2737
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/e/a/b;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$u;->b:Lkotlin/e/a/b;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/c/e/a;
    .locals 8

    .line 2756
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$u;->b:Lkotlin/e/a/b;

    const-class v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 2757
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$u;->c:Lcom/swedbank/mobile/business/authentication/session/l;

    const-class v1, Lcom/swedbank/mobile/business/authentication/session/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 2758
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$u;->d:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 2759
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$u;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$u;->b:Lkotlin/e/a/b;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$u;->c:Lcom/swedbank/mobile/business/authentication/session/l;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$u;->d:Lcom/swedbank/mobile/business/util/l;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$v;-><init>(Lcom/swedbank/mobile/a/b/s$c;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/a/c/e/a$a;
    .locals 0

    .line 2727
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$u;->a(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/a/b/s$c$u;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/e/a$a;
    .locals 0

    .line 2727
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$u;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$u;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/c/e/a$a;
    .locals 0

    .line 2727
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$u;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/b/s$c$u;

    move-result-object p1

    return-object p1
.end method
