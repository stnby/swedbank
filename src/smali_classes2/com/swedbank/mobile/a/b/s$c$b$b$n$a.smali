.class final Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/ac/d/b/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

.field private b:Lcom/swedbank/mobile/business/transfer/payment/form/g;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V
    .locals 0

    .line 6272
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 6272
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/ac/d/b/a;
    .locals 8

    .line 6299
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->b:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    const-class v1, Lcom/swedbank/mobile/business/transfer/payment/form/g;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6300
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->c:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6301
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->d:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 6302
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$n;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->b:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->c:Lcom/swedbank/mobile/business/util/l;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->d:Ljava/lang/Boolean;

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$n;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;
    .locals 0

    .line 6281
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/form/g;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->b:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;"
        }
    .end annotation

    .line 6287
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->c:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;
    .locals 0

    .line 6293
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->d:Ljava/lang/Boolean;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/a/ac/d/b/a$a;
    .locals 0

    .line 6272
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->a(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/ac/d/b/a$a;
    .locals 0

    .line 6272
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Z)Lcom/swedbank/mobile/a/ac/d/b/a$a;
    .locals 0

    .line 6272
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;->a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$n$a;

    move-result-object p1

    return-object p1
.end method
