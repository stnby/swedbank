.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/v;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/z;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/i;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/list/e;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/list/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)V
    .locals 0

    .line 7640
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7642
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7599
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)Ljavax/inject/Provider;
    .locals 0

    .line 7599
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->l:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)Ljavax/inject/Provider;
    .locals 0

    .line 7599
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->e:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)Ljavax/inject/Provider;
    .locals 0

    .line 7599
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->g:Ljavax/inject/Provider;

    return-object p0
.end method

.method private c()V
    .locals 20

    move-object/from16 v0, p0

    .line 7647
    new-instance v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$1;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)V

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->b:Ljavax/inject/Provider;

    .line 7652
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->b:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/a/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->c:Ljavax/inject/Provider;

    .line 7653
    new-instance v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$2;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)V

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->d:Ljavax/inject/Provider;

    .line 7658
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->d:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->e:Ljavax/inject/Provider;

    .line 7659
    new-instance v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$3;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;)V

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->f:Ljavax/inject/Provider;

    .line 7664
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->f:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/d/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/d/c;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->g:Ljavax/inject/Provider;

    .line 7665
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->k(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/swedbank/mobile/app/cards/w;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/w;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->h:Ljavax/inject/Provider;

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 7666
    invoke-static {v1, v2}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    iget-object v3, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->c(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    iget-object v3, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b$b;->b(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/e/b/e;->b()Lcom/swedbank/mobile/a/e/b/e;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->i:Ljavax/inject/Provider;

    .line 7667
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->i:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/e/b/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/b/d;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->j:Ljavax/inject/Provider;

    .line 7668
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->T(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/swedbank/mobile/business/cards/ac;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/ac;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->k:Ljavax/inject/Provider;

    .line 7669
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/j;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/j;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->l:Ljavax/inject/Provider;

    .line 7670
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->j:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->T(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->b(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v8, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->k:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->l:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b;->b(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v13

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v14

    invoke-static/range {v3 .. v14}, Lcom/swedbank/mobile/business/cards/list/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/list/i;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    .line 7671
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/e/b/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/b/c;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->n:Ljavax/inject/Provider;

    .line 7672
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/e/b/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/b/g;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->o:Ljavax/inject/Provider;

    .line 7673
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/app/cards/list/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/list/g;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->p:Ljavax/inject/Provider;

    .line 7674
    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->p:Ljavax/inject/Provider;

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->q:Ljavax/inject/Provider;

    .line 7675
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/cards/list/m;

    iget-object v4, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->q:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->r:Ljavax/inject/Provider;

    const/4 v2, 0x0

    .line 7676
    invoke-static {v1, v2}, La/a/j;->a(II)La/a/j$a;

    move-result-object v1

    invoke-static {}, Lcom/swedbank/mobile/app/cards/list/q;->b()Lcom/swedbank/mobile/app/cards/list/q;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/j$a;->a()La/a/j;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->s:Ljavax/inject/Provider;

    .line 7677
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->r:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->s:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/a/e/b/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/b/f;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->t:Ljavax/inject/Provider;

    .line 7678
    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->g(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->f(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->c:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->d(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->e(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v8, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->e:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->g:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->h:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->n:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->o:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->m:Ljavax/inject/Provider;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->t:Ljavax/inject/Provider;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v19

    invoke-static/range {v2 .. v19}, Lcom/swedbank/mobile/app/cards/list/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/list/i;

    move-result-object v1

    invoke-static {v1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->u:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 7599
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->b()Lcom/swedbank/mobile/app/cards/list/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/list/h;
    .locals 1

    .line 7683
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$b;->u:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/list/h;

    return-object v0
.end method
