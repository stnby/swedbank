.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;
.super Lcom/swedbank/mobile/a/e/g/a/c/a;
.source "DaggerApplicationComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/m;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/h;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V
    .locals 0

    .line 8495
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-direct {p0}, Lcom/swedbank/mobile/a/e/g/a/c/a;-><init>()V

    .line 8497
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->f()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 8474
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    return-void
.end method

.method private f()V
    .locals 5

    .line 8502
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->b:Ljavax/inject/Provider;

    .line 8503
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b$b;->f(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->b:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/o;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/o;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->c:Ljavax/inject/Provider;

    .line 8504
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->d(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->T(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->c:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-static {v4}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->d:Ljavax/inject/Provider;

    .line 8505
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->c(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->d:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/cards/f/a/c/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->e:Ljavax/inject/Provider;

    .line 8506
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->e:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->f:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 8507
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/c/h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->f:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->g:Ljavax/inject/Provider;

    .line 8508
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/a/c/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/c/i;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->h:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 8509
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->h:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->i:Ljavax/inject/Provider;

    .line 8510
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->g:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->i:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/g/a/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->j:Ljavax/inject/Provider;

    .line 8511
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->j:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/a/c/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/c/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->k:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 8474
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->b()Lcom/swedbank/mobile/app/cards/f/a/c/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/c/e;
    .locals 1

    .line 8516
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->k:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/c/e;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;
    .locals 1

    .line 8520
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;

    return-object v0
.end method

.method public synthetic d()Lcom/swedbank/mobile/business/cards/wallet/onboarding/l;
    .locals 1

    .line 8474
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;->c()Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;

    move-result-object v0

    return-object v0
.end method
