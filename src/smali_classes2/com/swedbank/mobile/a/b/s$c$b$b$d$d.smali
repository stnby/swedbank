.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$d;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$c;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$l;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$k;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$f;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$e;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$h;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$g;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$n;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$m;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$j;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$i;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$p;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$o;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$r;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$q;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$b;,
        Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$a;
    }
.end annotation


# instance fields
.field private A:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private B:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/i/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/i/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/h/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/h/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/d/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/g/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/b/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/e/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/a/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 8103
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 8105
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a(Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 8045
    invoke-direct/range {p0 .. p6}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8045
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->x:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 8114
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->b:Ljavax/inject/Provider;

    .line 8119
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->c:Ljavax/inject/Provider;

    .line 8120
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->d:Ljavax/inject/Provider;

    .line 8125
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/i/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/i/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->e:Ljavax/inject/Provider;

    .line 8126
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->f:Ljavax/inject/Provider;

    .line 8131
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/h/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/h/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->g:Ljavax/inject/Provider;

    .line 8132
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$4;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->h:Ljavax/inject/Provider;

    .line 8137
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->h:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/d/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->i:Ljavax/inject/Provider;

    .line 8138
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$5;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$5;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->j:Ljavax/inject/Provider;

    .line 8143
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->j:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/g/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/g/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->k:Ljavax/inject/Provider;

    .line 8144
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$6;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$6;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->l:Ljavax/inject/Provider;

    .line 8149
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->l:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/c/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->m:Ljavax/inject/Provider;

    .line 8150
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$7;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$7;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->n:Ljavax/inject/Provider;

    .line 8155
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->n:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->o:Ljavax/inject/Provider;

    .line 8156
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$8;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$8;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->p:Ljavax/inject/Provider;

    .line 8161
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->p:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/e/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/e/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->q:Ljavax/inject/Provider;

    .line 8162
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$9;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d$9;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->r:Ljavax/inject/Provider;

    .line 8167
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->r:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/a/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->s:Ljavax/inject/Provider;

    .line 8168
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->i:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->k:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->m:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->o:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->q:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->s:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/cards/f/a/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->t:Ljavax/inject/Provider;

    .line 8169
    invoke-static {p4}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->u:Ljavax/inject/Provider;

    .line 8170
    invoke-static {p5}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->v:Ljavax/inject/Provider;

    .line 8171
    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p4, p4, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->t:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->u:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->v:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->f(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-static {p4}, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->g(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->w:Ljavax/inject/Provider;

    .line 8172
    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->w:Ljavax/inject/Provider;

    invoke-static {p4}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->x:Ljavax/inject/Provider;

    .line 8173
    iget-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->c:Ljavax/inject/Provider;

    iget-object p5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->x:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->x:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$d;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p4, p5, v0, v1}, Lcom/swedbank/mobile/app/cards/f/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/i;

    move-result-object p4

    invoke-static {p4}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p4

    iput-object p4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->y:Ljavax/inject/Provider;

    .line 8174
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->z:Ljavax/inject/Provider;

    .line 8175
    invoke-static {p3}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->A:Ljavax/inject/Provider;

    .line 8176
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->B:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8045
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->v:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8045
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->z:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic d(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8045
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->A:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic e(Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;)Ljavax/inject/Provider;
    .locals 0

    .line 8045
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->B:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 8045
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->b()Lcom/swedbank/mobile/app/cards/f/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/f/a/h;
    .locals 1

    .line 8181
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;->y:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/h;

    return-object v0
.end method
