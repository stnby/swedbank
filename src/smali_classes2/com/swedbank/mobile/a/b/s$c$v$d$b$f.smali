.class final Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/aa/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$v$d$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/loading/LoadingServicesInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/m;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/services/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)V
    .locals 0

    .line 3928
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3930
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 3909
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)V

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3935
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/aa/a/d;->b()Lcom/swedbank/mobile/a/aa/a/d;

    move-result-object v3

    invoke-virtual {v2, v3}, La/a/j$a;->b(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/j$a;->a()La/a/j;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->b:Ljavax/inject/Provider;

    .line 3936
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->b:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/a/aa/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/a/c;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->c:Ljavax/inject/Provider;

    .line 3937
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->c:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/swedbank/mobile/business/services/loading/a;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/services/loading/a;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->d:Ljavax/inject/Provider;

    .line 3938
    invoke-static {}, Lcom/swedbank/mobile/app/services/a/d;->b()Lcom/swedbank/mobile/app/services/a/d;

    move-result-object v2

    invoke-static {v2}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->e:Ljavax/inject/Provider;

    .line 3939
    invoke-static {v1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/app/services/m;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->e:Ljavax/inject/Provider;

    invoke-virtual {v2, v3, v4}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v2

    invoke-virtual {v2}, La/a/f$a;->a()La/a/f;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->f:Ljavax/inject/Provider;

    .line 3940
    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a(Lcom/swedbank/mobile/a/b/s$c$v$d$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {}, Lcom/swedbank/mobile/a/aa/a/e;->b()Lcom/swedbank/mobile/a/aa/a/e;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/swedbank/mobile/app/services/o;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/o;

    move-result-object v2

    iput-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->g:Ljavax/inject/Provider;

    .line 3941
    invoke-static {v1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->g:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->h:Ljavax/inject/Provider;

    .line 3942
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->f:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->h:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/aa/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/aa/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->i:Ljavax/inject/Provider;

    .line 3943
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->d:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->i:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$v$d$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d$b;->a:Lcom/swedbank/mobile/a/b/s$c$v$d;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v$d;->a:Lcom/swedbank/mobile/a/b/s$c$v;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$v;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/services/a/f;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/a/f;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->j:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 3909
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->b()Lcom/swedbank/mobile/app/services/a/e;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/services/a/e;
    .locals 1

    .line 3948
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$v$d$b$f;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/services/a/e;

    return-object v0
.end method
