.class final Lcom/swedbank/mobile/a/b/s$c$ab$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/c/e/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$ab;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/g;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/network/aa;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/c;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/j;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/scoped/f;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/scoped/d;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/b;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/t;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/authentication/b/p;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/t;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ")V"
        }
    .end annotation

    .line 15010
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15012
    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a(Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 14965
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/b/s$c$ab$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$ab;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;)V

    return-void
.end method

.method private a(Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/l;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ")V"
        }
    .end annotation

    .line 15019
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->o(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->p(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s;->q(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/a/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/c;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->b:Ljavax/inject/Provider;

    .line 15020
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->c:Ljavax/inject/Provider;

    .line 15021
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->d:Ljavax/inject/Provider;

    .line 15022
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->e:Ljavax/inject/Provider;

    .line 15023
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->f:Ljavax/inject/Provider;

    .line 15024
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->g:Ljavax/inject/Provider;

    .line 15025
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->n(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/j;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->h:Ljavax/inject/Provider;

    .line 15026
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/a/c/h;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/h;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->i:Ljavax/inject/Provider;

    .line 15027
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->d:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->e:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->f:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->g:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->h:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->i:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/data/authentication/e;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/e;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->j:Ljavax/inject/Provider;

    .line 15028
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/m;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/m;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    .line 15029
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/scoped/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/scoped/g;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->l:Ljavax/inject/Provider;

    .line 15030
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/scoped/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/scoped/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->m:Ljavax/inject/Provider;

    .line 15031
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->j:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->l:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->m:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/session/refresh/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/refresh/c;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->n:Ljavax/inject/Provider;

    .line 15032
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->n:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/u;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/u;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->o:Ljavax/inject/Provider;

    .line 15033
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/data/authentication/b/q;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/data/authentication/b/q;

    move-result-object v0

    invoke-static {v0}, La/a/k;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->p:Ljavax/inject/Provider;

    .line 15034
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->j:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->m:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/u;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/u;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->q:Ljavax/inject/Provider;

    .line 15035
    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->o:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->w(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->x(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->p:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->q:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->n:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/a/c/e/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/c/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->r:Ljavax/inject/Provider;

    .line 15036
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->s:Ljavax/inject/Provider;

    .line 15037
    invoke-static {p2}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->t:Ljavax/inject/Provider;

    .line 15038
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->k:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->t:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/authentication/session/scoped/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/scoped/i;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->u:Ljavax/inject/Provider;

    .line 15039
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->r:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->s:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->u:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->a:Lcom/swedbank/mobile/a/b/s$c$ab;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$ab;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lcom/swedbank/mobile/app/b/e/b/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/e/b/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->v:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 14965
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$ab$b;->b()Lcom/swedbank/mobile/app/b/e/b/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/b/e/b/c;
    .locals 1

    .line 15044
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$ab$b;->v:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/b/c;

    return-object v0
.end method
