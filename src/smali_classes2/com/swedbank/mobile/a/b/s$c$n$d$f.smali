.class final Lcom/swedbank/mobile/a/b/s$c$n$d$f;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/d/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$n$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$f;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$e;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$b;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$a;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$h;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$g;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$d;,
        Lcom/swedbank/mobile/a/b/s$c$n$d$f$c;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$n$d;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/b/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/g/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/g/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/a/e/d/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/h/b<",
            "*>;>;"
        }
    .end annotation
.end field

.field private m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/r;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/a/e/d/b;",
            ">;)V"
        }
    .end annotation

    .line 13225
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13227
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a(Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 13190
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$n$d$f;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)Ljavax/inject/Provider;
    .locals 0

    .line 13190
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->m:Ljavax/inject/Provider;

    return-object p0
.end method

.method private a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/a/e/d/b;",
            ">;)V"
        }
    .end annotation

    .line 13232
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$1;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->b:Ljavax/inject/Provider;

    .line 13237
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/c/b/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->c:Ljavax/inject/Provider;

    .line 13238
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$2;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->d:Ljavax/inject/Provider;

    .line 13243
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->d:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/c/d;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->e:Ljavax/inject/Provider;

    .line 13244
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$n$d$f$3;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$3;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)V

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->f:Ljavax/inject/Provider;

    .line 13249
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->f:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->g:Ljavax/inject/Provider;

    .line 13250
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->h:Ljavax/inject/Provider;

    .line 13251
    new-instance p1, Lcom/swedbank/mobile/a/b/s$c$n$d$f$4;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f$4;-><init>(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->i:Ljavax/inject/Provider;

    .line 13256
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->i:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/c/a/b;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->j:Ljavax/inject/Provider;

    .line 13257
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->j:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/e/d/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->k:Ljavax/inject/Provider;

    .line 13258
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->h:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/e/d/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/e;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->l:Ljavax/inject/Provider;

    .line 13259
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->d(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/s;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/s;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->m:Ljavax/inject/Provider;

    .line 13260
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->m:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/cards/notauth/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/notauth/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->n:Ljavax/inject/Provider;

    .line 13261
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->n:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/e/d/f;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/f;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->o:Ljavax/inject/Provider;

    .line 13262
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->n:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/a/e/d/g;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/d/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->p:Ljavax/inject/Provider;

    .line 13263
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->c:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->e:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->g:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->k:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->l:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->n:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->n:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->o:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->p:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->n:Ljavax/inject/Provider;

    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v10

    invoke-static/range {v0 .. v10}, Lcom/swedbank/mobile/app/cards/c/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/d;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->q:Ljavax/inject/Provider;

    .line 13264
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->a(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->b(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->a:Lcom/swedbank/mobile/a/b/s$c$n$d;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n$d;->a:Lcom/swedbank/mobile/a/b/s$c$n;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$n;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->c(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/g;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->r:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic b(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)Ljavax/inject/Provider;
    .locals 0

    .line 13190
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->r:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic c(Lcom/swedbank/mobile/a/b/s$c$n$d$f;)Ljavax/inject/Provider;
    .locals 0

    .line 13190
    iget-object p0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->g:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 13190
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->b()Lcom/swedbank/mobile/app/cards/c/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/cards/c/c;
    .locals 1

    .line 13269
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$n$d$f;->q:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/c/c;

    return-object v0
.end method
