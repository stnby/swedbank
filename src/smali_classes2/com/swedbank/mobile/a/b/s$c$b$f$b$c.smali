.class final Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/d/c/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f$b;

.field private b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f$b;)V
    .locals 0

    .line 11418
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f$b;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11418
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;
    .locals 0

    .line 11433
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->c:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;"
        }
    .end annotation

    .line 11426
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->b:Lcom/swedbank/mobile/business/util/e;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/d/c/a/a;
    .locals 5

    .line 11439
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->b:Lcom/swedbank/mobile/business/util/e;

    const-class v1, Lcom/swedbank/mobile/business/util/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 11440
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->c:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    const-class v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 11441
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$f$b;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->b:Lcom/swedbank/mobile/business/util/e;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->c:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/a/b/s$c$b$f$b$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f$b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/biometric/onboarding/error/e;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)Lcom/swedbank/mobile/a/d/c/a/a$a;
    .locals 0

    .line 11418
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->a(Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/d/c/a/a$a;
    .locals 0

    .line 11418
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;->a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/b/s$c$b$f$b$c;

    move-result-object p1

    return-object p1
.end method
