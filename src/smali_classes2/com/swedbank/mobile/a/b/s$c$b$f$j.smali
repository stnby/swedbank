.class final Lcom/swedbank/mobile/a/b/s$c$b$f$j;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/w/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "j"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/q/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/q/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V
    .locals 0

    .line 11208
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11210
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 11193
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$f$j;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$f;)V

    return-void
.end method

.method private c()V
    .locals 4

    .line 11215
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$f;->b(Lcom/swedbank/mobile/a/b/s$c$b$f;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->v(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/privacy/onboarding/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/privacy/onboarding/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->b:Ljavax/inject/Provider;

    .line 11216
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->b:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/swedbank/mobile/app/q/a/e;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/q/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->c:Ljavax/inject/Provider;

    .line 11217
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->c:Ljavax/inject/Provider;

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->d:Ljavax/inject/Provider;

    const/4 v0, 0x1

    .line 11218
    invoke-static {v0}, La/a/f;->a(I)La/a/f$a;

    move-result-object v1

    const-class v2, Lcom/swedbank/mobile/app/q/a/i;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->d:Ljavax/inject/Provider;

    invoke-virtual {v1, v2, v3}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v1

    invoke-virtual {v1}, La/a/f$a;->a()La/a/f;

    move-result-object v1

    iput-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->e:Ljavax/inject/Provider;

    const/4 v1, 0x0

    .line 11219
    invoke-static {v0, v1}, La/a/j;->a(II)La/a/j$a;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/app/q/a/j;->b()Lcom/swedbank/mobile/app/q/a/j;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/j$a;->a()La/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->f:Ljavax/inject/Provider;

    .line 11220
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->e:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->f:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/w/a/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/w/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->g:Ljavax/inject/Provider;

    .line 11221
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->g:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$f;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$f;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/q/a/g;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/q/a/g;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->h:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 11193
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->b()Lcom/swedbank/mobile/app/q/a/f;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/q/a/f;
    .locals 1

    .line 11226
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$f$j;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/q/a/f;

    return-object v0
.end method
