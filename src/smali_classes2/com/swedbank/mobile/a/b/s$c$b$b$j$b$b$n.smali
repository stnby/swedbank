.class final Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/x/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "n"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/c/g;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/c/m;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/r/c/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 0

    .line 10365
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10367
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a(Lcom/swedbank/mobile/business/preferences/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 10342
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;Lcom/swedbank/mobile/business/preferences/a;)V

    return-void
.end method

.method private a(Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 4

    .line 10372
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->r(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b;->g(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v3, v3, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v3}, Lcom/swedbank/mobile/a/b/s$c$b;->d(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/push/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/b;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->b:Ljavax/inject/Provider;

    .line 10373
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b;->g(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    invoke-static {v2}, Lcom/swedbank/mobile/a/b/s$c$b;->d(Lcom/swedbank/mobile/a/b/s$c$b;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/business/push/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->c:Ljavax/inject/Provider;

    .line 10374
    invoke-static {p1}, La/a/e;->a(Ljava/lang/Object;)La/a/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->d:Ljavax/inject/Provider;

    .line 10375
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object p1, p1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {p1}, Lcom/swedbank/mobile/a/b/s;->P(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->b:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->c:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->d:Ljavax/inject/Provider;

    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/business/push/preferences/b;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/preferences/b;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->e:Ljavax/inject/Provider;

    .line 10376
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->e:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/swedbank/mobile/app/r/c/i;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/c/i;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->f:Ljavax/inject/Provider;

    .line 10377
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->f:Ljavax/inject/Provider;

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->g:Ljavax/inject/Provider;

    const/4 p1, 0x1

    .line 10378
    invoke-static {p1}, La/a/f;->a(I)La/a/f$a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/app/r/c/m;

    iget-object v2, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->g:Ljavax/inject/Provider;

    invoke-virtual {v0, v1, v2}, La/a/f$a;->b(Ljava/lang/Object;Ljavax/inject/Provider;)La/a/f$a;

    move-result-object v0

    invoke-virtual {v0}, La/a/f$a;->a()La/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->h:Ljavax/inject/Provider;

    .line 10379
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->D(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/r/c/o;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/c/o;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->i:Ljavax/inject/Provider;

    const/4 v0, 0x0

    .line 10380
    invoke-static {p1, v0}, La/a/j;->a(II)La/a/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->i:Ljavax/inject/Provider;

    invoke-virtual {p1, v0}, La/a/j$a;->a(Ljavax/inject/Provider;)La/a/j$a;

    move-result-object p1

    invoke-virtual {p1}, La/a/j$a;->a()La/a/j;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->j:Ljavax/inject/Provider;

    .line 10381
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->h:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->j:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/a/x/c/c;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/x/c/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->k:Ljavax/inject/Provider;

    .line 10382
    iget-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->e:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->k:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$j;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$j;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/r/c/k;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/r/c/k;

    move-result-object p1

    invoke-static {p1}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->l:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 10342
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->b()Lcom/swedbank/mobile/app/r/c/j;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/r/c/j;
    .locals 1

    .line 10387
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$j$b$b$n;->l:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/r/c/j;

    return-object v0
.end method
