.class final Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/e/g/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

.field private b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private f:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)V
    .locals 0

    .line 7991
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 7991
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;
    .locals 0

    .line 8024
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-object p0
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;"
        }
    .end annotation

    .line 8005
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->b:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;"
        }
    .end annotation

    .line 8017
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->d:Ljava/util/List;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;
    .locals 0

    .line 8011
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->c:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/e/g/a/a;
    .locals 10

    .line 8036
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->b:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8037
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->c:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8038
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->d:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8039
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    const-class v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8040
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->f:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8041
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$d;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->b:Lcom/swedbank/mobile/business/util/l;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->c:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->d:Ljava/util/List;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iget-object v8, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->f:Ljava/lang/Boolean;

    const/4 v9, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$d;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$d;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Ljava/util/List;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;
    .locals 0

    .line 8030
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->f:Ljava/lang/Boolean;

    return-object p0
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/e/g/a/a$a;
    .locals 0

    .line 7991
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/e/g/a/a$a;
    .locals 0

    .line 7991
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/a/a$a;
    .locals 0

    .line 7991
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->a(Ljava/util/List;)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Z)Lcom/swedbank/mobile/a/e/g/a/a$a;
    .locals 0

    .line 7991
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->b(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Z)Lcom/swedbank/mobile/a/e/g/a/a$a;
    .locals 0

    .line 7991
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;->a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$d$c;

    move-result-object p1

    return-object p1
.end method
