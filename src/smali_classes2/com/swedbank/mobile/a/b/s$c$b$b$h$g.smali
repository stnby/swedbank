.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/d/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

.field private b:Ljava/lang/String;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V
    .locals 0

    .line 5080
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5080
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;"
        }
    .end annotation

    .line 5098
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->c:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;
    .locals 0

    .line 5091
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;
    .locals 0

    .line 5110
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->e:Ljava/lang/Boolean;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/a/u/d/a;
    .locals 9

    .line 5116
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->b:Ljava/lang/String;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5117
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->c:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5118
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->d:Lcom/swedbank/mobile/business/util/l;

    const-class v1, Lcom/swedbank/mobile/business/util/l;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5119
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->e:Ljava/lang/Boolean;

    const-class v1, Ljava/lang/Boolean;

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 5120
    new-instance v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v4, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->c:Lcom/swedbank/mobile/business/util/l;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->d:Lcom/swedbank/mobile/business/util/l;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->e:Ljava/lang/Boolean;

    const/4 v8, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$h;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/util/l;Ljava/lang/Boolean;Lcom/swedbank/mobile/a/b/s$1;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;)",
            "Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;"
        }
    .end annotation

    .line 5104
    invoke-static {p1}, La/a/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->d:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/swedbank/mobile/a/u/d/a$a;
    .locals 0

    .line 5080
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->a(Ljava/lang/String;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Z)Lcom/swedbank/mobile/a/u/d/a$a;
    .locals 0

    .line 5080
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->a(Z)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;

    move-result-object p1

    return-object p1
.end method

.method public synthetic c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/d/a$a;
    .locals 0

    .line 5080
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;

    move-result-object p1

    return-object p1
.end method

.method public synthetic d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/d/a$a;
    .locals 0

    .line 5080
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/b/s$c$b$b$h$g;

    move-result-object p1

    return-object p1
.end method
