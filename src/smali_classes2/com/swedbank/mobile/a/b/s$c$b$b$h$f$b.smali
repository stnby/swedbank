.class final Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;
.super Ljava/lang/Object;
.source "DaggerApplicationComponent.java"

# interfaces
.implements Lcom/swedbank/mobile/a/u/g/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

.field private b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/d/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V
    .locals 0

    .line 5579
    iput-object p1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5581
    invoke-direct {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->c()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;Lcom/swedbank/mobile/a/b/s$1;)V
    .locals 0

    .line 5572
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;-><init>(Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;)V

    return-void
.end method

.method private c()V
    .locals 9

    .line 5586
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/c;->a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->b:Ljavax/inject/Provider;

    .line 5587
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->b(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v1, v1, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    invoke-static {v1}, Lcom/swedbank/mobile/a/b/s$c$b$b;->a(Lcom/swedbank/mobile/a/b/s$c$b$b;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/d;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/plugins/accounts/d;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->c:Ljavax/inject/Provider;

    .line 5588
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->h(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->S(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->b:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->h(Lcom/swedbank/mobile/a/b/s$c$b$b$h;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->c:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->c:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->c:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f;->a:Lcom/swedbank/mobile/a/b/s$c$b$b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b$h;->a:Lcom/swedbank/mobile/a/b/s$c$b$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b$b;->a:Lcom/swedbank/mobile/a/b/s$c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c$b;->a:Lcom/swedbank/mobile/a/b/s$c;

    iget-object v0, v0, Lcom/swedbank/mobile/a/b/s$c;->a:Lcom/swedbank/mobile/a/b/s;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b/s;->l(Lcom/swedbank/mobile/a/b/s;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/overview/d/a/i;->a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/d/a/i;

    move-result-object v0

    invoke-static {v0}, La/a/c;->a(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->d:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1

    .line 5572
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->b()Lcom/swedbank/mobile/app/overview/d/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/app/overview/d/a/h;
    .locals 1

    .line 5593
    iget-object v0, p0, Lcom/swedbank/mobile/a/b/s$c$b$b$h$f$b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/a/h;

    return-object v0
.end method
