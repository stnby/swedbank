.class public final Lcom/swedbank/mobile/a/u/i/a;
.super Ljava/lang/Object;
.source "RecurringAuthOverviewNavigationModule.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/navigation/a;
    .locals 1
    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    sget-object v0, Lcom/swedbank/mobile/app/overview/f;->a:Lcom/swedbank/mobile/app/overview/f;

    check-cast v0, Lcom/swedbank/mobile/app/navigation/a;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/b/d/a;)Lkotlin/e/a/a;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/app/b/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/swedbank/mobile/a/u/i/a$a;

    new-instance v1, Lcom/swedbank/mobile/app/b/d/g;

    const-string v2, "feature_overview"

    .line 34
    sget v3, Lcom/swedbank/mobile/app/overview/i$a;->background_blue:I

    .line 35
    sget v4, Lcom/swedbank/mobile/app/overview/i$f;->overview_not_authenticated_toolbar_title:I

    .line 32
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/b/d/g;-><init>(Ljava/lang/String;II)V

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/app/b/d/a;->a(Lcom/swedbank/mobile/app/b/d/g;)Lcom/swedbank/mobile/app/b/d/a;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/a/u/i/a$a;-><init>(Lcom/swedbank/mobile/app/b/d/a;)V

    check-cast v0, Lkotlin/e/a/a;

    return-object v0
.end method
