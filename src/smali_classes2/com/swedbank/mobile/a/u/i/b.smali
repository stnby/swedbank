.class public final Lcom/swedbank/mobile/a/u/i/b;
.super Ljava/lang/Object;
.source "RecurringAuthOverviewNavigationModule_ProvideNavigationItemFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/navigation/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/u/i/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/u/i/a;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/i/b;->a:Lcom/swedbank/mobile/a/u/i/a;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/u/i/a;)Lcom/swedbank/mobile/a/u/i/b;
    .locals 1

    .line 23
    new-instance v0, Lcom/swedbank/mobile/a/u/i/b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/u/i/b;-><init>(Lcom/swedbank/mobile/a/u/i/a;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/a/u/i/a;)Lcom/swedbank/mobile/app/navigation/a;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/i/a;->a()Lcom/swedbank/mobile/app/navigation/a;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/app/navigation/a;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/navigation/a;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/i/b;->a:Lcom/swedbank/mobile/a/u/i/a;

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/i/b;->b(Lcom/swedbank/mobile/a/u/i/a;)Lcom/swedbank/mobile/app/navigation/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/i/b;->a()Lcom/swedbank/mobile/app/navigation/a;

    move-result-object v0

    return-object v0
.end method
