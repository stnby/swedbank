.class public interface abstract Lcom/swedbank/mobile/a/u/l/a/a$a;
.super Ljava/lang/Object;
.source "TransactionDetailsComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/u/l/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/u/l/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/a/u/l/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/overview/statement/details/f;
        .annotation runtime Ljavax/inject/Named;
            value = "transaction_details_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/u/l/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "transaction_details_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;)",
            "Lcom/swedbank/mobile/a/u/l/a/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
