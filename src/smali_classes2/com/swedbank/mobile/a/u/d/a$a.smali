.class public interface abstract Lcom/swedbank/mobile/a/u/d/a$a;
.super Ljava/lang/Object;
.source "OverviewDetailedComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/u/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/u/d/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)Lcom/swedbank/mobile/a/u/d/a$a;
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_initial_selected_account_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Z)Lcom/swedbank/mobile/a/u/d/a$a;
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_independent"
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/d/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;)",
            "Lcom/swedbank/mobile/a/u/d/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/d/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;)",
            "Lcom/swedbank/mobile/a/u/d/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
