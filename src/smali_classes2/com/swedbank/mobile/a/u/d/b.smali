.class public final Lcom/swedbank/mobile/a/u/d/b;
.super Ljava/lang/Object;
.source "OverviewDetailedModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/u/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Lcom/swedbank/mobile/a/u/d/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/d/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/d/b;->a:Lcom/swedbank/mobile/a/u/d/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;Z)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 9
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_independent"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;Z)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_overview_detailed"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 56
    sget v2, Lcom/swedbank/mobile/app/overview/i$e;->view_overview_detailed:I

    const/4 v4, 0x0

    .line 59
    sget-object p2, Lcom/swedbank/mobile/architect/a/b/a/e;->a:Lcom/swedbank/mobile/architect/a/b/a/e$a;

    .line 114
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/d;

    .line 59
    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/d;-><init>()V

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 115
    new-instance v0, Lcom/swedbank/mobile/core/ui/a/d;

    .line 59
    invoke-direct {v0}, Lcom/swedbank/mobile/core/ui/a/d;-><init>()V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 113
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    invoke-direct {v3, p2, v0}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 55
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/f$d;

    move-object v1, p2

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/f;

    goto :goto_0

    .line 60
    :cond_0
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/f$b;

    .line 61
    sget v1, Lcom/swedbank/mobile/app/overview/i$e;->view_overview_detailed:I

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, p2

    move-object v3, p0

    move-object v4, p1

    .line 60
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/architect/a/b/f$b;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/f;

    :goto_0
    return-object p2
.end method

.method public static final a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/plugins/a;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_initial_selected_account_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "overview_statement_initial_account_selection"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "initialSelectedAccountId"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/overview/plugins/a$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/a;

    return-object v0
.end method
