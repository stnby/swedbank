.class public final Lcom/swedbank/mobile/a/u/d/c;
.super Ljava/lang/Object;
.source "OverviewDetailedModule_ProvideInitialAccountSelectionFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/overview/plugins/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/d/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/d/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/u/d/c;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/u/d/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/u/d/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/plugins/a;
    .locals 1

    .line 28
    invoke-static {p0}, Lcom/swedbank/mobile/a/u/d/b;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/plugins/a;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/plugins/a;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/overview/plugins/a;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/d/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/d/c;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/plugins/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/d/c;->a()Lcom/swedbank/mobile/business/overview/plugins/a;

    move-result-object v0

    return-object v0
.end method
