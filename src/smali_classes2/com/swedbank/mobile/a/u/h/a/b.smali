.class public final Lcom/swedbank/mobile/a/u/h/a/b;
.super Ljava/lang/Object;
.source "CombinedOverviewPreferenceModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/u/h/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/a/u/h/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/h/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/h/a/b;->a:Lcom/swedbank/mobile/a/u/h/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/overview/e/a/c;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/overview/e/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/e/a/c;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_preferences_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugin"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p0}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
