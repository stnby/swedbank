.class public final Lcom/swedbank/mobile/a/u/e/c;
.super Ljava/lang/Object;
.source "LoadingOverviewModule_ProvidePluginManagerFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/i/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/e/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/e/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/u/e/c;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/swedbank/mobile/a/u/e/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/u/e/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/i/d;"
        }
    .end annotation

    .line 29
    invoke-static {p0}, Lcom/swedbank/mobile/a/u/e/b;->a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/i/d;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/i/d;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/e/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/e/c;->a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/e/c;->a()Lcom/swedbank/mobile/business/i/d;

    move-result-object v0

    return-object v0
.end method
