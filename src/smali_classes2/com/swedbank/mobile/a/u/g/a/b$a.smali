.class public final Lcom/swedbank/mobile/a/u/g/a/b$a;
.super Ljava/lang/Object;
.source "OverviewAccountsPluginModule.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/a/u/g/a/b;->a(Lcom/swedbank/mobile/app/overview/d/a/d;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/d/a/d;

.field final synthetic b:Ljavax/inject/Provider;

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:I

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/app/overview/d/a/f;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/d/a/d;Ljavax/inject/Provider;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->a:Lcom/swedbank/mobile/app/overview/d/a/d;

    iput-object p2, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->b:Ljavax/inject/Provider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p1, "feature_overview_accounts"

    .line 40
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->c:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->e:Ljavax/inject/Provider;

    .line 43
    sget-object p1, Lcom/swedbank/mobile/app/overview/d/a/f;->a:Lcom/swedbank/mobile/app/overview/d/a/f;

    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->f:Lcom/swedbank/mobile/app/overview/d/a/f;

    .line 44
    const-class p1, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    invoke-static {p1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->g:Lkotlin/h/b;

    return-void
.end method


# virtual methods
.method public a()Ljavax/inject/Provider;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->e:Ljavax/inject/Provider;

    return-object v0
.end method

.method public synthetic b()Lkotlin/e/a/b;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/g/a/b$a;->c()Lcom/swedbank/mobile/app/overview/d/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/app/overview/d/a/f;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->f:Lcom/swedbank/mobile/app/overview/d/a/f;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->d:I

    return v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/a/b$a;->g:Lkotlin/h/b;

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    new-instance v0, Lcom/swedbank/mobile/a/u/g/a/b$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/u/g/a/b$a$a;-><init>(Lcom/swedbank/mobile/a/u/g/a/b$a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 39
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/c$a;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Z

    move-result v0

    return v0
.end method
