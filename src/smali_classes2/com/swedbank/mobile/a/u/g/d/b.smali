.class public final Lcom/swedbank/mobile/a/u/g/d/b;
.super Ljava/lang/Object;
.source "OverviewStatementPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/u/g/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/a/u/g/d/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/g/d/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/g/d/b;->a:Lcom/swedbank/mobile/a/u/g/d/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/overview/d/d/e;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/overview/d/d/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_statement"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/d/e;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;)",
            "Lcom/swedbank/mobile/app/plugins/list/c;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_overview_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "overviewStatementBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/swedbank/mobile/a/u/g/d/b$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/u/g/d/b$a;-><init>(Lcom/swedbank/mobile/app/overview/d/d/e;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method

.method public static final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_overview_statement"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/16 v0, 0x9

    .line 26
    new-array v0, v0, [Lcom/swedbank/mobile/app/plugins/list/f;

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/q;->a:Lcom/swedbank/mobile/app/overview/d/d/q;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/o;->a:Lcom/swedbank/mobile/app/overview/d/d/o;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 29
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/p;->a:Lcom/swedbank/mobile/app/overview/d/d/p;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 30
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/n;->a:Lcom/swedbank/mobile/app/overview/d/d/n;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/l;->a:Lcom/swedbank/mobile/app/overview/d/d/l;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 32
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/b;->a:Lcom/swedbank/mobile/app/overview/d/d/b;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 33
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/d;->a:Lcom/swedbank/mobile/app/overview/d/d/d;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 34
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/c;->a:Lcom/swedbank/mobile/app/overview/d/d/c;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 35
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/a;->a:Lcom/swedbank/mobile/app/overview/d/d/a;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 26
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
