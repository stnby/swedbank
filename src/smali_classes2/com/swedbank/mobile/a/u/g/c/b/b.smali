.class public final Lcom/swedbank/mobile/a/u/g/c/b/b;
.super Ljava/lang/Object;
.source "OverviewRemoteSearchPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/u/g/c/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/u/g/c/b/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/g/c/b/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/g/c/b/b;->a:Lcom/swedbank/mobile/a/u/g/c/b/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/overview/d/c/b/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/overview/d/c/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_remote_search"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/c/b/a;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;)",
            "Lcom/swedbank/mobile/app/plugins/list/c;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_overview_search_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "overviewRemoteSearchBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/u/g/c/b/b$a;-><init>(Lcom/swedbank/mobile/app/overview/d/c/b/a;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method

.method public static final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_overview_remote_search"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x2

    .line 28
    new-array v0, v0, [Lcom/swedbank/mobile/app/plugins/list/f;

    .line 29
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/c/b/h;->a:Lcom/swedbank/mobile/app/overview/d/c/b/h;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 30
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/c;->a:Lcom/swedbank/mobile/app/overview/d/d/c;

    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 28
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
