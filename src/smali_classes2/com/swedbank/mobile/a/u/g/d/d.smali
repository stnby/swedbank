.class public final Lcom/swedbank/mobile/a/u/g/d/d;
.super Ljava/lang/Object;
.source "OverviewStatementPluginModule_ProvideStatementRenderersFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/List<",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "*>;>;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/swedbank/mobile/a/u/g/d/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/swedbank/mobile/a/u/g/d/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/g/d/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/g/d/d;->a:Lcom/swedbank/mobile/a/u/g/d/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()Lcom/swedbank/mobile/a/u/g/d/d;
    .locals 1

    .line 18
    sget-object v0, Lcom/swedbank/mobile/a/u/g/d/d;->a:Lcom/swedbank/mobile/a/u/g/d/d;

    return-object v0
.end method

.method public static c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .line 22
    invoke-static {}, Lcom/swedbank/mobile/a/u/g/d/b;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .line 14
    invoke-static {}, Lcom/swedbank/mobile/a/u/g/d/d;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/g/d/d;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
