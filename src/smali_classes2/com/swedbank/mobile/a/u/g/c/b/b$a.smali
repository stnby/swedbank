.class public final Lcom/swedbank/mobile/a/u/g/c/b/b$a;
.super Ljava/lang/Object;
.source "OverviewRemoteSearchPluginModule.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/a/u/g/c/b/b;->a(Lcom/swedbank/mobile/app/overview/d/c/b/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/d/c/b/a;

.field final synthetic b:Ljavax/inject/Provider;

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:I

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/app/overview/d/c/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/d/c/b/a;Ljavax/inject/Provider;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->a:Lcom/swedbank/mobile/app/overview/d/c/b/a;

    iput-object p2, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->b:Ljavax/inject/Provider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p1, "feature_overview_search_remote"

    .line 41
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->c:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->e:Ljavax/inject/Provider;

    .line 44
    sget-object p1, Lcom/swedbank/mobile/app/overview/d/c/b/c;->a:Lcom/swedbank/mobile/app/overview/d/c/b/c;

    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->f:Lcom/swedbank/mobile/app/overview/d/c/b/c;

    .line 45
    const-class p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;

    invoke-static {p1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->g:Lkotlin/h/b;

    return-void
.end method


# virtual methods
.method public a()Ljavax/inject/Provider;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->e:Ljavax/inject/Provider;

    return-object v0
.end method

.method public synthetic b()Lkotlin/e/a/b;
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->c()Lcom/swedbank/mobile/app/overview/d/c/b/c;

    move-result-object v0

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/app/overview/d/c/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->f:Lcom/swedbank/mobile/app/overview/d/c/b/c;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->d:I

    return v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/g/c/b/b$a;->g:Lkotlin/h/b;

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    new-instance v0, Lcom/swedbank/mobile/a/u/g/c/b/b$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/u/g/c/b/b$a$a;-><init>(Lcom/swedbank/mobile/a/u/g/c/b/b$a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 40
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/c$a;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Z

    move-result v0

    return v0
.end method
