.class public final Lcom/swedbank/mobile/a/u/g/a/b;
.super Ljava/lang/Object;
.source "OverviewAccountsPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/u/g/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/u/g/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/g/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/g/a/b;->a:Lcom/swedbank/mobile/a/u/g/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/overview/d/a/d;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/overview/d/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_accounts"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/a/d;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;)",
            "Lcom/swedbank/mobile/app/plugins/list/c;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_overview_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "overviewAccountsBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/swedbank/mobile/a/u/g/a/b$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/u/g/a/b$a;-><init>(Lcom/swedbank/mobile/app/overview/d/a/d;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method

.method public static final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_overview_accounts"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/a/c;->a:Lcom/swedbank/mobile/app/overview/d/a/c;

    .line 28
    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
