.class public final Lcom/swedbank/mobile/a/u/g/c/a/b;
.super Ljava/lang/Object;
.source "OverviewHistorySearchPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/u/g/c/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/swedbank/mobile/a/u/g/c/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/u/g/c/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/u/g/c/a/b;->a:Lcom/swedbank/mobile/a/u/g/c/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/overview/d/c/a/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/overview/d/c/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_history_search"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/c/a/a;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;)",
            "Lcom/swedbank/mobile/app/plugins/list/c;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_overview_search_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "overviewHistorySearchBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/swedbank/mobile/a/u/g/c/a/b$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/u/g/c/a/b$a;-><init>(Lcom/swedbank/mobile/app/overview/d/c/a/a;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method

.method public static final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_overview_history_search"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;->a:Lcom/swedbank/mobile/app/overview/d/c/a/g;

    .line 27
    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
