.class public final Lcom/swedbank/mobile/a/u/c;
.super Ljava/lang/Object;
.source "OverviewModule_ProvideOverviewServiceFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/data/overview/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lretrofit2/r;",
            ">;)",
            "Lcom/swedbank/mobile/a/u/c;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/u/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/u/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lretrofit2/r;)Lcom/swedbank/mobile/data/overview/g;
    .locals 1

    .line 28
    invoke-static {p0}, Lcom/swedbank/mobile/a/u/b;->a(Lretrofit2/r;)Lcom/swedbank/mobile/data/overview/g;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/overview/g;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/data/overview/g;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/r;

    invoke-static {v0}, Lcom/swedbank/mobile/a/u/c;->a(Lretrofit2/r;)Lcom/swedbank/mobile/data/overview/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/c;->a()Lcom/swedbank/mobile/data/overview/g;

    move-result-object v0

    return-object v0
.end method
