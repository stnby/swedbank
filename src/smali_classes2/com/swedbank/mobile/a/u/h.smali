.class public final Lcom/swedbank/mobile/a/u/h;
.super Ljava/lang/Object;
.source "OverviewNavigationModule_ProvideNodeBuilderFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lkotlin/e/a/a<",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/u/f;

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/u/f;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/u/f;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/b;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/a/u/h;->a:Lcom/swedbank/mobile/a/u/f;

    .line 19
    iput-object p2, p0, Lcom/swedbank/mobile/a/u/h;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/u/f;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/u/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/u/f;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/b;",
            ">;)",
            "Lcom/swedbank/mobile/a/u/h;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/swedbank/mobile/a/u/h;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/u/h;-><init>(Lcom/swedbank/mobile/a/u/f;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/app/overview/b;)Lkotlin/e/a/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/u/f;",
            "Lcom/swedbank/mobile/app/overview/b;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .line 34
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/u/f;->a(Lcom/swedbank/mobile/app/overview/b;)Lkotlin/e/a/a;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/e/a/a;

    return-object p0
.end method


# virtual methods
.method public a()Lkotlin/e/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/a/u/h;->a:Lcom/swedbank/mobile/a/u/f;

    iget-object v1, p0, Lcom/swedbank/mobile/a/u/h;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/overview/b;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/u/h;->a(Lcom/swedbank/mobile/a/u/f;Lcom/swedbank/mobile/app/overview/b;)Lkotlin/e/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/u/h;->a()Lkotlin/e/a/a;

    move-result-object v0

    return-object v0
.end method
