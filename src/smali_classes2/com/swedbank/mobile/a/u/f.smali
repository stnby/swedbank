.class public final Lcom/swedbank/mobile/a/u/f;
.super Ljava/lang/Object;
.source "OverviewNavigationModule.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/navigation/a;
    .locals 1
    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/overview/f;->a:Lcom/swedbank/mobile/app/overview/f;

    check-cast v0, Lcom/swedbank/mobile/app/navigation/a;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/b;)Lkotlin/e/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/b;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/swedbank/mobile/a/u/f$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/a/u/f$a;-><init>(Lcom/swedbank/mobile/app/overview/b;)V

    check-cast v0, Lkotlin/e/a/a;

    return-object v0
.end method
