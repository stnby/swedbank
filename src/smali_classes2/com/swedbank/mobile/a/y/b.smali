.class public final Lcom/swedbank/mobile/a/y/b;
.super Ljava/lang/Object;
.source "RetryModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/y/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/swedbank/mobile/a/y/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/y/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/y/b;->a:Lcom/swedbank/mobile/a/y/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 9
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_retry"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_retry"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_retry"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    .line 34
    sget v2, Lcom/swedbank/mobile/core/a$e;->view_retry:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v1, v0

    move-object v5, p0

    move-object v6, p1

    .line 33
    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method
