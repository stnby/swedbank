.class public final Lcom/swedbank/mobile/a/a;
.super Ljava/lang/Object;
.source "FlowModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    new-instance v0, Lcom/swedbank/mobile/a/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/a;->a:Lcom/swedbank/mobile/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;)Lcom/swedbank/mobile/architect/business/a/c;
    .locals 1
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "flows_map"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;)",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "flows"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/swedbank/mobile/architect/a/a/a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/architect/a/a/a;-><init>(Ljava/util/Map;)V

    check-cast v0, Lcom/swedbank/mobile/architect/business/a/c;

    return-object v0
.end method
