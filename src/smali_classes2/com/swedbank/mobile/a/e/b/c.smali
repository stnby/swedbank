.class public final Lcom/swedbank/mobile/a/e/b/c;
.super Ljava/lang/Object;
.source "CardsListModule_ProvideOutdatedCardShortcutHelpListenerFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/general/confirmation/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/a/e/b/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;",
            ">;)",
            "Lcom/swedbank/mobile/a/e/b/c;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/swedbank/mobile/a/e/b/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/e/b/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1

    .line 30
    invoke-static {p0}, Lcom/swedbank/mobile/a/e/b/b;->a(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/a/e/b/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/a/e/b/c;->a(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/e/b/c;->a()Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object v0

    return-object v0
.end method
