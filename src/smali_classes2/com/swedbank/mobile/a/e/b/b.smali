.class public final Lcom/swedbank/mobile/a/e/b/b;
.super Ljava/lang/Object;
.source "CardsListModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/e/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    new-instance v0, Lcom/swedbank/mobile/a/e/b/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/e/b/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/e/b/b;->a:Lcom/swedbank/mobile/a/e/b/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 4
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_cards_list"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget v0, Lcom/swedbank/mobile/app/cards/n$g;->view_cards_list:I

    .line 67
    sget-object v1, Lcom/swedbank/mobile/architect/a/b/a/e;->a:Lcom/swedbank/mobile/architect/a/b/a/e$a;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/j;->a:Lcom/swedbank/mobile/app/cards/list/j;

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    sget-object v2, Lcom/swedbank/mobile/app/cards/list/j;->a:Lcom/swedbank/mobile/app/cards/list/j;

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 150
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    invoke-direct {v3, v1, v2}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 63
    new-instance v1, Lcom/swedbank/mobile/architect/a/b/f$b;

    invoke-direct {v1, v0, v3, p0, p1}, Lcom/swedbank/mobile/architect/a/b/f$b;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v1
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "outdated_card_shortcut_help_listener"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->k()Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;
    .locals 1
    .param p0    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/i/d;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_logged_in_navigation_item_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v0, Lcom/swedbank/mobile/business/i/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/i/d;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_logged_in_navigation_item_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "wallet_general_error_notification_listener"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->l()Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object p0

    return-object p0
.end method
