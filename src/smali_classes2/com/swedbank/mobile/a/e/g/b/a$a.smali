.class public interface abstract Lcom/swedbank/mobile/a/e/g/b/a$a;
.super Ljava/lang/Object;
.source "WalletPaymentComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/e/g/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/e/g/b/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/a/e/g/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/k;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/e/g/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)",
            "Lcom/swedbank/mobile/a/e/g/b/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Z)Lcom/swedbank/mobile/a/e/g/b/a$a;
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_started_from_background"
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
