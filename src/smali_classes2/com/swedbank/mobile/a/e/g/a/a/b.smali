.class public final Lcom/swedbank/mobile/a/e/g/a/a/b;
.super Ljava/lang/Object;
.source "WalletOnboardingContactlessModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/e/g/a/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/swedbank/mobile/a/e/g/a/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/e/g/a/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/e/g/a/a/b;->a:Lcom/swedbank/mobile/a/e/g/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 9
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_contactless"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_contactless"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_wallet_onboarding_contactless"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget v2, Lcom/swedbank/mobile/app/cards/n$g;->view_wallet_onboarding_contactless:I

    .line 42
    sget-object v0, Lcom/swedbank/mobile/architect/a/b/a/e;->a:Lcom/swedbank/mobile/architect/a/b/a/e$a;

    .line 78
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/j;

    .line 42
    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/f/a/j;-><init>()V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 79
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/j;

    .line 42
    invoke-direct {v1}, Lcom/swedbank/mobile/app/cards/f/a/j;-><init>()V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 77
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    invoke-direct {v3, v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 38
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v1, v0

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method
