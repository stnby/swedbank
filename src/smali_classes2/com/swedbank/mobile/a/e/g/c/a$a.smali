.class public interface abstract Lcom/swedbank/mobile/a/e/g/c/a$a;
.super Ljava/lang/Object;
.source "WalletPreconditionsResolverComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/e/g/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/e/g/c/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/a/e/g/c/a$a;
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/c/a$a;
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_not_satisfied_preconditions"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)",
            "Lcom/swedbank/mobile/a/e/g/c/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
