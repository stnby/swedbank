.class public interface abstract Lcom/swedbank/mobile/a/e/g/b/c/a$a;
.super Ljava/lang/Object;
.source "WalletPaymentResultComponent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/e/g/b/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/e/g/b/c/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)Lcom/swedbank/mobile/a/e/g/b/c/a$a;
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_result"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;)Lcom/swedbank/mobile/a/e/g/b/c/a$a;
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_result_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
