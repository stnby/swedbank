.class public final Lcom/swedbank/mobile/a/e/g/a/i/b;
.super Ljava/lang/Object;
.source "WalletOnboardingWelcomeModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/e/g/a/i/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/swedbank/mobile/a/e/g/a/i/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/e/g/a/i/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/e/g/a/i/b;->a:Lcom/swedbank/mobile/a/e/g/a/i/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 4
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_welcome"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_welcome"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_wallet_onboarding_welcome"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget v0, Lcom/swedbank/mobile/app/cards/n$g;->view_wallet_onboarding_welcome:I

    .line 39
    sget-object v1, Lcom/swedbank/mobile/architect/a/b/a/e;->a:Lcom/swedbank/mobile/architect/a/b/a/e$a;

    sget-object v1, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    sget-object v2, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 70
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    invoke-direct {v3, v1, v2}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 35
    new-instance v1, Lcom/swedbank/mobile/architect/a/b/f$b;

    invoke-direct {v1, v0, v3, p0, p1}, Lcom/swedbank/mobile/architect/a/b/f$b;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v1
.end method
