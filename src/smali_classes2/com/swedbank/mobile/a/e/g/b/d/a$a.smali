.class public interface abstract Lcom/swedbank/mobile/a/e/g/b/d/a$a;
.super Ljava/lang/Object;
.source "WalletPaymentTapComponent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/e/g/b/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/e/g/b/d/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_tap_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_start_with_tap_retry"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lio/reactivex/o;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .param p1    # Lio/reactivex/o;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_tap_retry_signal_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;)",
            "Lcom/swedbank/mobile/a/e/g/b/d/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "digitized_card_id_selected_for_tap"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
