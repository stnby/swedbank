.class public final Lcom/swedbank/mobile/a/e/f/a;
.super Ljava/lang/Object;
.source "RecurringAuthCardsNavigationModule.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/app/navigation/a;
    .locals 1
    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    sget-object v0, Lcom/swedbank/mobile/app/cards/k;->a:Lcom/swedbank/mobile/app/cards/k;

    check-cast v0, Lcom/swedbank/mobile/app/navigation/a;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/b/d/a;Lcom/swedbank/mobile/app/cards/c/a;)Lkotlin/e/a/a;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/b/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/d/a;",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "recurringLoginBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notAuthenticatedCardsBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/swedbank/mobile/a/e/f/a$a;

    new-instance v1, Lcom/swedbank/mobile/a/e/d/b;

    .line 42
    const-class v2, Lcom/swedbank/mobile/business/authentication/recurring/d;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    .line 43
    new-instance v3, Lcom/swedbank/mobile/a/e/f/a$b;

    new-instance v4, Lcom/swedbank/mobile/app/b/d/g;

    const-string v5, "feature_cards"

    .line 45
    sget v6, Lcom/swedbank/mobile/app/cards/n$b;->background_green:I

    .line 46
    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->not_authenticated_empty_cards_toolbar_title:I

    .line 43
    invoke-direct {v4, v5, v6, v7}, Lcom/swedbank/mobile/app/b/d/g;-><init>(Ljava/lang/String;II)V

    invoke-virtual {p1, v4}, Lcom/swedbank/mobile/app/b/d/a;->a(Lcom/swedbank/mobile/app/b/d/g;)Lcom/swedbank/mobile/app/b/d/a;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/a/e/f/a$b;-><init>(Lcom/swedbank/mobile/app/b/d/a;)V

    check-cast v3, Lkotlin/e/a/a;

    .line 41
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/a/e/d/b;-><init>(Lkotlin/h/b;Lkotlin/e/a/a;)V

    invoke-virtual {p2, v1}, Lcom/swedbank/mobile/app/cards/c/a;->a(Lcom/swedbank/mobile/a/e/d/b;)Lcom/swedbank/mobile/app/cards/c/a;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/a/e/f/a$a;-><init>(Lcom/swedbank/mobile/app/cards/c/a;)V

    check-cast v0, Lkotlin/e/a/a;

    return-object v0
.end method
