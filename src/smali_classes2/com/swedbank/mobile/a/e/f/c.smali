.class public final Lcom/swedbank/mobile/a/e/f/c;
.super Ljava/lang/Object;
.source "RecurringAuthCardsNavigationModule_ProvideNodeBuilderFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lkotlin/e/a/a<",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/e/f/a;

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/f/a;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/e/f/a;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/swedbank/mobile/a/e/f/c;->a:Lcom/swedbank/mobile/a/e/f/a;

    .line 24
    iput-object p2, p0, Lcom/swedbank/mobile/a/e/f/c;->b:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/swedbank/mobile/a/e/f/c;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/e/f/a;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/e/f/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/e/f/a;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ">;)",
            "Lcom/swedbank/mobile/a/e/f/c;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/swedbank/mobile/a/e/f/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/a/e/f/c;-><init>(Lcom/swedbank/mobile/a/e/f/a;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/app/b/d/a;Lcom/swedbank/mobile/app/cards/c/a;)Lkotlin/e/a/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/e/f/a;",
            "Lcom/swedbank/mobile/app/b/d/a;",
            "Lcom/swedbank/mobile/app/cards/c/a;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/a/e/f/a;->a(Lcom/swedbank/mobile/app/b/d/a;Lcom/swedbank/mobile/app/cards/c/a;)Lkotlin/e/a/a;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/e/a/a;

    return-object p0
.end method


# virtual methods
.method public a()Lkotlin/e/a/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/a/e/f/c;->a:Lcom/swedbank/mobile/a/e/f/a;

    iget-object v1, p0, Lcom/swedbank/mobile/a/e/f/c;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/b/d/a;

    iget-object v2, p0, Lcom/swedbank/mobile/a/e/f/c;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/app/cards/c/a;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/a/e/f/c;->a(Lcom/swedbank/mobile/a/e/f/a;Lcom/swedbank/mobile/app/b/d/a;Lcom/swedbank/mobile/app/cards/c/a;)Lkotlin/e/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/e/f/c;->a()Lkotlin/e/a/a;

    move-result-object v0

    return-object v0
.end method
