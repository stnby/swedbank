.class public final Lcom/swedbank/mobile/a/e/d/c;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/e/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    new-instance v0, Lcom/swedbank/mobile/a/e/d/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/e/d/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/e/d/c;->a:Lcom/swedbank/mobile/a/e/d/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "outdated_card_shortcut_help_listener"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->b()Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/cards/c/a/a;)Lkotlin/e/a/a;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/app/cards/c/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/a/e/d/b;",
            ">;",
            "Lcom/swedbank/mobile/app/cards/c/a/a;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_not_authenticated_cards"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "emptyState"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notAuthEmptyCardsBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    new-instance p0, Lcom/swedbank/mobile/a/e/d/c$a;

    invoke-direct {p0, p1}, Lcom/swedbank/mobile/a/e/d/c$a;-><init>(Lcom/swedbank/mobile/app/cards/c/a/a;)V

    goto :goto_0

    .line 121
    :cond_0
    instance-of p1, p0, Lcom/swedbank/mobile/business/util/n;

    if-eqz p1, :cond_1

    check-cast p0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/a/e/d/b;

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/e/d/b;->b()Lkotlin/e/a/a;

    move-result-object p0

    .line 122
    :goto_0
    check-cast p0, Lkotlin/e/a/a;

    return-object p0

    .line 66
    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/util/l;)Lkotlin/h/b;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/a/e/d/b;",
            ">;)",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_not_authenticated_cards"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "emptyState"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const-class p0, Lcom/swedbank/mobile/business/cards/notauth/empty/c;

    invoke-static {p0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p0

    goto :goto_0

    .line 117
    :cond_0
    instance-of v0, p0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/a/e/d/b;

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/e/d/b;->a()Lkotlin/h/b;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final b(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "wallet_general_error_notification_listener"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->i()Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object p0

    return-object p0
.end method
