.class public final Lcom/swedbank/mobile/a/a/a;
.super Ljava/lang/Object;
.source "AccountModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/swedbank/mobile/a/a/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/a/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/a/a;->a:Lcom/swedbank/mobile/a/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lretrofit2/r;)Lcom/swedbank/mobile/data/account/e;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "session_authenticated"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const-class v0, Lcom/swedbank/mobile/data/account/e;

    invoke-virtual {p0, v0}, Lretrofit2/r;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/account/e;

    return-object p0
.end method
