.class public final Lcom/swedbank/mobile/a/h/a/e;
.super Ljava/lang/Object;
.source "AuthenticatedContactModule_ProvidePreselectedPrivateFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/a/h/a/e;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/h/a/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;)",
            "Lcom/swedbank/mobile/a/h/a/e;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/swedbank/mobile/a/h/a/e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/h/a/e;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/business/customer/l;)Z
    .locals 0

    .line 27
    invoke-static {p0}, Lcom/swedbank/mobile/a/h/a/b;->a(Lcom/swedbank/mobile/business/customer/l;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/a/h/a/e;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/customer/l;

    invoke-static {v0}, Lcom/swedbank/mobile/a/h/a/e;->a(Lcom/swedbank/mobile/business/customer/l;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/h/a/e;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
