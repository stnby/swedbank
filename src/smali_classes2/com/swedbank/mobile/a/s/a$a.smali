.class public interface abstract Lcom/swedbank/mobile/a/s/a$a;
.super Ljava/lang/Object;
.source "OnboardingComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/s/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/s/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/onboarding/d;)Lcom/swedbank/mobile/a/s/a$a;
    .param p1    # Lcom/swedbank/mobile/business/onboarding/d;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/util/List;)Lcom/swedbank/mobile/a/s/a$a;
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_plugin_keys"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/s/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
