.class public interface abstract Lcom/swedbank/mobile/a/s/b/a$a;
.super Ljava/lang/Object;
.source "OnboardingIntroComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/s/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/s/b/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/a/s/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/util/List;)Lcom/swedbank/mobile/a/s/b/a$a;
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_plugin_keys"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/s/b/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
