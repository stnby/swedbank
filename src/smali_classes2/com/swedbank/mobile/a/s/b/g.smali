.class public final Lcom/swedbank/mobile/a/s/b/g;
.super Ljava/lang/Object;
.source "OnboardingIntroModule_ProvideOnboardingTourMainItemCountFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/a/s/b/g;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljava/util/List;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;)I"
        }
    .end annotation

    .line 28
    invoke-static {p0}, Lcom/swedbank/mobile/a/s/b/b;->a(Ljava/util/List;)I

    move-result p0

    return p0
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/s/b/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/s/b/g;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/swedbank/mobile/a/s/b/g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/s/b/g;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Integer;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/a/s/b/g;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/swedbank/mobile/a/s/b/g;->a(Ljava/util/List;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/s/b/g;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
