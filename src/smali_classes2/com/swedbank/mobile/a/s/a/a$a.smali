.class public interface abstract Lcom/swedbank/mobile/a/s/a/a$a;
.super Ljava/lang/Object;
.source "OnboardingErrorHandlerComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/s/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/s/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/app/onboarding/a/k;)Lcom/swedbank/mobile/a/s/a/a$a;
    .param p1    # Lcom/swedbank/mobile/app/onboarding/a/k;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_error_information"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/onboarding/error/c;)Lcom/swedbank/mobile/a/s/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/onboarding/error/c;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_error_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
