.class public final Lcom/swedbank/mobile/a/s/d/b;
.super Ljava/lang/Object;
.source "OnboardingTourModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/s/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/swedbank/mobile/a/s/d/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/s/d/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/s/d/b;->a:Lcom/swedbank/mobile/a/s/d/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/List;)I
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_tour_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;)I"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "onboarding_tour_item_count"
    .end annotation

    const-string v0, "tourDetails"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    return p0
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 7
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_tour"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_tour"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_onboarding_tour"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v4, Lcom/swedbank/mobile/architect/a/b/c;->c:Lcom/swedbank/mobile/architect/a/b/c;

    .line 40
    sget-object v0, Lcom/swedbank/mobile/architect/a/b/a/e;->a:Lcom/swedbank/mobile/architect/a/b/a/e$a;

    .line 78
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/a/c;

    .line 40
    invoke-direct {v0}, Lcom/swedbank/mobile/architect/a/b/a/c;-><init>()V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 79
    new-instance v1, Lcom/swedbank/mobile/architect/a/b/a/c;

    .line 40
    invoke-direct {v1}, Lcom/swedbank/mobile/architect/a/b/a/c;-><init>()V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 77
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    invoke-direct {v3, v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 35
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    const v2, 0x7f0d0094

    move-object v1, v0

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method
