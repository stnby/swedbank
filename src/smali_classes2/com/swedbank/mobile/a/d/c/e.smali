.class public final Lcom/swedbank/mobile/a/d/c/e;
.super Ljava/lang/Object;
.source "BiometricOnboardingPluginModule_ProvidePluginFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/a/s/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;>;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/a/d/c/e;->a:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/swedbank/mobile/a/d/c/e;->b:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/swedbank/mobile/a/d/c/e;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/c/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/a/d/c/e;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/swedbank/mobile/a/d/c/e;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/a/d/c/e;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/app/c/c/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/c/c/a;",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/s/f;"
        }
    .end annotation

    .line 43
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/a/d/c/d;->a(Lcom/swedbank/mobile/app/c/c/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/a/s/f;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/s/f;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/a/d/c/e;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/c/c/a;

    iget-object v1, p0, Lcom/swedbank/mobile/a/d/c/e;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/onboarding/f;

    iget-object v2, p0, Lcom/swedbank/mobile/a/d/c/e;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/g;

    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/a/d/c/e;->a(Lcom/swedbank/mobile/app/c/c/a;Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/d/c/e;->a()Lcom/swedbank/mobile/a/s/f;

    move-result-object v0

    return-object v0
.end method
