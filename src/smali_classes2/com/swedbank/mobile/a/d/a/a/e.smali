.class public final Lcom/swedbank/mobile/a/d/a/a/e;
.super Ljava/lang/Object;
.source "ExternalBiometricAuthenticationPlugin.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/app/c/a/a/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/c/a/a/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/c/a/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "externalBioAuthBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/a/d/a/a/e;->a:Lcom/swedbank/mobile/app/c/a/a/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/a/d/a/a/e;)Lcom/swedbank/mobile/app/c/a/a/a;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/a/d/a/a/e;->a:Lcom/swedbank/mobile/app/c/a/a/a;

    return-object p0
.end method


# virtual methods
.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    const-class v0, Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    new-instance v0, Lcom/swedbank/mobile/a/d/a/a/e$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/d/a/a/e$a;-><init>(Lcom/swedbank/mobile/a/d/a/a/e;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
