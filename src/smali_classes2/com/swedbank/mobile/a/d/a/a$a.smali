.class public interface abstract Lcom/swedbank/mobile/a/d/a/a$a;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/d/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/d/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/d/a/a$a;
    .param p1    # Lcom/swedbank/mobile/app/c/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_prompt_information"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/d/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_prompt_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/swedbank/mobile/a/d/a/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_prompt_key_base_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/d/a/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
