.class public final Lcom/swedbank/mobile/a/d/a/b;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/d/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/swedbank/mobile/a/d/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/d/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/d/a/b;->a:Lcom/swedbank/mobile/a/d/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/biometric/d;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/architect/a/b/b;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/r;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/e;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/swedbank/mobile/architect/a/b/b;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_biometric_authentication_prompt"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "embeddedImpl"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "systemImpl"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "legacyImpl"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "embeddedViewId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    instance-of p4, p4, Lcom/swedbank/mobile/business/util/n;

    if-eqz p4, :cond_0

    invoke-interface {p0}, Lcom/swedbank/mobile/business/biometric/d;->e()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    const-string p1, "embeddedImpl.get()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/b;

    goto :goto_0

    .line 57
    :cond_0
    invoke-interface {p0}, Lcom/swedbank/mobile/business/biometric/d;->c()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    const-string p1, "systemImpl.get()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/b;

    goto :goto_0

    .line 58
    :cond_1
    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    const-string p1, "legacyImpl.get()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/b;

    :goto_0
    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/util/l;Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_biometric_authentication_prompt"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "embeddedViewId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenters"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    instance-of p1, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz p1, :cond_0

    invoke-interface {p0}, Lcom/swedbank/mobile/business/biometric/d;->e()Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, Lcom/swedbank/mobile/architect/a/b/f$a;

    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/architect/a/b/f$a;-><init>(Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/f;

    goto :goto_0

    .line 40
    :cond_0
    new-instance p0, Lcom/swedbank/mobile/architect/a/b/f$c;

    invoke-direct {p0, p2, p3}, Lcom/swedbank/mobile/architect/a/b/f$c;-><init>(Ljava/util/Map;Ljavax/inject/Provider;)V

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/f;

    :goto_0
    return-object p0
.end method
