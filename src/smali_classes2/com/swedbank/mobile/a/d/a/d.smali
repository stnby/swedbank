.class public final Lcom/swedbank/mobile/a/d/a/d;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptModule_ProvideViewDetailsFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/architect/a/b/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/swedbank/mobile/a/d/a/d;->a:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/swedbank/mobile/a/d/a/d;->b:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/swedbank/mobile/a/d/a/d;->c:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/swedbank/mobile/a/d/a/d;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/d/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/d/a/d;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/swedbank/mobile/a/d/a/d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/a/d/a/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/util/l;Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .line 51
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/a/d/a/b;->a(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/util/l;Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/a/b/f;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/a/d/a/d;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/d/a/d;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/util/l;

    iget-object v2, p0, Lcom/swedbank/mobile/a/d/a/d;->c:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    iget-object v3, p0, Lcom/swedbank/mobile/a/d/a/d;->d:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/a/d/a/d;->a(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/util/l;Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/d/a/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    return-object v0
.end method
