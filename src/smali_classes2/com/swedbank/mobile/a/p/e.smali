.class public final Lcom/swedbank/mobile/a/p/e;
.super Ljava/lang/Object;
.source "NavigationPluginRegistryModule_ProvideNavigationItemsFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/List<",
        "Lcom/swedbank/mobile/app/navigation/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/swedbank/mobile/a/p/e;->a:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/swedbank/mobile/a/p/e;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/p/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/p/e;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/swedbank/mobile/a/p/e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/p/e;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {p0, p1}, Lcom/swedbank/mobile/a/p/d;->a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/List;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/a/p/e;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/f/a;

    iget-object v1, p0, Lcom/swedbank/mobile/a/p/e;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/p/e;->a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/p/e;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
