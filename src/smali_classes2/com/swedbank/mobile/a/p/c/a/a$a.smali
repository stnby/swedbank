.class public interface abstract Lcom/swedbank/mobile/a/p/c/a/a$a;
.super Ljava/lang/Object;
.source "RecurringAuthNavigationComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/p/c/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/p/c/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/a/p/c/a/b;)Lcom/swedbank/mobile/a/p/c/a/a$a;
    .param p1    # Lcom/swedbank/mobile/a/p/c/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/p/c/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "logged_in_customer_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/a/p/c/a/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/p/c/a/a$a;
    .param p1    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "route_to_alternative_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lio/reactivex/b/c;",
            "Lkotlin/s;",
            ">;)",
            "Lcom/swedbank/mobile/a/p/c/a/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
