.class public final Lcom/swedbank/mobile/a/p/c/a/c;
.super Ljava/lang/Object;
.source "RecurringAuthNavigationModule_ProvideNavigationUuidFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/p/c/a/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/p/c/a/b;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/a/p/c/a/c;->a:Lcom/swedbank/mobile/a/p/c/a/b;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/p/c/a/b;)Lcom/swedbank/mobile/a/p/c/a/c;
    .locals 1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/a/p/c/a/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/p/c/a/c;-><init>(Lcom/swedbank/mobile/a/p/c/a/b;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/a/p/c/a/b;)Ljava/lang/String;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/p/c/a/b;->b()Ljava/lang/String;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/a/p/c/a/c;->a:Lcom/swedbank/mobile/a/p/c/a/b;

    invoke-static {v0}, Lcom/swedbank/mobile/a/p/c/a/c;->b(Lcom/swedbank/mobile/a/p/c/a/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/p/c/a/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
