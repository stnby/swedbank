.class public final Lcom/swedbank/mobile/a/p/d;
.super Ljava/lang/Object;
.source "NavigationPluginRegistryModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/p/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/a/p/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/p/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/p/d;->a:Lcom/swedbank/mobile/a/p/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_navigation_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationItemDetails"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    check-cast p1, Ljava/lang/Iterable;

    .line 27
    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 28
    new-instance v0, Lcom/swedbank/mobile/a/p/d$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/p/d$b;-><init>(Lcom/swedbank/mobile/business/f/a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p0

    .line 47
    new-instance p1, Lcom/swedbank/mobile/a/p/d$a;

    invoke-direct {p1}, Lcom/swedbank/mobile/a/p/d$a;-><init>()V

    check-cast p1, Ljava/util/Comparator;

    invoke-static {p0, p1}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object p0

    .line 30
    invoke-static {p0}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "to_navigation_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "to_navigation_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_navigation_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "navigationItemDetails"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationNodeBuilders"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    check-cast p0, Ljava/lang/Iterable;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 49
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 50
    check-cast v1, Lcom/swedbank/mobile/app/navigation/a;

    .line 41
    invoke-interface {v1}, Lcom/swedbank/mobile/app/navigation/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Lkotlin/e/a/a;

    .line 42
    new-instance v3, Lcom/swedbank/mobile/business/navigation/k;

    invoke-direct {v3, v1, v2}, Lcom/swedbank/mobile/business/navigation/k;-><init>(Ljava/lang/String;Lkotlin/e/a/a;)V

    .line 44
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Required value was null."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 51
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
