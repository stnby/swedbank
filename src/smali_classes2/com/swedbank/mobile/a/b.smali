.class public final Lcom/swedbank/mobile/a/b;
.super Ljava/lang/Object;
.source "FlowModule_ProvideFlowManagerFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/architect/business/a/c<",
        "Lcom/swedbank/mobile/business/root/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;>;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/swedbank/mobile/a/b;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;>;)",
            "Lcom/swedbank/mobile/a/b;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/swedbank/mobile/a/b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/b;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;)Lcom/swedbank/mobile/architect/business/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;)",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation

    .line 31
    invoke-static {p0}, Lcom/swedbank/mobile/a/a;->a(Ljava/util/Map;)Lcom/swedbank/mobile/architect/business/a/c;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/business/a/c;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/business/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/a/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Lcom/swedbank/mobile/a/b;->a(Ljava/util/Map;)Lcom/swedbank/mobile/architect/business/a/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/b;->a()Lcom/swedbank/mobile/architect/business/a/c;

    move-result-object v0

    return-object v0
.end method
