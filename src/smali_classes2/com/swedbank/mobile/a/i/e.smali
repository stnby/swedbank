.class public final Lcom/swedbank/mobile/a/i/e;
.super Ljava/lang/Object;
.source "CustomerModule_ProvideCustomerRepositoryFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/customer/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/i/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/i/d;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/a/i/e;->a:Lcom/swedbank/mobile/a/i/d;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/i/e;
    .locals 1

    .line 21
    new-instance v0, Lcom/swedbank/mobile/a/i/e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/i/e;-><init>(Lcom/swedbank/mobile/a/i/d;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/business/customer/e;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/i/d;->b()Lcom/swedbank/mobile/business/customer/e;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/customer/e;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/customer/e;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/a/i/e;->a:Lcom/swedbank/mobile/a/i/d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/i/e;->b(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/business/customer/e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/i/e;->a()Lcom/swedbank/mobile/business/customer/e;

    move-result-object v0

    return-object v0
.end method
