.class public interface abstract Lcom/swedbank/mobile/a/i/a$a;
.super Ljava/lang/Object;
.source "CustomerComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/i/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/i/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/i/a$a;
    .param p1    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "customer_child_node_attached_callback"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;)",
            "Lcom/swedbank/mobile/a/i/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract d(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/i/a$a;
    .param p1    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "customer_child_node_build_callback"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)",
            "Lcom/swedbank/mobile/a/i/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
