.class public final Lcom/swedbank/mobile/a/i/f;
.super Ljava/lang/Object;
.source "CustomerModule_ProvideGetMobileAgreementIdUseCaseFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/j<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/i/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/i/d;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/a/i/f;->a:Lcom/swedbank/mobile/a/i/d;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/i/f;
    .locals 1

    .line 23
    new-instance v0, Lcom/swedbank/mobile/a/i/f;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/i/f;-><init>(Lcom/swedbank/mobile/a/i/d;)V

    return-object v0
.end method

.method public static b(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/i/d;",
            ")",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/i/d;->c()Lcom/swedbank/mobile/architect/business/g;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/a/i/f;->a:Lcom/swedbank/mobile/a/i/d;

    invoke-static {v0}, Lcom/swedbank/mobile/a/i/f;->b(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/i/f;->a()Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    return-object v0
.end method
