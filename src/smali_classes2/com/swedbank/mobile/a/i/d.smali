.class public final Lcom/swedbank/mobile/a/i/d;
.super Ljava/lang/Object;
.source "CustomerModule.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/business/customer/l;

.field private final b:Lcom/swedbank/mobile/business/customer/e;

.field private final c:Lcom/swedbank/mobile/business/customer/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/customer/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/customer/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "selectedCustomer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getMobileAgreementId"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/a/i/d;->a:Lcom/swedbank/mobile/business/customer/l;

    iput-object p2, p0, Lcom/swedbank/mobile/a/i/d;->b:Lcom/swedbank/mobile/business/customer/e;

    iput-object p3, p0, Lcom/swedbank/mobile/a/i/d;->c:Lcom/swedbank/mobile/business/customer/g;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/customer/l;
    .locals 1
    .annotation runtime Ljavax/inject/Named;
        value = "selected_customer"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/a/i/d;->a:Lcom/swedbank/mobile/business/customer/l;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/business/customer/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/a/i/d;->b:Lcom/swedbank/mobile/business/customer/e;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "getMobileAgreementIdUseCase"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/a/i/d;->c:Lcom/swedbank/mobile/business/customer/g;

    check-cast v0, Lcom/swedbank/mobile/architect/business/g;

    return-object v0
.end method
