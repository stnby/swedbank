.class public final Lcom/swedbank/mobile/a/r/b;
.super Ljava/lang/Object;
.source "OldAppMigrationPlugin.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/app/o/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/o/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/o/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "oldAppMigrationBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/a/r/b;->a:Lcom/swedbank/mobile/app/o/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/a/r/b;)Lcom/swedbank/mobile/app/o/a;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/a/r/b;->a:Lcom/swedbank/mobile/app/o/a;

    return-object p0
.end method


# virtual methods
.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 14
    const-class v0, Lcom/swedbank/mobile/business/oldappmigration/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    new-instance v0, Lcom/swedbank/mobile/a/r/b$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/r/b$a;-><init>(Lcom/swedbank/mobile/a/r/b;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 10
    invoke-static {p0}, Lcom/swedbank/mobile/business/i/b$a;->a(Lcom/swedbank/mobile/business/i/b;)Z

    move-result v0

    return v0
.end method
