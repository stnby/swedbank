.class public final Lcom/swedbank/mobile/a/v/a;
.super Ljava/lang/Object;
.source "PreferencesModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/v/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Lcom/swedbank/mobile/a/v/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/v/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/v/a;->a:Lcom/swedbank/mobile/a/v/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 9
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_preferences"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    const v2, 0x7f0d00a1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v1, v0

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_preferences_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_preferences_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_preferences"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allPreferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    check-cast p1, Ljava/lang/Iterable;

    .line 78
    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 79
    new-instance v0, Lcom/swedbank/mobile/a/v/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/v/a$a;-><init>(Lcom/swedbank/mobile/business/f/a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p0

    .line 85
    invoke-static {p0}, Lkotlin/i/f;->d(Lkotlin/i/e;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
