.class public interface abstract Lcom/swedbank/mobile/a/g/a$a;
.super Ljava/lang/Object;
.source "ConfirmationDialogComponent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/g/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/g/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/a/g/a$a;
    .param p1    # Lcom/swedbank/mobile/app/f/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/a/g/a$a;
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)Lcom/swedbank/mobile/a/g/a$a;
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "confirmation_dialog_tag"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Z)Lcom/swedbank/mobile/a/g/a$a;
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "confirmation_dialog_cancelable"
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
