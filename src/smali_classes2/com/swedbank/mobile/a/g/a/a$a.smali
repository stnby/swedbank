.class public interface abstract Lcom/swedbank/mobile/a/g/a/a$a;
.super Ljava/lang/Object;
.source "BottomSheetDialogComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/g/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/g/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/app/f/a/d;)Lcom/swedbank/mobile/a/g/a/a$a;
    .param p1    # Lcom/swedbank/mobile/app/f/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "bottom_sheet_dialog_information"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;)Lcom/swedbank/mobile/a/g/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "bottom_sheet_dialog_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
