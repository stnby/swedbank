.class public final Lcom/swedbank/mobile/a/k/b;
.super Ljava/lang/Object;
.source "ExternalAppModule_ProvideIntentHandlersFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/swedbank/mobile/data/device/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/h/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/h/a/c;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/a/k/b;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/k/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/h/a/c;",
            ">;)",
            "Lcom/swedbank/mobile/a/k/b;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/swedbank/mobile/a/k/b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/k/b;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/app/h/a/c;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/h/a/c;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 31
    invoke-static {p0}, Lcom/swedbank/mobile/a/k/a;->a(Lcom/swedbank/mobile/app/h/a/c;)Ljava/util/Set;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/a/k/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/h/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/a/k/b;->a(Lcom/swedbank/mobile/app/h/a/c;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/k/b;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
