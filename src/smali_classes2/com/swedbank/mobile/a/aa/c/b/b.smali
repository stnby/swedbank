.class public final Lcom/swedbank/mobile/a/aa/c/b/b;
.super Ljava/lang/Object;
.source "ServicesIbankPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/aa/c/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/swedbank/mobile/a/aa/c/b/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/aa/c/b/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/aa/c/b/b;->a:Lcom/swedbank/mobile/a/aa/c/b/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/services/c/b/g;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/plugins/list/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/services/c/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_services_ibank"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/services/c/b/g;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;)",
            "Lcom/swedbank/mobile/app/plugins/list/c;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_services_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "servicesIbankBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v0, Lcom/swedbank/mobile/a/aa/c/b/b$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/aa/c/b/b$a;-><init>(Lcom/swedbank/mobile/app/services/c/b/g;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/business/c/a;)Lcom/swedbank/mobile/business/c/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-interface {p0}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/app/services/c/b/d;Lcom/swedbank/mobile/app/services/c/b/a;)Ljava/util/List;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/app/services/c/b/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/app/services/c/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/services/c/b/d;",
            "Lcom/swedbank/mobile/app/services/c/b/a;",
            ")",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_services_ibank"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "ibankServiceRenderer"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ibankServiceHeaderRenderer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 34
    new-array v0, v0, [Lcom/swedbank/mobile/app/plugins/list/f;

    .line 35
    check-cast p0, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 36
    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/f;

    const/4 p0, 0x1

    aput-object p1, v0, p0

    .line 34
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method
