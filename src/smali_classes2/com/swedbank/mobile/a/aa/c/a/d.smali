.class public final Lcom/swedbank/mobile/a/aa/c/a/d;
.super Ljava/lang/Object;
.source "ServicesAgreementsPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/aa/c/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/swedbank/mobile/a/aa/c/a/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/aa/c/a/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/aa/c/a/d;->a:Lcom/swedbank/mobile/a/aa/c/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/services/c/a/b;)Lcom/swedbank/mobile/app/plugins/list/c;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/services/c/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "to_services_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "servicesAgreementsBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/swedbank/mobile/a/aa/c/a/d$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/aa/c/a/d$a;-><init>(Lcom/swedbank/mobile/app/services/c/a/b;)V

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method
