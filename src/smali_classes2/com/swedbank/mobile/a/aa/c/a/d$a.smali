.class public final Lcom/swedbank/mobile/a/aa/c/a/d$a;
.super Ljava/lang/Object;
.source "ServicesAgreementsPluginModule.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/a/aa/c/a/d;->a(Lcom/swedbank/mobile/app/services/c/a/b;)Lcom/swedbank/mobile/app/plugins/list/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/services/c/a/b;

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:I

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/app/services/c/a/d;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/services/c/a/b;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->a:Lcom/swedbank/mobile/app/services/c/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p1, "feature_services_agreements"

    .line 33
    iput-object p1, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->b:Ljava/lang/String;

    .line 35
    sget-object p1, Lcom/swedbank/mobile/a/aa/c/a/d$a$b;->a:Lcom/swedbank/mobile/a/aa/c/a/d$a$b;

    check-cast p1, Ljavax/inject/Provider;

    iput-object p1, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->d:Ljavax/inject/Provider;

    .line 41
    sget-object p1, Lcom/swedbank/mobile/app/services/c/a/d;->a:Lcom/swedbank/mobile/app/services/c/a/d;

    iput-object p1, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->e:Lcom/swedbank/mobile/app/services/c/a/d;

    .line 42
    const-class p1, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-static {p1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->f:Lkotlin/h/b;

    return-void
.end method


# virtual methods
.method public a()Ljavax/inject/Provider;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->d:Ljavax/inject/Provider;

    return-object v0
.end method

.method public synthetic b()Lkotlin/e/a/b;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/aa/c/a/d$a;->c()Lcom/swedbank/mobile/app/services/c/a/d;

    move-result-object v0

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/app/services/c/a/d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->e:Lcom/swedbank/mobile/app/services/c/a/d;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .line 34
    iget v0, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->c:I

    return v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/a/aa/c/a/d$a;->f:Lkotlin/h/b;

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    new-instance v0, Lcom/swedbank/mobile/a/aa/c/a/d$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/aa/c/a/d$a$a;-><init>(Lcom/swedbank/mobile/a/aa/c/a/d$a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 32
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/c$a;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Z

    move-result v0

    return v0
.end method
