.class public interface abstract Lcom/swedbank/mobile/a/ac/d/b/a$a;
.super Ljava/lang/Object;
.source "PaymentFormComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/ac/d/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/ac/d/b/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/a/ac/d/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/form/g;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_form_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/ac/d/b/a$a;
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_form_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;)",
            "Lcom/swedbank/mobile/a/ac/d/b/a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Z)Lcom/swedbank/mobile/a/ac/d/b/a$a;
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
