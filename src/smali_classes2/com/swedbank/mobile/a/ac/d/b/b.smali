.class public final Lcom/swedbank/mobile/a/ac/d/b/b;
.super Ljava/lang/Object;
.source "PaymentFormModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/d/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    new-instance v0, Lcom/swedbank/mobile/a/ac/d/b/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/d/b/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/d/b/b;->a:Lcom/swedbank/mobile/a/ac/d/b/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;Z)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 9
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;Z)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_payment_form"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget v2, Lcom/swedbank/mobile/app/transfer/a$g;->view_payment_form:I

    if-eqz p2, :cond_0

    .line 64
    sget p2, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_view_root:I

    .line 126
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/a/e;

    .line 127
    new-instance v1, Lcom/swedbank/mobile/core/ui/a/i;

    invoke-direct {v1}, Lcom/swedbank/mobile/core/ui/a/i;-><init>()V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 128
    new-instance v3, Lcom/swedbank/mobile/core/ui/a/k;

    invoke-direct {v3, p2}, Lcom/swedbank/mobile/core/ui/a/k;-><init>(I)V

    check-cast v3, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 126
    invoke-direct {v0, v1, v3}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    move-object v3, v0

    goto :goto_0

    .line 65
    :cond_0
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/a/e;

    .line 66
    sget-object v0, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 67
    new-instance v1, Lcom/swedbank/mobile/core/ui/a/k;

    sget v3, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_view_root:I

    invoke-direct {v1, v3}, Lcom/swedbank/mobile/core/ui/a/k;-><init>(I)V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 65
    invoke-direct {p2, v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    move-object v3, p2

    :goto_0
    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 59
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/f$d;

    const/4 v4, 0x0

    move-object v1, p2

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/f;

    return-object p2
.end method

.method public static final a(Ljava/util/Set;)Lcom/swedbank/mobile/business/i/d;
    .locals 1
    .param p0    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_payment_form_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/i/d;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_payment_form_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/swedbank/mobile/business/i/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/i/d;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public static final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_payment_form_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 81
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
