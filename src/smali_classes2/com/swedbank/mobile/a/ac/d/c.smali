.class public final Lcom/swedbank/mobile/a/ac/d/c;
.super Ljava/lang/Object;
.source "PaymentModule_ProvideExecutePaymentImplFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lkotlin/e/a/b<",
        "Ljava/lang/String;",
        "Lio/reactivex/j<",
        "Lkotlin/k<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;>;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/payment/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/payment/j;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/a/ac/d/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/d/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/transfer/payment/j;",
            ">;)",
            "Lcom/swedbank/mobile/a/ac/d/c;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/swedbank/mobile/a/ac/d/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/d/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/transfer/payment/j;",
            ")",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;"
        }
    .end annotation

    .line 34
    invoke-static {p0}, Lcom/swedbank/mobile/a/ac/d/a;->a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lkotlin/e/a/b;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/e/a/b;

    return-object p0
.end method


# virtual methods
.method public a()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/a/ac/d/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/transfer/payment/j;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/d/c;->a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lkotlin/e/a/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/ac/d/c;->a()Lkotlin/e/a/b;

    move-result-object v0

    return-object v0
.end method
