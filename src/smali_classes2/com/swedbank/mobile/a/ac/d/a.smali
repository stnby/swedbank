.class public final Lcom/swedbank/mobile/a/ac/d/a;
.super Ljava/lang/Object;
.source "PaymentModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/swedbank/mobile/a/ac/d/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/d/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/d/a;->a:Lcom/swedbank/mobile/a/ac/d/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Lcom/swedbank/mobile/business/general/a;
    .locals 6
    .annotation runtime Ljavax/inject/Named;
        value = "instant_payment_poll_data"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    new-instance v0, Lcom/swedbank/mobile/business/general/a;

    .line 42
    new-instance v1, Lcom/swedbank/mobile/business/util/y;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x1

    invoke-direct {v1, v3, v4, v2}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 43
    new-instance v2, Lcom/swedbank/mobile/business/util/y;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v2, v3, v4, v5}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 41
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;)V

    return-object v0
.end method

.method public static final a(Lretrofit2/r;)Lcom/swedbank/mobile/data/transfer/payment/n;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "session_authenticated"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    const-class v0, Lcom/swedbank/mobile/data/transfer/payment/n;

    invoke-virtual {p0, v0}, Lretrofit2/r;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/transfer/payment/n;

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/data/transfer/payment/j;)Lkotlin/e/a/b;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/data/transfer/payment/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/transfer/payment/j;",
            ")",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "execute_payment_impl"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentRepositoryImpl"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/swedbank/mobile/a/ac/d/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/d/a$a;-><init>(Lcom/swedbank/mobile/data/transfer/payment/j;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public static final b()Lcom/swedbank/mobile/business/general/a;
    .locals 3
    .annotation runtime Ljavax/inject/Named;
        value = "payment_signing_poll_data"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    new-instance v0, Lcom/swedbank/mobile/business/general/a;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;ILkotlin/e/b/g;)V

    return-object v0
.end method

.method public static final c()Lcom/swedbank/mobile/business/general/a;
    .locals 6
    .annotation runtime Ljavax/inject/Named;
        value = "biometric_payment_signing_poll_data"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 53
    new-instance v0, Lcom/swedbank/mobile/business/general/a;

    .line 54
    new-instance v1, Lcom/swedbank/mobile/business/util/y;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x0

    invoke-direct {v1, v3, v4, v2}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 55
    new-instance v2, Lcom/swedbank/mobile/business/util/y;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-direct {v2, v4, v5, v3}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 53
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;)V

    return-object v0
.end method
