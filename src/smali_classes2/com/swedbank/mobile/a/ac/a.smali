.class public final Lcom/swedbank/mobile/a/ac/a;
.super Ljava/lang/Object;
.source "TransferAppModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/swedbank/mobile/a/ac/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/a;->a:Lcom/swedbank/mobile/a/ac/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/app/transfer/c/d;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/app/transfer/payment/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/payment/a/d;",
            "Lcom/swedbank/mobile/app/transfer/c/d;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "intent_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentExecutionIntentHandler"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestIntentHandler"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 24
    new-array v0, v0, [Lcom/swedbank/mobile/data/device/h;

    .line 25
    check-cast p0, Lcom/swedbank/mobile/data/device/h;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 26
    check-cast p1, Lcom/swedbank/mobile/data/device/h;

    const/4 p0, 0x1

    aput-object p1, v0, p0

    .line 24
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
