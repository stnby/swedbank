.class public interface abstract Lcom/swedbank/mobile/a/ac/g/a/a$a;
.super Ljava/lang/Object;
.source "PaymentRequestOpeningComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/ac/g/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/ac/g/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/transfer/request/opening/f;)Lcom/swedbank/mobile/a/ac/g/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/transfer/request/opening/f;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_request_opening_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)Lcom/swedbank/mobile/a/ac/g/a/a$a;
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_request_opening_payment_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
