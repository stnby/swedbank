.class public final Lcom/swedbank/mobile/a/ac/a/b;
.super Ljava/lang/Object;
.source "TransferDetailedModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/swedbank/mobile/a/ac/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/a/b;->a:Lcom/swedbank/mobile/a/ac/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Map;Ljavax/inject/Provider;)Lcom/swedbank/mobile/architect/a/b/f;
    .locals 9
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/architect/a/b/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_detailed"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "presenters"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget v2, Lcom/swedbank/mobile/app/transfer/a$g;->view_transfer_detailed:I

    .line 41
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_detailed_root:I

    .line 93
    new-instance v3, Lcom/swedbank/mobile/architect/a/b/a/e;

    .line 94
    new-instance v1, Lcom/swedbank/mobile/core/ui/a/i;

    invoke-direct {v1}, Lcom/swedbank/mobile/core/ui/a/i;-><init>()V

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 95
    new-instance v4, Lcom/swedbank/mobile/core/ui/a/k;

    invoke-direct {v4, v0}, Lcom/swedbank/mobile/core/ui/a/k;-><init>(I)V

    check-cast v4, Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 93
    invoke-direct {v3, v1, v4}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 37
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v1, v0

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method

.method public static final a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/business/i/a/b;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/plugins/list/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_plugin_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "transfer_detailed_plugin_details"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginDetails"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    check-cast p0, Lcom/swedbank/mobile/business/i/a/b;

    return-object p0
.end method

.method public static final b(Lcom/swedbank/mobile/app/plugins/list/c;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/plugins/list/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_plugin_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ")",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_detailed"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginDetails"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-interface {p0}, Lcom/swedbank/mobile/app/plugins/list/c;->a()Ljavax/inject/Provider;

    move-result-object p0

    invoke-static {p0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Lcom/swedbank/mobile/app/plugins/list/c;)Ljava/util/Map;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/app/plugins/list/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_plugin_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_detailed"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginDetails"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-interface {p0}, Lcom/swedbank/mobile/app/plugins/list/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/swedbank/mobile/app/plugins/list/c;->b()Lkotlin/e/a/b;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    invoke-static {p0, v1}, Lkotlin/e/b/z;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/e/a/b;

    invoke-static {v0, p0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p0

    invoke-static {p0}, Lkotlin/a/x;->a(Lkotlin/k;)Ljava/util/Map;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.app.plugins.list.ListPluginDataMapper<kotlin.Any> /* = (kotlin.Any) -> com.swedbank.mobile.app.plugins.list.ListPluginMappedData */"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
