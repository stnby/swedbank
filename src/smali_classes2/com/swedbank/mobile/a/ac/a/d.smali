.class public final Lcom/swedbank/mobile/a/ac/a/d;
.super Ljava/lang/Object;
.source "TransferDetailedModule_ProvidePluginDataMappersFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/swedbank/mobile/a/ac/a/d;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;)",
            "Lcom/swedbank/mobile/a/ac/a/d;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/swedbank/mobile/a/ac/a/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/a/d;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/app/plugins/list/c;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation

    .line 32
    invoke-static {p0}, Lcom/swedbank/mobile/a/ac/a/b;->c(Lcom/swedbank/mobile/app/plugins/list/c;)Ljava/util/Map;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/a/ac/a/d;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/a/d;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/ac/a/d;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
