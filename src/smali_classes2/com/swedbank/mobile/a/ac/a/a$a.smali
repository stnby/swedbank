.class public interface abstract Lcom/swedbank/mobile/a/ac/a/a$a;
.super Ljava/lang/Object;
.source "TransferDetailedComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/ac/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/ac/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(I)Lcom/swedbank/mobile/a/ac/a/a$a;
    .param p1    # I
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_title_res"
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/a/ac/a/a$a;
    .param p1    # Lcom/swedbank/mobile/app/plugins/list/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_plugin_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/a/ac/a/a$a;
    .param p1    # Lcom/swedbank/mobile/business/transfer/detailed/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
