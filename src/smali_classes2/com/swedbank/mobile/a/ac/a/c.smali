.class public final Lcom/swedbank/mobile/a/ac/a/c;
.super Ljava/lang/Object;
.source "TransferDetailedModule_ProvideListPluginFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/i/a/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/a/ac/a/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/plugins/list/c;",
            ">;)",
            "Lcom/swedbank/mobile/a/ac/a/c;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/swedbank/mobile/a/ac/a/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/a/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/business/i/a/b;
    .locals 1

    .line 29
    invoke-static {p0}, Lcom/swedbank/mobile/a/ac/a/b;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/business/i/a/b;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/i/a/b;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/i/a/b;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/a/ac/a/c;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/c;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ac/a/c;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/business/i/a/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/ac/a/c;->a()Lcom/swedbank/mobile/business/i/a/b;

    move-result-object v0

    return-object v0
.end method
