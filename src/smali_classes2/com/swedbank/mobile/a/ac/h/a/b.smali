.class public final Lcom/swedbank/mobile/a/ac/h/a/b;
.super Ljava/lang/Object;
.source "TransferHistorySearchPlugin.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/search/d;


# instance fields
.field private final a:Lcom/swedbank/mobile/app/transfer/search/a/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/search/a/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/search/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transferHistorySearchBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/a/ac/h/a/b;->a:Lcom/swedbank/mobile/app/transfer/search/a/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/a/ac/h/a/b;)Lcom/swedbank/mobile/app/transfer/search/a/a;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/a/ac/h/a/b;->a:Lcom/swedbank/mobile/app/transfer/search/a/a;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "feature_transfer_history_search"

    return-object v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    const-class v0, Lcom/swedbank/mobile/business/transfer/search/history/b;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    new-instance v0, Lcom/swedbank/mobile/a/ac/h/a/b$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/h/a/b$a;-><init>(Lcom/swedbank/mobile/a/ac/h/a/b;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 12
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/search/d$a;->a(Lcom/swedbank/mobile/business/transfer/search/d;)Z

    move-result v0

    return v0
.end method
