.class public final Lcom/swedbank/mobile/a/ac/h/b;
.super Ljava/lang/Object;
.source "TransferSearchDataModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/h/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/swedbank/mobile/a/ac/h/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/h/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/h/b;->a:Lcom/swedbank/mobile/a/ac/h/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lretrofit2/r;)Lcom/swedbank/mobile/data/transfer/search/c;
    .locals 1
    .param p0    # Lretrofit2/r;
        .annotation runtime Ljavax/inject/Named;
            value = "session_authenticated"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const-class v0, Lcom/swedbank/mobile/data/transfer/search/c;

    invoke-virtual {p0, v0}, Lretrofit2/r;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/data/transfer/search/c;

    return-object p0
.end method
