.class public final Lcom/swedbank/mobile/a/ac/h/f;
.super Ljava/lang/Object;
.source "TransferSearchPluginRegistryModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/h/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/swedbank/mobile/a/ac/h/f;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/h/f;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/h/f;->a:Lcom/swedbank/mobile/a/ac/h/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_transfer_search_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/transfer/search/d;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/d;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_transfer_search_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferPlugins"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    check-cast p1, Ljava/lang/Iterable;

    .line 28
    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 29
    new-instance v0, Lcom/swedbank/mobile/a/ac/h/f$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/h/f$a;-><init>(Lcom/swedbank/mobile/business/f/a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p0

    .line 30
    invoke-static {p0}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method
