.class public final Lcom/swedbank/mobile/a/ac/e/a;
.super Ljava/lang/Object;
.source "TransferPluginRegistryModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/e/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/swedbank/mobile/a/ac/e/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/e/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/e/a;->a:Lcom/swedbank/mobile/a/ac/e/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/business/f/a;Ljava/util/Set;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "to_transfer_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_plugins"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferPlugins"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    check-cast p1, Ljava/lang/Iterable;

    .line 30
    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 31
    new-instance v0, Lcom/swedbank/mobile/a/ac/e/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ac/e/a$a;-><init>(Lcom/swedbank/mobile/business/f/a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p0

    .line 32
    invoke-static {p0}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_plugins"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transferPlugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final b(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;)",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_plugins"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transferPlugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    check-cast p0, Ljava/lang/Iterable;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 64
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 65
    check-cast v1, Lcom/swedbank/mobile/app/transfer/f;

    .line 48
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/transfer/f;->a()Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final c(Ljava/util/List;)Ljava/util/Map;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "for_transfer_plugins"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transferPlugins"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Landroidx/c/a;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroidx/c/a;-><init>(I)V

    .line 58
    check-cast p0, Ljava/lang/Iterable;

    .line 67
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/transfer/f;

    .line 59
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/transfer/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/transfer/f;->b()Lkotlin/e/a/b;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v4, 0x1

    invoke-static {v1, v4}, Lkotlin/e/b/z;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v3, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.app.plugins.list.ListPluginDataMapper<kotlin.Any> /* = (kotlin.Any) -> com.swedbank.mobile.app.plugins.list.ListPluginMappedData */"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 57
    :cond_1
    check-cast v0, Ljava/util/Map;

    return-object v0
.end method
