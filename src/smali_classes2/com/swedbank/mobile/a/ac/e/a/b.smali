.class public final Lcom/swedbank/mobile/a/ac/e/a/b;
.super Ljava/lang/Object;
.source "TransferOwnAccountsPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/e/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/swedbank/mobile/a/ac/e/a/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/e/a/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/e/a/b;->a:Lcom/swedbank/mobile/a/ac/e/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/transfer/plugins/a/a;)Lcom/swedbank/mobile/app/transfer/f;
    .locals 11
    .param p0    # Lcom/swedbank/mobile/app/transfer/plugins/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "to_transfer_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transferOwnAccountsBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/swedbank/mobile/app/transfer/f;

    const-string v4, "feature_transfer_myaccounts"

    .line 30
    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_my_accounts_tab_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 31
    sget-object v1, Lcom/swedbank/mobile/a/ac/e/a/b$a;->a:Lcom/swedbank/mobile/a/ac/e/a/b$a;

    move-object v2, v1

    check-cast v2, Ljavax/inject/Provider;

    .line 32
    sget-object v1, Lcom/swedbank/mobile/a/ac/e/a/b$b;->a:Lcom/swedbank/mobile/a/ac/e/a/b$b;

    move-object v9, v1

    check-cast v9, Ljavax/inject/Provider;

    .line 33
    sget-object v1, Lcom/swedbank/mobile/app/transfer/plugins/a/f;->a:Lcom/swedbank/mobile/app/transfer/plugins/a/f;

    move-object v3, v1

    check-cast v3, Lkotlin/e/a/b;

    .line 34
    sget-object v1, Lcom/swedbank/mobile/app/transfer/plugins/a/c;->a:Lcom/swedbank/mobile/app/transfer/plugins/a/c;

    move-object v10, v1

    check-cast v10, Lkotlin/e/a/b;

    .line 35
    const-class v1, Lcom/swedbank/mobile/business/transfer/plugins/accounts/d;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v6

    .line 36
    new-instance v1, Lcom/swedbank/mobile/a/ac/e/a/b$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/a/ac/e/a/b$c;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/a/a;)V

    move-object v7, v1

    check-cast v7, Lkotlin/e/a/b;

    const/16 v5, 0xc8

    move-object v1, v0

    .line 27
    invoke-direct/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/f;-><init>(Ljavax/inject/Provider;Lkotlin/e/a/b;Ljava/lang/String;ILkotlin/h/b;Lkotlin/e/a/b;Ljava/lang/Integer;Ljavax/inject/Provider;Lkotlin/e/a/b;)V

    return-object v0
.end method
