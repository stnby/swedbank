.class public final Lcom/swedbank/mobile/a/ac/e/b/d;
.super Ljava/lang/Object;
.source "TransferPredefinedPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/e/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/a/ac/e/b/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/e/b/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/e/b/d;->a:Lcom/swedbank/mobile/a/ac/e/b/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/transfer/plugins/b/a;)Lcom/swedbank/mobile/app/transfer/f;
    .locals 11
    .param p0    # Lcom/swedbank/mobile/app/transfer/plugins/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "to_transfer_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transferPredefinedBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/swedbank/mobile/app/transfer/f;

    const-string v4, "feature_transfer_predefined"

    .line 32
    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->predefined_payments_tab_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 33
    sget-object v1, Lcom/swedbank/mobile/a/ac/e/b/d$a;->a:Lcom/swedbank/mobile/a/ac/e/b/d$a;

    move-object v2, v1

    check-cast v2, Ljavax/inject/Provider;

    .line 34
    sget-object v1, Lcom/swedbank/mobile/a/ac/e/b/d$b;->a:Lcom/swedbank/mobile/a/ac/e/b/d$b;

    move-object v9, v1

    check-cast v9, Ljavax/inject/Provider;

    .line 35
    sget-object v1, Lcom/swedbank/mobile/app/transfer/plugins/b/f;->a:Lcom/swedbank/mobile/app/transfer/plugins/b/f;

    move-object v3, v1

    check-cast v3, Lkotlin/e/a/b;

    .line 36
    sget-object v1, Lcom/swedbank/mobile/app/transfer/plugins/b/c;->a:Lcom/swedbank/mobile/app/transfer/plugins/b/c;

    move-object v10, v1

    check-cast v10, Lkotlin/e/a/b;

    .line 37
    const-class v1, Lcom/swedbank/mobile/business/transfer/plugins/predefined/d;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v6

    .line 38
    new-instance v1, Lcom/swedbank/mobile/a/ac/e/b/d$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/a/ac/e/b/d$c;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/b/a;)V

    move-object v7, v1

    check-cast v7, Lkotlin/e/a/b;

    const/16 v5, 0x12c

    move-object v1, v0

    .line 29
    invoke-direct/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/f;-><init>(Ljavax/inject/Provider;Lkotlin/e/a/b;Ljava/lang/String;ILkotlin/h/b;Lkotlin/e/a/b;Ljava/lang/Integer;Ljavax/inject/Provider;Lkotlin/e/a/b;)V

    return-object v0
.end method
