.class public final Lcom/swedbank/mobile/a/ac/e/c/b;
.super Ljava/lang/Object;
.source "TransferServicesPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ac/e/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/swedbank/mobile/a/ac/e/c/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ac/e/c/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ac/e/c/b;->a:Lcom/swedbank/mobile/a/ac/e/c/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/transfer/plugins/c/a;)Lcom/swedbank/mobile/app/transfer/f;
    .locals 13
    .param p0    # Lcom/swedbank/mobile/app/transfer/plugins/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Named;
        value = "to_transfer_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transferServicesBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/swedbank/mobile/app/transfer/f;

    const-string v4, "feature_transfer_services"

    .line 30
    sget-object v1, Lcom/swedbank/mobile/a/ac/e/c/b$a;->a:Lcom/swedbank/mobile/a/ac/e/c/b$a;

    move-object v2, v1

    check-cast v2, Ljavax/inject/Provider;

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/transfer/plugins/c/c;->a:Lcom/swedbank/mobile/app/transfer/plugins/c/c;

    move-object v3, v1

    check-cast v3, Lkotlin/e/a/b;

    .line 32
    const-class v1, Lcom/swedbank/mobile/business/transfer/plugins/services/c;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v6

    .line 33
    new-instance v1, Lcom/swedbank/mobile/a/ac/e/c/b$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/a/ac/e/c/b$b;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/c/a;)V

    move-object v7, v1

    check-cast v7, Lkotlin/e/a/b;

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1c0

    const/4 v12, 0x0

    move-object v1, v0

    .line 27
    invoke-direct/range {v1 .. v12}, Lcom/swedbank/mobile/app/transfer/f;-><init>(Ljavax/inject/Provider;Lkotlin/e/a/b;Ljava/lang/String;ILkotlin/h/b;Lkotlin/e/a/b;Ljava/lang/Integer;Ljavax/inject/Provider;Lkotlin/e/a/b;ILkotlin/e/b/g;)V

    return-object v0
.end method
