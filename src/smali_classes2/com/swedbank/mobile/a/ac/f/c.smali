.class public final Lcom/swedbank/mobile/a/ac/f/c;
.super Ljava/lang/Object;
.source "RecurringAuthTransferNavigationModule_ProvideNodeBuilderFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lkotlin/e/a/a<",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/a/ac/f/a;

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/ac/f/a;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/ac/f/a;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/a/ac/f/c;->a:Lcom/swedbank/mobile/a/ac/f/a;

    .line 20
    iput-object p2, p0, Lcom/swedbank/mobile/a/ac/f/c;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Lcom/swedbank/mobile/a/ac/f/a;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/f/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/ac/f/a;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ">;)",
            "Lcom/swedbank/mobile/a/ac/f/c;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/swedbank/mobile/a/ac/f/c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/ac/f/c;-><init>(Lcom/swedbank/mobile/a/ac/f/a;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/app/b/d/a;)Lkotlin/e/a/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/ac/f/a;",
            "Lcom/swedbank/mobile/app/b/d/a;",
            ")",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/a/ac/f/a;->a(Lcom/swedbank/mobile/app/b/d/a;)Lkotlin/e/a/a;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/e/a/a;

    return-object p0
.end method


# virtual methods
.method public a()Lkotlin/e/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/a/ac/f/c;->a:Lcom/swedbank/mobile/a/ac/f/a;

    iget-object v1, p0, Lcom/swedbank/mobile/a/ac/f/c;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/b/d/a;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/ac/f/c;->a(Lcom/swedbank/mobile/a/ac/f/a;Lcom/swedbank/mobile/app/b/d/a;)Lkotlin/e/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/ac/f/c;->a()Lkotlin/e/a/a;

    move-result-object v0

    return-object v0
.end method
