.class public final Lcom/swedbank/mobile/a/ac/b;
.super Ljava/lang/Object;
.source "TransferAppModule_ProvideIntentHandlersFactory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Ljava/util/Set<",
        "Lcom/swedbank/mobile/data/device/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/a/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/d;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/swedbank/mobile/a/ac/b;->a:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/swedbank/mobile/a/ac/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/a/ac/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/a/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/d;",
            ">;)",
            "Lcom/swedbank/mobile/a/ac/b;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/swedbank/mobile/a/ac/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/ac/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static a(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/app/transfer/c/d;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/payment/a/d;",
            "Lcom/swedbank/mobile/app/transfer/c/d;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 38
    invoke-static {p0, p1}, Lcom/swedbank/mobile/a/ac/a;->a(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/app/transfer/c/d;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, La/a/h;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/a/ac/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/d;

    iget-object v1, p0, Lcom/swedbank/mobile/a/ac/b;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/transfer/c/d;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/a/ac/b;->a(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/app/transfer/c/d;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/a/ac/b;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
