.class public final Lcom/swedbank/mobile/a/ae/b/e;
.super Ljava/lang/Object;
.source "WidgetPreferencesModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ae/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/swedbank/mobile/a/ae/b/e;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ae/b/e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ae/b/e;->a:Lcom/swedbank/mobile/a/ae/b/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/a/ae/b/d;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/a/ae/b/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/a/ae/b/d;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_preferences_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "plugin"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {p0}, Lkotlin/a/ac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
