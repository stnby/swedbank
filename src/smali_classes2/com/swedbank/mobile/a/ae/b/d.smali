.class public final Lcom/swedbank/mobile/a/ae/b/d;
.super Lcom/swedbank/mobile/app/p/c$a;
.source "WidgetPreferencesPlugin.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/app/widget/b/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/widget/b/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/widget/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "nodeBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/c$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/a/ae/b/d;->a:Lcom/swedbank/mobile/app/widget/b/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/a/ae/b/d;)Lcom/swedbank/mobile/app/widget/b/a;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/a/ae/b/d;->a:Lcom/swedbank/mobile/app/widget/b/a;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "feature_widget_preferences"

    return-object v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x60

    return v0
.end method

.method public c()Lcom/swedbank/mobile/app/p/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    sget-object v0, Lcom/swedbank/mobile/app/p/a;->b:Lcom/swedbank/mobile/app/p/a;

    return-object v0
.end method

.method public d()I
    .locals 1

    .line 25
    sget v0, Lcom/swedbank/mobile/app/widget/b$g;->widget_preferences_title:I

    return v0
.end method

.method public e()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    const-class v0, Lcom/swedbank/mobile/business/widget/preferences/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    new-instance v0, Lcom/swedbank/mobile/a/ae/b/d$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/ae/b/d$a;-><init>(Lcom/swedbank/mobile/a/ae/b/d;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public h()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 33
    sget v0, Lcom/swedbank/mobile/app/widget/b$g;->widget_preferences_subtitle:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
