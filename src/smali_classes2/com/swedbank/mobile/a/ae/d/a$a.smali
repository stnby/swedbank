.class public interface abstract Lcom/swedbank/mobile/a/ae/d/a$a;
.super Ljava/lang/Object;
.source "WidgetUpdateComp.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/a/ae/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a()Lcom/swedbank/mobile/a/ae/d/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/a/ae/d/a$a;
    .param p1    # Lcom/swedbank/mobile/a/c/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/business/widget/update/e;)Lcom/swedbank/mobile/a/ae/d/a$a;
    .param p1    # Lcom/swedbank/mobile/business/widget/update/e;
        .annotation runtime Ljavax/inject/Named;
            value = "widget_update_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Z)Lcom/swedbank/mobile/a/ae/d/a$a;
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "widget_update_listener"
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
