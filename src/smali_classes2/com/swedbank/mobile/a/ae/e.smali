.class public final Lcom/swedbank/mobile/a/ae/e;
.super Ljava/lang/Object;
.source "WidgetOnboardingTourPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ae/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/swedbank/mobile/a/ae/e;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ae/e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ae/e;->a:Lcom/swedbank/mobile/a/ae/e;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Lcom/swedbank/mobile/app/onboarding/i;
    .locals 10
    .annotation runtime Ljavax/inject/Named;
        value = "to_onboarding_tour_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    new-instance v9, Lcom/swedbank/mobile/app/onboarding/i;

    const-string v1, "onboarding_tour_id_widget"

    .line 22
    sget v3, Lcom/swedbank/mobile/app/widget/b$g;->onboarding_tour_widget_title:I

    .line 23
    sget v4, Lcom/swedbank/mobile/app/widget/b$g;->onboarding_tour_widget_text:I

    .line 24
    sget v5, Lcom/swedbank/mobile/app/widget/b$f;->onboarding_tour_widget:I

    const/16 v2, 0x3e8

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, v9

    .line 19
    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/onboarding/i;-><init>(Ljava/lang/String;IIIIZILkotlin/e/b/g;)V

    return-object v9
.end method
