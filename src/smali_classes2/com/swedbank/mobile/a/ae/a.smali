.class public final Lcom/swedbank/mobile/a/ae/a;
.super Ljava/lang/Object;
.source "WidgetAppModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/ae/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/swedbank/mobile/a/ae/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/ae/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/ae/a;->a:Lcom/swedbank/mobile/a/ae/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/widget/c;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/app/widget/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/widget/c;",
            ")",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "intent_handlers"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "widgetIntentHandler"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 23
    new-array v0, v0, [Lcom/swedbank/mobile/app/widget/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
