.class public final Lcom/swedbank/mobile/a/w/a/d;
.super Ljava/lang/Object;
.source "UserPrivacyOnboardingPluginModule.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/a/w/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/swedbank/mobile/a/w/a/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/a/w/a/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/a/w/a/d;->a:Lcom/swedbank/mobile/a/w/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/swedbank/mobile/app/q/a/a;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/app/q/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isUserPrivacyOnboardingNeededUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/q/a/a;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/swedbank/mobile/a/s/f;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "to_onboarding_plugin_point"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "onboardingBuilder"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isOnboardingNeeded"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/swedbank/mobile/a/w/a/d$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/a/w/a/d$a;-><init>(Lcom/swedbank/mobile/app/q/a/a;Lcom/swedbank/mobile/architect/business/g;)V

    check-cast v0, Lcom/swedbank/mobile/a/s/f;

    return-object v0
.end method
