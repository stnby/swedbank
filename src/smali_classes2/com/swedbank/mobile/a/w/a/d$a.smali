.class public final Lcom/swedbank/mobile/a/w/a/d$a;
.super Ljava/lang/Object;
.source "UserPrivacyOnboardingPluginModule.kt"

# interfaces
.implements Lcom/swedbank/mobile/a/s/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/a/w/a/d;->a(Lcom/swedbank/mobile/app/q/a/a;Lcom/swedbank/mobile/architect/business/g;)Lcom/swedbank/mobile/a/s/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/q/a/a;

.field final synthetic b:Lcom/swedbank/mobile/architect/business/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/q/a/a;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/swedbank/mobile/a/w/a/d$a;->a:Lcom/swedbank/mobile/app/q/a/a;

    iput-object p2, p0, Lcom/swedbank/mobile/a/w/a/d$a;->b:Lcom/swedbank/mobile/architect/business/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "feature_user_privacy"

    return-object v0
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()Lcom/swedbank/mobile/architect/business/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/a/w/a/d$a;->b:Lcom/swedbank/mobile/architect/business/g;

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/app/onboarding/i;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 33
    invoke-static {p0}, Lcom/swedbank/mobile/a/s/f$a;->a(Lcom/swedbank/mobile/a/s/f;)Lcom/swedbank/mobile/app/onboarding/i;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    const-class v0, Lcom/swedbank/mobile/business/privacy/onboarding/e;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    new-instance v0, Lcom/swedbank/mobile/a/w/a/d$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/a/w/a/d$a$a;-><init>(Lcom/swedbank/mobile/a/w/a/d$a;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 33
    invoke-static {p0}, Lcom/swedbank/mobile/a/s/f$a;->b(Lcom/swedbank/mobile/a/s/f;)Z

    move-result v0

    return v0
.end method
