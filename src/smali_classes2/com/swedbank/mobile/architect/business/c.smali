.class public Lcom/swedbank/mobile/architect/business/c;
.super Ljava/lang/Object;
.source "Interactor.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/d;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/b/b;

.field private volatile b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private final c:Lio/reactivex/k/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/e<",
            "TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lio/reactivex/b/b;

    invoke-direct {v0}, Lio/reactivex/b/b;-><init>()V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->a:Lio/reactivex/b/b;

    .line 21
    invoke-static {}, Lio/reactivex/k/e;->g()Lio/reactivex/k/e;

    move-result-object v0

    const-string v1, "SingleSubject.create<R>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->c:Lio/reactivex/k/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lio/reactivex/b/c;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p1

    check-cast v0, Ljava/lang/Object;

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->b:Ljava/lang/Object;

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->c:Lio/reactivex/k/e;

    invoke-virtual {v0, p1}, Lio/reactivex/k/e;->a_(Ljava/lang/Object;)V

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    return-void
.end method

.method protected final a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->a:Lio/reactivex/b/b;

    invoke-virtual {v0, p1}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Disposable added after interactor deactivation - it will be disposed"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected i_()V
    .locals 0

    return-void
.end method

.method protected final j_()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot provide router outside lifecycle methods onActivate()/onDeactivate. Currently "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "interactor "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/architect/business/c;->c:Lio/reactivex/k/e;

    invoke-virtual {v1}, Lio/reactivex/k/e;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "is already dead"

    goto :goto_0

    :cond_0
    const-string v1, "onActivate() has not been called yet"

    .line 47
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_1
    return-object v0
.end method

.method protected final k_()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final l_()V
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/business/c;->i_()V

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->a:Lio/reactivex/b/b;

    invoke-virtual {v0}, Lio/reactivex/b/b;->a()V

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->b:Ljava/lang/Object;

    return-void
.end method

.method protected m_()V
    .locals 0

    return-void
.end method

.method protected final p_()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "TR;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 68
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(router)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/c;->c:Lio/reactivex/k/e;

    check-cast v0, Lio/reactivex/w;

    return-object v0
.end method
