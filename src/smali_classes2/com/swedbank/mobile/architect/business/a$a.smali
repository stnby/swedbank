.class final Lcom/swedbank/mobile/architect/business/a$a;
.super Ljava/lang/Object;
.source "BusinessTree.kt"

# interfaces
.implements Lio/reactivex/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/business/a;->b()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/m<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/business/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/architect/business/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a$a;->a:Lcom/swedbank/mobile/architect/business/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/k;)V
    .locals 3
    .param p1    # Lio/reactivex/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/k<",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a$a;->a:Lcom/swedbank/mobile/architect/business/a;

    .line 133
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0}, Lcom/swedbank/mobile/architect/business/a;->b(Lcom/swedbank/mobile/architect/business/a;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a$a;->a:Lcom/swedbank/mobile/architect/business/a;

    invoke-static {v0}, Lcom/swedbank/mobile/architect/business/a;->a(Lcom/swedbank/mobile/architect/business/a;)Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->t()Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 47
    invoke-interface {p1}, Lio/reactivex/k;->c()V

    goto :goto_1

    .line 48
    :cond_1
    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 137
    :cond_2
    invoke-static {v0}, Lcom/swedbank/mobile/architect/business/a;->b(Lcom/swedbank/mobile/architect/business/a;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/business/a$a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/architect/business/a$a$a;-><init>(Lcom/swedbank/mobile/architect/business/a$a;Lio/reactivex/k;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void
.end method
