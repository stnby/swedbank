.class public final Lcom/swedbank/mobile/architect/business/a;
.super Ljava/lang/Object;
.source "BusinessTree.kt"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/swedbank/mobile/architect/a/h;

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lio/reactivex/v;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/architect/business/a;-><init>(Lio/reactivex/v;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lio/reactivex/v;)V
    .locals 1
    .param p1    # Lio/reactivex/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "scheduleAttachmentChangeObservationsOn"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a;->d:Lio/reactivex/v;

    .line 24
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a;->a:Landroid/os/Handler;

    .line 26
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<AttachmentChange>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a;->c:Lcom/b/c/c;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/v;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 22
    invoke-static {}, Lio/reactivex/j/a;->c()Lio/reactivex/v;

    move-result-object p1

    const-string p2, "Schedulers.trampoline()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/architect/business/a;-><init>(Lio/reactivex/v;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/business/a;)Lcom/swedbank/mobile/architect/a/h;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/architect/business/a;->b:Lcom/swedbank/mobile/architect/a/h;

    return-object p0
.end method

.method public static synthetic a(Lcom/swedbank/mobile/architect/business/a;ZZLkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    .line 57
    sget-object p3, Lcom/swedbank/mobile/architect/business/a$c;->a:Lcom/swedbank/mobile/architect/business/a$c;

    check-cast p3, Lkotlin/e/a/b;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/architect/business/a;->a(ZZLkotlin/e/a/b;Lkotlin/e/a/b;)V

    return-void
.end method

.method private final a(Lcom/swedbank/mobile/architect/a/h;ZZLkotlin/e/a/b;)Z
    .locals 4
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/a/h;",
            "ZZ",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->u()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v1

    instance-of v1, v1, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v1, :cond_1

    .line 75
    :cond_0
    invoke-interface {p4, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    .line 79
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 142
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/h;

    const-string v3, "child"

    .line 80
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, p2, p3, p4}, Lcom/swedbank/mobile/architect/business/a;->a(Lcom/swedbank/mobile/architect/a/h;ZZLkotlin/e/a/b;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v0

    :cond_3
    if-eqz p2, :cond_5

    if-eqz p3, :cond_4

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->u()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object p2

    instance-of p2, p2, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz p2, :cond_5

    .line 85
    :cond_4
    invoke-interface {p4, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_5

    return v0

    :cond_5
    const/4 p1, 0x0

    return p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/architect/a/h;ZZLkotlin/e/a/b;)Z
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/architect/business/a;->a(Lcom/swedbank/mobile/architect/a/h;ZZLkotlin/e/a/b;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/architect/business/a;)Landroid/os/Handler;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/architect/business/a;->a:Landroid/os/Handler;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a;->c:Lcom/b/c/c;

    .line 29
    iget-object v1, p0, Lcom/swedbank/mobile/architect/business/a;->d:Lio/reactivex/v;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "attachmentChangeStream\n \u2026mentChangeObservationsOn)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a;->b:Lcom/swedbank/mobile/architect/a/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->s()V

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a;->b:Lcom/swedbank/mobile/architect/a/h;

    .line 36
    iget-object v2, p0, Lcom/swedbank/mobile/architect/business/a;->c:Lcom/b/c/c;

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    move-object v1, p1

    .line 35
    invoke-virtual/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/b/c/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(ZZLkotlin/e/a/b;Lkotlin/e/a/b;)V
    .locals 8
    .param p3    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/s;",
            ">;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "endCallback"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "visitCallback"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/business/a;->b(Lcom/swedbank/mobile/architect/business/a;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    invoke-static {p0}, Lcom/swedbank/mobile/architect/business/a;->a(Lcom/swedbank/mobile/architect/business/a;)Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, v0, p1, p2, p4}, Lcom/swedbank/mobile/architect/business/a;->a(Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/architect/a/h;ZZLkotlin/e/a/b;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/4 p2, 0x1

    .line 65
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {p3, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 137
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/architect/business/a;->b(Lcom/swedbank/mobile/architect/business/a;)Landroid/os/Handler;

    move-result-object v0

    new-instance v7, Lcom/swedbank/mobile/architect/business/a$b;

    move-object v1, v7

    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/business/a$b;-><init>(Lcom/swedbank/mobile/architect/business/a;ZZLkotlin/e/a/b;Lkotlin/e/a/b;)V

    check-cast v7, Ljava/lang/Runnable;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void
.end method

.method public final b()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    new-instance v0, Lcom/swedbank/mobile/architect/business/a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/architect/business/a$a;-><init>(Lcom/swedbank/mobile/architect/business/a;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026tadata)\n      }\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
