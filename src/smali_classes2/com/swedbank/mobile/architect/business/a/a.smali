.class public final Lcom/swedbank/mobile/architect/business/a/a;
.super Ljava/lang/Object;
.source "FlowManager.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RootNode::",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/a/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "TRootNode;",
            "Lcom/swedbank/mobile/architect/business/a/b;",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/architect/business/a/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/f;Lcom/swedbank/mobile/architect/business/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "TRootNode;",
            "Lcom/swedbank/mobile/architect/business/a/b;",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/b;",
            ")V"
        }
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a/a;->a:Lcom/swedbank/mobile/architect/business/a/f;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/business/a/a;->b:Lcom/swedbank/mobile/architect/business/a/b;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/architect/business/a/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "TRootNode;",
            "Lcom/swedbank/mobile/architect/business/a/b;",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a/a;->a:Lcom/swedbank/mobile/architect/business/a/f;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/architect/business/a/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a/a;->b:Lcom/swedbank/mobile/architect/business/a/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/architect/business/a/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/architect/business/a/a;

    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a/a;->a:Lcom/swedbank/mobile/architect/business/a/f;

    iget-object v1, p1, Lcom/swedbank/mobile/architect/business/a/a;->a:Lcom/swedbank/mobile/architect/business/a/f;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a/a;->b:Lcom/swedbank/mobile/architect/business/a/b;

    iget-object p1, p1, Lcom/swedbank/mobile/architect/business/a/a;->b:Lcom/swedbank/mobile/architect/business/a/b;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a/a;->a:Lcom/swedbank/mobile/architect/business/a/f;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/architect/business/a/a;->b:Lcom/swedbank/mobile/architect/business/a/b;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FlowExecutionRequest(flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/business/a/a;->a:Lcom/swedbank/mobile/architect/business/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/business/a/a;->b:Lcom/swedbank/mobile/architect/business/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
