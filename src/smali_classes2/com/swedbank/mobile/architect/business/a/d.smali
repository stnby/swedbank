.class public final Lcom/swedbank/mobile/architect/business/a/d;
.super Ljava/lang/Object;
.source "NodeFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputNode::",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j<",
            "TInputNode;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/j;)V
    .locals 1
    .param p1    # Lio/reactivex/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j<",
            "TInputNode;>;)V"
        }
    .end annotation

    const-string v0, "flowStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/a/d;->a:Lio/reactivex/j;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 3
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<NextNode::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/e/a/b<",
            "-TInputNode;+",
            "Lio/reactivex/j<",
            "TNextNode;>;>;)",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "TNextNode;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "step"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/swedbank/mobile/architect/business/a/d;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/business/a/d;->a:Lio/reactivex/j;

    new-instance v2, Lcom/swedbank/mobile/architect/business/a/d$a;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/architect/business/a/d$a;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v1, "flowStream.flatMap { step(it) }"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/architect/business/a/d;-><init>(Lio/reactivex/j;)V

    return-object v0
.end method

.method public final a()Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "TInputNode;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/a/d;->a:Lio/reactivex/j;

    return-object v0
.end method
