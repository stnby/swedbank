.class public abstract Lcom/swedbank/mobile/architect/business/a/f;
.super Ljava/lang/Object;
.source "NodeFlow.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RootNode::",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        "Input::",
        "Lcom/swedbank/mobile/architect/business/a/b;",
        "EndNode::",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/e/a/m<",
        "TRootNode;TInput;",
        "Lcom/swedbank/mobile/architect/business/a/d<",
        "TEndNode;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .param p1    # Lcom/swedbank/mobile/architect/business/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRootNode;TInput;)",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "TEndNode;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public final a(Lkotlin/e/a/a;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 1
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<NextNode::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/e/a/a<",
            "+",
            "Lio/reactivex/j<",
            "TNextNode;>;>;)",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "TNextNode;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "firstStepFunction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/swedbank/mobile/architect/business/a/d;

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/j;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/architect/business/a/d;-><init>(Lio/reactivex/j;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRootNode;TInput;)",
            "Lio/reactivex/j<",
            "TEndNode;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "rootNode"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/architect/business/a/f;->a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/a/d;->a()Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
