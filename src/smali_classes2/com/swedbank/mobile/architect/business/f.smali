.class public abstract Lcom/swedbank/mobile/architect/business/f;
.super Ljava/lang/Object;
.source "SharedUseCase.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ObserveType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/o<",
        "TObserveType;>;>;"
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/architect/business/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "observable"

    const-string v4, "getObservable()Lio/reactivex/Observable;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/architect/business/f;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/swedbank/mobile/architect/business/f$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/architect/business/f$a;-><init>(Lcom/swedbank/mobile/architect/business/f;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {v0}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/f;->b:Lkotlin/d;

    return-void
.end method

.method private final d()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TObserveType;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/f;->b:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/architect/business/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method


# virtual methods
.method protected abstract a()Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TObserveType;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public final c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "TObserveType;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/f;->d()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
