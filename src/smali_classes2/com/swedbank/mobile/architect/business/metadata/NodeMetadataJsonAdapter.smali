.class public final Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "NodeMetadataJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field private final nullableListOfNodeMetadataAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nullableViewTypeAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lcom/swedbank/mobile/architect/business/metadata/ViewType;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;

.field private final stringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 4
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v0, "nodeId"

    const-string v1, "children"

    const-string v2, "nodeName"

    const-string v3, "viewType"

    .line 17
    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"n\u2026, \"nodeName\", \"viewType\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 20
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "nodeId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String>(St\u2026ons.emptySet(), \"nodeId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 23
    const-class v0, Ljava/util/List;

    check-cast v0, Ljava/lang/reflect/Type;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    check-cast v2, Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/moshi/p;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "children"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<List<NodeM\u2026s.emptySet(), \"children\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->nullableListOfNodeMetadataAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 26
    const-class v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "viewType"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<ViewType?>\u2026s.emptySet(), \"viewType\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->nullableViewTypeAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 72
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "nodeId"

    .line 73
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "children"

    .line 75
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->nullableListOfNodeMetadataAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "nodeName"

    .line 77
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "viewType"

    .line 79
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->nullableViewTypeAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 81
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 70
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p2, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .locals 20
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 31
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 32
    move-object v4, v2

    check-cast v4, Ljava/util/List;

    .line 35
    check-cast v2, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    .line 37
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v5, 0x0

    move-object/from16 v18, v2

    move-object v2, v3

    const/16 v17, 0x0

    .line 38
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 39
    iget-object v6, v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v6}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v6

    const/4 v7, 0x1

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 47
    :pswitch_0
    iget-object v6, v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->nullableViewTypeAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v6, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    move-object/from16 v18, v6

    const/16 v17, 0x1

    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v2, v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'nodeName\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 42
    :pswitch_2
    iget-object v4, v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->nullableListOfNodeMetadataAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v4, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    const/4 v5, 0x1

    goto :goto_0

    .line 40
    :pswitch_3
    iget-object v3, v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadataJsonAdapter;->stringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'nodeId\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 52
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 53
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 58
    new-instance v19, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-eqz v3, :cond_6

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xfb

    const/16 v16, 0x0

    move-object/from16 v6, v19

    move-object v9, v3

    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/util/List;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V

    .line 60
    new-instance v1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-eqz v5, :cond_3

    :goto_1
    move-object v12, v4

    goto :goto_2

    .line 62
    :cond_3
    invoke-virtual/range {v19 .. v19}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d()Ljava/util/List;

    move-result-object v4

    goto :goto_1

    :goto_2
    if-eqz v2, :cond_4

    :goto_3
    move-object v13, v2

    goto :goto_4

    .line 63
    :cond_4
    invoke-virtual/range {v19 .. v19}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->e()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :goto_4
    if-eqz v17, :cond_5

    move-object/from16 v14, v18

    goto :goto_5

    .line 64
    :cond_5
    invoke-virtual/range {v19 .. v19}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    move-result-object v2

    move-object v14, v2

    :goto_5
    const/16 v15, 0x1b

    const/16 v16, 0x0

    move-object v6, v1

    move-object v9, v3

    .line 60
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/util/List;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V

    return-object v1

    .line 59
    :cond_6
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Required property \'nodeId\' missing at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(NodeMetadata)"

    return-object v0
.end method
