.class public final enum Lcom/swedbank/mobile/architect/business/metadata/ViewType;
.super Ljava/lang/Enum;
.source "NodeMetadata.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/architect/business/metadata/ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/architect/business/metadata/ViewType;

.field public static final enum CONSUMING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

.field public static final enum CONTRIBUTING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

.field public static final enum MODAL_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

.field public static final enum ROOT_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    new-instance v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    const-string v2, "ROOT_VIEW"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/business/metadata/ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->ROOT_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    const-string v2, "CONTRIBUTING_VIEW"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/business/metadata/ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONTRIBUTING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    const-string v2, "CONSUMING_VIEW"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/business/metadata/ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONSUMING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    const-string v2, "MODAL_VIEW"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/business/metadata/ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->MODAL_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->$VALUES:[Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/architect/business/metadata/ViewType;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/architect/business/metadata/ViewType;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->$VALUES:[Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/architect/business/metadata/ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-object v0
.end method
