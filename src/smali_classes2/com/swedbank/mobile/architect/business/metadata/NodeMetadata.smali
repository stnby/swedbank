.class public final Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
.super Ljava/lang/Object;
.source "NodeMetadata.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/metadata/b;


# annotations
.annotation runtime Lcom/squareup/moshi/e;
    a = true
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final transient b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private transient c:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/architect/business/metadata/ViewType;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/util/List;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/metadata/ViewType;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/architect/business/c;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/metadata/ViewType;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/architect/business/metadata/ViewType;",
            ")V"
        }
    .end annotation

    const-string p1, "nodeId"

    invoke-static {p3, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "nodeName"

    invoke-static {p7, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->b:Ljava/lang/Class;

    iput-object p5, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->c:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    iput-object p6, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d:Ljava/util/List;

    iput-object p7, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->e:Ljava/lang/String;

    iput-object p8, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->f:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/util/List;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 36
    move-object v1, v2

    check-cast v1, Lcom/swedbank/mobile/architect/business/c;

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object v4, p1

    :goto_0
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    .line 37
    move-object v1, v2

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/f;

    move-object v5, v1

    goto :goto_1

    :cond_1
    move-object v5, p2

    :goto_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_3

    if-eqz v4, :cond_2

    .line 39
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v1, v2

    :goto_2
    move-object v7, v1

    goto :goto_3

    :cond_3
    move-object/from16 v7, p4

    :goto_3
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_4

    .line 40
    move-object v1, v2

    check-cast v1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-object v8, v1

    goto :goto_4

    :cond_4
    move-object/from16 v8, p5

    :goto_4
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_5

    .line 41
    move-object v1, v2

    check-cast v1, Ljava/util/List;

    move-object v9, v1

    goto :goto_5

    :cond_5
    move-object/from16 v9, p6

    :goto_5
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_7

    if-eqz v4, :cond_6

    .line 71
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "this::class\n      .java\n      .simpleName"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "InteractorImpl"

    .line 74
    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v1, v3}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    goto :goto_6

    :cond_6
    const-string v1, ""

    :goto_6
    move-object v10, v1

    goto :goto_7

    :cond_7
    move-object/from16 v10, p7

    :goto_7
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_d

    if-eqz v5, :cond_c

    .line 79
    :try_start_0
    instance-of v0, v5, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->ROOT_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    :goto_8
    move-object v2, v0

    goto :goto_9

    .line 80
    :cond_8
    instance-of v0, v5, Lcom/swedbank/mobile/architect/a/b/f$b;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONTRIBUTING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    goto :goto_8

    .line 81
    :cond_9
    instance-of v0, v5, Lcom/swedbank/mobile/architect/a/b/f$a;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONSUMING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    goto :goto_8

    .line 82
    :cond_a
    instance-of v0, v5, Lcom/swedbank/mobile/architect/a/b/f$c;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->MODAL_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    goto :goto_8

    :cond_b
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_c
    :goto_9
    move-object v11, v2

    goto :goto_a

    :cond_d
    move-object/from16 v11, p8

    :goto_a
    move-object v3, p0

    move-object v6, p3

    .line 87
    invoke-direct/range {v3 .. v11}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/util/List;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/metadata/ViewType;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/squareup/moshi/n;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "jsonMapper"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    const-class v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-virtual {p1, v0}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    .line 50
    invoke-virtual {p1, p0}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "jsonMapper\n      .adapte\u2026java)\n      .toJson(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 40
    iput-object p1, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->c:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    return-void
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->b:Ljava/lang/Class;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->c:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->f:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/swedbank/mobile/architect/business/metadata/b$a;->a(Lcom/swedbank/mobile/architect/business/metadata/b;)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/swedbank/mobile/architect/business/metadata/b$a;->c(Lcom/swedbank/mobile/architect/business/metadata/b;)Z

    move-result v0

    return v0
.end method
