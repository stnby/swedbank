.class public final Lcom/swedbank/mobile/architect/business/metadata/b$a;
.super Ljava/lang/Object;
.source "NodeMetadata.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/architect/business/metadata/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/architect/business/metadata/b;)Z
    .locals 1

    .line 23
    invoke-interface {p0}, Lcom/swedbank/mobile/architect/business/metadata/b;->f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    move-result-object p0

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->ROOT_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static b(Lcom/swedbank/mobile/architect/business/metadata/b;)Z
    .locals 1

    .line 25
    invoke-interface {p0}, Lcom/swedbank/mobile/architect/business/metadata/b;->f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    move-result-object p0

    sget-object v0, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONTRIBUTING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static c(Lcom/swedbank/mobile/architect/business/metadata/b;)Z
    .locals 0

    .line 31
    invoke-interface {p0}, Lcom/swedbank/mobile/architect/business/metadata/b;->f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
