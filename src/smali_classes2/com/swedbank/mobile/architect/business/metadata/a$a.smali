.class public final Lcom/swedbank/mobile/architect/business/metadata/a$a;
.super Lcom/swedbank/mobile/architect/business/metadata/a;
.source "AttachmentChange.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/architect/business/metadata/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/architect/business/metadata/ViewType;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/ViewType;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/architect/business/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/metadata/ViewType;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/metadata/ViewType;",
            ")V"
        }
    .end annotation

    const-string p2, "interactor"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "nodeId"

    invoke-static {p3, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "parentNodeId"

    invoke-static {p4, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "nodeName"

    invoke-static {p5, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "nodeType"

    invoke-static {p6, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 24
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/architect/business/metadata/a;-><init>(Lkotlin/e/b/g;)V

    iput-object p3, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->c:Ljava/lang/String;

    iput-object p6, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->d:Ljava/lang/Class;

    iput-object p7, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->e:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V
    .locals 8

    move-object v2, p2

    and-int/lit8 v1, p8, 0x10

    if-eqz v1, :cond_0

    .line 36
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "this::class\n      .java\n      .simpleName"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "InteractorImpl"

    .line 39
    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v1, v3}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, p5

    :goto_0
    and-int/lit8 v1, p8, 0x20

    if-eqz v1, :cond_1

    .line 22
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object v6, p6

    :goto_1
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    if-eqz v2, :cond_6

    .line 44
    :try_start_0
    instance-of v1, v2, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->ROOT_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    :goto_2
    move-object v0, v1

    goto :goto_3

    .line 45
    :cond_2
    instance-of v1, v2, Lcom/swedbank/mobile/architect/a/b/f$b;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONTRIBUTING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    goto :goto_2

    .line 46
    :cond_3
    instance-of v1, v2, Lcom/swedbank/mobile/architect/a/b/f$a;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->CONSUMING_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    goto :goto_2

    .line 47
    :cond_4
    instance-of v1, v2, Lcom/swedbank/mobile/architect/a/b/f$c;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/swedbank/mobile/architect/business/metadata/ViewType;->MODAL_VIEW:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    goto :goto_2

    :cond_5
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_6
    :goto_3
    move-object v7, v0

    goto :goto_4

    :cond_7
    move-object v7, p7

    :goto_4
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 52
    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/architect/business/metadata/a$a;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/ViewType;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/swedbank/mobile/architect/business/metadata/ViewType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/architect/business/metadata/a$a;->e:Lcom/swedbank/mobile/architect/business/metadata/ViewType;

    return-object v0
.end method
