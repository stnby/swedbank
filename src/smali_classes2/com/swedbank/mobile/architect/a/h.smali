.class public Lcom/swedbank/mobile/architect/a/h;
.super Ljava/lang/Object;
.source "Router.kt"


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Lio/reactivex/b/b;

.field private final f:Landroid/os/Handler;

.field private volatile g:Z

.field private final h:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private j:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/swedbank/mobile/architect/business/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;"
        }
    .end annotation
.end field

.field private final l:Lcom/swedbank/mobile/architect/a/b/g;

.field private final m:Lcom/swedbank/mobile/architect/a/b/f;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ")V"
        }
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/h;->l:Lcom/swedbank/mobile/architect/a/b/g;

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    .line 30
    new-instance p1, Lio/reactivex/b/b;

    invoke-direct {p1}, Lio/reactivex/b/b;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h;->e:Lio/reactivex/b/b;

    .line 31
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h;->f:Landroid/os/Handler;

    .line 37
    new-instance p1, Ljava/util/ArrayDeque;

    const/4 p2, 0x2

    invoke-direct {p1, p2}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h;->h:Ljava/util/ArrayDeque;

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UUID.randomUUID().toString()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 28
    check-cast p3, Lcom/swedbank/mobile/architect/a/b/f;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->d(Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->e(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->f(Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->g(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/architect/a/h;)Lcom/b/c/c;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/h;->j:Lcom/b/c/c;

    if-nez p0, :cond_0

    const-string v0, "attachmentChangeStream"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/h;->f:Landroid/os/Handler;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/architect/a/h;)Z
    .locals 0

    .line 25
    iget-boolean p0, p0, Lcom/swedbank/mobile/architect/a/h;->g:Z

    return p0
.end method


# virtual methods
.method public final a(Lcom/b/c/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1    # Lcom/b/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v6, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    const-string v4, "attachmentChangeStream"

    invoke-static {p1, v4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "parentNodeId"

    invoke-static {p2, v4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "directParentWithViewNodeId"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "rootViewParentNodeId"

    move-object/from16 v5, p4

    invoke-static {v5, v4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "closestRootViewNodeId"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    .line 188
    iput-boolean v4, v0, Lcom/swedbank/mobile/architect/a/h;->g:Z

    .line 189
    iput-object v1, v0, Lcom/swedbank/mobile/architect/a/h;->j:Lcom/b/c/c;

    .line 190
    iput-object v6, v0, Lcom/swedbank/mobile/architect/a/h;->a:Ljava/lang/String;

    .line 191
    iput-object v2, v0, Lcom/swedbank/mobile/architect/a/h;->b:Ljava/lang/String;

    .line 192
    iget-object v4, v0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    .line 193
    instance-of v4, v4, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    move-object v11, v4

    goto :goto_0

    :cond_0
    move-object v11, v5

    .line 196
    :goto_0
    iput-object v11, v0, Lcom/swedbank/mobile/architect/a/h;->c:Ljava/lang/String;

    .line 197
    iput-object v3, v0, Lcom/swedbank/mobile/architect/a/h;->d:Ljava/lang/String;

    .line 198
    iget-object v8, v0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    if-eqz v8, :cond_1

    .line 199
    iget-object v7, v0, Lcom/swedbank/mobile/architect/a/h;->l:Lcom/swedbank/mobile/architect/a/b/g;

    .line 201
    iget-object v9, v0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    move-object/from16 v10, p3

    move-object/from16 v12, p5

    .line 199
    invoke-virtual/range {v7 .. v12}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_1
    new-instance v12, Lcom/swedbank/mobile/architect/business/metadata/a$a;

    .line 207
    iget-object v4, v0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    .line 208
    iget-object v3, v0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    .line 209
    iget-object v5, v0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x70

    const/4 v11, 0x0

    move-object v2, v12

    move-object v6, p2

    .line 206
    invoke-direct/range {v2 .. v11}, Lcom/swedbank/mobile/architect/business/metadata/a$a;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V

    invoke-virtual {p1, v12}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 212
    iget-object v1, v0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    invoke-virtual {v1, p0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public final a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->e:Lio/reactivex/b/b;

    invoke-virtual {v0, p1}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 250
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Disposable added after router detach - it will be disposed"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected final b(Lkotlin/e/a/b;)V
    .locals 3
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 286
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/a/h$g;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p0, p1}, Lcom/swedbank/mobile/architect/a/h$g;-><init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method protected final c(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 300
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 69
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->h(Lcom/swedbank/mobile/architect/a/h;)Lcom/b/c/c;

    move-result-object v2

    .line 70
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->m()Ljava/lang/String;

    move-result-object v3

    .line 72
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->u()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->m()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v4, v0

    goto :goto_1

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :goto_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->o()Ljava/lang/String;

    move-result-object v5

    .line 76
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->u()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    .line 77
    instance-of v0, v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->m()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v6, v0

    goto :goto_3

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :goto_3
    move-object v1, p1

    .line 68
    invoke-virtual/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/b/c/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 301
    :cond_2
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 302
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/a/h$a;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p0, p1}, Lcom/swedbank/mobile/architect/a/h$a;-><init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    :goto_4
    return-void
.end method

.method protected final c(Lkotlin/e/a/b;)V
    .locals 3
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "predicate"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 355
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 143
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 144
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    const-string v2, "next"

    .line 145
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 147
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/h;->s()V

    goto :goto_0

    .line 359
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/a/h$e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, p0, p1}, Lcom/swedbank/mobile/architect/a/h$e;-><init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method protected final d(Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lcom/swedbank/mobile/architect/business/d;",
            ">(",
            "Lcom/swedbank/mobile/architect/a/h;",
            ")TR;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;)V

    .line 89
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    return-object p1
.end method

.method protected final e(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;)V

    .line 99
    new-instance v0, Lcom/swedbank/mobile/architect/a/h$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/architect/a/h$c;-><init>(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lkotlin/e/a/b;)V

    goto :goto_0

    .line 316
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/a/h$b;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p0, p1}, Lcom/swedbank/mobile/architect/a/h$b;-><init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method protected final f(Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lcom/swedbank/mobile/architect/business/d;",
            ">(",
            "Lcom/swedbank/mobile/architect/a/h;",
            ")TR;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->e(Lcom/swedbank/mobile/architect/a/h;)V

    .line 109
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    return-object p1
.end method

.method protected final g(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 327
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->s()V

    goto :goto_0

    .line 118
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " was not attached"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 331
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/a/h$f;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, p0, p1}, Lcom/swedbank/mobile/architect/a/h$f;-><init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public final l()Ljava/util/ArrayDeque;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayDeque<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->h:Ljava/util/ArrayDeque;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v1, "directParentWithViewNodeId"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v1, "rootViewParentNodeId"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v1, "closestRootViewNodeId"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final q()Lcom/swedbank/mobile/architect/business/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lcom/swedbank/mobile/architect/business/d;",
            ">()TR;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/swedbank/mobile/architect/business/d;

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type R"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final r()V
    .locals 3

    .line 341
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    .line 130
    :goto_0
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 133
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/h;->s()V

    goto :goto_0

    .line 345
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/h;->i(Lcom/swedbank/mobile/architect/a/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/architect/a/h$d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, p0}, Lcom/swedbank/mobile/architect/a/h$d;-><init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public final s()V
    .locals 12

    const/4 v0, 0x0

    .line 217
    iput-boolean v0, p0, Lcom/swedbank/mobile/architect/a/h;->g:Z

    .line 218
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->r()V

    .line 219
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/c;->l_()V

    .line 220
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/h;->l:Lcom/swedbank/mobile/architect/a/b/g;

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/f;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->e:Lio/reactivex/b/b;

    invoke-virtual {v0}, Lio/reactivex/b/b;->a()V

    .line 222
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->j:Lcom/b/c/c;

    if-nez v0, :cond_1

    const-string v1, "attachmentChangeStream"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/swedbank/mobile/architect/business/metadata/a$b;

    .line 223
    iget-object v4, p0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    .line 224
    iget-object v3, p0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    .line 225
    iget-object v5, p0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    .line 226
    iget-object v6, p0, Lcom/swedbank/mobile/architect/a/h;->a:Ljava/lang/String;

    if-nez v6, :cond_2

    const-string v2, "parentNodeId"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x70

    const/4 v11, 0x0

    move-object v2, v1

    .line 222
    invoke-direct/range {v2 .. v11}, Lcom/swedbank/mobile/architect/business/metadata/a$b;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final t()Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .locals 12
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 268
    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/h;->k:Lcom/swedbank/mobile/architect/business/c;

    .line 269
    iget-object v2, p0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    .line 270
    iget-object v3, p0, Lcom/swedbank/mobile/architect/a/h;->i:Ljava/lang/String;

    .line 397
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    .line 398
    move-object v4, v0

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v5

    :goto_0
    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Iterable;

    .line 400
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v0, v5}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 401
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 402
    check-cast v5, Lcom/swedbank/mobile/architect/a/h;

    .line 399
    invoke-virtual {v5}, Lcom/swedbank/mobile/architect/a/h;->t()Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 403
    :cond_1
    check-cast v4, Ljava/util/List;

    move-object v6, v4

    goto :goto_2

    :cond_2
    move-object v6, v5

    :goto_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xd8

    const/4 v10, 0x0

    .line 267
    new-instance v11, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/Class;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/util/List;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/metadata/ViewType;ILkotlin/e/b/g;)V

    .line 273
    invoke-virtual {v11}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Ljava/lang/Iterable;

    .line 404
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    .line 274
    invoke-virtual {v1, v11}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V

    goto :goto_3

    :cond_3
    return-object v11
.end method

.method public final u()Lcom/swedbank/mobile/architect/a/b/f;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h;->m:Lcom/swedbank/mobile/architect/a/b/f;

    return-object v0
.end method
