.class public final Lcom/swedbank/mobile/architect/a/a/a;
.super Ljava/lang/Object;
.source "FlowManagerImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/a/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RootNode::",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/a/c<",
        "TRootNode;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/architect/business/a/a<",
            "TRootNode;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/swedbank/mobile/architect/business/a/f<",
            "***>;>;)V"
        }
    .end annotation

    const-string v0, "flows"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/a/a;->b:Ljava/util/Map;

    .line 18
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Flow\u2026utionRequest<RootNode>>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/a/a;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/a/a;)Ljava/util/Map;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/a/a;->b:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/architect/business/a/a<",
            "TRootNode;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/a/a;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/architect/business/a/b;)V
    .locals 5
    .param p1    # Lcom/swedbank/mobile/architect/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "flowInput"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 42
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/a/a;->a(Lcom/swedbank/mobile/architect/a/a/a;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/f;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_1

    .line 43
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/a/a;->a(Lcom/swedbank/mobile/architect/a/a/a;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 48
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/Class;

    .line 45
    invoke-virtual {v4, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_2
    move-object v3, v2

    .line 49
    :goto_0
    check-cast v3, Ljava/lang/Class;

    if-eqz v3, :cond_3

    .line 46
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/a/a;->a(Lcom/swedbank/mobile/architect/a/a/a;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/f;

    goto :goto_1

    :cond_3
    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/a/a;->a:Lcom/b/c/c;

    new-instance v2, Lcom/swedbank/mobile/architect/business/a/a;

    invoke-direct {v2, v1, p1}, Lcom/swedbank/mobile/architect/business/a/a;-><init>(Lcom/swedbank/mobile/architect/business/a/f;Lcom/swedbank/mobile/architect/business/a/b;)V

    invoke-virtual {v0, v2}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void

    .line 28
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No flow defined for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
