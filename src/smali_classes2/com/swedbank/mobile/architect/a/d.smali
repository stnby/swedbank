.class public abstract Lcom/swedbank/mobile/architect/a/d;
.super Lcom/swedbank/mobile/architect/a/e;
.source "Presenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/architect/a/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/swedbank/mobile/architect/a/b/i<",
        "-TVS;>;VS:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/swedbank/mobile/architect/a/e;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/k/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/a<",
            "TVS;>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Lio/reactivex/b/c;

.field private d:Lio/reactivex/b/c;

.field private e:Lio/reactivex/b/b;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/swedbank/mobile/architect/a/d$a<",
            "TV;*>;>;"
        }
    .end annotation
.end field

.field private volatile g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/e;-><init>()V

    .line 34
    invoke-static {}, Lio/reactivex/k/a;->a()Lio/reactivex/k/a;

    move-result-object v0

    const-string v1, "BehaviorSubject.create()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->a:Lio/reactivex/k/a;

    .line 36
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->c:Lio/reactivex/b/c;

    .line 37
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->d:Lio/reactivex/b/c;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/d;)Lio/reactivex/b/b;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/d;->e:Lio/reactivex/b/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/b/b;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/d;->e:Lio/reactivex/b/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V
    .locals 0

    .line 33
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/d;->a(Lio/reactivex/o;)V

    return-void
.end method


# virtual methods
.method protected final a(Lkotlin/e/a/b;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/e/a/b<",
            "-TV;+",
            "Lio/reactivex/o<",
            "TA;>;>;)",
            "Lio/reactivex/o<",
            "TA;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "binder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {}, Lio/reactivex/k/c;->a()Lio/reactivex/k/c;

    move-result-object v0

    const-string v1, "PublishSubject.create<A>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/d;->f:Ljava/util/ArrayList;

    new-instance v2, Lcom/swedbank/mobile/architect/a/d$a;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/architect/a/d$a;-><init>(Lio/reactivex/k/c;Lkotlin/e/a/b;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method protected abstract a()V
.end method

.method public a(Lcom/swedbank/mobile/architect/a/b/b;)V
    .locals 8
    .param p1    # Lcom/swedbank/mobile/architect/a/b/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/i;

    .line 87
    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/d;->f:Ljava/util/ArrayList;

    .line 88
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 90
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    const-string v5, "binders[i]"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/swedbank/mobile/architect/a/d$a;

    .line 127
    invoke-virtual {v4}, Lcom/swedbank/mobile/architect/a/d$a;->a()Lio/reactivex/k/c;

    move-result-object v5

    .line 128
    invoke-virtual {v4}, Lcom/swedbank/mobile/architect/a/d$a;->b()Lkotlin/e/a/b;

    move-result-object v4

    .line 130
    invoke-interface {v4, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/reactivex/o;

    .line 132
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;)Lio/reactivex/b/b;

    move-result-object v6

    if-eqz v6, :cond_0

    goto :goto_1

    :cond_0
    new-instance v6, Lio/reactivex/b/b;

    invoke-direct {v6}, Lio/reactivex/b/b;-><init>()V

    .line 133
    :goto_1
    invoke-static {p0, v6}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/b/b;)V

    .line 135
    new-instance v7, Lcom/swedbank/mobile/architect/a/a;

    invoke-direct {v7, v5}, Lcom/swedbank/mobile/architect/a/a;-><init>(Lio/reactivex/k/c;)V

    check-cast v7, Lio/reactivex/u;

    invoke-virtual {v4, v7}, Lio/reactivex/o;->e(Lio/reactivex/u;)Lio/reactivex/u;

    move-result-object v4

    check-cast v4, Lio/reactivex/b/c;

    invoke-virtual {v6, v4}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/d;->d:Lio/reactivex/b/c;

    invoke-interface {v1}, Lio/reactivex/b/c;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 98
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/d;->a:Lio/reactivex/k/a;

    .line 137
    sget-object v1, Lcom/swedbank/mobile/architect/a/f;->b:Lio/reactivex/v;

    if-eqz v1, :cond_2

    goto :goto_2

    .line 138
    :cond_2
    invoke-static {}, Lcom/swedbank/mobile/architect/a/f;->a()Lio/reactivex/v;

    move-result-object v1

    .line 99
    :goto_2
    invoke-virtual {p1, v1}, Lio/reactivex/k/a;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object p1

    .line 100
    new-instance v1, Lcom/swedbank/mobile/architect/a/d$b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/architect/a/d$b;-><init>(Lcom/swedbank/mobile/architect/a/b/i;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v0, Lcom/swedbank/mobile/architect/a/g;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/architect/a/g;-><init>(Lkotlin/e/a/b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "viewStateSubject\n       \u2026 .subscribe(view::render)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/d;->d:Lio/reactivex/b/c;

    return-void

    .line 94
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tried to attach view twice. This is an architect bug!\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Presenter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "New attached view "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected final a(Lio/reactivex/o;)V
    .locals 2
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "TVS;>;)V"
        }
    .end annotation

    const-string v0, "viewStateObservable"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/d;->b:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 60
    iput-boolean v1, p0, Lcom/swedbank/mobile/architect/a/d;->b:Z

    .line 62
    new-instance v0, Lcom/swedbank/mobile/architect/a/b;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/d;->a:Lio/reactivex/k/a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/architect/a/b;-><init>(Lio/reactivex/k/a;)V

    check-cast v0, Lio/reactivex/u;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->e(Lio/reactivex/u;)Lio/reactivex/u;

    move-result-object p1

    const-string v0, "viewStateObservable.subs\u2026server(viewStateSubject))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/b/c;

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/d;->c:Lio/reactivex/b/c;

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "subscribeViewState() method is only allowed to be called once"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method protected final e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Lcom/swedbank/mobile/architect/a/d;->g:Z

    .line 72
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/d;->a()V

    return-void
.end method

.method public g()V
    .locals 1

    const/4 v0, 0x0

    .line 76
    iput-boolean v0, p0, Lcom/swedbank/mobile/architect/a/d;->g:Z

    .line 77
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/d;->e()V

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->c:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public h()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/d;->g:Z

    return v0
.end method

.method public i()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->d:Lio/reactivex/b/c;

    invoke-interface {v0}, Lio/reactivex/b/c;->a()V

    .line 105
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->e:Lio/reactivex/b/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/reactivex/b/b;->a()V

    :cond_0
    const/4 v0, 0x0

    .line 106
    check-cast v0, Lio/reactivex/b/b;

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/d;->e:Lio/reactivex/b/b;

    return-void
.end method
