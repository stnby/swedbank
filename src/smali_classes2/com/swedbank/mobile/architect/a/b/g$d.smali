.class public final Lcom/swedbank/mobile/architect/a/b/g$d;
.super Ljava/lang/Object;
.source "ViewManager.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/b/g;->a(Landroid/view/ViewGroup;Ljava/lang/String;Landroid/os/Bundle;Landroid/view/Window;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/b/g;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Landroid/view/Window;

.field final synthetic e:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/architect/a/b/g;Landroid/view/ViewGroup;Ljava/lang/String;Landroid/view/Window;Landroid/os/Bundle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/String;",
            "Landroid/view/Window;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .line 76
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->a:Lcom/swedbank/mobile/architect/a/b/g;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->d:Landroid/view/Window;

    iput-object p5, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->e:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->a:Lcom/swedbank/mobile/architect/a/b/g;

    new-instance v0, Lcom/swedbank/mobile/architect/a/b/g$c;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/architect/a/b/g$c;-><init>(Landroid/view/ViewGroup;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/g$c;)V

    .line 79
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->a:Lcom/swedbank/mobile/architect/a/b/g;

    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/g;->b(Lcom/swedbank/mobile/architect/a/b/g;)V

    .line 82
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->d:Landroid/view/Window;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->e:Landroid/os/Bundle;

    if-eqz p1, :cond_0

    .line 83
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->e:Landroid/os/Bundle;

    const-string v0, "android:viewHierarchyState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->d:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->restoreHierarchyState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->b:Landroid/view/ViewGroup;

    move-object v0, p0

    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 90
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$d;->a:Lcom/swedbank/mobile/architect/a/b/g;

    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/g;->c(Lcom/swedbank/mobile/architect/a/b/g;)V

    return-void
.end method
