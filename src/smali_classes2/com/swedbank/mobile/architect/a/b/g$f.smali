.class final Lcom/swedbank/mobile/architect/a/b/g$f;
.super Lkotlin/e/b/k;
.source "ViewManager.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/view/ViewGroup;",
        "Landroidx/c/b<",
        "Lcom/swedbank/mobile/architect/a/b/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/b/f;

.field final synthetic b:Lcom/swedbank/mobile/architect/a/b/g$c;

.field final synthetic c:Lcom/swedbank/mobile/architect/a/b/g;

.field final synthetic d:Lcom/swedbank/mobile/architect/a/b/f;

.field final synthetic e:Z

.field final synthetic f:Lcom/swedbank/mobile/architect/a/b/d;

.field final synthetic g:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ZLcom/swedbank/mobile/architect/a/b/d;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->a:Lcom/swedbank/mobile/architect/a/b/f;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->b:Lcom/swedbank/mobile/architect/a/b/g$c;

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->c:Lcom/swedbank/mobile/architect/a/b/g;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->d:Lcom/swedbank/mobile/architect/a/b/f;

    iput-boolean p5, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->e:Z

    iput-object p6, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->f:Lcom/swedbank/mobile/architect/a/b/d;

    iput-boolean p7, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->g:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroidx/c/b;
    .locals 9
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroidx/c/b<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "newRootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->c:Lcom/swedbank/mobile/architect/a/b/g;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->a:Lcom/swedbank/mobile/architect/a/b/f;

    .line 594
    new-instance v2, Ljava/util/ArrayDeque;

    invoke-direct {v2}, Ljava/util/ArrayDeque;-><init>()V

    .line 595
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g;->a()Ljava/util/ArrayDeque;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/a/b/d;

    .line 596
    invoke-virtual {v3}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v4

    if-ne v4, v1, :cond_0

    goto :goto_1

    .line 599
    :cond_0
    invoke-virtual {v2, v3}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    .line 601
    :cond_1
    :goto_1
    check-cast v2, Ljava/lang/Iterable;

    .line 602
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 611
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 610
    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/architect/a/b/d;

    const-string v2, "it"

    .line 601
    invoke-static {v4, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object v3, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->c:Lcom/swedbank/mobile/architect/a/b/g;

    .line 215
    iget-object v6, p0, Lcom/swedbank/mobile/architect/a/b/g$f;->b:Lcom/swedbank/mobile/architect/a/b/g$c;

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v5, p1

    .line 213
    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/g$a;->a()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 610
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 613
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 219
    new-instance p1, Landroidx/c/b;

    invoke-direct {p1}, Landroidx/c/b;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 614
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 615
    check-cast v1, Ljava/util/Set;

    .line 219
    check-cast v1, Ljava/lang/Iterable;

    .line 616
    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_3

    .line 618
    :cond_4
    check-cast p1, Landroidx/c/b;

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/b/g$f;->a(Landroid/view/ViewGroup;)Landroidx/c/b;

    move-result-object p1

    return-object p1
.end method
