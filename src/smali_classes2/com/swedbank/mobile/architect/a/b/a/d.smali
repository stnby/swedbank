.class public final Lcom/swedbank/mobile/architect/a/b/a/d;
.super Ljava/lang/Object;
.source "SwapTransition.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/b/a/f;


# static fields
.field public static final a:Lcom/swedbank/mobile/architect/a/b/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/a/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/architect/a/b/a/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 11
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->a(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string p4, "container"

    invoke-static {p1, p4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "transitionCompleted"

    invoke-static {p5, p4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 18
    invoke-static {p1, p3}, Lcom/swedbank/mobile/architect/c/a;->a(Landroid/view/ViewGroup;Landroid/view/View;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 20
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    if-nez p3, :cond_1

    .line 21
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 23
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 24
    invoke-interface {p5}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    goto :goto_0

    .line 26
    :cond_2
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/a/d$a;

    invoke-direct {p2, p5}, Lcom/swedbank/mobile/architect/a/b/a/d$a;-><init>(Lkotlin/e/a/a;)V

    check-cast p2, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    :goto_0
    return-void
.end method

.method public b()V
    .locals 0

    .line 11
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->b(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method
