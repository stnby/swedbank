.class public final Lcom/swedbank/mobile/architect/a/b/f$d;
.super Lcom/swedbank/mobile/architect/a/b/f;
.source "ViewDetails.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/b/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/architect/a/b/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/swedbank/mobile/architect/a/b/a/e;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/architect/a/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;)V
    .locals 1
    .param p2    # Lcom/swedbank/mobile/architect/a/b/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/swedbank/mobile/architect/a/b/a/e;",
            "Lcom/swedbank/mobile/architect/a/b/c;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;+",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "transition"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priority"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenters"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, p4, p5, v0}, Lcom/swedbank/mobile/architect/a/b/f;-><init>(Ljava/util/Map;Ljavax/inject/Provider;Lkotlin/e/b/g;)V

    iput p1, p0, Lcom/swedbank/mobile/architect/a/b/f$d;->a:I

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/f$d;->b:Lcom/swedbank/mobile/architect/a/b/a/e;

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/b/f$d;->c:Lcom/swedbank/mobile/architect/a/b/c;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V
    .locals 7

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    .line 68
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/a/e;

    const/4 p7, 0x3

    const/4 v0, 0x0

    invoke-direct {p2, v0, v0, p7, v0}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;ILkotlin/e/b/g;)V

    :cond_0
    move-object v3, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    .line 69
    sget-object p3, Lcom/swedbank/mobile/architect/a/b/c;->a:Lcom/swedbank/mobile/architect/a/b/c;

    :cond_1
    move-object v4, p3

    move-object v1, p0

    move v2, p1

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/b/f$d;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Lcom/swedbank/mobile/architect/a/b/c;Ljava/util/Map;Ljavax/inject/Provider;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;ZLjava/lang/String;)Landroid/view/ViewGroup;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "into"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget v0, p0, Lcom/swedbank/mobile/architect/a/b/f$d;->a:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 80
    sget v0, Lcom/swedbank/mobile/architect/a$a;->architect_node_id:I

    invoke-virtual {p2, v0, p4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    if-eqz p3, :cond_0

    .line 82
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 84
    check-cast p2, Landroid/view/ViewGroup;

    return-object p2

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    check-cast p1, Landroid/view/View;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/architect/c/a;->a(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    return-object p1
.end method

.method public a()Lcom/swedbank/mobile/architect/a/b/a/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/f$d;->b:Lcom/swedbank/mobile/architect/a/b/a/e;

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/architect/a/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/f$d;->c:Lcom/swedbank/mobile/architect/a/b/c;

    return-object v0
.end method
