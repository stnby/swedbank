.class public final Lcom/swedbank/mobile/architect/a/b/g$e;
.super Ljava/lang/Object;
.source "ViewManager.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/b/f;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Ljava/util/Set;

.field final synthetic d:Landroid/view/ViewGroup;

.field final synthetic e:Lcom/swedbank/mobile/architect/a/b/g;

.field final synthetic f:Lcom/swedbank/mobile/architect/a/b/d;

.field final synthetic g:Lcom/swedbank/mobile/architect/a/b/g$c;

.field final synthetic h:Landroid/view/ViewGroup;

.field final synthetic i:Z

.field final synthetic j:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/architect/a/b/f;Landroid/view/ViewGroup;Ljava/util/Set;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Lcom/swedbank/mobile/architect/a/b/g$c;Landroid/view/ViewGroup;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->a:Lcom/swedbank/mobile/architect/a/b/f;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->c:Ljava/util/Set;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->d:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->e:Lcom/swedbank/mobile/architect/a/b/g;

    iput-object p6, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->f:Lcom/swedbank/mobile/architect/a/b/d;

    iput-object p7, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->g:Lcom/swedbank/mobile/architect/a/b/g$c;

    iput-object p8, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->h:Landroid/view/ViewGroup;

    iput-boolean p9, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->i:Z

    iput-boolean p10, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->j:Z

    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->a:Lcom/swedbank/mobile/architect/a/b/f;

    instance-of v0, v0, Lcom/swedbank/mobile/architect/a/b/f$a;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->b:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->c:Ljava/util/Set;

    const-string v1, "views"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 594
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/b;

    .line 367
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/architect/a/b/b;->a(Landroid/view/View;)V

    goto :goto_0

    .line 368
    :cond_1
    iget-boolean p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->j:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->c:Ljava/util/Set;

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 596
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/b;

    .line 368
    invoke-interface {v0}, Lcom/swedbank/mobile/architect/a/b/b;->m()V

    goto :goto_1

    .line 369
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->a:Lcom/swedbank/mobile/architect/a/b/f;

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->c:Ljava/util/Set;

    const-string v1, "views"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 598
    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/f;->a(Lcom/swedbank/mobile/architect/a/b/f;)Ljava/util/Map;

    move-result-object p1

    .line 599
    check-cast v0, Ljava/lang/Iterable;

    .line 600
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/b;

    .line 607
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/e;

    if-eqz v2, :cond_3

    goto :goto_4

    .line 609
    :cond_3
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 610
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/Class;

    .line 609
    invoke-virtual {v4, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_7

    check-cast v3, Ljava/lang/Class;

    .line 614
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    check-cast v2, Lcom/swedbank/mobile/architect/a/e;

    .line 369
    :goto_4
    invoke-virtual {v2, v1}, Lcom/swedbank/mobile/architect/a/e;->a(Lcom/swedbank/mobile/architect/a/b/b;)V

    goto :goto_2

    .line 614
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 612
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Missing presenter for view = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 609
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_8
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->d:Landroid/view/ViewGroup;

    move-object v0, p0

    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 374
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->a:Lcom/swedbank/mobile/architect/a/b/f;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/f;->b()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 619
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/e;

    .line 374
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/e;->i()V

    goto :goto_0

    .line 375
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g$e;->c:Ljava/util/Set;

    const-string v0, "views"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 621
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/b;

    .line 375
    invoke-interface {v0}, Lcom/swedbank/mobile/architect/a/b/b;->l()V

    goto :goto_1

    :cond_1
    return-void
.end method
