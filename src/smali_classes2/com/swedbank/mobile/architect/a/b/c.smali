.class public final enum Lcom/swedbank/mobile/architect/a/b/c;
.super Ljava/lang/Enum;
.source "ViewDetails.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/architect/a/b/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/architect/a/b/c;

.field public static final enum b:Lcom/swedbank/mobile/architect/a/b/c;

.field public static final enum c:Lcom/swedbank/mobile/architect/a/b/c;

.field private static final synthetic d:[Lcom/swedbank/mobile/architect/a/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/architect/a/b/c;

    new-instance v1, Lcom/swedbank/mobile/architect/a/b/c;

    const-string v2, "NORMAL"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/a/b/c;->a:Lcom/swedbank/mobile/architect/a/b/c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/architect/a/b/c;

    const-string v2, "MEDIUM"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/a/b/c;->b:Lcom/swedbank/mobile/architect/a/b/c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/architect/a/b/c;

    const-string v2, "HIGH"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/architect/a/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/architect/a/b/c;->c:Lcom/swedbank/mobile/architect/a/b/c;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/architect/a/b/c;->d:[Lcom/swedbank/mobile/architect/a/b/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/architect/a/b/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/architect/a/b/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/a/b/c;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/architect/a/b/c;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/architect/a/b/c;->d:[Lcom/swedbank/mobile/architect/a/b/c;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/architect/a/b/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/architect/a/b/c;

    return-object v0
.end method
