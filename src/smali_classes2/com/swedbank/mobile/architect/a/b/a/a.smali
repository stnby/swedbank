.class public abstract Lcom/swedbank/mobile/architect/a/b/a/a;
.super Ljava/lang/Object;
.source "AndroidTransitionHandler.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/b/a/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)Landroidx/l/n;
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public a()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->a(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transitionCompleted"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/architect/a/b/a/a;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)Landroidx/l/n;

    move-result-object p4

    .line 21
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/a/a$a;

    invoke-direct {v0, p5}, Lcom/swedbank/mobile/architect/a/b/a/a$a;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Landroidx/l/n$d;

    invoke-virtual {p4, v0}, Landroidx/l/n;->addListener(Landroidx/l/n$d;)Landroidx/l/n;

    .line 37
    invoke-static {p1, p4}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    if-eqz p3, :cond_0

    .line 39
    invoke-static {p1, p3}, Lcom/swedbank/mobile/architect/c/a;->a(Landroid/view/ViewGroup;Landroid/view/View;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 41
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    if-nez p3, :cond_1

    .line 42
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->b(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method
