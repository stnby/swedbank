.class public abstract Lcom/swedbank/mobile/architect/a/b/a/b;
.super Ljava/lang/Object;
.source "AnimatorTransitionHandler.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/b/a/f;


# instance fields
.field private a:Landroid/animation/Animator;

.field private b:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field private volatile d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/b/a/b;)Lkotlin/e/a/a;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->b:Lkotlin/e/a/a;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/b/a/b;Lkotlin/e/a/a;)V
    .locals 0

    .line 10
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->b:Lkotlin/e/a/a;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/architect/a/b/a/b;)Z
    .locals 0

    .line 10
    iget-boolean p0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->c:Z

    return p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/architect/a/b/a/b;)Landroid/animation/Animator;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->a:Landroid/animation/Animator;

    return-object p0
.end method


# virtual methods
.method protected abstract a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;Z)Landroid/animation/Animator;
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x1

    .line 114
    iput-boolean v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->c:Z

    .line 115
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->a(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 117
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->b:Lkotlin/e/a/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/s;

    :cond_1
    return-void
.end method

.method protected abstract a(Landroid/view/View;)V
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V
    .locals 16
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    const-string v0, "container"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transitionCompleted"

    move-object/from16 v12, p5

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/a/b$b;

    invoke-direct {v0, v8, v11, v9}, Lcom/swedbank/mobile/architect/a/b/a/b$b;-><init>(Lcom/swedbank/mobile/architect/a/b/a/b;Landroid/view/View;Landroid/view/ViewGroup;)V

    check-cast v0, Lkotlin/e/a/a;

    iput-object v0, v8, Lcom/swedbank/mobile/architect/a/b/a/b;->b:Lkotlin/e/a/a;

    const/4 v0, 0x1

    if-eqz v10, :cond_0

    .line 36
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v14, 0x1

    goto :goto_0

    :cond_0
    const/4 v14, 0x0

    :goto_0
    if-eqz v10, :cond_4

    if-eqz v14, :cond_3

    if-nez p4, :cond_2

    if-nez v11, :cond_1

    goto :goto_1

    .line 42
    :cond_1
    invoke-virtual {v9, v11}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v9, v10, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_2

    .line 40
    :cond_2
    :goto_1
    invoke-virtual/range {p1 .. p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 45
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-gtz v1, :cond_4

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-gtz v1, :cond_4

    .line 47
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    new-instance v15, Lcom/swedbank/mobile/architect/a/b/a/b$c;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move v4, v14

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object v13, v7

    move-object/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/a/b$c;-><init>(Lcom/swedbank/mobile/architect/a/b/a/b;Landroid/view/View;Landroid/view/ViewGroup;ZLandroid/view/View;ZLkotlin/e/a/a;)V

    move-object v0, v15

    check-cast v0, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v13, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    const/4 v0, 0x0

    goto :goto_3

    .line 56
    :cond_3
    invoke-virtual {v8, v10}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/View;)V

    :cond_4
    :goto_3
    if-eqz v0, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move v3, v14

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    .line 61
    invoke-virtual/range {v0 .. v6}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;ZLkotlin/e/a/a;)V

    :cond_5
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;ZLkotlin/e/a/a;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p6    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Z",
            "Landroid/view/View;",
            "Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transitionCompleted"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->c:Z

    if-eqz v0, :cond_0

    return-void

    .line 75
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;Z)Landroid/animation/Animator;

    move-result-object p1

    .line 76
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/a/b$a;

    invoke-direct {p2, p0, p6, p5, p4}, Lcom/swedbank/mobile/architect/a/b/a/b$a;-><init>(Lcom/swedbank/mobile/architect/a/b/a/b;Lkotlin/e/a/a;ZLandroid/view/View;)V

    check-cast p2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p1, p2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 98
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->a:Landroid/animation/Animator;

    .line 99
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method public final a(Lkotlin/e/a/a;Landroid/animation/AnimatorListenerAdapter;)V
    .locals 1
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/animation/AnimatorListenerAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;",
            "Landroid/animation/AnimatorListenerAdapter;",
            ")V"
        }
    .end annotation

    const-string v0, "transitionCompleted"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animatorListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 104
    iput-boolean v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->d:Z

    .line 105
    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->a:Landroid/animation/Animator;

    const/4 v0, 0x0

    .line 109
    check-cast v0, Landroid/animation/Animator;

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->a:Landroid/animation/Animator;

    if-eqz p1, :cond_1

    .line 110
    check-cast p2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p1, p2}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->b(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    .line 124
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b;->b:Lkotlin/e/a/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/s;

    :cond_1
    return-void
.end method
