.class public final Lcom/swedbank/mobile/architect/a/b/a/e;
.super Ljava/lang/Object;
.source "TransitionDetails.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/architect/a/b/a/e$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/architect/a/b/a/e$a;


# instance fields
.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/architect/a/b/a/f;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/a/b/a/f;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/swedbank/mobile/architect/a/b/a/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/e$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/architect/a/b/a/e;->a:Lcom/swedbank/mobile/architect/a/b/a/e$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/b/a/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/a/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "enter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "leave"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    .line 7
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UUID.randomUUID().toString()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->b:Ljava/lang/String;

    .line 10
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    instance-of p1, p1, Lcom/swedbank/mobile/architect/a/b/a/b;

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    instance-of p1, p1, Lcom/swedbank/mobile/architect/a/b/a/b;

    if-eqz p1, :cond_2

    .line 11
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    iget-object p2, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    if-eq p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    :cond_2
    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Enter and leave transitions cannot have the same object reference if either of them is AnimatorTransitionHandler"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 5
    sget-object p1, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    check-cast p1, Lcom/swedbank/mobile/architect/a/b/a/f;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 6
    sget-object p2, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/a/f;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/architect/a/b/a/f;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    return-object v0
.end method

.method public final c()Lcom/swedbank/mobile/architect/a/b/a/f;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 6
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/architect/a/b/a/e;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/architect/a/b/a/e;

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    iget-object v1, p1, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    iget-object p1, p1, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransitionDetails(enter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->c:Lcom/swedbank/mobile/architect/a/b/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", leave="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/b/a/e;->d:Lcom/swedbank/mobile/architect/a/b/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
