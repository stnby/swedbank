.class public final Lcom/swedbank/mobile/architect/a/b/g;
.super Ljava/lang/Object;
.source "ViewManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/architect/a/b/g$c;,
        Lcom/swedbank/mobile/architect/a/b/g$a;,
        Lcom/swedbank/mobile/architect/a/b/g$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/architect/a/b/g$c;

.field private d:Lcom/swedbank/mobile/architect/a/b/d;

.field private e:Lcom/swedbank/mobile/architect/a/b/g$b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayDeque;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    .line 53
    new-instance v0, Ljava/util/ArrayDeque;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->b:Ljava/util/ArrayDeque;

    return-void
.end method

.method private final a(Lcom/swedbank/mobile/architect/a/b/d;Lcom/swedbank/mobile/architect/a/b/g$c;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/architect/a/b/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 388
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    .line 389
    instance-of v1, v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v1, :cond_0

    goto :goto_1

    .line 390
    :cond_0
    instance-of v0, v0, Lcom/swedbank/mobile/architect/a/b/f$c;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object p3

    goto :goto_1

    .line 392
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/d;->c()Ljava/lang/String;

    move-result-object p1

    .line 393
    iget-object p2, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    check-cast p2, Ljava/lang/Iterable;

    .line 651
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    .line 393
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/d;

    .line 396
    invoke-virtual {v0, p3}, Lcom/swedbank/mobile/architect/a/b/d;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object p1

    if-eqz p1, :cond_4

    move-object p3, p1

    :cond_4
    :goto_1
    return-object p3

    .line 393
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Item parent is not in stack"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final a(Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;
    .locals 17
    .param p1    # Lcom/swedbank/mobile/architect/a/b/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v6, p1

    .line 351
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v1

    move-object/from16 v11, p0

    move-object/from16 v8, p2

    move-object/from16 v7, p3

    .line 352
    invoke-direct {v11, v6, v7, v8}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/d;Lcom/swedbank/mobile/architect/a/b/g$c;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v12

    .line 353
    invoke-virtual {v6, v12}, Lcom/swedbank/mobile/architect/a/b/d;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356
    new-instance v1, Lcom/swedbank/mobile/architect/a/b/g$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v12, v2, v3}, Lcom/swedbank/mobile/architect/a/b/g$a;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;ZLjava/util/Set;)V

    return-object v1

    .line 362
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/swedbank/mobile/architect/a/b/g$c;->a()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/architect/a/b/d;->b()Ljava/lang/String;

    move-result-object v2

    move/from16 v9, p4

    invoke-virtual {v1, v12, v0, v9, v2}, Lcom/swedbank/mobile/architect/a/b/f;->a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;ZLjava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v13

    .line 363
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/f;->c()Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/util/Set;

    .line 364
    move-object v15, v13

    check-cast v15, Landroid/view/View;

    new-instance v16, Lcom/swedbank/mobile/architect/a/b/g$e;

    move-object/from16 v0, v16

    move-object v2, v12

    move-object v3, v14

    move-object v4, v13

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p2

    move/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lcom/swedbank/mobile/architect/a/b/g$e;-><init>(Lcom/swedbank/mobile/architect/a/b/f;Landroid/view/ViewGroup;Ljava/util/Set;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Lcom/swedbank/mobile/architect/a/b/g$c;Landroid/view/ViewGroup;ZZ)V

    move-object/from16 v0, v16

    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    .line 646
    invoke-virtual {v15}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 647
    invoke-interface {v0, v15}, Landroid/view/View$OnAttachStateChangeListener;->onViewAttachedToWindow(Landroid/view/View;)V

    .line 649
    :cond_1
    invoke-virtual {v15, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 378
    new-instance v0, Lcom/swedbank/mobile/architect/a/b/g$a;

    const/4 v1, 0x1

    invoke-direct {v0, v13, v12, v1, v14}, Lcom/swedbank/mobile/architect/a/b/g$a;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;ZLjava/util/Set;)V

    return-object v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;
    .locals 0

    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZILjava/lang/Object;)Lcom/swedbank/mobile/architect/a/b/g$a;
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 350
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/b/g;)Lcom/swedbank/mobile/architect/a/b/g$c;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/architect/a/b/g;->c:Lcom/swedbank/mobile/architect/a/b/g$c;

    return-object p0
.end method

.method private final a(Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/k;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/k<",
            "Ljava/lang/Boolean;",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            ">;"
        }
    .end annotation

    .line 434
    new-instance v7, Lcom/swedbank/mobile/architect/a/b/d;

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/architect/a/b/d;-><init>(Lcom/swedbank/mobile/architect/a/b/f;Landroid/util/SparseArray;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 439
    instance-of p3, p1, Lcom/swedbank/mobile/architect/a/b/f$c;

    const/4 p4, 0x1

    if-eqz p3, :cond_0

    .line 440
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g;->b:Ljava/util/ArrayDeque;

    invoke-virtual {p1, v7}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 441
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v7}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    .line 444
    :cond_0
    iget-object p3, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    .line 446
    instance-of v0, p1, Lcom/swedbank/mobile/architect/a/b/f$d;

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    if-eqz p2, :cond_5

    .line 448
    check-cast p1, Lcom/swedbank/mobile/architect/a/b/f$d;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/f$d;->d()Lcom/swedbank/mobile/architect/a/b/c;

    move-result-object p1

    .line 450
    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/b/d;->b()Ljava/lang/String;

    move-result-object p5

    invoke-static {p6, p5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p5

    xor-int/2addr p5, p4

    if-eqz p5, :cond_6

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object p2

    if-eqz p2, :cond_4

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/f$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/b/f$d;->d()Lcom/swedbank/mobile/architect/a/b/c;

    move-result-object p2

    check-cast p2, Ljava/lang/Enum;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/a/b/c;->compareTo(Ljava/lang/Enum;)I

    move-result p2

    if-gez p2, :cond_6

    .line 451
    new-instance p2, Ljava/util/ArrayDeque;

    invoke-virtual {p3}, Ljava/util/ArrayDeque;->size()I

    move-result p4

    invoke-direct {p2, p4}, Ljava/util/ArrayDeque;-><init>(I)V

    .line 452
    invoke-virtual {p3}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object p4

    const-string p5, "viewStack.iterator()"

    invoke-static {p4, p5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 453
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_2

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/swedbank/mobile/architect/a/b/d;

    .line 454
    invoke-virtual {p5}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object p6

    .line 455
    instance-of v0, p6, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v0, :cond_1

    check-cast p6, Lcom/swedbank/mobile/architect/a/b/f$d;

    invoke-virtual {p6}, Lcom/swedbank/mobile/architect/a/b/f$d;->d()Lcom/swedbank/mobile/architect/a/b/c;

    move-result-object p6

    move-object v0, p1

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {p6, v0}, Lcom/swedbank/mobile/architect/a/b/c;->compareTo(Ljava/lang/Enum;)I

    move-result p6

    if-gtz p6, :cond_1

    goto :goto_1

    .line 458
    :cond_1
    invoke-interface {p4}, Ljava/util/Iterator;->remove()V

    .line 459
    invoke-virtual {p2, p5}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    .line 462
    :cond_2
    :goto_1
    invoke-virtual {p3, v7}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 463
    check-cast p2, Ljava/lang/Iterable;

    .line 667
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/architect/a/b/d;

    .line 463
    invoke-virtual {p3, p2}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_2

    .line 464
    :cond_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v7}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    .line 450
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.architect.app.view.ViewDetails.RootView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 466
    :cond_5
    move-object p1, p3

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, p4

    if-eqz p1, :cond_6

    .line 469
    invoke-virtual {p3, v7}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 471
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v7}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    .line 474
    :cond_6
    invoke-virtual {p3, v7}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 475
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v7}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    :cond_7
    if-eqz p2, :cond_f

    .line 479
    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/b/d;->b()Ljava/lang/String;

    move-result-object p2

    invoke-static {p5, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_8

    .line 480
    invoke-virtual {p3, v7}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 481
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v7}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    .line 484
    :cond_8
    new-instance p2, Ljava/util/ArrayDeque;

    invoke-virtual {p3}, Ljava/util/ArrayDeque;->size()I

    move-result p6

    invoke-direct {p2, p6}, Ljava/util/ArrayDeque;-><init>(I)V

    .line 485
    invoke-virtual {p3}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object p6

    const-string v0, "viewStack.iterator()"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 486
    :goto_3
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/b/d;

    .line 487
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    goto :goto_4

    .line 490
    :cond_9
    invoke-interface {p6}, Ljava/util/Iterator;->remove()V

    .line 491
    invoke-virtual {p2, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_3

    .line 494
    :cond_a
    :goto_4
    move-object p5, p3

    check-cast p5, Ljava/util/Collection;

    invoke-interface {p5}, Ljava/util/Collection;->isEmpty()Z

    move-result p5

    xor-int/2addr p5, p4

    if-eqz p5, :cond_e

    .line 498
    check-cast p2, Ljava/lang/Iterable;

    .line 669
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/swedbank/mobile/architect/a/b/d;

    if-nez p2, :cond_b

    .line 499
    invoke-virtual {p5}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object p6

    instance-of p6, p6, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz p6, :cond_b

    .line 501
    invoke-virtual {p3, v7}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    const/4 p2, 0x1

    .line 503
    :cond_b
    invoke-virtual {p3, p5}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_5

    :cond_c
    if-nez p2, :cond_d

    .line 506
    invoke-virtual {p3, v7}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 508
    :cond_d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v7}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    .line 495
    :cond_e
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "No matching parent found when pushing "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 494
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 478
    :cond_f
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final a(Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;)Lkotlin/k;
    .locals 12
    .param p1    # Lcom/swedbank/mobile/architect/a/b/g$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/a/b/g$c;",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            "Z",
            "Lkotlin/e/a/b<",
            "-",
            "Landroid/view/ViewGroup;",
            "+",
            "Ljava/util/Set<",
            "+",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)",
            "Lkotlin/k<",
            "Landroid/view/ViewGroup;",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation

    move-object v6, p3

    move-object/from16 v0, p4

    if-eqz p5, :cond_4

    if-eqz p2, :cond_3

    .line 292
    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/e;

    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/b/e;->a()Lcom/swedbank/mobile/architect/a/b/a/e;

    move-result-object v2

    .line 293
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->a()Ljava/lang/String;

    move-result-object v3

    .line 294
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->b()Lcom/swedbank/mobile/architect/a/b/a/f;

    move-result-object v4

    .line 295
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/g$c;->b()Landroidx/c/a;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 298
    invoke-virtual/range {p4 .. p4}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/e;

    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/b/e;->a()Lcom/swedbank/mobile/architect/a/b/a/e;

    move-result-object v2

    .line 299
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/g$c;->b()Landroidx/c/a;

    move-result-object v5

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroidx/c/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/a/f;

    if-eqz v2, :cond_1

    .line 300
    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/b/a/f;->b()V

    goto :goto_0

    .line 298
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.swedbank.mobile.architect.app.view.Transitionable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    move-object v8, v3

    move-object v7, v4

    goto :goto_1

    .line 292
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.swedbank.mobile.architect.app.view.Transitionable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Entering transition cannot be null when pushing views"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    if-eqz v0, :cond_11

    .line 304
    invoke-virtual/range {p4 .. p4}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v2

    if-eqz v2, :cond_10

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/e;

    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/b/e;->a()Lcom/swedbank/mobile/architect/a/b/a/e;

    move-result-object v2

    .line 305
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->a()Ljava/lang/String;

    move-result-object v3

    .line 306
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->c()Lcom/swedbank/mobile/architect/a/b/a/f;

    move-result-object v4

    .line 308
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/g$c;->b()Landroidx/c/a;

    move-result-object v5

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroidx/c/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/a/f;

    if-eqz v2, :cond_1

    .line 309
    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/b/a/f;->a()V

    goto :goto_0

    :goto_1
    const/4 v9, 0x0

    if-eqz v0, :cond_5

    .line 312
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/architect/a/b/d;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, v9

    :goto_2
    move-object v10, v0

    check-cast v10, Landroid/view/View;

    if-eqz p2, :cond_b

    .line 318
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v3, p1

    .line 317
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZ)Lcom/swedbank/mobile/architect/a/b/g$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$a;->b()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$a;->c()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$a;->d()Z

    move-result v3

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$a;->e()Ljava/util/Set;

    move-result-object v0

    if-eqz v3, :cond_6

    move-object/from16 v3, p6

    move-object v4, v1

    goto :goto_3

    :cond_6
    move-object/from16 v3, p6

    move-object v4, v9

    .line 325
    :goto_3
    invoke-interface {v3, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 641
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/ac;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_8

    goto :goto_4

    :cond_8
    if-eqz v1, :cond_9

    move-object v0, v1

    goto :goto_4

    :cond_9
    move-object v0, v9

    :goto_4
    if-eqz v6, :cond_a

    if-eqz v4, :cond_a

    .line 327
    invoke-virtual {v4, p3}, Landroid/view/ViewGroup;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_a
    move-object v1, v2

    move-object v6, v4

    goto :goto_7

    :cond_b
    if-eqz v10, :cond_c

    .line 329
    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_5

    :cond_c
    move-object v0, v9

    :goto_5
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_d

    move-object v0, v9

    :cond_d
    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_e

    goto :goto_6

    :cond_e
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v0

    .line 330
    :goto_6
    move-object v4, v9

    check-cast v4, Landroid/view/ViewGroup;

    .line 331
    move-object v1, v9

    check-cast v1, Ljava/util/Set;

    move-object v6, v4

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_7
    if-eqz p5, :cond_f

    if-eqz v10, :cond_f

    .line 335
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v10, v9}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 339
    :cond_f
    move-object v2, v6

    check-cast v2, Landroid/view/View;

    new-instance v3, Lcom/swedbank/mobile/architect/a/b/g$h;

    move-object v4, p1

    invoke-direct {v3, p1, v8, v0}, Lcom/swedbank/mobile/architect/a/b/g$h;-><init>(Lcom/swedbank/mobile/architect/a/b/g$c;Ljava/lang/String;Ljava/util/Set;)V

    move-object v5, v3

    check-cast v5, Lkotlin/e/a/a;

    move-object v0, v7

    move-object v3, v10

    move/from16 v4, p5

    invoke-interface/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/b/a/f;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V

    .line 343
    invoke-static {v6, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    return-object v0

    .line 304
    :cond_10
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.swedbank.mobile.architect.app.view.Transitionable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_11
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Leaving view cannot be null when popping views"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method static synthetic a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;ILjava/lang/Object;)Lkotlin/k;
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    const/4 p3, 0x0

    .line 283
    check-cast p3, Landroid/util/SparseArray;

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p7, 0x10

    if-eqz p3, :cond_1

    .line 286
    sget-object p3, Lcom/swedbank/mobile/architect/a/b/g$g;->a:Lcom/swedbank/mobile/architect/a/b/g$g;

    move-object p6, p3

    check-cast p6, Lkotlin/e/a/b;

    :cond_1
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;)Lkotlin/k;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/g$c;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/g;->c:Lcom/swedbank/mobile/architect/a/b/g$c;

    return-void
.end method

.method private final b()V
    .locals 4

    const/4 v0, 0x0

    .line 96
    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/g$c;

    iput-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g;->c:Lcom/swedbank/mobile/architect/a/b/g$c;

    .line 97
    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    iput-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    .line 98
    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g;->e:Lcom/swedbank/mobile/architect/a/b/g$b;

    .line 99
    check-cast v0, Lcom/swedbank/mobile/architect/a/b/g$b;

    iput-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->e:Lcom/swedbank/mobile/architect/a/b/g$b;

    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/g$b;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/g$b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/g$b;->c()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/g$b;->d()Landroid/view/Window;

    move-result-object v1

    .line 101
    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/swedbank/mobile/architect/a/b/g;->a(Landroid/view/ViewGroup;Ljava/lang/String;Landroid/os/Bundle;Landroid/view/Window;)V

    :cond_0
    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/g;->c()V

    return-void
.end method

.method private final b(Lcom/swedbank/mobile/architect/a/b/f;)Z
    .locals 3
    .param p1    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 404
    instance-of v0, p1, Lcom/swedbank/mobile/architect/a/b/f$c;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/d;

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/d;->d()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v2

    if-ne v2, p1, :cond_2

    return v1

    .line 409
    :cond_2
    instance-of v2, v2, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_3
    return v1
.end method

.method private final c(Lcom/swedbank/mobile/architect/a/b/f;)Lkotlin/k;
    .locals 6
    .param p1    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ")",
            "Lkotlin/k<",
            "Ljava/lang/Boolean;",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            ">;"
        }
    .end annotation

    .line 516
    instance-of v0, p1, Lcom/swedbank/mobile/architect/a/b/f$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->b:Ljava/util/ArrayDeque;

    goto :goto_0

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    .line 518
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-string v1, "when (this) {\n      is V\u2026iewStack\n    }.iterator()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    const/4 v2, 0x1

    .line 671
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/a/b/d;

    .line 520
    invoke-virtual {v3}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v4

    invoke-static {v4, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v2, :cond_3

    if-eqz v4, :cond_2

    .line 523
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 524
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    if-eqz v4, :cond_1

    .line 528
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 529
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    return-object p1

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method private final c()V
    .locals 12

    .line 617
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;)Lcom/swedbank/mobile/architect/a/b/g$c;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 249
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    if-eqz v1, :cond_6

    .line 251
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/d;->d()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    .line 253
    instance-of v0, v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v0, :cond_0

    .line 254
    invoke-virtual {v8}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v3, v8

    .line 253
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZILjava/lang/Object;)Lcom/swedbank/mobile/architect/a/b/g$a;

    goto/16 :goto_3

    .line 258
    :cond_0
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/g;->d()Lcom/swedbank/mobile/architect/a/b/d;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 260
    invoke-virtual {v8}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, v9

    move-object v3, v8

    .line 259
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZILjava/lang/Object;)Lcom/swedbank/mobile/architect/a/b/g$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$a;->b()Landroid/view/ViewGroup;

    move-result-object v10

    .line 263
    invoke-virtual {v9}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    .line 618
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    .line 619
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/g;->a()Ljava/util/ArrayDeque;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/a/b/d;

    .line 620
    invoke-virtual {v3}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v4

    if-ne v4, v0, :cond_1

    goto :goto_1

    .line 623
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    .line 625
    :cond_2
    :goto_1
    check-cast v1, Ljava/lang/Iterable;

    .line 626
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v9, v0

    check-cast v9, Ljava/util/Collection;

    .line 635
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 634
    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    const-string v0, "it"

    .line 625
    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, v10

    move-object v3, v8

    .line 264
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZILjava/lang/Object;)Lcom/swedbank/mobile/architect/a/b/g$a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 634
    invoke-interface {v9, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 637
    :cond_4
    check-cast v9, Ljava/util/List;

    goto :goto_3

    .line 258
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stack top is inconsistent - no top view; stack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 272
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v9

    const-string v0, "modalStack.descendingIterator()"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 638
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    const-string v0, "it"

    .line 273
    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    invoke-virtual {v8}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v3, v8

    .line 273
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZILjava/lang/Object;)Lcom/swedbank/mobile/architect/a/b/g$a;

    goto :goto_4

    :cond_7
    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/g;->b()V

    return-void
.end method

.method private final d()Lcom/swedbank/mobile/architect/a/b/d;
    .locals 3

    .line 400
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    check-cast v0, Ljava/lang/Iterable;

    .line 653
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/d;

    .line 401
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v2

    instance-of v2, v2, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 654
    :goto_0
    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    return-object v1
.end method

.method private final e()Lcom/swedbank/mobile/architect/a/b/d;
    .locals 2

    .line 536
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    const/4 v1, 0x0

    .line 537
    check-cast v1, Lcom/swedbank/mobile/architect/a/b/d;

    iput-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/ArrayDeque;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayDeque<",
            "Lcom/swedbank/mobile/architect/a/b/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/lang/String;Landroid/os/Bundle;Landroid/view/Window;)V
    .locals 8
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/Window;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/g;->c:Lcom/swedbank/mobile/architect/a/b/g$c;

    if-eqz v0, :cond_1

    .line 70
    new-instance v1, Lcom/swedbank/mobile/architect/a/b/g$b;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/swedbank/mobile/architect/a/b/g$b;-><init>(Landroid/view/ViewGroup;Ljava/lang/String;Landroid/os/Bundle;Landroid/view/Window;)V

    iput-object v1, p0, Lcom/swedbank/mobile/architect/a/b/g;->e:Lcom/swedbank/mobile/architect/a/b/g$b;

    .line 71
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$c;->e()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 72
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/g$c;->c()Landroid/app/Activity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    .line 76
    :cond_1
    move-object v0, p1

    check-cast v0, Landroid/view/View;

    new-instance v7, Lcom/swedbank/mobile/architect/a/b/g$d;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/b/g$d;-><init>(Lcom/swedbank/mobile/architect/a/b/g;Landroid/view/ViewGroup;Ljava/lang/String;Landroid/view/Window;Landroid/os/Bundle;)V

    check-cast v7, Landroid/view/View$OnAttachStateChangeListener;

    .line 594
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 595
    invoke-interface {v7, v0}, Landroid/view/View$OnAttachStateChangeListener;->onViewAttachedToWindow(Landroid/view/View;)V

    .line 597
    :cond_2
    invoke-virtual {v0, v7}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method

.method public final a(Lcom/swedbank/mobile/architect/a/b/f;)V
    .locals 20
    .param p1    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v9, p0

    move-object/from16 v4, p1

    const-string v0, "leaving"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/architect/a/b/f;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 605
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/e;

    .line 175
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/e;->g()V

    goto :goto_0

    .line 177
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/swedbank/mobile/architect/a/b/g;->b(Lcom/swedbank/mobile/architect/a/b/f;)Z

    move-result v7

    .line 178
    invoke-direct/range {p0 .. p1}, Lcom/swedbank/mobile/architect/a/b/g;->c(Lcom/swedbank/mobile/architect/a/b/f;)Lkotlin/k;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/architect/a/b/d;

    .line 607
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;)Lcom/swedbank/mobile/architect/a/b/g$c;

    move-result-object v10

    if-eqz v10, :cond_e

    .line 181
    instance-of v0, v4, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v0, :cond_c

    .line 182
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/architect/a/b/g;->d()Lcom/swedbank/mobile/architect/a/b/d;

    move-result-object v11

    if-nez v5, :cond_a

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez v11, :cond_5

    .line 189
    invoke-virtual {v8}, Lcom/swedbank/mobile/architect/a/b/d;->b()Ljava/lang/String;

    move-result-object v2

    .line 190
    iget-object v3, v9, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    check-cast v3, Ljava/lang/Iterable;

    .line 608
    instance-of v6, v3, Ljava/util/Collection;

    if-eqz v6, :cond_2

    move-object v6, v3

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    .line 609
    :cond_2
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/swedbank/mobile/architect/a/b/d;

    .line 190
    invoke-virtual {v6}, Lcom/swedbank/mobile/architect/a/b/d;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v1

    if-nez v6, :cond_3

    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_4

    goto :goto_2

    .line 191
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Last leaving root view must always be removed after its children are removed\n\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "RemovedItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tstack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v9, Lcom/swedbank/mobile/architect/a/b/g;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 198
    :cond_5
    :goto_2
    invoke-virtual {v10}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v2

    .line 611
    invoke-virtual {v8, v2}, Lcom/swedbank/mobile/architect/a/b/d;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_9

    if-eqz v11, :cond_a

    .line 614
    invoke-virtual {v11}, Lcom/swedbank/mobile/architect/a/b/d;->a()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v2

    instance-of v3, v2, Lcom/swedbank/mobile/architect/a/b/e;

    if-nez v3, :cond_7

    const/4 v2, 0x0

    :cond_7
    check-cast v2, Lcom/swedbank/mobile/architect/a/b/e;

    if-eqz v2, :cond_8

    .line 615
    invoke-virtual {v10}, Lcom/swedbank/mobile/architect/a/b/g$c;->b()Landroidx/c/a;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/b/e;->a()Lcom/swedbank/mobile/architect/a/b/a/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/b/a/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    :cond_8
    if-ne v0, v1, :cond_a

    :cond_9
    return-void

    :cond_a
    if-eqz v11, :cond_b

    .line 205
    invoke-virtual {v11}, Lcom/swedbank/mobile/architect/a/b/d;->d()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v1

    invoke-virtual {v11}, Lcom/swedbank/mobile/architect/a/b/d;->e()Landroid/util/SparseArray;

    move-result-object v12

    const/4 v13, 0x0

    .line 211
    new-instance v14, Lcom/swedbank/mobile/architect/a/b/g$f;

    move-object v0, v14

    move-object v2, v10

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object v6, v8

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/g$f;-><init>(Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ZLcom/swedbank/mobile/architect/a/b/d;Z)V

    move-object v6, v14

    check-cast v6, Lkotlin/e/a/b;

    move-object/from16 v0, p0

    move-object v1, v10

    move-object v2, v11

    move-object v3, v12

    move-object v4, v8

    move v5, v13

    .line 206
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;)Lkotlin/k;

    goto :goto_4

    .line 226
    :cond_b
    iput-object v8, v9, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    goto :goto_4

    .line 229
    :cond_c
    instance-of v0, v4, Lcom/swedbank/mobile/architect/a/b/e;

    if-eqz v0, :cond_d

    if-nez v7, :cond_e

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x12

    const/4 v11, 0x0

    move-object/from16 v0, p0

    move-object v1, v10

    move-object v4, v8

    move-object v8, v11

    .line 230
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;ILjava/lang/Object;)Lkotlin/k;

    goto :goto_4

    :cond_d
    if-nez v7, :cond_e

    .line 236
    sget-object v12, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    .line 238
    invoke-virtual {v10}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {v9, v8, v10, v0}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/d;Lcom/swedbank/mobile/architect/a/b/g$c;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v13

    const/4 v14, 0x0

    .line 240
    invoke-virtual {v10}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/swedbank/mobile/architect/a/b/d;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/view/View;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x10

    const/16 v19, 0x0

    .line 236
    invoke-static/range {v12 .. v19}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->a(Lcom/swedbank/mobile/architect/a/b/a/f;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;ILjava/lang/Object;)V

    :cond_e
    :goto_4
    return-void

    :cond_f
    return-void
.end method

.method public final a(Lcom/swedbank/mobile/architect/a/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object v9, p0

    move-object v7, p1

    const-string v0, "entering"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeId"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "directParentWithViewNodeId"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rootViewParentNodeId"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "closestRootViewNodeId"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/g;->d()Lcom/swedbank/mobile/architect/a/b/d;

    move-result-object v10

    .line 114
    iget-object v0, v9, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    const/4 v1, 0x0

    const/4 v8, 0x1

    if-eqz v0, :cond_0

    const/4 v11, 0x1

    goto :goto_0

    :cond_0
    const/4 v11, 0x0

    .line 116
    :goto_0
    instance-of v12, v7, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v12, :cond_6

    if-nez v10, :cond_1

    if-eqz v11, :cond_3

    :cond_1
    if-eqz v10, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    xor-int/2addr v0, v11

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    goto :goto_5

    .line 117
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "While pushing root view to stack there must either be at least one previous root view or pending remove, but not both at once"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tTopmostRootView="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tPendingRemove="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v9, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tEntering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 121
    :cond_6
    instance-of v0, v7, Lcom/swedbank/mobile/architect/a/b/f$c;

    if-nez v0, :cond_8

    if-eqz v10, :cond_7

    if-nez v11, :cond_7

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    :cond_8
    :goto_3
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_13

    .line 128
    :goto_5
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/f;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 599
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/e;

    .line 128
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/e;->f()V

    goto :goto_6

    .line 133
    :cond_9
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/b/f;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 601
    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_a

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    goto :goto_7

    .line 602
    :cond_a
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/e;

    .line 133
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/e;->h()Z

    move-result v2

    xor-int/2addr v2, v8

    if-eqz v2, :cond_b

    const/4 v1, 0x1

    :cond_c
    :goto_7
    if-eqz v1, :cond_d

    const-string v0, "ViewManager"

    const-string v1, "Node got detached before anything got rendered"

    .line 134
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_d
    move-object v0, p0

    move-object v1, p1

    move-object v2, v10

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    .line 138
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/k;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/architect/a/b/d;

    if-nez v1, :cond_e

    return-void

    .line 604
    :cond_e
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;)Lcom/swedbank/mobile/architect/a/b/g$c;

    move-result-object v3

    if-eqz v3, :cond_12

    if-eqz v12, :cond_10

    if-eqz v10, :cond_f

    move-object v4, v10

    goto :goto_8

    .line 153
    :cond_f
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/g;->e()Lcom/swedbank/mobile/architect/a/b/d;

    move-result-object v0

    move-object v4, v0

    :goto_8
    const/4 v5, 0x0

    xor-int/lit8 v6, v11, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x12

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, v3

    move-object v3, v5

    move v5, v6

    move-object v6, v7

    move v7, v8

    move-object v8, v11

    .line 154
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;ILjava/lang/Object;)Lkotlin/k;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    if-eqz v10, :cond_12

    .line 159
    invoke-virtual {v10, v0}, Lcom/swedbank/mobile/architect/a/b/d;->a(Landroid/util/SparseArray;)V

    goto :goto_9

    .line 161
    :cond_10
    instance-of v0, v7, Lcom/swedbank/mobile/architect/a/b/e;

    if-eqz v0, :cond_11

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x12

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move v7, v8

    move-object v8, v10

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/g$c;Lcom/swedbank/mobile/architect/a/b/d;Landroid/util/SparseArray;Lcom/swedbank/mobile/architect/a/b/d;ZLkotlin/e/a/b;ILjava/lang/Object;)Lkotlin/k;

    goto :goto_9

    .line 166
    :cond_11
    invoke-virtual {v3}, Lcom/swedbank/mobile/architect/a/b/g$c;->d()Landroid/view/ViewGroup;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, v2

    move-object v2, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move-object v7, v8

    .line 165
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/architect/a/b/g;->a(Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/d;Landroid/view/ViewGroup;Lcom/swedbank/mobile/architect/a/b/g$c;ZZILjava/lang/Object;)Lcom/swedbank/mobile/architect/a/b/g$a;

    :cond_12
    :goto_9
    return-void

    .line 122
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "While pushing non-root view to stack there must be at least one previous root view in stack and there can\'t be any pending removes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tTopmostRootView="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tPendingRemove="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v9, Lcom/swedbank/mobile/architect/a/b/g;->d:Lcom/swedbank/mobile/architect/a/b/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n\tEntering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
