.class public final Lcom/swedbank/mobile/architect/a/b/f$b;
.super Lcom/swedbank/mobile/architect/a/b/f;
.source "ViewDetails.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/b/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/architect/a/b/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/swedbank/mobile/architect/a/b/a/e;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;)V
    .locals 1
    .param p2    # Lcom/swedbank/mobile/architect/a/b/a/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/swedbank/mobile/architect/a/b/a/e;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;+",
            "Lcom/swedbank/mobile/architect/a/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/architect/a/b/b;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "transition"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenters"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "views"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 103
    invoke-direct {p0, p3, p4, v0}, Lcom/swedbank/mobile/architect/a/b/f;-><init>(Ljava/util/Map;Ljavax/inject/Provider;Lkotlin/e/b/g;)V

    iput p1, p0, Lcom/swedbank/mobile/architect/a/b/f$b;->a:I

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/f$b;->b:Lcom/swedbank/mobile/architect/a/b/a/e;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    .line 100
    new-instance p2, Lcom/swedbank/mobile/architect/a/b/a/e;

    const/4 p5, 0x3

    const/4 p6, 0x0

    invoke-direct {p2, p6, p6, p5, p6}, Lcom/swedbank/mobile/architect/a/b/a/e;-><init>(Lcom/swedbank/mobile/architect/a/b/a/f;Lcom/swedbank/mobile/architect/a/b/a/f;ILkotlin/e/b/g;)V

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/architect/a/b/f$b;-><init>(ILcom/swedbank/mobile/architect/a/b/a/e;Ljava/util/Map;Ljavax/inject/Provider;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;ZLjava/lang/String;)Landroid/view/ViewGroup;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "into"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget v0, p0, Lcom/swedbank/mobile/architect/a/b/f$b;->a:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 111
    sget v0, Lcom/swedbank/mobile/architect/a$a;->architect_node_id:I

    invoke-virtual {p2, v0, p4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    if-eqz p3, :cond_0

    .line 113
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 115
    check-cast p2, Landroid/view/ViewGroup;

    return-object p2

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    check-cast p1, Landroid/view/View;

    invoke-static {p1, p2}, Lcom/swedbank/mobile/architect/c/a;->a(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    return-object p1
.end method

.method public a()Lcom/swedbank/mobile/architect/a/b/a/e;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/f$b;->b:Lcom/swedbank/mobile/architect/a/b/a/e;

    return-object v0
.end method
