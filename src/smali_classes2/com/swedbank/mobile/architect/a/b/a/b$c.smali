.class public final Lcom/swedbank/mobile/architect/a/b/a/b$c;
.super Ljava/lang/Object;
.source "AnimatorTransitionHandler.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/b/a/b;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/view/ViewGroup;

.field final synthetic d:Z

.field final synthetic e:Landroid/view/View;

.field final synthetic f:Z

.field final synthetic g:Lkotlin/e/a/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/architect/a/b/a/b;Landroid/view/View;Landroid/view/ViewGroup;ZLandroid/view/View;ZLkotlin/e/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "Z",
            "Landroid/view/View;",
            "Z",
            "Lkotlin/e/a/a;",
            ")V"
        }
    .end annotation

    .line 47
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->c:Landroid/view/ViewGroup;

    iput-boolean p4, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->d:Z

    iput-object p5, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->e:Landroid/view/View;

    iput-boolean p6, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->f:Z

    iput-object p7, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->g:Lkotlin/e/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 9

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 50
    iget-object v2, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    iget-object v3, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->c:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->b:Landroid/view/View;

    iget-boolean v5, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->d:Z

    iget-object v6, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->e:Landroid/view/View;

    iget-boolean v7, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->f:Z

    iget-object v8, p0, Lcom/swedbank/mobile/architect/a/b/a/b$c;->g:Lkotlin/e/a/a;

    invoke-virtual/range {v2 .. v8}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;ZLkotlin/e/a/a;)V

    const/4 v0, 0x1

    return v0
.end method
