.class public final Lcom/swedbank/mobile/architect/a/b/a/b$a;
.super Landroid/animation/AnimatorListenerAdapter;
.source "AnimatorTransitionHandler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;ZLkotlin/e/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/b/a/b;

.field final synthetic b:Lkotlin/e/a/a;

.field final synthetic c:Z

.field final synthetic d:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/architect/a/b/a/b;Lkotlin/e/a/a;ZLandroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a;",
            "Z",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 76
    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->b:Lkotlin/e/a/a;

    iput-boolean p3, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->d:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "animation"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 79
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Lcom/swedbank/mobile/architect/a/b/a/b;)Lkotlin/e/a/a;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/s;

    .line 81
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->b:Lkotlin/e/a/a;

    move-object v1, p0

    check-cast v1, Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {p1, v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Lkotlin/e/a/a;Landroid/animation/AnimatorListenerAdapter;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "animation"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 86
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/a/b;->b(Lcom/swedbank/mobile/architect/a/b/a/b;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/a/b;->c(Lcom/swedbank/mobile/architect/a/b/a/b;)Landroid/animation/Animator;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    invoke-static {p1}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Lcom/swedbank/mobile/architect/a/b/a/b;)Lkotlin/e/a/a;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/s;

    .line 91
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->b:Lkotlin/e/a/a;

    move-object v1, p0

    check-cast v1, Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {p1, v0, v1}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Lkotlin/e/a/a;Landroid/animation/AnimatorListenerAdapter;)V

    .line 93
    iget-boolean p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->c:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->d:Landroid/view/View;

    if-eqz p1, :cond_2

    .line 94
    iget-object p1, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->a:Lcom/swedbank/mobile/architect/a/b/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b/a/b$a;->d:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/a/b/a/b;->a(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    :goto_0
    return-void
.end method
