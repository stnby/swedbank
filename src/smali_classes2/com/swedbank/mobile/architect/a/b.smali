.class public final Lcom/swedbank/mobile/architect/a/b;
.super Lio/reactivex/f/a;
.source "DisposableViewStateObserver.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VS:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/f/a<",
        "TVS;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/k/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/a<",
            "TVS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/k/a;)V
    .locals 1
    .param p1    # Lio/reactivex/k/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/k/a<",
            "TVS;>;)V"
        }
    .end annotation

    const-string v0, "subject"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Lio/reactivex/f/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/b;->a:Lio/reactivex/k/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)Ljava/lang/Void;
    .locals 2
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewState observable must not reach error state - onError()"

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onComplete()V
    .locals 0

    return-void
.end method

.method public synthetic onError(Ljava/lang/Throwable;)V
    .locals 0

    .line 6
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/b;->a(Ljava/lang/Throwable;)Ljava/lang/Void;

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;)V"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/b;->a:Lio/reactivex/k/a;

    invoke-virtual {v0, p1}, Lio/reactivex/k/a;->onNext(Ljava/lang/Object;)V

    return-void
.end method
