.class public final Lcom/swedbank/mobile/architect/a/f;
.super Ljava/lang/Object;
.source "Presenter.kt"


# static fields
.field static final synthetic a:[Lkotlin/h/g;

.field public static b:Lio/reactivex/v;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private static final c:Lkotlin/d;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/r;

    const-class v2, Lcom/swedbank/mobile/architect/a/f;

    const-string v3, "architect_release"

    invoke-static {v2, v3}, Lkotlin/e/b/v;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object v2

    const-string v3, "DEFAULT_MAIN_THREAD_SCHEDULER"

    const-string v4, "getDEFAULT_MAIN_THREAD_SCHEDULER()Lio/reactivex/Scheduler;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/r;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/q;)Lkotlin/h/h;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/architect/a/f;->a:[Lkotlin/h/g;

    .line 22
    sget-object v0, Lkotlin/i;->c:Lkotlin/i;

    sget-object v1, Lcom/swedbank/mobile/architect/a/f$a;->a:Lcom/swedbank/mobile/architect/a/f$a;

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v1}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/architect/a/f;->c:Lkotlin/d;

    return-void
.end method

.method public static final a()Lio/reactivex/v;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    sget-object v0, Lcom/swedbank/mobile/architect/a/f;->c:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/architect/a/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/v;

    return-object v0
.end method
