.class public final Lcom/swedbank/mobile/architect/a/h$e;
.super Ljava/lang/Object;
.source "Router.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/h;->c(Lkotlin/e/a/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic b:Z

.field final synthetic c:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic d:Lkotlin/e/a/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h$e;->a:Lcom/swedbank/mobile/architect/a/h;

    iput-boolean p2, p0, Lcom/swedbank/mobile/architect/a/h$e;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/h$e;->c:Lcom/swedbank/mobile/architect/a/h;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/h$e;->d:Lkotlin/e/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 239
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/h$e;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$e;->a:Lcom/swedbank/mobile/architect/a/h;

    invoke-static {v0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$e;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 283
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 284
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 285
    iget-object v2, p0, Lcom/swedbank/mobile/architect/a/h$e;->d:Lkotlin/e/a/b;

    const-string v3, "next"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 287
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/h;->s()V

    goto :goto_0

    :cond_2
    return-void
.end method
