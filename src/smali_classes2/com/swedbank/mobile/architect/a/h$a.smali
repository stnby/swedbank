.class public final Lcom/swedbank/mobile/architect/a/h$a;
.super Ljava/lang/Object;
.source "Router.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic b:Z

.field final synthetic c:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic d:Lcom/swedbank/mobile/architect/a/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h$a;->a:Lcom/swedbank/mobile/architect/a/h;

    iput-boolean p2, p0, Lcom/swedbank/mobile/architect/a/h$a;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/h$a;->d:Lcom/swedbank/mobile/architect/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 239
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->a:Lcom/swedbank/mobile/architect/a/h;

    invoke-static {v0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/h$a;->d:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 283
    iget-object v2, p0, Lcom/swedbank/mobile/architect/a/h$a;->d:Lcom/swedbank/mobile/architect/a/h;

    .line 284
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-static {v0}, Lcom/swedbank/mobile/architect/a/h;->h(Lcom/swedbank/mobile/architect/a/h;)Lcom/b/c/c;

    move-result-object v3

    .line 285
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->m()Ljava/lang/String;

    move-result-object v4

    .line 287
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->u()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->m()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v5, v0

    goto :goto_1

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 290
    :goto_1
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->o()Ljava/lang/String;

    move-result-object v6

    .line 291
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->u()Lcom/swedbank/mobile/architect/a/b/f;

    move-result-object v0

    .line 292
    instance-of v0, v0, Lcom/swedbank/mobile/architect/a/b/f$d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->m()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v7, v0

    goto :goto_3

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$a;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 283
    :goto_3
    invoke-virtual/range {v2 .. v7}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/b/c/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method
