.class public final Lcom/swedbank/mobile/architect/a/h$f;
.super Ljava/lang/Object;
.source "Router.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/architect/a/h;->g(Lcom/swedbank/mobile/architect/a/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic b:Z

.field final synthetic c:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic d:Lcom/swedbank/mobile/architect/a/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/a/h;ZLcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/architect/a/h$f;->a:Lcom/swedbank/mobile/architect/a/h;

    iput-boolean p2, p0, Lcom/swedbank/mobile/architect/a/h$f;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/architect/a/h$f;->c:Lcom/swedbank/mobile/architect/a/h;

    iput-object p4, p0, Lcom/swedbank/mobile/architect/a/h$f;->d:Lcom/swedbank/mobile/architect/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 239
    iget-boolean v0, p0, Lcom/swedbank/mobile/architect/a/h$f;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$f;->a:Lcom/swedbank/mobile/architect/a/h;

    invoke-static {v0}, Lcom/swedbank/mobile/architect/a/h;->j(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$f;->c:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->l()Ljava/util/ArrayDeque;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/h$f;->d:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lcom/swedbank/mobile/architect/a/h$f;->d:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->s()V

    :cond_1
    return-void

    .line 282
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/swedbank/mobile/architect/a/h$f;->d:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " was not attached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
