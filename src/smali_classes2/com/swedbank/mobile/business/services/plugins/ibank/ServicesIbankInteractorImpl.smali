.class public final Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ServicesIbankInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/services/plugins/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/services/plugins/ibank/c;",
        ">;",
        "Lcom/swedbank/mobile/business/services/plugins/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/services/j;

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/services/j;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/services/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getIbankServiceLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/services/j;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "servicesRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getIbankServiceLink"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->a:Lcom/swedbank/mobile/business/services/j;

    iput-object p2, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p1, Lcom/swedbank/mobile/business/services/plugins/ibank/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/b;

    check-cast p1, Lcom/swedbank/mobile/business/services/plugins/ibank/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/plugins/ibank/a;->a()Lcom/swedbank/mobile/business/services/a/a;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/services/plugins/ibank/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/services/plugins/ibank/c;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 36
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    invoke-static {p0}, Lcom/swedbank/mobile/business/services/plugins/a$a;->a(Lcom/swedbank/mobile/business/services/plugins/a;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->a:Lcom/swedbank/mobile/business/services/j;

    .line 25
    iget-object v1, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/ServicesIbankInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/services/j;->a(Z)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
