.class public final Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "MPosOnboardingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/a;
.implements Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/d;",
        ">;",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/a;",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;
        .annotation runtime Ljavax/inject/Named;
            value = "mpos_onboarding_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

    return-void
.end method


# virtual methods
.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;->k()V

    return-void
.end method
