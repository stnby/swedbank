.class public final Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ServicesAgreementsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
.implements Lcom/swedbank/mobile/business/services/plugins/a;
.implements Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/h;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
        "Lcom/swedbank/mobile/business/services/plugins/a;",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

.field private final b:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/services/plugins/agreements/g;Lcom/swedbank/mobile/business/customer/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/services/plugins/agreements/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "servicesAgreementsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    iput-object p2, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->d()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/swedbank/mobile/business/services/plugins/agreements/a;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->a()V

    goto :goto_0

    .line 65
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/services/plugins/agreements/c;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/swedbank/mobile/business/services/plugins/agreements/h$a;->a(Lcom/swedbank/mobile/business/services/plugins/agreements/h;Ljava/lang/String;Lkotlin/e/a/b;ILjava/lang/Object;)V

    goto :goto_0

    .line 66
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/business/services/plugins/agreements/b;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->c()V

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->d()V

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    .line 80
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    .line 81
    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->d()Ljava/lang/String;

    move-result-object v2

    .line 82
    check-cast p1, Lcom/swedbank/mobile/business/services/plugins/agreements/d;

    sget-object v3, Lcom/swedbank/mobile/business/services/plugins/agreements/e;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/d;->ordinal()I

    move-result p1

    aget p1, v3, p1

    packed-switch p1, :pswitch_data_0

    .line 84
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->f()Lkotlin/e/a/b;

    move-result-object p1

    goto :goto_0

    .line 83
    :pswitch_1
    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->e()Lkotlin/e/a/b;

    move-result-object p1

    .line 80
    :goto_0
    invoke-interface {v1, v2, p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->a(Ljava/lang/String;Lkotlin/e/a/b;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    .line 95
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 51
    iget-object v1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    .line 52
    iget-object v2, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    .line 53
    iget-object v2, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    .line 54
    invoke-interface {v2}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->c()Lio/reactivex/b;

    move-result-object v2

    .line 55
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v2, v3}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v2

    const-string v3, "servicesAgreementsReposi\u2026   .toSingleDefault(Unit)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/aa;

    .line 50
    invoke-virtual {v0, v1, v2}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 56
    sget-object v1, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Singles.zip(\n        ser\u2026 -> mPosAgreementResult }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(QueryResult.Success)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    .line 94
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    .line 38
    invoke-interface {v1}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->a()Lio/reactivex/o;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v2, "servicesAgreementsReposi\u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v2, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    .line 41
    invoke-interface {v2}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->b()Lio/reactivex/o;

    move-result-object v2

    .line 42
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    const-string v3, "servicesAgreementsReposi\u2026  .distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0, v1, v2}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    .line 43
    sget-object v1, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026osAppInstalled)\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/business/services/plugins/agreements/i;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Lcom/swedbank/mobile/business/services/plugins/agreements/i;-><init>(ZZILkotlin/e/b/g;)V

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.just(Services\u2026ta(hasAgreement = false))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public j()V
    .locals 2

    .line 71
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    iget-object v1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/g;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/services/plugins/agreements/g;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->a(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->b()V

    return-void
.end method

.method public k()V
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/services/plugins/agreements/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/h;->b()V

    return-void
.end method
