.class final Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;
.super Ljava/lang/Object;
.source "ServicesAgreementsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl;->i()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lcom/swedbank/mobile/business/services/plugins/agreements/i;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/i;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 44
    new-instance v1, Lcom/swedbank/mobile/business/services/plugins/agreements/i;

    const-string v2, "hasMPosAgreement"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v2, "hasMPosAppInstalled"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/i;-><init>(ZZ)V

    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/ServicesAgreementsInteractorImpl$a;->a(Lkotlin/k;)Lcom/swedbank/mobile/business/services/plugins/agreements/i;

    move-result-object p1

    return-object p1
.end method
