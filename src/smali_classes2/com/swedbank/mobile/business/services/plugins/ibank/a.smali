.class public final Lcom/swedbank/mobile/business/services/plugins/ibank/a;
.super Ljava/lang/Object;
.source "ServicesIbankActions.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/a/c;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/services/a/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/services/a/a;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/services/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/a;->a:Lcom/swedbank/mobile/business/services/a/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/a;->b:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/services/a/a;Ljava/lang/String;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, "feature_services_ibank"

    .line 10
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/business/services/plugins/ibank/a;-><init>(Lcom/swedbank/mobile/business/services/a/a;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/services/a/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/a;->a:Lcom/swedbank/mobile/business/services/a/a;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/plugins/ibank/a;->b:Ljava/lang/String;

    return-object v0
.end method
