.class public final Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;
.super Ljava/lang/Object;
.source "MPosOnboardingInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;",
            ">;)V"
        }
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;",
            ">;)",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;"
        }
    .end annotation

    .line 21
    new-instance v0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;
    .locals 2

    .line 16
    new-instance v0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;-><init>(Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/b;->a()Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/MPosOnboardingInteractorImpl;

    move-result-object v0

    return-object v0
.end method
