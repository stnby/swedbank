.class public final enum Lcom/swedbank/mobile/business/services/a/b;
.super Ljava/lang/Enum;
.source "IbankService.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/services/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/services/a/b;

.field public static final enum b:Lcom/swedbank/mobile/business/services/a/b;

.field public static final enum c:Lcom/swedbank/mobile/business/services/a/b;

.field public static final enum d:Lcom/swedbank/mobile/business/services/a/b;

.field private static final synthetic e:[Lcom/swedbank/mobile/business/services/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/business/services/a/b;

    new-instance v1, Lcom/swedbank/mobile/business/services/a/b;

    const-string v2, "GENERAL"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/services/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/b;->a:Lcom/swedbank/mobile/business/services/a/b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/services/a/b;

    const-string v2, "LOANS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/services/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/b;->b:Lcom/swedbank/mobile/business/services/a/b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/services/a/b;

    const-string v2, "LIFE_INSURANCE"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/services/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/b;->c:Lcom/swedbank/mobile/business/services/a/b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/services/a/b;

    const-string v2, "INSURANCE"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/services/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/services/a/b;->e:[Lcom/swedbank/mobile/business/services/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/services/a/b;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/services/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/services/a/b;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/services/a/b;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/services/a/b;->e:[Lcom/swedbank/mobile/business/services/a/b;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/services/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/services/a/b;

    return-object v0
.end method
