.class public final enum Lcom/swedbank/mobile/business/services/a/a;
.super Ljava/lang/Enum;
.source "IbankService.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/services/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum b:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum c:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum d:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum e:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum f:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum g:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum h:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum i:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum j:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum k:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum l:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum m:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum n:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum o:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum p:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum q:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum r:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum s:Lcom/swedbank/mobile/business/services/a/a;

.field public static final enum t:Lcom/swedbank/mobile/business/services/a/a;

.field private static final synthetic u:[Lcom/swedbank/mobile/business/services/a/a;


# instance fields
.field private final v:Lcom/swedbank/mobile/business/services/a/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/swedbank/mobile/business/services/a/a;

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CURRENCY_EXCHANGE"

    .line 6
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->a:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->a:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CURRENCY_RATES"

    .line 7
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->a:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x1

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->b:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "E_INVOICES"

    .line 8
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->a:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x2

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->c:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "PAYMENTS_LIST"

    .line 9
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->a:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x3

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->d:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "SMALL_LOAN"

    .line 11
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->b:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x4

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->e:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "HOME_SMALL_LOAN"

    .line 12
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->b:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x5

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->f:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "MORTGAGE"

    .line 13
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->b:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x6

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->g:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CAR_LOAN"

    .line 14
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->b:Lcom/swedbank/mobile/business/services/a/b;

    const/4 v4, 0x7

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->h:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CAR_LEASING"

    .line 15
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->b:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0x8

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->i:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "LIFE_RISK_INSURANCE"

    .line 17
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->c:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0x9

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->j:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "SAFE_CHILD"

    .line 18
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->c:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0xa

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->k:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "SAFE_PENSION_FUND"

    .line 19
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->c:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0xb

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->l:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "PRIVATE_PORTFOLIO"

    .line 20
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->c:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0xc

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->m:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "HOME_INSURANCE"

    .line 22
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0xd

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->n:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CASCO_INSURANCE"

    .line 23
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0xe

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->o:Lcom/swedbank/mobile/business/services/a/a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "TRAFFIC_INSURANCE"

    .line 24
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0xf

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->p:Lcom/swedbank/mobile/business/services/a/a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "TRAVEL_INSURANCE"

    .line 25
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0x10

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->q:Lcom/swedbank/mobile/business/services/a/a;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CREDIT_COVER"

    .line 26
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0x11

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->r:Lcom/swedbank/mobile/business/services/a/a;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "LOAN_PAYMENT_PROTECTION"

    .line 27
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0x12

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->s:Lcom/swedbank/mobile/business/services/a/a;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/services/a/a;

    const-string v2, "CLAIM_APPLICATION"

    .line 28
    sget-object v3, Lcom/swedbank/mobile/business/services/a/b;->d:Lcom/swedbank/mobile/business/services/a/b;

    const/16 v4, 0x13

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/services/a/a;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V

    sput-object v1, Lcom/swedbank/mobile/business/services/a/a;->t:Lcom/swedbank/mobile/business/services/a/a;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/business/services/a/a;->u:[Lcom/swedbank/mobile/business/services/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/swedbank/mobile/business/services/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/services/a/b;",
            ")V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/business/services/a/a;->v:Lcom/swedbank/mobile/business/services/a/b;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/services/a/a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/services/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/services/a/a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/services/a/a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/services/a/a;->u:[Lcom/swedbank/mobile/business/services/a/a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/services/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/services/a/a;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/services/a/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/a/a;->v:Lcom/swedbank/mobile/business/services/a/b;

    return-object v0
.end method
