.class public final Lcom/swedbank/mobile/business/services/a;
.super Ljava/lang/Object;
.source "GetIbankServiceLink.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/services/a/a;",
        "Lio/reactivex/w<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/services/j;

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/services/j;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/services/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/services/j;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "servicesRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getJumpToIbankLink"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/services/a;->a:Lcom/swedbank/mobile/business/services/j;

    iput-object p2, p0, Lcom/swedbank/mobile/business/services/a;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/services/a;->c:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/services/a;)Lcom/swedbank/mobile/business/services/j;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/services/a;->a:Lcom/swedbank/mobile/business/services/j;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/services/a;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/services/a;->b:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/services/a/a;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/services/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/services/a/a;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/swedbank/mobile/business/services/a$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/services/a$a;-><init>(Lcom/swedbank/mobile/business/services/a;Lcom/swedbank/mobile/business/services/a/a;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/business/services/a;->c:Lcom/swedbank/mobile/architect/business/b;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/services/b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/services/b;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single\n      .fromCallab\u2026atMap(getJumpToIbankLink)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/business/services/a/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/services/a;->a(Lcom/swedbank/mobile/business/services/a/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
