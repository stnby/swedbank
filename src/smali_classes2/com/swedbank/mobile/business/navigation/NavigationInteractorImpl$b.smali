.class final Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;
.super Ljava/lang/Object;
.source "NavigationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->b(Ljava/lang/String;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/navigation/m;)Lio/reactivex/j;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/navigation/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/navigation/m;",
            ")",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;->b:Ljava/lang/String;

    .line 90
    invoke-static {v0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->c(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 91
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/business/navigation/k;

    .line 90
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/navigation/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    check-cast v2, Lcom/swedbank/mobile/business/navigation/k;

    .line 94
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/navigation/k;->b()Lkotlin/e/a/a;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 76
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/navigation/m;->b(Lcom/swedbank/mobile/architect/a/h;)Lio/reactivex/j;

    move-result-object p1

    .line 77
    new-instance v0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->b(Lio/reactivex/c/g;)Lio/reactivex/j;

    move-result-object p1

    return-object p1

    .line 93
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Missing navigation plugin for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 90
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/business/navigation/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;->a(Lcom/swedbank/mobile/business/navigation/m;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
