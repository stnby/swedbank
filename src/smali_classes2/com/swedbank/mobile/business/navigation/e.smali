.class public final Lcom/swedbank/mobile/business/navigation/e;
.super Ljava/lang/Object;
.source "GenericPushNotificationFlow.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/a/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/UserPushMessage;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/authentication/f;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/UserPushMessage;Lcom/swedbank/mobile/business/authentication/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/UserPushMessage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationExplanation"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/navigation/e;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    iput-object p2, p0, Lcom/swedbank/mobile/business/navigation/e;->b:Lcom/swedbank/mobile/business/authentication/f;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/f;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/e;->b:Lcom/swedbank/mobile/business/authentication/f;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/business/navigation/e;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/navigation/e;

    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/e;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    iget-object v1, p1, Lcom/swedbank/mobile/business/navigation/e;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/e;->b:Lcom/swedbank/mobile/business/authentication/f;

    iget-object p1, p1, Lcom/swedbank/mobile/business/navigation/e;->b:Lcom/swedbank/mobile/business/authentication/f;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/e;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/business/navigation/e;->b:Lcom/swedbank/mobile/business/authentication/f;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GenericPushNotificationFlowInput(message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/navigation/e;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", authenticationExplanation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/navigation/e;->b:Lcom/swedbank/mobile/business/authentication/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
