.class final Lcom/swedbank/mobile/business/navigation/d$b;
.super Lkotlin/e/b/k;
.source "GenericPushNotificationFlow.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/navigation/d;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/navigation/e;)Lcom/swedbank/mobile/architect/business/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
        "Lio/reactivex/j<",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/navigation/d$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/navigation/d$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/navigation/d$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/navigation/d$b;->a:Lcom/swedbank/mobile/business/navigation/d$b;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/authenticated/c;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/authenticated/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/authentication/authenticated/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/navigation/d$b;->a(Lcom/swedbank/mobile/business/authentication/authenticated/c;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
