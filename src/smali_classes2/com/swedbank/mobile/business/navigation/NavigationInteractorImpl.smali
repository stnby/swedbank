.class public final Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "NavigationInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/navigation/g;
.implements Lcom/swedbank/mobile/business/navigation/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/navigation/m;",
        ">;",
        "Lcom/swedbank/mobile/business/navigation/g;",
        "Lcom/swedbank/mobile/business/navigation/j;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/navigation/l;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/navigation/l;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/navigation/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "to_navigation_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/navigation/l;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "navigationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationPlugins"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a:Lcom/swedbank/mobile/business/navigation/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;)Lcom/swedbank/mobile/business/navigation/l;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a:Lcom/swedbank/mobile/business/navigation/l;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;)Lio/reactivex/w;
    .locals 0

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;)Ljava/util/List;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->b:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a:Lcom/swedbank/mobile/business/navigation/l;

    .line 44
    invoke-interface {v0}, Lcom/swedbank/mobile/business/navigation/l;->a()Lio/reactivex/o;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "navigationRepository\n   \u2026Key\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "navigationItemKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 94
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 95
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/navigation/k;

    .line 56
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/navigation/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a:Lcom/swedbank/mobile/business/navigation/l;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/navigation/l;->a(Ljava/lang/String;)V

    return-void

    .line 57
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Missing navigation plugin for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 56
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/j;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "navigationItemKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026igationItemKey) }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b()V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a:Lcom/swedbank/mobile/business/navigation/l;

    .line 63
    invoke-interface {v0}, Lcom/swedbank/mobile/business/navigation/l;->c()Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 65
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    .line 98
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    check-cast v1, Lcom/swedbank/mobile/business/navigation/m;

    .line 65
    invoke-interface {v1}, Lcom/swedbank/mobile/business/navigation/m;->a()V

    goto :goto_0

    .line 99
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;

    .line 66
    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public i()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">()",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapMay\u2026irstNavigationNode<T>() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->a:Lcom/swedbank/mobile/business/navigation/l;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/navigation/l;->b()Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 35
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 89
    invoke-static {p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->c(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 90
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/business/navigation/k;

    .line 89
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/navigation/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    check-cast v2, Lcom/swedbank/mobile/business/navigation/k;

    goto :goto_1

    .line 92
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing navigation plugin for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 36
    :cond_3
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->b:Ljava/util/List;

    invoke-static {v0}, Lkotlin/a/h;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/navigation/k;

    .line 38
    :goto_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/navigation/m;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/navigation/k;->b()Lkotlin/e/a/a;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    new-instance v3, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$d;

    invoke-direct {v3, p0, v2}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;Lcom/swedbank/mobile/business/navigation/k;)V

    check-cast v3, Lkotlin/e/a/a;

    invoke-interface {v0, v1, v3}, Lcom/swedbank/mobile/business/navigation/m;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/a;)V

    return-void

    .line 36
    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
