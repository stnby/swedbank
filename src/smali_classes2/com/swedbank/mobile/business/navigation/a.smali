.class public final Lcom/swedbank/mobile/business/navigation/a;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "AuthenticatedNavigationFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/navigation/b;",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/navigation/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/navigation/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/navigation/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/navigation/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/navigation/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/navigation/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/swedbank/mobile/business/navigation/a$a;

    invoke-direct {v0, p2, p1}, Lcom/swedbank/mobile/business/navigation/a$a;-><init>(Lcom/swedbank/mobile/business/navigation/b;Lcom/swedbank/mobile/business/root/c;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/business/navigation/a;->a(Lkotlin/e/a/a;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 21
    sget-object v0, Lcom/swedbank/mobile/business/navigation/a$b;->a:Lcom/swedbank/mobile/business/navigation/a$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/navigation/a$c;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/business/navigation/a$c;-><init>(Lcom/swedbank/mobile/business/navigation/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/navigation/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/navigation/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/navigation/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
