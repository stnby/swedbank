.class public final Lcom/swedbank/mobile/business/navigation/h;
.super Ljava/lang/Object;
.source "NavigationInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/l;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;>;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/business/navigation/h;->a:Ljavax/inject/Provider;

    .line 17
    iput-object p2, p0, Lcom/swedbank/mobile/business/navigation/h;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/navigation/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/navigation/k;",
            ">;>;)",
            "Lcom/swedbank/mobile/business/navigation/h;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/navigation/h;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/navigation/h;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;
    .locals 3

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/navigation/h;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/navigation/l;

    iget-object v2, p0, Lcom/swedbank/mobile/business/navigation/h;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;-><init>(Lcom/swedbank/mobile/business/navigation/l;Ljava/util/List;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/navigation/h;->a()Lcom/swedbank/mobile/business/navigation/NavigationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
