.class public interface abstract Lcom/swedbank/mobile/business/navigation/m;
.super Ljava/lang/Object;
.source "NavigationRouter.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/e;


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lcom/swedbank/mobile/architect/a/h;)V
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/a;)V
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b()Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">()",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b(Lcom/swedbank/mobile/architect/a/h;)Lio/reactivex/j;
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lcom/swedbank/mobile/architect/a/h;",
            ")",
            "Lio/reactivex/j<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method
