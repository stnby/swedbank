.class public final Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "MessageCenterAccessInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/messagecenter/access/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/messagecenter/access/c;",
        ">;",
        "Lcom/swedbank/mobile/business/messagecenter/access/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/customer/l;

.field private final b:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectedCustomer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getJumpToIbankLink"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/b;

    new-instance v1, Lcom/swedbank/mobile/business/e;

    .line 26
    iget-object v2, p0, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "business/useful/bank/communication/notifications"

    goto :goto_0

    :cond_0
    const-string v2, "private/home/important/communication/notifications"

    :goto_0
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 23
    invoke-direct {v1, v2, v4, v3, v4}, Lcom/swedbank/mobile/business/e;-><init>(Ljava/lang/String;Ljava/util/Map;ILkotlin/e/b/g;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 29
    new-instance v1, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/messagecenter/access/c;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/messagecenter/access/MessageCenterAccessInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/messagecenter/access/c;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x1

    invoke-static {v0, v4, v1, v2, v4}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 34
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
