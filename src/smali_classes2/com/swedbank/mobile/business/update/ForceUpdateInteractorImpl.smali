.class public final Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ForceUpdateInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/update/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/update/c;",
        ">;",
        "Lcom/swedbank/mobile/business/update/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/firebase/c;

.field private final c:Lcom/swedbank/mobile/business/i/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "force_update_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->b:Lcom/swedbank/mobile/business/firebase/c;

    iput-object p3, p0, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->c:Lcom/swedbank/mobile/business/i/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/update/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/update/c;->a()V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->b:Lcom/swedbank/mobile/business/firebase/c;

    const-string v1, "remote_config_min_supported_version"

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/firebase/c;->c(Ljava/lang/String;)J

    move-result-wide v0

    .line 25
    iget-object v2, p0, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/e/i;->b()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->c:Lcom/swedbank/mobile/business/i/c;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/update/ForceUpdateInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    :cond_0
    return-void
.end method
