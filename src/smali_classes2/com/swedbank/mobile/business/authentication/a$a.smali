.class final Lcom/swedbank/mobile/business/authentication/a$a;
.super Ljava/lang/Object;
.source "Authenticate.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/a;->a(Lcom/swedbank/mobile/business/authentication/e;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/a;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/a;Lcom/swedbank/mobile/business/authentication/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/x;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/x;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    .line 25
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/e$a;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/a;->a(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/authentication/l;

    move-result-object p1

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    check-cast v1, Lcom/swedbank/mobile/business/authentication/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/e$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/business/authentication/l;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/a;->b(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/general/a;

    move-result-object v0

    .line 79
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 81
    new-instance v1, Lcom/swedbank/mobile/business/authentication/a$a$a;

    invoke-direct {v1, v0, p0}, Lcom/swedbank/mobile/business/authentication/a$a$a;-><init>(Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/business/authentication/a$a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 80
    sget-object v0, Lcom/swedbank/mobile/business/authentication/b$b;->a:Lcom/swedbank/mobile/business/authentication/b$b;

    check-cast v0, Lkotlin/e/a/m;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/authentication/c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/authentication/c;-><init>(Lkotlin/e/a/m;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/d;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "toObservable()\n    .publ\u2026hanged(::classesAreEqual)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 30
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/e$c;

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/a;->a(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/authentication/l;

    move-result-object p1

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    check-cast v1, Lcom/swedbank/mobile/business/authentication/e$c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/e$c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/business/authentication/l;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/a;->b(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/general/a;

    move-result-object v0

    .line 82
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 84
    new-instance v1, Lcom/swedbank/mobile/business/authentication/a$a$b;

    invoke-direct {v1, v0, p0}, Lcom/swedbank/mobile/business/authentication/a$a$b;-><init>(Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/business/authentication/a$a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 83
    sget-object v0, Lcom/swedbank/mobile/business/authentication/b$b;->a:Lcom/swedbank/mobile/business/authentication/b$b;

    check-cast v0, Lkotlin/e/a/m;

    if-eqz v0, :cond_2

    new-instance v1, Lcom/swedbank/mobile/business/authentication/c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/authentication/c;-><init>(Lkotlin/e/a/m;)V

    move-object v0, v1

    :cond_2
    check-cast v0, Lio/reactivex/c/d;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "toObservable()\n    .publ\u2026hanged(::classesAreEqual)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_3
    instance-of p1, p1, Lcom/swedbank/mobile/business/authentication/e$b;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/a;->a(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/authentication/l;

    move-result-object p1

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    check-cast v1, Lcom/swedbank/mobile/business/authentication/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/e$b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/business/authentication/l;->d(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/authentication/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/a$a;->a(Lcom/swedbank/mobile/business/authentication/x;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
