.class public final Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "AuthenticatedInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/authenticated/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/authenticated/d;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/authenticated/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/i/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_authenticated_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pluginManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;)Lcom/swedbank/mobile/business/i/d;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/navigation/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/authentication/authenticated/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/authentication/authenticated/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapMay\u2026lectedCustomerNavigation)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lkotlin/h/b;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/h/b<",
            "*>;)",
            "Lio/reactivex/j<",
            "TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginRouterClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;Lkotlin/h/b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026(pluginRouterClass, it) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected m_()V
    .locals 7

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/authenticated/AuthenticatedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/authenticated/d;

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/authenticated/d;->a()Lio/reactivex/j;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    .line 33
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/j;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 44
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
