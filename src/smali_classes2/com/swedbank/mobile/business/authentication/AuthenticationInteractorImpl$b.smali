.class public final Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;
.super Ljava/lang/Object;
.source "AuthenticationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/j<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "lastLoggedInUserId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_2

    .line 71
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->e(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lio/reactivex/k/b;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 74
    invoke-static {}, Lio/reactivex/k/b;->e()Lio/reactivex/k/b;

    move-result-object p1

    const-string v0, "MaybeSubject.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {v0, p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;Lio/reactivex/k/b;)V

    .line 77
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/k/b;->a(Lio/reactivex/c/g;)Lio/reactivex/j;

    move-result-object p1

    .line 78
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b$2;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    goto :goto_1

    .line 71
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There cannot be more than one forget previous user stream at a time"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 80
    :cond_2
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;->a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
