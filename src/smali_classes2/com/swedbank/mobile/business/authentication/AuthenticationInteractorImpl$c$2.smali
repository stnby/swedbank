.class public final Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;
.super Ljava/lang/Object;
.source "AuthenticationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/k;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/authentication/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/k$a;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;

    iget-object v0, v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->i(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/s;

    .line 89
    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;

    iget-object v2, v2, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/e;->b()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v2

    .line 90
    iget-object v3, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;

    iget-object v3, v3, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->c:Ljava/lang/String;

    .line 91
    iget-object v4, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;

    iget-object v4, v4, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/authentication/e;->c()Ljava/lang/String;

    move-result-object v4

    .line 88
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/business/authentication/login/s;-><init>(Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    goto :goto_0

    .line 92
    :cond_0
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    :goto_0
    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;->a(Lcom/swedbank/mobile/business/authentication/k;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
