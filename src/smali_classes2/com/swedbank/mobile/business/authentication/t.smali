.class public final Lcom/swedbank/mobile/business/authentication/t;
.super Ljava/lang/Object;
.source "LogOut.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/m;

.field private final b:Lcom/swedbank/mobile/business/authentication/l;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/m;Lcom/swedbank/mobile/business/authentication/l;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "invalidateSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/m;",
            "Lcom/swedbank/mobile/business/authentication/l;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sessionRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invalidateSession"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/t;->a:Lcom/swedbank/mobile/business/authentication/session/m;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/t;->b:Lcom/swedbank/mobile/business/authentication/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/t;->c:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/t;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/t;->c:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/t;)Lcom/swedbank/mobile/business/authentication/l;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/t;->b:Lcom/swedbank/mobile/business/authentication/l;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/t;->a:Lcom/swedbank/mobile/business/authentication/session/m;

    .line 17
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/m;->d()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 18
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 19
    new-instance v1, Lcom/swedbank/mobile/business/authentication/t$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/authentication/t$a;-><init>(Lcom/swedbank/mobile/business/authentication/t;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "sessionRepository\n      \u2026ication(session))\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/t;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
