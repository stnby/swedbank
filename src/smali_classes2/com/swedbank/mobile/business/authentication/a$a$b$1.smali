.class public final Lcom/swedbank/mobile/business/authentication/a$a$b$1;
.super Ljava/lang/Object;
.source "Authenticate.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/a$a$b;->a(Lio/reactivex/o;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/a$a$b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/a$a$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a$b$1;->a:Lcom/swedbank/mobile/business/authentication/a$a$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/k;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/authentication/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/a$a$b$1;->a:Lcom/swedbank/mobile/business/authentication/a$a$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/authentication/a$a$b;->b:Lcom/swedbank/mobile/business/authentication/a$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/authentication/a$a;->a:Lcom/swedbank/mobile/business/authentication/a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/a;->a(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/authentication/l;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a$b$1;->a:Lcom/swedbank/mobile/business/authentication/a$a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/authentication/a$a$b;->b:Lcom/swedbank/mobile/business/authentication/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/authentication/a$a;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/authentication/l;->c(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    const-string v0, "block()\n                    .toObservable()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a$a$b$1;->a:Lcom/swedbank/mobile/business/authentication/a$a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/authentication/a$a$b;->a:Lcom/swedbank/mobile/business/general/a;

    .line 79
    new-instance v1, Lcom/swedbank/mobile/business/authentication/a$a$b$1$1;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/authentication/a$a$b$1$1;-><init>(Lcom/swedbank/mobile/business/general/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->l(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 80
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/y;->a()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/y;->b()Ljava/util/concurrent/TimeUnit;

    move-result-object v0

    invoke-virtual {p1, v1, v2, v0}, Lio/reactivex/o;->c(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "repeatWhen { it.flatMap \u2026ime, pollData.delay.unit)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/swedbank/mobile/business/authentication/b$a$2$1;->a:Lcom/swedbank/mobile/business/authentication/b$a$2$1;

    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/a$a$b$1;->a(Lcom/swedbank/mobile/business/authentication/k;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
