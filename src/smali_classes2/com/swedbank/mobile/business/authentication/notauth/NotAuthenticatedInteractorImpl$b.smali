.class final Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;
.super Lkotlin/e/b/k;
.source "NotAuthenticatedInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/authentication/login/c;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/login/c;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "lastUsedLoginMethod"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;)Lcom/swedbank/mobile/business/authentication/notauth/e;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/swedbank/mobile/business/authentication/notauth/a;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/c;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 44
    :pswitch_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->b(Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;)Lcom/swedbank/mobile/business/customer/k;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/customer/k;->e()Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 43
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/notauth/e;->a(Lcom/swedbank/mobile/business/util/l;)V

    goto :goto_0

    .line 39
    :pswitch_1
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/notauth/e;->a()V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;->a(Lcom/swedbank/mobile/business/authentication/login/c;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
