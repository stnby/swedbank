.class public final Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "NotAuthenticatedInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/notauth/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/notauth/e;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/notauth/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/f/a;

.field private final b:Lcom/swedbank/mobile/business/customer/k;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/customer/k;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "observeLastUsedLoginMethodUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lcom/swedbank/mobile/business/customer/k;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localCustomerRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeLastUsedLoginMethod"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/k;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;)Lcom/swedbank/mobile/business/authentication/notauth/e;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/notauth/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;)Lcom/swedbank/mobile/business/customer/k;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/k;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/navigation/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/authentication/notauth/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/authentication/notauth/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapMay\u2026flowToAttachedNavigation)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 7

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    const-string v1, "feature_recurring_login_screen"

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/notauth/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/notauth/e;->a()V

    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/o;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/authentication/notauth/NotAuthenticatedInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 56
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
