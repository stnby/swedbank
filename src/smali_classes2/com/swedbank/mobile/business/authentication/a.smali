.class public final Lcom/swedbank/mobile/business/authentication/a;
.super Ljava/lang/Object;
.source "Authenticate.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/e;",
        "Lio/reactivex/o<",
        "Lcom/swedbank/mobile/business/authentication/k;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/l;

.field private final b:Lcom/swedbank/mobile/business/general/a;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/l;Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/general/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "saveSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "acquireScopedSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/l;",
            "Lcom/swedbank/mobile/business/general/a;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pollData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "saveSession"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "acquireScopedSession"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/a;->b:Lcom/swedbank/mobile/business/general/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/a;->c:Lcom/swedbank/mobile/architect/business/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/authentication/a;->d:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/authentication/l;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/business/general/a;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/a;->b:Lcom/swedbank/mobile/business/general/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/a;->d:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/authentication/a;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/a;->c:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/e;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    .line 22
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/l;->a()Lio/reactivex/w;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/authentication/a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/authentication/a$a;-><init>(Lcom/swedbank/mobile/business/authentication/a;Lcom/swedbank/mobile/business/authentication/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 40
    new-instance v0, Lcom/swedbank/mobile/business/authentication/a$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/a$b;-><init>(Lcom/swedbank/mobile/business/authentication/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 56
    sget-object v0, Lcom/swedbank/mobile/business/authentication/a$c;->a:Lcom/swedbank/mobile/business/authentication/a$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->j(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "authenticationRepository\u2026(causingException = it) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/a;->a(Lcom/swedbank/mobile/business/authentication/e;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
