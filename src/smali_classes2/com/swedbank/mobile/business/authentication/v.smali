.class public abstract Lcom/swedbank/mobile/business/authentication/v;
.super Ljava/lang/Object;
.source "AuthenticationInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/authentication/v$d;,
        Lcom/swedbank/mobile/business/authentication/v$a;,
        Lcom/swedbank/mobile/business/authentication/v$e;,
        Lcom/swedbank/mobile/business/authentication/v$b;,
        Lcom/swedbank/mobile/business/authentication/v$c;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/swedbank/mobile/business/authentication/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .line 152
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 153
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$a;->a:Lcom/swedbank/mobile/business/authentication/v$a;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_2

    .line 154
    :cond_1
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/v$e;

    if-eqz v0, :cond_2

    goto :goto_1

    .line 155
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 156
    :cond_3
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/v$c;

    if-eqz v0, :cond_4

    :goto_1
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
