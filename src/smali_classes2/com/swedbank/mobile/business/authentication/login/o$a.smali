.class final Lcom/swedbank/mobile/business/authentication/login/o$a;
.super Ljava/lang/Object;
.source "ObserveLastUsedLoginMethod.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/login/o;->a()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/login/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/login/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/o$a;->a:Lcom/swedbank/mobile/business/authentication/login/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;"
        }
    .end annotation

    const-string v0, "biometricsEnabled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/swedbank/mobile/business/authentication/login/c;->b:Lcom/swedbank/mobile/business/authentication/login/c;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 19
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/o$a;->a:Lcom/swedbank/mobile/business/authentication/login/o;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/login/o;->a(Lcom/swedbank/mobile/business/authentication/login/o;)Lcom/swedbank/mobile/business/authentication/login/g;

    move-result-object p1

    .line 20
    invoke-interface {p1}, Lcom/swedbank/mobile/business/authentication/login/g;->e()Lio/reactivex/o;

    move-result-object p1

    .line 21
    new-instance v0, Lcom/swedbank/mobile/business/authentication/login/o$a$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/login/o$a$1;-><init>(Lcom/swedbank/mobile/business/authentication/login/o$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/login/o$a;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
