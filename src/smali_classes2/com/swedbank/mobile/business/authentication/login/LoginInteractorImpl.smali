.class public final Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "LoginInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/j;
.implements Lcom/swedbank/mobile/business/authentication/login/h;
.implements Lcom/swedbank/mobile/business/authentication/login/l;
.implements Lcom/swedbank/mobile/business/biometric/authentication/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/login/n;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/j;",
        "Lcom/swedbank/mobile/business/authentication/login/h;",
        "Lcom/swedbank/mobile/business/authentication/login/l;",
        "Lcom/swedbank/mobile/business/biometric/authentication/h;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/c/a;

.field private final c:Lcom/swedbank/mobile/business/authentication/login/l;

.field private final d:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/authentication/o;

.field private final f:Lcom/swedbank/mobile/business/biometric/login/f;

.field private final g:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/biometric/login/f;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/login/l;
        .annotation runtime Ljavax/inject/Named;
            value = "login_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "logged_in_customer_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "login_authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/biometric/login/f;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_login_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getSavedLoginInfoUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "observeLastUsedLoginMethodUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/c/a;",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/o;",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/login/c;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggedInCustomerName"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginAuthenticationStream"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricLoginStream"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getSavedLoginInfo"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeLastUsedLoginMethod"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->b:Lcom/swedbank/mobile/business/c/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->d:Lcom/swedbank/mobile/business/util/l;

    iput-object p4, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->e:Lcom/swedbank/mobile/business/authentication/o;

    iput-object p5, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->f:Lcom/swedbank/mobile/business/biometric/login/f;

    iput-object p6, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    iput-object p7, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    .line 56
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<LoginState>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->b:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginMethod"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    return-object p1
.end method

.method public a()V
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/login/n;

    .line 94
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->d:Lcom/swedbank/mobile/business/util/l;

    .line 93
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/authentication/login/n;->a(Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/login/n;

    .line 101
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/p$a;

    if-eqz v1, :cond_0

    .line 102
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/login/n;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    .line 103
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/n;->b()V

    goto :goto_0

    .line 105
    :cond_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/p$c;

    if-eqz v1, :cond_1

    .line 106
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/n;->b()V

    .line 107
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/login/n;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/v;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "loginState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->a:Lcom/b/c/c;

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 122
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/n;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/authentication/login/n;->a()V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/authentication/login/m;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extra"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginMethod"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/i;->a:[I

    invoke-virtual {p3}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 88
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot login with "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 85
    :pswitch_1
    new-instance p3, Lcom/swedbank/mobile/business/authentication/e$b;

    invoke-direct {p3, p1, p2}, Lcom/swedbank/mobile/business/authentication/e$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p3, Lcom/swedbank/mobile/business/authentication/e;

    goto :goto_1

    :pswitch_2
    const-string p3, "+"

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 162
    invoke-static {p2, p3, v0, v1, v2}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 163
    :cond_0
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->b(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->d()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/c/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 82
    :goto_0
    new-instance p3, Lcom/swedbank/mobile/business/authentication/e$a;

    invoke-direct {p3, p1, p2}, Lcom/swedbank/mobile/business/authentication/e$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p3, Lcom/swedbank/mobile/business/authentication/e;

    goto :goto_1

    .line 79
    :pswitch_3
    new-instance p3, Lcom/swedbank/mobile/business/authentication/e$c;

    invoke-direct {p3, p1, p2}, Lcom/swedbank/mobile/business/authentication/e$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p3, Lcom/swedbank/mobile/business/authentication/e;

    .line 90
    :goto_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/n;

    invoke-interface {p1, p3}, Lcom/swedbank/mobile/business/authentication/login/n;->a(Lcom/swedbank/mobile/business/authentication/e;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/authentication/login/m;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginMethod"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/i;->b:[I

    invoke-virtual {p3}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    .line 141
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No form for "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 140
    :pswitch_1
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_6

    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_1

    invoke-static {p2}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_6

    goto :goto_6

    .line 139
    :pswitch_2
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_6

    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_3

    invoke-static {p2}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p1, 0x1

    :goto_3
    if-nez p1, :cond_6

    goto :goto_6

    .line 138
    :pswitch_3
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_6

    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_5

    invoke-static {p2}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 p1, 0x1

    :goto_5
    if-nez p1, :cond_6

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    :pswitch_4
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public e()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    .line 132
    sget-object v1, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/authentication/login/k;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/authentication/login/k;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 133
    sget-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "observeLastUsedLoginMeth\u2026d)\n      .first(SMART_ID)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/l;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/l;->j()V

    return-void
.end method

.method public g()V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->e:Lcom/swedbank/mobile/business/authentication/o;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/o;->b()V

    .line 148
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->f:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->a()V

    return-void
.end method

.method public h_()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->f:Lcom/swedbank/mobile/business/biometric/login/f;

    .line 118
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->f()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->a:Lcom/b/c/c;

    sget-object v1, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public j()V
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->k_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/login/n;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/n;->c()V

    :cond_0
    return-void
.end method

.method protected m_()V
    .locals 7

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->f:Lcom/swedbank/mobile/business/biometric/login/f;

    .line 60
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->b()Lio/reactivex/o;

    move-result-object v1

    .line 61
    new-instance v0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 157
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->f:Lcom/swedbank/mobile/business/biometric/login/f;

    .line 65
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->f()Lio/reactivex/o;

    move-result-object v1

    .line 66
    new-instance v0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 159
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
