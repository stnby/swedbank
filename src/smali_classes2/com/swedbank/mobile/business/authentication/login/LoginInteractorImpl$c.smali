.class final Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$c;
.super Lkotlin/e/b/k;
.source "LoginInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/biometric/login/a;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/login/a;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/login/a$d;

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/login/a$d;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/login/a$d;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 158
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/v$e;

    .line 70
    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/authentication/v$e;-><init>(Ljava/util/List;)V

    goto :goto_0

    .line 159
    :cond_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/v$c;

    .line 69
    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/authentication/v$c;-><init>(Ljava/lang/Throwable;)V

    .line 68
    :goto_0
    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    goto :goto_1

    .line 69
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/login/LoginInteractorImpl$c;->a(Lcom/swedbank/mobile/business/biometric/login/a;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
