.class public final Lcom/swedbank/mobile/business/authentication/login/a;
.super Ljava/lang/Object;
.source "GetSavedLoginInfo.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/login/m;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/authentication/login/s;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loginCredentialsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/a;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/w;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginMethod"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/a;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    .line 11
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/login/g;->a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/o;

    move-result-object v0

    .line 12
    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/a$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/authentication/login/a$a;-><init>(Lcom/swedbank/mobile/business/authentication/login/m;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 18
    new-instance v7, Lcom/swedbank/mobile/business/authentication/login/s;

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/business/authentication/login/s;-><init>(Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v7}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "loginCredentialsReposito\u2026     userId = \"\"\n      ))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7
    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/login/a;->a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
