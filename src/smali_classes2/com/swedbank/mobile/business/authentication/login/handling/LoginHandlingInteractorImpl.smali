.class public final Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "LoginHandlingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/j;
.implements Lcom/swedbank/mobile/business/authentication/login/handling/c;
.implements Lcom/swedbank/mobile/business/authentication/login/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/login/handling/d;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/j;",
        "Lcom/swedbank/mobile/business/authentication/login/handling/c;",
        "Lcom/swedbank/mobile/business/authentication/login/l;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/customer/k;

.field private final b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;"
        }
    .end annotation
.end field

.field private final synthetic c:Lcom/swedbank/mobile/business/authentication/login/handling/b;

.field private final synthetic d:Lcom/swedbank/mobile/business/authentication/login/handling/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/k;Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/login/handling/b;
        .annotation runtime Ljavax/inject/Named;
            value = "login_handling_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/customer/k;",
            "Lcom/swedbank/mobile/business/authentication/login/handling/b;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "localCustomerRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationInput"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->d:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/k;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->b:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/v;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "loginState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->d:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/login/handling/b;->a(Lcom/swedbank/mobile/business/authentication/v;)V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/handling/b;->j()V

    return-void
.end method

.method protected m_()V
    .locals 3

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/login/handling/d;

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->b:Lcom/swedbank/mobile/business/util/l;

    .line 53
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    .line 34
    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/k;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/customer/k;->e()Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    .line 32
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/login/handling/d;->a(Lcom/swedbank/mobile/business/authentication/f;Lcom/swedbank/mobile/business/util/l;)V

    goto :goto_0

    .line 54
    :cond_0
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_4

    check-cast v1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/authentication/g;

    .line 38
    instance-of v2, v1, Lcom/swedbank/mobile/business/authentication/g$c;

    if-eqz v2, :cond_1

    .line 39
    check-cast v1, Lcom/swedbank/mobile/business/authentication/g$c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/g$c;->a()Lcom/swedbank/mobile/business/authentication/f;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/login/handling/LoginHandlingInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/k;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/customer/k;->e()Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    .line 38
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/login/handling/d;->a(Lcom/swedbank/mobile/business/authentication/f;Lcom/swedbank/mobile/business/util/l;)V

    goto :goto_0

    .line 41
    :cond_1
    instance-of v2, v1, Lcom/swedbank/mobile/business/authentication/g$b;

    if-eqz v2, :cond_2

    .line 42
    check-cast v1, Lcom/swedbank/mobile/business/authentication/g$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/g$b;->a()Lcom/swedbank/mobile/business/authentication/e;

    move-result-object v2

    .line 43
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/g$b;->b()Lcom/swedbank/mobile/business/authentication/o;

    move-result-object v1

    .line 41
    invoke-interface {v0, v2, v1}, Lcom/swedbank/mobile/business/authentication/login/handling/d;->a(Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;)V

    goto :goto_0

    .line 44
    :cond_2
    instance-of v2, v1, Lcom/swedbank/mobile/business/authentication/g$a;

    if-eqz v2, :cond_3

    .line 45
    check-cast v1, Lcom/swedbank/mobile/business/authentication/g$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/g$a;->a()Lcom/swedbank/mobile/business/authentication/p;

    move-result-object v1

    .line 44
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/authentication/login/handling/d;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    :cond_3
    :goto_0
    return-void

    .line 47
    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
