.class public final enum Lcom/swedbank/mobile/business/authentication/login/m;
.super Ljava/lang/Enum;
.source "LoginMethod.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/authentication/login/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/authentication/login/m;

.field public static final enum b:Lcom/swedbank/mobile/business/authentication/login/m;

.field public static final enum c:Lcom/swedbank/mobile/business/authentication/login/m;

.field public static final enum d:Lcom/swedbank/mobile/business/authentication/login/m;

.field public static final enum e:Lcom/swedbank/mobile/business/authentication/login/m;

.field private static final synthetic f:[Lcom/swedbank/mobile/business/authentication/login/m;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/business/authentication/login/m;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/m;

    const-string v2, "BIOMETRICS"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->a:Lcom/swedbank/mobile/business/authentication/login/m;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/m;

    const-string v2, "SMART_ID"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/m;

    const-string v2, "MOBILE_ID"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->c:Lcom/swedbank/mobile/business/authentication/login/m;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/m;

    const-string v2, "PIN_CALCULATOR"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->d:Lcom/swedbank/mobile/business/authentication/login/m;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/m;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->e:Lcom/swedbank/mobile/business/authentication/login/m;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->f:[Lcom/swedbank/mobile/business/authentication/login/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/login/m;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->f:[Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/authentication/login/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/authentication/login/m;

    return-object v0
.end method
