.class public final Lcom/swedbank/mobile/business/authentication/login/q;
.super Ljava/lang/Object;
.source "SaveLoginInfo.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/login/s;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loginCredentialsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/login/q;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/b;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->a()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/authentication/login/m;->e:Lcom/swedbank/mobile/business/authentication/login/m;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/login/q;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    .line 13
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->a()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v1

    .line 14
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->b()Ljava/lang/String;

    move-result-object v2

    .line 15
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->a()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v3

    sget-object v4, Lcom/swedbank/mobile/business/authentication/login/m;->d:Lcom/swedbank/mobile/business/authentication/login/m;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->c()Ljava/lang/String;

    move-result-object v3

    .line 12
    :goto_1
    invoke-interface {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/g;->a(Lcom/swedbank/mobile/business/authentication/login/m;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/login/q;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/authentication/login/g;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "loginCredentialsReposito\u2026edInUserId(input.userId))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 11
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Unsupported login method"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7
    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/login/q;->a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
