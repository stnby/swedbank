.class public final enum Lcom/swedbank/mobile/business/authentication/login/c;
.super Ljava/lang/Enum;
.source "LastUsedLoginMethod.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/authentication/login/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/authentication/login/c;

.field public static final enum b:Lcom/swedbank/mobile/business/authentication/login/c;

.field public static final enum c:Lcom/swedbank/mobile/business/authentication/login/c;

.field public static final enum d:Lcom/swedbank/mobile/business/authentication/login/c;

.field public static final enum e:Lcom/swedbank/mobile/business/authentication/login/c;

.field private static final synthetic f:[Lcom/swedbank/mobile/business/authentication/login/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/business/authentication/login/c;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/c;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/c;->a:Lcom/swedbank/mobile/business/authentication/login/c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/c;

    const-string v2, "BIOMETRICS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/c;->b:Lcom/swedbank/mobile/business/authentication/login/c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/c;

    const-string v2, "SMART_ID"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/c;->c:Lcom/swedbank/mobile/business/authentication/login/c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/c;

    const-string v2, "MOBILE_ID"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/c;->d:Lcom/swedbank/mobile/business/authentication/login/c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/login/c;

    const-string v2, "PIN_CALCULATOR"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/login/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/login/c;->e:Lcom/swedbank/mobile/business/authentication/login/c;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/authentication/login/c;->f:[Lcom/swedbank/mobile/business/authentication/login/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/authentication/login/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/authentication/login/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/login/c;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/authentication/login/c;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/c;->f:[Lcom/swedbank/mobile/business/authentication/login/c;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/authentication/login/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/authentication/login/c;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 10
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/d;->a:[I

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/login/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 15
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->d:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_0

    .line 14
    :pswitch_1
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->c:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_0

    .line 13
    :pswitch_2
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_0

    .line 12
    :pswitch_3
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->a:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_0

    .line 11
    :pswitch_4
    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->e:Lcom/swedbank/mobile/business/authentication/login/m;

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
