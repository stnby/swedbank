.class final Lcom/swedbank/mobile/business/authentication/q$a;
.super Ljava/lang/Object;
.source "GetLastUsedAuthenticationCredentials.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/q;->a()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/login/m;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/login/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/q$a;->a:Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lcom/swedbank/mobile/business/util/l;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 21
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/q$a;->a:Lcom/swedbank/mobile/business/authentication/login/m;

    sget-object v2, Lcom/swedbank/mobile/business/authentication/r;->a:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 31
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    goto :goto_0

    .line 28
    :pswitch_1
    new-instance v1, Lcom/swedbank/mobile/business/authentication/e$b;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/authentication/e$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    goto :goto_0

    .line 25
    :pswitch_2
    new-instance v1, Lcom/swedbank/mobile/business/authentication/e$a;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/authentication/e$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    goto :goto_0

    .line 22
    :pswitch_3
    new-instance v1, Lcom/swedbank/mobile/business/authentication/e$c;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/authentication/e$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    .line 32
    :goto_0
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/q$a;->a(Lkotlin/k;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    return-object p1
.end method
