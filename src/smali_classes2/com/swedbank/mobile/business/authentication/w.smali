.class public final enum Lcom/swedbank/mobile/business/authentication/w;
.super Ljava/lang/Enum;
.source "SecurityLevel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/authentication/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/authentication/w;

.field public static final enum b:Lcom/swedbank/mobile/business/authentication/w;

.field public static final enum c:Lcom/swedbank/mobile/business/authentication/w;

.field private static final synthetic d:[Lcom/swedbank/mobile/business/authentication/w;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/authentication/w;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/w;

    const-string v2, "NOT_AUTHENTICATED"

    const/4 v3, 0x0

    .line 4
    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/business/authentication/w;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/w;->a:Lcom/swedbank/mobile/business/authentication/w;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/w;

    const-string v2, "AUTHENTICATION_IN_PROGRESS"

    const/4 v3, 0x1

    .line 5
    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/business/authentication/w;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/w;->b:Lcom/swedbank/mobile/business/authentication/w;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/w;

    const-string v2, "AUTHENTICATED"

    const/4 v3, 0x2

    .line 6
    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/business/authentication/w;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/w;->c:Lcom/swedbank/mobile/business/authentication/w;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/authentication/w;->d:[Lcom/swedbank/mobile/business/authentication/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/business/authentication/w;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/authentication/w;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/authentication/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/w;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/authentication/w;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/authentication/w;->d:[Lcom/swedbank/mobile/business/authentication/w;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/authentication/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/authentication/w;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 3
    iget v0, p0, Lcom/swedbank/mobile/business/authentication/w;->e:I

    return v0
.end method
