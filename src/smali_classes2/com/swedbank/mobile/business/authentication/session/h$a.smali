.class public final Lcom/swedbank/mobile/business/authentication/session/h$a;
.super Lcom/swedbank/mobile/business/authentication/session/h;
.source "Session.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/authentication/session/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/o;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:J

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/o;JJ)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tokens"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/authentication/session/h;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->a:Lcom/swedbank/mobile/business/authentication/session/o;

    iput-wide p2, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    iput-wide p4, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/authentication/session/o;JJILkotlin/e/b/g;)V
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    .line 15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p4

    :cond_0
    move-wide v4, p4

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/authentication/session/h$a;-><init>(Lcom/swedbank/mobile/business/authentication/session/o;JJ)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    iget-wide v4, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    const/16 v6, 0x3e8

    int-to-long v6, v6

    mul-long v4, v4, v6

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b()Lcom/swedbank/mobile/business/authentication/session/o;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->a:Lcom/swedbank/mobile/business/authentication/session/o;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .line 11
    iget-wide v0, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .line 15
    iget-wide v0, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->a:Lcom/swedbank/mobile/business/authentication/session/o;

    iget-object v3, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;->a:Lcom/swedbank/mobile/business/authentication/session/o;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-wide v3, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    iget-wide v5, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget-wide v3, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    iget-wide v5, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->a:Lcom/swedbank/mobile/business/authentication/session/o;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    const/16 v3, 0x20

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Active(tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->a:Lcom/swedbank/mobile/business/authentication/session/o;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", expiresIn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", created="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/swedbank/mobile/business/authentication/session/h$a;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
