.class public final Lcom/swedbank/mobile/business/authentication/session/scoped/f;
.super Ljava/lang/Object;
.source "SaveScopedSession.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/scoped/j;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/scoped/j;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/scoped/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sessionRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/f;->a:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/f;->a:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    .line 12
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/session/scoped/j;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/scoped/f;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
