.class public final Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "SessionRefreshInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/refresh/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/session/refresh/g;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/session/refresh/f;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/authentication/session/refresh/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/authentication/session/refresh/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "refreshSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/refresh/e;
        .annotation runtime Ljavax/inject/Named;
            value = "session_refresh_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/e;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "refreshSession"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionRefreshListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;)Lcom/swedbank/mobile/business/authentication/session/refresh/e;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/e;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 8

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/refresh/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/refresh/g;->a()V

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/b;

    new-instance v7, Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/business/authentication/session/refresh/a;-><init>(ZJILkotlin/e/b/g;)V

    invoke-interface {v0, v7}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    .line 24
    new-instance v2, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 22
    invoke-static {v0, v2, v1}, Lio/reactivex/i/f;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;)Lio/reactivex/b/c;

    move-result-object v0

    .line 29
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
