.class public final Lcom/swedbank/mobile/business/authentication/session/b;
.super Ljava/lang/Object;
.source "InvalidateFullSession.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/privacy/f;

.field private final b:Lcom/swedbank/mobile/business/authentication/session/m;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/privacy/f;Lcom/swedbank/mobile/business/authentication/session/m;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/privacy/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "clearAppDataUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/privacy/f;",
            "Lcom/swedbank/mobile/business/authentication/session/m;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "userPrivacyRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clearData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/b;->a:Lcom/swedbank/mobile/business/privacy/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/b;->b:Lcom/swedbank/mobile/business/authentication/session/m;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/session/b;->c:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/b;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/b;->c:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/session/b;)Lcom/swedbank/mobile/business/authentication/session/m;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/b;->b:Lcom/swedbank/mobile/business/authentication/session/m;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/b;->a:Lcom/swedbank/mobile/business/privacy/f;

    .line 18
    invoke-interface {v0}, Lcom/swedbank/mobile/business/privacy/f;->a()Lio/reactivex/w;

    move-result-object v0

    .line 19
    new-instance v1, Lcom/swedbank/mobile/business/authentication/session/b$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/authentication/session/b$a;-><init>(Lcom/swedbank/mobile/business/authentication/session/b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "userPrivacyRepository\n  \u2026ssion()\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/b;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
