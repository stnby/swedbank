.class final synthetic Lcom/swedbank/mobile/business/authentication/session/scoped/a$a;
.super Lkotlin/e/b/i;
.source "AcquireScopedSession.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/session/scoped/a;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/session/scoped/j;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/a$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    .line 22
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/session/scoped/j;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/scoped/a$a;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "saveSession"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "saveSession(Lcom/swedbank/mobile/business/authentication/session/Session;)Lio/reactivex/Completable;"

    return-object v0
.end method
