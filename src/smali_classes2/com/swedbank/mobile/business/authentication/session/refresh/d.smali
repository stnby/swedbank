.class public final Lcom/swedbank/mobile/business/authentication/session/refresh/d;
.super Ljava/lang/Object;
.source "SessionRefreshInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/e;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/d;->a:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/d;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/session/refresh/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/e;",
            ">;)",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/d;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/authentication/session/refresh/d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/authentication/session/refresh/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;
    .locals 3

    .line 24
    new-instance v0, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/d;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/b;

    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/d;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/authentication/session/refresh/e;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;-><init>(Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/authentication/session/refresh/e;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/refresh/d;->a()Lcom/swedbank/mobile/business/authentication/session/refresh/SessionRefreshInteractorImpl;

    move-result-object v0

    return-object v0
.end method
