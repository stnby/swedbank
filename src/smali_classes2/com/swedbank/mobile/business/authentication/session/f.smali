.class public final Lcom/swedbank/mobile/business/authentication/session/f;
.super Ljava/lang/Object;
.source "SaveFullSession.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fullSessionRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/f;->a:Lcom/swedbank/mobile/business/authentication/session/a;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/f;->a:Lcom/swedbank/mobile/business/authentication/session/a;

    .line 13
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/session/a;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/f;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
