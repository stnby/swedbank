.class public final Lcom/swedbank/mobile/business/authentication/session/refresh/b;
.super Ljava/lang/Object;
.source "RefreshSession.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/l;

.field private final b:Lcom/swedbank/mobile/business/authentication/session/m;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/l;Lcom/swedbank/mobile/business/authentication/session/m;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "saveSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "invalidateSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/l;",
            "Lcom/swedbank/mobile/business/authentication/session/m;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "saveSession"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invalidateSession"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->a:Lcom/swedbank/mobile/business/authentication/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->b:Lcom/swedbank/mobile/business/authentication/session/m;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->c:Lcom/swedbank/mobile/architect/business/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->d:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/business/authentication/session/m;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->b:Lcom/swedbank/mobile/business/authentication/session/m;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/business/authentication/l;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->a:Lcom/swedbank/mobile/business/authentication/l;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->c:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->d:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/refresh/a;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/refresh/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;-><init>(Lcom/swedbank/mobile/business/authentication/session/refresh/b;Lcom/swedbank/mobile/business/authentication/session/refresh/a;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    .line 68
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.fromCallable {\n  \u2026scribeOn(Schedulers.io())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->a(Lcom/swedbank/mobile/business/authentication/session/refresh/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
