.class public final Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "SessionInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/login/handling/b;
.implements Lcom/swedbank/mobile/business/authentication/session/refresh/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/session/n;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/login/handling/b;",
        "Lcom/swedbank/mobile/business/authentication/session/refresh/e;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/m;

.field private final b:Lcom/swedbank/mobile/business/e/b;

.field private final c:Lcom/swedbank/mobile/business/authentication/session/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/m;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/authentication/session/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation runtime Ljavax/inject/Named;
            value = "session_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sessionRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->a:Lcom/swedbank/mobile/business/authentication/session/m;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->b:Lcom/swedbank/mobile/business/e/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/session/l;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->b:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)Lcom/swedbank/mobile/business/authentication/session/n;
    .locals 0

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/session/n;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)Lcom/swedbank/mobile/business/authentication/session/l;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->k_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/n;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/n;->d()V

    :cond_0
    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/v;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "loginState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->j()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/session/l;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/l;->a()V

    return-void
.end method

.method protected m_()V
    .locals 10

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->a:Lcom/swedbank/mobile/business/authentication/session/m;

    .line 34
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/m;->d()Lio/reactivex/o;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lio/reactivex/o;->l()Lio/reactivex/e/a;

    move-result-object v0

    const-string v1, "stream"

    .line 98
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Lio/reactivex/o;

    .line 99
    sget-object v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$a;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    const-class v3, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Ljava/lang/Class;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, "filter { it is R }.cast(R::class.java)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v3, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 48
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    const-string v2, "sessionStream\n          \u2026  .distinctUntilChanged()"

    invoke-static {v3, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$c;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)V

    move-object v6, v2

    check-cast v6, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v2

    .line 100
    invoke-static {p0, v2}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 59
    sget-object v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$f;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$f;

    check-cast v2, Lio/reactivex/c/d;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v2

    .line 60
    sget-object v3, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$g;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$g;

    check-cast v3, Lio/reactivex/c/k;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v4

    const-string v2, "sessionStream\n          \u2026tive && !it.isExpired() }"

    invoke-static {v4, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)V

    move-object v7, v2

    check-cast v7, Lkotlin/e/a/b;

    const/4 v6, 0x0

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v2

    .line 102
    invoke-static {p0, v2}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 71
    sget-object v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$h;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$h;

    check-cast v2, Lkotlin/e/a/m;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/business/authentication/session/k;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/authentication/session/k;-><init>(Lkotlin/e/a/m;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/d;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v1

    .line 72
    sget-object v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$i;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$i;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 73
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v1

    .line 74
    sget-object v2, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$j;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$j;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v3

    const-string v1, "sessionStream\n          \u2026{ it !is Session.Active }"

    invoke-static {v3, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 75
    new-instance v1, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)V

    move-object v6, v1

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v1

    .line 104
    invoke-static {p0, v1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 106
    invoke-virtual {v0}, Lio/reactivex/e/a;->a()Lio/reactivex/b/c;

    return-void
.end method
