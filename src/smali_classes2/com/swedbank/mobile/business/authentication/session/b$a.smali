.class final Lcom/swedbank/mobile/business/authentication/session/b$a;
.super Ljava/lang/Object;
.source "InvalidateFullSession.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/session/b;->a()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/privacy/e;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/session/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/session/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/privacy/e;)Lio/reactivex/f;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/privacy/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "userDataSaving"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object v0, Lcom/swedbank/mobile/business/authentication/session/c;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/privacy/e;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 23
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/b;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/session/b;->b(Lcom/swedbank/mobile/business/authentication/session/b;)Lcom/swedbank/mobile/business/authentication/session/m;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/authentication/session/m;->e()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 22
    :pswitch_1
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/b;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/session/b;->a(Lcom/swedbank/mobile/business/authentication/session/b;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/privacy/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/b$a;->a(Lcom/swedbank/mobile/business/privacy/e;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
