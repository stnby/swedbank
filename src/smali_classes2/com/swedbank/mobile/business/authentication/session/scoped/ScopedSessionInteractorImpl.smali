.class public final Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ScopedSessionInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/session/scoped/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

.field private final b:Lcom/swedbank/mobile/business/authentication/session/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/scoped/j;Lcom/swedbank/mobile/business/authentication/session/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/scoped/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation runtime Ljavax/inject/Named;
            value = "session_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sessionRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;->a:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/session/l;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;)Lcom/swedbank/mobile/business/authentication/session/scoped/k;
    .locals 0

    .line 16
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;->k_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/session/scoped/k;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;)Lcom/swedbank/mobile/business/authentication/session/l;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 8

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;->a:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    .line 22
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/scoped/j;->d()Lio/reactivex/o;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/m;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/authentication/session/scoped/h;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/authentication/session/scoped/h;-><init>(Lkotlin/e/a/m;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/d;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "sessionRepository\n      \u2026hanged(::classesAreEqual)"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/authentication/session/scoped/ScopedSessionInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 35
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
