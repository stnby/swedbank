.class public final Lcom/swedbank/mobile/business/authentication/session/scoped/d;
.super Ljava/lang/Object;
.source "InvalidateScopedSession.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/session/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/session/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sessionRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/d;->a:Lcom/swedbank/mobile/business/authentication/session/m;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/d;->a:Lcom/swedbank/mobile/business/authentication/session/m;

    .line 12
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/m;->e()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/scoped/d;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
