.class final Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;
.super Ljava/lang/Object;
.source "RefreshSession.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/session/refresh/b;->a(Lcom/swedbank/mobile/business/authentication/session/refresh/a;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/session/refresh/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/session/refresh/b;Lcom/swedbank/mobile/business/authentication/session/refresh/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/session/h;
    .locals 5

    .line 29
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->a(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/business/authentication/session/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/m;->b()Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v0

    .line 31
    instance-of v1, v0, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v1, :cond_6

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/session/refresh/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/session/refresh/a;->a()Z

    move-result v1

    if-nez v1, :cond_2

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->d()J

    move-result-wide v1

    iget-object v3, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/authentication/session/refresh/a;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 34
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Token was actually not expired. RefreshRequest="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->b:Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    return-object v0

    .line 37
    :cond_2
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    monitor-enter v1

    .line 39
    :try_start_0
    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    invoke-static {v2}, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->a(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/business/authentication/session/m;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/session/m;->b()Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v2

    .line 41
    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 42
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :try_start_1
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->b(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/business/authentication/l;

    move-result-object v0

    .line 45
    invoke-interface {v0, v2}, Lcom/swedbank/mobile/business/authentication/l;->b(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/w;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/reactivex/w;->b()Ljava/lang/Object;

    move-result-object v0

    .line 44
    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/h;

    .line 47
    instance-of v2, v0, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v2, :cond_3

    .line 48
    sget-object v2, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 49
    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    invoke-static {v2}, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->c(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/b;

    invoke-virtual {v2}, Lio/reactivex/b;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v0

    .line 54
    :catch_0
    :try_start_3
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 57
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a:Lcom/swedbank/mobile/business/authentication/session/refresh/b;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/refresh/b;->d(Lcom/swedbank/mobile/business/authentication/session/refresh/b;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    invoke-virtual {v0}, Lio/reactivex/b;->b()V

    goto :goto_0

    .line 59
    :cond_4
    instance-of v0, v2, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v0, :cond_5

    .line 61
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    monitor-exit v1

    return-object v2

    .line 64
    :cond_5
    :goto_0
    :try_start_4
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 38
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 66
    :cond_6
    :goto_1
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    .line 67
    sget-object v0, Lcom/swedbank/mobile/business/authentication/session/h$b;->a:Lcom/swedbank/mobile/business/authentication/session/h$b;

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/h;

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/session/refresh/b$a;->a()Lcom/swedbank/mobile/business/authentication/session/h;

    move-result-object v0

    return-object v0
.end method
