.class final Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b;
.super Ljava/lang/Object;
.source "SessionInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/session/h$a;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/h$a;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b;->a:Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl;)Lcom/swedbank/mobile/business/e/b;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->a()Lio/reactivex/o;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/authentication/session/h$a;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 44
    sget-object v1, Lcom/swedbank/mobile/business/authentication/session/i;->a:Lcom/swedbank/mobile/business/authentication/session/i;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->a()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 40
    invoke-static {v0, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/SessionInteractorImpl$b;->a(Lcom/swedbank/mobile/business/authentication/session/h$a;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
