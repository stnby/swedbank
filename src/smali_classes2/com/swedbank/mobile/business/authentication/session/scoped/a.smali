.class public final Lcom/swedbank/mobile/business/authentication/session/scoped/a;
.super Ljava/lang/Object;
.source "AcquireScopedSession.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/authentication/session/h;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/l;

.field private final b:Lcom/swedbank/mobile/business/authentication/session/scoped/j;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/l;Lcom/swedbank/mobile/business/authentication/session/scoped/j;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/scoped/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopedSessionRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/a;->b:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v0, :cond_1

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/o;->c()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/a;->a:Lcom/swedbank/mobile/business/authentication/l;

    .line 21
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/l;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/authentication/session/scoped/a$a;

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/session/scoped/a;->b:Lcom/swedbank/mobile/business/authentication/session/scoped/j;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/authentication/session/scoped/a$a;-><init>(Lcom/swedbank/mobile/business/authentication/session/scoped/j;)V

    check-cast v0, Lkotlin/e/a/b;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/session/scoped/b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/authentication/session/scoped/b;-><init>(Lkotlin/e/a/b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    .line 23
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "authenticationRepository\u2026       .onErrorComplete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Id token cannot be missing"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 14
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Session must be active to acquire scoped session"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/session/scoped/a;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
