.class public final Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "AuthenticationInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/challenge/d;
.implements Lcom/swedbank/mobile/business/general/confirmation/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/n;",
        ">;",
        "Lcom/swedbank/mobile/business/challenge/d;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lio/reactivex/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/b<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/authentication/login/g;

.field private final d:Lcom/swedbank/mobile/business/authentication/e;

.field private final e:Lcom/swedbank/mobile/business/authentication/j;

.field private final f:Lcom/swedbank/mobile/business/authentication/o;

.field private final g:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/g;Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/j;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_credentials"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/authentication/j;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "authenticateUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "saveLoginInfoUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "clearAppDataUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            "Lcom/swedbank/mobile/business/authentication/e;",
            "Lcom/swedbank/mobile/business/authentication/j;",
            "Lcom/swedbank/mobile/business/authentication/o;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loginCredentialsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationCredentials"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationStream"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticate"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "saveLoginInfo"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clearAppData"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/g;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->d:Lcom/swedbank/mobile/business/authentication/e;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->e:Lcom/swedbank/mobile/business/authentication/j;

    iput-object p4, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->f:Lcom/swedbank/mobile/business/authentication/o;

    iput-object p5, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    iput-object p6, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/b;

    iput-object p7, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/g;

    .line 44
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<LoginState>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/o;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->f:Lcom/swedbank/mobile/business/authentication/o;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;Lio/reactivex/k/b;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->b:Lio/reactivex/k/b;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/j;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->e:Lcom/swedbank/mobile/business/authentication/j;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/g;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/g;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lio/reactivex/k/b;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->b:Lio/reactivex/k/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/n;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/n;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/n;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->k_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/n;

    return-object p0
.end method


# virtual methods
.method public b()V
    .locals 2

    .line 136
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/n;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/n;->c()V

    .line 137
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->a:Lcom/b/c/c;

    sget-object v1, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->b:Lio/reactivex/k/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/reactivex/k/b;->onComplete()V

    :cond_0
    const/4 v0, 0x0

    .line 139
    check-cast v0, Lio/reactivex/k/b;

    iput-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->b:Lio/reactivex/k/b;

    return-void
.end method

.method public i()V
    .locals 2

    .line 190
    invoke-static {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->d(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 197
    invoke-static {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->j(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/n;->a()V

    :cond_0
    return-void
.end method

.method protected m_()V
    .locals 10

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->f:Lcom/swedbank/mobile/business/authentication/o;

    .line 49
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/o;->c()Lio/reactivex/o;

    move-result-object v1

    .line 50
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$f;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 169
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->a:Lcom/b/c/c;

    .line 54
    invoke-virtual {v0}, Lcom/b/c/c;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v0, "loginProgressStream\n    \u2026  .distinctUntilChanged()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 171
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->d:Lcom/swedbank/mobile/business/authentication/e;

    .line 173
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e;->a()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-static {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->c(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/g;

    move-result-object v2

    .line 180
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/login/g;->b()Lio/reactivex/w;

    move-result-object v2

    .line 179
    new-instance v3, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$a;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/w;->a(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object v2

    .line 178
    new-instance v3, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;

    invoke-direct {v3, p0, v1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v2

    .line 177
    new-instance v3, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;

    invoke-direct {v3, p0, v0, v1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;Lcom/swedbank/mobile/business/authentication/e;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/j;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v0, "loginCredentialsReposito\u2026              }\n        }"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)V

    move-object v7, v0

    check-cast v7, Lkotlin/e/a/b;

    .line 182
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v8, 0x2

    const/4 v9, 0x0

    .line 176
    invoke-static/range {v4 .. v9}, Lio/reactivex/i/f;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 183
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public q_()V
    .locals 2

    .line 130
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/n;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/n;->c()V

    .line 131
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->b:Lio/reactivex/k/b;

    if-eqz v0, :cond_0

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lio/reactivex/k/b;->a_(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x0

    .line 132
    check-cast v0, Lio/reactivex/k/b;

    iput-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->b:Lio/reactivex/k/b;

    return-void
.end method
