.class public final Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;
.super Ljava/lang/Object;
.source "AuthenticationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/e;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;Lcom/swedbank/mobile/business/authentication/e;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->b:Lcom/swedbank/mobile/business/authentication/e;

    iput-object p3, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->h(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->b:Lcom/swedbank/mobile/business/authentication/e;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/o;

    .line 85
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->d(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$1;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$1;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 86
    new-instance v0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c$2;-><init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$c;->a(Lkotlin/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
