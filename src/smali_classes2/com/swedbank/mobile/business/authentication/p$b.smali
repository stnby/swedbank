.class public final enum Lcom/swedbank/mobile/business/authentication/p$b;
.super Ljava/lang/Enum;
.source "BiometricAuthenticationResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/authentication/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/authentication/p$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum b:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum c:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum d:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum e:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum f:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum g:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum h:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum i:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum j:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum k:Lcom/swedbank/mobile/business/authentication/p$b;

.field public static final enum l:Lcom/swedbank/mobile/business/authentication/p$b;

.field private static final synthetic m:[Lcom/swedbank/mobile/business/authentication/p$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/swedbank/mobile/business/authentication/p$b;

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "INVALID_KEY"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->a:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "CANCELED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->b:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "HW_NOT_PRESENT"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->c:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "HW_UNAVAILABLE"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->d:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "LOCKOUT"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->e:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "LOCKOUT_PERMANENT"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->f:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "NO_BIOMETRICS"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->g:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "NO_SPACE"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->h:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "TIMEOUT"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->i:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "UNABLE_TO_PROCESS"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->j:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "USER_CANCELED"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->k:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$b;

    const-string v2, "VENDOR"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->l:Lcom/swedbank/mobile/business/authentication/p$b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/authentication/p$b;->m:[Lcom/swedbank/mobile/business/authentication/p$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/authentication/p$b;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/authentication/p$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/p$b;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/authentication/p$b;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/authentication/p$b;->m:[Lcom/swedbank/mobile/business/authentication/p$b;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/authentication/p$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/authentication/p$b;

    return-object v0
.end method
