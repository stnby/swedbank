.class public abstract Lcom/swedbank/mobile/business/authentication/e;
.super Ljava/lang/Object;
.source "AuthenticationCredentials.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/authentication/e$a;,
        Lcom/swedbank/mobile/business/authentication/e$c;,
        Lcom/swedbank/mobile/business/authentication/e$b;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/swedbank/mobile/business/authentication/e;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public final b()Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/e$c;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_0

    .line 26
    :cond_0
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/e$a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->c:Lcom/swedbank/mobile/business/authentication/login/m;

    goto :goto_0

    .line 27
    :cond_1
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/e$b;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/swedbank/mobile/business/authentication/login/m;->d:Lcom/swedbank/mobile/business/authentication/login/m;

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/e$c;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/e$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e$c;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 33
    :cond_0
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/e$a;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e$a;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 34
    :cond_1
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/e$b;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/e$b;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
