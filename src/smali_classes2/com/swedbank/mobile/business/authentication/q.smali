.class public final Lcom/swedbank/mobile/business/authentication/q;
.super Ljava/lang/Object;
.source "GetLastUsedAuthenticationCredentials.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/util/l<",
        "Lcom/swedbank/mobile/business/authentication/e;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loginCredentialsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/q;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/q;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    .line 15
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/g;->d()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/q;->a:Lcom/swedbank/mobile/business/authentication/login/g;

    .line 18
    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/authentication/login/g;->a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, ""

    const-string v3, ""

    .line 19
    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v1

    .line 20
    new-instance v2, Lcom/swedbank/mobile/business/authentication/q$a;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/business/authentication/q$a;-><init>(Lcom/swedbank/mobile/business/authentication/login/m;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "loginCredentialsReposito\u2026oOptional()\n            }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "loginCredentialsReposito\u2026l()\n            }\n      }"

    .line 16
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/q;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
