.class public final Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PinCalculatorLoginMethodInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/c;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/authentication/o;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/authentication/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getLastUsedAuthenticationCredentialsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "pin_calculator_login_method_authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "getLastUsedAuthenticationCredentials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/g;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;)Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/c;
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/o;

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/o;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/o;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/o;->b()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "password"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 36
    new-instance v1, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2, p1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 54
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
