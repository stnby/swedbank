.class final Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl$a;
.super Lkotlin/e/b/k;
.source "PollingLoginMethodInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/util/l<",
        "Lcom/swedbank/mobile/business/authentication/e;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;)V"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;)Lcom/swedbank/mobile/business/authentication/recurring/polling/c;

    move-result-object v0

    .line 44
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 45
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    .line 31
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/polling/c;->a(Lcom/swedbank/mobile/business/authentication/e;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 30
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot execute polling login method if there are no saved credentials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl$a;->a(Lcom/swedbank/mobile/business/util/l;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
