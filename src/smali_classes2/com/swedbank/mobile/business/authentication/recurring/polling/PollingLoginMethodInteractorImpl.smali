.class public final Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PollingLoginMethodInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/recurring/polling/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/authentication/recurring/polling/c;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/recurring/polling/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/authentication/o;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/authentication/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getLastUsedAuthenticationCredentialsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "polling_login_method_authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "getLastUsedAuthenticationCredentials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/g;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;)Lcom/swedbank/mobile/business/authentication/recurring/polling/c;
    .locals 0

    .line 22
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/o;

    .line 38
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/o;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 4

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 28
    new-instance v1, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 43
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/polling/PollingLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/o;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/o;->b()V

    return-void
.end method
