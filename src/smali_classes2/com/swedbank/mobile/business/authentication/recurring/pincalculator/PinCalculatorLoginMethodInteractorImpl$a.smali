.class final Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;
.super Lkotlin/e/b/k;
.source "PinCalculatorLoginMethodInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->b(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/util/l<",
        "Lcom/swedbank/mobile/business/authentication/e;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;->b:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;)V"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 56
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    .line 40
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/e$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;->a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;)Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/c;

    move-result-object v0

    .line 41
    check-cast p1, Lcom/swedbank/mobile/business/authentication/e$b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;->b:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/authentication/e$b;->a(Lcom/swedbank/mobile/business/authentication/e$b;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/business/authentication/e$b;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/e;

    .line 40
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/c;->a(Lcom/swedbank/mobile/business/authentication/e;)V

    return-void

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot execute pin calc login method for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 44
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 45
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot execute pin calc login method if there are no saved credentials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl$a;->a(Lcom/swedbank/mobile/business/util/l;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
