.class public final Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;
.super Ljava/lang/Object;
.source "PinCalculatorLoginMethodInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;->a:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/e;",
            ">;>;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;)",
            "Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;
    .locals 3

    .line 26
    new-instance v0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/g;

    iget-object v2, p0, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/authentication/o;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;-><init>(Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/authentication/o;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/b;->a()Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/PinCalculatorLoginMethodInteractorImpl;

    move-result-object v0

    return-object v0
.end method
