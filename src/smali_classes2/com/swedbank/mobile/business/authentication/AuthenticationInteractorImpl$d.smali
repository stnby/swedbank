.class public final Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;
.super Lkotlin/e/b/k;
.source "AuthenticationInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/authentication/k;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/k;)V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->d(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    const-string v1, "loginResult"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/i;->a(Lcom/swedbank/mobile/business/authentication/k;)Lcom/swedbank/mobile/business/authentication/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 99
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/k$c;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->f(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/n;

    move-result-object v0

    .line 101
    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/authentication/k$c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/k$c;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 169
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/k;->a()Ljava/util/List;

    move-result-object p1

    .line 170
    sget-object v2, Lcom/swedbank/mobile/business/authentication/k$e;->a:Lcom/swedbank/mobile/business/authentication/k$e;

    check-cast v2, Lkotlin/e/a/b;

    .line 171
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 176
    invoke-static {p1, v2}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 175
    sget-object v2, Lcom/swedbank/mobile/business/authentication/m;->a:Lkotlin/h/i;

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {p1, v2}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 174
    invoke-static {p1}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p1

    .line 100
    new-instance v2, Lcom/swedbank/mobile/business/challenge/a;

    invoke-direct {v2, v1, p1}, Lcom/swedbank/mobile/business/challenge/a;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v2}, Lcom/swedbank/mobile/business/authentication/n;->a(Lcom/swedbank/mobile/business/challenge/a;)V

    goto :goto_0

    .line 101
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot display challenge node without challenge code"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 105
    :cond_1
    instance-of p1, p1, Lcom/swedbank/mobile/business/authentication/k$b;

    if-eqz p1, :cond_2

    .line 106
    iget-object p1, p0, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;

    .line 177
    invoke-static {p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;->j(Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/authentication/n;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/swedbank/mobile/business/authentication/n;->a()V

    :cond_2
    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/authentication/AuthenticationInteractorImpl$d;->a(Lcom/swedbank/mobile/business/authentication/k;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
