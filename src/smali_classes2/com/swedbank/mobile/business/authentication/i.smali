.class public final Lcom/swedbank/mobile/business/authentication/i;
.super Ljava/lang/Object;
.source "AuthenticationInteractor.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/authentication/k;)Lcom/swedbank/mobile/business/authentication/v;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/business/authentication/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toLoginState"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/k$a;

    if-eqz v0, :cond_0

    sget-object p0, Lcom/swedbank/mobile/business/authentication/v$a;->a:Lcom/swedbank/mobile/business/authentication/v$a;

    check-cast p0, Lcom/swedbank/mobile/business/authentication/v;

    goto :goto_0

    .line 162
    :cond_0
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/k$c;

    if-eqz v0, :cond_1

    sget-object p0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    check-cast p0, Lcom/swedbank/mobile/business/authentication/v;

    goto :goto_0

    .line 163
    :cond_1
    instance-of v0, p0, Lcom/swedbank/mobile/business/authentication/k$b;

    if-eqz v0, :cond_3

    .line 164
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/k$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/k$b;->b()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance p0, Lcom/swedbank/mobile/business/authentication/v$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/k$b;->b()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/authentication/v$c;-><init>(Ljava/lang/Throwable;)V

    check-cast p0, Lcom/swedbank/mobile/business/authentication/v;

    goto :goto_0

    .line 165
    :cond_2
    new-instance v0, Lcom/swedbank/mobile/business/authentication/v$e;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/authentication/k;->a()Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/authentication/v$e;-><init>(Ljava/util/List;)V

    move-object p0, v0

    check-cast p0, Lcom/swedbank/mobile/business/authentication/v;

    :goto_0
    return-object p0

    .line 163
    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
