.class public final enum Lcom/swedbank/mobile/business/e/a;
.super Ljava/lang/Enum;
.source "ActivityLifecycleMonitor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/e/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/e/a;

.field public static final enum b:Lcom/swedbank/mobile/business/e/a;

.field public static final enum c:Lcom/swedbank/mobile/business/e/a;

.field public static final enum d:Lcom/swedbank/mobile/business/e/a;

.field public static final enum e:Lcom/swedbank/mobile/business/e/a;

.field public static final enum f:Lcom/swedbank/mobile/business/e/a;

.field public static final enum g:Lcom/swedbank/mobile/business/e/a;

.field private static final synthetic h:[Lcom/swedbank/mobile/business/e/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/swedbank/mobile/business/e/a;

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "CREATE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->a:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "START"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->b:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "RESUME"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->c:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "PAUSE"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->d:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "STOP"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->e:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "DESTROY"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->f:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/a;

    const-string v2, "NIL"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/a;->g:Lcom/swedbank/mobile/business/e/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/e/a;->h:[Lcom/swedbank/mobile/business/e/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/e/a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/e/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/e/a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/e/a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/e/a;->h:[Lcom/swedbank/mobile/business/e/a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/e/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/e/a;

    return-object v0
.end method
