.class public final enum Lcom/swedbank/mobile/business/e/k;
.super Ljava/lang/Enum;
.source "PermissionManager.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/e/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/e/k;

.field public static final enum b:Lcom/swedbank/mobile/business/e/k;

.field public static final enum c:Lcom/swedbank/mobile/business/e/k;

.field private static final synthetic d:[Lcom/swedbank/mobile/business/e/k;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/e/k;

    new-instance v1, Lcom/swedbank/mobile/business/e/k;

    const-string v2, "GRANTED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/k;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/k;->a:Lcom/swedbank/mobile/business/e/k;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/k;

    const-string v2, "DENIED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/k;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/k;->b:Lcom/swedbank/mobile/business/e/k;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/k;

    const-string v2, "DENIED_PERMANENTLY"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/k;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/k;->c:Lcom/swedbank/mobile/business/e/k;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/e/k;->d:[Lcom/swedbank/mobile/business/e/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/e/k;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/e/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/e/k;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/e/k;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/e/k;->d:[Lcom/swedbank/mobile/business/e/k;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/e/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/e/k;

    return-object v0
.end method
