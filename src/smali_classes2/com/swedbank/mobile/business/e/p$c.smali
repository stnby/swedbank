.class public final enum Lcom/swedbank/mobile/business/e/p$c;
.super Ljava/lang/Enum;
.source "DeviceRepository.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/e/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/e/p$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/e/p$c;

.field public static final enum b:Lcom/swedbank/mobile/business/e/p$c;

.field public static final enum c:Lcom/swedbank/mobile/business/e/p$c;

.field public static final enum d:Lcom/swedbank/mobile/business/e/p$c;

.field public static final enum e:Lcom/swedbank/mobile/business/e/p$c;

.field public static final enum f:Lcom/swedbank/mobile/business/e/p$c;

.field private static final synthetic g:[Lcom/swedbank/mobile/business/e/p$c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/swedbank/mobile/business/e/p$c;

    new-instance v1, Lcom/swedbank/mobile/business/e/p$c;

    const-string v2, "OS_API_LEVEL"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/p$c;->a:Lcom/swedbank/mobile/business/e/p$c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/p$c;

    const-string v2, "NO_NFC"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/p$c;->b:Lcom/swedbank/mobile/business/e/p$c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/p$c;

    const-string v2, "NO_HCE"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/p$c;->c:Lcom/swedbank/mobile/business/e/p$c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/p$c;

    const-string v2, "ROOTED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/p$c;->d:Lcom/swedbank/mobile/business/e/p$c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/p$c;

    const-string v2, "FEATURE_DISABLED"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/p$c;->e:Lcom/swedbank/mobile/business/e/p$c;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/p$c;

    const-string v2, "OTHER"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/p$c;->f:Lcom/swedbank/mobile/business/e/p$c;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/e/p$c;->g:[Lcom/swedbank/mobile/business/e/p$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/e/p$c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/e/p$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/e/p$c;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/e/p$c;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/e/p$c;->g:[Lcom/swedbank/mobile/business/e/p$c;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/e/p$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/e/p$c;

    return-object v0
.end method
