.class public final enum Lcom/swedbank/mobile/business/e/l;
.super Ljava/lang/Enum;
.source "DeviceRepository.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/e/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/e/l;

.field public static final enum b:Lcom/swedbank/mobile/business/e/l;

.field public static final enum c:Lcom/swedbank/mobile/business/e/l;

.field public static final enum d:Lcom/swedbank/mobile/business/e/l;

.field public static final enum e:Lcom/swedbank/mobile/business/e/l;

.field public static final enum f:Lcom/swedbank/mobile/business/e/l;

.field public static final enum g:Lcom/swedbank/mobile/business/e/l;

.field public static final enum h:Lcom/swedbank/mobile/business/e/l;

.field public static final enum i:Lcom/swedbank/mobile/business/e/l;

.field public static final enum j:Lcom/swedbank/mobile/business/e/l;

.field public static final enum k:Lcom/swedbank/mobile/business/e/l;

.field public static final enum l:Lcom/swedbank/mobile/business/e/l;

.field public static final enum m:Lcom/swedbank/mobile/business/e/l;

.field public static final enum n:Lcom/swedbank/mobile/business/e/l;

.field public static final enum o:Lcom/swedbank/mobile/business/e/l;

.field public static final enum p:Lcom/swedbank/mobile/business/e/l;

.field public static final enum q:Lcom/swedbank/mobile/business/e/l;

.field private static final synthetic r:[Lcom/swedbank/mobile/business/e/l;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/swedbank/mobile/business/e/l;

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->a:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_BBOX"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->b:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_BIN"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->c:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_CLOAK_APPS"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->d:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_DANG_APPS"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->e:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_MGM_APPS"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->f:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_SU"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->g:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_DANG_PROPS"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->h:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_TEST_KEYS"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->i:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_ALCATEL"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->j:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_MEMORY"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->k:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_PROC"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->l:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_STRINGS"

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->m:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEVICE_FILES"

    const/16 v3, 0xd

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->n:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_CONNECTION"

    const/16 v3, 0xe

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->o:Lcom/swedbank/mobile/business/e/l;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_STACKTRACE"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->p:Lcom/swedbank/mobile/business/e/l;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/e/l;

    const-string v2, "ROOTED_DEBUGGABLE"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/e/l;->q:Lcom/swedbank/mobile/business/e/l;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/business/e/l;->r:[Lcom/swedbank/mobile/business/e/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/e/l;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/e/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/e/l;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/e/l;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/e/l;->r:[Lcom/swedbank/mobile/business/e/l;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/e/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/e/l;

    return-object v0
.end method
