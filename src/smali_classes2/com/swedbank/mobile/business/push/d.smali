.class public final Lcom/swedbank/mobile/business/push/d;
.super Ljava/lang/Object;
.source "DeactivatePushService_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/push/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/h;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/h/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/h/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/swedbank/mobile/business/push/d;->a:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/swedbank/mobile/business/push/d;->b:Ljavax/inject/Provider;

    .line 22
    iput-object p3, p0, Lcom/swedbank/mobile/business/push/d;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/h/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/business/push/d;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/swedbank/mobile/business/push/d;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/push/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/push/c;
    .locals 4

    .line 27
    new-instance v0, Lcom/swedbank/mobile/business/push/c;

    iget-object v1, p0, Lcom/swedbank/mobile/business/push/d;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/push/h;

    iget-object v2, p0, Lcom/swedbank/mobile/business/push/d;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/h/c;

    iget-object v3, p0, Lcom/swedbank/mobile/business/push/d;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/business/g;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/push/c;-><init>(Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/business/h/c;Lcom/swedbank/mobile/architect/business/g;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/d;->a()Lcom/swedbank/mobile/business/push/c;

    move-result-object v0

    return-object v0
.end method
