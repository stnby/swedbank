.class public final Lcom/swedbank/mobile/business/push/a$b$a;
.super Ljava/lang/Object;
.source "ActivatePushService.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/a$b;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/push/a$b;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/a$b;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/a$b$a;->a:Lcom/swedbank/mobile/business/push/a$b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/push/a$b$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/w;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/security/PublicKey;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/PublicKey;

    .line 45
    check-cast p1, Ljava/security/Key;

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/a$b$a;->a:Lcom/swedbank/mobile/business/push/a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/push/a$b;->a:Lcom/swedbank/mobile/business/push/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/push/a;->a(Lcom/swedbank/mobile/business/push/a;)Lcom/swedbank/mobile/business/h/c;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/swedbank/mobile/business/push/a$b$a;->b:Ljava/lang/String;

    const-string v2, "mobileAppId"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/h/c;->a(Ljava/lang/String;Ljava/security/Key;)Lio/reactivex/w;

    move-result-object p1

    .line 55
    new-instance v0, Lcom/swedbank/mobile/business/push/a$b$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/push/a$b$a$a;-><init>(Lcom/swedbank/mobile/business/push/a$b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "serviceOrderingRepositor\u2026        }\n              }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 44
    new-instance p1, Lcom/swedbank/mobile/business/util/p$a;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, v1, v0, v1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/a$b$a;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
