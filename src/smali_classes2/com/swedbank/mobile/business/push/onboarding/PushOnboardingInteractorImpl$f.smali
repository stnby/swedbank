.class final Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$f;
.super Ljava/lang/Object;
.source "PushOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lkotlin/s;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$f;->a:Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/b;
    .locals 2
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object p1, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$f;->a:Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;

    .line 112
    invoke-static {p1}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->c(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-static {p1}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 116
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$g;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "activatePushService()\n  \u2026        }\n              }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_0
    invoke-static {p1}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->e(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/push/h;

    move-result-object v0

    .line 127
    sget-object v1, Lcom/swedbank/mobile/business/push/i;->c:Lcom/swedbank/mobile/business/push/i;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/push/h;->a(Lcom/swedbank/mobile/business/push/i;)Lio/reactivex/b;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance p1, Lcom/swedbank/mobile/business/push/onboarding/c;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/push/onboarding/c;-><init>(Lkotlin/e/a/a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "pushRepository\n         \u2026otifyOnboardingComplete))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$f;->a(Lkotlin/s;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
