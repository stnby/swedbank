.class public final Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PushOnboardingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/error/c;
.implements Lcom/swedbank/mobile/business/push/onboarding/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/push/onboarding/f;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/error/c;",
        "Lcom/swedbank/mobile/business/push/onboarding/a;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final b:Lcom/swedbank/mobile/business/push/h;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/push/onboarding/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "activatePushServiceUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "push_onboarding_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/h;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/push/onboarding/e;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activatePushService"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/push/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    iput-object p3, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/util/e;

    .line 40
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/push/onboarding/f;
    .locals 0

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/push/onboarding/f;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/push/onboarding/f;
    .locals 0

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->k_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/push/onboarding/f;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/push/h;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/push/h;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/util/e;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/util/e;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 74
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/onboarding/f;->a()V

    .line 124
    invoke-static {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 126
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/e;

    .line 127
    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/onboarding/e;->a()V

    goto :goto_0

    .line 128
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    const-string v1, "feature_push"

    .line 129
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/onboarding/f;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public a(Z)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public b()V
    .locals 4

    .line 79
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/onboarding/f;->a()V

    .line 132
    invoke-static {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->c(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-static {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 136
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "activatePushService()\n  \u2026        }\n              }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->e(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/push/h;

    move-result-object v0

    .line 147
    sget-object v1, Lcom/swedbank/mobile/business/push/i;->c:Lcom/swedbank/mobile/business/push/i;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/push/h;->a(Lcom/swedbank/mobile/business/push/i;)Lio/reactivex/b;

    move-result-object v0

    .line 146
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/business/push/onboarding/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/push/onboarding/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "pushRepository\n         \u2026otifyOnboardingComplete))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 151
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/util/e;

    .line 121
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/e;

    .line 68
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    .line 65
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->a()Lio/reactivex/o;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$d;->a:Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$d;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/swedbank/mobile/business/push/onboarding/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/push/onboarding/d;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "onboardingStream\n       \u2026       .map(Enabled::not)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const-string v1, "listener.fold(\n      ifL\u2026 Observable.empty() }\n  )"

    .line 123
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 66
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method protected m_()V
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/util/e;

    instance-of v0, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/util/e;

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    .line 45
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->a()Lio/reactivex/o;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 46
    new-instance v0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 112
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/util/e;

    .line 115
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/e;

    .line 56
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(Unit)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    .line 55
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->d()Lio/reactivex/w;

    move-result-object v0

    .line 57
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "listener.fold(\n        i\u2026difyPushServiceStatus() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v1, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 118
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void

    .line 55
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
