.class public final synthetic Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;
.super Lkotlin/e/b/i;
.source "PushOnboardingInteractor.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "notifyOnboardingComplete"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "notifyOnboardingComplete()V"

    return-object v0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;

    .line 112
    invoke-static {v0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 114
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/onboarding/e;

    .line 115
    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/onboarding/e;->a()V

    goto :goto_0

    .line 116
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    const-string v1, "feature_push"

    .line 117
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/onboarding/f;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/onboarding/PushOnboardingInteractorImpl$c;->d()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
