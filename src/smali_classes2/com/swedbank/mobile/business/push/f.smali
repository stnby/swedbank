.class public final Lcom/swedbank/mobile/business/push/f;
.super Ljava/lang/Object;
.source "IsPushOnboardingNeeded.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/h;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isAppServicesEnabledUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/h;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isAppServicesEnabled"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/f;->a:Lcom/swedbank/mobile/business/push/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/push/f;->b:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 17
    iget-object v1, p0, Lcom/swedbank/mobile/business/push/f;->b:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    .line 18
    iget-object v2, p0, Lcom/swedbank/mobile/business/push/f;->a:Lcom/swedbank/mobile/business/push/h;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/push/h;->c()Lio/reactivex/w;

    move-result-object v2

    check-cast v2, Lio/reactivex/aa;

    .line 16
    invoke-virtual {v0, v1, v2}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 19
    sget-object v1, Lcom/swedbank/mobile/business/push/f$a;->a:Lcom/swedbank/mobile/business/push/f$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Singles.zip(\n      isApp\u2026State === UNKNOWN\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/f;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
