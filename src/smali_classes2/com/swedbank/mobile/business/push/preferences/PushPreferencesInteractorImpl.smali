.class public final Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PushPreferencesInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/push/preferences/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/push/preferences/c;",
        ">;",
        "Lcom/swedbank/mobile/business/push/preferences/a;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "activatePushServiceUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "deactivatePushServiceUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "push_preferences_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/h;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activatePushService"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deactivatePushService"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    iput-object p3, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    iput-object p4, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->d:Lcom/swedbank/mobile/business/preferences/a;

    .line 35
    invoke-interface {p1}, Lcom/swedbank/mobile/business/push/h;->d()Lio/reactivex/o;

    move-result-object p1

    .line 36
    sget-object p2, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$b;->a:Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$b;

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "pushRepository\n      .ob\u2026  .map { it === ENABLED }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->a:Lio/reactivex/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->a:Lio/reactivex/o;

    return-object v0
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->a:Lio/reactivex/o;

    .line 42
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "pushStatusStream\n      .\u2026rvice()\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->d:Lcom/swedbank/mobile/business/preferences/a;

    .line 51
    const-class v1, Lcom/swedbank/mobile/business/push/preferences/c;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method
