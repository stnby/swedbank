.class final Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;
.super Ljava/lang/Object;
.source "PushPreferencesInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->b()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;->a:Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pushEnabled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;->a:Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->a(Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    goto :goto_0

    .line 46
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;->a:Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;->b(Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/preferences/PushPreferencesInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
