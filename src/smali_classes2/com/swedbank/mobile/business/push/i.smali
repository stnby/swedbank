.class public final enum Lcom/swedbank/mobile/business/push/i;
.super Ljava/lang/Enum;
.source "PushRepository.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/push/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/push/i;

.field public static final enum b:Lcom/swedbank/mobile/business/push/i;

.field public static final enum c:Lcom/swedbank/mobile/business/push/i;

.field private static final synthetic d:[Lcom/swedbank/mobile/business/push/i;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/push/i;

    new-instance v1, Lcom/swedbank/mobile/business/push/i;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 25
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/business/push/i;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/push/i;->a:Lcom/swedbank/mobile/business/push/i;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/push/i;

    const-string v2, "ENABLED"

    const/4 v4, 0x1

    invoke-direct {v1, v2, v4, v4}, Lcom/swedbank/mobile/business/push/i;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/push/i;->b:Lcom/swedbank/mobile/business/push/i;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/push/i;

    const-string v2, "DISABLED"

    const/4 v4, 0x2

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/push/i;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/push/i;->c:Lcom/swedbank/mobile/business/push/i;

    aput-object v1, v0, v4

    sput-object v0, Lcom/swedbank/mobile/business/push/i;->d:[Lcom/swedbank/mobile/business/push/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/business/push/i;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/push/i;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/push/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/push/i;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/push/i;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/push/i;->d:[Lcom/swedbank/mobile/business/push/i;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/push/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/push/i;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/swedbank/mobile/business/push/i;->e:I

    return v0
.end method
