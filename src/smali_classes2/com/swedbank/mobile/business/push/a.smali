.class public final Lcom/swedbank/mobile/business/push/a;
.super Ljava/lang/Object;
.source "ActivatePushService.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/util/p;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/h;

.field private final b:Lcom/swedbank/mobile/business/d/a;

.field private final c:Lcom/swedbank/mobile/business/h/c;

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/h/c;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/h/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getMobileAppIdUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/h;",
            "Lcom/swedbank/mobile/business/d/a;",
            "Lcom/swedbank/mobile/business/h/c;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceOrderingRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getMobileAppId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/a;->a:Lcom/swedbank/mobile/business/push/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/push/a;->b:Lcom/swedbank/mobile/business/d/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/push/a;->c:Lcom/swedbank/mobile/business/h/c;

    iput-object p4, p0, Lcom/swedbank/mobile/business/push/a;->d:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/push/a;)Lcom/swedbank/mobile/business/h/c;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/a;->c:Lcom/swedbank/mobile/business/h/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/push/a;)Lcom/swedbank/mobile/business/push/h;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/a;->a:Lcom/swedbank/mobile/business/push/h;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/push/a;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/a;->b:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/a;->d:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 22
    new-instance v1, Lcom/swedbank/mobile/business/push/a$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/a$b;-><init>(Lcom/swedbank/mobile/business/push/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "getMobileAppId()\n      .\u2026      }\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/a;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
