.class final Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$b;
.super Ljava/lang/Object;
.source "PushMessageHandlingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$b;->a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/push/e;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/push/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/e;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/push/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/push/e;->b()Ljava/lang/String;

    move-result-object p1

    .line 69
    iget-object v1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$b;->a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->b(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v1

    const-string v2, "push_key_alias"

    .line 71
    invoke-static {v2}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    .line 69
    invoke-interface {v1, p1, v2}, Lcom/swedbank/mobile/business/d/a;->c(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 93
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 73
    invoke-static {v0, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 94
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 74
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/swedbank/mobile/business/push/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$b;->a(Lcom/swedbank/mobile/business/push/e;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
