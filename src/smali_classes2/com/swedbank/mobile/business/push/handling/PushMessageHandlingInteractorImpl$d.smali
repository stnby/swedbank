.class final synthetic Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$d;
.super Lkotlin/e/b/i;
.source "PushMessageHandlingInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/e/m;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/e/n;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$d;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/e/n;

    .line 85
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/e/n;->a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/e/n;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/swedbank/mobile/business/e/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$d;->a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "showNotification"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "showNotification(Lcom/swedbank/mobile/business/device/SystemNotificationInformation;)Lio/reactivex/Completable;"

    return-object v0
.end method
