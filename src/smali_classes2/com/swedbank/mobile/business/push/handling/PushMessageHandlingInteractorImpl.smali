.class public final Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PushMessageHandlingInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/push/handling/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/f/a;

.field private final c:Lcom/swedbank/mobile/business/push/j;

.field private final d:Lcom/swedbank/mobile/business/d/a;

.field private final e:Lcom/swedbank/mobile/business/push/h;

.field private final f:Lcom/swedbank/mobile/business/e/n;

.field private final g:Lcom/swedbank/mobile/business/i/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/push/j;Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/business/e/n;Ljava/util/Set;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/push/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/e/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "push_message_notification_information_providers"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "push_message_handling_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lcom/swedbank/mobile/business/push/j;",
            "Lcom/swedbank/mobile/business/d/a;",
            "Lcom/swedbank/mobile/business/push/h;",
            "Lcom/swedbank/mobile/business/e/n;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;",
            "Lcom/swedbank/mobile/business/i/c;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPushMessageStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "systemNotificationRepository"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationInformationProviders"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->b:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->c:Lcom/swedbank/mobile/business/push/j;

    iput-object p3, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->d:Lcom/swedbank/mobile/business/d/a;

    iput-object p4, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->e:Lcom/swedbank/mobile/business/push/h;

    iput-object p5, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->f:Lcom/swedbank/mobile/business/e/n;

    iput-object p7, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->g:Lcom/swedbank/mobile/business/i/c;

    .line 42
    new-instance p1, Landroidx/c/a;

    invoke-interface {p6}, Ljava/util/Set;->size()I

    move-result p2

    invoke-direct {p1, p2}, Landroidx/c/a;-><init>(I)V

    .line 43
    move-object p2, p6

    check-cast p2, Ljava/lang/Iterable;

    .line 94
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/swedbank/mobile/business/push/handling/d;

    .line 44
    invoke-interface {p3}, Lcom/swedbank/mobile/business/push/handling/d;->a()Ljava/util/Set;

    move-result-object p4

    check-cast p4, Ljava/lang/Iterable;

    .line 95
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_0

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/String;

    .line 45
    move-object p7, p1

    check-cast p7, Ljava/util/Map;

    invoke-interface {p7, p5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p7

    xor-int/lit8 p7, p7, 0x1

    if-eqz p7, :cond_1

    .line 48
    invoke-virtual {p1, p5, p3}, Landroidx/c/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 46
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Duplicate PushMessageNotificationProvider for type "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 45
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 41
    :cond_2
    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->a:Ljava/util/Map;

    .line 55
    check-cast p6, Ljava/util/Collection;

    invoke-interface {p6}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_4

    .line 58
    iget-object p1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->a:Ljava/util/Map;

    const-string p2, "com.swedbank.mobile.GENERIC_PUSH_NOTIFICATION"

    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "There must be PushMessageNotificationProvider for type com.swedbank.mobile.GENERIC_PUSH_NOTIFICATION"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 55
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "There must be at least one PushMessageNotificationProvider"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->b:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->d:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Lcom/swedbank/mobile/business/push/h;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->e:Lcom/swedbank/mobile/business/push/h;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Ljava/util/Map;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->a:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->c:Lcom/swedbank/mobile/business/push/j;

    .line 65
    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/j;->a()Lio/reactivex/o;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 67
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$d;

    iget-object v2, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->f:Lcom/swedbank/mobile/business/e/n;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/e/n;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/push/handling/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/push/handling/b;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lio/reactivex/b;->d()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "userPushMessageStream\n  \u2026ication)\n        .retry()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 87
    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 92
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
