.class final Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;
.super Ljava/lang/Object;
.source "PushMessageHandlingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;->a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lcom/swedbank/mobile/business/e/m;
    .locals 2
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/business/e/m;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;->a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->c(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Lcom/swedbank/mobile/business/push/h;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/swedbank/mobile/business/push/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/swedbank/mobile/business/push/UserPushMessage;

    move-result-object p1

    .line 81
    iget-object v1, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;->a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->d(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/push/handling/d;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;->a:Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;->d(Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "com.swedbank.mobile.GENERIC_PUSH_NOTIFICATION"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/push/handling/d;

    .line 83
    :goto_0
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/push/handling/d;->a(Lcom/swedbank/mobile/business/push/UserPushMessage;)Lcom/swedbank/mobile/business/e/m;

    move-result-object p1

    return-object p1

    .line 82
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl$c;->a(Lkotlin/k;)Lcom/swedbank/mobile/business/e/m;

    move-result-object p1

    return-object p1
.end method
