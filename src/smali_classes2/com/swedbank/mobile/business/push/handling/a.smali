.class public final Lcom/swedbank/mobile/business/push/handling/a;
.super Ljava/lang/Object;
.source "PushMessageHandlingInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/j;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/h;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/n;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/swedbank/mobile/business/push/handling/a;->a:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/swedbank/mobile/business/push/handling/a;->b:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/swedbank/mobile/business/push/handling/a;->c:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/swedbank/mobile/business/push/handling/a;->d:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/swedbank/mobile/business/push/handling/a;->e:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/swedbank/mobile/business/push/handling/a;->f:Ljavax/inject/Provider;

    .line 43
    iput-object p7, p0, Lcom/swedbank/mobile/business/push/handling/a;->g:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/push/handling/a;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/push/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/push/handling/d;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;)",
            "Lcom/swedbank/mobile/business/push/handling/a;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/swedbank/mobile/business/push/handling/a;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/business/push/handling/a;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;
    .locals 9

    .line 48
    new-instance v8, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/f/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/push/j;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/d/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/push/h;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/e/n;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Set;

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/handling/a;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/business/i/c;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;-><init>(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/push/j;Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/business/e/n;Ljava/util/Set;Lcom/swedbank/mobile/business/i/c;)V

    return-object v8
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/push/handling/a;->a()Lcom/swedbank/mobile/business/push/handling/PushMessageHandlingInteractorImpl;

    move-result-object v0

    return-object v0
.end method
