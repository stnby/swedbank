.class final Lcom/swedbank/mobile/business/push/a$b$a$a;
.super Ljava/lang/Object;
.source "ActivatePushService.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/push/a$b$a;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/push/a$b$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/push/a$b$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/push/a$b$a$a;->a:Lcom/swedbank/mobile/business/push/a$b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/push/a$b$a$a;->a:Lcom/swedbank/mobile/business/push/a$b$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/push/a$b$a;->a:Lcom/swedbank/mobile/business/push/a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/push/a$b;->a:Lcom/swedbank/mobile/business/push/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/push/a;->b(Lcom/swedbank/mobile/business/push/a;)Lcom/swedbank/mobile/business/push/h;

    move-result-object v0

    .line 30
    sget-object v1, Lcom/swedbank/mobile/business/push/i;->b:Lcom/swedbank/mobile/business/push/i;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/push/h;->a(Lcom/swedbank/mobile/business/push/i;)Lio/reactivex/b;

    move-result-object v0

    .line 31
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "pushRepository\n         \u2026Then(Single.just(result))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 32
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(result)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/push/a$b$a$a;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
