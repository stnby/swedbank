.class public final Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "FeatureDiscoveryInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/discovery/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/discovery/e;",
        ">;",
        "Lcom/swedbank/mobile/business/discovery/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/f/a;

.field private final b:Lcom/swedbank/mobile/business/discovery/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/discovery/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/discovery/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureDiscoveryStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;->b:Lcom/swedbank/mobile/business/discovery/f;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/discovery/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;->b:Lcom/swedbank/mobile/business/discovery/f;

    .line 21
    invoke-interface {v0}, Lcom/swedbank/mobile/business/discovery/f;->a()Lio/reactivex/o;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "featureDiscoveryStream\n \u2026 .map { request }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "featureKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    .line 31
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/f/a;->d(Ljava/lang/String;)V

    return-void
.end method
