.class public final Lcom/swedbank/mobile/business/firebase/b;
.super Ljava/lang/Object;
.source "FirebaseInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/k;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/e;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/b;->a:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/swedbank/mobile/business/firebase/b;->b:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/swedbank/mobile/business/firebase/b;->c:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/swedbank/mobile/business/firebase/b;->d:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/swedbank/mobile/business/firebase/b;->e:Ljavax/inject/Provider;

    .line 35
    iput-object p6, p0, Lcom/swedbank/mobile/business/firebase/b;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/firebase/b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/e;",
            ">;)",
            "Lcom/swedbank/mobile/business/firebase/b;"
        }
    .end annotation

    .line 50
    new-instance v7, Lcom/swedbank/mobile/business/firebase/b;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/firebase/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;
    .locals 8

    .line 40
    new-instance v7, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/customer/k;

    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/b;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/authentication/login/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/b;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/firebase/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/d/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/b;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/firebase/c;

    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/b;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/firebase/e;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;-><init>(Lcom/swedbank/mobile/business/customer/k;Lcom/swedbank/mobile/business/authentication/login/g;Lcom/swedbank/mobile/business/firebase/a;Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/firebase/e;)V

    return-object v7
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/firebase/b;->a()Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    move-result-object v0

    return-object v0
.end method
