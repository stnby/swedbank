.class public final Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;
.super Ljava/lang/Object;
.source "FirebaseInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lkotlin/p<",
        "+",
        "Lcom/swedbank/mobile/business/util/l<",
        "+",
        "Ljava/lang/String;",
        ">;+",
        "Ljava/lang/Boolean;",
        "+",
        "Ljava/lang/Boolean;",
        ">;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/p;)Lio/reactivex/b;
    .locals 6
    .param p1    # Lkotlin/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/p<",
            "+",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/p;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p1}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1}, Lkotlin/p;->f()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 56
    iget-object v2, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->d(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/swedbank/mobile/business/firebase/f;

    const-string v4, "lastLoggedInUserId"

    .line 58
    invoke-static {v0, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v4, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 74
    :cond_0
    instance-of v4, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 71
    :goto_0
    check-cast v0, Ljava/lang/String;

    .line 59
    new-instance v4, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b$1;

    iget-object v5, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    invoke-static {v5}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->e(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/d/a;)V

    check-cast v4, Lkotlin/e/a/b;

    .line 57
    new-instance v5, Lcom/swedbank/mobile/business/firebase/f$c;

    invoke-direct {v5, v0, v4}, Lcom/swedbank/mobile/business/firebase/f$c;-><init>(Ljava/lang/String;Lkotlin/e/a/b;)V

    check-cast v5, Lcom/swedbank/mobile/business/firebase/f;

    const/4 v0, 0x0

    aput-object v5, v3, v0

    const/4 v0, 0x1

    .line 60
    new-instance v4, Lcom/swedbank/mobile/business/firebase/f$a;

    const-string v5, "bleeding_edge_user"

    invoke-direct {v4, v5, v1}, Lcom/swedbank/mobile/business/firebase/f$a;-><init>(Ljava/lang/String;Z)V

    check-cast v4, Lcom/swedbank/mobile/business/firebase/f;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    .line 63
    new-instance v1, Lcom/swedbank/mobile/business/firebase/f$a;

    const-string v4, "swedbank_employee"

    const-string v5, "swedbankEmployee"

    .line 65
    invoke-static {p1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 63
    invoke-direct {v1, v4, p1}, Lcom/swedbank/mobile/business/firebase/f$a;-><init>(Ljava/lang/String;Z)V

    check-cast v1, Lcom/swedbank/mobile/business/firebase/f;

    aput-object v1, v3, v0

    .line 56
    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/firebase/c;->a([Lcom/swedbank/mobile/business/firebase/f;)Lio/reactivex/b;

    move-result-object p1

    .line 66
    new-instance v0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b$2;

    iget-object v1, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->a(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/firebase/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b$2;-><init>(Lcom/swedbank/mobile/business/firebase/e;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {v0}, Lio/reactivex/i/a;->a(Lkotlin/e/a/a;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    return-object p1

    .line 75
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;->a(Lkotlin/p;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
