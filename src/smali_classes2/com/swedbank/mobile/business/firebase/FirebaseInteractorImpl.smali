.class public final Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "FirebaseInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/firebase/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/customer/k;

.field private final b:Lcom/swedbank/mobile/business/authentication/login/g;

.field private final c:Lcom/swedbank/mobile/business/firebase/a;

.field private final d:Lcom/swedbank/mobile/business/d/a;

.field private final e:Lcom/swedbank/mobile/business/firebase/c;

.field private final f:Lcom/swedbank/mobile/business/firebase/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/k;Lcom/swedbank/mobile/business/authentication/login/g;Lcom/swedbank/mobile/business/firebase/a;Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/firebase/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/firebase/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/firebase/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "localCustomerRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginCredentialsRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bleedingEdgeUserRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remoteConfigUpdateStream"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/k;

    iput-object p2, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/login/g;

    iput-object p3, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->c:Lcom/swedbank/mobile/business/firebase/a;

    iput-object p4, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->d:Lcom/swedbank/mobile/business/d/a;

    iput-object p5, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->e:Lcom/swedbank/mobile/business/firebase/c;

    iput-object p6, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->f:Lcom/swedbank/mobile/business/firebase/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/firebase/e;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->f:Lcom/swedbank/mobile/business/firebase/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/g;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/login/g;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/customer/k;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/k;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/firebase/c;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->e:Lcom/swedbank/mobile/business/firebase/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->d:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 8

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->c:Lcom/swedbank/mobile/business/firebase/a;

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/a;->c()Lio/reactivex/o;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "bleedingEdgeUserReposito\u2026) }\n          )\n        }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    .line 41
    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 71
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
