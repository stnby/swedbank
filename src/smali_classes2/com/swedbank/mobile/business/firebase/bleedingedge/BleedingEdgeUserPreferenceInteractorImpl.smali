.class public final Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BleedingEdgeUserPreferenceInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/firebase/bleedingedge/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/firebase/bleedingedge/c;",
        ">;",
        "Lcom/swedbank/mobile/business/firebase/bleedingedge/a;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final b:Lcom/swedbank/mobile/business/firebase/a;

.field private final c:Lcom/swedbank/mobile/business/firebase/c;

.field private final d:Lcom/swedbank/mobile/business/firebase/e;

.field private final e:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/firebase/a;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/firebase/e;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/firebase/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/firebase/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "bleeding_edge_user_preference_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bleedingEdgeUserRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remoteConfigUpdateStream"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/business/firebase/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->c:Lcom/swedbank/mobile/business/firebase/c;

    iput-object p3, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->d:Lcom/swedbank/mobile/business/firebase/e;

    iput-object p4, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->e:Lcom/swedbank/mobile/business/preferences/a;

    .line 35
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/firebase/c;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->c:Lcom/swedbank/mobile/business/firebase/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/firebase/f$a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->c:Lcom/swedbank/mobile/business/firebase/c;

    .line 38
    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/c;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "userPropertyName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 80
    invoke-static {p0}, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a(Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v0

    new-array v2, v1, [Lcom/swedbank/mobile/business/firebase/f;

    .line 87
    new-instance v3, Lcom/swedbank/mobile/business/firebase/f$a;

    invoke-direct {v3, p1, v1}, Lcom/swedbank/mobile/business/firebase/f$a;-><init>(Ljava/lang/String;Z)V

    check-cast v3, Lcom/swedbank/mobile/business/firebase/f;

    const/4 p1, 0x0

    aput-object v3, v2, p1

    .line 80
    invoke-interface {v0, v2}, Lcom/swedbank/mobile/business/firebase/c;->a([Lcom/swedbank/mobile/business/firebase/f;)Lio/reactivex/b;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 86
    invoke-static {p1, v0, v0, v1, v0}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 90
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->c:Lcom/swedbank/mobile/business/firebase/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/c;->b()V

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/business/firebase/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/a;->b()V

    .line 60
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->c()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "userPropertyName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 93
    invoke-static {p0}, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a(Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v0

    new-array v1, v1, [Lcom/swedbank/mobile/business/firebase/f;

    .line 100
    new-instance v2, Lcom/swedbank/mobile/business/firebase/f$a;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/swedbank/mobile/business/firebase/f$a;-><init>(Ljava/lang/String;Z)V

    check-cast v2, Lcom/swedbank/mobile/business/firebase/f;

    aput-object v2, v1, v3

    .line 93
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/firebase/c;->a([Lcom/swedbank/mobile/business/firebase/f;)Lio/reactivex/b;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 99
    invoke-static {p1, v0, v0, v1, v0}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 103
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->d:Lcom/swedbank/mobile/business/firebase/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/e;->b()V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;->e:Lcom/swedbank/mobile/business/preferences/a;

    const-class v1, Lcom/swedbank/mobile/business/firebase/bleedingedge/c;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method
