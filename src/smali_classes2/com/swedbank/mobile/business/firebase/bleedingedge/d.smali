.class public final Lcom/swedbank/mobile/business/firebase/bleedingedge/d;
.super Ljava/lang/Object;
.source "ObserveBleedingEdgeUserStatus.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/o<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/firebase/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/firebase/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/firebase/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bleedingEdgeUserRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/d;->a:Lcom/swedbank/mobile/business/firebase/a;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/d;->a:Lcom/swedbank/mobile/business/firebase/a;

    .line 13
    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/a;->c()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/firebase/bleedingedge/d;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
