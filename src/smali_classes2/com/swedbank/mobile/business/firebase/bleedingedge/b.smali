.class public final Lcom/swedbank/mobile/business/firebase/bleedingedge/b;
.super Ljava/lang/Object;
.source "BleedingEdgeUserPreferenceInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->a:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->b:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->c:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/firebase/bleedingedge/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ">;)",
            "Lcom/swedbank/mobile/business/firebase/bleedingedge/b;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;
    .locals 5

    .line 33
    new-instance v0, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/firebase/a;

    iget-object v2, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/firebase/c;

    iget-object v3, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/firebase/e;

    iget-object v4, p0, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/preferences/a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;-><init>(Lcom/swedbank/mobile/business/firebase/a;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/firebase/e;Lcom/swedbank/mobile/business/preferences/a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/firebase/bleedingedge/b;->a()Lcom/swedbank/mobile/business/firebase/bleedingedge/BleedingEdgeUserPreferenceInteractorImpl;

    move-result-object v0

    return-object v0
.end method
