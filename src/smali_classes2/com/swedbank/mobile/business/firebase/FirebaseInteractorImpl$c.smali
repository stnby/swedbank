.class final Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;
.super Ljava/lang/Object;
.source "FirebaseInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/o<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string v0, "bleedingEdgeUserStatusStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;

    .line 71
    sget-object v1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 74
    invoke-static {v0}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->b(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/g;

    move-result-object v2

    .line 77
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/login/g;->c()Lio/reactivex/o;

    move-result-object v2

    .line 76
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    const-string v3, "loginCredentialsReposito\u2026  .distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-static {v0}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;->c(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)Lcom/swedbank/mobile/business/customer/k;

    move-result-object v3

    .line 85
    invoke-interface {v3}, Lcom/swedbank/mobile/business/customer/k;->g()Lio/reactivex/o;

    move-result-object v3

    .line 84
    sget-object v4, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$a;->a:Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$a;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 83
    invoke-virtual {v3}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    const-string v4, "localCustomerRepository\n\u2026  .distinctUntilChanged()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {v1, v2, p1, v3}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v1

    .line 73
    new-instance v2, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->n(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    const-wide/16 v1, 0x1

    .line 37
    invoke-virtual {p1, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object p1

    .line 38
    new-instance v1, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c$1;-><init>(Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 33
    invoke-static {v0, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/firebase/FirebaseInteractorImpl$c;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
