.class final Lcom/swedbank/mobile/business/root/RootInteractorImpl$g$1;
.super Ljava/lang/Object;
.source "RootInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/root/RootInteractorImpl$g;->a(Lcom/swedbank/mobile/business/root/d;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/root/d;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/root/d;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl$g$1;->a:Lcom/swedbank/mobile/business/root/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/j;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/b;",
            ">;"
        }
    .end annotation

    const-string v0, "isEligible"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl$g$1;->a:Lcom/swedbank/mobile/business/root/d;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/root/d;->f()Lio/reactivex/w;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/w;->e()Lio/reactivex/j;

    move-result-object p1

    goto :goto_0

    .line 122
    :cond_0
    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$g$1;->a(Ljava/lang/Boolean;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
