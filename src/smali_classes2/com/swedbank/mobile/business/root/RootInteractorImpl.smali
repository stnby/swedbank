.class public final Lcom/swedbank/mobile/business/root/RootInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "RootInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/l;
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/a;
.implements Lcom/swedbank/mobile/business/root/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/root/d;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/session/l;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
        "Lcom/swedbank/mobile/business/root/c;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/authentication/session/e;

.field private final d:Lcom/swedbank/mobile/business/i/d;

.field private final e:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/authentication/session/e;Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/session/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_root_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isDeviceEligibleForWalletUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isDeviceRootedUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/session/e;",
            "Lcom/swedbank/mobile/business/i/d;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priorToSessionRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pluginManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isDeviceEligibleForWallet"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isDeviceRooted"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/session/e;

    iput-object p3, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->d:Lcom/swedbank/mobile/business/i/d;

    iput-object p4, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    iput-object p5, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/g;

    .line 56
    iget-object p1, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {p1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    invoke-virtual {p1}, Lio/reactivex/w;->a()Lio/reactivex/w;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->a:Lio/reactivex/w;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)Lcom/swedbank/mobile/business/root/d;
    .locals 0

    .line 48
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/root/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)Lio/reactivex/w;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->a:Lio/reactivex/w;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)Lcom/swedbank/mobile/business/i/d;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->d:Lcom/swedbank/mobile/business/i/d;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/f;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 108
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$o;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$o;-><init>(Lcom/swedbank/mobile/business/authentication/f;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow().flatMapMaybe { it\u2026catedState(explanation) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/g;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/g;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow().flatMapMaybe { it\u2026eWithCredentials(input) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lio/reactivex/j;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$d;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026   this\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lkotlin/h/b;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/h/b<",
            "*>;)",
            "Lio/reactivex/j<",
            "TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginRouterClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 128
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$f;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;Lkotlin/h/b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026(pluginRouterClass, it) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/root/d;

    .line 99
    invoke-interface {v0}, Lcom/swedbank/mobile/business/root/d;->d()V

    .line 100
    invoke-interface {v0}, Lcom/swedbank/mobile/business/root/d;->b()V

    return-void
.end method

.method public b()V
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/root/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/root/d;->g()V

    return-void
.end method

.method public i()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 105
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$n;->a:Lcom/swedbank/mobile/business/root/RootInteractorImpl$n;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow().flatMapMaybe { it\u2026uthenticatedState(null) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            "Lcom/swedbank/mobile/business/authentication/notauth/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 114
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$c;->a:Lcom/swedbank/mobile/business/root/RootInteractorImpl$c;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/root/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/root/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow().flatMapMaybe(Root\u2026rrentAuthenticationState)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 116
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 117
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapMay\u2026  }\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public l()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/notify/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 130
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 131
    sget-object v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$e;->a:Lcom/swedbank/mobile/business/root/RootInteractorImpl$e;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/root/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/root/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapMay\u2026Router::flowToNotifyUser)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public m()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 141
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 142
    sget-object v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$b;->a:Lcom/swedbank/mobile/business/root/RootInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/root/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/root/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapCom\u2026able(RootRouter::exitApp)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 8

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->d:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 63
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 145
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/session/e;

    .line 69
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/session/e;->a()Lio/reactivex/w;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$i;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$i;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 147
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->a:Lio/reactivex/w;

    const-string v1, "isEligibleForWallet"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$j;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$j;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 149
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/business/root/RootInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/a/c;

    .line 81
    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/a/c;->a()Lio/reactivex/o;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$k;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/root/RootInteractorImpl$k;-><init>(Lcom/swedbank/mobile/business/root/RootInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->e(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 83
    sget-object v1, Lcom/swedbank/mobile/business/root/RootInteractorImpl$l;->a:Lcom/swedbank/mobile/business/root/RootInteractorImpl$l;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "flowManager\n        .obs\u2026turn@retry true\n        }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->d()Lkotlin/e/a/b;

    move-result-object v5

    .line 90
    sget-object v0, Lcom/swedbank/mobile/business/root/RootInteractorImpl$m;->a:Lcom/swedbank/mobile/business/root/RootInteractorImpl$m;

    move-object v3, v0

    check-cast v3, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 88
    invoke-static/range {v2 .. v7}, Lio/reactivex/i/f;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 151
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
