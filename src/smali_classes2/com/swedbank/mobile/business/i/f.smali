.class public Lcom/swedbank/mobile/business/i/f;
.super Lcom/swedbank/mobile/architect/a/h;
.source "Plugins.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ")V"
        }
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 105
    check-cast p3, Lcom/swedbank/mobile/architect/a/b/f;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/i/f;Lkotlin/e/a/b;)V
    .locals 0

    .line 102
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/i/f;->b(Lkotlin/e/a/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->e(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/i/c;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/i/c;",
            ")V"
        }
    .end annotation

    const-string v0, "routerClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    new-instance v0, Lcom/swedbank/mobile/business/i/f$b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/business/i/f$b;-><init>(Lcom/swedbank/mobile/business/i/f;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/business/i/f;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/i/c;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/i/c;",
            ")",
            "Lio/reactivex/j<",
            "TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "routerClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    new-instance v0, Lcom/swedbank/mobile/business/i/f$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/business/i/f$a;-><init>(Lcom/swedbank/mobile/business/i/f;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string p2, "Maybe.create { emitter -\u2026      }\n      }\n    }\n  }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
