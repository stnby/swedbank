.class public final Lcom/swedbank/mobile/business/i/d;
.super Ljava/lang/Object;
.source "Plugins.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/i/e;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/swedbank/mobile/business/i/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "allPlugins"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    check-cast p1, Ljava/lang/Iterable;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 144
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/business/i/b;

    .line 70
    invoke-interface {v3}, Lcom/swedbank/mobile/business/i/b;->i()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_1
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/swedbank/mobile/business/i/d;->b:Ljava/util/List;

    const/16 v0, 0xa

    .line 146
    invoke-static {p1, v0}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/a/x;->a(I)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lkotlin/g/e;->c(II)I

    move-result v0

    .line 147
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 148
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 149
    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/i/b;

    .line 71
    invoke-interface {v2}, Lcom/swedbank/mobile/business/i/b;->f()Lkotlin/h/b;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 151
    :cond_2
    iput-object v1, p0, Lcom/swedbank/mobile/business/i/d;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/h/b;Lcom/swedbank/mobile/business/i/e;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/i/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/swedbank/mobile/business/i/e;",
            ")",
            "Lio/reactivex/j<",
            "TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginRouterClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "router"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/i/d;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/i/b;

    .line 93
    invoke-interface {v0}, Lcom/swedbank/mobile/business/i/b;->g()Lkotlin/e/a/b;

    move-result-object v0

    .line 94
    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/business/i/c;

    .line 91
    invoke-interface {p2, p1, v0, v1}, Lcom/swedbank/mobile/business/i/e;->b(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)Lio/reactivex/j;

    move-result-object p1

    return-object p1

    .line 89
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No plugin defined for router class "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lkotlin/e/a;->a(Lkotlin/h/b;)Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 88
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/business/i/d;->a:Lcom/swedbank/mobile/business/i/e;

    if-eqz v0, :cond_0

    .line 99
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/i/e;->a(Lcom/swedbank/mobile/architect/a/h;)V

    return-void

    .line 98
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Plugins are not activated"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final a(Lcom/swedbank/mobile/business/i/e;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/i/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/business/i/d;->a:Lcom/swedbank/mobile/business/i/e;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 75
    iput-object p1, p0, Lcom/swedbank/mobile/business/i/d;->a:Lcom/swedbank/mobile/business/i/e;

    .line 77
    sget-object v0, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Activating "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/i/d;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " plugins"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/business/i/d;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 141
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/b;

    .line 80
    invoke-interface {v1}, Lcom/swedbank/mobile/business/i/b;->f()Lkotlin/h/b;

    move-result-object v2

    .line 81
    invoke-interface {v1}, Lcom/swedbank/mobile/business/i/b;->g()Lkotlin/e/a/b;

    move-result-object v1

    .line 82
    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/business/i/c;

    .line 79
    invoke-interface {p1, v2, v1, v3}, Lcom/swedbank/mobile/business/i/e;->a(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V

    goto :goto_1

    :cond_1
    return-void

    .line 74
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Plugins can be activated only once"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
