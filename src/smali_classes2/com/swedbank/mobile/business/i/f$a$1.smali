.class final Lcom/swedbank/mobile/business/i/f$a$1;
.super Lkotlin/e/b/k;
.source "Plugins.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/i/f$a;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/i/f$a;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/i/f$a;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/i/f$a$1;->a:Lcom/swedbank/mobile/business/i/f$a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/i/f$a$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/swedbank/mobile/business/i/f$a$1;->a:Lcom/swedbank/mobile/business/i/f$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/i/f$a;->b:Lkotlin/h/b;

    invoke-static {v0}, Lkotlin/e/a;->a(Lkotlin/h/b;)Ljava/lang/Class;

    move-result-object v0

    .line 124
    check-cast p1, Ljava/lang/Iterable;

    .line 137
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 139
    :goto_0
    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    if-nez v1, :cond_2

    .line 127
    iget-object p1, p0, Lcom/swedbank/mobile/business/i/f$a$1;->a:Lcom/swedbank/mobile/business/i/f$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/i/f$a;->a:Lcom/swedbank/mobile/business/i/f;

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/business/i/f$a$1;->a:Lcom/swedbank/mobile/business/i/f$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/i/f$a;->c:Lkotlin/e/a/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/i/f$a$1;->a:Lcom/swedbank/mobile/business/i/f$a;

    iget-object v1, v1, Lcom/swedbank/mobile/business/i/f$a;->d:Lcom/swedbank/mobile/business/i/c;

    invoke-interface {v0, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 127
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    iget-object v1, p0, Lcom/swedbank/mobile/business/i/f$a$1;->b:Lio/reactivex/k;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 140
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 129
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/business/i/f$a$1;->b:Lio/reactivex/k;

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 102
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/i/f$a$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
