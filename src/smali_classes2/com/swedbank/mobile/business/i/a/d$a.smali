.class public final Lcom/swedbank/mobile/business/i/a/d$a;
.super Ljava/lang/Object;
.source "ListPluginInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/i/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/business/i/a/d;Ljava/util/List;)V
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PluginNodeType::",
            "Lcom/swedbank/mobile/architect/business/d;",
            ">(",
            "Lcom/swedbank/mobile/business/i/a/d<",
            "TPluginNodeType;>;",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pluginList"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 14
    new-instance v1, Landroidx/c/a;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Landroidx/c/a;-><init>(I)V

    .line 15
    check-cast p1, Ljava/lang/Iterable;

    .line 25
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/i/a/b;

    .line 16
    invoke-interface {v2}, Lcom/swedbank/mobile/business/i/a/b;->g()Lkotlin/e/a/b;

    move-result-object v3

    invoke-interface {v3, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    .line 17
    invoke-virtual {v3}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v4

    .line 18
    move-object v5, v0

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 19
    move-object v3, v1

    check-cast v3, Ljava/util/Map;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/i/a/b;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2}, Lcom/swedbank/mobile/business/i/a/b;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 21
    :cond_0
    check-cast v0, Ljava/util/List;

    invoke-interface {p0, v0}, Lcom/swedbank/mobile/business/i/a/d;->a(Ljava/util/List;)V

    .line 22
    check-cast v1, Ljava/util/Map;

    invoke-interface {p0, v1}, Lcom/swedbank/mobile/business/i/a/d;->a(Ljava/util/Map;)V

    return-void
.end method
