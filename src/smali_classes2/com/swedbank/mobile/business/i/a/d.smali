.class public interface abstract Lcom/swedbank/mobile/business/i/a/d;
.super Ljava/lang/Object;
.source "ListPluginInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/i/a/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PluginNodeType::",
        "Lcom/swedbank/mobile/architect/business/d;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/business/i/c;"
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Map;)V
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+TPluginNodeType;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation
.end method
