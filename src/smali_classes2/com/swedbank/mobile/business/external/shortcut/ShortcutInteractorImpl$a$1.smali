.class final synthetic Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;
.super Lkotlin/e/b/i;
.source "ShortcutInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;->a(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/a;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/a;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/cards/a;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->t()Z

    move-result p1

    return p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;->a(Lcom/swedbank/mobile/business/cards/a;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "isDigitized"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "isDigitized()Z"

    return-object v0
.end method
