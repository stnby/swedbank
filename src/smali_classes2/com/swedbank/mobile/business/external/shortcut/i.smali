.class public final Lcom/swedbank/mobile/business/external/shortcut/i;
.super Ljava/lang/Object;
.source "TrackCardSelectedForPaymentShortcutUsage.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Ljava/lang/String;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/external/shortcut/f;

.field private final b:Lcom/swedbank/mobile/business/external/shortcut/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/external/shortcut/f;Lcom/swedbank/mobile/business/external/shortcut/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/external/shortcut/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/external/shortcut/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "shortcutProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortcutRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/i;->a:Lcom/swedbank/mobile/business/external/shortcut/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/external/shortcut/i;->b:Lcom/swedbank/mobile/business/external/shortcut/g;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/i;->a:Lcom/swedbank/mobile/business/external/shortcut/f;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/external/shortcut/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/i;->b:Lcom/swedbank/mobile/business/external/shortcut/g;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/external/shortcut/g;->a(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/i;->a(Ljava/lang/String;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
