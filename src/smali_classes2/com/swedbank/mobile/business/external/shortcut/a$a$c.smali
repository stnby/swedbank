.class public final synthetic Lcom/swedbank/mobile/business/external/shortcut/a$a$c;
.super Lkotlin/e/b/i;
.source "CardShortcutFlow.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/external/shortcut/a$a;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/notauth/c;",
        "Lio/reactivex/j<",
        "Lcom/swedbank/mobile/business/general/confirmation/d;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/external/shortcut/a$a$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/external/shortcut/a$a$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/external/shortcut/a$a$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/external/shortcut/a$a$c;->a:Lcom/swedbank/mobile/business/external/shortcut/a$a$c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/notauth/c;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/notauth/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/notauth/c;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/notauth/c;->j()Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/notauth/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/business/cards/notauth/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/a$a$c;->a(Lcom/swedbank/mobile/business/cards/notauth/c;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "flowToOutdatedCardShortcutHelp"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "flowToOutdatedCardShortcutHelp()Lio/reactivex/Maybe;"

    return-object v0
.end method
