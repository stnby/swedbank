.class public final Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$a;
.super Ljava/lang/Object;
.source "Comparisons.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;->a(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .line 121
    check-cast p2, Lcom/swedbank/mobile/business/cards/a;

    .line 321
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object p2

    .line 322
    instance-of v0, p2, Lcom/swedbank/mobile/business/cards/s$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 323
    check-cast p2, Lcom/swedbank/mobile/business/cards/s$a;

    .line 320
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/s$a;->c()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v1

    :goto_0
    check-cast p2, Ljava/lang/Comparable;

    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    .line 326
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object p1

    .line 327
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v0, :cond_1

    .line 328
    check-cast p1, Lcom/swedbank/mobile/business/cards/s$a;

    .line 325
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/s$a;->c()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_1
    check-cast v1, Ljava/lang/Comparable;

    invoke-static {p2, v1}, Lkotlin/b/a;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    return p1
.end method
