.class public final Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ShortcutInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/external/shortcut/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/external/shortcut/f;

.field private final b:Lcom/swedbank/mobile/business/external/shortcut/g;

.field private final c:Lcom/swedbank/mobile/business/i/c;

.field private final d:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/external/shortcut/f;Lcom/swedbank/mobile/business/external/shortcut/g;Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/architect/business/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/external/shortcut/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/external/shortcut/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "shortcut_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/external/shortcut/f;",
            "Lcom/swedbank/mobile/business/external/shortcut/g;",
            "Lcom/swedbank/mobile/business/i/c;",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "shortcutProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortcutRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeAllCards"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->a:Lcom/swedbank/mobile/business/external/shortcut/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->b:Lcom/swedbank/mobile/business/external/shortcut/g;

    iput-object p3, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->c:Lcom/swedbank/mobile/business/i/c;

    iput-object p4, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/f;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;)Lcom/swedbank/mobile/business/external/shortcut/f;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->a:Lcom/swedbank/mobile/business/external/shortcut/f;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;)Lcom/swedbank/mobile/business/external/shortcut/g;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->b:Lcom/swedbank/mobile/business/external/shortcut/g;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 7

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->b:Lcom/swedbank/mobile/business/external/shortcut/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/external/shortcut/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->c:Lcom/swedbank/mobile/business/i/c;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    .line 31
    sget-object v1, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v0, "observeAllCards()\n      \u2026  .distinctUntilChanged()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 47
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
