.class final Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;
.super Ljava/lang/Object;
.source "ShortcutInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "allCards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    check-cast p1, Ljava/lang/Iterable;

    .line 33
    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 34
    sget-object v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$1;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 47
    new-instance v0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$a$a;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object p1

    const/4 v0, 0x4

    .line 36
    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;I)Lkotlin/i/e;

    move-result-object p1

    .line 37
    invoke-static {p1}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
