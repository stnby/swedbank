.class final Lcom/swedbank/mobile/business/external/shortcut/a$b$1;
.super Ljava/lang/Object;
.source "CardShortcutFlow.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/external/shortcut/a$b;->a()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/external/shortcut/a$b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/external/shortcut/a$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/a$b$1;->a:Lcom/swedbank/mobile/business/external/shortcut/a$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/j;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/j<",
            "+",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/a$b$1;->a:Lcom/swedbank/mobile/business/external/shortcut/a$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/external/shortcut/a$b;->a:Lcom/swedbank/mobile/business/external/shortcut/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/a$b$1;->a:Lcom/swedbank/mobile/business/external/shortcut/a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/external/shortcut/a$b;->c:Lcom/swedbank/mobile/business/root/c;

    .line 70
    invoke-interface {v0}, Lcom/swedbank/mobile/business/root/c;->j()Lio/reactivex/j;

    move-result-object v1

    .line 71
    new-instance v2, Lcom/swedbank/mobile/business/external/shortcut/a$a;

    invoke-direct {v2, p1, v0}, Lcom/swedbank/mobile/business/external/shortcut/a$a;-><init>(Lcom/swedbank/mobile/business/external/shortcut/a;Lcom/swedbank/mobile/business/root/c;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "root.flowToCurrentAuthen\u2026            )\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/a$b$1;->a:Lcom/swedbank/mobile/business/external/shortcut/a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/external/shortcut/a$b;->a:Lcom/swedbank/mobile/business/external/shortcut/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/external/shortcut/a;->b(Lcom/swedbank/mobile/business/external/shortcut/a;)Lcom/swedbank/mobile/business/cards/wallet/payment/d;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/business/external/shortcut/a$b$1;->a:Lcom/swedbank/mobile/business/external/shortcut/a$b;

    iget-object v1, v1, Lcom/swedbank/mobile/business/external/shortcut/a$b;->c:Lcom/swedbank/mobile/business/root/c;

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/e;

    .line 38
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/e;

    .line 39
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 38
    invoke-direct {v2, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/e;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v2, Lcom/swedbank/mobile/architect/business/a/b;

    .line 36
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/d;->b(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lio/reactivex/j;

    move-result-object p1

    :goto_0
    return-object p1

    .line 39
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/a$b$1;->a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
