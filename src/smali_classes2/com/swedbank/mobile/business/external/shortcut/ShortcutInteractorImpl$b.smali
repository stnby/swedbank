.class final Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;
.super Lkotlin/e/b/k;
.source "ShortcutInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/List<",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "it"

    .line 41
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->a(Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;)Lcom/swedbank/mobile/business/external/shortcut/f;

    move-result-object v0

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 49
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    .line 50
    invoke-static {}, Lkotlin/a/h;->b()V

    :cond_0
    check-cast v3, Lcom/swedbank/mobile/business/cards/a;

    .line 41
    invoke-interface {v0, v2, v3}, Lcom/swedbank/mobile/business/external/shortcut/f;->a(ILcom/swedbank/mobile/business/cards/a;)Landroid/content/pm/ShortcutInfo;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto :goto_0

    .line 51
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 42
    iget-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;->a:Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;->b(Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl;)Lcom/swedbank/mobile/business/external/shortcut/g;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/swedbank/mobile/business/external/shortcut/g;->a(Ljava/util/List;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/ShortcutInteractorImpl$b;->a(Ljava/util/List;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
