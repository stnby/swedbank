.class public final Lcom/swedbank/mobile/business/external/shortcut/a$a;
.super Ljava/lang/Object;
.source "CardShortcutFlow.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/external/shortcut/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/external/shortcut/a;

.field final synthetic b:Lcom/swedbank/mobile/business/root/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/external/shortcut/a;Lcom/swedbank/mobile/business/root/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/a$a;->a:Lcom/swedbank/mobile/business/external/shortcut/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/external/shortcut/a$a;->b:Lcom/swedbank/mobile/business/root/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/notauth/d;",
            ">;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authenticationState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/notauth/d;

    .line 57
    invoke-interface {p1}, Lcom/swedbank/mobile/business/authentication/notauth/d;->a()Lio/reactivex/j;

    move-result-object p1

    .line 58
    sget-object v0, Lcom/swedbank/mobile/business/external/shortcut/a$a$b;->a:Lcom/swedbank/mobile/business/external/shortcut/a$a$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    .line 59
    sget-object v0, Lcom/swedbank/mobile/business/external/shortcut/a$a$c;->a:Lcom/swedbank/mobile/business/external/shortcut/a$a$c;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/external/shortcut/b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/external/shortcut/b;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    goto :goto_0

    .line 70
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/authenticated/c;

    .line 51
    iget-object p1, p0, Lcom/swedbank/mobile/business/external/shortcut/a$a;->a:Lcom/swedbank/mobile/business/external/shortcut/a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/external/shortcut/a;->c(Lcom/swedbank/mobile/business/external/shortcut/a;)Lcom/swedbank/mobile/business/cards/list/a;

    move-result-object p1

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/business/external/shortcut/a$a;->b:Lcom/swedbank/mobile/business/root/c;

    check-cast v0, Lcom/swedbank/mobile/architect/business/a/e;

    sget-object v1, Lcom/swedbank/mobile/business/cards/list/b;->a:Lcom/swedbank/mobile/business/cards/list/b;

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/swedbank/mobile/business/cards/list/a;->b(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lio/reactivex/j;

    move-result-object p1

    .line 53
    sget-object v0, Lcom/swedbank/mobile/business/external/shortcut/a$a$a;->a:Lcom/swedbank/mobile/business/external/shortcut/a$a$a;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_2

    new-instance v1, Lcom/swedbank/mobile/business/external/shortcut/b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/external/shortcut/b;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_2
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/external/shortcut/a$a;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
