.class public final Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "UserPrivacyPreferenceInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/privacy/preference/e;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/privacy/f;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/privacy/f;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/privacy/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "clearAppDataUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "user_privacy_preference_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/privacy/f;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/business/preferences/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "userPrivacyRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clearAppData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/privacy/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    iput-object p3, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->c:Lcom/swedbank/mobile/business/preferences/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/privacy/preference/e;
    .locals 0

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/privacy/preference/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/privacy/f;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/privacy/f;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/preferences/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->c:Lcom/swedbank/mobile/business/preferences/a;

    return-object p0
.end method


# virtual methods
.method public b()V
    .locals 2

    .line 70
    invoke-static {p0}, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->c(Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/preferences/a;

    move-result-object v0

    .line 71
    const-class v1, Lcom/swedbank/mobile/business/privacy/preference/e;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method

.method protected m_()V
    .locals 8

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/privacy/f;

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/business/privacy/f;->b()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 32
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "userPrivacyRepository\n  \u2026  }\n          )\n        }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;)V

    move-object v3, v0

    check-cast v3, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 68
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public q_()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CheckResult"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/privacy/preference/UserPrivacyPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    return-void
.end method
