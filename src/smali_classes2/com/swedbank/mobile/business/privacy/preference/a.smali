.class public final Lcom/swedbank/mobile/business/privacy/preference/a;
.super Ljava/lang/Object;
.source "ObserveUserDataSaving.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/o<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/privacy/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/privacy/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/privacy/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "userPrivacyRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/privacy/preference/a;->a:Lcom/swedbank/mobile/business/privacy/f;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/business/privacy/preference/a;->a:Lcom/swedbank/mobile/business/privacy/f;

    .line 14
    invoke-interface {v0}, Lcom/swedbank/mobile/business/privacy/f;->b()Lio/reactivex/o;

    move-result-object v0

    .line 15
    sget-object v1, Lcom/swedbank/mobile/business/privacy/preference/a$a;->a:Lcom/swedbank/mobile/business/privacy/preference/a$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "userPrivacyRepository\n  \u2026=== UserDataSaving.SAVE }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/privacy/preference/a;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
