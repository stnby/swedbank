.class public final Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "UserPrivacyOnboardingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/privacy/onboarding/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/privacy/onboarding/e;",
        ">;",
        "Lcom/swedbank/mobile/business/privacy/onboarding/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/onboarding/f;

.field private final b:Lcom/swedbank/mobile/business/privacy/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/business/privacy/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/privacy/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPrivacyRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/privacy/f;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/privacy/f;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/privacy/f;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/f;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    return-object p0
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    .line 39
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/onboarding/f;->a(Z)V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    .line 26
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->d()Lio/reactivex/w;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "onboardingStream\n       \u2026       .onErrorComplete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v1, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/privacy/onboarding/UserPrivacyOnboardingInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 42
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
