.class public final Lcom/swedbank/mobile/business/privacy/onboarding/a;
.super Ljava/lang/Object;
.source "IsUserPrivacyOnboardingNeeded.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/privacy/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/privacy/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/privacy/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "userPrivacyRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/privacy/onboarding/a;->a:Lcom/swedbank/mobile/business/privacy/f;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/business/privacy/onboarding/a;->a:Lcom/swedbank/mobile/business/privacy/f;

    .line 13
    invoke-interface {v0}, Lcom/swedbank/mobile/business/privacy/f;->a()Lio/reactivex/w;

    move-result-object v0

    .line 14
    sget-object v1, Lcom/swedbank/mobile/business/privacy/onboarding/a$a;->a:Lcom/swedbank/mobile/business/privacy/onboarding/a$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "userPrivacyRepository\n  \u2026 UserDataSaving.UNKNOWN }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/privacy/onboarding/a;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
