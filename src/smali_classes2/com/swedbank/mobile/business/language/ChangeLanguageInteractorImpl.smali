.class public final Lcom/swedbank/mobile/business/language/ChangeLanguageInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ChangeLanguageInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/language/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/language/c;",
        ">;",
        "Lcom/swedbank/mobile/business/language/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appPreferenceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/language/ChangeLanguageInteractorImpl;->a:Lcom/swedbank/mobile/business/c/a;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/business/language/ChangeLanguageInteractorImpl;->a:Lcom/swedbank/mobile/business/c/a;

    .line 21
    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/c/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "language"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/business/language/ChangeLanguageInteractorImpl;->a:Lcom/swedbank/mobile/business/c/a;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/c/a;->a(Lcom/swedbank/mobile/business/c/g;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/business/language/ChangeLanguageInteractorImpl;->a:Lcom/swedbank/mobile/business/c/a;

    .line 24
    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->c()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
