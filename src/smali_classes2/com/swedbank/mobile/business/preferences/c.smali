.class public final Lcom/swedbank/mobile/business/preferences/c;
.super Ljava/lang/Object;
.source "PreferencesInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/swedbank/mobile/business/preferences/c;->a:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/swedbank/mobile/business/preferences/c;->b:Ljavax/inject/Provider;

    .line 23
    iput-object p3, p0, Lcom/swedbank/mobile/business/preferences/c;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/preferences/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/preferences/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/business/preferences/c;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/swedbank/mobile/business/preferences/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/preferences/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;
    .locals 4

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/preferences/c;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/firebase/a;

    iget-object v2, p0, Lcom/swedbank/mobile/business/preferences/c;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/preferences/d;

    iget-object v3, p0, Lcom/swedbank/mobile/business/preferences/c;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/business/g;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;-><init>(Lcom/swedbank/mobile/business/firebase/a;Lcom/swedbank/mobile/business/preferences/d;Lcom/swedbank/mobile/architect/business/g;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/preferences/c;->a()Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;

    move-result-object v0

    return-object v0
.end method
