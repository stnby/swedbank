.class public final Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PreferencesInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/preferences/a;
.implements Lcom/swedbank/mobile/business/preferences/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/preferences/e;",
        ">;",
        "Lcom/swedbank/mobile/business/preferences/a;",
        "Lcom/swedbank/mobile/business/preferences/b;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/firebase/a;

.field private final b:Lcom/swedbank/mobile/business/preferences/d;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/firebase/a;Lcom/swedbank/mobile/business/preferences/d;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/firebase/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/preferences/d;
        .annotation runtime Ljavax/inject/Named;
            value = "preferences_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "observeIsAppServicesEnabledUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/firebase/a;",
            "Lcom/swedbank/mobile/business/preferences/d;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bleedingEdgeUserRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferencesListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeIsAppServicesEnabled"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->a:Lcom/swedbank/mobile/business/firebase/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->b:Lcom/swedbank/mobile/business/preferences/d;

    iput-object p3, p0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lkotlin/h/b;)V
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/swedbank/mobile/architect/business/e;",
            ">(",
            "Lkotlin/h/b<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "nodeClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/preferences/e;

    .line 57
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/preferences/e;->a(Lkotlin/h/b;)V

    return-void
.end method

.method public a(Lkotlin/h/b;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "preferenceNodeClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferenceBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/preferences/e;

    invoke-interface {v0, p1, p2}, Lcom/swedbank/mobile/business/preferences/e;->a(Lkotlin/h/b;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->b:Lcom/swedbank/mobile/business/preferences/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/preferences/d;->i()V

    return-void
.end method

.method public c()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->a:Lcom/swedbank/mobile/business/firebase/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/firebase/a;->a()V

    return-void
.end method

.method protected m_()V
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/preferences/PreferencesInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/preferences/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/preferences/e;->a()V

    return-void
.end method
