.class public final Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ClearDataInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/preferences/cleardata/d;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/preferences/a;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/preferences/a;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "clear_data_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "clearAppDataUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clearAppData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;->a:Lcom/swedbank/mobile/business/preferences/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;->a:Lcom/swedbank/mobile/business/preferences/a;

    .line 36
    const-class v1, Lcom/swedbank/mobile/business/preferences/cleardata/d;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method

.method protected m_()V
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/preferences/cleardata/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/preferences/cleardata/d;->a()V

    return-void
.end method

.method public q_()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CheckResult"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/business/preferences/cleardata/ClearDataInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/preferences/cleardata/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/preferences/cleardata/b;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/preferences/cleardata/c;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/preferences/cleardata/c;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    return-void
.end method
