.class public final Lcom/swedbank/mobile/business/oldappmigration/a;
.super Ljava/lang/Object;
.source "OldAppMigrationInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/business/oldappmigration/a;->a:Ljavax/inject/Provider;

    .line 17
    iput-object p2, p0, Lcom/swedbank/mobile/business/oldappmigration/a;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/oldappmigration/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;)",
            "Lcom/swedbank/mobile/business/oldappmigration/a;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/oldappmigration/a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/oldappmigration/a;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;
    .locals 3

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/oldappmigration/a;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/c;

    iget-object v2, p0, Lcom/swedbank/mobile/business/oldappmigration/a;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/e/i;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;-><init>(Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/business/e/i;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/oldappmigration/a;->a()Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
