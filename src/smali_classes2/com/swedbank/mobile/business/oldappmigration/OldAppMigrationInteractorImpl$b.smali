.class final Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;
.super Ljava/lang/Object;
.source "OldAppMigrationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/b;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    const-string v0, "databaseNames"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->a(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    .line 35
    check-cast p1, Ljava/util/Collection;

    const/4 v1, 0x0

    .line 48
    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, [Ljava/lang/String;

    array-length v1, p1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    .line 35
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/e/i;->a([Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->a(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->n()Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    return-object p1

    .line 48
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;->a(Ljava/util/List;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
