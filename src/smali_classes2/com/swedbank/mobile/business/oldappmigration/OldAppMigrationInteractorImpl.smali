.class public final Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OldAppMigrationInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/oldappmigration/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/i/c;

.field private final b:Lcom/swedbank/mobile/business/e/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/c;Lcom/swedbank/mobile/business/e/i;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "oldAppMigrationListener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->a:Lcom/swedbank/mobile/business/i/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/i/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->a:Lcom/swedbank/mobile/business/i/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/oldappmigration/c;
    .locals 0

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/oldappmigration/c;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-static {p0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->b(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/i/c;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->c(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)Lcom/swedbank/mobile/business/oldappmigration/c;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->o()Lio/reactivex/w;

    move-result-object v0

    .line 31
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/swedbank/mobile/business/oldappmigration/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/oldappmigration/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "deviceRepository\n       \u2026pPreferences())\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v1, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$c;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/a;

    .line 40
    new-instance v2, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/oldappmigration/OldAppMigrationInteractorImpl;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 38
    invoke-static {v0, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;)Lio/reactivex/b/c;

    move-result-object v0

    .line 48
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
