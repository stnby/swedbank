.class public final Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ForceInformInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/inform/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/inform/c;",
        ">;",
        "Lcom/swedbank/mobile/business/inform/a;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/k/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/e<",
            "Lcom/swedbank/mobile/business/inform/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/f/a;

.field private final c:Lcom/swedbank/mobile/business/inform/e;

.field private final d:Lcom/swedbank/mobile/business/i/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/inform/e;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/inform/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "force_inform_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "informUserRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->b:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->c:Lcom/swedbank/mobile/business/inform/e;

    iput-object p3, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->d:Lcom/swedbank/mobile/business/i/c;

    .line 28
    invoke-static {}, Lio/reactivex/k/e;->g()Lio/reactivex/k/e;

    move-result-object p1

    const-string p2, "SingleSubject.create<InformUserInformation>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a:Lio/reactivex/k/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;)Lcom/swedbank/mobile/business/i/c;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->d:Lcom/swedbank/mobile/business/i/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;)Lcom/swedbank/mobile/business/inform/c;
    .locals 0

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/inform/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a:Lio/reactivex/k/e;

    .line 69
    invoke-virtual {v0}, Lio/reactivex/k/e;->h()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 45
    check-cast v0, Lcom/swedbank/mobile/business/inform/d;

    iget-object v1, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->b:Lcom/swedbank/mobile/business/f/a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/inform/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/f/a;->d(Ljava/lang/String;)V

    .line 70
    invoke-static {p0}, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a(Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;)Lcom/swedbank/mobile/business/i/c;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->b(Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;)Lcom/swedbank/mobile/business/inform/c;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void

    .line 69
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public b()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a:Lio/reactivex/k/e;

    .line 71
    invoke-virtual {v0}, Lio/reactivex/k/e;->h()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/inform/d;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/inform/d;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/inform/c;

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/inform/c;->b(Ljava/lang/String;)V

    :cond_0
    return-void

    .line 71
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public c()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/inform/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a:Lio/reactivex/k/e;

    check-cast v0, Lio/reactivex/w;

    return-object v0
.end method

.method protected m_()V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->c:Lcom/swedbank/mobile/business/inform/e;

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/business/inform/e;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 61
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_1

    .line 62
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 59
    check-cast v0, Lcom/swedbank/mobile/business/inform/d;

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->b:Lcom/swedbank/mobile/business/f/a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/inform/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 63
    :goto_1
    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a:Lio/reactivex/k/e;

    .line 65
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;

    .line 66
    invoke-static {v0}, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->a(Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;)Lcom/swedbank/mobile/business/i/c;

    move-result-object v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;->b(Lcom/swedbank/mobile/business/inform/ForceInformInteractorImpl;)Lcom/swedbank/mobile/business/inform/c;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    goto :goto_2

    .line 67
    :cond_2
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_3

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/inform/d;

    .line 36
    invoke-virtual {v1, v0}, Lio/reactivex/k/e;->a_(Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 33
    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
