.class public final Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WidgetRootInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/l;
.implements Lcom/swedbank/mobile/business/widget/root/e;
.implements Lcom/swedbank/mobile/business/widget/update/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/widget/root/f;",
        ">;",
        "Lcom/swedbank/mobile/business/authentication/session/l;",
        "Lcom/swedbank/mobile/business/widget/root/e;",
        "Lcom/swedbank/mobile/business/widget/update/e;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/configuration/h;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/widget/root/g;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/configuration/h;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/configuration/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "observeWidgetStateUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/widget/root/g;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "widgetRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeWidgetState"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;)Lcom/swedbank/mobile/business/widget/configuration/h;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;)Lcom/swedbank/mobile/business/widget/root/f;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/widget/root/f;

    return-object p0
.end method


# virtual methods
.method public a(Z)Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/j<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl$a;-><init>(Z)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026dgetUpdate(showBalance) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/widget/root/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/root/f;->a()V

    return-void
.end method

.method public b()V
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/widget/root/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/root/f;->e()V

    return-void
.end method

.method protected m_()V
    .locals 7

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/o;

    .line 31
    new-instance v0, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/widget/root/WidgetRootInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 57
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
