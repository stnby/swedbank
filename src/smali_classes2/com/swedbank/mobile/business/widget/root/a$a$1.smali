.class final Lcom/swedbank/mobile/business/widget/root/a$a$1;
.super Ljava/lang/Object;
.source "ObserveWidgetState.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/widget/root/a$a;->a(Ljava/lang/Boolean;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/widget/root/a$a$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/widget/root/a$a$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/widget/root/a$a$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/widget/root/a$a$1;->a:Lcom/swedbank/mobile/business/widget/root/a$a$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/p;)Lcom/swedbank/mobile/business/widget/root/g;
    .locals 2
    .param p1    # Lkotlin/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/p<",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/swedbank/mobile/business/widget/root/g;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lkotlin/p;->f()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    const-string v1, "loading"

    .line 30
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/swedbank/mobile/business/widget/root/g;->c:Lcom/swedbank/mobile/business/widget/root/g;

    goto :goto_0

    :cond_0
    const-string p1, "balanceShown"

    .line 31
    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/swedbank/mobile/business/widget/root/g;->d:Lcom/swedbank/mobile/business/widget/root/g;

    goto :goto_0

    .line 32
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/widget/root/g;->e:Lcom/swedbank/mobile/business/widget/root/g;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lkotlin/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/widget/root/a$a$1;->a(Lkotlin/p;)Lcom/swedbank/mobile/business/widget/root/g;

    move-result-object p1

    return-object p1
.end method
