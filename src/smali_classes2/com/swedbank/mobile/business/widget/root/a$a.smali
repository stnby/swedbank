.class final Lcom/swedbank/mobile/business/widget/root/a$a;
.super Ljava/lang/Object;
.source "ObserveWidgetState.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/widget/root/a;->a()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/widget/root/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/widget/root/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/root/a$a;->a:Lcom/swedbank/mobile/business/widget/root/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 4
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/widget/root/g;",
            ">;"
        }
    .end annotation

    const-string v0, "hasEnrolledAccounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/root/a$a;->a:Lcom/swedbank/mobile/business/widget/root/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/widget/root/a;->a(Lcom/swedbank/mobile/business/widget/root/a;)Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v0

    .line 20
    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/configuration/h;->c()Lio/reactivex/o;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "widgetRepository\n       \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v1, p0, Lcom/swedbank/mobile/business/widget/root/a$a;->a:Lcom/swedbank/mobile/business/widget/root/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/widget/root/a;->a(Lcom/swedbank/mobile/business/widget/root/a;)Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v1

    .line 23
    invoke-interface {v1}, Lcom/swedbank/mobile/business/widget/configuration/h;->e()Lio/reactivex/o;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v2, "widgetRepository\n       \u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v2, p0, Lcom/swedbank/mobile/business/widget/root/a$a;->a:Lcom/swedbank/mobile/business/widget/root/a;

    invoke-static {v2}, Lcom/swedbank/mobile/business/widget/root/a;->a(Lcom/swedbank/mobile/business/widget/root/a;)Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v2

    .line 26
    invoke-interface {v2}, Lcom/swedbank/mobile/business/widget/configuration/h;->h()Lio/reactivex/o;

    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    const-string v3, "widgetRepository\n       \u2026  .distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1, v0, v1, v2}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    .line 28
    sget-object v0, Lcom/swedbank/mobile/business/widget/root/a$a$1;->a:Lcom/swedbank/mobile/business/widget/root/a$a$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 35
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object p1

    .line 36
    sget-object v0, Lcom/swedbank/mobile/business/widget/root/g;->b:Lcom/swedbank/mobile/business/widget/root/g;

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 37
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/business/widget/root/g;->a:Lcom/swedbank/mobile/business/widget/root/g;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/widget/root/a$a;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
