.class public final enum Lcom/swedbank/mobile/business/widget/root/g;
.super Ljava/lang/Enum;
.source "ObserveWidgetState.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/widget/root/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/widget/root/g;

.field public static final enum b:Lcom/swedbank/mobile/business/widget/root/g;

.field public static final enum c:Lcom/swedbank/mobile/business/widget/root/g;

.field public static final enum d:Lcom/swedbank/mobile/business/widget/root/g;

.field public static final enum e:Lcom/swedbank/mobile/business/widget/root/g;

.field private static final synthetic f:[Lcom/swedbank/mobile/business/widget/root/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/business/widget/root/g;

    new-instance v1, Lcom/swedbank/mobile/business/widget/root/g;

    const-string v2, "DISABLED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/widget/root/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/widget/root/g;->a:Lcom/swedbank/mobile/business/widget/root/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/widget/root/g;

    const-string v2, "ENABLED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/widget/root/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/widget/root/g;->b:Lcom/swedbank/mobile/business/widget/root/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/widget/root/g;

    const-string v2, "LOADING"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/widget/root/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/widget/root/g;->c:Lcom/swedbank/mobile/business/widget/root/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/widget/root/g;

    const-string v2, "SHOWING_BALANCES"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/widget/root/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/widget/root/g;->d:Lcom/swedbank/mobile/business/widget/root/g;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/widget/root/g;

    const-string v2, "NOT_SHOWING_BALANCES"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/widget/root/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/widget/root/g;->e:Lcom/swedbank/mobile/business/widget/root/g;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/widget/root/g;->f:[Lcom/swedbank/mobile/business/widget/root/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/widget/root/g;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/widget/root/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/widget/root/g;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/widget/root/g;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/widget/root/g;->f:[Lcom/swedbank/mobile/business/widget/root/g;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/widget/root/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/widget/root/g;

    return-object v0
.end method
