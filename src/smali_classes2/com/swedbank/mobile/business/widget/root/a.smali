.class public final Lcom/swedbank/mobile/business/widget/root/a;
.super Ljava/lang/Object;
.source "ObserveWidgetState.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/o<",
        "Lcom/swedbank/mobile/business/widget/root/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/configuration/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/configuration/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/configuration/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "widgetRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/root/a;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/widget/root/a;)Lcom/swedbank/mobile/business/widget/configuration/h;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/business/widget/root/a;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/widget/root/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/root/a;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    .line 14
    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/configuration/h;->a()Lio/reactivex/o;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/swedbank/mobile/business/widget/root/a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/widget/root/a$a;-><init>(Lcom/swedbank/mobile/business/widget/root/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "widgetRepository\n      .\u2026SABLED)\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/root/a;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
