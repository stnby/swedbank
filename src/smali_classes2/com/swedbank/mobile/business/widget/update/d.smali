.class public final Lcom/swedbank/mobile/business/widget/update/d;
.super Ljava/lang/Object;
.source "WidgetUpdateInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/a/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/update/d;->a:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/swedbank/mobile/business/widget/update/d;->b:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/swedbank/mobile/business/widget/update/d;->c:Ljavax/inject/Provider;

    .line 25
    iput-object p4, p0, Lcom/swedbank/mobile/business/widget/update/d;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/widget/update/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/a/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;)",
            "Lcom/swedbank/mobile/business/widget/update/d;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/swedbank/mobile/business/widget/update/d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/business/widget/update/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;
    .locals 5

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/widget/update/d;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/widget/configuration/h;

    iget-object v2, p0, Lcom/swedbank/mobile/business/widget/update/d;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/a/a/a;

    iget-object v3, p0, Lcom/swedbank/mobile/business/widget/update/d;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v4, p0, Lcom/swedbank/mobile/business/widget/update/d;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/widget/update/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;-><init>(Lcom/swedbank/mobile/business/widget/configuration/h;Lcom/swedbank/mobile/business/a/a/a;ZLcom/swedbank/mobile/business/widget/update/e;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/update/d;->a()Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;

    move-result-object v0

    return-object v0
.end method
