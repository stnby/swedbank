.class public final Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WidgetUpdateInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/widget/update/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/configuration/h;

.field private final b:Lcom/swedbank/mobile/business/a/a/a;

.field private final c:Z

.field private final d:Lcom/swedbank/mobile/business/widget/update/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/configuration/h;Lcom/swedbank/mobile/business/a/a/a;ZLcom/swedbank/mobile/business/widget/update/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/configuration/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/a/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "widget_update_listener"
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/widget/update/e;
        .annotation runtime Ljavax/inject/Named;
            value = "widget_update_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "widgetRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopedAccountRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "widgetUpdateListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->b:Lcom/swedbank/mobile/business/a/a/a;

    iput-boolean p3, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->d:Lcom/swedbank/mobile/business/widget/update/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;)Lcom/swedbank/mobile/business/widget/configuration/h;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;)Lcom/swedbank/mobile/business/widget/update/e;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->d:Lcom/swedbank/mobile/business/widget/update/e;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 3

    .line 33
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->c:Z

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/widget/configuration/h;->c(Z)V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->b:Lcom/swedbank/mobile/business/a/a/a;

    .line 36
    invoke-interface {v0}, Lcom/swedbank/mobile/business/a/a/a;->a()Lio/reactivex/w;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/widget/configuration/h;->a(Z)Lio/reactivex/b;

    move-result-object v0

    .line 47
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/widget/update/WidgetUpdateInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->c(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "when {\n      showBalance\u2026dateCompleted()\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 51
    invoke-static {v0, v2, v2, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 56
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
