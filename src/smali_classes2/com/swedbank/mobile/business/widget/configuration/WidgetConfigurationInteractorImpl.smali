.class public final Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WidgetConfigurationInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/widget/configuration/c;
.implements Lcom/swedbank/mobile/business/widget/configuration/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/widget/configuration/g;",
        ">;",
        "Lcom/swedbank/mobile/business/widget/configuration/c;",
        "Lcom/swedbank/mobile/business/widget/configuration/f;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/configuration/h;

.field private final b:Lcom/swedbank/mobile/business/e/i;

.field private final c:Lcom/swedbank/mobile/business/a/b;

.field private final d:Lcom/swedbank/mobile/business/customer/e;

.field private final e:Lcom/swedbank/mobile/business/e/b;

.field private final f:Lcom/swedbank/mobile/business/i/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/configuration/h;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/configuration/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "widget_configuration_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "widgetRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    iput-object p3, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/e;

    iput-object p5, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->e:Lcom/swedbank/mobile/business/e/b;

    iput-object p6, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->f:Lcom/swedbank/mobile/business/i/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;)Lcom/swedbank/mobile/business/widget/configuration/g;
    .locals 0

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/widget/configuration/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;)Lcom/swedbank/mobile/business/i/c;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->f:Lcom/swedbank/mobile/business/i/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 72
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/widget/configuration/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/widget/configuration/g;->a(Z)V

    .line 73
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->f:Lcom/swedbank/mobile/business/i/c;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void
.end method

.method public a(Ljava/util/Set;)V
    .locals 3
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "selectedAccounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    .line 63
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/a/h;->e(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/widget/configuration/h;->a(Ljava/util/List;)Lio/reactivex/b;

    move-result-object p1

    .line 64
    new-instance v0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;)V

    check-cast v0, Lkotlin/e/a/a;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v0, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 82
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/e;

    .line 77
    iget-object v1, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/customer/e;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    .line 56
    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/configuration/h;->b()Lio/reactivex/w;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/widget/configuration/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/widget/configuration/e;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "widgetRepository\n      .\u2026p(List<AccountId>::toSet)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/a/b;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected m_()V
    .locals 9

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->e:Lcom/swedbank/mobile/business/e/b;

    .line 47
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->a()Lio/reactivex/o;

    move-result-object v0

    .line 48
    sget-object v1, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/widget/configuration/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/widget/configuration/e;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$c;->a:Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$c;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 50
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "activityLifecycleMonitor\u2026 { !it }\n        .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 51
    new-instance v0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 80
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
