.class public final Lcom/swedbank/mobile/business/widget/configuration/d;
.super Ljava/lang/Object;
.source "WidgetConfigurationInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->a:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->b:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->c:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->d:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->e:Ljavax/inject/Provider;

    .line 37
    iput-object p6, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/widget/configuration/d;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/configuration/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;)",
            "Lcom/swedbank/mobile/business/widget/configuration/d;"
        }
    .end annotation

    .line 52
    new-instance v7, Lcom/swedbank/mobile/business/widget/configuration/d;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/widget/configuration/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;
    .locals 8

    .line 42
    new-instance v7, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/widget/configuration/h;

    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/e/i;

    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/customer/e;

    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/e/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/configuration/d;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/i/c;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;-><init>(Lcom/swedbank/mobile/business/widget/configuration/h;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/i/c;)V

    return-object v7
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/widget/configuration/d;->a()Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
