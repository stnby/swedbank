.class final synthetic Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;
.super Lkotlin/e/b/i;
.source "WidgetConfigurationInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl;->c()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/Iterable<",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/Set;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 57
    invoke-static {p1}, Lkotlin/a/h;->g(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 2

    const-class v0, Lkotlin/a/h;

    const-string v1, "widget-business_ltRelease"

    invoke-static {v0, v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/widget/configuration/WidgetConfigurationInteractorImpl$a;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "toSet"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "toSet(Ljava/lang/Iterable;)Ljava/util/Set;"

    return-object v0
.end method
