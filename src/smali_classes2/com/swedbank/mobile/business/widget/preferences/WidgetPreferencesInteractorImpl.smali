.class public final Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WidgetPreferencesInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/widget/preferences/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/widget/preferences/d;",
        ">;",
        "Lcom/swedbank/mobile/business/widget/preferences/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/configuration/h;

.field private final b:Lcom/swedbank/mobile/business/e/i;

.field private final c:Lcom/swedbank/mobile/business/a/b;

.field private final d:Lcom/swedbank/mobile/business/customer/e;

.field private final e:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/configuration/h;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/configuration/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "widget_preferences_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "widgetRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    iput-object p3, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/e;

    iput-object p5, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->e:Lcom/swedbank/mobile/business/preferences/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Set;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selectedAccounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    .line 43
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/a/h;->e(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/widget/configuration/h;->a(Ljava/util/List;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->e:Lcom/swedbank/mobile/business/preferences/a;

    .line 46
    const-class v1, Lcom/swedbank/mobile/business/widget/preferences/d;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/e;

    .line 49
    iget-object v1, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->b:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/customer/e;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->a:Lcom/swedbank/mobile/business/widget/configuration/h;

    .line 37
    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/configuration/h;->b()Lio/reactivex/w;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl$a;->a:Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/widget/preferences/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/widget/preferences/c;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "widgetRepository\n      .\u2026p(List<AccountId>::toSet)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/widget/preferences/WidgetPreferencesInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/a/b;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
