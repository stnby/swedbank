.class public final enum Lcom/swedbank/mobile/business/c/c;
.super Ljava/lang/Enum;
.source "Configuration.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/c/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/c/c;

.field public static final enum b:Lcom/swedbank/mobile/business/c/c;

.field public static final enum c:Lcom/swedbank/mobile/business/c/c;

.field public static final enum d:Lcom/swedbank/mobile/business/c/c;

.field private static final synthetic e:[Lcom/swedbank/mobile/business/c/c;


# instance fields
.field private final f:Ljava/util/Locale;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/business/c/g;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/business/c/e;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 20

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/business/c/c;

    new-instance v10, Lcom/swedbank/mobile/business/c/c;

    const-string v2, "EE"

    .line 32
    sget-object v4, Lcom/swedbank/mobile/business/c/g;->c:Lcom/swedbank/mobile/business/c/g;

    sget-object v5, Lcom/swedbank/mobile/business/c/e;->a:Lcom/swedbank/mobile/business/c/e;

    const-string v6, "+372"

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/swedbank/mobile/business/c/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V

    sput-object v10, Lcom/swedbank/mobile/business/c/c;->a:Lcom/swedbank/mobile/business/c/c;

    const/4 v1, 0x0

    aput-object v10, v0, v1

    new-instance v1, Lcom/swedbank/mobile/business/c/c;

    const-string v12, "LV"

    .line 33
    sget-object v14, Lcom/swedbank/mobile/business/c/g;->d:Lcom/swedbank/mobile/business/c/g;

    sget-object v15, Lcom/swedbank/mobile/business/c/e;->a:Lcom/swedbank/mobile/business/c/e;

    const-string v16, "+371"

    const/4 v13, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x8

    const/16 v19, 0x0

    move-object v11, v1

    invoke-direct/range {v11 .. v19}, Lcom/swedbank/mobile/business/c/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/c;->b:Lcom/swedbank/mobile/business/c/c;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/c/c;

    const-string v4, "LT"

    .line 34
    sget-object v6, Lcom/swedbank/mobile/business/c/g;->e:Lcom/swedbank/mobile/business/c/g;

    sget-object v7, Lcom/swedbank/mobile/business/c/e;->a:Lcom/swedbank/mobile/business/c/e;

    const-string v8, "+370"

    const/4 v5, 0x2

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v11}, Lcom/swedbank/mobile/business/c/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/c;->c:Lcom/swedbank/mobile/business/c/c;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/c/c;

    const-string v4, "UNKNOWN"

    .line 35
    sget-object v6, Lcom/swedbank/mobile/business/c/g;->a:Lcom/swedbank/mobile/business/c/g;

    sget-object v7, Lcom/swedbank/mobile/business/c/e;->a:Lcom/swedbank/mobile/business/c/e;

    const-string v8, ""

    const/4 v5, 0x3

    move-object v3, v1

    invoke-direct/range {v3 .. v11}, Lcom/swedbank/mobile/business/c/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/c;->d:Lcom/swedbank/mobile/business/c/c;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/business/c/c;->e:[Lcom/swedbank/mobile/business/c/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/c/g;",
            "Lcom/swedbank/mobile/business/c/e;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/business/c/c;->g:Lcom/swedbank/mobile/business/c/g;

    iput-object p4, p0, Lcom/swedbank/mobile/business/c/c;->h:Lcom/swedbank/mobile/business/c/e;

    iput-object p5, p0, Lcom/swedbank/mobile/business/c/c;->i:Ljava/lang/String;

    iput-object p6, p0, Lcom/swedbank/mobile/business/c/c;->j:Ljava/util/List;

    .line 37
    new-instance p1, Ljava/util/Locale;

    iget-object p2, p0, Lcom/swedbank/mobile/business/c/c;->g:Lcom/swedbank/mobile/business/c/g;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/c/c;->f:Ljava/util/Locale;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V
    .locals 7

    and-int/lit8 p7, p7, 0x8

    if-eqz p7, :cond_0

    const/4 p6, 0x3

    .line 31
    new-array p6, p6, [Lcom/swedbank/mobile/business/c/g;

    const/4 p7, 0x0

    aput-object p3, p6, p7

    const/4 p7, 0x1

    sget-object p8, Lcom/swedbank/mobile/business/c/g;->a:Lcom/swedbank/mobile/business/c/g;

    aput-object p8, p6, p7

    const/4 p7, 0x2

    sget-object p8, Lcom/swedbank/mobile/business/c/g;->b:Lcom/swedbank/mobile/business/c/g;

    aput-object p8, p6, p7

    invoke-static {p6}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p6

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/c/c;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/c/g;Lcom/swedbank/mobile/business/c/e;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/c/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/c/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/c/c;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/c/c;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/c/c;->e:[Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/c/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/c/c;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/c/g;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/business/c/c;->g:Lcom/swedbank/mobile/business/c/g;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/c/g;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "language"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/c/c;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/c/c;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/swedbank/mobile/business/c/g;)Ljava/util/Locale;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "language"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Ljava/util/Locale;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/g;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/c/c;->j:Ljava/util/List;

    return-object v0
.end method
