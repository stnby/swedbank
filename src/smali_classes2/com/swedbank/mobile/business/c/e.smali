.class public final enum Lcom/swedbank/mobile/business/c/e;
.super Ljava/lang/Enum;
.source "Configuration.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/c/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/c/e;

.field private static final synthetic b:[Lcom/swedbank/mobile/business/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/swedbank/mobile/business/c/e;

    new-instance v1, Lcom/swedbank/mobile/business/c/e;

    const-string v2, "EUR"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/c/e;->a:Lcom/swedbank/mobile/business/c/e;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/c/e;->b:[Lcom/swedbank/mobile/business/c/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/c/e;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/c/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/c/e;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/c/e;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/c/e;->b:[Lcom/swedbank/mobile/business/c/e;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/c/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/c/e;

    return-object v0
.end method
