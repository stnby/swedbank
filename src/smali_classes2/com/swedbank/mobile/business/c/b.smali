.class public final Lcom/swedbank/mobile/business/c/b;
.super Ljava/lang/Object;
.source "Configuration.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/c/d;
.implements Lcom/swedbank/mobile/business/c/f;


# instance fields
.field private final synthetic a:Lcom/swedbank/mobile/business/c/f;

.field private final synthetic b:Lcom/swedbank/mobile/business/c/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/d;Lcom/swedbank/mobile/business/c/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "country"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endpoint"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p2, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    .line 9
    iput-object p1, p0, Lcom/swedbank/mobile/business/c/b;->b:Lcom/swedbank/mobile/business/c/d;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/swedbank/mobile/business/c/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/c/b;->b:Lcom/swedbank/mobile/business/c/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/d;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object v0

    return-object v0
.end method
