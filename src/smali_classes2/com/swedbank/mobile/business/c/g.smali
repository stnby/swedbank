.class public final enum Lcom/swedbank/mobile/business/c/g;
.super Ljava/lang/Enum;
.source "Configuration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/c/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/c/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/c/g;

.field public static final enum b:Lcom/swedbank/mobile/business/c/g;

.field public static final enum c:Lcom/swedbank/mobile/business/c/g;

.field public static final enum d:Lcom/swedbank/mobile/business/c/g;

.field public static final enum e:Lcom/swedbank/mobile/business/c/g;

.field public static final enum f:Lcom/swedbank/mobile/business/c/g;

.field public static final g:Lcom/swedbank/mobile/business/c/g$a;

.field private static final synthetic h:[Lcom/swedbank/mobile/business/c/g;


# instance fields
.field private final i:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/swedbank/mobile/business/c/g;

    new-instance v1, Lcom/swedbank/mobile/business/c/g;

    const-string v2, "EN"

    const-string v3, "ENG"

    const/4 v4, 0x0

    .line 46
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/c/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/g;->a:Lcom/swedbank/mobile/business/c/g;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/c/g;

    const-string v2, "RU"

    const-string v3, "RUS"

    const/4 v4, 0x1

    .line 47
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/c/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/g;->b:Lcom/swedbank/mobile/business/c/g;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/c/g;

    const-string v2, "ET"

    const-string v3, "EST"

    const/4 v4, 0x2

    .line 48
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/c/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/g;->c:Lcom/swedbank/mobile/business/c/g;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/c/g;

    const-string v2, "LV"

    const-string v3, "LAV"

    const/4 v4, 0x3

    .line 49
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/c/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/g;->d:Lcom/swedbank/mobile/business/c/g;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/c/g;

    const-string v2, "LT"

    const-string v3, "LIT"

    const/4 v4, 0x4

    .line 50
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/c/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/g;->e:Lcom/swedbank/mobile/business/c/g;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/c/g;

    const-string v2, "UNKNOWN"

    const-string v3, ""

    const/4 v4, 0x5

    .line 51
    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/c/g;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/swedbank/mobile/business/c/g;->f:Lcom/swedbank/mobile/business/c/g;

    aput-object v1, v0, v4

    sput-object v0, Lcom/swedbank/mobile/business/c/g;->h:[Lcom/swedbank/mobile/business/c/g;

    new-instance v0, Lcom/swedbank/mobile/business/c/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/c/g$a;-><init>(Lkotlin/e/b/g;)V

    sput-object v0, Lcom/swedbank/mobile/business/c/g;->g:Lcom/swedbank/mobile/business/c/g$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/business/c/g;->i:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/c/g;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/c/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/c/g;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/c/g;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/c/g;->h:[Lcom/swedbank/mobile/business/c/g;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/c/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/c/g;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/c/g;->i:Ljava/lang/String;

    return-object v0
.end method
