.class public final Lcom/swedbank/mobile/business/general/a;
.super Ljava/lang/Object;
.source "PollData.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/y;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/util/y;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/y;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/y;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "delay"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interval"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/general/a;->a:Lcom/swedbank/mobile/business/util/y;

    iput-object p2, p0, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 13
    invoke-static {}, Lcom/swedbank/mobile/business/general/b;->a()Lcom/swedbank/mobile/business/util/y;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 14
    invoke-static {}, Lcom/swedbank/mobile/business/general/b;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object p2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;)V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/y;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/y;->b()Ljava/util/concurrent/TimeUnit;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.timer(interval.time, interval.unit)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/business/util/y;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/business/general/a;->a:Lcom/swedbank/mobile/business/util/y;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/business/general/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/general/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/a;->a:Lcom/swedbank/mobile/business/util/y;

    iget-object v1, p1, Lcom/swedbank/mobile/business/general/a;->a:Lcom/swedbank/mobile/business/util/y;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    iget-object p1, p1, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/a;->a:Lcom/swedbank/mobile/business/util/y;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PollData(delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/general/a;->a:Lcom/swedbank/mobile/business/util/y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/general/a;->b:Lcom/swedbank/mobile/business/util/y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
