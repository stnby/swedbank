.class public final Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ConfirmationDialogInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/a;
.implements Lcom/swedbank/mobile/business/general/confirmation/c;
.implements Lcom/swedbank/mobile/business/general/confirmation/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/general/confirmation/e;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/a;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;",
        "Lcom/swedbank/mobile/business/general/confirmation/d;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final synthetic b:Lcom/swedbank/mobile/business/general/confirmation/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/general/confirmation/c;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "confirmation_dialog_tag"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "confirmationDialogListener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tag"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/general/confirmation/c;->b()V

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public q_()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/confirmation/ConfirmationDialogInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/general/confirmation/c;->q_()V

    return-void
.end method
