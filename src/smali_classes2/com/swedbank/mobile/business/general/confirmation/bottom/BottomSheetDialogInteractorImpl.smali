.class public final Lcom/swedbank/mobile/business/general/confirmation/bottom/BottomSheetDialogInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BottomSheetDialogInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/a;
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/d;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/a;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;"
    }
.end annotation


# instance fields
.field private final synthetic a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "bottom_sheet_dialog_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/business/general/confirmation/bottom/BottomSheetDialogInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/confirmation/bottom/BottomSheetDialogInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/general/confirmation/bottom/c;->a()V

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/general/confirmation/bottom/BottomSheetDialogInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/general/confirmation/bottom/c;->a(Ljava/lang/Object;)V

    return-void
.end method
