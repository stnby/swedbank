.class public final Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CustomerSelectionInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/customer/selection/a;
.implements Lcom/swedbank/mobile/business/preferences/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/customer/selection/d;",
        ">;",
        "Lcom/swedbank/mobile/business/customer/selection/a;",
        "Lcom/swedbank/mobile/business/preferences/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/customer/e;

.field private final c:Lcom/swedbank/mobile/business/b/c;

.field private final d:Lcom/swedbank/mobile/business/customer/selection/c;

.field private final e:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/b/c;Lcom/swedbank/mobile/business/customer/selection/c;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/customer/selection/c;
        .annotation runtime Ljavax/inject/Named;
            value = "customer_selection_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "logOutUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/business/customer/e;",
            "Lcom/swedbank/mobile/business/b/c;",
            "Lcom/swedbank/mobile/business/customer/selection/c;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankInfoRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logOut"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getJumpToIbankLink"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    iput-object p3, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->c:Lcom/swedbank/mobile/business/b/c;

    iput-object p4, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/selection/c;

    iput-object p5, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    iput-object p6, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    .line 61
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/customer/e;->c(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    .line 51
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->c()Lcom/swedbank/mobile/business/e/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/e/h;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    .line 55
    iget-object v1, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/customer/e;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/customer/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    .line 58
    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/e;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .line 71
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/customer/selection/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/selection/d;->a()V

    return-void
.end method

.method public e()V
    .locals 4

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->c:Lcom/swedbank/mobile/business/b/c;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/b/c;->c()Lcom/swedbank/mobile/business/e;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 75
    new-instance v1, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/customer/selection/d;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/customer/selection/d;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 84
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public f()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CheckResult"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 68
    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    return-void
.end method

.method public g()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/selection/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/selection/c;->b()V

    return-void
.end method

.method public i()V
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/selection/CustomerSelectionInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/customer/selection/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/selection/d;->b()V

    return-void
.end method
