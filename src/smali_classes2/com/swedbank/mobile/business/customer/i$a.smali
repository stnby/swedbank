.class final Lcom/swedbank/mobile/business/customer/i$a;
.super Ljava/lang/Object;
.source "GetMobileAppId.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/customer/i;->a()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/customer/i$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/customer/i$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/customer/i$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/customer/i$a;->a:Lcom/swedbank/mobile/business/customer/i$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/h/b;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/h/b;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    instance-of v0, p1, Lcom/swedbank/mobile/business/h/b$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/h/b$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/h/b$b;->a()Ljava/lang/String;

    move-result-object p1

    .line 34
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 27
    :cond_0
    instance-of p1, p1, Lcom/swedbank/mobile/business/h/b$a;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Mobile app activation should have been successful"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/h/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/customer/i$a;->a(Lcom/swedbank/mobile/business/h/b;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
