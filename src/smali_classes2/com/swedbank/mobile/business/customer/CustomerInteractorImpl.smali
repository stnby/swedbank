.class public final Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CustomerInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/retry/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/customer/f;",
        ">;",
        "Lcom/swedbank/mobile/business/general/retry/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/customer/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/customer/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lcom/swedbank/mobile/business/customer/e;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lio/reactivex/w;
    .locals 0

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 83
    invoke-static {p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->c(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lio/reactivex/w;

    move-result-object v0

    .line 86
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "flow()\n        .flatMapC\u2026ismissLoading))\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 85
    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 87
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method protected m_()V
    .locals 8

    .line 72
    invoke-static {p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->c(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lio/reactivex/w;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "flow()\n        .flatMapC\u2026ismissLoading))\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 74
    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 76
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    .line 26
    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/e;->d()Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$b;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$b;

    check-cast v1, Lio/reactivex/c/d;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$c;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/customer/f;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/customer/f;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/customer/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/customer/d;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "customerRepository\n     \u2026te()::toSelectedCustomer)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/e;

    .line 34
    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/e;->b()Lio/reactivex/o;

    move-result-object v0

    .line 35
    sget-object v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$d;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$d;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->o(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "customerRepository\n     \u2026nfo\n          )\n        }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    .line 45
    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 81
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
