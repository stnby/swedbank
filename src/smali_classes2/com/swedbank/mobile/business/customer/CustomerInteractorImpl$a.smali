.class public final Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;
.super Ljava/lang/Object;
.source "CustomerInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/customer/f;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/customer/f;)Lio/reactivex/b;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/customer/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$1;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$1;-><init>(Lcom/swedbank/mobile/business/customer/f;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/business/customer/c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/customer/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->b(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lcom/swedbank/mobile/business/customer/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/business/customer/e;->c()Lio/reactivex/j;

    move-result-object v1

    check-cast v1, Lio/reactivex/n;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object v0

    .line 56
    sget-object v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$2;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$2;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->b(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lcom/swedbank/mobile/business/customer/e;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;->a:Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;->a(Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/business/e/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/customer/e;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3;-><init>(Lcom/swedbank/mobile/business/customer/f;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$4;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$4;-><init>(Lcom/swedbank/mobile/business/customer/f;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$5;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$5;-><init>(Lcom/swedbank/mobile/business/customer/f;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance p1, Lcom/swedbank/mobile/business/customer/c;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/customer/c;-><init>(Lkotlin/e/a/a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/business/customer/f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;->a(Lcom/swedbank/mobile/business/customer/f;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
