.class final Lcom/swedbank/mobile/business/customer/g$a$1;
.super Ljava/lang/Object;
.source "GetMobileAgreementId.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/customer/g$a;->a(Lcom/swedbank/mobile/business/customer/a;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/customer/g$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/customer/g$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/g$a$1;->a:Lcom/swedbank/mobile/business/customer/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 33
    iget-object p1, p0, Lcom/swedbank/mobile/business/customer/g$a$1;->a:Lcom/swedbank/mobile/business/customer/g$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/customer/g$a;->a:Lcom/swedbank/mobile/business/customer/g;

    invoke-static {p1}, Lcom/swedbank/mobile/business/customer/g;->b(Lcom/swedbank/mobile/business/customer/g;)Lcom/swedbank/mobile/business/customer/e;

    move-result-object p1

    .line 34
    invoke-interface {p1}, Lcom/swedbank/mobile/business/customer/e;->b()Lio/reactivex/o;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object p1

    .line 36
    sget-object v0, Lcom/swedbank/mobile/business/customer/g$a$1$1;->a:Lcom/swedbank/mobile/business/customer/g$a$1$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    return-object p1

    .line 48
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Generate mobile agreement should have been successful"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/customer/g$a$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
