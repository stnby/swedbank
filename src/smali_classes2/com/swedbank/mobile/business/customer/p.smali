.class public final Lcom/swedbank/mobile/business/customer/p;
.super Ljava/lang/Object;
.source "SyncMobileAppId.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/h/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/h/c;

.field private final b:Lcom/swedbank/mobile/business/push/h;

.field private final c:Lcom/swedbank/mobile/business/e/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/h/c;Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/business/e/i;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/h/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "serviceOrderingRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/p;->a:Lcom/swedbank/mobile/business/h/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/customer/p;->b:Lcom/swedbank/mobile/business/push/h;

    iput-object p3, p0, Lcom/swedbank/mobile/business/customer/p;->c:Lcom/swedbank/mobile/business/e/i;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/customer/p;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/customer/p;->c:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/customer/p;)Lcom/swedbank/mobile/business/h/c;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/customer/p;->a:Lcom/swedbank/mobile/business/h/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/h/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/p;->b:Lcom/swedbank/mobile/business/push/h;

    .line 19
    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/h;->a()Lio/reactivex/w;

    move-result-object v0

    .line 20
    new-instance v1, Lcom/swedbank/mobile/business/customer/p$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/customer/p$a;-><init>(Lcom/swedbank/mobile/business/customer/p;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "pushRepository\n      .ge\u2026oken = pushToken)\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/p;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
