.class final Lcom/swedbank/mobile/business/customer/g$a;
.super Ljava/lang/Object;
.source "GetMobileAgreementId.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/customer/g;->a()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/customer/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/customer/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/g$a;->a:Lcom/swedbank/mobile/business/customer/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/customer/a;)Lio/reactivex/j;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/customer/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/customer/a;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/customer/a;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 21
    invoke-static {v0}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/g$a;->a:Lcom/swedbank/mobile/business/customer/g;

    invoke-static {v0}, Lcom/swedbank/mobile/business/customer/g;->a(Lcom/swedbank/mobile/business/customer/g;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->c()Lcom/swedbank/mobile/business/e/h;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/swedbank/mobile/business/customer/g$a;->a:Lcom/swedbank/mobile/business/customer/g;

    invoke-static {v1}, Lcom/swedbank/mobile/business/customer/g;->a(Lcom/swedbank/mobile/business/customer/g;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->a()Ljava/lang/String;

    move-result-object v1

    .line 25
    iget-object v2, p0, Lcom/swedbank/mobile/business/customer/g$a;->a:Lcom/swedbank/mobile/business/customer/g;

    invoke-static {v2}, Lcom/swedbank/mobile/business/customer/g;->b(Lcom/swedbank/mobile/business/customer/g;)Lcom/swedbank/mobile/business/customer/e;

    move-result-object v2

    .line 26
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/customer/a;->a()Ljava/lang/String;

    move-result-object p1

    .line 25
    invoke-interface {v2, p1, v1, v0}, Lcom/swedbank/mobile/business/customer/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/e/h;)Lio/reactivex/w;

    move-result-object p1

    .line 29
    new-instance v0, Lcom/swedbank/mobile/business/customer/g$a$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/customer/g$a$1;-><init>(Lcom/swedbank/mobile/business/customer/g$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/customer/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/customer/g$a;->a(Lcom/swedbank/mobile/business/customer/a;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
