.class public final Lcom/swedbank/mobile/business/customer/g;
.super Ljava/lang/Object;
.source "GetMobileAgreementId.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/j<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/customer/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/customer/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/g;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/customer/g;->b:Lcom/swedbank/mobile/business/customer/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/customer/g;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/business/customer/g;->a:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/customer/g;)Lcom/swedbank/mobile/business/customer/e;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/business/customer/g;->b:Lcom/swedbank/mobile/business/customer/e;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/g;->b:Lcom/swedbank/mobile/business/customer/e;

    .line 16
    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/e;->b()Lio/reactivex/o;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 18
    new-instance v1, Lcom/swedbank/mobile/business/customer/g$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/customer/g$a;-><init>(Lcom/swedbank/mobile/business/customer/g;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "customerRepository\n     \u2026      }\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/customer/g;->a()Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method
