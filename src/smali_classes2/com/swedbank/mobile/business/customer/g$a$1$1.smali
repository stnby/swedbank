.class final Lcom/swedbank/mobile/business/customer/g$a$1$1;
.super Ljava/lang/Object;
.source "GetMobileAgreementId.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/customer/g$a$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/customer/g$a$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/customer/g$a$1$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/customer/g$a$1$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/customer/g$a$1$1;->a:Lcom/swedbank/mobile/business/customer/g$a$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/customer/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/customer/g$a$1$1;->a(Lcom/swedbank/mobile/business/customer/a;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/business/customer/a;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/customer/a;->c()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 48
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "After trying to generate mobile agreement it was still missing for logged in customer"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
