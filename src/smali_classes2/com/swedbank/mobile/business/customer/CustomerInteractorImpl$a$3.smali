.class public final Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3;
.super Ljava/lang/Object;
.source "CustomerInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a;->a(Lcom/swedbank/mobile/business/customer/f;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/util/p;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/customer/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/f;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3;->a:Lcom/swedbank/mobile/business/customer/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/f;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 61
    :cond_0
    instance-of p1, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3$1;

    iget-object v0, p0, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3;->a:Lcom/swedbank/mobile/business/customer/f;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3$1;-><init>(Lcom/swedbank/mobile/business/customer/f;)V

    check-cast p1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/business/customer/c;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/customer/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/customer/CustomerInteractorImpl$a$3;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
