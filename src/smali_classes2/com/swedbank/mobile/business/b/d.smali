.class public final Lcom/swedbank/mobile/business/b/d;
.super Ljava/lang/Object;
.source "GetBankContacts.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lcom/swedbank/mobile/business/b/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/b/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/b/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bankInfoRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/b/d;->a:Lcom/swedbank/mobile/business/b/c;

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/b/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/swedbank/mobile/business/b/d;->a:Lcom/swedbank/mobile/business/b/c;

    .line 10
    new-instance v1, Lcom/swedbank/mobile/business/b/b;

    .line 11
    invoke-interface {v0}, Lcom/swedbank/mobile/business/b/c;->a()Ljava/util/List;

    move-result-object v2

    .line 12
    invoke-interface {v0}, Lcom/swedbank/mobile/business/b/c;->b()Ljava/util/List;

    move-result-object v0

    .line 10
    invoke-direct {v1, v2, v0}, Lcom/swedbank/mobile/business/b/b;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v1
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/b/d;->a()Lcom/swedbank/mobile/business/b/b;

    move-result-object v0

    return-object v0
.end method
