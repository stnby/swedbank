.class public final Lcom/swedbank/mobile/business/challenge/c;
.super Ljava/lang/Object;
.source "ChallengeInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/challenge/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/challenge/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/challenge/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/challenge/d;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/business/challenge/c;->a:Ljavax/inject/Provider;

    .line 15
    iput-object p2, p0, Lcom/swedbank/mobile/business/challenge/c;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/challenge/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/challenge/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/challenge/d;",
            ">;)",
            "Lcom/swedbank/mobile/business/challenge/c;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/challenge/c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/challenge/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;
    .locals 3

    .line 20
    new-instance v0, Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/challenge/c;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/challenge/a;

    iget-object v2, p0, Lcom/swedbank/mobile/business/challenge/c;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/challenge/d;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;-><init>(Lcom/swedbank/mobile/business/challenge/a;Lcom/swedbank/mobile/business/challenge/d;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/challenge/c;->a()Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;

    move-result-object v0

    return-object v0
.end method
