.class public final Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ChallengeInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/challenge/b;
.implements Lcom/swedbank/mobile/business/challenge/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/challenge/e;",
        ">;",
        "Lcom/swedbank/mobile/business/challenge/b;",
        "Lcom/swedbank/mobile/business/challenge/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/challenge/a;

.field private final synthetic b:Lcom/swedbank/mobile/business/challenge/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/challenge/a;Lcom/swedbank/mobile/business/challenge/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/challenge/a;
        .annotation runtime Ljavax/inject/Named;
            value = "challenge_info"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/challenge/d;
        .annotation runtime Ljavax/inject/Named;
            value = "challenge_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "challengeInfo"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;->b:Lcom/swedbank/mobile/business/challenge/d;

    iput-object p1, p0, Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;->a:Lcom/swedbank/mobile/business/challenge/a;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/challenge/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;->a:Lcom/swedbank/mobile/business/challenge/a;

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(challengeInfo)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/challenge/ChallengeInteractorImpl;->b:Lcom/swedbank/mobile/business/challenge/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/challenge/d;->i()V

    return-void
.end method
