.class public final Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OrderCardInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/order/c;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/cards/m;

.field private final c:Lcom/swedbank/mobile/business/cards/order/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/m;Lcom/swedbank/mobile/business/cards/order/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/cards/order/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/business/cards/m;",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "getJumpToIbankLink"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardsRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/m;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/order/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;)Lcom/swedbank/mobile/business/cards/order/c;
    .locals 0

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/order/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;)Lcom/swedbank/mobile/business/cards/order/b;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/order/b;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/order/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/order/c;->b()V

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/order/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/order/b;->q()V

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/order/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/order/c;->b()V

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/m;

    check-cast p1, Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/cards/m;->b(Lcom/swedbank/mobile/business/cards/CardClass;)Lcom/swedbank/mobile/business/e;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 37
    new-instance v0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v0, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 45
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method protected m_()V
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/order/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/order/c;->a()V

    return-void
.end method
