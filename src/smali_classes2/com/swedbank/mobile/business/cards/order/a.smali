.class public final Lcom/swedbank/mobile/business/cards/order/a;
.super Ljava/lang/Object;
.source "OrderCardInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/m;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/m;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/order/a;->a:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/order/a;->b:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/order/a;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/order/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/m;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/order/a;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/swedbank/mobile/business/cards/order/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/cards/order/a;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;
    .locals 4

    .line 29
    new-instance v0, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/order/a;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/b;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/order/a;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/m;

    iget-object v3, p0, Lcom/swedbank/mobile/business/cards/order/a;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/cards/order/b;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;-><init>(Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/m;Lcom/swedbank/mobile/business/cards/order/b;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/order/a;->a()Lcom/swedbank/mobile/business/cards/order/OrderCardInteractorImpl;

    move-result-object v0

    return-object v0
.end method
