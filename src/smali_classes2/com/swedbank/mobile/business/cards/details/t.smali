.class public final Lcom/swedbank/mobile/business/cards/details/t;
.super Ljava/lang/Object;
.source "UnblockCard.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Ljava/lang/String;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/cards/details/n;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/t;->a:Lcom/swedbank/mobile/business/cards/m;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/details/t;)Lcom/swedbank/mobile/business/cards/m;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/t;->a:Lcom/swedbank/mobile/business/cards/m;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/t;->a:Lcom/swedbank/mobile/business/cards/m;

    .line 16
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/m;->c(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 17
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/t$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/cards/details/t$a;-><init>(Lcom/swedbank/mobile/business/cards/details/t;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "cardsRepository\n      .u\u2026ust(it)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/t;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
