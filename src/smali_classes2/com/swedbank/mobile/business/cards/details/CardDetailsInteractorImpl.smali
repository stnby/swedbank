.class public final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/details/h;
.implements Lcom/swedbank/mobile/business/cards/details/l;
.implements Lcom/swedbank/mobile/business/cards/order/b;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
.implements Lcom/swedbank/mobile/business/general/confirmation/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/details/m;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/details/h;",
        "Lcom/swedbank/mobile/business/cards/details/l;",
        "Lcom/swedbank/mobile/business/cards/order/b;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/general/confirmation/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Lio/reactivex/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/k/b<",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/cards/details/k;

.field private final e:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private final g:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/n;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final l:Lcom/swedbank/mobile/business/cards/m;

.field private final m:Lcom/swedbank/mobile/business/e/i;

.field private final n:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/details/k;Lcom/swedbank/mobile/architect/business/b;Ljava/lang/String;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/m;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/details/k;
        .annotation runtime Ljavax/inject/Named;
            value = "card_details_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "observeCardDetailsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "displayed_card_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "blockCardUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "unblockCardUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "changeCardContactlessStatusUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteWalletCardUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/cards/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/k;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/n;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/business/cards/m;",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "detailsListener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeCardDetails"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardId"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "blockCard"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unblockCard"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeCardContactlessStatus"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteWalletCard"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getJumpToIbankLink"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardsRepository"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/details/k;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->f:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/b;

    iput-object p6, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/b;

    iput-object p7, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j:Lcom/swedbank/mobile/architect/business/b;

    iput-object p8, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->k:Lcom/swedbank/mobile/architect/business/b;

    iput-object p9, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->l:Lcom/swedbank/mobile/business/cards/m;

    iput-object p10, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->m:Lcom/swedbank/mobile/business/e/i;

    iput-object p11, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->n:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 70
    new-instance p1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$e;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast p1, Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    .line 73
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    new-instance p2, Lcom/swedbank/mobile/business/cards/details/q$a;

    iget-object p3, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->f:Ljava/lang/String;

    invoke-direct {p2, p3}, Lcom/swedbank/mobile/business/cards/details/q$a;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/o;

    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/business/cards/m;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->l:Lcom/swedbank/mobile/business/cards/m;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;Lio/reactivex/k/b;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c:Lio/reactivex/k/b;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Ljava/lang/String;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->f:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lio/reactivex/k/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c:Lio/reactivex/k/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/business/cards/details/m;
    .locals 0

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/details/m;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->n:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->k:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method


# virtual methods
.method public b()V
    .locals 1

    .line 224
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/m;->b()V

    .line 225
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c:Lio/reactivex/k/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/reactivex/k/b;->onComplete()V

    :cond_0
    const/4 v0, 0x0

    .line 226
    check-cast v0, Lio/reactivex/k/b;

    iput-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c:Lio/reactivex/k/b;

    return-void
.end method

.method public c()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/details/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/k;->c()V

    return-void
.end method

.method public e()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->n:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 94
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->b()Lio/reactivex/o;

    move-result-object v0

    .line 95
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$k;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$k;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "walletRepository\n      .\u2026  .toObservable()\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    return-object v0
.end method

.method public g()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->l:Lcom/swedbank/mobile/business/cards/m;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/m;->j(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public h()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    .line 107
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026tate}\")\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    .line 140
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026  }\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    .line 161
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 162
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026  }\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 180
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->m:Lcom/swedbank/mobile/business/e/i;

    .line 181
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->k()Lio/reactivex/o;

    move-result-object v0

    .line 182
    sget-object v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$i;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$i;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "deviceRepository\n      .\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public l()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    .line 186
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 187
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026rdId) }\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public m()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 198
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    .line 199
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v0

    .line 200
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$l;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$l;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026 .ignoreElement()\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 82
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->l:Lcom/swedbank/mobile/business/cards/m;

    .line 83
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/m;->f(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$j;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$j;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    return-void
.end method

.method public n()V
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/m;->g()V

    return-void
.end method

.method public n_()V
    .locals 1

    .line 194
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/m;->c()V

    return-void
.end method

.method public o()V
    .locals 4

    .line 234
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b:Lio/reactivex/o;

    .line 235
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 236
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026etCardClass()))\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$h;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/cards/details/m;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 246
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public o_()V
    .locals 1

    .line 196
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/m;->c()V

    return-void
.end method

.method public final p()Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object v0
.end method

.method public q()V
    .locals 1

    .line 231
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/m;->h()V

    return-void
.end method

.method public q_()V
    .locals 2

    .line 207
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/details/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/m;->b()V

    .line 208
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 209
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/y;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->c(Lio/reactivex/y;)Lio/reactivex/y;

    move-result-object v0

    const-string v1, "blockCard(cardId)\n      \u2026ll\n          }\n        })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/b/c;

    .line 244
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
