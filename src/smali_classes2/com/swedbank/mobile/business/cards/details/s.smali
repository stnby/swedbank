.class public final Lcom/swedbank/mobile/business/cards/details/s;
.super Ljava/lang/Object;
.source "ObserveCardDetails_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/details/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/s;->a:Ljavax/inject/Provider;

    .line 17
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/details/s;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/details/s;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/s;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/cards/details/s;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/details/r;
    .locals 3

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/r;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/s;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/cards/u;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/details/s;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/details/r;-><init>(Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/cards/wallet/ad;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/s;->a()Lcom/swedbank/mobile/business/cards/details/r;

    move-result-object v0

    return-object v0
.end method
