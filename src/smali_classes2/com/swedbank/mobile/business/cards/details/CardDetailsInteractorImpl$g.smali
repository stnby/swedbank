.class final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;
.super Ljava/lang/Object;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->o()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/w;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/a;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->i(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/e;->a(Lcom/swedbank/mobile/business/cards/a;)Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lcom/swedbank/mobile/business/cards/m;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/CardClass;)Lcom/swedbank/mobile/business/e;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$g;->a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
