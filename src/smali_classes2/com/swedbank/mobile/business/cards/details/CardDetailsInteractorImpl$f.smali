.class public final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;
.super Lio/reactivex/f/b;
.source "CardDetailsInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->q_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/f/b<",
        "Lcom/swedbank/mobile/business/cards/details/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 209
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-direct {p0}, Lio/reactivex/f/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/details/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/details/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lio/reactivex/k/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lio/reactivex/k/b;->a_(Ljava/lang/Object;)V

    .line 212
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    const/4 v0, 0x0

    check-cast v0, Lio/reactivex/k/b;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;Lio/reactivex/k/b;)V

    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .line 209
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;->a(Lcom/swedbank/mobile/business/cards/details/c;)V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "fatalError"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lio/reactivex/k/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lio/reactivex/k/b;->onError(Ljava/lang/Throwable;)V

    .line 217
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    const/4 v0, 0x0

    check-cast v0, Lio/reactivex/k/b;

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;Lio/reactivex/k/b;)V

    return-void
.end method
