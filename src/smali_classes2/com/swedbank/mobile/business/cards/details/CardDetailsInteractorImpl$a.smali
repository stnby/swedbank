.class final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;
.super Ljava/lang/Object;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->h()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/a;",
            ")",
            "Lio/reactivex/j<",
            "+",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/cards/details/i;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot change card state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 127
    :pswitch_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->e(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 128
    sget-object v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    return-object p1

    .line 111
    :pswitch_1
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lio/reactivex/k/b;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 114
    invoke-static {}, Lio/reactivex/k/b;->e()Lio/reactivex/k/b;

    move-result-object v0

    const-string v1, "MaybeSubject.create<CardBlockResult>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v1, v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;Lio/reactivex/k/b;)V

    .line 117
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;Lcom/swedbank/mobile/business/cards/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/k/b;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    .line 125
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$2;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/g;)Lio/reactivex/j;

    move-result-object p1

    return-object p1

    .line 111
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There cannot be more than one card block confirmation stream at a time"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
