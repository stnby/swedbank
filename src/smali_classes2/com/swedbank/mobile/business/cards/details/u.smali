.class public final Lcom/swedbank/mobile/business/cards/details/u;
.super Ljava/lang/Object;
.source "UnblockCard_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/details/t;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/m;",
            ">;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/u;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/details/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/m;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/details/u;"
        }
    .end annotation

    .line 21
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/u;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/details/u;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/details/t;
    .locals 2

    .line 17
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/t;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/u;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/cards/m;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/cards/details/t;-><init>(Lcom/swedbank/mobile/business/cards/m;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/details/u;->a()Lcom/swedbank/mobile/business/cards/details/t;

    move-result-object v0

    return-object v0
.end method
