.class final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;
.super Ljava/lang/Object;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;->a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;

.field final synthetic b:Lcom/swedbank/mobile/business/cards/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;Lcom/swedbank/mobile/business/cards/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;->b:Lcom/swedbank/mobile/business/cards/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/details/d;)Lio/reactivex/j;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/cards/details/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    sget-object v0, Lcom/swedbank/mobile/business/cards/details/d$c;->a:Lcom/swedbank/mobile/business/cards/details/d$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;->b:Lcom/swedbank/mobile/business/cards/a;

    const-string v1, "card"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/a;->k()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v1

    instance-of v1, v1, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ne v1, v2, :cond_1

    .line 245
    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->g(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object p1

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/a;->o()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "deleteWalletCard(digitizedCardId).ignoreElement()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 246
    :cond_1
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.complete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    :goto_1
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1$1;-><init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/c/a;)Lio/reactivex/j;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "card.deleteWalletCardIfN\u2026 }\n                    })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 154
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/d$a;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/d$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/details/d$a;->a()Ljava/lang/String;

    move-result-object p1

    .line 248
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 155
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/d$b;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 249
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1$a;-><init>(Lcom/swedbank/mobile/business/util/s;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    .line 155
    invoke-static {v0}, Lio/reactivex/j;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.error(it.asErrorSupplier())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$b$1;->a(Lcom/swedbank/mobile/business/cards/details/d;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
