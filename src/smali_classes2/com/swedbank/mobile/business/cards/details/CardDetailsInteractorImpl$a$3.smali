.class final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;
.super Ljava/lang/Object;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/details/n;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/details/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/n;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/details/c$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    sget-object v0, Lcom/swedbank/mobile/business/cards/details/n$c;->a:Lcom/swedbank/mobile/business/cards/details/n$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.empty()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/n$a;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/cards/details/c$a;

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/n$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/details/n$a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/c$a;-><init>(Ljava/lang/String;)V

    .line 244
    invoke-static {v0}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/n$b;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 245
    new-instance v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3$a;-><init>(Lcom/swedbank/mobile/business/util/s;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    .line 132
    invoke-static {v0}, Lio/reactivex/j;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.error(it.asErrorSupplier())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$3;->a(Lcom/swedbank/mobile/business/cards/details/n;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
