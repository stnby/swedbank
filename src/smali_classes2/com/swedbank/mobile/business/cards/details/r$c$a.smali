.class public final Lcom/swedbank/mobile/business/cards/details/r$c$a;
.super Ljava/lang/Object;
.source "Observables.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/r$c;->a(Lkotlin/k;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "TT1;TT2;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/r;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/details/r;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/r$c$a;->a:Lcom/swedbank/mobile/business/cards/details/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)TR;"
        }
    .end annotation

    .line 20
    move-object/from16 v0, p2

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    move-object/from16 v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/cards/a;

    move-object/from16 v2, p0

    iget-object v3, v2, Lcom/swedbank/mobile/business/cards/details/r$c$a;->a:Lcom/swedbank/mobile/business/cards/details/r;

    .line 252
    instance-of v3, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v3, :cond_2

    .line 254
    instance-of v3, v1, Lcom/swedbank/mobile/business/cards/p;

    if-eqz v3, :cond_0

    move-object v4, v1

    check-cast v4, Lcom/swedbank/mobile/business/cards/p;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, Lcom/swedbank/mobile/business/cards/s;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0x3dfff

    const/16 v24, 0x0

    invoke-static/range {v4 .. v24}, Lcom/swedbank/mobile/business/cards/p;->a(Lcom/swedbank/mobile/business/cards/p;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;ZLcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/s;ZZZLcom/swedbank/mobile/business/cards/r;ILjava/lang/Object;)Lcom/swedbank/mobile/business/cards/p;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/cards/a;

    goto :goto_0

    .line 255
    :cond_0
    instance-of v3, v1, Lcom/swedbank/mobile/business/cards/o;

    if-eqz v3, :cond_1

    move-object v4, v1

    check-cast v4, Lcom/swedbank/mobile/business/cards/o;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, Lcom/swedbank/mobile/business/cards/s;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0x3dfff

    const/16 v24, 0x0

    invoke-static/range {v4 .. v24}, Lcom/swedbank/mobile/business/cards/o;->a(Lcom/swedbank/mobile/business/cards/o;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;ZLcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/s;ZZZLcom/swedbank/mobile/business/cards/r;ILjava/lang/Object;)Lcom/swedbank/mobile/business/cards/o;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/cards/a;

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_2
    :goto_0
    return-object v1
.end method
