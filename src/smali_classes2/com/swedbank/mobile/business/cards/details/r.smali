.class public final Lcom/swedbank/mobile/business/cards/details/r;
.super Ljava/lang/Object;
.source "ObserveCardDetails.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/cards/details/q;",
        "Lio/reactivex/o<",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/u;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/r;->a:Lcom/swedbank/mobile/business/cards/u;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/details/r;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/details/r;)Lcom/swedbank/mobile/business/cards/u;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/r;->a:Lcom/swedbank/mobile/business/cards/u;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/details/r;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/details/r;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/details/q;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/details/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "observeCard"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/q$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/r;->a:Lcom/swedbank/mobile/business/cards/u;

    .line 20
    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/cards/details/q$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/details/q$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/u;->g(Ljava/lang/String;)Lio/reactivex/j;

    move-result-object v0

    .line 21
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/r$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/details/r$a;-><init>(Lcom/swedbank/mobile/business/cards/details/q;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object v0

    goto :goto_0

    .line 23
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/q$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/r;->a:Lcom/swedbank/mobile/business/cards/u;

    .line 24
    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/cards/details/q$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/details/q$a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/u;->h(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/r$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/details/r$b;-><init>(Lcom/swedbank/mobile/business/cards/details/q;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 26
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/cards/details/r$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/details/r$c;-><init>(Lcom/swedbank/mobile/business/cards/details/r;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 34
    instance-of v1, p1, Lcom/swedbank/mobile/business/cards/details/q$a;

    if-eqz v1, :cond_1

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/r;->a:Lcom/swedbank/mobile/business/cards/u;

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/q$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/details/q$a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/cards/u;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v0, p1}, Lio/reactivex/o;->g(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 36
    :cond_1
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    const-string v0, "when (observeCard) {\n   \u2026 }.distinctUntilChanged()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 25
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/r;->a(Lcom/swedbank/mobile/business/cards/details/q;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
