.class final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c;
.super Ljava/lang/Object;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->j()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/a;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->l()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 163
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/cards/m;->b(Ljava/lang/String;Z)Lio/reactivex/w;

    move-result-object p1

    .line 166
    sget-object v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c$1;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$c;->a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
