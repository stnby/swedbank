.class final Lcom/swedbank/mobile/business/cards/details/r$c;
.super Ljava/lang/Object;
.source "ObserveCardDetails.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/r;->a(Lcom/swedbank/mobile/business/cards/details/q;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/r;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/r;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/r$c;->a:Lcom/swedbank/mobile/business/cards/details/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 27
    sget-object v1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 28
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/r$c;->a:Lcom/swedbank/mobile/business/cards/details/r;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/details/r;->a(Lcom/swedbank/mobile/business/cards/details/r;)Lcom/swedbank/mobile/business/cards/u;

    move-result-object v1

    const-string v2, "id"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/cards/u;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 30
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/r$c;->a:Lcom/swedbank/mobile/business/cards/details/r;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/details/r;->b(Lcom/swedbank/mobile/business/cards/details/r;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v1

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.just(Absent)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    :goto_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/details/r$c;->a:Lcom/swedbank/mobile/business/cards/details/r;

    .line 51
    check-cast p1, Lio/reactivex/s;

    check-cast v0, Lio/reactivex/s;

    .line 52
    new-instance v2, Lcom/swedbank/mobile/business/cards/details/r$c$a;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/details/r$c$a;-><init>(Lcom/swedbank/mobile/business/cards/details/r;)V

    check-cast v2, Lio/reactivex/c/c;

    .line 51
    invoke-static {p1, v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/r$c;->a(Lkotlin/k;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
