.class public final Lcom/swedbank/mobile/business/cards/details/o;
.super Ljava/lang/Object;
.source "ChangeCardContactlessStatus.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lkotlin/k<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/cards/details/d;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/o;->a:Lcom/swedbank/mobile/business/cards/m;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/k;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/o;->a:Lcom/swedbank/mobile/business/cards/m;

    .line 19
    invoke-virtual {p1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 20
    invoke-virtual {p1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 18
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/cards/m;->a(Ljava/lang/String;Z)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/o;->a(Lkotlin/k;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
