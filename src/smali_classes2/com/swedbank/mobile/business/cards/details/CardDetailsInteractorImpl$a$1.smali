.class final Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;
.super Ljava/lang/Object;
.source "CardDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a(Lcom/swedbank/mobile/business/cards/a;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;

.field final synthetic b:Lcom/swedbank/mobile/business/cards/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;Lcom/swedbank/mobile/business/cards/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;->b:Lcom/swedbank/mobile/business/cards/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/details/c;)Lio/reactivex/j;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/cards/details/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ")",
            "Lio/reactivex/j<",
            "+",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/c$c;

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 119
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;->b:Lcom/swedbank/mobile/business/cards/a;

    const-string v3, "card"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v3

    instance-of v3, v3, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-ne v3, v1, :cond_1

    .line 245
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;->g(Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    invoke-virtual {v0}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "deleteWalletCard(digitizedCardId).ignoreElement()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 246
    :cond_1
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    :goto_1
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    check-cast p1, Lio/reactivex/n;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/n;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "card\n                   \u2026 .andThen(Maybe.just(it))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 122
    :cond_2
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.just(it)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/details/CardDetailsInteractorImpl$a$1;->a(Lcom/swedbank/mobile/business/cards/details/c;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
