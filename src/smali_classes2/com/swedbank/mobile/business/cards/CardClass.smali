.class public final enum Lcom/swedbank/mobile/business/cards/CardClass;
.super Ljava/lang/Enum;
.source "Card.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/CardClass;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/cards/CardClass;

.field public static final enum CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

.field public static final enum DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

.field public static final enum UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;


# instance fields
.field private final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/swedbank/mobile/business/cards/CardClass;

    new-instance v2, Lcom/swedbank/mobile/business/cards/CardClass;

    const-string v3, "CREDIT"

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 145
    invoke-direct {v2, v3, v4, v5}, Lcom/swedbank/mobile/business/cards/CardClass;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    aput-object v2, v1, v4

    new-instance v2, Lcom/swedbank/mobile/business/cards/CardClass;

    const-string v3, "DEBIT"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v5, v4}, Lcom/swedbank/mobile/business/cards/CardClass;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    aput-object v2, v1, v5

    new-instance v2, Lcom/swedbank/mobile/business/cards/CardClass;

    const-string v3, "UNKNOWN"

    invoke-direct {v2, v3, v4, v0}, Lcom/swedbank/mobile/business/cards/CardClass;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/swedbank/mobile/business/cards/CardClass;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardClass;

    aput-object v2, v1, v4

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardClass;->$VALUES:[Lcom/swedbank/mobile/business/cards/CardClass;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/business/cards/CardClass;->id:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardClass;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/CardClass;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/CardClass;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/CardClass;->$VALUES:[Lcom/swedbank/mobile/business/cards/CardClass;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/CardClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/CardClass;

    return-object v0
.end method


# virtual methods
.method public final getId()I
    .locals 1

    .line 144
    iget v0, p0, Lcom/swedbank/mobile/business/cards/CardClass;->id:I

    return v0
.end method
