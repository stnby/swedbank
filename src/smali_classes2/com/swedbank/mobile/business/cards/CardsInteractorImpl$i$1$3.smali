.class public final Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;
.super Ljava/lang/Object;
.source "CardsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1;->a(Ljava/lang/Boolean;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardsEligibleForDigitization"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$c;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$c;-><init>(Z)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;

    goto :goto_0

    .line 102
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;->a(Ljava/util/List;)Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;

    move-result-object p1

    return-object p1
.end method
