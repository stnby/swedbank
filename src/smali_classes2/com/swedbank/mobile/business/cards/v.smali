.class public final Lcom/swedbank/mobile/business/cards/v;
.super Lcom/swedbank/mobile/architect/business/f;
.source "ObserveAllCards.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/f<",
        "Ljava/util/List<",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;>;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final b:Lcom/swedbank/mobile/business/cards/u;

.field private final c:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/f;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/v;->b:Lcom/swedbank/mobile/business/cards/u;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/v;->c:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method


# virtual methods
.method protected a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/v;->b:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->a()Lio/reactivex/o;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/v;->c:Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->h()Lio/reactivex/o;

    move-result-object v1

    .line 60
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    .line 61
    new-instance v2, Lcom/swedbank/mobile/business/cards/v$a;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/cards/v$a;-><init>(Lcom/swedbank/mobile/business/cards/v;)V

    check-cast v2, Lio/reactivex/c/c;

    .line 60
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 22
    sget-object v1, Lcom/swedbank/mobile/business/cards/v$b;->a:Lcom/swedbank/mobile/business/cards/v$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observables\n      .combi\u2026th(DebitsBeforeCredits) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
