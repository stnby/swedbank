.class final Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;
.super Ljava/lang/Object;
.source "CardsListInteractor.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->c(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cards"

    .line 133
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 208
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/cards/a;

    .line 133
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 209
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/cards/a;

    if-eqz v0, :cond_2

    .line 134
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->d(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/cards/list/k;

    move-result-object p1

    .line 135
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;->b:Ljava/lang/String;

    const/4 v2, 0x0

    .line 137
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/a;->k()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 134
    invoke-interface {p1, v1, v2, v0}, Lcom/swedbank/mobile/business/cards/list/k;->a(Ljava/lang/String;ZZ)Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;

    :cond_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 56
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;->a(Ljava/util/List;)V

    return-void
.end method
