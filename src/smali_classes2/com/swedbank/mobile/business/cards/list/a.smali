.class public final Lcom/swedbank/mobile/business/cards/list/a;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "CardsListFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/cards/list/b;",
        "Lcom/swedbank/mobile/business/cards/list/j;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/list/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/list/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/list/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/list/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/cards/list/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/cards/list/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance p2, Lcom/swedbank/mobile/business/cards/list/a$a;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/business/cards/list/a$a;-><init>(Lcom/swedbank/mobile/business/root/c;)V

    check-cast p2, Lkotlin/e/a/a;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/business/cards/list/a;->a(Lkotlin/e/a/a;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 15
    sget-object p2, Lcom/swedbank/mobile/business/cards/list/a$b;->a:Lcom/swedbank/mobile/business/cards/list/a$b;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 16
    sget-object p2, Lcom/swedbank/mobile/business/cards/list/a$c;->a:Lcom/swedbank/mobile/business/cards/list/a$c;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 17
    sget-object p2, Lcom/swedbank/mobile/business/cards/list/a$d;->a:Lcom/swedbank/mobile/business/cards/list/a$d;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/list/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/list/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/list/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
