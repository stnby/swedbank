.class public final Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CardsListInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/details/k;
.implements Lcom/swedbank/mobile/business/cards/list/d;
.implements Lcom/swedbank/mobile/business/cards/list/j;
.implements Lcom/swedbank/mobile/business/cards/order/b;
.implements Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/list/k;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/details/k;",
        "Lcom/swedbank/mobile/business/cards/list/d;",
        "Lcom/swedbank/mobile/business/cards/list/j;",
        "Lcom/swedbank/mobile/business/cards/order/b;",
        "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/general/confirmation/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/general/confirmation/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/customer/l;

.field private final d:Lcom/swedbank/mobile/business/i/d;

.field private final e:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/cards/af;",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/swedbank/mobile/business/cards/m;

.field private final l:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final m:Lcom/swedbank/mobile/business/e/i;

.field private final n:Lcom/swedbank/mobile/business/onboarding/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/f;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/m;Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/onboarding/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "queryAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "startWalletPaymentUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "replenishPaymentTokensUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteWalletCardUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/cards/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/business/onboarding/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/business/i/d;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/cards/af;",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/b/c;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/business/cards/m;",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectedCustomer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pluginManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queryAllCards"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeAllCards"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startWalletPayment"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "replenishPaymentTokens"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteWalletCard"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getJumpToIbankLink"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardsRepository"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingRepository"

    invoke-static {p12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->c:Lcom/swedbank/mobile/business/customer/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->d:Lcom/swedbank/mobile/business/i/d;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/f;

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    iput-object p6, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    iput-object p7, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/b;

    iput-object p8, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j:Lcom/swedbank/mobile/architect/business/b;

    iput-object p9, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->k:Lcom/swedbank/mobile/business/cards/m;

    iput-object p10, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->l:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p11, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->m:Lcom/swedbank/mobile/business/e/i;

    iput-object p12, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->n:Lcom/swedbank/mobile/business/onboarding/h;

    .line 71
    new-instance p1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$m;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$m;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)V

    check-cast p1, Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    .line 75
    new-instance p1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$p;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$p;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)V

    check-cast p1, Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->c:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->n:Lcom/swedbank/mobile/business/onboarding/h;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/cards/list/k;
    .locals 0

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/list/k;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/cards/m;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->k:Lcom/swedbank/mobile/business/cards/m;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/w;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/w;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$h;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow().flatMapMaybe { it\u2026rrorNotification(error) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/k;->c()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/list/k;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/details/l;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/b/c;

    .line 210
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/k;->a()V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->m()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 131
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 132
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 139
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/list/g;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/business/cards/list/g;-><init>(Lkotlin/e/a/b;)V

    move-object p1, v2

    :cond_0
    check-cast p1, Lio/reactivex/c/g;

    .line 132
    invoke-virtual {v0, v1, p1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "observeAllCards()\n      \u2026  }\n        }, LOG_ERROR)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/b;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 145
    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->f()Lio/reactivex/c/g;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "deleteWalletCard(digitiz\u2026er(), LOG_ERROR_CONSUMER)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->m()Lio/reactivex/o;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 158
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "observeAllCards()\n      \u2026etCardClass()))\n        }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance v0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/list/k;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v0, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 216
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public e()Z
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->c:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v0

    return v0
.end method

.method public f(Ljava/lang/String;)Lio/reactivex/j;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/details/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 220
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$f;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$f;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flatMapMaybe { Maybe.just(routing(it)) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public f()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->l:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 91
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->b()Lio/reactivex/o;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$n;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$n;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "walletRepository\n      .\u2026)).toObservable()\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lio/reactivex/j;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 180
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026>()\n            }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public g()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/e/p$c;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->m:Lcom/swedbank/mobile/business/e/i;

    .line 111
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->k()Lio/reactivex/o;

    move-result-object v0

    .line 112
    sget-object v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$j;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$k;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$k;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 114
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 115
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "deviceRepository\n      .\u2026to it }\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public h()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->n:Lcom/swedbank/mobile/business/onboarding/h;

    const/4 v1, 0x1

    .line 127
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/onboarding/h;->a(Z)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lio/reactivex/j;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/list/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 200
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$d;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026   this\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public i()V
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/k;->g()V

    return-void
.end method

.method public j()V
    .locals 4

    .line 171
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j:Lcom/swedbank/mobile/architect/business/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->k:Lcom/swedbank/mobile/business/cards/m;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->c:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/cards/m;->d(Ljava/lang/String;)Lcom/swedbank/mobile/business/e;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 172
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$o;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$o;-><init>(Lcom/swedbank/mobile/business/cards/list/k;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 218
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public final k()Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object v0
.end method

.method public final l()Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object v0
.end method

.method public m()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    .line 97
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$i;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$i;-><init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "observeAllCards.invoke()\u2026edCustomer.customerId } }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->d:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 83
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 84
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/list/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/list/f;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/cards/list/g;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/list/g;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "replenishPaymentTokens()\u2026ibe(NO_ACTION, LOG_ERROR)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public n()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 194
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$g;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$g;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/list/h;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/list/h;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow().flatMapMaybe(Card\u2026OutdatedCardShortcutHelp)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public n_()V
    .locals 1

    .line 149
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/k;->b()V

    return-void
.end method

.method public o_()V
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/k;->b()V

    return-void
.end method

.method public q()V
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/list/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/k;->h()V

    return-void
.end method
