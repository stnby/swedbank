.class final Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;
.super Ljava/lang/Object;
.source "CardsListInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->e(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/w;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    check-cast p1, Ljava/lang/Iterable;

    .line 208
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/a;

    .line 159
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->e(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object p1

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->f(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/e;->a(Lcom/swedbank/mobile/business/cards/a;)Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/cards/m;->a(Lcom/swedbank/mobile/business/cards/CardClass;)Lcom/swedbank/mobile/business/e;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    return-object p1

    .line 209
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$a;->a(Ljava/util/List;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
