.class final Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;
.super Ljava/lang/Object;
.source "CardsListInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;->a(Lcom/swedbank/mobile/business/cards/list/k;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;

.field final synthetic b:Lcom/swedbank/mobile/business/cards/list/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;Lcom/swedbank/mobile/business/cards/list/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;->b:Lcom/swedbank/mobile/business/cards/list/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/j;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    check-cast p1, Ljava/lang/Iterable;

    .line 208
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/cards/a;

    .line 184
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;

    iget-object v2, v2, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 209
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/cards/a;

    if-eqz v0, :cond_2

    .line 185
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;->b:Lcom/swedbank/mobile/business/cards/list/k;

    .line 186
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;

    iget-object v1, v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e;->b:Ljava/lang/String;

    const/4 v2, 0x0

    .line 188
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/a;->k()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 185
    invoke-interface {p1, v1, v2, v0}, Lcom/swedbank/mobile/business/cards/list/k;->a(Ljava/lang/String;ZZ)Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    if-eqz p1, :cond_2

    goto :goto_1

    .line 189
    :cond_2
    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$e$1;->a(Ljava/util/List;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
