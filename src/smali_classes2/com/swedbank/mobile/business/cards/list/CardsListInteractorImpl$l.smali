.class final Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l;
.super Ljava/lang/Object;
.source "CardsListInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->g()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/e/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e/p;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/e/p$c;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    const-string v0, "eligibility"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    check-cast p1, Lcom/swedbank/mobile/business/e/p$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/p$b;->a()Lcom/swedbank/mobile/business/e/p$c;

    move-result-object p1

    .line 117
    sget-object v0, Lcom/swedbank/mobile/business/cards/list/e;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/p$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;

    move-result-object v0

    .line 120
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/h;->d()Lio/reactivex/o;

    move-result-object v0

    .line 121
    sget-object v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l$1;->a:Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l$1;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l$2;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l$2;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 118
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/list/CardsListInteractorImpl$l;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
