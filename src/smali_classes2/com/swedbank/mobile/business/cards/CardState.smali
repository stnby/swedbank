.class public final enum Lcom/swedbank/mobile/business/cards/CardState;
.super Ljava/lang/Enum;
.source "Card.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/CardState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/cards/CardState;

.field public static final enum ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

.field public static final enum BLOCKED:Lcom/swedbank/mobile/business/cards/CardState;

.field public static final enum NOT_ISSUED:Lcom/swedbank/mobile/business/cards/CardState;

.field public static final enum UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/CardState;

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardState;

    const-string v2, "ACTIVE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardState;

    const-string v2, "BLOCKED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardState;->BLOCKED:Lcom/swedbank/mobile/business/cards/CardState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardState;

    const-string v2, "NOT_ISSUED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardState;->NOT_ISSUED:Lcom/swedbank/mobile/business/cards/CardState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardState;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/cards/CardState;->$VALUES:[Lcom/swedbank/mobile/business/cards/CardState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardState;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/CardState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/CardState;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/CardState;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/CardState;->$VALUES:[Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/CardState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/CardState;

    return-object v0
.end method
