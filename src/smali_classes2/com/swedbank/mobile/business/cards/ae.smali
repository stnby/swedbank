.class public final Lcom/swedbank/mobile/business/cards/ae;
.super Ljava/lang/Object;
.source "StartWalletPayment_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/ad;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;>;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/ae;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/business/cards/ae;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/cards/ae;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/ae;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/ad;
    .locals 2

    .line 20
    new-instance v0, Lcom/swedbank/mobile/business/cards/ad;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/ae;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/g;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/cards/ad;-><init>(Lcom/swedbank/mobile/architect/business/g;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/ae;->a()Lcom/swedbank/mobile/business/cards/ad;

    move-result-object v0

    return-object v0
.end method
