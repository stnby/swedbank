.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletPaymentTapInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/d;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

.field private final d:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "digitized_card_id_selected_for_tap"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "observeCardDetailsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_start_with_tap_retry"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/o;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_tap_retry_signal_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_tap_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectedCard"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeCardDetails"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startWithTapState"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tapStateStream"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->d:Lio/reactivex/o;

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->d:Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 43
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026\n      tapStateStream\n  )"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/b;

    new-instance v1, Lcom/swedbank/mobile/business/cards/details/q$b;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/details/q$b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;->b()V

    return-void
.end method
