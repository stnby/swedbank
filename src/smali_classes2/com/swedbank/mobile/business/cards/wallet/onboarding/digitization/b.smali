.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;
.super Ljava/lang/Object;
.source "WalletOnboardingDigitizationInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;>;>;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->a:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->b:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->c:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->d:Ljavax/inject/Provider;

    .line 35
    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->e:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;
    .locals 7

    .line 40
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/List;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/architect/business/f;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;-><init>(ZLjava/util/List;Lcom/swedbank/mobile/architect/business/f;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V

    return-object v6
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/b;->a()Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
