.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;
.super Ljava/lang/Object;
.source "WalletOnboardingInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->a:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->b:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->c:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->d:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->e:Ljavax/inject/Provider;

    .line 37
    iput-object p6, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;"
        }
    .end annotation

    .line 52
    new-instance v7, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;
    .locals 8

    .line 42
    new-instance v7, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/onboarding/h;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/architect/business/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/architect/business/g;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;-><init>(Lcom/swedbank/mobile/business/onboarding/h;Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;ZLcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;)V

    return-object v7
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/e;->a()Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    move-result-object v0

    return-object v0
.end method
