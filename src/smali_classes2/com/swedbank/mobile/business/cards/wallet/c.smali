.class public final Lcom/swedbank/mobile/business/cards/wallet/c;
.super Ljava/lang/Object;
.source "CleanWalletCards.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/u;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/c;->a:Lcom/swedbank/mobile/business/cards/u;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/c;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/c;->a:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->a()Lio/reactivex/o;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/c;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->h()Lio/reactivex/o;

    move-result-object v1

    .line 45
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    .line 46
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/c$a;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/cards/wallet/c$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/c;)V

    check-cast v2, Lio/reactivex/c/c;

    .line 45
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 23
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/c$b;->a:Lcom/swedbank/mobile/business/cards/wallet/c$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/c$c;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/c;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/c$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/d;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->f(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Observables\n      .combi\u2026\n      .onErrorComplete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/c;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
