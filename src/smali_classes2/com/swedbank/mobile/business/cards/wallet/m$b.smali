.class public final synthetic Lcom/swedbank/mobile/business/cards/wallet/m$b;
.super Lkotlin/e/b/i;
.source "DigitizeCards.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/wallet/a;",
        "Lio/reactivex/w<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/a;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/a;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/m$b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 70
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->a(Lcom/swedbank/mobile/business/cards/wallet/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$b;->a(Lcom/swedbank/mobile/business/cards/wallet/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "digitizeCard"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "digitizeCard(Lcom/swedbank/mobile/business/cards/wallet/CardDigitizationParams;)Lio/reactivex/Single;"

    return-object v0
.end method
