.class public final synthetic Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;
.super Lkotlin/e/b/i;
.source "WalletOnboardingInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;->a(Lkotlin/k;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;->a()V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$1;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "toCancelConfirmation"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "toCancelConfirmation()V"

    return-object v0
.end method
