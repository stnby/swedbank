.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;
.super Ljava/lang/Object;
.source "WalletPaymentAuthenticationInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;->a:Ljavax/inject/Provider;

    .line 17
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;
    .locals 3

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/ad;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/b;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
