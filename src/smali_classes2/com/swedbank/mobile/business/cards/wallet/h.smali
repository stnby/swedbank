.class public final Lcom/swedbank/mobile/business/cards/wallet/h;
.super Ljava/lang/Object;
.source "DeleteWallet.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final b:Lcom/swedbank/mobile/business/cards/u;

.field private final c:Lcom/swedbank/mobile/business/onboarding/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/onboarding/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/onboarding/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardsRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->b:Lcom/swedbank/mobile/business/cards/u;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->c:Lcom/swedbank/mobile/business/onboarding/h;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 16
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->p()Lio/reactivex/b;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->b:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/u;->b()Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->c:Lcom/swedbank/mobile/business/onboarding/h;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/onboarding/h;->a(Z)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/h;->c:Lcom/swedbank/mobile/business/onboarding/h;

    sget-object v2, Lcom/swedbank/mobile/business/onboarding/i;->b:Lcom/swedbank/mobile/business/onboarding/i;

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/onboarding/h;->a(Lcom/swedbank/mobile/business/onboarding/i;)Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "walletRepository\n      .\u2026nboardingState.CANCELED))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/h;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
