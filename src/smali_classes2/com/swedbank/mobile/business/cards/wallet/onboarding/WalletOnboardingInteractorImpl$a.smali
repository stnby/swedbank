.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;
.super Ljava/lang/Object;
.source "WalletOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/onboarding/i;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/onboarding/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;Lcom/swedbank/mobile/business/onboarding/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->b:Lcom/swedbank/mobile/business/onboarding/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/onboarding/i;)Lio/reactivex/f;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/onboarding/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    sget-object v0, Lcom/swedbank/mobile/business/onboarding/i;->c:Lcom/swedbank/mobile/business/onboarding/i;

    if-eq p1, v0, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;

    move-result-object p1

    .line 180
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->b:Lcom/swedbank/mobile/business/onboarding/i;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/onboarding/h;->a(Lcom/swedbank/mobile/business/onboarding/i;)Lio/reactivex/b;

    move-result-object p1

    .line 181
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->b:Lcom/swedbank/mobile/business/onboarding/i;

    sget-object v1, Lcom/swedbank/mobile/business/onboarding/i;->b:Lcom/swedbank/mobile/business/onboarding/i;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "andThen(deleteAllWalletCards())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    :cond_1
    check-cast p1, Lio/reactivex/f;

    goto :goto_1

    .line 182
    :cond_2
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/swedbank/mobile/business/onboarding/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;->a(Lcom/swedbank/mobile/business/onboarding/i;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
