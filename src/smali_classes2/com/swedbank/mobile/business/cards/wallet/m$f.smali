.class final Lcom/swedbank/mobile/business/cards/wallet/m$f;
.super Ljava/lang/Object;
.source "DigitizeCards.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/m;->a(Ljava/util/List;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/m;

.field final synthetic b:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/m;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->a:Lcom/swedbank/mobile/business/cards/wallet/m;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/w;
    .locals 4
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;+",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    .line 36
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_1

    .line 37
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_0

    .line 38
    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "mobileAgreementIdOptional.value"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    .line 39
    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 40
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->a:Lcom/swedbank/mobile/business/cards/wallet/m;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/m;->a(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v1

    .line 41
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->b:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/m;->a(Ljava/util/List;Z)Lio/reactivex/b;

    move-result-object v1

    .line 42
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->b:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lio/reactivex/i/c;->a(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    invoke-virtual {v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 43
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;-><init>(Lcom/swedbank/mobile/business/cards/wallet/m$f;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->f(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lio/reactivex/o;->p()Lio/reactivex/w;

    move-result-object p1

    const-string v0, "cardsRepository\n        \u2026}\n              .toList()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->a:Lcom/swedbank/mobile/business/cards/wallet/m;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/m;->a(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/m;->a(Ljava/util/List;Z)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lio/reactivex/b;)Lio/reactivex/w;

    move-result-object p1

    return-object p1

    .line 37
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing payment app instance id - cannot digitize cards without it"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 36
    :cond_1
    new-instance p1, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;

    const-string v0, "Missing mobile agreement id - cannot digitize cards without it"

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/util/RequirementNotSatisfiedException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$f;->a(Lkotlin/k;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
