.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/l;
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/m;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private volatile b:Lcom/swedbank/mobile/business/cards/wallet/payment/n;

.field private c:Z

.field private final d:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final e:Lcom/swedbank/mobile/business/e/b;

.field private final f:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

.field private final g:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/cards/wallet/payment/k;Lcom/swedbank/mobile/business/util/e;Z)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/cards/wallet/payment/k;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_started_from_background"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/k;",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentInput"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->e:Lcom/swedbank/mobile/business/e/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->g:Lcom/swedbank/mobile/business/util/e;

    iput-boolean p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->h:Z

    .line 53
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->g:Lcom/swedbank/mobile/business/util/e;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/h;->a(Lcom/swedbank/mobile/business/util/e;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/n;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lkotlin/e/a/a;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 92
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a(Lkotlin/e/a/a;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Z)V
    .locals 0

    .line 45
    iput-boolean p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c:Z

    return-void
.end method

.method private final a(Lkotlin/e/a/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .line 93
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    .line 95
    iget-boolean v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->h:Z

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->b(Lkotlin/e/a/a;)V

    goto :goto_0

    .line 96
    :cond_0
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Lkotlin/e/a/a;)V

    :goto_0
    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/m;
    .locals 0

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    return-object p0
.end method

.method private final b(Lcom/swedbank/mobile/business/util/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)V"
        }
    .end annotation

    .line 102
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lcom/swedbank/mobile/business/util/e;)V

    .line 118
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->e:Lcom/swedbank/mobile/business/e/b;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    .line 120
    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$k;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$k;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;)V

    check-cast p1, Lkotlin/e/a/a;

    invoke-direct {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a(Lkotlin/e/a/a;)V

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a()V

    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 123
    invoke-static {p0, v0, p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lkotlin/e/a/a;ILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Z
    .locals 0

    .line 45
    iget-boolean p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c:Z

    return p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->e:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/k;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Z
    .locals 0

    .line 45
    iget-boolean p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->h:Z

    return p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    .line 221
    iget-boolean v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->h:Z

    .line 219
    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/k;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;Z)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    .line 76
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 295
    invoke-static {v2, v0}, Lcom/swedbank/mobile/business/util/k;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v1

    if-nez v3, :cond_2

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 296
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 82
    :goto_1
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->e:Lcom/swedbank/mobile/business/e/b;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v0, :cond_4

    .line 83
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/h;->a(Lcom/swedbank/mobile/business/util/e;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 86
    iput-boolean v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->h:Z

    .line 87
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->b()V

    .line 88
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_2

    .line 84
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "New input to wallet payment can only be for selected card.\nSelected card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nNew input card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/h;->a(Lcom/swedbank/mobile/business/util/e;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 83
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    :goto_2
    return-void

    .line 74
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public a(Z)V
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Z)V

    if-nez p1, :cond_0

    .line 177
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/k;->a()V

    goto :goto_0

    .line 178
    :cond_0
    iget-boolean p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c:Z

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 179
    iput-boolean p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c:Z

    .line 180
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->b(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public b()V
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/k;->a()V

    return-void
.end method

.method protected i_()V
    .locals 1

    .line 67
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->h:Z

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a()V

    :cond_0
    return-void
.end method

.method protected m_()V
    .locals 8

    .line 240
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction {\u2026_ACTION, LOG_ERROR)\n    }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 252
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->e(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/e/b;

    move-result-object v0

    .line 259
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->a()Lio/reactivex/o;

    move-result-object v0

    .line 258
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 257
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$e;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 256
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 255
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "activityLifecycleMonitor\u2026e()\n          }\n        }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    move-object v3, v0

    check-cast v3, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lio/reactivex/i/f;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 260
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 263
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 270
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->k()Lio/reactivex/o;

    move-result-object v0

    .line 269
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "walletRepository\n       \u2026eMonitor.isAppRunning() }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v3, 0x0

    const/4 v6, 0x3

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 271
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 274
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 281
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->f(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    .line 280
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$l;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$l;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 279
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$m;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$m;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/j;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/j;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "walletRepository\n       \u2026delegateTransactionEvent)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 285
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 289
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->c()Lio/reactivex/o;

    move-result-object v0

    .line 288
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "walletRepository\n       \u2026alError.NotHandledError }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 290
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->g:Lcom/swedbank/mobile/business/util/e;

    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method
