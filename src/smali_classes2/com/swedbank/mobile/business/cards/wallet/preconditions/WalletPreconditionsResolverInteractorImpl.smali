.class public final Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletPreconditionsResolverInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
.implements Lcom/swedbank/mobile/business/general/confirmation/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;"
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Ljava/util/List;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_not_satisfied_preconditions"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteAllWalletCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notSatisfiedPreconditions"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteAllWalletCards"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->d:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    .line 34
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->d:Ljava/util/List;

    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/ab;->b:Lcom/swedbank/mobile/business/cards/wallet/ab;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->a:Z

    .line 42
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->d:Ljava/util/List;

    .line 43
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    if-gt p2, p3, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-ne p2, p3, :cond_0

    invoke-static {p1}, Lkotlin/a/h;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/ab;

    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/ab;->a:Lcom/swedbank/mobile/business/cards/wallet/ab;

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 42
    :cond_1
    :goto_0
    iput-boolean p3, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->b:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method


# virtual methods
.method public b()V
    .locals 4

    .line 67
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->b:Z

    if-eqz v0, :cond_2

    .line 86
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 87
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/preconditions/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/b;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/preconditions/c;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/c;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "deleteAllWalletCards()\n \u2026ibe(NO_ACTION, LOG_ERROR)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;->a()V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 47
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 48
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->a:Z

    if-eqz v0, :cond_2

    .line 84
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 85
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/preconditions/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/b;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/preconditions/c;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/c;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "deleteAllWalletCards()\n \u2026ibe(NO_ACTION, LOG_ERROR)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;->b()V

    goto :goto_0

    .line 52
    :cond_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;

    .line 53
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->d:Ljava/util/List;

    .line 54
    iget-boolean v2, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->b:Z

    xor-int/lit8 v2, v2, 0x1

    .line 52
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;->a(Ljava/util/List;Z)V

    :goto_0
    return-void
.end method

.method public n_()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;->a()V

    return-void
.end method

.method public o_()V
    .locals 4

    .line 88
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 89
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/preconditions/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/b;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/preconditions/c;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/c;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "deleteAllWalletCards()\n \u2026ibe(NO_ACTION, LOG_ERROR)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;->a()V

    return-void
.end method

.method public q_()V
    .locals 1

    .line 59
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->a:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;->a()V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/WalletPreconditionsResolverInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;->a()V

    :goto_0
    return-void
.end method
