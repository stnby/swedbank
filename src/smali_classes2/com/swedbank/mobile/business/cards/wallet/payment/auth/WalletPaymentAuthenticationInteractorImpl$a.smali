.class final Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a;
.super Ljava/lang/Object;
.source "WalletPaymentAuthenticationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authenticated"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->j()Lio/reactivex/b;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a$1;-><init>(Ljava/lang/Boolean;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    .line 36
    :cond_0
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
