.class public interface abstract Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;
.super Ljava/lang/Object;
.source "WalletPaymentBackgroundRouter.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/e;


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/w;
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(Lcom/swedbank/mobile/business/cards/wallet/w;)V
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/swedbank/mobile/business/util/e;Z)V
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract b()V
.end method
