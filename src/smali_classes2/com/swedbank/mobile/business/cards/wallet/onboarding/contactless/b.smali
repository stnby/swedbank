.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;
.super Ljava/lang/Object;
.source "WalletOnboardingContactlessInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->a:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->b:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;
    .locals 4

    .line 31
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/util/l;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/b;

    iget-object v3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;-><init>(Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/b;->a()Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;

    move-result-object v0

    return-object v0
.end method
