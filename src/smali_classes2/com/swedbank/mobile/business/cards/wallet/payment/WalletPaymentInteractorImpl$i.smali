.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;
.super Lkotlin/e/b/k;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/wallet/x;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/x;)V
    .locals 9

    .line 229
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    move-result-object v0

    .line 230
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    .line 232
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/x;->a()Lcom/swedbank/mobile/business/cards/wallet/w;

    move-result-object p1

    .line 241
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_0

    .line 242
    :cond_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    if-eqz v1, :cond_1

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/w$a;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_0

    .line 243
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    goto :goto_0

    .line 244
    :cond_2
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_f

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    .line 247
    :goto_0
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    :goto_1
    move-object v5, p1

    goto/16 :goto_3

    .line 248
    :cond_3
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 249
    :cond_4
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 250
    :cond_5
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 251
    :cond_6
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 252
    :cond_7
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 253
    :cond_8
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 254
    :cond_9
    instance-of v1, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v1, :cond_a

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 255
    :cond_a
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 256
    :cond_b
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    goto :goto_2

    .line 257
    :cond_c
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    goto :goto_2

    .line 258
    :cond_d
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_e

    :goto_2
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    :goto_3
    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    .line 229
    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;IILkotlin/e/b/g;)V

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    return-void

    .line 258
    :cond_e
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 244
    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$i;->a(Lcom/swedbank/mobile/business/cards/wallet/x;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
