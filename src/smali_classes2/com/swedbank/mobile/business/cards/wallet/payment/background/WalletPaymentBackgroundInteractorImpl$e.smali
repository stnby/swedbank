.class final Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;
.super Ljava/lang/Object;
.source "WalletPaymentBackgroundInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/c;)Lcom/swedbank/mobile/business/cards/wallet/x;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/c;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    move-result-object p1

    .line 60
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object p1

    .line 187
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_0

    .line 188
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;->b()Lcom/swedbank/mobile/business/e/l;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/w$a;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_0

    .line 189
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_0

    .line 190
    :cond_2
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    goto :goto_0

    .line 61
    :cond_3
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/w;

    :goto_0
    const/4 v0, 0x1

    .line 58
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-direct {v1, p1, v0}, Lcom/swedbank/mobile/business/cards/wallet/x;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;Z)V

    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/c;)Lcom/swedbank/mobile/business/cards/wallet/x;

    move-result-object p1

    return-object p1
.end method
