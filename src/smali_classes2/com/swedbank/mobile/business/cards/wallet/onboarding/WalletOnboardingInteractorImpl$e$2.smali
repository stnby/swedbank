.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;
.super Ljava/lang/Object;
.source "WalletOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;->a(Lkotlin/k;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;

.field final synthetic b:I

.field final synthetic c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;ILcom/swedbank/mobile/business/cards/wallet/onboarding/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;

    iput p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;->b:I

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;->c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "stepCompleted"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 164
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;

    iget-object p1, p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->d(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/b/c/c;

    move-result-object p1

    iget v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 165
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 167
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2$1;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;)V

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$e$2;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
