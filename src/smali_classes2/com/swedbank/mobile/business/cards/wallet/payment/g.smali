.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/g;
.super Ljava/lang/Object;
.source "WalletPaymentInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/k;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->a:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->b:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->c:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->d:Ljavax/inject/Provider;

    .line 30
    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->e:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/g;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/g;"
        }
    .end annotation

    .line 44
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/payment/g;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/g;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
    .locals 7

    .line 35
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/ad;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/e/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/util/e;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/cards/wallet/payment/k;Lcom/swedbank/mobile/business/util/e;Z)V

    return-object v6
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/g;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    move-result-object v0

    return-object v0
.end method
