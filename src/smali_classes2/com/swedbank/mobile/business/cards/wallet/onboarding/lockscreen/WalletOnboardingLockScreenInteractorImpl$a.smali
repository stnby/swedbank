.class final Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;
.super Ljava/lang/Object;
.source "WalletOnboardingLockScreenInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;->a()Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;->b()Lio/reactivex/w;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
