.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingDefaultPayAppInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/a;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/c;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/a;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final synthetic b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->m()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 33
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->n()Lio/reactivex/w;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 43
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c()V
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->k()V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 25
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;->a()Lio/reactivex/w;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/defaultpay/WalletOnboardingDefaultPayAppInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 41
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
