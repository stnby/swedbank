.class public final Lcom/swedbank/mobile/business/cards/wallet/g;
.super Ljava/lang/Object;
.source "DeleteAllWalletCards_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/g;->a:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/g;->b:Ljavax/inject/Provider;

    .line 21
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/g;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/wallet/g;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/g;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/cards/wallet/g;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/f;
    .locals 4

    .line 26
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/f;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/g;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/ad;

    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/g;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/u;

    iget-object v3, p0, Lcom/swedbank/mobile/business/cards/wallet/g;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/onboarding/h;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/f;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/onboarding/h;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/g;->a()Lcom/swedbank/mobile/business/cards/wallet/f;

    move-result-object v0

    return-object v0
.end method
