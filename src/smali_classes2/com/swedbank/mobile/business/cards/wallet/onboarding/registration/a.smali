.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;
.super Ljava/lang/Object;
.source "RegisterWallet.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final b:Lcom/swedbank/mobile/business/push/h;

.field private final c:Lcom/swedbank/mobile/business/c/a;

.field private final d:Lcom/swedbank/mobile/business/e/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/business/e/i;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->b:Lcom/swedbank/mobile/business/push/h;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->c:Lcom/swedbank/mobile/business/c/a;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->d:Lcom/swedbank/mobile/business/e/i;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/push/h;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->b:Lcom/swedbank/mobile/business/push/h;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->c:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->d:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->d()Lio/reactivex/w;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "walletRepository\n      .\u2026  }\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
