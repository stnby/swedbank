.class final Lcom/swedbank/mobile/business/cards/wallet/y$a$1;
.super Ljava/lang/Object;
.source "WalletGeneralErrorNotificationFlow.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/y$a;->a()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/y$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/y$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/y$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/y$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/notauth/d;",
            ">;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authenticationState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/notauth/d;

    .line 29
    invoke-interface {p1}, Lcom/swedbank/mobile/business/authentication/notauth/d;->a()Lio/reactivex/j;

    move-result-object p1

    .line 30
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/y$a$1$c;->a:Lcom/swedbank/mobile/business/cards/wallet/y$a$1$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    .line 31
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/y$a$1$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/y$a$1$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/y$a$1;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    goto :goto_0

    .line 43
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/authenticated/c;

    .line 23
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/y$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/y$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/cards/wallet/y$a;->a:Lcom/swedbank/mobile/business/cards/wallet/y;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/y;->a(Lcom/swedbank/mobile/business/cards/wallet/y;)Lcom/swedbank/mobile/business/cards/list/a;

    move-result-object p1

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/y$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/y$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/y$a;->b:Lcom/swedbank/mobile/business/root/c;

    check-cast v0, Lcom/swedbank/mobile/architect/business/a/e;

    sget-object v1, Lcom/swedbank/mobile/business/cards/list/b;->a:Lcom/swedbank/mobile/business/cards/list/b;

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/swedbank/mobile/business/cards/list/a;->b(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lio/reactivex/j;

    move-result-object p1

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/y$a$1$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/y$a$1$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/y$a$1;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->a(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/y$a$1;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
