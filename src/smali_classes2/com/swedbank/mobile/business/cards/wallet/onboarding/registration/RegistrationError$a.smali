.class public final enum Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;
.super Ljava/lang/Enum;
.source "RegisterWallet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

.field public static final enum b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

.field public static final enum c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

.field private static final synthetic d:[Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    const-string v2, "NO_NETWORK"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    const-string v2, "TRANSFER_PROBLEM"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    const-string v2, "SERVER_ERROR"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->c:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->d:[Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->d:[Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    return-object v0
.end method
