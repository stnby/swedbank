.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;
.super Ljava/lang/Object;
.source "WalletPaymentBackgroundInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/af;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/af;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->a:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->b:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->c:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->d:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->e:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->f:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->g:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->h:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->i:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/af;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;"
        }
    .end annotation

    .line 73
    new-instance v10, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;
    .locals 11

    .line 60
    new-instance v10, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/ad;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/cards/u;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/e/i;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/e/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/cards/wallet/af;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/architect/business/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/architect/business/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/architect/business/f;

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/cards/wallet/af;Lcom/swedbank/mobile/business/cards/wallet/payment/a;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/f;)V

    return-object v10
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/a;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;

    move-result-object v0

    return-object v0
.end method
