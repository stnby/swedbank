.class final Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;
.super Ljava/lang/Object;
.source "WalletPaymentBackgroundInteractor.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "General error handling stream must not emit errors"

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;->a(Ljava/lang/Throwable;)V

    return-void
.end method
