.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
.implements Lcom/swedbank/mobile/business/general/confirmation/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final c:Lcom/swedbank/mobile/business/onboarding/h;

.field private final d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;

.field private final e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private final f:Z

.field private final g:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/h;Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;ZLcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "only_enable_preconditions"
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isWalletPreconditionsSatisfiedUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteAllWalletCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Z",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingPluginRegistry"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isWalletPreconditionsSatisfied"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteAllWalletCards"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-boolean p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->f:Z

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/g;

    iput-object p6, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    .line 73
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Change>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->a:Lcom/b/c/c;

    .line 74
    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/h;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;
    .locals 0

    .line 64
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Z
    .locals 0

    .line 64
    iget-boolean p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->f:Z

    return p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-object p0
.end method


# virtual methods
.method public b()V
    .locals 2

    .line 127
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;->b()V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->a:Lcom/b/c/c;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->a:Lcom/b/c/c;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public j()V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->a:Lcom/b/c/c;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public k()V
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;->a()V

    return-void
.end method

.method public l()V
    .locals 4

    .line 122
    sget-object v0, Lcom/swedbank/mobile/business/onboarding/i;->b:Lcom/swedbank/mobile/business/onboarding/i;

    .line 214
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;

    move-result-object v1

    .line 218
    invoke-interface {v1}, Lcom/swedbank/mobile/business/onboarding/h;->a()Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 217
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 216
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;

    invoke-direct {v2, p0, v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;Lcom/swedbank/mobile/business/onboarding/i;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v1

    const-string v2, "onboardingRepository\n   \u2026e()\n          }\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x3

    .line 215
    invoke-static {v1, v2, v2, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    .line 219
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->h(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    move-result-object v1

    .line 220
    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/d;->b:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/onboarding/i;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 223
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot change state to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 222
    :pswitch_1
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;->n_()V

    goto :goto_0

    .line 221
    :pswitch_2
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;->o_()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected m_()V
    .locals 4

    .line 77
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 78
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 79
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    .line 80
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/h;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/onboarding/h;->a()Lio/reactivex/o;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v2

    const-string v3, "onboardingRepository.obs\u2026ingState().firstOrError()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/aa;

    .line 78
    invoke-virtual {v0, v1, v2}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 90
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$g;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 91
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/y;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->c(Lio/reactivex/y;)Lio/reactivex/y;

    move-result-object v0

    const-string v1, "Singles.zip(\n        isW\u2026elOnboarding()\n        })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/b/c;

    .line 196
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public q_()V
    .locals 4

    .line 124
    sget-object v0, Lcom/swedbank/mobile/business/onboarding/i;->b:Lcom/swedbank/mobile/business/onboarding/i;

    .line 228
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;

    move-result-object v1

    .line 232
    invoke-interface {v1}, Lcom/swedbank/mobile/business/onboarding/h;->a()Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 231
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 230
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;

    invoke-direct {v2, p0, v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;Lcom/swedbank/mobile/business/onboarding/i;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v1

    const-string v2, "onboardingRepository\n   \u2026e()\n          }\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x3

    .line 229
    invoke-static {v1, v2, v2, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    .line 233
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->h(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    move-result-object v1

    .line 234
    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/d;->b:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/onboarding/i;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 237
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot change state to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 236
    :pswitch_1
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;->n_()V

    goto :goto_0

    .line 235
    :pswitch_2
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;->o_()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
