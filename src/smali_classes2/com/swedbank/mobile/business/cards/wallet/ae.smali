.class public final Lcom/swedbank/mobile/business/cards/wallet/ae;
.super Ljava/lang/Object;
.source "WalletRepository.kt"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/v;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x1c

    .line 56
    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/wallet/v;

    .line 57
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "EUR"

    .line 58
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"EUR\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x3a98

    .line 57
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 60
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "USD"

    .line 61
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"USD\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x4650

    .line 60
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 63
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "GBP"

    .line 64
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"GBP\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x32c8

    .line 63
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 66
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "SEK"

    .line 67
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"SEK\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x249f0

    .line 66
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 69
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "AED"

    .line 70
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"AED\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x109a0

    .line 69
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 72
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "AUD"

    .line 73
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"AUD\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x59d8

    .line 72
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 75
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "BGN"

    .line 76
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"BGN\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x7148

    .line 75
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 78
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "CAD"

    .line 79
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"CAD\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x5dc0

    .line 78
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 81
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "CHF"

    .line 82
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v4, "Currency.getInstance(\"CHF\")"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v4, 0x4268

    .line 81
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 84
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "CNY"

    .line 85
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v4, "Currency.getInstance(\"CNY\")"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x186a0

    .line 84
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 87
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "CZK"

    .line 88
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"CZK\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x55730

    .line 87
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 90
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "DKK"

    .line 91
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"DKK\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 93
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "HKD"

    .line 94
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"HKD\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0xc

    aput-object v1, v0, v2

    .line 96
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "HRK"

    .line 97
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"HRK\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0xd

    aput-object v1, v0, v2

    .line 99
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "HUF"

    .line 100
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v4, "Currency.getInstance(\"HUF\")"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x3d0900

    .line 99
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0xe

    aput-object v1, v0, v2

    .line 102
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "ILS"

    .line 103
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v4, "Currency.getInstance(\"ILS\")"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0xfa00

    .line 102
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0xf

    aput-object v1, v0, v2

    .line 105
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "INR"

    .line 106
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v4, "Currency.getInstance(\"INR\")"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0xf4240

    .line 105
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x10

    aput-object v1, v0, v2

    .line 108
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "JPY"

    .line 109
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"JPY\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x1cfde0

    .line 108
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x11

    aput-object v1, v0, v2

    .line 111
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "MXN"

    .line 112
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"MXN\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x493e0

    .line 111
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x12

    aput-object v1, v0, v2

    .line 114
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "NOK"

    .line 115
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"NOK\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x222e0

    .line 114
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x13

    aput-object v1, v0, v2

    .line 117
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "NZD"

    .line 118
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"NZD\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v5, 0x61a8

    .line 117
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x14

    aput-object v1, v0, v2

    .line 120
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "PLN"

    .line 121
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"PLN\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0xf230

    .line 120
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x15

    aput-object v1, v0, v2

    .line 123
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "RON"

    .line 124
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"RON\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x10d88

    .line 123
    invoke-direct {v1, v2, v5}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x16

    aput-object v1, v0, v2

    .line 126
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "RUB"

    .line 127
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v5, "Currency.getInstance(\"RUB\")"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-direct {v1, v2, v4}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x17

    aput-object v1, v0, v2

    .line 129
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "SGD"

    .line 130
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v4, "Currency.getInstance(\"SGD\")"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x18

    aput-object v1, v0, v2

    .line 132
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "THB"

    .line 133
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"THB\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7a120

    .line 132
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x19

    aput-object v1, v0, v2

    .line 135
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "TRY"

    .line 136
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"TRY\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x11170

    .line 135
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    .line 138
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/v;

    const-string v2, "ZAR"

    .line 139
    invoke-static {v2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v2

    const-string v3, "Currency.getInstance(\"ZAR\")"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x30d40

    .line 138
    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/v;-><init>(Ljava/util/Currency;I)V

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    .line 56
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/ae;->a:Ljava/util/List;

    return-void
.end method

.method public static final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/v;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/ae;->a:Ljava/util/List;

    return-object v0
.end method
