.class public final Lcom/swedbank/mobile/business/cards/wallet/r;
.super Ljava/lang/Object;
.source "IsWalletPreconditionsSatisfied.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/cards/wallet/ac;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/cards/wallet/ad;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/r;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/r;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/r;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->g()Lio/reactivex/w;

    move-result-object v0

    check-cast v0, Lio/reactivex/aa;

    .line 29
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/r;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->f()Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    .line 30
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/r;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/cards/wallet/ad;->m()Lio/reactivex/w;

    move-result-object v2

    check-cast v2, Lio/reactivex/aa;

    .line 42
    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/r$a;

    invoke-direct {v3}, Lcom/swedbank/mobile/business/cards/wallet/r$a;-><init>()V

    check-cast v3, Lio/reactivex/c/i;

    invoke-static {v0, v1, v2, v3}, Lio/reactivex/w;->a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/i;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.zip(s1, s2, s3, F\u2026per.invoke(t1, t2, t3) })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Singles.zip(\n      devic\u2026scribeOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/r;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
