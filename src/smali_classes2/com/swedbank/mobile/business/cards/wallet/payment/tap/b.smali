.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;
.super Ljava/lang/Object;
.source "WalletPaymentTapInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->a:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->b:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->c:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->d:Ljavax/inject/Provider;

    .line 31
    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->e:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            ">;)",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;"
        }
    .end annotation

    .line 44
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;
    .locals 7

    .line 36
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/reactivex/o;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;Lio/reactivex/o;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)V

    return-object v6
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/b;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/tap/WalletPaymentTapInteractorImpl;

    move-result-object v0

    return-object v0
.end method
