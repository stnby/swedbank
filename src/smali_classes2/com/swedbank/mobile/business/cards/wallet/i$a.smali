.class final Lcom/swedbank/mobile/business/cards/wallet/i$a;
.super Ljava/lang/Object;
.source "DeleteWalletCard.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/i;->a(Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/i;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/i;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/i$a;->a:Lcom/swedbank/mobile/business/cards/wallet/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/i$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardDeleted"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/i$a;->a:Lcom/swedbank/mobile/business/cards/wallet/i;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/i;->a(Lcom/swedbank/mobile/business/cards/wallet/i;)Lcom/swedbank/mobile/business/cards/u;

    move-result-object p1

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/i$a;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/cards/u;->i(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    .line 23
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/i$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/i$a$1;

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 24
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/i$a;->a(Ljava/lang/Boolean;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
