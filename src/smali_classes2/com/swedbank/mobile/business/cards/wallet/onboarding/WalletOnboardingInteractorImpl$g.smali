.class final synthetic Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$g;
.super Lkotlin/e/b/i;
.source "WalletOnboardingInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/onboarding/i;",
        "Ljava/util/List<",
        "+",
        "Lkotlin/e/a/a<",
        "+",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
        ">;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/onboarding/i;)Ljava/util/List;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/onboarding/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/onboarding/i;",
            ")",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$g;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    .line 196
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->e(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;

    move-result-object v1

    .line 197
    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/d;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/onboarding/i;->ordinal()I

    move-result p1

    aget p1, v2, p1

    packed-switch p1, :pswitch_data_0

    .line 201
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;->d()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 202
    :cond_0
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;->c()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 199
    :pswitch_1
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;->b()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 198
    :pswitch_2
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;->a()Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/swedbank/mobile/business/onboarding/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/WalletOnboardingInteractorImpl$g;->a(Lcom/swedbank/mobile/business/onboarding/i;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "getStepsForOnboardingState"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "getStepsForOnboardingState(Lcom/swedbank/mobile/business/onboarding/WalletOnboardingState;)Ljava/util/List;"

    return-object v0
.end method
