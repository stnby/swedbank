.class public abstract Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;
.super Ljava/lang/Exception;
.source "RegisterWallet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;,
        Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;,
        Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;,
        Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$Unknown;,
        Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;,
        Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;
    }
.end annotation


# instance fields
.field private final a:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;->a:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLkotlin/e/b/g;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;->a:Z

    return v0
.end method
