.class final Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;
.super Lkotlin/e/b/k;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/util/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/util/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lcom/swedbank/mobile/business/util/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->b:Lcom/swedbank/mobile/business/util/e;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .line 102
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->b:Lcom/swedbank/mobile/business/util/e;

    .line 115
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    .line 241
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_31

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    .line 242
    invoke-static {v1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V

    .line 243
    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    move-result-object v2

    .line 245
    instance-of v3, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;

    if-eqz v3, :cond_0

    .line 246
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Ljava/lang/String;)V

    .line 247
    invoke-interface {v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->c()V

    goto/16 :goto_9

    .line 249
    :cond_0
    instance-of v4, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    if-eqz v4, :cond_10

    if-eqz v4, :cond_1

    .line 251
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    .line 252
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v6

    .line 253
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v7

    .line 254
    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->c()D

    move-result-wide v8

    .line 255
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->d()Ljava/util/Currency;

    move-result-object v10

    .line 256
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->e()Ljava/lang/String;

    move-result-object v11

    move-object v5, v1

    .line 251
    invoke-direct/range {v5 .. v11}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    goto/16 :goto_2

    .line 257
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    if-eqz v1, :cond_e

    .line 258
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v1

    .line 259
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v3

    .line 260
    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v4

    .line 269
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_1

    .line 270
    :cond_2
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_1

    .line 271
    :cond_3
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 272
    :cond_4
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 273
    :cond_5
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 274
    :cond_6
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 275
    :cond_7
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 276
    :cond_8
    instance-of v5, v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v5, :cond_9

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 277
    :cond_9
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 278
    :cond_a
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    goto :goto_0

    .line 279
    :cond_b
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    goto :goto_0

    .line 280
    :cond_c
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    :goto_0
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    .line 282
    :goto_1
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result v0

    .line 257
    new-instance v5, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-direct {v5, v1, v3, v4, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;I)V

    move-object v1, v5

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    .line 249
    :goto_2
    invoke-interface {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    goto/16 :goto_9

    .line 280
    :cond_d
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_e
    if-eqz v3, :cond_f

    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No wallet payment result for AuthenticationRequired transaction event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_f
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 287
    :cond_10
    instance-of v5, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    if-eqz v5, :cond_32

    .line 288
    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v7

    sget-object v8, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_20

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    move-result-object v2

    if-eqz v4, :cond_11

    .line 291
    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    .line 292
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v8

    .line 293
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v9

    .line 294
    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->c()D

    move-result-wide v10

    .line 295
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->d()Ljava/util/Currency;

    move-result-object v12

    .line 296
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->e()Ljava/lang/String;

    move-result-object v13

    move-object v7, v3

    .line 291
    invoke-direct/range {v7 .. v13}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    goto/16 :goto_5

    :cond_11
    if-eqz v5, :cond_1e

    .line 298
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v3

    .line 299
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v0

    .line 300
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v4

    .line 309
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_4

    .line 310
    :cond_12
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_4

    .line 311
    :cond_13
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 312
    :cond_14
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 313
    :cond_15
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 314
    :cond_16
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 315
    :cond_17
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 316
    :cond_18
    instance-of v5, v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v5, :cond_19

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 317
    :cond_19
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 318
    :cond_1a
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    goto :goto_3

    .line 319
    :cond_1b
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    goto :goto_3

    .line 320
    :cond_1c
    sget-object v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    :goto_3
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    .line 322
    :goto_4
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result v5

    .line 297
    new-instance v6, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-direct {v6, v3, v0, v4, v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;I)V

    move-object v3, v6

    check-cast v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    .line 327
    :goto_5
    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->g(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Z

    move-result v0

    .line 288
    invoke-interface {v2, v3, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/k;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;Z)V

    goto/16 :goto_9

    .line 320
    :cond_1d
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_1e
    if-eqz v3, :cond_1f

    .line 323
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No wallet payment result for AuthenticationRequired transaction event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1f
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 328
    :cond_20
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;->a()Z

    move-result v7

    if-eqz v7, :cond_21

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;)V

    goto/16 :goto_9

    :cond_21
    if-eqz v4, :cond_22

    .line 331
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    .line 332
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v8

    .line 333
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v9

    .line 334
    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->c()D

    move-result-wide v10

    .line 335
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->d()Ljava/util/Currency;

    move-result-object v12

    .line 336
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->e()Ljava/lang/String;

    move-result-object v13

    move-object v7, v1

    .line 331
    invoke-direct/range {v7 .. v13}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    goto/16 :goto_8

    :cond_22
    if-eqz v5, :cond_2f

    .line 338
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v1

    .line 339
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v3

    .line 349
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_7

    .line 350
    :cond_23
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_7

    .line 351
    :cond_24
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 352
    :cond_25
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_26

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 353
    :cond_26
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 354
    :cond_27
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 355
    :cond_28
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 356
    :cond_29
    instance-of v4, v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v4, :cond_2a

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 357
    :cond_2a
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 358
    :cond_2b
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    goto :goto_6

    .line 359
    :cond_2c
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    goto :goto_6

    .line 360
    :cond_2d
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2e

    :goto_6
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    .line 362
    :goto_7
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result v4

    .line 337
    new-instance v5, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-direct {v5, v1, v0, v3, v4}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;I)V

    move-object v1, v5

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    .line 329
    :goto_8
    invoke-interface {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    goto :goto_9

    .line 360
    :cond_2e
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_2f
    if-eqz v3, :cond_30

    .line 363
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No wallet payment result for AuthenticationRequired transaction event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_30
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 371
    :cond_31
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_33

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    .line 106
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v2

    .line 107
    invoke-interface {v2}, Lcom/swedbank/mobile/business/cards/wallet/ad;->i()Lio/reactivex/w;

    move-result-object v2

    .line 108
    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j$a;

    invoke-direct {v3, v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/m;Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;)V

    check-cast v3, Lkotlin/e/a/b;

    const/4 v0, 0x1

    const/4 v4, 0x0

    invoke-static {v2, v4, v3, v0, v4}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 372
    invoke-static {v1, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_32
    :goto_9
    return-void

    .line 114
    :cond_33
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$j;->a()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
