.class public final Lcom/swedbank/mobile/business/cards/wallet/p;
.super Ljava/lang/Object;
.source "IsDeviceEligibleForWallet.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/f/a;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteWalletUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteWallet"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->b:Lcom/swedbank/mobile/business/f/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->c:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->a:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->c:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->b:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/p;->a:Lcom/swedbank/mobile/business/e/i;

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->k()Lio/reactivex/o;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/p$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/p$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/p;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "deviceRepository\n      .\u2026      }\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/p;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
