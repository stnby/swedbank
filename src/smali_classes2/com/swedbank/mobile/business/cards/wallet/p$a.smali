.class public final Lcom/swedbank/mobile/business/cards/wallet/p$a;
.super Ljava/lang/Object;
.source "IsDeviceEligibleForWallet.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/p;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/p;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$a;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/e/g;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/e/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e/g;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/p$a;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 82
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/p;->c(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v0

    const-string v1, "feature_cards_wallet"

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    new-instance p1, Lcom/swedbank/mobile/business/e/p$b;

    sget-object v0, Lcom/swedbank/mobile/business/e/p$c;->e:Lcom/swedbank/mobile/business/e/p$c;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/g;->a()I

    move-result v0

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    new-instance p1, Lcom/swedbank/mobile/business/e/p$b;

    sget-object v0, Lcom/swedbank/mobile/business/e/p$c;->a:Lcom/swedbank/mobile/business/e/p$c;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/g;->b()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance p1, Lcom/swedbank/mobile/business/e/p$b;

    sget-object v0, Lcom/swedbank/mobile/business/e/p$c;->b:Lcom/swedbank/mobile/business/e/p$c;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/g;->c()Z

    move-result p1

    if-nez p1, :cond_3

    new-instance p1, Lcom/swedbank/mobile/business/e/p$b;

    sget-object v0, Lcom/swedbank/mobile/business/e/p$c;->c:Lcom/swedbank/mobile/business/e/p$c;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    goto :goto_0

    .line 89
    :cond_3
    sget-object p1, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    .line 59
    :goto_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/p$a;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/p;->a(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    .line 60
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/e/i;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/b;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$a;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 92
    sget-object v1, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_1

    .line 93
    :cond_4
    instance-of v1, p1, Lcom/swedbank/mobile/business/e/p$b;

    if-eqz v1, :cond_5

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    :goto_1
    const-string v1, "when (this) {\n    Eligib\u2026 device eligibility\")\n  }"

    .line 94
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    check-cast p1, Lio/reactivex/aa;

    .line 61
    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    return-object p1

    .line 94
    :cond_5
    sget-object v0, Lcom/swedbank/mobile/business/e/p$d;->a:Lcom/swedbank/mobile/business/e/p$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Undetermined device eligibility"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/business/e/g;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/p$a;->a(Lcom/swedbank/mobile/business/e/g;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
