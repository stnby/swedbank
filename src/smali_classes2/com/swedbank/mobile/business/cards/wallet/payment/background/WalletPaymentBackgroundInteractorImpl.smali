.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletPaymentBackgroundInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/b;
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/b;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/k;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final b:Lcom/swedbank/mobile/business/cards/u;

.field private final c:Lcom/swedbank/mobile/business/e/i;

.field private final d:Lcom/swedbank/mobile/business/e/b;

.field private final e:Lcom/swedbank/mobile/business/cards/wallet/af;

.field private final f:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

.field private final g:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/cards/wallet/af;Lcom/swedbank/mobile/business/cards/wallet/payment/a;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/cards/wallet/af;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/cards/wallet/payment/a;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_background_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteWalletUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteAllWalletCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            "Lcom/swedbank/mobile/business/cards/u;",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/business/cards/wallet/af;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localCardsRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionBackgroundEventStream"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteWallet"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteAllWalletCards"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeAllCards"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/u;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->c:Lcom/swedbank/mobile/business/e/i;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->d:Lcom/swedbank/mobile/business/e/b;

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/af;

    iput-object p6, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    iput-object p7, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/g;

    iput-object p8, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    iput-object p9, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/f;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;
    .locals 0

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/u;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/u;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->d:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->c:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->g:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/a;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 182
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lio/reactivex/w;->e()Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMap { \u2026input) }\n      .toMaybe()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;->b()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;Z)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 172
    instance-of p2, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    move-result-object p1

    sget-object p2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    if-ne p1, p2, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;->a()V

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;->b()V

    return-void
.end method

.method protected m_()V
    .locals 10

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/af;

    .line 44
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/af;->a()Lio/reactivex/o;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "transactionEventStream"

    .line 47
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)V

    move-object v4, v1

    check-cast v4, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, v0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v1

    .line 186
    invoke-static {p0, v1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 56
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$e;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->c()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$f;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$g;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/background/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/d;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 68
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/background/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/c;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$h;

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "transactionEventStream\n \u2026 not emit errors\", it) })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 72
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->g()Lio/reactivex/o;

    move-result-object v0

    .line 73
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$i;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$j;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$j;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "walletRepository\n       \u2026rovisionFailed)\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 77
    invoke-static {v0, v2, v2, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 190
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/u;

    .line 81
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->d()Lio/reactivex/o;

    move-result-object v0

    .line 82
    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$k;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$k;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v0, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v0, "localCardsRepository\n   \u2026ervable.empty()\n        }"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x7

    const/4 v9, 0x0

    .line 93
    invoke-static/range {v4 .. v9}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 192
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 96
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 98
    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$l;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$l;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v0, v3}, Lio/reactivex/o;->o(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v0, "observeAllCards()\n      \u2026              }\n        }"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-static/range {v4 .. v9}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 194
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 111
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 112
    invoke-static {}, Lcom/swedbank/mobile/business/cards/wallet/ae;->a()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    .line 111
    invoke-interface {v0, v3, v4}, Lcom/swedbank/mobile/business/cards/wallet/ad;->a(Ljava/util/List;I)Lio/reactivex/b;

    move-result-object v0

    .line 114
    invoke-static {v0, v2, v2, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 196
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
