.class final Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;
.super Ljava/lang/Object;
.source "RegisterWallet.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a(Ljava/lang/Boolean;)Lio/reactivex/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/String;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pushId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    iget-object v1, v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->c(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v1

    .line 31
    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/c/g;)Lio/reactivex/b;

    move-result-object p1

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->d(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    check-cast v1, Lcom/swedbank/mobile/business/e/p;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/e/i;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->a(Z)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    .line 36
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
