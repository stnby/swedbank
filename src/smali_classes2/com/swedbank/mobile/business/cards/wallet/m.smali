.class public final Lcom/swedbank/mobile/business/cards/wallet/m;
.super Ljava/lang/Object;
.source "DigitizeCards.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Ljava/util/List<",
        "Ljava/lang/String;",
        ">;",
        "Lio/reactivex/w<",
        "Ljava/util/List<",
        "Lcom/swedbank/mobile/business/cards/wallet/b;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/m;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/m;Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getMobileAgreementIdUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "cleanWalletCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/m;",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getMobileAgreementId"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cleanWalletCards"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->a:Lcom/swedbank/mobile/business/cards/m;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->c:Lcom/swedbank/mobile/architect/business/g;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->d:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/m;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->a:Lcom/swedbank/mobile/business/cards/m;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/util/List;)Lio/reactivex/w;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->d:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    sget-object v1, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 29
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->c:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v2}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/j;

    .line 30
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/m$e;->a:Lcom/swedbank/mobile/business/cards/wallet/m$e;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v2

    .line 31
    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v2, v3}, Lio/reactivex/j;->d(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v2

    const-string v3, "getMobileAgreementId()\n \u2026        .toSingle(Absent)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/aa;

    .line 32
    iget-object v3, p0, Lcom/swedbank/mobile/business/cards/wallet/m;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 33
    invoke-interface {v3}, Lcom/swedbank/mobile/business/cards/wallet/ad;->b()Lio/reactivex/o;

    move-result-object v3

    .line 34
    invoke-virtual {v3}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v3

    const-string v4, "walletRepository\n       \u2026          .firstOrError()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/aa;

    .line 28
    invoke-virtual {v1, v2, v3}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v1

    .line 35
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/m$f;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$f;-><init>(Lcom/swedbank/mobile/business/cards/wallet/m;Ljava/util/List;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 52
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/w;->b(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    .line 28
    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "cleanWalletCards().andTh\u2026cribeOn(Schedulers.io()))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 27
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invoker provided no cards for digitization"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m;->a(Ljava/util/List;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
