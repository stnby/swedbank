.class final Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b;
.super Ljava/lang/Object;
.source "WalletOnboardingLockScreenInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authenticated"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 43
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;

    .line 59
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->j()Lio/reactivex/b;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b$1;-><init>(Ljava/lang/Boolean;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v0

    .line 45
    invoke-virtual {v0, p1}, Lio/reactivex/w;->c(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    .line 46
    :cond_0
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b;->a(Ljava/lang/Boolean;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
