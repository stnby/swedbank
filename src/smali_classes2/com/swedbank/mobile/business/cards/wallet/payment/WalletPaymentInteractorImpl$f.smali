.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;
.super Ljava/lang/Object;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v0

    .line 156
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->c(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 157
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/ad;->d(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$f;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
