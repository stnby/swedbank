.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingDigitizationInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/g;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;"
    }
.end annotation


# instance fields
.field private volatile a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final synthetic f:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(ZLjava/util/List;Lcom/swedbank/mobile/architect/business/f;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "digitize_cards_in_background"
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "cards_to_digitize"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "digitizeCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;>;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsToDigitize"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeAllCards"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "digitizeCards"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 33
    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    iput-boolean p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b:Z

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->c:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/f;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    .line 35
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;Ljava/util/List;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a:Ljava/util/List;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->c:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Lcom/swedbank/mobile/architect/business/f;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/f;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 86
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    .line 87
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    .line 89
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Lcom/swedbank/mobile/architect/business/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    .line 93
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/e;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "observeAllCards()\n      \u2026::filterDigitizableCards)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    :goto_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_3

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/f;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_3
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "findCardsToDigitize()\n  \u2026cardId)\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 40
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 41
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b:Z

    if-eqz v0, :cond_4

    .line 75
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    .line 76
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Ljava/util/List;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)Lcom/swedbank/mobile/architect/business/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    .line 82
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    .line 81
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/e;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "observeAllCards()\n      \u2026::filterDigitizableCards)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    :goto_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_3

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/f;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_3
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "findCardsToDigitize()\n  \u2026  .flatMap(digitizeCards)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$c;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lio/reactivex/i/f;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    .line 46
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->i()V

    :cond_4
    return-void
.end method
