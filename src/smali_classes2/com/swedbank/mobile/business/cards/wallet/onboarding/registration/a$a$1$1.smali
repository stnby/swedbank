.class final Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;
.super Ljava/lang/Object;
.source "RegisterWallet.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a(Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/f;
    .locals 3
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;->d(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;

    iget-object v1, v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;

    iget-object v1, v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a;

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    .line 71
    new-instance v2, Lcom/swedbank/mobile/business/e/p$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;->b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    move-result-object v1

    .line 72
    instance-of v1, v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/swedbank/mobile/business/e/p$c;->d:Lcom/swedbank/mobile/business/e/p$c;

    goto :goto_0

    .line 73
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/business/e/p$c;->f:Lcom/swedbank/mobile/business/e/p$c;

    .line 71
    :goto_0
    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast v2, Lcom/swedbank/mobile/business/e/p;

    .line 39
    invoke-interface {v0, v2}, Lcom/swedbank/mobile/business/e/i;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/b;

    move-result-object v0

    .line 40
    invoke-static {p1}, Lio/reactivex/b;->a(Ljava/lang/Throwable;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_1

    .line 41
    :cond_1
    invoke-static {p1}, Lio/reactivex/b;->a(Ljava/lang/Throwable;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/a$a$1$1;->a(Ljava/lang/Throwable;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
