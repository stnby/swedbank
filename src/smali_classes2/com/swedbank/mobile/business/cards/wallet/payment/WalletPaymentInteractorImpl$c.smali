.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;
.super Ljava/lang/Object;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lio/reactivex/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    const/4 v0, 0x2

    .line 130
    new-array v0, v0, [Lio/reactivex/b;

    .line 131
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v1

    .line 132
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/ad;->d(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 134
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->c(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v1

    .line 135
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/ad;->e(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 130
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/lang/Iterable;)Lio/reactivex/b;

    move-result-object v0

    .line 137
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/i;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/i;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/payment/j;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/j;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    return-void
.end method
