.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingLockScreenInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final c:Z

.field private final synthetic d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/cards/wallet/ad;ZLcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "only_enable_preconditions"
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 30
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-boolean p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->c:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->f()Lio/reactivex/w;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public b()V
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    .line 37
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->f()Lio/reactivex/w;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "deviceRepository\n       \u2026ed)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 59
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->f()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/WalletOnboardingLockScreenInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method
