.class final synthetic Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$g;
.super Lkotlin/e/b/i;
.source "WalletPaymentBackgroundInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/wallet/x;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/x;)Lio/reactivex/b;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$g;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;

    .line 186
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;

    move-result-object v1

    .line 187
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/x;->a()Lcom/swedbank/mobile/business/cards/wallet/w;

    move-result-object v2

    .line 188
    new-instance v3, Lkotlin/e/b/u$a;

    invoke-direct {v3}, Lkotlin/e/b/u$a;-><init>()V

    const/4 v4, 0x0

    iput-boolean v4, v3, Lkotlin/e/b/u$a;->a:Z

    .line 192
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->e(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    goto :goto_0

    .line 193
    :cond_0
    instance-of v4, v2, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    if-eqz v4, :cond_1

    .line 194
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->d(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v4

    invoke-interface {v4}, Lcom/swedbank/mobile/business/cards/wallet/ad;->a()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    iput-boolean v4, v3, Lkotlin/e/b/u$a;->a:Z

    .line 195
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->f(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v4

    .line 201
    new-instance v5, Lcom/swedbank/mobile/business/e/p$b;

    .line 202
    sget-object v6, Lcom/swedbank/mobile/business/e/p$c;->d:Lcom/swedbank/mobile/business/e/p$c;

    .line 201
    invoke-direct {v5, v6}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    check-cast v5, Lcom/swedbank/mobile/business/e/p;

    invoke-interface {v4, v5}, Lcom/swedbank/mobile/business/e/i;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/b;

    move-result-object v4

    .line 200
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->g(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v5

    invoke-interface {v5}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lio/reactivex/f;

    invoke-virtual {v4, v5}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v4

    .line 199
    new-instance v5, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$b;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->h(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/a;)V

    check-cast v5, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/b;

    invoke-direct {v0, v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/b;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {v4, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_1
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;->g(Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 191
    :goto_0
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v0

    .line 190
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$c;

    invoke-direct {v4, v3, p1, v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$c;-><init>(Lkotlin/e/b/u$a;Lcom/swedbank/mobile/business/cards/wallet/x;Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;Lcom/swedbank/mobile/business/cards/wallet/w;)V

    check-cast v4, Lio/reactivex/c/a;

    invoke-virtual {v0, v4}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "when (error) {\n      Wal\u2026or)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 207
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not handled errors are not handled. Received event="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$g;->a(Lcom/swedbank/mobile/business/cards/wallet/x;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "handleGeneralErrors"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "handleGeneralErrors(Lcom/swedbank/mobile/business/cards/wallet/WalletGeneralErrorEvent;)Lio/reactivex/Completable;"

    return-object v0
.end method
