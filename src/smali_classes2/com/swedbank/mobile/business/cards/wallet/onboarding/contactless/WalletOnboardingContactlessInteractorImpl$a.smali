.class final Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a;
.super Ljava/lang/Object;
.source "WalletOnboardingContactlessInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;->c()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/details/d;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/details/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/d;",
            ")",
            "Lio/reactivex/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    sget-object v0, Lcom/swedbank/mobile/business/cards/details/d$c;->a:Lcom/swedbank/mobile/business/cards/details/d$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a$1;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/j;->a(Lio/reactivex/c/a;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.fromAction { onboardingStepCompleted() }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/d$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/d$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/details/d$a;->a()Ljava/lang/String;

    move-result-object p1

    .line 49
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/d$b;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 50
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a$a;-><init>(Lcom/swedbank/mobile/business/util/s;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    .line 42
    invoke-static {v0}, Lio/reactivex/j;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.error(it.asErrorSupplier())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/WalletOnboardingContactlessInteractorImpl$a;->a(Lcom/swedbank/mobile/business/cards/details/d;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
