.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletPaymentResultInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/result/e;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final c:Lcom/swedbank/mobile/business/e/b;

.field private final d:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

.field private final f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "observeCardDetailsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_result"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_result_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/cards/details/q;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeCardDetails"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->c:Lcom/swedbank/mobile/business/e/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;

    .line 38
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/b;

    new-instance p2, Lcom/swedbank/mobile/business/cards/details/q$b;

    iget-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    invoke-virtual {p3}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;->a()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/swedbank/mobile/business/cards/details/q$b;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/o;

    .line 39
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->a:Lio/reactivex/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    return-object v0
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->a:Lio/reactivex/o;

    return-object v0
.end method

.method public c()V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    return-void
.end method

.method protected m_()V
    .locals 8

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->a:Lio/reactivex/o;

    const-wide/16 v1, 0x1

    .line 45
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "cardDetailsStream\n      \u2026Of(it.digitizedCardId)) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 54
    invoke-static {v0, v2, v2, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 72
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->c:Lcom/swedbank/mobile/business/e/b;

    .line 58
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->c()Lio/reactivex/o;

    move-result-object v0

    .line 59
    sget-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$c;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "activityLifecycleMonitor\u2026        .skipWhile { it }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 60
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 74
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_1
    return-void
.end method
