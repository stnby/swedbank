.class final Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;
.super Ljava/lang/Object;
.source "WalletPaymentResultInteractor.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/a;)Z
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object p1

    .line 73
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/s$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 74
    check-cast p1, Lcom/swedbank/mobile/business/cards/s$a;

    .line 48
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/s$a;->d()Lcom/swedbank/mobile/business/cards/x;

    move-result-object p1

    .line 49
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/x$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/cards/x$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/x$a;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 50
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    .line 73
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :cond_2
    return v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 31
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/WalletPaymentResultInteractorImpl$a;->a(Lcom/swedbank/mobile/business/cards/a;)Z

    move-result p1

    return p1
.end method
