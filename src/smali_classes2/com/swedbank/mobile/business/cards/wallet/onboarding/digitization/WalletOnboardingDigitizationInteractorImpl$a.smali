.class final Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;
.super Ljava/lang/Object;
.source "WalletOnboardingDigitizationInteractor.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->b()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Ljava/util/List<",
        "Lcom/swedbank/mobile/business/cards/wallet/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    .line 53
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 75
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/b;

    .line 53
    instance-of v2, v2, Lcom/swedbank/mobile/business/cards/wallet/b$a;

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 55
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->i()V

    goto :goto_2

    .line 56
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;

    check-cast v0, Ljava/lang/Iterable;

    .line 77
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 78
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 79
    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/b;

    .line 56
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/wallet/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 80
    :cond_3
    check-cast v1, Ljava/util/List;

    invoke-static {p1, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl;Ljava/util/List;)V

    :goto_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/WalletOnboardingDigitizationInteractorImpl$a;->a(Ljava/util/List;)V

    return-void
.end method
