.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingRegistrationInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/f;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final synthetic b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "registerWalletUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "registerWallet"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 22
    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->a:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 29
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl$a;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-virtual {v0, v2}, Lio/reactivex/b;->b(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "registerWallet()\n      .\u2026:onboardingStepCompleted)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/WalletOnboardingRegistrationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method
