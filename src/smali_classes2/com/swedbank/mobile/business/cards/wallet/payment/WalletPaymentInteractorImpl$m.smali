.class public final synthetic Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$m;
.super Lkotlin/e/b/i;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;

    .line 240
    invoke-static {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V

    .line 241
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    move-result-object v1

    .line 243
    instance-of v2, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$a;

    if-eqz v2, :cond_0

    .line 244
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Ljava/lang/String;)V

    .line 245
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->c()V

    goto/16 :goto_9

    .line 247
    :cond_0
    instance-of v3, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    if-eqz v3, :cond_10

    if-eqz v3, :cond_1

    .line 249
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    .line 250
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v5

    .line 251
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v6

    .line 252
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->c()D

    move-result-wide v7

    .line 253
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->d()Ljava/util/Currency;

    move-result-object v9

    .line 254
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->e()Ljava/lang/String;

    move-result-object v10

    move-object v4, v0

    .line 249
    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    goto/16 :goto_2

    .line 255
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    if-eqz v0, :cond_e

    .line 256
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v2

    .line 258
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v3

    .line 267
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_1

    .line 268
    :cond_2
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_1

    .line 269
    :cond_3
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 270
    :cond_4
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 271
    :cond_5
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 272
    :cond_6
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 273
    :cond_7
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 274
    :cond_8
    instance-of v4, v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v4, :cond_9

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 275
    :cond_9
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_1

    .line 276
    :cond_a
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    goto :goto_0

    .line 277
    :cond_b
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    goto :goto_0

    .line 278
    :cond_c
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    :goto_0
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    .line 280
    :goto_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result p1

    .line 255
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-direct {v4, v0, v2, v3, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;I)V

    move-object v0, v4

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    .line 247
    :goto_2
    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    goto/16 :goto_9

    .line 278
    :cond_d
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_e
    if-eqz v2, :cond_f

    .line 281
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No wallet payment result for AuthenticationRequired transaction event"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 285
    :cond_10
    instance-of v4, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    if-eqz v4, :cond_31

    .line 286
    move-object v5, p1

    check-cast v5, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_20

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->f(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    move-result-object v1

    if-eqz v3, :cond_11

    .line 289
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    .line 290
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v7

    .line 291
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v8

    .line 292
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->c()D

    move-result-wide v9

    .line 293
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->d()Ljava/util/Currency;

    move-result-object v11

    .line 294
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->e()Ljava/lang/String;

    move-result-object v12

    move-object v6, v2

    .line 289
    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    goto/16 :goto_5

    :cond_11
    if-eqz v4, :cond_1e

    .line 296
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v2

    .line 297
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object p1

    .line 298
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v3

    .line 307
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_4

    .line 308
    :cond_12
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_4

    .line 309
    :cond_13
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 310
    :cond_14
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 311
    :cond_15
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 312
    :cond_16
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 313
    :cond_17
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 314
    :cond_18
    instance-of v4, v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v4, :cond_19

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 315
    :cond_19
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_4

    .line 316
    :cond_1a
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    goto :goto_3

    .line 317
    :cond_1b
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    goto :goto_3

    .line 318
    :cond_1c
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    :goto_3
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    .line 320
    :goto_4
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result v4

    .line 295
    new-instance v5, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-direct {v5, v2, p1, v3, v4}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;I)V

    move-object v2, v5

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    .line 325
    :goto_5
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->g(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;)Z

    move-result p1

    .line 286
    invoke-interface {v1, v2, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/k;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;Z)V

    goto/16 :goto_9

    .line 318
    :cond_1d
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1e
    if-eqz v2, :cond_1f

    .line 321
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No wallet payment result for AuthenticationRequired transaction event"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 326
    :cond_20
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;->a()Z

    move-result v6

    if-eqz v6, :cond_21

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1, v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;)V

    goto/16 :goto_9

    :cond_21
    if-eqz v3, :cond_22

    .line 329
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    .line 330
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v7

    .line 331
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object v8

    .line 332
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->c()D

    move-result-wide v9

    .line 333
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->d()Ljava/util/Currency;

    move-result-object v11

    .line 334
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$c;->e()Ljava/lang/String;

    move-result-object v12

    move-object v6, v0

    .line 329
    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;DLjava/util/Currency;Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    goto/16 :goto_8

    :cond_22
    if-eqz v4, :cond_2f

    .line 336
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object v0

    .line 337
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->b()Ljava/lang/String;

    move-result-object p1

    .line 338
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a;

    move-result-object v2

    .line 347
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$j;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_7

    .line 348
    :cond_23
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$g;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto/16 :goto_7

    .line 349
    :cond_24
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$e;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 350
    :cond_25
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$d;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 351
    :cond_26
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$b;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 352
    :cond_27
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$i;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 353
    :cond_28
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$l;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_29

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 354
    :cond_29
    instance-of v3, v2, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$c;

    if-eqz v3, :cond_2a

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 355
    :cond_2a
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$h;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    goto :goto_7

    .line 356
    :cond_2b
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$k;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2c

    goto :goto_6

    .line 357
    :cond_2c
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$a;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2d

    goto :goto_6

    .line 358
    :cond_2d
    sget-object v3, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/n$b$a$f;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    :goto_6
    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    .line 360
    :goto_7
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;->e()I

    move-result v3

    .line 335
    new-instance v4, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-direct {v4, v0, p1, v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;I)V

    move-object v0, v4

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    .line 327
    :goto_8
    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/m;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V

    goto :goto_9

    .line 358
    :cond_2e
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2f
    if-eqz v2, :cond_30

    .line 361
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No wallet payment result for AuthenticationRequired transaction event"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_30
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_31
    :goto_9
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$m;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/n;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "delegateTransactionEvent"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "delegateTransactionEvent(Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentTransactionEvent;)V"

    return-object v0
.end method
