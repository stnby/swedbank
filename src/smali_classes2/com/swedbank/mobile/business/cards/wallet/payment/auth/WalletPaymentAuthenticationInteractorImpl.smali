.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletPaymentAuthenticationInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_payment_auth_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/cards/wallet/ad;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 5

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/d;

    .line 29
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/d;->a()Lio/reactivex/w;

    move-result-object v1

    const/4 v2, 0x0

    .line 30
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/w;->c(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v1

    .line 31
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v1

    const-string v2, "route.toAuthentication()\u2026ed)\n          }\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$b;

    iget-object v4, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)V

    check-cast v3, Lkotlin/e/a/b;

    invoke-static {v1, v2, v3}, Lio/reactivex/i/f;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;)Lio/reactivex/b/c;

    move-result-object v1

    .line 48
    invoke-static {p0, v1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 43
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/WalletPaymentAuthenticationInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/d;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables\n        .fro\u2026e::dismissAuthentication)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
