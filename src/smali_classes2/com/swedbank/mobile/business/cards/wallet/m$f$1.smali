.class final Lcom/swedbank/mobile/business/cards/wallet/m$f$1;
.super Ljava/lang/Object;
.source "DigitizeCards.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/m$f;->a(Lkotlin/k;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/m$f;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/m$f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->a:Lcom/swedbank/mobile/business/cards/wallet/m$f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->a:Lcom/swedbank/mobile/business/cards/wallet/m$f;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/wallet/m$f;->a:Lcom/swedbank/mobile/business/cards/wallet/m;

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->b:Ljava/lang/String;

    .line 47
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->c:Ljava/lang/String;

    .line 104
    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/m;->a(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v3

    .line 109
    invoke-interface {v3, p1, v2, v1}, Lcom/swedbank/mobile/business/cards/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v1

    .line 108
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/m$a;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/m;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v1

    .line 107
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/m$b;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/m;->b(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/wallet/ad;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/business/cards/wallet/m$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;)V

    check-cast v2, Lkotlin/e/a/b;

    new-instance v3, Lcom/swedbank/mobile/business/cards/wallet/n;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/wallet/n;-><init>(Lkotlin/e/a/b;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v1, v3}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v1

    .line 106
    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/m$c;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/m;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/m$d;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$d;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "cardsRepository\n      .g\u2026      error = it)\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$f$1;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
