.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$m;
.super Ljava/lang/Object;
.source "WalletPaymentBackgroundInteractor.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$m;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/k;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/k<",
            "Ljava/lang/Boolean;",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    .line 120
    check-cast v0, Lcom/swedbank/mobile/business/cards/a;

    .line 121
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$m;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/a;

    .line 122
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v3

    .line 123
    instance-of v4, v3, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v4

    sget-object v5, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    if-ne v4, v5, :cond_0

    .line 124
    check-cast v3, Lcom/swedbank/mobile/business/cards/s$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/cards/s$a;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    if-nez v0, :cond_0

    .line 129
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v3

    .line 132
    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->b:Lcom/swedbank/mobile/business/cards/t;

    if-eq v3, v4, :cond_2

    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->c:Lcom/swedbank/mobile/business/cards/t;

    if-eq v3, v4, :cond_2

    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->e:Lcom/swedbank/mobile/business/cards/t;

    if-ne v3, v4, :cond_0

    :cond_2
    move-object v0, v2

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    .line 138
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/payment/background/WalletPaymentBackgroundInteractorImpl$m;->a()Lkotlin/k;

    move-result-object v0

    return-object v0
.end method
