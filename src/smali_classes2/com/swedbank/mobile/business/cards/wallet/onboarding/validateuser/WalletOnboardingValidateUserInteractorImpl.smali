.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingValidateUserInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/c;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/a;"
    }
.end annotation


# instance fields
.field private final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    .line 17
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 20
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/c;

    .line 22
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/c;->a()Lio/reactivex/w;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/WalletOnboardingValidateUserInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 30
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
