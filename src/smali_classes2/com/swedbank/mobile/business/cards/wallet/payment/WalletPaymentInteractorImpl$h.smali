.class public final Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;
.super Ljava/lang/Object;
.source "WalletPaymentInteractor.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Lcom/swedbank/mobile/business/cards/wallet/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/x;)Z
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/x;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/x;->a()Lcom/swedbank/mobile/business/cards/wallet/w;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 45
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/WalletPaymentInteractorImpl$h;->a(Lcom/swedbank/mobile/business/cards/wallet/x;)Z

    move-result p1

    return p1
.end method
