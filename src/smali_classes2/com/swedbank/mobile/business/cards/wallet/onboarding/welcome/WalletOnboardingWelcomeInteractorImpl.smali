.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/WalletOnboardingWelcomeInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingWelcomeInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/c;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/a;"
    }
.end annotation


# instance fields
.field private final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/WalletOnboardingWelcomeInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    .line 16
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/WalletOnboardingWelcomeInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/WalletOnboardingWelcomeInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/WalletOnboardingWelcomeInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/welcome/WalletOnboardingWelcomeInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method
