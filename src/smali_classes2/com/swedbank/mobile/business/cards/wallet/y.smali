.class public final Lcom/swedbank/mobile/business/cards/wallet/y;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "WalletGeneralErrorNotificationFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/cards/wallet/z;",
        "Lcom/swedbank/mobile/business/general/confirmation/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/list/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/list/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/list/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticatedCardsListFlow"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/y;->a:Lcom/swedbank/mobile/business/cards/list/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/y;)Lcom/swedbank/mobile/business/cards/list/a;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/y;->a:Lcom/swedbank/mobile/business/cards/list/a;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/z;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/wallet/y;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/wallet/z;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/wallet/z;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/z;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/cards/wallet/z;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/swedbank/mobile/business/cards/wallet/y$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/cards/wallet/y$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/y;Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/wallet/z;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/business/cards/wallet/y;->a(Lkotlin/e/a/a;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/z;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/wallet/y;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/wallet/z;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
