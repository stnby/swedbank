.class final Lcom/swedbank/mobile/business/cards/wallet/p$b;
.super Ljava/lang/Object;
.source "IsDeviceEligibleForWallet.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/wallet/p;->a()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/p;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/p;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/w;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/e/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e/p;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "prevEligibility"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget-object v0, Lcom/swedbank/mobile/business/e/p$d;->a:Lcom/swedbank/mobile/business/e/p$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 80
    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/p;->a(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    .line 83
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->d()Lio/reactivex/w;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/p$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/p$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/p;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "deviceRepository\n      .\u2026ity.isEligible())\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 35
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 36
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 90
    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/p;->c(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/f/a;

    move-result-object p1

    const-string v0, "feature_cards_wallet"

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_1

    .line 38
    :cond_1
    new-instance p1, Lcom/swedbank/mobile/business/e/p$b;

    sget-object v2, Lcom/swedbank/mobile/business/e/p$c;->e:Lcom/swedbank/mobile/business/e/p$c;

    invoke-direct {p1, v2}, Lcom/swedbank/mobile/business/e/p$b;-><init>(Lcom/swedbank/mobile/business/e/p$c;)V

    .line 39
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    invoke-static {v2}, Lcom/swedbank/mobile/business/cards/wallet/p;->a(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v2

    .line 40
    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    invoke-interface {v2, p1}, Lcom/swedbank/mobile/business/e/i;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/b;

    move-result-object v2

    .line 41
    iget-object v3, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    invoke-static {v3}, Lcom/swedbank/mobile/business/cards/wallet/p;->b(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v3

    invoke-interface {v3}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/reactivex/f;

    invoke-virtual {v2, v3}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v2

    .line 42
    iget-object v3, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 92
    sget-object v3, Lcom/swedbank/mobile/business/e/p$a;->a:Lcom/swedbank/mobile/business/e/p$a;

    invoke-static {p1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    .line 93
    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    :goto_0
    const-string v0, "when (this) {\n    Eligib\u2026 device eligibility\")\n  }"

    .line 94
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    check-cast p1, Lio/reactivex/aa;

    .line 42
    invoke-virtual {v2, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    :goto_1
    const-string v0, "when {\n            isWal\u2026            }\n          }"

    .line 35
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 45
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/business/e/p$b;

    if-eqz v0, :cond_6

    .line 49
    check-cast p1, Lcom/swedbank/mobile/business/e/p$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/p$b;->a()Lcom/swedbank/mobile/business/e/p$c;

    move-result-object v0

    sget-object v2, Lcom/swedbank/mobile/business/e/p$c;->a:Lcom/swedbank/mobile/business/e/p$c;

    if-eq v0, v2, :cond_5

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/p$b;->a()Lcom/swedbank/mobile/business/e/p$c;

    move-result-object v0

    sget-object v2, Lcom/swedbank/mobile/business/e/p$c;->e:Lcom/swedbank/mobile/business/e/p$c;

    if-eq v0, v2, :cond_5

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/p$b;->a()Lcom/swedbank/mobile/business/e/p$c;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/business/e/p$c;->d:Lcom/swedbank/mobile/business/e/p$c;

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 96
    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/p;->c(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/f/a;

    move-result-object p1

    const-string v0, "feature_cards_wallet"

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_2

    .line 50
    :cond_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(false)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 49
    :cond_5
    :goto_2
    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a:Lcom/swedbank/mobile/business/cards/wallet/p;

    .line 97
    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/wallet/p;->a(Lcom/swedbank/mobile/business/cards/wallet/p;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v0

    .line 100
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->d()Lio/reactivex/w;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/p$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/p$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/p;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "deviceRepository\n      .\u2026ity.isEligible())\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    return-object p1

    .line 45
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/business/e/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/p$b;->a(Lcom/swedbank/mobile/business/e/p;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
