.class public final enum Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;
.super Ljava/lang/Enum;
.source "WalletPaymentResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field public static final enum j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

.field private static final synthetic k:[Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;


# instance fields
.field private final l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    new-instance v7, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v2, "NO_PAYMENT_TOKENS_ERROR"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v7, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v9, "TRANSACTION_LIMIT_EXCEEDED_ERROR"

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v4, "TERMINAL_ERROR"

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/4 v3, 0x2

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v5, "NFC_ERROR"

    const/4 v6, 0x3

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v9}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->d:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/4 v3, 0x3

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v5, "CARD_ERROR"

    const/4 v6, 0x4

    move-object v4, v1

    invoke-direct/range {v4 .. v9}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->e:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/4 v3, 0x4

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v5, "TECHNICAL_ERROR"

    const/4 v6, 0x5

    move-object v4, v1

    invoke-direct/range {v4 .. v9}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->f:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/4 v3, 0x5

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v3, "USER_CHANGED_ERROR"

    const/4 v4, 0x6

    .line 37
    invoke-direct {v1, v3, v4, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->g:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v3, "DEVICE_ROOTED_ERROR"

    const/4 v4, 0x7

    .line 38
    invoke-direct {v1, v3, v4, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->h:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v3, "SECURITY_BREACH_ERROR"

    const/16 v4, 0x8

    .line 39
    invoke-direct {v1, v3, v4, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const-string v6, "UNKNOWN"

    const/16 v7, 0x9

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v10}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->j:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->k:[Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->l:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IZILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 29
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;-><init>(Ljava/lang/String;IZ)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->k:[Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .line 29
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->l:Z

    return v0
.end method
