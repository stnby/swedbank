.class public final Lcom/swedbank/mobile/business/cards/wallet/m$c;
.super Ljava/lang/Object;
.source "DigitizeCards.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/wallet/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/wallet/m;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/m;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$c;->a:Lcom/swedbank/mobile/business/cards/wallet/m;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$c;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/b$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/m$c;->a:Lcom/swedbank/mobile/business/cards/wallet/m;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/wallet/m;->a(Lcom/swedbank/mobile/business/cards/wallet/m;)Lcom/swedbank/mobile/business/cards/m;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/m$c;->b:Ljava/lang/String;

    .line 72
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/cards/m;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/b$b;

    .line 76
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/wallet/m$c;->b:Ljava/lang/String;

    .line 75
    invoke-direct {v1, v2, p1}, Lcom/swedbank/mobile/business/cards/wallet/b$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/wallet/m$c;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
