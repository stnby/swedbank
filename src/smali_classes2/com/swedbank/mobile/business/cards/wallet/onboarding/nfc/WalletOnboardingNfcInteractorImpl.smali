.class public final Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "WalletOnboardingNfcInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/c;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;",
        "Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/e/i;

.field private final b:Lcom/swedbank/mobile/business/e/j;

.field private final c:Z

.field private final synthetic d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/e/j;ZLcom/swedbank/mobile/business/cards/wallet/onboarding/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "only_enable_preconditions"
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deviceRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalActivityManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingStepListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 34
    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->b:Lcom/swedbank/mobile/business/e/j;

    iput-boolean p3, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->c:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;)Lcom/swedbank/mobile/business/e/j;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->b:Lcom/swedbank/mobile/business/e/j;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->g()Lio/reactivex/w;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 39
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(false)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->h()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 3

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->a:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->g()Lio/reactivex/w;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "deviceRepository.isNfcEn\u2026).map { false }\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v1

    new-instance v2, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;)V

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {v0, v1, v2}, Lio/reactivex/i/f;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;)Lio/reactivex/b/c;

    move-result-object v0

    .line 54
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->i()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->j()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->k()V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/WalletOnboardingNfcInteractorImpl;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/m;->l()V

    return-void
.end method
