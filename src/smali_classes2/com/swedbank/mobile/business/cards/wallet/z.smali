.class public final Lcom/swedbank/mobile/business/cards/wallet/z;
.super Ljava/lang/Object;
.source "WalletGeneralErrorNotificationFlow.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/a/b;


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/w;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/w;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/wallet/z;->a:Lcom/swedbank/mobile/business/cards/wallet/w;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/cards/wallet/w;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/z;->a:Lcom/swedbank/mobile/business/cards/wallet/w;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/z;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/z;

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/z;->a:Lcom/swedbank/mobile/business/cards/wallet/w;

    iget-object p1, p1, Lcom/swedbank/mobile/business/cards/wallet/z;->a:Lcom/swedbank/mobile/business/cards/wallet/w;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/wallet/z;->a:Lcom/swedbank/mobile/business/cards/wallet/w;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WalletGeneralErrorNotificationFlowInput(error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/wallet/z;->a:Lcom/swedbank/mobile/business/cards/wallet/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
