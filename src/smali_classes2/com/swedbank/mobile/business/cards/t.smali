.class public final enum Lcom/swedbank/mobile/business/cards/t;
.super Ljava/lang/Enum;
.source "Card.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum b:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum c:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum d:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum e:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum f:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum g:Lcom/swedbank/mobile/business/cards/t;

.field public static final enum h:Lcom/swedbank/mobile/business/cards/t;

.field private static final synthetic i:[Lcom/swedbank/mobile/business/cards/t;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/t;

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "DIGITIZATION_STARTED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->a:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "DIGITIZED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->b:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "PROVISIONED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->c:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "PROVISION_FAILED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->d:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "ACTIVE"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->e:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "SUSPENDED"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "MARKED_FOR_DELETION"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/t;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/t;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/t;->h:Lcom/swedbank/mobile/business/cards/t;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/cards/t;->i:[Lcom/swedbank/mobile/business/cards/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/t;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/t;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/t;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/t;->i:[Lcom/swedbank/mobile/business/cards/t;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/t;

    return-object v0
.end method
