.class public final Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;
.super Ljava/lang/Object;
.source "CardsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l;->a(Ljava/lang/Boolean;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/ac;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ac;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/ac$a;->a:Lcom/swedbank/mobile/business/cards/wallet/ac$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 121
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/ac$b;

    if-eqz v0, :cond_1

    .line 122
    new-instance v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$b;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/ac$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/ac$b;->a()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$b;-><init>(Ljava/util/List;)V

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/ac;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l$1;->a(Lcom/swedbank/mobile/business/cards/wallet/ac;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
