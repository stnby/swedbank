.class public final Lcom/swedbank/mobile/business/cards/q;
.super Ljava/lang/Object;
.source "ObserveAllCards.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/swedbank/mobile/business/cards/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/cards/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    new-instance v0, Lcom/swedbank/mobile/business/cards/q;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/cards/q;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/cards/q;->a:Lcom/swedbank/mobile/business/cards/q;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/a;Lcom/swedbank/mobile/business/cards/a;)I
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 53
    invoke-static {p1, p2}, Lcom/swedbank/mobile/business/util/k;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    .line 54
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/p;

    if-eqz v0, :cond_1

    :goto_0
    const/4 v1, -0x1

    goto :goto_1

    .line 55
    :cond_1
    instance-of p2, p2, Lcom/swedbank/mobile/business/cards/p;

    if-eqz p2, :cond_2

    goto :goto_1

    .line 56
    :cond_2
    instance-of p1, p1, Lcom/swedbank/mobile/business/cards/o;

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    return v1
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 51
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    check-cast p2, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/q;->a(Lcom/swedbank/mobile/business/cards/a;Lcom/swedbank/mobile/business/cards/a;)I

    move-result p1

    return p1
.end method
