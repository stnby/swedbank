.class public final Lcom/swedbank/mobile/business/cards/e;
.super Ljava/lang/Object;
.source "Card.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/cards/a;)Lcom/swedbank/mobile/business/cards/CardClass;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$getCardClass"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    instance-of v0, p0, Lcom/swedbank/mobile/business/cards/p;

    if-eqz v0, :cond_0

    sget-object p0, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    goto :goto_0

    .line 64
    :cond_0
    instance-of p0, p0, Lcom/swedbank/mobile/business/cards/o;

    if-eqz p0, :cond_1

    sget-object p0, Lcom/swedbank/mobile/business/cards/CardClass;->CREDIT:Lcom/swedbank/mobile/business/cards/CardClass;

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
