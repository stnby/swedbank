.class public final Lcom/swedbank/mobile/business/cards/v$a;
.super Ljava/lang/Object;
.source "Observables.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/v;->a()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "TT1;TT2;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/v;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/v;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/v$a;->a:Lcom/swedbank/mobile/business/cards/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)TR;"
        }
    .end annotation

    .line 20
    move-object/from16 v0, p2

    check-cast v0, Ljava/util/List;

    move-object/from16 v1, p1

    check-cast v1, Ljava/util/List;

    move-object/from16 v2, p0

    iget-object v3, v2, Lcom/swedbank/mobile/business/cards/v$a;->a:Lcom/swedbank/mobile/business/cards/v;

    .line 255
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_4

    .line 258
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 259
    check-cast v1, Ljava/lang/Iterable;

    .line 260
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/cards/a;

    .line 262
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/a;->s()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 263
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/a;->o()Ljava/lang/String;

    move-result-object v5

    .line 264
    move-object v6, v0

    check-cast v6, Ljava/lang/Iterable;

    .line 267
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Lcom/swedbank/mobile/business/cards/s$a;

    .line 265
    invoke-virtual {v8}, Lcom/swedbank/mobile/business/cards/s$a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    .line 268
    :goto_1
    check-cast v7, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v7, :cond_5

    .line 270
    instance-of v5, v4, Lcom/swedbank/mobile/business/cards/p;

    if-eqz v5, :cond_3

    move-object v8, v4

    check-cast v8, Lcom/swedbank/mobile/business/cards/p;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v22, v7

    check-cast v22, Lcom/swedbank/mobile/business/cards/s;

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const v27, 0x3dfff

    const/16 v28, 0x0

    invoke-static/range {v8 .. v28}, Lcom/swedbank/mobile/business/cards/p;->a(Lcom/swedbank/mobile/business/cards/p;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;ZLcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/s;ZZZLcom/swedbank/mobile/business/cards/r;ILjava/lang/Object;)Lcom/swedbank/mobile/business/cards/p;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/cards/a;

    goto :goto_2

    .line 271
    :cond_3
    instance-of v5, v4, Lcom/swedbank/mobile/business/cards/o;

    if-eqz v5, :cond_4

    move-object v8, v4

    check-cast v8, Lcom/swedbank/mobile/business/cards/o;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v22, v7

    check-cast v22, Lcom/swedbank/mobile/business/cards/s;

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const v27, 0x3dfff

    const/16 v28, 0x0

    invoke-static/range {v8 .. v28}, Lcom/swedbank/mobile/business/cards/o;->a(Lcom/swedbank/mobile/business/cards/o;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;ZLcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/s;ZZZLcom/swedbank/mobile/business/cards/r;ILjava/lang/Object;)Lcom/swedbank/mobile/business/cards/o;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/cards/a;

    :goto_2
    if-eqz v5, :cond_5

    move-object v4, v5

    goto :goto_3

    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 278
    :cond_5
    :goto_3
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 281
    :cond_6
    move-object v1, v3

    check-cast v1, Ljava/util/List;

    :goto_4
    return-object v1
.end method
