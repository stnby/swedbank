.class final Lcom/swedbank/mobile/business/cards/z$a;
.super Ljava/lang/Object;
.source "ReplenishPaymentTokens.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/z;->a()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/z;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/z;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/z$a;->a:Lcom/swedbank/mobile/business/cards/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/z$a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object p1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/business/cards/z$a$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/z$a$1;-><init>(Lcom/swedbank/mobile/business/cards/z$a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 19
    sget-object v0, Lcom/swedbank/mobile/business/cards/aa;->a:Lkotlin/h/i;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 20
    invoke-static {p1}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
