.class final Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Long;)Lio/reactivex/o;
    .locals 4
    .param p1    # Ljava/lang/Long;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "digitizedCardCount"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    .line 55
    sget-object p1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$a;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$a;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 56
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 57
    sget-object v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d$1;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v0

    .line 58
    sget-object v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d$2;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d$2;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 54
    invoke-static {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.concat(\n     \u2026         .toObservable())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_0
    sget-object p1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$b;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$b;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(Empty)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d;->a(Ljava/lang/Long;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
