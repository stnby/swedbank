.class public final Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "NotAuthenticatedCardsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/notauth/c;
.implements Lcom/swedbank/mobile/business/cards/notauth/list/h;
.implements Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/notauth/d;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/notauth/c;",
        "Lcom/swedbank/mobile/business/cards/notauth/list/h;",
        "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/general/confirmation/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/general/confirmation/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/cards/u;

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isWalletPreconditionsSatisfiedUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/u;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "localCardsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isWalletPreconditionsSatisfied"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/u;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    .line 35
    new-instance p1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$f;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)V

    check-cast p1, Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    .line 39
    new-instance p1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$g;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)V

    check-cast p1, Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)Lcom/swedbank/mobile/business/cards/notauth/d;
    .locals 0

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/notauth/d;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/w;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/w;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow().flatMapMaybe { it\u2026rrorNotification(error) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/notauth/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/notauth/d;->c()V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)V"
        }
    .end annotation

    const-string v0, "notSatisfiedPreconditions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/notauth/d;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/notauth/d;->a(Ljava/util/List;)V

    return-void
.end method

.method public final b()Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object v0
.end method

.method public final i()Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->b:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object v0
.end method

.method public j()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/notauth/a;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/notauth/a;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow().flatMapMaybe(NotA\u2026OutdatedCardShortcutHelp)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 7

    .line 45
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/u;

    .line 47
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/u;->d()Lio/reactivex/o;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;->c:Lcom/swedbank/mobile/business/cards/u;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/u;->c()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 65
    sget-object v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$a;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$a;

    check-cast v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a;

    goto :goto_0

    .line 66
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$b;->a:Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a$b;

    check-cast v1, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$a;

    .line 63
    :goto_0
    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v0, "localCardsRepository\n   \u2026  .distinctUntilChanged()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 70
    new-instance v0, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/cards/notauth/NotAuthenticatedCardsInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 100
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
