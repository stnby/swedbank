.class public final Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "NotAuthenticatedCardsListInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/notauth/list/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/notauth/list/i;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/notauth/list/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl$a;

.field private final b:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/cards/af;",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/cards/notauth/list/h;

.field private final f:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/f;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/cards/notauth/list/h;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "startWalletPaymentUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "replenishPaymentTokensUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/cards/notauth/list/h;
        .annotation runtime Ljavax/inject/Named;
            value = "not_authenticated_cards_list_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "deleteWalletCardUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/cards/af;",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/b/c;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/notauth/list/h;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "observeAllCards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startWalletPayment"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "replenishPaymentTokens"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteWalletCard"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/notauth/list/h;

    iput-object p5, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/b;

    .line 38
    new-instance p1, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl$a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;)Lcom/swedbank/mobile/business/cards/notauth/list/i;
    .locals 0

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/notauth/list/i;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;)Lcom/swedbank/mobile/business/cards/notauth/list/h;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->e:Lcom/swedbank/mobile/business/cards/notauth/list/h;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/notauth/list/i;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/notauth/list/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->a:Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl$a;

    invoke-static {v1, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/b/c;

    .line 75
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/notauth/list/i;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/notauth/list/i;->b(Ljava/lang/String;)V

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/b;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 59
    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v0

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->f()Lio/reactivex/c/g;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "deleteWalletCard(digitiz\u2026er(), LOG_ERROR_CONSUMER)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/notauth/list/i;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/notauth/list/i;->d(Ljava/lang/String;)V

    return-void
.end method

.method public m()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 45
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/list/NotAuthenticatedCardsListInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 47
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/notauth/list/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/notauth/list/f;-><init>(Lkotlin/e/a/a;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/a;

    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->e()Lkotlin/e/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/swedbank/mobile/business/cards/notauth/list/g;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/notauth/list/g;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "replenishPaymentTokens()\u2026ibe(NO_ACTION, LOG_ERROR)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
