.class public final Lcom/swedbank/mobile/business/cards/notauth/list/a;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "CardActivationFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/cards/notauth/list/b;",
        "Lcom/swedbank/mobile/business/cards/list/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/list/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/list/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/list/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsListFlow"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/notauth/list/a;->a:Lcom/swedbank/mobile/business/cards/list/a;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/notauth/list/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/notauth/list/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/notauth/list/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/notauth/list/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/notauth/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/cards/notauth/list/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/cards/list/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/notauth/list/a;->a:Lcom/swedbank/mobile/business/cards/list/a;

    sget-object v1, Lcom/swedbank/mobile/business/cards/list/b;->a:Lcom/swedbank/mobile/business/cards/list/b;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/business/cards/list/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/list/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/business/cards/notauth/list/a$a;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/business/cards/notauth/list/a$a;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/cards/notauth/list/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/cards/notauth/list/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/cards/notauth/list/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
