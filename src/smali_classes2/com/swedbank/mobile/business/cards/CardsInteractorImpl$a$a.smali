.class public final Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;
.super Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;
.source "CardsInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;-><init>(Lcom/swedbank/mobile/business/util/l;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "cardsEligibleForDigitization"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 138
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;->a:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/util/l;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 138
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;->a:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method
