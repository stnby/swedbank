.class public final Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CardsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/l;
.implements Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/cards/n;",
        ">;",
        "Lcom/swedbank/mobile/business/cards/l;",
        "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/onboarding/h;

.field private final c:Lcom/swedbank/mobile/architect/business/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/h;Lcom/swedbank/mobile/architect/business/f;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/f;
        .annotation runtime Ljavax/inject/Named;
            value = "observeAllCardsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isDeviceEligibleForWalletUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isWalletPreconditionsSatisfiedUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/onboarding/h;",
            "Lcom/swedbank/mobile/architect/business/f<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observeAllCards"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isDeviceEligibleForWallet"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isWalletPreconditionsSatisfied"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->b:Lcom/swedbank/mobile/business/onboarding/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/f;

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    iput-object p4, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    .line 40
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<CardsNodeState>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/swedbank/mobile/business/cards/n;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/n;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/h;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->b:Lcom/swedbank/mobile/business/onboarding/h;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/swedbank/mobile/architect/business/f;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/f;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 128
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/n;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/n;->b()V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->a:Lcom/b/c/c;

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$c;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$c;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/cards/list/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 132
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 133
    sget-object v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/cards/j;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/cards/j;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lio/reactivex/w;->e()Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMap(Ca\u2026rdsList)\n      .toMaybe()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 8

    .line 43
    invoke-super {p0}, Lcom/swedbank/mobile/architect/business/c;->m_()V

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->b:Lcom/swedbank/mobile/business/onboarding/h;

    .line 45
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/h;->a()Lio/reactivex/o;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$c;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$c;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->b:Lcom/swedbank/mobile/business/onboarding/h;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/onboarding/h;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2, v3}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$a;-><init>(Lcom/swedbank/mobile/business/util/l;ILkotlin/e/b/g;)V

    check-cast v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;

    goto :goto_0

    .line 58
    :cond_0
    new-instance v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$c;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a$c;-><init>(Z)V

    check-cast v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;

    .line 56
    :goto_0
    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "onboardingRepository\n   \u2026rding = false)\n        })"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 60
    new-instance v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 143
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
