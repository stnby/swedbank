.class public abstract Lcom/swedbank/mobile/business/cards/a;
.super Ljava/lang/Object;
.source "Card.kt"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/business/cards/a;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract c()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract f()Lcom/swedbank/mobile/business/cards/CardState;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract g()Lcom/swedbank/mobile/business/cards/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract h()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract i()Lcom/swedbank/mobile/business/cards/g;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract j()Z
.end method

.method public abstract k()Z
.end method

.method public abstract l()Z
.end method

.method public abstract m()Lcom/swedbank/mobile/business/cards/DigitizationProgression;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract n()Z
.end method

.method public abstract o()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract p()Lcom/swedbank/mobile/business/cards/s;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract q()Lcom/swedbank/mobile/business/cards/r;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public final r()Z
    .locals 3

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/swedbank/mobile/business/cards/s$b;->a:Lcom/swedbank/mobile/business/cards/s$b;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    instance-of v1, v0, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/cards/s$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final s()Z
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->o()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final t()Z
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v0

    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/s$a;

    return v0
.end method
