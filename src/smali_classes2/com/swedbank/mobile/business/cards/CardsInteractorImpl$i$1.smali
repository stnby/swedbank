.class public final Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1;
.super Ljava/lang/Object;
.source "CardsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;->a(Lcom/swedbank/mobile/business/onboarding/i;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;",
            ">;"
        }
    .end annotation

    const-string v0, "isDeviceEligible"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;

    iget-object p1, p1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->e(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/swedbank/mobile/architect/business/f;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/f;->c()Lio/reactivex/o;

    move-result-object p1

    .line 92
    sget-object v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$1;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$1;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/cards/h;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/cards/h;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    .line 99
    sget-object v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$2;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$2;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/swedbank/mobile/business/cards/i;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/cards/i;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_1
    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    .line 100
    sget-object v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1$3;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 104
    :cond_2
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i$1;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
