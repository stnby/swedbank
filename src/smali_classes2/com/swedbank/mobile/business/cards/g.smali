.class public final enum Lcom/swedbank/mobile/business/cards/g;
.super Ljava/lang/Enum;
.source "CardType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum B:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum C:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum D:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum E:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum F:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum G:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum H:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum I:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum J:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum K:Lcom/swedbank/mobile/business/cards/g;

.field private static final synthetic L:[Lcom/swedbank/mobile/business/cards/g;

.field public static final enum a:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum b:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum c:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum d:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum e:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum f:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum g:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum h:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum i:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum j:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum k:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum l:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum m:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum n:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum o:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum p:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum q:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum r:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum s:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum t:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum u:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum v:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum w:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum x:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum y:Lcom/swedbank/mobile/business/cards/g;

.field public static final enum z:Lcom/swedbank/mobile/business/cards/g;


# instance fields
.field private final M:Lcom/swedbank/mobile/business/cards/d;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final N:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x25

    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/g;

    new-instance v8, Lcom/swedbank/mobile/business/cards/g;

    const-string v2, "UNKNOWN"

    .line 8
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->f:Lcom/swedbank/mobile/business/cards/d;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v8, Lcom/swedbank/mobile/business/cards/g;->a:Lcom/swedbank/mobile/business/cards/g;

    const/4 v1, 0x0

    aput-object v8, v0, v1

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v10, "SELF_DESIGN"

    .line 10
    sget-object v12, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/4 v11, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x2

    const/4 v15, 0x0

    move-object v9, v1

    invoke-direct/range {v9 .. v15}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->b:Lcom/swedbank/mobile/business/cards/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v4, "MAESTRO_ART"

    .line 11
    sget-object v6, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v9}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->c:Lcom/swedbank/mobile/business/cards/g;

    const/4 v3, 0x2

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "MAESTRO_MUSIC"

    .line 12
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/4 v6, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->d:Lcom/swedbank/mobile/business/cards/g;

    const/4 v3, 0x3

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "VISA_JUNIOR"

    .line 13
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/4 v6, 0x4

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->e:Lcom/swedbank/mobile/business/cards/g;

    const/4 v3, 0x4

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "VISA_ELECTRON"

    .line 14
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/4 v6, 0x5

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->f:Lcom/swedbank/mobile/business/cards/g;

    const/4 v3, 0x5

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "MASTERCARD_WEB"

    .line 15
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/4 v6, 0x6

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->g:Lcom/swedbank/mobile/business/cards/g;

    const/4 v3, 0x6

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "MASTERCARD_WEB_CL"

    .line 16
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/4 v5, 0x7

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->h:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "MAESTRO"

    .line 17
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x8

    const/4 v10, 0x0

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->i:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x8

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "MAESTRO_BASIC"

    .line 18
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x9

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->j:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x9

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "DEBIT_MASTERCARD"

    .line 19
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0xa

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->k:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0xa

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "DEBIT_MASTERCARD_CL"

    .line 20
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0xb

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->l:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "SENIOR"

    .line 21
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0xc

    const/4 v10, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->m:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0xc

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "PB_DEBIT_MASTERCARD"

    .line 22
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0xd

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->n:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0xd

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "PB_DEBIT_MASTERCARD_CL"

    .line 23
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0xe

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->o:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "SELF_DESIGN_2"

    .line 25
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0xf

    const/4 v10, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->p:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0xf

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "SELF_DESIGN_3"

    .line 26
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x10

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->q:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x10

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "PLATINUM_CREDIT"

    .line 27
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x11

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->r:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x11

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "NEW_PLATINUM_CREDIT"

    .line 28
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x12

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->s:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x12

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "NEW_PLATINUM_CREDIT_CL"

    .line 29
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x13

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->t:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "GOLD_CREDIT"

    .line 30
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x14

    const/4 v10, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->u:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x14

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "GOLD_CREDIT_CL"

    .line 31
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x15

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->v:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "VISA_PRIVATE_BANKING"

    .line 32
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x16

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->w:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x16

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "PLATINUM_CREDIT_2"

    .line 33
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x17

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->x:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x17

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "CLASSIC_CREDIT"

    .line 34
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x18

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->y:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x18

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "CLASSIC_CREDIT_CL"

    .line 35
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x19

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->z:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "GOLD_CREDIT_2"

    .line 36
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x1a

    const/4 v10, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->A:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x1a

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "GOLD_CREDIT_2_CL"

    .line 37
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x1b

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->B:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "FIXED_PAYMENT_CREDIT"

    .line 38
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x1c

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->C:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x1c

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "FIXED_PAYMENT_CREDIT_CL"

    .line 39
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x1d

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->D:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "BUSINESS_DEBIT"

    .line 41
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x1e

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->E:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x1e

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "BUSINESS_DEBIT_CL"

    .line 42
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->d:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x1f

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->F:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "BUSINESS_DEBIT_MAESTRO"

    .line 43
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->a:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x20

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->G:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x20

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v5, "BUSINESS_CREDIT"

    .line 45
    sget-object v7, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v6, 0x21

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->H:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x21

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "BUSINESS_CREDIT_CL"

    .line 46
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x22

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->I:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v7, "BUSINESS_GOLD"

    .line 47
    sget-object v9, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v8, 0x23

    const/4 v10, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->J:Lcom/swedbank/mobile/business/cards/g;

    const/16 v3, 0x23

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/g;

    const-string v3, "BUSINESS_GOLD_CL"

    .line 48
    sget-object v4, Lcom/swedbank/mobile/business/cards/d;->b:Lcom/swedbank/mobile/business/cards/d;

    const/16 v5, 0x24

    invoke-direct {v1, v3, v5, v4, v2}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/g;->K:Lcom/swedbank/mobile/business/cards/g;

    aput-object v1, v0, v5

    sput-object v0, Lcom/swedbank/mobile/business/cards/g;->L:[Lcom/swedbank/mobile/business/cards/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/d;",
            "Z)V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/swedbank/mobile/business/cards/g;->M:Lcom/swedbank/mobile/business/cards/d;

    iput-boolean p4, p0, Lcom/swedbank/mobile/business/cards/g;->N:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;ZILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/business/cards/g;-><init>(Ljava/lang/String;ILcom/swedbank/mobile/business/cards/d;Z)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/g;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/g;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/g;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/g;->L:[Lcom/swedbank/mobile/business/cards/g;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/g;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/cards/d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/g;->M:Lcom/swedbank/mobile/business/cards/d;

    return-object v0
.end method
