.class final Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;
.super Ljava/lang/Object;
.source "CardsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/onboarding/i;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$a;",
            ">;"
        }
    .end annotation

    const-string v0, "walletOnboardingCompletedStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;

    .line 146
    sget-object v0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$f;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$f;

    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 145
    sget-object v1, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$g;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$g;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "this\n      .filter { it \u2026smissOnboarding = true) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/s;

    .line 51
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;

    .line 152
    sget-object v2, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$h;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$h;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    const-wide/16 v3, 0x1

    .line 151
    invoke-virtual {v2, v3, v4}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v2

    .line 150
    new-instance v3, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;

    invoke-direct {v3, v1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$i;-><init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "this\n      .filter { it \u2026  }\n            }\n      }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    check-cast v1, Lio/reactivex/s;

    .line 52
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;

    .line 163
    sget-object v3, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$j;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$j;

    check-cast v3, Lio/reactivex/c/k;

    invoke-virtual {p1, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    .line 162
    new-instance v3, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$k;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$k;-><init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {p1, v3}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 161
    new-instance v3, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$l;-><init>(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {p1, v3}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v2, "this\n      .filter { it \u2026  }\n            }\n      }"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    check-cast p1, Lio/reactivex/s;

    .line 53
    iget-object v2, p0, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;->a:Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;->a(Lcom/swedbank/mobile/business/cards/CardsInteractorImpl;)Lcom/b/c/c;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 49
    invoke-static {v0, v1, p1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/CardsInteractorImpl$d;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
