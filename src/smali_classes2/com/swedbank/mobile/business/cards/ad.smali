.class public final Lcom/swedbank/mobile/business/cards/ad;
.super Ljava/lang/Object;
.source "StartWalletPayment.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lkotlin/k<",
        "Lcom/swedbank/mobile/business/cards/af;",
        "Ljava/lang/String;",
        ">;",
        "Lio/reactivex/b/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isWalletPreconditionsSatisfiedUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/ac;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "isWalletPreconditionsSatisfied"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/ad;->a:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/k;)Lio/reactivex/b/c;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/cards/af;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b/c;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pair"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/af;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 15
    iget-object v1, p0, Lcom/swedbank/mobile/business/cards/ad;->a:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/w;

    .line 16
    new-instance v2, Lcom/swedbank/mobile/business/cards/ad$a;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/business/cards/ad$a;-><init>(Lcom/swedbank/mobile/business/cards/af;Ljava/lang/String;)V

    check-cast v2, Lkotlin/e/a/b;

    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-static {v1, p1, v2, v0, p1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/ad;->a(Lkotlin/k;)Lio/reactivex/b/c;

    move-result-object p1

    return-object p1
.end method
