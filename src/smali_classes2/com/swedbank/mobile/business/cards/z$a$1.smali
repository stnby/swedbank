.class final Lcom/swedbank/mobile/business/cards/z$a$1;
.super Lkotlin/e/b/k;
.source "ReplenishPaymentTokens.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/cards/z$a;->a(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/a;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/cards/z$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/cards/z$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/cards/z$a$1;->a:Lcom/swedbank/mobile/business/cards/z$a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/a;)Z
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/business/cards/z$a$1;->a:Lcom/swedbank/mobile/business/cards/z$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/cards/z$a;->a:Lcom/swedbank/mobile/business/cards/z;

    .line 41
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object p1

    .line 42
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/s$a;

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    .line 43
    check-cast p1, Lcom/swedbank/mobile/business/cards/s$a;

    .line 44
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v0

    sget-object v2, Lcom/swedbank/mobile/business/cards/t;->e:Lcom/swedbank/mobile/business/cards/t;

    if-ne v0, v2, :cond_2

    .line 48
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/s$a;->d()Lcom/swedbank/mobile/business/cards/x;

    move-result-object p1

    .line 50
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/x$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/cards/x$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/x$a;->a()Z

    move-result p1

    goto :goto_0

    .line 51
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/cards/x$b;->a:Lcom/swedbank/mobile/business/cards/x$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    const/4 p1, 0x0

    .line 44
    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    .line 42
    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/cards/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/cards/z$a$1;->a(Lcom/swedbank/mobile/business/cards/a;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
