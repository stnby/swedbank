.class public final enum Lcom/swedbank/mobile/business/cards/CardLimitValidity;
.super Ljava/lang/Enum;
.source "Card.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/cards/CardLimitValidity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/cards/CardLimitValidity;

.field public static final enum DAY:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

.field public static final enum MONTH:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

.field public static final enum UNRECOGNIZED:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

.field public static final enum WEEK:Lcom/swedbank/mobile/business/cards/CardLimitValidity;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    const-string v2, "DAY"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->DAY:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    const-string v2, "WEEK"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->WEEK:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    const-string v2, "MONTH"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->MONTH:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    const-string v2, "UNRECOGNIZED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->UNRECOGNIZED:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->$VALUES:[Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 231
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/cards/CardLimitValidity;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->$VALUES:[Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/cards/CardLimitValidity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    return-object v0
.end method
