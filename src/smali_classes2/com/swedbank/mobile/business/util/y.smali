.class public final Lcom/swedbank/mobile/business/util/y;
.super Ljava/lang/Object;
.source "Types.kt"


# instance fields
.field private final a:J

.field private final b:Ljava/util/concurrent/TimeUnit;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "unit"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/swedbank/mobile/business/util/y;->a:J

    iput-object p3, p0, Lcom/swedbank/mobile/business/util/y;->b:Ljava/util/concurrent/TimeUnit;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .line 18
    iget-wide v0, p0, Lcom/swedbank/mobile/business/util/y;->a:J

    return-wide v0
.end method

.method public final b()Ljava/util/concurrent/TimeUnit;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/business/util/y;->b:Ljava/util/concurrent/TimeUnit;

    return-object v0
.end method
