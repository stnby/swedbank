.class final Lcom/swedbank/mobile/business/util/j$b;
.super Lkotlin/e/b/k;
.source "NodeMetadata.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Z)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/StringBuilder;

.field final synthetic b:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Ljava/lang/StringBuilder;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/util/j$b;->a:Ljava/lang/StringBuilder;

    iput-object p2, p0, Lcom/swedbank/mobile/business/util/j$b;->b:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    iput-boolean p3, p0, Lcom/swedbank/mobile/business/util/j$b;->c:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "node"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-boolean v0, p0, Lcom/swedbank/mobile/business/util/j$b;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/util/j$b;->a:Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/business/util/j$b;->a:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/util/j$b;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
