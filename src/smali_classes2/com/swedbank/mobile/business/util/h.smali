.class public final Lcom/swedbank/mobile/business/util/h;
.super Ljava/lang/Object;
.source "Dates.kt"

# interfaces
.implements Lkotlin/g/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/g/a<",
        "Lorg/threeten/bp/LocalDate;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/LocalDate;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lorg/threeten/bp/LocalDate;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V
    .locals 1
    .param p1    # Lorg/threeten/bp/LocalDate;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lorg/threeten/bp/LocalDate;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "start"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endInclusive"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/util/h;->a:Lorg/threeten/bp/LocalDate;

    iput-object p2, p0, Lcom/swedbank/mobile/business/util/h;->b:Lorg/threeten/bp/LocalDate;

    return-void
.end method


# virtual methods
.method public a()Lorg/threeten/bp/LocalDate;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/swedbank/mobile/business/util/h;->a:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Comparable;)Z
    .locals 0

    .line 8
    check-cast p1, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/util/h;->a(Lorg/threeten/bp/LocalDate;)Z

    move-result p1

    return p1
.end method

.method public a(Lorg/threeten/bp/LocalDate;)Z
    .locals 1
    .param p1    # Lorg/threeten/bp/LocalDate;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    check-cast p1, Ljava/lang/Comparable;

    invoke-static {p0, p1}, Lkotlin/g/a$a;->a(Lkotlin/g/a;Ljava/lang/Comparable;)Z

    move-result p1

    return p1
.end method

.method public synthetic b()Ljava/lang/Comparable;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/h;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public c()Lorg/threeten/bp/LocalDate;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/swedbank/mobile/business/util/h;->b:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public synthetic d()Ljava/lang/Comparable;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/h;->c()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method
