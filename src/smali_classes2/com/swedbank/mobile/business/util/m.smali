.class public final Lcom/swedbank/mobile/business/util/m;
.super Ljava/lang/Object;
.source "Optional.kt"


# direct methods
.method public static final a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/swedbank/mobile/business/util/l<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    if-nez p0, :cond_0

    .line 45
    sget-object p0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p0, Lcom/swedbank/mobile/business/util/l;

    goto :goto_0

    .line 46
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/business/util/n;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/util/n;-><init>(Ljava/lang/Object;)V

    move-object p0, v0

    check-cast p0, Lcom/swedbank/mobile/business/util/l;

    :goto_0
    return-object p0
.end method

.method public static final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 1
    .param p0    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/business/util/l<",
            "+TT;>;>;)",
            "Lio/reactivex/o<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$filterPresent"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    const-class v0, Lcom/swedbank/mobile/business/util/n;

    invoke-static {v0}, Lio/reactivex/d/b/a;->b(Ljava/lang/Class;)Lio/reactivex/c/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p0

    .line 106
    sget-object v0, Lcom/swedbank/mobile/business/util/o;->a:Lcom/swedbank/mobile/business/util/o;

    if-eqz v0, :cond_0

    check-cast v0, Lio/reactivex/c/h;

    .line 62
    invoke-virtual {p0, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p0

    const-string v0, "this\n    .filter(Functio\u2026presentOptionalToValue())"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 106
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.functions.Function<com.swedbank.mobile.business.util.Optional<T>, T>"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/a;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/swedbank/mobile/business/util/l<",
            "+TT;>;",
            "Lkotlin/e/a/a<",
            "+",
            "Ljava/lang/Object;",
            ">;)TT;"
        }
    .end annotation

    const-string v0, "$this$getOrThrow"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyMessage"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    instance-of p1, p0, Lcom/swedbank/mobile/business/util/n;

    if-eqz p1, :cond_0

    check-cast p0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p0

    return-object p0

    .line 104
    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 32
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static synthetic a(Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/a;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 31
    sget-object p1, Lcom/swedbank/mobile/business/util/m$a;->a:Lcom/swedbank/mobile/business/util/m$a;

    check-cast p1, Lkotlin/e/a/a;

    :cond_0
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/util/m;->a(Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/a;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
