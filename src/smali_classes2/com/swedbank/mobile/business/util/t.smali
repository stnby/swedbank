.class public final Lcom/swedbank/mobile/business/util/t;
.super Ljava/lang/Object;
.source "Routers.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Z
    .locals 2
    .param p0    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$isEquivalentTo"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    instance-of v0, p0, Lcom/swedbank/mobile/business/util/c;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/swedbank/mobile/business/util/c;

    if-eqz v0, :cond_0

    .line 70
    check-cast p0, Lcom/swedbank/mobile/business/util/c;

    invoke-interface {p0}, Lcom/swedbank/mobile/business/util/c;->a()Ljava/lang/String;

    move-result-object p0

    check-cast p1, Lcom/swedbank/mobile/business/util/c;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/c;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method
