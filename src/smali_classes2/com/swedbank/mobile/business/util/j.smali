.class public final Lcom/swedbank/mobile/business/util/j;
.super Ljava/lang/Object;
.source "NodeMetadata.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "$this$firstPushedChild"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-static {p0}, Lkotlin/a/h;->f(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/lang/String;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "$this$find"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeWithId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/util/j$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/j$a;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "$this$first"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predicate"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-interface {p1, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->d()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_2

    check-cast p0, Ljava/lang/Iterable;

    .line 44
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    .line 38
    invoke-static {v0, p1}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Z)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$pathToRoot"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    new-instance v1, Lcom/swedbank/mobile/business/util/j$b;

    invoke-direct {v1, v0, p0, p1}, Lcom/swedbank/mobile/business/util/j$b;-><init>(Ljava/lang/StringBuilder;Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Z)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {p0, v1}, Lcom/swedbank/mobile/business/util/j;->b(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)V

    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "StringBuilder()\n    .app\u2026  }\n    }\n    .toString()"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final b(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)V
    .locals 1
    .param p0    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->c()Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v0

    if-nez v0, :cond_0

    .line 23
    invoke-interface {p1, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 26
    :cond_0
    invoke-static {v0, p1}, Lcom/swedbank/mobile/business/util/j;->b(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)V

    .line 27
    invoke-interface {p1, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
