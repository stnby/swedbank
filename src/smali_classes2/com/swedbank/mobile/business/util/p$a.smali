.class public final Lcom/swedbank/mobile/business/util/p$a;
.super Lcom/swedbank/mobile/business/util/p;
.source "Network.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/util/s;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/util/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/util/p;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/util/p$a;->a:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 14
    new-instance p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/swedbank/mobile/business/util/e$b;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/business/util/p$a;->a:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetworkError(error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
