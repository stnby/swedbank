.class public final Lcom/swedbank/mobile/business/util/b;
.super Ljava/lang/Object;
.source "Collections.kt"


# direct methods
.method public static final varargs a([Lkotlin/k;)Ljava/util/Map;
    .locals 5
    .param p0    # [Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([",
            "Lkotlin/k<",
            "+TK;+TV;>;)",
            "Ljava/util/Map<",
            "TK;TV;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pairs"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 20
    new-instance v0, Landroidx/c/a;

    array-length v1, p0

    invoke-direct {v0, v1}, Landroidx/c/a;-><init>(I)V

    .line 85
    array-length v1, p0

    const/4 v2, 0x0

    goto :goto_0

    .line 16
    :pswitch_0
    invoke-static {p0}, Lkotlin/a/b;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/k;

    invoke-virtual {p0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p0

    .line 17
    invoke-static {v0, p0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    const-string v0, "Collections.singletonMap(key, value)"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 14
    :pswitch_1
    invoke-static {}, Lkotlin/a/x;->a()Ljava/util/Map;

    move-result-object p0

    goto :goto_1

    :goto_0
    if-ge v2, v1, :cond_0

    .line 85
    aget-object v3, p0, v2

    invoke-virtual {v3}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v3

    .line 21
    invoke-virtual {v0, v4, v3}, Landroidx/c/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 22
    :cond_0
    move-object p0, v0

    check-cast p0, Ljava/util/Map;

    :goto_1
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final varargs a([Ljava/lang/Object;)Ljava/util/Set;
    .locals 4
    .param p0    # [Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "Ljava/util/Set<",
            "TE;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "elements"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    array-length v0, p0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 30
    array-length v0, p0

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    .line 87
    new-instance v0, Landroidx/c/b;

    array-length v2, p0

    invoke-direct {v0, v2}, Landroidx/c/b;-><init>(I)V

    .line 89
    array-length v2, p0

    goto :goto_0

    .line 29
    :pswitch_0
    aget-object p0, p0, v1

    invoke-static {p0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    const-string v0, "Collections.singleton(elements[0])"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 28
    :pswitch_1
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object p0

    goto :goto_1

    :goto_0
    if-ge v1, v2, :cond_0

    .line 89
    aget-object v3, p0, v1

    .line 88
    invoke-virtual {v0, v3}, Landroidx/c/b;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    :cond_0
    move-object p0, v0

    check-cast p0, Ljava/util/Set;

    :goto_1
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
