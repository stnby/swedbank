.class final Lcom/swedbank/mobile/business/g/a$a;
.super Ljava/lang/Object;
.source "GetJumpToIbankLink.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/g/a;->a(Lcom/swedbank/mobile/business/e;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/g/a;

.field final synthetic b:Lcom/swedbank/mobile/business/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/g/a;Lcom/swedbank/mobile/business/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/g/a$a;->a:Lcom/swedbank/mobile/business/g/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/g/a$a;->b:Lcom/swedbank/mobile/business/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/g/a$a;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/business/authentication/session/h;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/g/a$a;->a:Lcom/swedbank/mobile/business/g/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/g/a;->a(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/swedbank/mobile/business/g/a$a;->b:Lcom/swedbank/mobile/business/e;

    .line 35
    iget-object v2, p0, Lcom/swedbank/mobile/business/g/a$a;->a:Lcom/swedbank/mobile/business/g/a;

    invoke-static {v2}, Lcom/swedbank/mobile/business/g/a;->b(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v2

    .line 80
    invoke-interface {v2}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v2

    sget-object v3, Lcom/swedbank/mobile/business/g/b;->a:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/g;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 86
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Ibank language code is unknown for country UNKNOWN"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_1
    const-string v2, "LIT"

    goto :goto_0

    :pswitch_2
    const-string v2, "LAT"

    goto :goto_0

    :pswitch_3
    const-string v2, "EST"

    goto :goto_0

    :pswitch_4
    const-string v2, "RUS"

    goto :goto_0

    :pswitch_5
    const-string v2, "ENG"

    .line 88
    :goto_0
    invoke-static {v0}, Lokhttp3/t;->f(Ljava/lang/String;)Lokhttp3/t;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 103
    invoke-virtual {v0}, Lokhttp3/t;->o()Lokhttp3/t$a;

    move-result-object v0

    .line 102
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/e;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lokhttp3/t$a;->e(Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v0

    const-string v3, "language"

    .line 101
    invoke-virtual {v0, v3, v2}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v0

    .line 107
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/e;->b()Ljava/util/Map;

    move-result-object v1

    .line 111
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 109
    invoke-virtual {v0, v3, v2}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    goto :goto_1

    .line 99
    :cond_0
    invoke-virtual {v0}, Lokhttp3/t$a;->c()Lokhttp3/t;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lokhttp3/t;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "requireNotNull(HttpUrl.p\u2026 .build()\n    .toString()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/business/g/a$a;->a:Lcom/swedbank/mobile/business/g/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/g/a;->c(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v1

    const-string v2, "feature_app_to_ibank"

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 38
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    :goto_2
    if-ne v1, v2, :cond_3

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/business/g/a$a;->a:Lcom/swedbank/mobile/business/g/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/g/a;->a(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/business/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokhttp3/t;->f(Ljava/lang/String;)Lokhttp3/t;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 42
    invoke-virtual {v1}, Lokhttp3/t;->o()Lokhttp3/t$a;

    move-result-object v1

    const-string v2, "auth/oauth/v2/authorize"

    .line 43
    invoke-virtual {v1, v2}, Lokhttp3/t$a;->e(Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v1

    const-string v2, "response_type"

    const-string v3, "code"

    .line 44
    invoke-virtual {v1, v2, v3}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v1

    const-string v2, "client_id"

    const-string v3, "ibank"

    .line 45
    invoke-virtual {v1, v2, v3}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v1

    const-string v2, "redirect_uri"

    .line 46
    invoke-virtual {v1, v2, v0}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v0

    const-string v1, "scope"

    const-string v2, "openid login_id_token"

    .line 47
    invoke-virtual {v0, v1, v2}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v0

    const-string v1, "id_token_hint"

    .line 48
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/h$a;->b()Lcom/swedbank/mobile/business/authentication/session/o;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/session/o;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lokhttp3/t$a;->c()Lokhttp3/t;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lokhttp3/t;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 40
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_3
    :goto_3
    return-object v0

    .line 88
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Malformed ibank base url"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
