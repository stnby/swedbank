.class public final Lcom/swedbank/mobile/business/g/c;
.super Ljava/lang/Object;
.source "GetJumpToIbankLink_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/g/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/swedbank/mobile/business/g/c;->a:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/swedbank/mobile/business/g/c;->b:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/swedbank/mobile/business/g/c;->c:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/swedbank/mobile/business/g/c;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/g/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;)",
            "Lcom/swedbank/mobile/business/g/c;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/swedbank/mobile/business/g/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/business/g/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/g/a;
    .locals 5

    .line 36
    new-instance v0, Lcom/swedbank/mobile/business/g/a;

    iget-object v1, p0, Lcom/swedbank/mobile/business/g/c;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/b;

    iget-object v2, p0, Lcom/swedbank/mobile/business/g/c;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/c/f;

    iget-object v3, p0, Lcom/swedbank/mobile/business/g/c;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/c/a;

    iget-object v4, p0, Lcom/swedbank/mobile/business/g/c;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/f/a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/business/g/a;-><init>(Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/c/f;Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/business/f/a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/g/c;->a()Lcom/swedbank/mobile/business/g/a;

    move-result-object v0

    return-object v0
.end method
