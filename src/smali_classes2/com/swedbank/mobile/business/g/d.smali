.class public final Lcom/swedbank/mobile/business/g/d;
.super Ljava/lang/Object;
.source "GetNotAuthJumpToIbankLink.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/e;",
        "Lio/reactivex/w<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/c/f;

.field private final b:Lcom/swedbank/mobile/business/c/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/f;Lcom/swedbank/mobile/business/c/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "endpointConfiguration"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/g/d;->a:Lcom/swedbank/mobile/business/c/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/g/d;->b:Lcom/swedbank/mobile/business/c/a;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/e;)Lio/reactivex/w;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "urlComponents"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/business/g/d;->a:Lcom/swedbank/mobile/business/c/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/f;->g()Ljava/lang/String;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/swedbank/mobile/business/g/d;->b:Lcom/swedbank/mobile/business/c/a;

    .line 21
    invoke-interface {v1}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/business/g/b;->a:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/g;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 27
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Ibank language code is unknown for country UNKNOWN"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_1
    const-string v1, "LIT"

    goto :goto_0

    :pswitch_2
    const-string v1, "LAT"

    goto :goto_0

    :pswitch_3
    const-string v1, "EST"

    goto :goto_0

    :pswitch_4
    const-string v1, "RUS"

    goto :goto_0

    :pswitch_5
    const-string v1, "ENG"

    .line 29
    :goto_0
    invoke-static {v0}, Lokhttp3/t;->f(Ljava/lang/String;)Lokhttp3/t;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {v0}, Lokhttp3/t;->o()Lokhttp3/t$a;

    move-result-object v0

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lokhttp3/t$a;->e(Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v0

    const-string v2, "language"

    .line 42
    invoke-virtual {v0, v2, v1}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    move-result-object v0

    .line 48
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e;->b()Ljava/util/Map;

    move-result-object p1

    .line 52
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v2, v1}, Lokhttp3/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/t$a;

    goto :goto_1

    .line 40
    :cond_0
    invoke-virtual {v0}, Lokhttp3/t$a;->c()Lokhttp3/t;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lokhttp3/t;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "requireNotNull(HttpUrl.p\u2026 .build()\n    .toString()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 29
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Malformed ibank base url"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/g/d;->a(Lcom/swedbank/mobile/business/e;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
