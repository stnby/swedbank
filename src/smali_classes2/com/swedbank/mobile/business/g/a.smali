.class public final Lcom/swedbank/mobile/business/g/a;
.super Ljava/lang/Object;
.source "GetJumpToIbankLink.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/e;",
        "Lio/reactivex/w<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/c/f;

.field private final c:Lcom/swedbank/mobile/business/c/a;

.field private final d:Lcom/swedbank/mobile/business/f/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/c/f;Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/business/f/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "refreshSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/c/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ">;>;",
            "Lcom/swedbank/mobile/business/c/f;",
            "Lcom/swedbank/mobile/business/c/a;",
            "Lcom/swedbank/mobile/business/f/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "refreshSession"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endpointConfiguration"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/g/a;->a:Lcom/swedbank/mobile/architect/business/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/g/a;->b:Lcom/swedbank/mobile/business/c/f;

    iput-object p3, p0, Lcom/swedbank/mobile/business/g/a;->c:Lcom/swedbank/mobile/business/c/a;

    iput-object p4, p0, Lcom/swedbank/mobile/business/g/a;->d:Lcom/swedbank/mobile/business/f/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/c/f;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/business/g/a;->b:Lcom/swedbank/mobile/business/c/f;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/business/g/a;->c:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/g/a;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/business/g/a;->d:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/e;)Lio/reactivex/w;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "urlComponents"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/g/a;->a:Lcom/swedbank/mobile/architect/business/b;

    new-instance v7, Lcom/swedbank/mobile/business/authentication/session/refresh/a;

    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/business/authentication/session/refresh/a;-><init>(ZJILkotlin/e/b/g;)V

    invoke-interface {v0, v7}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 31
    new-instance v1, Lcom/swedbank/mobile/business/g/a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/g/a$a;-><init>(Lcom/swedbank/mobile/business/g/a;Lcom/swedbank/mobile/business/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "refreshSession(RefreshRe\u2026bankUrl\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/business/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/g/a;->a(Lcom/swedbank/mobile/business/e;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
