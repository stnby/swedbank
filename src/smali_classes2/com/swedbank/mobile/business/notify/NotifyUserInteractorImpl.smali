.class public final Lcom/swedbank/mobile/business/notify/NotifyUserInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "NotifyUserInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/notify/a;
.implements Lcom/swedbank/mobile/business/notify/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/notify/g;",
        ">;",
        "Lcom/swedbank/mobile/business/notify/a;",
        "Lcom/swedbank/mobile/business/notify/f;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/notify/c;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/notify/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/notify/c;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/notify/NotifyUserInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/swedbank/mobile/business/notify/NotifyUserInteractorImpl$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/notify/NotifyUserInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/notify/c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow()\n      .flatMapMay\u2026flowToNotifyUser(input) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tag"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/notify/NotifyUserInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/notify/g;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/notify/g;->a(Ljava/lang/String;)V

    return-void
.end method
