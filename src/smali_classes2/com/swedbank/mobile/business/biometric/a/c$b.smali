.class final Lcom/swedbank/mobile/business/biometric/a/c$b;
.super Ljava/lang/Object;
.source "SignPaymentWithBiometrics.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/a/c;->a(Lcom/swedbank/mobile/business/biometric/a/a;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/biometric/a/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/biometric/a/c$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/biometric/a/c$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/biometric/a/c$b;->a:Lcom/swedbank/mobile/business/biometric/a/c$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/authentication/m;)Lcom/swedbank/mobile/business/biometric/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/m$c;->a:Lcom/swedbank/mobile/business/biometric/authentication/m$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/business/biometric/a/b$b;->a:Lcom/swedbank/mobile/business/biometric/a/b$b;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/a/b;

    goto :goto_1

    .line 33
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/biometric/a/b$a;

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$b;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 33
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/biometric/a/b$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/a/b;

    goto :goto_1

    .line 35
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    if-eqz v0, :cond_3

    .line 36
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$a;->a()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 35
    new-instance v0, Lcom/swedbank/mobile/business/biometric/a/b$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/biometric/a/b$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/a/b;

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/a/c$b;->a(Lcom/swedbank/mobile/business/biometric/authentication/m;)Lcom/swedbank/mobile/business/biometric/a/b;

    move-result-object p1

    return-object p1
.end method
