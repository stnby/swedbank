.class final Lcom/swedbank/mobile/business/biometric/a/c$a;
.super Ljava/lang/Object;
.source "SignPaymentWithBiometrics.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/a/c;->a(Lcom/swedbank/mobile/business/biometric/a/a;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/a/c;

.field final synthetic b:Lcom/swedbank/mobile/business/biometric/a/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/a/c;Lcom/swedbank/mobile/business/biometric/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/a/c$a;->a:Lcom/swedbank/mobile/business/biometric/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/a/c$a;->b:Lcom/swedbank/mobile/business/biometric/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/authentication/m;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "mobileAppId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/a/c$a;->a:Lcom/swedbank/mobile/business/biometric/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/a/c;->a(Lcom/swedbank/mobile/business/biometric/a/c;)Lcom/swedbank/mobile/business/biometric/authentication/k;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/a/c$a;->b:Lcom/swedbank/mobile/business/biometric/a/a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/biometric/a/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/a/c$a;->b:Lcom/swedbank/mobile/business/biometric/a/a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/biometric/a/a;->b()Ljava/lang/String;

    move-result-object v2

    .line 28
    iget-object v3, p0, Lcom/swedbank/mobile/business/biometric/a/c$a;->b:Lcom/swedbank/mobile/business/biometric/a/a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/biometric/a/a;->c()Ljava/security/Signature;

    move-result-object v3

    .line 24
    invoke-interface {v0, p1, v1, v2, v3}, Lcom/swedbank/mobile/business/biometric/authentication/k;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/a/c$a;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
