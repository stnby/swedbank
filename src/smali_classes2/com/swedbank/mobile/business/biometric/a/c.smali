.class public final Lcom/swedbank/mobile/business/biometric/a/c;
.super Ljava/lang/Object;
.source "SignPaymentWithBiometrics.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/biometric/a/a;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/biometric/a/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/authentication/k;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/k;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getMobileAppIdUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/authentication/k;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricAuthorizationRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getMobileAppId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/a/c;->a:Lcom/swedbank/mobile/business/biometric/authentication/k;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/a/c;->b:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/a/c;)Lcom/swedbank/mobile/business/biometric/authentication/k;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/a/c;->a:Lcom/swedbank/mobile/business/biometric/authentication/k;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/biometric/a/a;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/biometric/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/a/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/a/c;->b:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/biometric/a/c$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/biometric/a/c$a;-><init>(Lcom/swedbank/mobile/business/biometric/a/c;Lcom/swedbank/mobile/business/biometric/a/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 30
    sget-object v0, Lcom/swedbank/mobile/business/biometric/a/c$b;->a:Lcom/swedbank/mobile/business/biometric/a/c$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 39
    sget-object v0, Lcom/swedbank/mobile/business/biometric/a/c$c;->a:Lcom/swedbank/mobile/business/biometric/a/c$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "getMobileAppId()\n       \u2026iled(error = it.left()) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/biometric/a/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/a/c;->a(Lcom/swedbank/mobile/business/biometric/a/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
