.class public final Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;
.super Lkotlin/e/b/k;
.source "BiometricLoginInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/authentication/k;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

.field final synthetic b:J


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;J)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

    iput-wide p2, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->b:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/k;)V
    .locals 5

    .line 72
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/k$a;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/e;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->b:J

    sub-long/2addr v0, v2

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/business/biometric/login/e;->a(J)V

    goto/16 :goto_1

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/k$c;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/biometric/login/f;->c()V

    goto/16 :goto_1

    .line 74
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/k$b;

    if-eqz v0, :cond_5

    .line 75
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/authentication/k$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/k$b;->b()Ljava/lang/Throwable;

    move-result-object v0

    .line 76
    instance-of v1, v0, Ljava/security/GeneralSecurityException;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

    sget-object v0, Lcom/swedbank/mobile/business/biometric/login/g$a;->a:Lcom/swedbank/mobile/business/biometric/login/g$a;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/login/g;

    .line 119
    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/e;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/biometric/login/e;->a(Lcom/swedbank/mobile/business/biometric/login/g;)V

    .line 120
    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object v0

    .line 127
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 126
    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026       .andThen(logOut())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/l;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/login/l;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 128
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_1

    .line 77
    :cond_2
    instance-of v1, v0, Lcom/swedbank/mobile/business/biometric/authentication/n;

    if-eqz v1, :cond_3

    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/g$b;

    .line 78
    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/authentication/n;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/biometric/login/g$b;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/g;

    .line 131
    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/login/e;->a(Lcom/swedbank/mobile/business/biometric/login/g;)V

    .line 132
    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object v0

    .line 139
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 138
    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026       .andThen(logOut())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/l;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/login/l;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 140
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_1

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a:Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/swedbank/mobile/business/biometric/login/a$d;

    if-eqz v0, :cond_4

    .line 82
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_0

    .line 83
    :cond_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/k;->a()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 81
    :goto_0
    invoke-direct {v2, v0}, Lcom/swedbank/mobile/business/biometric/login/a$d;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v2, Lcom/swedbank/mobile/business/biometric/login/a;

    .line 80
    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/biometric/login/f;->a(Lcom/swedbank/mobile/business/biometric/login/a;)V

    :cond_5
    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/swedbank/mobile/business/authentication/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;->a(Lcom/swedbank/mobile/business/authentication/k;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
