.class public final Lcom/swedbank/mobile/business/biometric/login/method/b;
.super Ljava/lang/Object;
.source "BiometricLoginMethodInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/method/b;->a:Ljavax/inject/Provider;

    .line 18
    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/login/method/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/login/method/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            ">;)",
            "Lcom/swedbank/mobile/business/biometric/login/method/b;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/swedbank/mobile/business/biometric/login/method/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/biometric/login/method/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;
    .locals 3

    .line 23
    new-instance v0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/login/method/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/util/l;

    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/login/method/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;-><init>(Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/biometric/login/f;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/method/b;->a()Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;

    move-result-object v0

    return-object v0
.end method
