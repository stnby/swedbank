.class public final Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BiometricLoginMethodInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/h;
.implements Lcom/swedbank/mobile/business/biometric/login/f;
.implements Lcom/swedbank/mobile/business/biometric/login/method/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/login/method/c;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/authentication/h;",
        "Lcom/swedbank/mobile/business/biometric/login/f;",
        "Lcom/swedbank/mobile/business/biometric/login/method/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/biometric/login/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/biometric/login/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "logged_in_customer_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/login/f;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_login_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loggedInCustomerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricLoginStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->a:Lcom/swedbank/mobile/business/util/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->a()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/login/method/c;

    .line 38
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/p$a;

    if-eqz v1, :cond_0

    .line 39
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/biometric/login/method/c;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    .line 40
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/method/c;->a()V

    goto :goto_0

    .line 42
    :cond_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/authentication/p$c;

    if-eqz v1, :cond_4

    .line 43
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/method/c;->a()V

    .line 44
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/biometric/login/method/c;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    .line 45
    check-cast p1, Lcom/swedbank/mobile/business/authentication/p$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/p$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 65
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/p$b;

    .line 54
    sget-object v0, Lcom/swedbank/mobile/business/authentication/p$b;->b:Lcom/swedbank/mobile/business/authentication/p$b;

    if-ne p1, v0, :cond_1

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->g()V

    .line 57
    :cond_1
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    new-instance v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/e$b;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/swedbank/mobile/business/util/e;

    goto :goto_0

    .line 66
    :cond_2
    instance-of p1, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz p1, :cond_3

    :goto_0
    return-void

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 67
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public a(Lcom/swedbank/mobile/business/biometric/login/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/biometric/login/f;->a(Lcom/swedbank/mobile/business/biometric/login/a;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->c()V

    return-void
.end method

.method public e()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->e()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public f()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->f()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/login/method/c;

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->a:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/login/method/c;->a(Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method public i()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->c()V

    return-void
.end method

.method protected m_()V
    .locals 0

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/method/BiometricLoginMethodInteractorImpl;->g()V

    return-void
.end method
