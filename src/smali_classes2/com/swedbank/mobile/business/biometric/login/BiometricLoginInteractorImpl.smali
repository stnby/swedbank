.class public final Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BiometricLoginInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/login/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/login/e;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/login/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/d;

.field private final b:Lcom/swedbank/mobile/business/authentication/p;

.field private final c:Lcom/swedbank/mobile/business/authentication/login/l;

.field private final d:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/biometric/login/f;

.field private final f:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/security/Signature;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/authentication/p;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/biometric/login/f;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_result"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/authentication/login/l;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_login_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "logOutUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/biometric/login/f;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_login_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "authenticateWithBiometricsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Lcom/swedbank/mobile/business/authentication/p;",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/business/biometric/login/f;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/security/Signature;",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationResult"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logOut"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricLoginStream"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticateWithBiometrics"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a:Lcom/swedbank/mobile/business/biometric/d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/p;

    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/l;

    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    iput-object p5, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e:Lcom/swedbank/mobile/business/biometric/login/f;

    iput-object p6, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/l;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c:Lcom/swedbank/mobile/business/authentication/login/l;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->f:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e:Lcom/swedbank/mobile/business/biometric/login/f;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/e;
    .locals 0

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/login/e;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a:Lcom/swedbank/mobile/business/biometric/d;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 10

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/login/f;->b()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 54
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e:Lcom/swedbank/mobile/business/biometric/login/f;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/biometric/login/f;->f()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 52
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 55
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "Observable.merge(\n      \u2026rrors())\n        .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 119
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->b:Lcom/swedbank/mobile/business/authentication/p;

    .line 122
    instance-of v1, v0, Lcom/swedbank/mobile/business/authentication/p$a;

    if-eqz v1, :cond_0

    .line 123
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 124
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->b(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v3

    check-cast v0, Lcom/swedbank/mobile/business/authentication/p$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$a;->a()Ljava/security/Signature;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    .line 128
    new-instance v3, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$b;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v0, v3}, Lio/reactivex/o;->b(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 127
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/swedbank/mobile/business/biometric/login/f;->b()Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    invoke-virtual {v0, v3}, Lio/reactivex/o;->h(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v4

    const-string v0, "authenticateWithBiometri\u2026eam.observeLoginCancel())"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;

    invoke-direct {v0, p0, v1, v2}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;J)V

    move-object v7, v0

    check-cast v7, Lkotlin/e/a/b;

    const/4 v6, 0x0

    .line 132
    new-instance v0, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v8, 0x2

    const/4 v9, 0x0

    .line 126
    invoke-static/range {v4 .. v9}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 133
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto/16 :goto_0

    .line 135
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/authentication/p$c;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/swedbank/mobile/business/authentication/p$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 137
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/p$b;

    .line 138
    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/b;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 155
    :pswitch_0
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/l;->j()V

    goto/16 :goto_0

    .line 154
    :pswitch_1
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/biometric/login/a$c;-><init>(Z)V

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/login/f;->a(Lcom/swedbank/mobile/business/biometric/login/a;)V

    goto/16 :goto_0

    .line 153
    :pswitch_2
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/biometric/login/a$c;-><init>(Z)V

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/login/f;->a(Lcom/swedbank/mobile/business/biometric/login/a;)V

    goto/16 :goto_0

    .line 152
    :pswitch_3
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/f;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/a$b;->b:Lcom/swedbank/mobile/business/biometric/login/a$b;

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/login/f;->a(Lcom/swedbank/mobile/business/biometric/login/a;)V

    goto/16 :goto_0

    .line 139
    :pswitch_4
    sget-object v0, Lcom/swedbank/mobile/business/biometric/login/g$a;->a:Lcom/swedbank/mobile/business/biometric/login/g$a;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/login/g;

    .line 140
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/e;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/biometric/login/e;->a(Lcom/swedbank/mobile/business/biometric/login/g;)V

    .line 141
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object v0

    .line 148
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 147
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026       .andThen(logOut())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;

    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/l;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/login/l;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 149
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_0

    .line 158
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/e/l;

    .line 159
    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/g$c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/biometric/login/g$c;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/g;

    .line 160
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/login/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/login/e;->a(Lcom/swedbank/mobile/business/biometric/login/g;)V

    .line 161
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->e(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object v0

    .line 168
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 167
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026       .andThen(logOut())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;

    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/l;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/swedbank/mobile/business/biometric/login/BiometricLoginInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/authentication/login/l;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 169
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :goto_0
    return-void

    .line 159
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 172
    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
