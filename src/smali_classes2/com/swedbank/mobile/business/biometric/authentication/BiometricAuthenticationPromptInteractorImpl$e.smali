.class final Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptInteractor.kt"

# interfaces
.implements Lio/reactivex/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->a()Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/m<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/k;)V
    .locals 4
    .param p1    # Lio/reactivex/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/k<",
            "Ljava/security/Signature;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->b(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/g;

    move-result-object v2

    .line 106
    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 50
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/login/g;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 107
    :cond_0
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_3

    check-cast v1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v1

    .line 104
    :goto_0
    check-cast v1, Ljava/lang/String;

    .line 50
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/d/a;->i(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 111
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Signature;

    .line 58
    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 112
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/h;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$c;

    sget-object v2, Lcom/swedbank/mobile/business/authentication/p$b;->a:Lcom/swedbank/mobile/business/authentication/p$b;

    invoke-static {v2}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/authentication/p$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/p;

    .line 53
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/authentication/h;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    .line 56
    invoke-interface {p1}, Lio/reactivex/k;->c()V

    :goto_1
    return-void

    .line 57
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 108
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
