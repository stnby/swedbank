.class public final Lcom/swedbank/mobile/business/biometric/authentication/a;
.super Ljava/lang/Object;
.source "AuthenticateWithBiometrics.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Ljava/security/Signature;",
        "Lio/reactivex/o<",
        "Lcom/swedbank/mobile/business/authentication/k;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/general/a;

.field private final b:Lcom/swedbank/mobile/business/authentication/login/g;

.field private final c:Lcom/swedbank/mobile/business/h/a;

.field private final d:Lcom/swedbank/mobile/business/authentication/l;

.field private final e:Lcom/swedbank/mobile/business/biometric/authentication/j;

.field private final f:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/g;Lcom/swedbank/mobile/business/h/a;Lcom/swedbank/mobile/business/authentication/l;Lcom/swedbank/mobile/business/biometric/authentication/j;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/h/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/authentication/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/biometric/authentication/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "saveSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "acquireScopedSessionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            "Lcom/swedbank/mobile/business/h/a;",
            "Lcom/swedbank/mobile/business/authentication/l;",
            "Lcom/swedbank/mobile/business/biometric/authentication/j;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loginCredentialsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localServiceOrderingRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "saveSession"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "acquireScopedSession"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->b:Lcom/swedbank/mobile/business/authentication/login/g;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->c:Lcom/swedbank/mobile/business/h/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->d:Lcom/swedbank/mobile/business/authentication/l;

    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->e:Lcom/swedbank/mobile/business/biometric/authentication/j;

    iput-object p5, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->f:Lcom/swedbank/mobile/architect/business/b;

    iput-object p6, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->g:Lcom/swedbank/mobile/architect/business/b;

    .line 32
    new-instance p1, Lcom/swedbank/mobile/business/general/a;

    .line 33
    new-instance p2, Lcom/swedbank/mobile/business/util/y;

    sget-object p3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p4, 0x0

    invoke-direct {p2, p4, p5, p3}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 34
    new-instance p3, Lcom/swedbank/mobile/business/util/y;

    sget-object p4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p5, 0x1

    invoke-direct {p3, p5, p6, p4}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 32
    invoke-direct {p1, p2, p3}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->a:Lcom/swedbank/mobile/business/general/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/authentication/login/g;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->b:Lcom/swedbank/mobile/business/authentication/login/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/biometric/authentication/j;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->e:Lcom/swedbank/mobile/business/biometric/authentication/j;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/general/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->a:Lcom/swedbank/mobile/business/general/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/authentication/l;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->d:Lcom/swedbank/mobile/business/authentication/l;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->g:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->f:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/security/Signature;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/Signature;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->c:Lcom/swedbank/mobile/business/h/a;

    .line 38
    invoke-interface {v1}, Lcom/swedbank/mobile/business/h/a;->a()Lio/reactivex/j;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Lio/reactivex/j;->c()Lio/reactivex/w;

    move-result-object v1

    const-string v2, "localServiceOrderingRepo\u2026d()\n          .toSingle()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/aa;

    .line 40
    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/authentication/a;->d:Lcom/swedbank/mobile/business/authentication/l;

    .line 41
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/l;->a()Lio/reactivex/w;

    move-result-object v2

    check-cast v2, Lio/reactivex/aa;

    .line 36
    invoke-virtual {v0, v1, v2}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/swedbank/mobile/business/biometric/authentication/a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/a$a;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/a;Ljava/security/Signature;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 53
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/a$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/authentication/a$b;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 70
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/a$c;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$c;

    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    .line 71
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/a$d;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->j(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Singles.zip(\n      local\u2026(causingException = it) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/security/Signature;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/a;->a(Ljava/security/Signature;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
