.class final Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/biometric/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;Lcom/swedbank/mobile/business/biometric/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;->b:Lcom/swedbank/mobile/business/biometric/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;)",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "rootReason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    new-instance p1, Lcom/swedbank/mobile/business/authentication/p$a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;->b:Lcom/swedbank/mobile/business/biometric/a;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/a$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/a$b;->a()Ljava/security/Signature;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/authentication/p$a;-><init>(Ljava/security/Signature;)V

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    .line 106
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/e/l;

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->e(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/i;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/g$c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/biometric/login/g$c;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/g;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/authentication/i;->a(Lcom/swedbank/mobile/business/biometric/login/g;)V

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object v0

    .line 77
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$c;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/authentication/p$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;->a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
