.class public final Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BiometricAuthenticationPromptInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/authentication/i;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/authentication/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/d/a;

.field private final b:Lcom/swedbank/mobile/business/biometric/d;

.field private final c:Lcom/swedbank/mobile/business/e/b;

.field private final d:Lcom/swedbank/mobile/business/authentication/login/g;

.field private final e:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/business/biometric/authentication/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/authentication/login/g;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/biometric/authentication/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "isDeviceRootedUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_prompt_key_base_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_prompt_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/d/a;",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cryptoRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginCredentialsRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isDeviceRooted"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keyBaseName"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->a:Lcom/swedbank/mobile/business/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->c:Lcom/swedbank/mobile/business/e/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->d:Lcom/swedbank/mobile/business/authentication/login/g;

    iput-object p5, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    iput-object p6, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->f:Lcom/swedbank/mobile/business/util/l;

    iput-object p7, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->a:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/util/l;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->f:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/authentication/login/g;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->d:Lcom/swedbank/mobile/business/authentication/login/g;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/h;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/i;
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/authentication/i;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Ljava/security/Signature;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026::onSuccess\n        )\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/biometric/a;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/biometric/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/a$b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 70
    new-instance v3, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$a;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v0, v3}, Lio/reactivex/w;->a(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object v0

    .line 71
    new-instance v3, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;

    invoke-direct {v3, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;Lcom/swedbank/mobile/business/biometric/a;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v0, v3}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "isDeviceRooted()\n       \u2026            )\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$c;

    iget-object v3, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-direct {v0, v3}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/h;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 104
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_1

    .line 84
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/a$a;

    if-eqz v0, :cond_1

    .line 85
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/biometric/a$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/a$a;->a()Lcom/swedbank/mobile/business/authentication/p$b;

    move-result-object v0

    sget-object v3, Lcom/swedbank/mobile/business/biometric/authentication/e;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$b;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 90
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object v0

    const-string v3, "Completable.complete()"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :pswitch_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/i;

    sget-object v3, Lcom/swedbank/mobile/business/biometric/login/g$a;->a:Lcom/swedbank/mobile/business/biometric/login/g$a;

    check-cast v3, Lcom/swedbank/mobile/business/biometric/login/g;

    invoke-interface {v0, v3}, Lcom/swedbank/mobile/business/biometric/authentication/i;->a(Lcom/swedbank/mobile/business/biometric/login/g;)V

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    sget-object v3, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v3}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 91
    :goto_0
    new-instance v3, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$d;

    invoke-direct {v3, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;Lcom/swedbank/mobile/business/biometric/a;)V

    check-cast v3, Lkotlin/e/a/a;

    invoke-static {v0, v2, v3, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 106
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_1

    .line 96
    :cond_1
    instance-of p1, p1, Lcom/swedbank/mobile/business/biometric/a$c;

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->c:Lcom/swedbank/mobile/business/e/b;

    .line 63
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->a()Lio/reactivex/o;

    move-result-object v0

    .line 64
    sget-object v1, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$f;->a:Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$f;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/biometric/authentication/g;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/biometric/authentication/g;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "activityLifecycleMonitor\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    .line 101
    new-instance v1, Lcom/swedbank/mobile/business/authentication/p$c;

    sget-object v2, Lcom/swedbank/mobile/business/authentication/p$b;->k:Lcom/swedbank/mobile/business/authentication/p$b;

    invoke-static {v2}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/authentication/p$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/p;

    .line 100
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/authentication/h;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    return-void
.end method
