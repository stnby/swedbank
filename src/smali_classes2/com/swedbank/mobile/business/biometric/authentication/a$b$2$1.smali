.class final Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;
.super Ljava/lang/Object;
.source "AuthenticateWithBiometrics.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;->a(Lcom/swedbank/mobile/business/authentication/k$a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/k$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;Lcom/swedbank/mobile/business/authentication/k$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;->b:Lcom/swedbank/mobile/business/authentication/k$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/session/h;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "session"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/a$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/a;->e(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;

    iget-object v1, v1, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$b;

    iget-object v1, v1, Lcom/swedbank/mobile/business/biometric/authentication/a$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/biometric/authentication/a;->f(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;->b:Lcom/swedbank/mobile/business/authentication/k$a;

    .line 74
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/s;

    .line 64
    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/business/authentication/session/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/a$b$2$1;->a(Lcom/swedbank/mobile/business/authentication/session/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
