.class public final Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;
.super Lcom/swedbank/mobile/business/biometric/authentication/external/i;
.source "ExternalBiometricAuthenticationInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/biometric/authentication/external/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/e/l;",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 174
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/i;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/e/l;",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 174
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a:Lcom/swedbank/mobile/business/util/e;

    iget-object p1, p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a:Lcom/swedbank/mobile/business/util/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RetryableError(reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a:Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
