.class public final Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "ExternalBiometricAuthenticationInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/external/d;
.implements Lcom/swedbank/mobile/business/biometric/authentication/external/h;
.implements Lcom/swedbank/mobile/business/biometric/authentication/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/authentication/external/j;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/authentication/external/d;",
        "Lcom/swedbank/mobile/business/biometric/authentication/external/h;",
        "Lcom/swedbank/mobile/business/biometric/authentication/h;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/g;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final d:Lcom/swedbank/mobile/business/e/b;

.field private final e:Lcom/swedbank/mobile/business/biometric/authentication/k;

.field private final f:Lcom/swedbank/mobile/business/h/a;

.field private final g:Lcom/swedbank/mobile/business/i/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/biometric/authentication/k;Lcom/swedbank/mobile/business/h/a;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/authentication/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/h/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "external_biometric_authentication_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthorizationRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localServiceOrderingRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->d:Lcom/swedbank/mobile/business/e/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->e:Lcom/swedbank/mobile/business/biometric/authentication/k;

    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->f:Lcom/swedbank/mobile/business/h/a;

    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->g:Lcom/swedbank/mobile/business/i/c;

    .line 62
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Ext\u2026AuthenticationMetadata>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->a:Lcom/b/c/b;

    .line 63
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Exte\u2026AuthenticationProgress>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->b:Lcom/b/c/c;

    .line 64
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->d:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->a:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/h/a;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->f:Lcom/swedbank/mobile/business/h/a;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/k;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->e:Lcom/swedbank/mobile/business/biometric/authentication/k;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->b:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/external/j;
    .locals 0

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/i/c;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->g:Lcom/swedbank/mobile/business/i/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/biometric/authentication/external/g;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/external/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/g;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "metadata"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;Lcom/swedbank/mobile/business/biometric/authentication/external/g;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flow().flatMapMaybe { ro\u2026omCallable this\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->a:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/p$a;

    if-eqz v0, :cond_1

    .line 99
    check-cast p1, Lcom/swedbank/mobile/business/authentication/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/p$a;->a()Ljava/security/Signature;

    move-result-object p1

    .line 177
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 177
    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/external/g;

    .line 179
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/h/a;

    move-result-object v1

    .line 184
    invoke-interface {v1}, Lcom/swedbank/mobile/business/h/a;->a()Lio/reactivex/j;

    move-result-object v1

    .line 183
    invoke-virtual {v1}, Lio/reactivex/j;->c()Lio/reactivex/w;

    move-result-object v1

    .line 182
    new-instance v2, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$a;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/external/g;Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;Ljava/security/Signature;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "localServiceOrderingRepo\u2026re = signature)\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    new-instance v1, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;Ljava/security/Signature;)V

    check-cast v1, Lkotlin/e/a/b;

    .line 195
    new-instance v2, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$c;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;Ljava/security/Signature;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 181
    invoke-static {v0, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;)Lio/reactivex/b/c;

    move-result-object p1

    .line 196
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 198
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->g(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/j;->a()V

    goto/16 :goto_1

    .line 178
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 100
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/p$c;

    if-eqz v0, :cond_4

    .line 101
    check-cast p1, Lcom/swedbank/mobile/business/authentication/p$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/p$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 200
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->g(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/j;->a()V

    .line 201
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    .line 203
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/p$b;

    .line 204
    sget-object v1, Lcom/swedbank/mobile/business/biometric/authentication/external/e;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/p$b;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    .line 213
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/i$b;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 212
    :pswitch_1
    new-instance p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/biometric/login/a$c;-><init>(Z)V

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 210
    :pswitch_2
    new-instance p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/biometric/login/a$c;-><init>(Z)V

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 208
    :pswitch_3
    new-instance p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/a$b;->b:Lcom/swedbank/mobile/business/biometric/login/a$b;

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 206
    :pswitch_4
    new-instance p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;

    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/a$b;->b:Lcom/swedbank/mobile/business/biometric/login/a$b;

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 215
    :cond_2
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/e/l;

    .line 216
    new-instance v1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v1

    .line 201
    :goto_0
    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 216
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 218
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/i;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()V
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->a:Lcom/b/c/b;

    .line 176
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/external/g;

    .line 91
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    .line 92
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/g;->b()Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/g;->c()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-interface {v1, v2, v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 176
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public d()V
    .locals 2

    .line 228
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->h(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/i/c;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->g(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void
.end method

.method public i()V
    .locals 0

    .line 55
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/h$a;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)V

    return-void
.end method

.method protected i_()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/j;->c()V

    :cond_0
    return-void
.end method
