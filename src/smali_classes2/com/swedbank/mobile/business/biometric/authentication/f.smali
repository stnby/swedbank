.class public final Lcom/swedbank/mobile/business/biometric/authentication/f;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;>;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->a:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->b:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->c:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->d:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->e:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->f:Ljavax/inject/Provider;

    .line 44
    iput-object p7, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->g:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/authentication/f;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/e/l;",
            ">;>;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;)",
            "Lcom/swedbank/mobile/business/biometric/authentication/f;"
        }
    .end annotation

    .line 60
    new-instance v8, Lcom/swedbank/mobile/business/biometric/authentication/f;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/business/biometric/authentication/f;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;
    .locals 9

    .line 49
    new-instance v8, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/d/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/biometric/d;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/e/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/authentication/login/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/architect/business/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/util/l;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/f;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/business/biometric/authentication/h;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;-><init>(Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/authentication/login/g;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/biometric/authentication/h;)V

    return-object v8
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/f;->a()Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;

    move-result-object v0

    return-object v0
.end method
