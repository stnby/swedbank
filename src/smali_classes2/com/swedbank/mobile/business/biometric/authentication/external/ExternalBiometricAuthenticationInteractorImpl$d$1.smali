.class final Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;
.super Ljava/lang/Object;
.source "ExternalBiometricAuthenticationInteractor.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;->a(Lcom/swedbank/mobile/business/biometric/authentication/external/j;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

.field final synthetic b:Lcom/swedbank/mobile/business/biometric/authentication/external/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;Lcom/swedbank/mobile/business/biometric/authentication/external/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->b:Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/swedbank/mobile/business/e/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 77
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    iget-object v1, v1, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->b(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->b:Lcom/swedbank/mobile/business/biometric/authentication/external/j;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/j;->b()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    iget-object v1, v1, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;->b:Lcom/swedbank/mobile/business/biometric/authentication/external/g;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$d$1;->a()Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
