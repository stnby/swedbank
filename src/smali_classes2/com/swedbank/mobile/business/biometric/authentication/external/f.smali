.class public final Lcom/swedbank/mobile/business/biometric/authentication/external/f;
.super Ljava/lang/Object;
.source "ExternalBiometricAuthenticationInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/k;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/h/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/h/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->a:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->b:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->c:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/authentication/external/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/h/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/c;",
            ">;)",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/f;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/business/biometric/authentication/external/f;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;
    .locals 5

    .line 33
    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/e/b;

    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/biometric/authentication/k;

    iget-object v3, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/h/a;

    iget-object v4, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/i/c;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;-><init>(Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/business/biometric/authentication/k;Lcom/swedbank/mobile/business/h/a;Lcom/swedbank/mobile/business/i/c;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/authentication/external/f;->a()Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    move-result-object v0

    return-object v0
.end method
