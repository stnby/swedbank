.class public final Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;
.super Lkotlin/e/b/k;
.source "ExternalBiometricAuthenticationInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/biometric/authentication/m;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

.field final synthetic b:Ljava/security/Signature;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;Ljava/security/Signature;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;->b:Ljava/security/Signature;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/authentication/m;)V
    .locals 4

    .line 118
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/m$c;->a:Lcom/swedbank/mobile/business/biometric/authentication/m$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    const-wide/16 v0, 0x5dc

    .line 176
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/w;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.timer(1500, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    new-instance v1, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 180
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 120
    sget-object p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/i$a;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 122
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    .line 123
    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/a;->a:Lcom/swedbank/mobile/business/biometric/login/a$a;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/swedbank/mobile/business/biometric/login/a$a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/biometric/login/a$d;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 122
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    goto :goto_0

    .line 124
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;

    .line 125
    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/a;->a:Lcom/swedbank/mobile/business/biometric/login/a$a;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/m$b;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/swedbank/mobile/business/biometric/login/a$a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/biometric/login/a$d;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 124
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void

    .line 124
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/ExternalBiometricAuthenticationInteractorImpl$b;->a(Lcom/swedbank/mobile/business/biometric/authentication/m;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
