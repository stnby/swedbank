.class final synthetic Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$c;
.super Lkotlin/e/b/i;
.source "BiometricAuthenticationPromptInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/authentication/p;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/h;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/h;

    .line 82
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/h;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/business/authentication/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/BiometricAuthenticationPromptInteractorImpl$c;->a(Lcom/swedbank/mobile/business/authentication/p;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "biometricAuthenticationResultReceived"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "biometricAuthenticationResultReceived(Lcom/swedbank/mobile/business/authentication/BiometricAuthenticationResult;)V"

    return-object v0
.end method
