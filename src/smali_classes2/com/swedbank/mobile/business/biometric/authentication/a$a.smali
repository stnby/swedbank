.class final Lcom/swedbank/mobile/business/biometric/authentication/a$a;
.super Ljava/lang/Object;
.source "AuthenticateWithBiometrics.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/authentication/a;->a(Ljava/security/Signature;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/authentication/a;

.field final synthetic b:Ljava/security/Signature;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/a;Ljava/security/Signature;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->b:Ljava/security/Signature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/authentication/x;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/authentication/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/a;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/authentication/a;->a(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/authentication/login/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/g;->a()Ljava/lang/String;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/biometric/authentication/a;->b(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/biometric/authentication/j;

    move-result-object v1

    const-string v2, "mobileAppId"

    .line 47
    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->b:Ljava/security/Signature;

    .line 45
    invoke-interface {v1, v0, p1, v2}, Lcom/swedbank/mobile/business/biometric/authentication/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)Lio/reactivex/w;

    move-result-object p1

    .line 49
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/a;

    invoke-static {v1}, Lcom/swedbank/mobile/business/biometric/authentication/a;->c(Lcom/swedbank/mobile/business/biometric/authentication/a;)Lcom/swedbank/mobile/business/general/a;

    move-result-object v1

    .line 74
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 76
    new-instance v2, Lcom/swedbank/mobile/business/biometric/authentication/a$a$a;

    invoke-direct {v2, v1, p0, v0}, Lcom/swedbank/mobile/business/biometric/authentication/a$a$a;-><init>(Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/business/biometric/authentication/a$a;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 75
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/a$a$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/a$a$b;

    check-cast v0, Lkotlin/e/a/m;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/business/biometric/authentication/b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/biometric/authentication/b;-><init>(Lkotlin/e/a/m;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/d;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "toObservable()\n    .publ\u2026hanged(::classesAreEqual)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/a$a;->a(Lkotlin/k;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
