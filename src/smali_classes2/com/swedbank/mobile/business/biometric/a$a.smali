.class public final Lcom/swedbank/mobile/business/biometric/a$a;
.super Lcom/swedbank/mobile/business/biometric/a;
.source "BiometricRepository.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/biometric/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/p$b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Ljava/lang/CharSequence;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/p$b;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/p$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/business/biometric/a;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/a$a;->a:Lcom/swedbank/mobile/business/authentication/p$b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/a$a;->b:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/authentication/p$b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/a$a;->a:Lcom/swedbank/mobile/business/authentication/p$b;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/a$a;->b:Ljava/lang/CharSequence;

    return-object v0
.end method
