.class final Lcom/swedbank/mobile/business/biometric/preference/e$a;
.super Ljava/lang/Object;
.source "ObserveBiometricLoginEnabledStatus.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/preference/e;->a()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/biometric/preference/e$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/biometric/preference/e$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/biometric/preference/e$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/biometric/preference/e$a;->a:Lcom/swedbank/mobile/business/biometric/preference/e$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/biometric/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/preference/e$a;->a(Lcom/swedbank/mobile/business/biometric/b;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/business/biometric/b;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/swedbank/mobile/business/biometric/b;->b:Lcom/swedbank/mobile/business/biometric/b;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
