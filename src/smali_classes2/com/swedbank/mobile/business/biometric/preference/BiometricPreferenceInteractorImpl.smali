.class public final Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BiometricPreferenceInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/onboarding/e;
.implements Lcom/swedbank/mobile/business/onboarding/loading/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/preference/d;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
        "Lcom/swedbank/mobile/business/onboarding/loading/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/d;

.field private final b:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_preference_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/biometric/d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/business/preferences/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/biometric/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/biometric/preference/d;
    .locals 0

    .line 22
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/preference/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/preferences/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/business/preferences/a;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 57
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/preferences/a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/business/biometric/preference/d;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 56
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/preferences/a;

    move-result-object v0

    const-class v1, Lcom/swedbank/mobile/business/biometric/preference/d;

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/preferences/a;->a(Lkotlin/h/b;)V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/biometric/d;

    .line 29
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/d;->b()Lio/reactivex/w;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v1, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 54
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
