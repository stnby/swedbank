.class final Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;
.super Ljava/lang/Object;
.source "BiometricPreferenceInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/biometric/b;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/f;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/biometric/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "biometricAuthState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/swedbank/mobile/business/biometric/preference/a;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/b;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 35
    new-instance p1, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a$2;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a$2;-><init>(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 32
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object p1

    .line 33
    sget-object v0, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object p1

    .line 34
    new-instance v0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a$1;

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a$1;-><init>(Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/preference/c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/biometric/preference/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/business/biometric/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/preference/BiometricPreferenceInteractorImpl$a;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
