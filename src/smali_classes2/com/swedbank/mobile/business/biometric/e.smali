.class public final Lcom/swedbank/mobile/business/biometric/e;
.super Ljava/lang/Object;
.source "IsDeviceEligibleForBiometricAuthentication.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/g<",
        "Lio/reactivex/w<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/e;->a:Lcom/swedbank/mobile/business/biometric/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/e;)Lcom/swedbank/mobile/business/biometric/d;
    .locals 0

    .line 6
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/e;->a:Lcom/swedbank/mobile/business/biometric/d;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/e;->a:Lcom/swedbank/mobile/business/biometric/d;

    .line 10
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/d;->f()Lio/reactivex/w;

    move-result-object v0

    .line 11
    new-instance v1, Lcom/swedbank/mobile/business/biometric/e$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/biometric/e$a;-><init>(Lcom/swedbank/mobile/business/biometric/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026(false)\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/e;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
