.class public final enum Lcom/swedbank/mobile/business/biometric/b;
.super Ljava/lang/Enum;
.source "BiometricRepository.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/biometric/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/biometric/b;

.field public static final enum b:Lcom/swedbank/mobile/business/biometric/b;

.field public static final enum c:Lcom/swedbank/mobile/business/biometric/b;

.field private static final synthetic d:[Lcom/swedbank/mobile/business/biometric/b;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/biometric/b;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/b;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 29
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/business/biometric/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/biometric/b;->a:Lcom/swedbank/mobile/business/biometric/b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/biometric/b;

    const-string v2, "ENABLED"

    const/4 v4, 0x1

    invoke-direct {v1, v2, v4, v4}, Lcom/swedbank/mobile/business/biometric/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/biometric/b;->b:Lcom/swedbank/mobile/business/biometric/b;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/business/biometric/b;

    const-string v2, "DISABLED"

    const/4 v4, 0x2

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/business/biometric/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/swedbank/mobile/business/biometric/b;->d:[Lcom/swedbank/mobile/business/biometric/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/business/biometric/b;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/biometric/b;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/biometric/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/b;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/biometric/b;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/biometric/b;->d:[Lcom/swedbank/mobile/business/biometric/b;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/biometric/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/biometric/b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/swedbank/mobile/business/biometric/b;->e:I

    return v0
.end method
