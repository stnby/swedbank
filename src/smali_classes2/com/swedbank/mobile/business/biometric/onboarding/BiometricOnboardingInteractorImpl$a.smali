.class final Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;
.super Ljava/lang/Object;
.source "BiometricOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->a(Lcom/swedbank/mobile/business/authentication/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/String;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/p;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->b:Lcom/swedbank/mobile/business/authentication/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "mobileAppId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    .line 182
    new-instance v1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object v1

    .line 181
    new-instance v2, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Single\n      .fromCallab\u2026= block\n        )\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
