.class final Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;
.super Ljava/lang/Object;
.source "BiometricOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lkotlin/s;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/f;
    .locals 1
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->a(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h$1;

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h$1;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    check-cast p1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/d;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 78
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->b(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object p1

    .line 79
    sget-object v0, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object p1

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a(Lkotlin/s;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
