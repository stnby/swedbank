.class public final Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;
.super Ljava/lang/Object;
.source "BiometricOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a(Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/util/e<",
        "+",
        "Ljava/lang/Throwable;",
        "+",
        "Ljava/security/PublicKey;",
        ">;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->b:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/b;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/security/PublicKey;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "publicKeyResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/Key;

    .line 183
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->b:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->d(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/h/c;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->c:Ljava/lang/String;

    const-string v2, "mobileAppId"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->b:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

    iget-object v2, v2, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->b:Lcom/swedbank/mobile/business/authentication/p;

    check-cast v2, Lcom/swedbank/mobile/business/authentication/p$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/p$a;->a()Ljava/security/Signature;

    move-result-object v2

    .line 183
    invoke-interface {v0, v1, p1, v2}, Lcom/swedbank/mobile/business/h/c;->a(Ljava/lang/String;Ljava/security/Key;Ljava/security/Signature;)Lio/reactivex/w;

    move-result-object p1

    .line 187
    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "serviceOrderingRepositor\u2026    }\n                  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 143
    new-instance p1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$1;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026.right())\n              }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    .line 145
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
