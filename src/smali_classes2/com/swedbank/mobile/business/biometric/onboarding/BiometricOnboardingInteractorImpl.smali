.class public final Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BiometricOnboardingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/h;
.implements Lcom/swedbank/mobile/business/biometric/onboarding/a;
.implements Lcom/swedbank/mobile/business/biometric/onboarding/error/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/onboarding/f;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/authentication/h;",
        "Lcom/swedbank/mobile/business/biometric/onboarding/a;",
        "Lcom/swedbank/mobile/business/biometric/onboarding/error/e;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final b:Lcom/swedbank/mobile/business/biometric/d;

.field private final c:Lcom/swedbank/mobile/business/d/a;

.field private final d:Lcom/swedbank/mobile/business/h/c;

.field private final e:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/d/a;Lcom/swedbank/mobile/business/h/c;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/business/util/e;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/h/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getMobileAppIdUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_onboarding_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "authenticated_user_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Lcom/swedbank/mobile/business/d/a;",
            "Lcom/swedbank/mobile/business/h/c;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cryptoRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceOrderingRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getMobileAppId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userId"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/d/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/h/c;

    iput-object p4, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    iput-object p5, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f:Lcom/swedbank/mobile/business/util/e;

    iput-object p6, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->g:Ljava/lang/String;

    .line 58
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lio/reactivex/b;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->k()Lio/reactivex/b;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/h/c;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->d:Lcom/swedbank/mobile/business/h/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/onboarding/f;
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->k_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/onboarding/f;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/onboarding/f;
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/biometric/onboarding/f;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/d/a;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/d/a;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/lang/String;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->g:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/util/e;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f:Lcom/swedbank/mobile/business/util/e;

    return-object p0
.end method

.method private final k()Lio/reactivex/b;
    .locals 2

    .line 171
    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.fromAction {\u2026oardingFinished\n    )\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f:Lcom/swedbank/mobile/business/util/e;

    .line 188
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/e;

    .line 94
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object v0

    goto :goto_0

    .line 189
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    .line 91
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->a()Lio/reactivex/o;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$f;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$f;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/swedbank/mobile/business/biometric/onboarding/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/biometric/onboarding/c;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "onboardingStream\n       \u2026       .map(Enabled::not)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const-string v1, "listener.fold(\n      ifL\u2026 Observable.empty() }\n  )"

    .line 190
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 92
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->a()V

    .line 103
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/p$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 104
    new-instance v2, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;Lcom/swedbank/mobile/business/authentication/p;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "getMobileAppId()\n       \u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 191
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto/16 :goto_2

    .line 126
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/p$c;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/authentication/p$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    .line 195
    instance-of v3, v2, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/authentication/p$b;

    .line 196
    sget-object v3, Lcom/swedbank/mobile/business/biometric/onboarding/error/c;->a:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/p$b;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 201
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;

    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 200
    :pswitch_1
    sget-object v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;

    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 199
    :pswitch_2
    sget-object v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;

    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 198
    :pswitch_3
    sget-object v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;

    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 197
    :pswitch_4
    sget-object v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;

    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 203
    :cond_1
    instance-of v3, v2, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v3, :cond_4

    check-cast v2, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/e/l;

    new-instance v3, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;

    .line 204
    invoke-direct {v3, v2}, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v2, v3

    .line 205
    :goto_0
    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    .line 127
    sget-object v3, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    .line 128
    sget-object v2, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v2}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 129
    new-instance v2, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$c;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;Lcom/swedbank/mobile/business/authentication/p;)V

    check-cast v2, Lkotlin/e/a/a;

    const/4 p1, 0x1

    invoke-static {v0, v1, v2, p1, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 207
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_2

    .line 131
    :cond_3
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/onboarding/f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->a(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_2

    .line 204
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_5
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 209
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->g(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v0

    .line 212
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->h(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/d/a;->g(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 214
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/PublicKey;

    .line 215
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/onboarding/f;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->h(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 217
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/onboarding/f;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->g:Lcom/swedbank/mobile/business/authentication/p$b;

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->a(Lcom/swedbank/mobile/business/util/e;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public i()V
    .locals 0

    .line 49
    invoke-static {p0}, Lcom/swedbank/mobile/business/biometric/authentication/h$a;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)V

    return-void
.end method

.method public j()V
    .locals 3

    .line 154
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->b()V

    .line 155
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/d;

    .line 156
    sget-object v1, Lcom/swedbank/mobile/business/biometric/b;->c:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object v0

    .line 157
    invoke-direct {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->k()Lio/reactivex/b;

    move-result-object v1

    check-cast v1, Lio/reactivex/f;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026tifyOnboardingComplete())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 158
    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 220
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method protected m_()V
    .locals 7

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f:Lcom/swedbank/mobile/business/util/e;

    instance-of v0, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f:Lcom/swedbank/mobile/business/util/e;

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    .line 63
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->a()Lio/reactivex/o;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 64
    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 179
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f:Lcom/swedbank/mobile/business/util/e;

    .line 182
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/e;

    .line 74
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(Unit)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/f;

    .line 73
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->d()Lio/reactivex/w;

    move-result-object v0

    .line 75
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$i;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$i;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "listener.fold(\n        i\u2026ifyOnboardingComplete() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 84
    invoke-static {v0, v2, v2, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 185
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void

    .line 73
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
