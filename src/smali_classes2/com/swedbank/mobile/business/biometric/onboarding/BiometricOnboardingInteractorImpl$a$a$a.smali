.class final Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;
.super Ljava/lang/Object;
.source "BiometricOnboardingInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/util/p;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/f;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->b:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->b(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/d;

    move-result-object p1

    .line 113
    sget-object v0, Lcom/swedbank/mobile/business/biometric/b;->b:Lcom/swedbank/mobile/business/biometric/b;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/biometric/d;->a(Lcom/swedbank/mobile/business/biometric/b;)Lio/reactivex/b;

    move-result-object p1

    .line 114
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a;->b:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->c(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/f;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 115
    :cond_0
    instance-of p1, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a$1;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$a$a$a;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
