.class final synthetic Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h$1;
.super Lkotlin/e/b/i;
.source "BiometricOnboardingInteractor.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h;->a(Lkotlin/s;)Lio/reactivex/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "authenticateBiometrics"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "authenticateBiometrics()V"

    return-object v0
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h$1;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;

    .line 179
    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->g(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/d/a;

    move-result-object v1

    .line 182
    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->h(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/d/a;->g(Ljava/lang/String;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    .line 184
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/PublicKey;

    .line 185
    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/onboarding/f;

    move-result-object v1

    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->h(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 186
    :cond_0
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 187
    invoke-static {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;->f(Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl;)Lcom/swedbank/mobile/business/biometric/onboarding/f;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/authentication/p$b;->g:Lcom/swedbank/mobile/business/authentication/p$b;

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/biometric/onboarding/f;->a(Lcom/swedbank/mobile/business/util/e;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/BiometricOnboardingInteractorImpl$h$1;->d()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
