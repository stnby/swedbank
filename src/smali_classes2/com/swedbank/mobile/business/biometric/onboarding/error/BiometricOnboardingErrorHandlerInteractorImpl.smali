.class public final Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "BiometricOnboardingErrorHandlerInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/onboarding/error/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/biometric/onboarding/error/f;",
        ">;",
        "Lcom/swedbank/mobile/business/biometric/onboarding/error/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/e/l;",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_onboarding_error_reason"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/onboarding/error/e;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_onboarding_error_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/error/e;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "failReason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->a:Lcom/swedbank/mobile/business/util/e;

    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/biometric/onboarding/error/g;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->a:Lcom/swedbank/mobile/business/util/e;

    .line 73
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/authentication/p$b;

    .line 74
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/c;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/p$b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 79
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 78
    :pswitch_1
    sget-object v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 77
    :pswitch_2
    sget-object v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 76
    :pswitch_3
    sget-object v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 75
    :pswitch_4
    sget-object v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    goto :goto_0

    .line 81
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/e/l;

    new-instance v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;

    .line 82
    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;-><init>(Lcom/swedbank/mobile/business/e/l;)V

    move-object v0, v1

    .line 83
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    return-object v0

    .line 82
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/e;->j()V

    return-void
.end method

.method public c()V
    .locals 3

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->a()Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    move-result-object v0

    .line 37
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 38
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 39
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;

    if-eqz v1, :cond_2

    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->b()V

    goto :goto_2

    .line 40
    :cond_2
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    .line 41
    :cond_3
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_1
    iget-object v0, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/e;->b()V

    goto :goto_2

    .line 42
    :cond_4
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/f;

    .line 43
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/f;->a()Lio/reactivex/b;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl$a;

    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;->b:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/biometric/onboarding/error/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-virtual {v0, v2}, Lio/reactivex/b;->d(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "route()\n          .toBio\u2026ner::errorRetryRequested)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_5
    :goto_2
    return-void
.end method
