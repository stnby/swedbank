.class public final Lcom/swedbank/mobile/business/biometric/onboarding/error/b;
.super Ljava/lang/Object;
.source "BiometricOnboardingErrorHandlerInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/onboarding/error/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/onboarding/error/e;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;->a:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/biometric/onboarding/error/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/onboarding/error/e;",
            ">;)",
            "Lcom/swedbank/mobile/business/biometric/onboarding/error/b;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;
    .locals 3

    .line 24
    new-instance v0, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/util/e;

    iget-object v2, p0, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;-><init>(Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/onboarding/error/b;->a()Lcom/swedbank/mobile/business/biometric/onboarding/error/BiometricOnboardingErrorHandlerInteractorImpl;

    move-result-object v0

    return-object v0
.end method
