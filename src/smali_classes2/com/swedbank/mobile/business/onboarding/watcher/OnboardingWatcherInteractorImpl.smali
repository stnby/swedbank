.class public final Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OnboardingWatcherInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/d;
.implements Lcom/swedbank/mobile/business/onboarding/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/onboarding/watcher/c;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/d;",
        "Lcom/swedbank/mobile/business/onboarding/g;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/i/c;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/c;Ljava/util/Map;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/c;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_watcher_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "to_onboarding_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/i/c;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;>;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isOnboardingNeededUseCases"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->b:Lcom/swedbank/mobile/business/i/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->c:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/watcher/c;
    .locals 0

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/onboarding/watcher/c;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;Ljava/util/List;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->b:Lcom/swedbank/mobile/business/i/c;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/i/c;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void
.end method

.method public b()V
    .locals 3

    .line 60
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/watcher/c;

    .line 61
    iget-object v1, p0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->a:Ljava/util/List;

    if-nez v1, :cond_0

    const-string v2, "onboardingPluginKeys"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/onboarding/watcher/c;->b(Ljava/util/List;)V

    .line 62
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/watcher/c;->a()V

    return-void
.end method

.method protected m_()V
    .locals 7

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->c:Ljava/util/Map;

    .line 36
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;->a()V

    return-void

    .line 69
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 70
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 71
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/g;

    .line 42
    invoke-interface {v2}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/w;

    new-instance v4, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$b;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$b;-><init>(Ljava/lang/String;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v2, v4}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 74
    new-instance v0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$a;-><init>(Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/c/h;

    const/4 v2, 0x0

    .line 81
    new-array v2, v2, [Lio/reactivex/w;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    check-cast v1, [Lio/reactivex/aa;

    .line 73
    invoke-static {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.zipArray(\n       \u2026,\n        toTypedArray())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v1, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$c;->a:Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$c;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lcom/swedbank/mobile/business/onboarding/watcher/b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/onboarding/watcher/b;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/k;)Lio/reactivex/j;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lio/reactivex/j;->d()Lio/reactivex/j;

    move-result-object v1

    const-string v0, "isOnboardingNeededUseCas\u2026       .onErrorComplete()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    .line 55
    new-instance v0, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$e;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;

    invoke-direct {v0, v3}, Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/onboarding/watcher/OnboardingWatcherInteractorImpl;)V

    move-object v3, v0

    check-cast v3, Lkotlin/e/a/a;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 50
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/j;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 82
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void

    .line 81
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
