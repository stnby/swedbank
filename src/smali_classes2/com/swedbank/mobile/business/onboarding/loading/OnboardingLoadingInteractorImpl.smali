.class public final Lcom/swedbank/mobile/business/onboarding/loading/OnboardingLoadingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OnboardingLoadingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/loading/a;
.implements Lcom/swedbank/mobile/business/onboarding/loading/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/onboarding/loading/d;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/loading/a;",
        "Lcom/swedbank/mobile/business/onboarding/loading/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/onboarding/loading/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/loading/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/loading/c;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_loading_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/loading/OnboardingLoadingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/loading/c;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/loading/OnboardingLoadingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/loading/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/loading/c;->b()V

    return-void
.end method
