.class public final Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OnboardingTourInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/g;
.implements Lcom/swedbank/mobile/business/onboarding/tour/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/onboarding/tour/d;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/g;",
        "Lcom/swedbank/mobile/business/onboarding/tour/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/onboarding/tour/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:Lcom/swedbank/mobile/business/onboarding/g;


# direct methods
.method public constructor <init>(ILcom/swedbank/mobile/business/onboarding/g;)V
    .locals 2
    .param p1    # I
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_tour_item_count"
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_tour_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput p1, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->b:I

    iput-object p2, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/g;

    .line 80
    new-instance p1, Lcom/swedbank/mobile/business/onboarding/tour/c;

    .line 82
    invoke-static {p0}, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a(Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;)I

    move-result p2

    const/4 v0, 0x1

    sub-int/2addr p2, v0

    const/4 v1, 0x0

    if-gtz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 80
    :goto_0
    invoke-direct {p1, v1, v0}, Lcom/swedbank/mobile/business/onboarding/tour/c;-><init>(IZ)V

    .line 31
    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026n(INITIAL_TOUR_POSITION))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;)I
    .locals 0

    .line 27
    iget p0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->b:I

    return p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/onboarding/tour/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/g;->b()V

    return-void
.end method

.method public c()V
    .locals 5

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a:Lcom/b/c/b;

    .line 59
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 36
    check-cast v0, Lcom/swedbank/mobile/business/onboarding/tour/c;

    .line 38
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/onboarding/tour/c;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/g;->b()V

    goto :goto_1

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a:Lcom/b/c/b;

    .line 60
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/onboarding/tour/c;->a()I

    move-result v0

    const/4 v2, 0x1

    add-int/2addr v0, v2

    .line 62
    new-instance v3, Lcom/swedbank/mobile/business/onboarding/tour/c;

    .line 64
    invoke-static {p0}, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a(Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;)I

    move-result v4

    sub-int/2addr v4, v2

    if-lt v0, v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 62
    :goto_0
    invoke-direct {v3, v0, v2}, Lcom/swedbank/mobile/business/onboarding/tour/c;-><init>(IZ)V

    .line 39
    invoke-virtual {v1, v3}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 59
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public d()V
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a:Lcom/b/c/b;

    .line 66
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 44
    check-cast v0, Lcom/swedbank/mobile/business/onboarding/tour/c;

    .line 46
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/onboarding/tour/c;->a()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/g;->b()V

    goto :goto_1

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a:Lcom/b/c/b;

    .line 67
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/onboarding/tour/c;->a()I

    move-result v0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    .line 68
    new-instance v3, Lcom/swedbank/mobile/business/onboarding/tour/c;

    .line 70
    invoke-static {p0}, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;->a(Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;)I

    move-result v4

    sub-int/2addr v4, v2

    if-lt v0, v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 68
    :goto_0
    invoke-direct {v3, v0, v2}, Lcom/swedbank/mobile/business/onboarding/tour/c;-><init>(IZ)V

    .line 47
    invoke-virtual {v1, v3}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 66
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
