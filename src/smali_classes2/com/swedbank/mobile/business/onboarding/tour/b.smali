.class public final Lcom/swedbank/mobile/business/onboarding/tour/b;
.super Ljava/lang/Object;
.source "OnboardingTourInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/tour/b;->a:Ljavax/inject/Provider;

    .line 16
    iput-object p2, p0, Lcom/swedbank/mobile/business/onboarding/tour/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/onboarding/tour/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ">;)",
            "Lcom/swedbank/mobile/business/onboarding/tour/b;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/swedbank/mobile/business/onboarding/tour/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/onboarding/tour/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;
    .locals 3

    .line 21
    new-instance v0, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/onboarding/tour/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/onboarding/tour/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/onboarding/g;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;-><init>(ILcom/swedbank/mobile/business/onboarding/g;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/tour/b;->a()Lcom/swedbank/mobile/business/onboarding/tour/OnboardingTourInteractorImpl;

    move-result-object v0

    return-object v0
.end method
