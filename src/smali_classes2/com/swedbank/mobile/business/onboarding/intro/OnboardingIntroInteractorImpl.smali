.class public final Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OnboardingIntroInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/g;
.implements Lcom/swedbank/mobile/business/onboarding/intro/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/onboarding/intro/c;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/g;",
        "Lcom/swedbank/mobile/business/onboarding/intro/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/f/a;

.field private final b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/onboarding/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;ILjava/util/List;Lcom/swedbank/mobile/business/onboarding/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_tour_item_count"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_tour_extra_ids"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extraItemIds"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    iput p2, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->b:I

    iput-object p3, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->d:Lcom/swedbank/mobile/business/onboarding/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)Ljava/util/List;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->c:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/intro/c;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/onboarding/intro/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->c:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->c:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    iget-object v2, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    .line 55
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 56
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 57
    check-cast v4, Ljava/lang/String;

    .line 40
    invoke-interface {v2, v4}, Lcom/swedbank/mobile/business/f/a;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 58
    :cond_0
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 40
    new-instance v0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-static {v3, v0}, Lio/reactivex/w;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v2, "Single\n          .zip(ex\u2026            }\n          }"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v2, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)V

    check-cast v2, Lkotlin/e/a/b;

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v1, v3}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 59
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_1

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/onboarding/intro/c;

    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/onboarding/intro/c;->a(Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->d:Lcom/swedbank/mobile/business/onboarding/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/g;->b()V

    return-void
.end method

.method protected m_()V
    .locals 1

    .line 32
    iget v0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->b:I

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->d:Lcom/swedbank/mobile/business/onboarding/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/g;->b()V

    :cond_0
    return-void
.end method
