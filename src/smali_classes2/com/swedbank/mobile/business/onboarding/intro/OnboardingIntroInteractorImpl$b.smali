.class final Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;
.super Lkotlin/e/b/k;
.source "OnboardingIntroInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/ArrayList<",
        "Ljava/lang/String;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;->a:Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "undiscoveredFeatures"

    .line 46
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    iget-object v1, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;->a:Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->b(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v1

    .line 55
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 46
    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;->a:Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;->c(Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl;)Lcom/swedbank/mobile/business/onboarding/intro/c;

    move-result-object v0

    check-cast p1, Ljava/util/List;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/onboarding/intro/c;->a(Ljava/util/List;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/onboarding/intro/OnboardingIntroInteractorImpl$b;->a(Ljava/util/ArrayList;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
