.class public final Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OnboardingInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/onboarding/e;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/onboarding/f;

.field private final b:Lcom/swedbank/mobile/business/i/d;

.field private final c:Lcom/swedbank/mobile/business/onboarding/d;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/f;Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/business/onboarding/d;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/onboarding/d;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_plugin_keys"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "Lcom/swedbank/mobile/business/i/d;",
            "Lcom/swedbank/mobile/business/onboarding/d;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pluginManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingPluginKeys"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    iput-object p2, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/i/d;

    iput-object p3, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/d;

    iput-object p4, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/f;->c()V

    return-void
.end method

.method public b()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/d;->a()V

    return-void
.end method

.method protected m_()V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->b:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/f;

    .line 34
    iget-object v1, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/onboarding/f;->a(Ljava/util/List;)Lio/reactivex/b;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl$a;

    iget-object v2, p0, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl;->c:Lcom/swedbank/mobile/business/onboarding/d;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/onboarding/OnboardingInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/onboarding/d;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/business/onboarding/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/onboarding/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-virtual {v0, v2}, Lio/reactivex/b;->d(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "onboardingStream\n       \u2026ner::onboardingCompleted)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
