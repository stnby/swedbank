.class public final Lcom/swedbank/mobile/business/onboarding/error/OnboardingErrorHandlerInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OnboardingErrorHandlerInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/error/a;
.implements Lcom/swedbank/mobile/business/onboarding/error/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/onboarding/error/d;",
        ">;",
        "Lcom/swedbank/mobile/business/onboarding/error/a;",
        "Lcom/swedbank/mobile/business/onboarding/error/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/onboarding/error/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/error/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/error/c;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_error_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/onboarding/error/OnboardingErrorHandlerInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/error/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/error/OnboardingErrorHandlerInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/error/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/error/c;->a()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/onboarding/error/OnboardingErrorHandlerInteractorImpl;->a:Lcom/swedbank/mobile/business/onboarding/error/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/error/c;->b()V

    return-void
.end method
