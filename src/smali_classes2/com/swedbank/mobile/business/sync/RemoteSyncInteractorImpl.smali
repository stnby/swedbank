.class public final Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "RemoteSyncInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/sync/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/h;

.field private final b:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/h/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/h;Lcom/swedbank/mobile/architect/business/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "syncMobileAppIdUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/h;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/h/b;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "syncMobileAppId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->a:Lcom/swedbank/mobile/business/push/h;

    iput-object p2, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;)Lcom/swedbank/mobile/business/push/h;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->a:Lcom/swedbank/mobile/business/push/h;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 3

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->a:Lcom/swedbank/mobile/business/push/h;

    .line 24
    invoke-interface {v0}, Lcom/swedbank/mobile/business/push/h;->b()Lio/reactivex/o;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "pushRepository\n        .\u2026e()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x3

    .line 42
    invoke-static {v0, v1, v1, v2, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 47
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
