.class final Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;
.super Ljava/lang/Object;
.source "RemoteSyncInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Boolean;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;->a:Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/f;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "hasPendingPushToken"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;->a:Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;->a(Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a$1;-><init>(Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 39
    :cond_0
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/sync/RemoteSyncInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
