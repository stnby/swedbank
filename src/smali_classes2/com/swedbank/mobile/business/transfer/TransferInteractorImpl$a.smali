.class public final Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$a;
.super Ljava/lang/Object;
.source "Flows.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/business/e;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/transfer/h;

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/transfer/h;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/transfer/payment/form/h;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/architect/business/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$a;->a(Lcom/swedbank/mobile/architect/business/e;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
