.class public final Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferRemoteSearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/search/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/search/remote/c;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/search/g;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/transfer/search/e;

.field private final c:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/search/e;Lcom/swedbank/mobile/business/customer/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/search/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transferSearchRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/search/e;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/customer/l;

    .line 23
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<List<TransferSearchResult>>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026lts.accept(emptyList()) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/search/e;

    .line 31
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/transfer/search/e;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 32
    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 33
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl$c;

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->a:Lcom/b/c/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl$c;-><init>(Lcom/b/c/c;)V

    check-cast v0, Lkotlin/e/a/b;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/search/remote/a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/business/transfer/search/remote/a;-><init>(Lkotlin/e/a/b;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {p1, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "transferSearchRepository\u2026         .ignoreElement()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/remote/TransferRemoteSearchInteractorImpl;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public b()V
    .locals 0

    .line 18
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/search/g$a;->a(Lcom/swedbank/mobile/business/transfer/search/g;)V

    return-void
.end method
