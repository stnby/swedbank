.class public final Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferSearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/c;
.implements Lcom/swedbank/mobile/business/transfer/search/a;
.implements Lcom/swedbank/mobile/business/transfer/search/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/search/h;",
        ">;",
        "Lcom/swedbank/mobile/business/i/c;",
        "Lcom/swedbank/mobile/business/transfer/search/a;",
        "Lcom/swedbank/mobile/business/transfer/search/c;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/transfer/search/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/transfer/search/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/search/c;Ljava/util/List;)V
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/transfer/search/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_search_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "to_transfer_search_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/search/c;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/d;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "injectedPlugins"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/search/c;

    .line 54
    move-object p1, p2

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    .line 58
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 59
    new-instance v0, Landroidx/c/a;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroidx/c/a;-><init>(I)V

    .line 60
    check-cast p2, Ljava/lang/Iterable;

    .line 117
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/search/d;

    .line 61
    invoke-interface {v1}, Lcom/swedbank/mobile/business/transfer/search/d;->g()Lkotlin/e/a/b;

    move-result-object v2

    invoke-interface {v2, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/a/h;

    .line 62
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/business/transfer/search/g;

    .line 63
    move-object v4, p1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 64
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/transfer/search/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 66
    :cond_0
    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->a:Ljava/util/List;

    .line 67
    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->b:Ljava/util/Map;

    return-void

    .line 54
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There must be at least one search plugin"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->b:Ljava/util/Map;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 108
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 109
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/transfer/search/g;

    .line 75
    invoke-interface {v2, p1}, Lcom/swedbank/mobile/business/transfer/search/g;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 75
    invoke-static {v1}, Lio/reactivex/b;->a(Ljava/lang/Iterable;)Lio/reactivex/b;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .merge\u2026\n      .onErrorComplete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->b:Ljava/util/Map;

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 112
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 113
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/transfer/search/g;

    .line 80
    invoke-interface {v2}, Lcom/swedbank/mobile/business/transfer/search/g;->a()Lio/reactivex/o;

    move-result-object v2

    new-instance v4, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$a;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$a;-><init>(Ljava/lang/String;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 79
    invoke-static {v1}, Lio/reactivex/o;->b(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v0

    .line 82
    invoke-static {}, Lkotlin/a/x;->a()Ljava/util/Map;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$b;

    check-cast v2, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 85
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    .line 86
    sget-object v1, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;->a:Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .merge(\u2026        .toList()\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/search/h;

    .line 100
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/search/h;->b(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 95
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/search/c;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/search/c;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    .line 96
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 115
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/search/g;

    .line 96
    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/search/g;->b()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected m_()V
    .locals 5

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->a:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    .line 103
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 104
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 105
    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    move-object v4, v1

    check-cast v4, Lcom/swedbank/mobile/business/transfer/search/h;

    .line 71
    invoke-interface {v4, v3}, Lcom/swedbank/mobile/business/transfer/search/h;->a(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :cond_0
    check-cast v2, Ljava/util/List;

    return-void
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/search/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/search/c;->n()V

    return-void
.end method
