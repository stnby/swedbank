.class public final Lcom/swedbank/mobile/business/transfer/search/prediction/f;
.super Ljava/lang/Object;
.source "TransferPredictionSearchInteractor.kt"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 42
    new-instance v11, Lcom/swedbank/mobile/business/transfer/search/f;

    .line 44
    sget-object v2, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const-string v8, ""

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x7c

    const/4 v10, 0x0

    move-object v0, v11

    .line 42
    invoke-direct/range {v0 .. v10}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v11}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/f;->a:Ljava/util/List;

    const/4 v0, 0x6

    .line 48
    new-array v0, v0, [Lkotlin/k;

    const-string v1, "\u20ac"

    const-string v2, "EUR"

    .line 49
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "$"

    const-string v2, "USD"

    .line 50
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "\u00a3"

    const-string v2, "GBP"

    .line 51
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "KR"

    const-string v2, "SEK"

    .line 52
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "\u20bd"

    const-string v2, "RUB"

    .line 53
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "\u00a5"

    const-string v2, "JPY"

    .line 54
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 48
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/b;->a([Lkotlin/k;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/f;->b:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic a()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/f;->a:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic b()Ljava/util/Map;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/f;->b:Ljava/util/Map;

    return-object v0
.end method
