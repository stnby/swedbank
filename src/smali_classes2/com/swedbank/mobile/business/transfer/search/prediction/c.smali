.class public final Lcom/swedbank/mobile/business/transfer/search/prediction/c;
.super Ljava/lang/Object;
.source "TransferPredictionSearchInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/String;",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/transfer/search/f;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/search/prediction/c;

.field private static final b:Ljava/math/BigDecimal;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 64
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/prediction/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/search/prediction/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/c;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/c;

    .line 65
    new-instance v0, Ljava/math/BigDecimal;

    const v1, 0xf4240

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/c;->b:Ljava/math/BigDecimal;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 17
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "input"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkotlin/j/n;->a(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v9

    const/4 v2, 0x0

    if-eqz v9, :cond_1

    .line 70
    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/c;->b:Ljava/math/BigDecimal;

    invoke-virtual {v9, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_0

    return-object v2

    .line 74
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v4, 0x0

    .line 76
    sget-object v5, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v10, "EUR"

    const/4 v11, 0x0

    const/16 v12, 0x9c

    const/4 v13, 0x0

    move-object v3, v0

    .line 74
    invoke-direct/range {v3 .. v13}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 73
    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v3, "[0-9]+\\.?([0-9]+)?"

    .line 80
    new-instance v4, Lkotlin/j/k;

    invoke-direct {v4, v3}, Lkotlin/j/k;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-static {v4, v1, v5, v3, v2}, Lkotlin/j/k;->a(Lkotlin/j/k;Ljava/lang/CharSequence;IILjava/lang/Object;)Lkotlin/j/i;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 83
    invoke-interface {v1}, Lkotlin/j/i;->a()Lkotlin/g/d;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/j/n;->a(Ljava/lang/String;Lkotlin/g/d;)Ljava/lang/String;

    move-result-object v3

    .line 84
    new-instance v12, Ljava/math/BigDecimal;

    invoke-direct {v12, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 86
    sget-object v3, Lcom/swedbank/mobile/business/transfer/search/prediction/c;->b:Ljava/math/BigDecimal;

    invoke-virtual {v12, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-lez v3, :cond_2

    return-object v2

    .line 90
    :cond_2
    invoke-static {}, Lcom/swedbank/mobile/business/transfer/search/prediction/f;->b()Ljava/util/Map;

    move-result-object v2

    .line 93
    invoke-interface {v1}, Lkotlin/j/i;->a()Lkotlin/g/d;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/g/d;->h()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v3, :cond_b

    .line 94
    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 95
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    const-string v3, "(this as java.lang.String).toUpperCase()"

    invoke-static {v13, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-interface {v2, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 98
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v7, 0x0

    .line 100
    sget-object v8, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x9c

    const/16 v16, 0x0

    move-object v6, v0

    move-object v13, v3

    .line 98
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 104
    :cond_3
    invoke-interface {v2, v13}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 105
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v7, 0x0

    .line 107
    sget-object v8, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x9c

    const/16 v16, 0x0

    move-object v6, v0

    .line 105
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 114
    :cond_4
    invoke-interface {v1}, Lkotlin/j/i;->a()Lkotlin/g/d;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/g/d;->i()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_9

    .line 115
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 116
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    const-string v0, "(this as java.lang.String).toUpperCase()"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-interface {v2, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 119
    new-instance v1, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v7, 0x0

    .line 121
    sget-object v8, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x9c

    const/16 v16, 0x0

    move-object v6, v1

    move-object v13, v0

    .line 119
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 125
    :cond_5
    invoke-interface {v2, v13}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 126
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v7, 0x0

    .line 128
    sget-object v8, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x9c

    const/16 v16, 0x0

    move-object v6, v0

    .line 126
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 134
    :cond_6
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 135
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v7, 0x0

    .line 137
    sget-object v8, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x9c

    const/16 v16, 0x0

    move-object v6, v0

    .line 135
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 143
    :cond_7
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    const/4 v7, 0x0

    .line 145
    sget-object v8, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v13, "EUR"

    const/4 v14, 0x0

    const/16 v15, 0x9c

    const/16 v16, 0x0

    move-object v6, v0

    .line 143
    invoke-direct/range {v6 .. v16}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 116
    :cond_8
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_9
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_a
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_b
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    return-object v2
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/search/prediction/c;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
