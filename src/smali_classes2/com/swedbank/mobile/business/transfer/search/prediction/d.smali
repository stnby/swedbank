.class public final Lcom/swedbank/mobile/business/transfer/search/prediction/d;
.super Ljava/lang/Object;
.source "TransferPredictionSearchInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/String;",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/transfer/search/f;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/search/prediction/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 175
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/prediction/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/search/prediction/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/d;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 179
    sget-object v3, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    .line 180
    invoke-static {p1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf8

    const/4 v11, 0x0

    .line 177
    new-instance p1, Lcom/swedbank/mobile/business/transfer/search/f;

    move-object v1, p1

    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {p1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 175
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/search/prediction/d;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
