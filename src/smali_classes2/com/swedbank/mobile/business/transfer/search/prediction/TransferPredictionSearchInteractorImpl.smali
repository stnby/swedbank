.class public final Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferPredictionSearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/search/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/search/prediction/g;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/search/g;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 22
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<List<TransferSearchResult>>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;->a:Lcom/b/c/c;

    const/4 v0, 0x4

    .line 23
    new-array v0, v0, [Lkotlin/e/a/b;

    .line 24
    sget-object v1, Lcom/swedbank/mobile/business/transfer/search/prediction/a;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/a;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 25
    sget-object v1, Lcom/swedbank/mobile/business/transfer/search/prediction/b;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/b;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 26
    sget-object v1, Lcom/swedbank/mobile/business/transfer/search/prediction/c;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/c;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 27
    sget-object v1, Lcom/swedbank/mobile/business/transfer/search/prediction/d;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/d;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 23
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;)Ljava/util/List;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;->b:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026ccept(parsingResults)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    invoke-static {}, Lcom/swedbank/mobile/business/transfer/search/prediction/f;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/search/prediction/TransferPredictionSearchInteractorImpl;->a:Lcom/b/c/c;

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .just(e\u2026.mergeWith(searchResults)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()V
    .locals 0

    .line 19
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/search/g$a;->a(Lcom/swedbank/mobile/business/transfer/search/g;)V

    return-void
.end method
