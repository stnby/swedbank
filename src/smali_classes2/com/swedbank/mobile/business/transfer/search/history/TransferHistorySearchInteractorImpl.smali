.class public final Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferHistorySearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/search/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/search/history/b;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/search/g;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/transfer/search/history/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/search/history/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/search/history/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transferSearchHistoryRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/search/history/c;

    const/4 p1, 0x1

    .line 21
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string v0, "BehaviorRelay.createDefault(true)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->a:Lcom/b/c/b;

    .line 22
    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v0, ""

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->a:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;)Lcom/swedbank/mobile/business/transfer/search/history/c;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/search/history/c;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026cept(input.isBlank())\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->a:Lcom/b/c/b;

    .line 40
    new-instance v1, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "searchResultsEnabled\n   \u2026List())\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()V
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "lastInput.get()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 32
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/search/history/c;

    .line 34
    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/transfer/search/history/c;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 35
    invoke-static {v0, v2, v2, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    :cond_1
    return-void

    .line 30
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
