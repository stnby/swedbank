.class final Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;
.super Ljava/lang/Object;
.source "TransferSearchInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl;->a()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;->a:Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 45
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c;->a(Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "groupedResults"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {p1}, Lkotlin/a/x;->c(Ljava/util/Map;)Lkotlin/i/e;

    move-result-object p1

    .line 89
    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c$1;->a:Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c$1;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->b(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 103
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/search/TransferSearchInteractorImpl$c$a;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object p1

    .line 91
    invoke-static {p1}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
