.class final Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a;
.super Ljava/lang/Object;
.source "TransferHistorySearchInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->a()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;>;"
        }
    .end annotation

    const-string v0, "enabled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;->c(Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl;)Lcom/swedbank/mobile/business/transfer/search/history/c;

    move-result-object p1

    .line 43
    invoke-interface {p1}, Lcom/swedbank/mobile/business/transfer/search/history/c;->a()Lio/reactivex/o;

    move-result-object p1

    .line 44
    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a$1;->a:Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/search/history/TransferHistorySearchInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
