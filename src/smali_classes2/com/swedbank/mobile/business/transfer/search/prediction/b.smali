.class public final Lcom/swedbank/mobile/business/transfer/search/prediction/b;
.super Ljava/lang/Object;
.source "TransferPredictionSearchInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/String;",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/transfer/search/f;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/search/prediction/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 154
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/prediction/b;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/search/prediction/b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/search/prediction/b;->a:Lcom/swedbank/mobile/business/transfer/search/prediction/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 19
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "input"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ge v1, v3, :cond_0

    return-object v2

    .line 159
    :cond_0
    invoke-static {}, Lcom/swedbank/mobile/business/c/c;->values()[Lcom/swedbank/mobile/business/c/c;

    move-result-object v4

    .line 186
    array-length v5, v4

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_4

    aget-object v7, v4, v6

    .line 160
    sget-object v8, Lcom/swedbank/mobile/business/c/c;->d:Lcom/swedbank/mobile/business/c/c;

    if-ne v7, v8, :cond_1

    goto :goto_1

    .line 162
    :cond_1
    invoke-virtual {v7}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v0, v7, v8}, Lkotlin/j/n;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_3

    if-eq v1, v3, :cond_2

    const/4 v7, 0x3

    if-lt v1, v7, :cond_3

    .line 163
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->isDigit(C)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    const/4 v9, 0x0

    .line 166
    sget-object v10, Lcom/swedbank/mobile/business/transfer/search/f$a;->b:Lcom/swedbank/mobile/business/transfer/search/f$a;

    const/4 v11, 0x0

    .line 167
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v12

    const-string v0, "(this as java.lang.String).toUpperCase()"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0xf4

    const/16 v18, 0x0

    .line 164
    new-instance v0, Lcom/swedbank/mobile/business/transfer/search/f;

    move-object v8, v0

    invoke-direct/range {v8 .. v18}, Lcom/swedbank/mobile/business/transfer/search/f;-><init>(ILcom/swedbank/mobile/business/transfer/search/f$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_3
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_4
    return-object v2
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 154
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/search/prediction/b;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
