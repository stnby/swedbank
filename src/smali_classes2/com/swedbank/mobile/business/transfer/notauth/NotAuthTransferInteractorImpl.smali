.class public final Lcom/swedbank/mobile/business/transfer/notauth/NotAuthTransferInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "NotAuthTransferInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/notauth/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/notauth/c;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/notauth/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/i/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_out_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pluginManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/notauth/NotAuthTransferInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/notauth/NotAuthTransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/notauth/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/notauth/c;->a()V

    return-void
.end method

.method protected m_()V
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/notauth/NotAuthTransferInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/notauth/NotAuthTransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    return-void
.end method
