.class final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/h;)Lio/reactivex/w;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/h;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/h$c;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    .line 70
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/h$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/h$c;->a()Z

    move-result v1

    .line 71
    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/h$c;->b()Ljava/util/List;

    move-result-object p1

    .line 319
    sget-object v3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 320
    check-cast p1, Ljava/lang/Iterable;

    .line 321
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 322
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    .line 323
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 324
    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 320
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->getPriorityLevel()I

    move-result v3

    .line 325
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 326
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 327
    move-object v5, v4

    check-cast v5, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 320
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->getPriorityLevel()I

    move-result v5

    if-ge v3, v5, :cond_1

    move-object v2, v4

    move v3, v5

    goto :goto_0

    :cond_2
    move-object p1, v2

    .line 333
    :goto_1
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    goto :goto_2

    .line 334
    :cond_3
    instance-of p1, v2, Lcom/swedbank/mobile/business/util/n;

    if-eqz p1, :cond_5

    check-cast v2, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    .line 317
    :goto_2
    move-object v6, p1

    check-cast v6, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 337
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    .line 342
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Ljava/lang/String;

    move-result-object v4

    .line 343
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->c(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p1

    .line 337
    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;ILkotlin/e/b/g;)V

    if-eqz v1, :cond_4

    .line 353
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->e(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/w;

    .line 354
    new-instance v2, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "startPaymentSigning(inpu\u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 356
    :cond_4
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->d(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    goto/16 :goto_5

    .line 335
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 72
    :cond_6
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/h$a;

    if-eqz v0, :cond_7

    .line 73
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 74
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 75
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/h$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/h$a;->a()Ljava/util/List;

    move-result-object p1

    .line 73
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    .line 359
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_5

    .line 77
    :cond_7
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/h$b;

    if-eqz v0, :cond_b

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 361
    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    .line 363
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$b;

    const/4 v3, 0x1

    if-eqz v2, :cond_8

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 364
    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v3, v1

    goto :goto_3

    .line 365
    :cond_8
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_a

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    :goto_3
    if-eqz v3, :cond_9

    .line 368
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    .line 370
    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 368
    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    goto :goto_4

    .line 372
    :cond_9
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 374
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 372
    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    .line 376
    :goto_4
    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_5
    return-object p1

    .line 366
    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 376
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/h;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
