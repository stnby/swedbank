.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/a;
.super Ljava/lang/Object;
.source "ExecutePayment.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/payment/c;

.field private final b:Lcom/swedbank/mobile/business/e/i;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/c;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "executePreparedPaymentUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executePreparedPayment"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->b:Lcom/swedbank/mobile/business/e/i;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->c:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/payment/execution/a;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->c:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/payment/execution/a;)Lcom/swedbank/mobile/business/transfer/payment/c;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    .line 20
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/c;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/b;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->b:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/e/i;->i()Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/a;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/c;->c(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    invoke-virtual {v0, p1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRepository\n      \u2026uledPaymentResult(input))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
