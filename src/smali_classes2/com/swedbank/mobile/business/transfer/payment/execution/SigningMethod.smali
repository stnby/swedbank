.class public final enum Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
.super Ljava/lang/Enum;
.source "PaymentSigningChallenge.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

.field public static final enum MOBILE_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

.field public static final enum PIN_CALCULATOR:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

.field public static final enum SIMPLE_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

.field public static final enum SMART_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

.field public static final enum UNKNOWN:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    const-string v2, "SIMPLE_ID"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->SIMPLE_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    const-string v2, "SMART_ID"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->SMART_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    const-string v2, "MOBILE_ID"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->MOBILE_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    const-string v2, "PIN_CALCULATOR"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->PIN_CALCULATOR:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->UNKNOWN:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    return-object v0
.end method
