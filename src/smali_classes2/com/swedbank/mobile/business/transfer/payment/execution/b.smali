.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/b;
.super Ljava/lang/Object;
.source "ExecutePayment_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/b;",
            ">;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->a:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->b:Ljavax/inject/Provider;

    .line 23
    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/b;",
            ">;>;)",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/b;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/payment/execution/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/transfer/payment/execution/a;
    .locals 4

    .line 28
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/a;

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/c;

    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/e/i;

    iget-object v3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/business/b;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/c;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/architect/business/b;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/b;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/a;

    move-result-object v0

    return-object v0
.end method
