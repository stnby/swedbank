.class public final Lcom/swedbank/mobile/business/transfer/payment/form/a;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "PaymentFormFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/transfer/payment/form/b;",
        "Lcom/swedbank/mobile/business/transfer/payment/form/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transferFlow"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/a;->a:Lcom/swedbank/mobile/business/transfer/a;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/transfer/payment/form/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/payment/form/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/payment/form/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/payment/form/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/form/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/transfer/payment/form/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/a;->a:Lcom/swedbank/mobile/business/transfer/a;

    sget-object v1, Lcom/swedbank/mobile/business/transfer/b;->a:Lcom/swedbank/mobile/business/transfer/b;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/business/transfer/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 15
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/form/a$a;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/business/transfer/payment/form/a$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/transfer/payment/form/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/payment/form/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/payment/form/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
