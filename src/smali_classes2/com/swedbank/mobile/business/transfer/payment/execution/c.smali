.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/c;
.super Ljava/lang/Object;
.source "ExecutePreparedPayment.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Ljava/lang/String;",
        "Lio/reactivex/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/a;

.field private final b:Lcom/swedbank/mobile/business/transfer/payment/execution/o;

.field private final c:Lcom/swedbank/mobile/business/e/n;

.field private final d:Lcom/swedbank/mobile/business/e/b;

.field private final e:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/business/transfer/payment/execution/o;Lcom/swedbank/mobile/business/e/n;Lcom/swedbank/mobile/business/e/b;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/execution/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/e/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "execute_payment_impl"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a;",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/o;",
            "Lcom/swedbank/mobile/business/e/n;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "businessTree"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationProvider"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "systemNotificationRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executePaymentImpl"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->a:Lcom/swedbank/mobile/architect/business/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/o;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->c:Lcom/swedbank/mobile/business/e/n;

    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->d:Lcom/swedbank/mobile/business/e/b;

    iput-object p5, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->e:Lkotlin/e/a/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/architect/business/a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->a:Lcom/swedbank/mobile/architect/business/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->d:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/e/n;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->c:Lcom/swedbank/mobile/business/e/n;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/transfer/payment/execution/o;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/o;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->e:Lkotlin/e/a/b;

    invoke-interface {v0, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/j;

    .line 29
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/c;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "executePaymentImpl(input\u2026  }\n            }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
