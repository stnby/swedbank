.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/execution/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/u;)Lio/reactivex/aa;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/u;",
            ")",
            "Lio/reactivex/aa<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    const-string v0, "pinChallengeState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/u$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->n(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/v;

    .line 265
    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->b:Ljava/lang/String;

    .line 266
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/u$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/u$b;->a()Ljava/lang/String;

    move-result-object p1

    .line 264
    invoke-direct {v1, v2, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/v;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 267
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    check-cast p1, Lio/reactivex/aa;

    goto :goto_0

    .line 276
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/u$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/u$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 277
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->j(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/execution/p;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/p;->b()V

    .line 278
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 279
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 280
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 278
    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    .line 317
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/aa;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/u;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/u;)Lio/reactivex/aa;

    move-result-object p1

    return-object p1
.end method
