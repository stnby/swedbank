.class final Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;
.super Ljava/lang/Object;
.source "StartPaymentSigning.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/ad;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/ad;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/r;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/r;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/r$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/ad;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/ad;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/ad;)Lcom/swedbank/mobile/business/transfer/payment/c;

    move-result-object v0

    .line 25
    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/util/r$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/r$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/c;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a$1;-><init>(Lcom/swedbank/mobile/business/util/r;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRepository\n      \u2026        }\n              }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/r$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 59
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/t$a;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 60
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/util/r;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;->a(Lcom/swedbank/mobile/business/util/r;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
