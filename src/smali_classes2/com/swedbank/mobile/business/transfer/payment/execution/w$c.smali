.class final Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;
.super Ljava/lang/Object;
.source "PollPaymentSigningStatus.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/w;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/y;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/s;)Lcom/swedbank/mobile/business/transfer/payment/execution/ac;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/s$b;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/x;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 29
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "In progress polling status should not be propagated"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 28
    :pswitch_1
    sget-object p1, Lcom/swedbank/mobile/business/transfer/payment/execution/ac$b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/ac$b;

    .line 27
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/ac;

    goto :goto_0

    .line 31
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s$a;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/ac$a;

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/s$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ac$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/ac;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/s;)Lcom/swedbank/mobile/business/transfer/payment/execution/ac;

    move-result-object p1

    return-object p1
.end method
