.class final Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;
.super Ljava/lang/Object;
.source "ExecutePreparedPayment.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a(Lkotlin/k;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Boolean;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/f;
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "relevantPaymentFormStillActive"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/e/b;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 39
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    instance-of p1, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->c(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/e/n;

    move-result-object p1

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->d(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/transfer/payment/execution/o;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    .line 43
    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 41
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/o;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/e/m;

    move-result-object v0

    .line 40
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/e/n;->a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 45
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    iget-object p1, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->c(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/e/n;

    move-result-object p1

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->d(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/business/transfer/payment/execution/o;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    .line 49
    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 47
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/o;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/e/m;

    move-result-object v0

    .line 46
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/e/n;->a(Lcom/swedbank/mobile/business/e/m;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;->a(Ljava/lang/Boolean;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
