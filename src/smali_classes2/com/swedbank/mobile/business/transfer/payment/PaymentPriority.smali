.class public final enum Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
.super Ljava/lang/Enum;
.source "PaymentRepository.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

.field public static final enum INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

.field public static final enum NORMAL:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;


# instance fields
.field private final priorityLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    const-string v2, "NORMAL"

    const/4 v3, 0x0

    .line 37
    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->NORMAL:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    const-string v2, "INSTANT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->priorityLevel:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    return-object v0
.end method


# virtual methods
.method public final getPriorityLevel()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->priorityLevel:I

    return v0
.end method
