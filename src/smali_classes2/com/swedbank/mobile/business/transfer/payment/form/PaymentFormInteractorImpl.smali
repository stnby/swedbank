.class public final Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PaymentFormInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/a;
.implements Lcom/swedbank/mobile/business/general/confirmation/c;
.implements Lcom/swedbank/mobile/business/transfer/payment/execution/m;
.implements Lcom/swedbank/mobile/business/transfer/payment/form/d;
.implements Lcom/swedbank/mobile/business/transfer/payment/form/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/payment/form/i;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/a;",
        "Lcom/swedbank/mobile/business/general/confirmation/c;",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
        "Lcom/swedbank/mobile/business/transfer/payment/form/d;",
        "Lcom/swedbank/mobile/business/transfer/payment/form/h;"
    }
.end annotation


# instance fields
.field private a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field private b:Ljava/lang/String;

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/business/transfer/payment/c;

.field private final j:Lcom/swedbank/mobile/business/a/b;

.field private final k:Lcom/swedbank/mobile/business/i/d;

.field private final l:Lcom/swedbank/mobile/business/transfer/payment/form/g;

.field private final m:Lcom/swedbank/mobile/business/customer/l;

.field private final n:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/c;Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_payment_form_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/transfer/payment/form/g;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_form_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_form_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            "Lcom/swedbank/mobile/business/a/b;",
            "Lcom/swedbank/mobile/business/i/d;",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pluginManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentFormListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prefilledInput"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->i:Lcom/swedbank/mobile/business/transfer/payment/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j:Lcom/swedbank/mobile/business/a/b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->k:Lcom/swedbank/mobile/business/i/d;

    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->l:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    iput-object p5, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->m:Lcom/swedbank/mobile/business/customer/l;

    iput-object p6, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->n:Lcom/swedbank/mobile/business/util/l;

    .line 84
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Optional<AccountId>>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->c:Lcom/b/c/c;

    .line 85
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->n:Lcom/swedbank/mobile/business/util/l;

    .line 256
    sget-object p2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 254
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_0

    .line 257
    :cond_0
    instance-of p2, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz p2, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    .line 254
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 85
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 258
    :goto_0
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    .line 85
    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026(PaymentInput::currency))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->d:Lcom/b/c/b;

    .line 86
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<PaymentExecutionResult>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->e:Lcom/b/c/c;

    .line 87
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Pair\u2026put, PaymentPriority?>>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->f:Lcom/b/c/c;

    .line 88
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j:Lcom/swedbank/mobile/business/a/b;

    .line 89
    iget-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->m:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/business/a/b;->d(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 90
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->g:Lio/reactivex/o;

    .line 91
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->g:Lio/reactivex/o;

    .line 92
    new-instance p2, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "accountsStream\n      .sw\u2026  }\n            }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->h:Lio/reactivex/o;

    return-void

    .line 85
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/a/b;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j:Lcom/swedbank/mobile/business/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->m:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->d:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/form/g;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->l:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/form/i;
    .locals 0

    .line 73
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/util/l;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->n:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->c:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->i:Lcom/swedbank/mobile/business/transfer/payment/c;

    .line 173
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/c;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 232
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->d()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 181
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    const-string v2, "it"

    .line 183
    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-interface {v1, v0, p1, p2}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    .line 181
    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->b:Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->a()V

    .line 192
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->f()V

    .line 193
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->e:Lcom/b/c/c;

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 194
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x5dc

    .line 195
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/w;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.timer(1500, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$c;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;Lcom/swedbank/mobile/business/transfer/payment/execution/n;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, p1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 248
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_0

    .line 200
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    if-eq v0, v1, :cond_2

    .line 201
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;->b()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    if-ne p1, v0, :cond_3

    .line 202
    :cond_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->c()V

    :cond_3
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->d()V

    .line 221
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    if-eqz v0, :cond_0

    .line 222
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/form/j;

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/form/e;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/form/j;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 224
    :pswitch_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->f:Lcom/b/c/c;

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->NORMAL:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-static {v0, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    :pswitch_1
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->f:Lcom/b/c/c;

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-static {v0, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->c:Lcom/b/c/c;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 236
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->f()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "currency"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->d:Lcom/b/c/b;

    .line 140
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public c()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->n:Lcom/swedbank/mobile/business/util/l;

    .line 244
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    invoke-static {}, Lcom/swedbank/mobile/business/transfer/payment/b;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v0

    goto :goto_0

    .line 245
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 242
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    return-object v0

    .line 246
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public e()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j:Lcom/swedbank/mobile/business/a/b;

    .line 130
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->m:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public f()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->g:Lio/reactivex/o;

    return-object v0
.end method

.method public g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->h:Lio/reactivex/o;

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->d:Lcom/b/c/b;

    .line 143
    invoke-virtual {v0}, Lcom/b/c/b;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "selectedCurrencyStream\n \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j:Lcom/swedbank/mobile/business/a/b;

    .line 146
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->m:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->g(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public j()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->h:Lio/reactivex/o;

    .line 149
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    .line 150
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "selectedAccountStream\n  \u2026cy)\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 175
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->e:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public l()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 177
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->f:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public m()V
    .locals 4

    .line 209
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/i;->b()Lio/reactivex/w;

    move-result-object v0

    .line 210
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 250
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method protected m_()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->k:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    return-void
.end method

.method public n()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 206
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->b:Ljava/lang/String;

    return-object v0
.end method

.method public q_()V
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->l:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/g;->m()V

    return-void
.end method
