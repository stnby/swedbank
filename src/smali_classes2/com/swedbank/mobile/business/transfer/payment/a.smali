.class public final enum Lcom/swedbank/mobile/business/transfer/payment/a;
.super Ljava/lang/Enum;
.source "PaymentInput.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/transfer/payment/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/transfer/payment/a;

.field public static final enum b:Lcom/swedbank/mobile/business/transfer/payment/a;

.field public static final enum c:Lcom/swedbank/mobile/business/transfer/payment/a;

.field public static final enum d:Lcom/swedbank/mobile/business/transfer/payment/a;

.field public static final enum e:Lcom/swedbank/mobile/business/transfer/payment/a;

.field public static final enum f:Lcom/swedbank/mobile/business/transfer/payment/a;

.field public static final enum g:Lcom/swedbank/mobile/business/transfer/payment/a;

.field private static final synthetic h:[Lcom/swedbank/mobile/business/transfer/payment/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/swedbank/mobile/business/transfer/payment/a;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->a:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "SENDER_ACCOUNT_ID"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->b:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "AMOUNT"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->c:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "RECIPIENT_NAME"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->d:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "RECIPIENT_IBAN"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->e:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "DESCRIPTION"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->f:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/a;

    const-string v2, "REFERENCE_NUMBER"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/a;->g:Lcom/swedbank/mobile/business/transfer/payment/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/a;->h:[Lcom/swedbank/mobile/business/transfer/payment/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/payment/a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/transfer/payment/a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/a;->h:[Lcom/swedbank/mobile/business/transfer/payment/a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/transfer/payment/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/transfer/payment/a;

    return-object v0
.end method
