.class final Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;
.super Ljava/lang/Object;
.source "PaymentFormInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;-><init>(Lcom/swedbank/mobile/business/transfer/payment/c;Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/util/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/o;
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {p1}, Lkotlin/a/h;->d(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    .line 95
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 244
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto/16 :goto_2

    .line 245
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_7

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 242
    check-cast v0, Lcom/swedbank/mobile/business/a/a;

    .line 97
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->f(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v1

    .line 247
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 248
    :cond_1
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_6

    check-cast v1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 100
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 103
    iget-object v3, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    .line 249
    move-object v3, p1

    check-cast v3, Ljava/lang/Iterable;

    .line 250
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/swedbank/mobile/business/a/a;

    .line 249
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    .line 251
    :goto_0
    check-cast v4, Lcom/swedbank/mobile/business/a/a;

    if-eqz v4, :cond_4

    move-object v0, v2

    goto :goto_1

    .line 104
    :cond_4
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_5

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/a/a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 105
    :cond_5
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 108
    :goto_1
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 253
    :goto_2
    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    .line 111
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->g(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/b/c/c;

    move-result-object v1

    .line 112
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {v1, v0}, Lcom/b/c/c;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 114
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;Ljava/util/List;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1

    .line 106
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 108
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a(Ljava/util/List;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
