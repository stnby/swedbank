.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/aa;
.super Ljava/lang/Object;
.source "SignPaymentWithPin.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/payment/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/aa;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/v;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/aa;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    .line 15
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/v;->a()Ljava/lang/String;

    move-result-object v1

    .line 16
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/v;->b()Ljava/lang/String;

    move-result-object p1

    .line 14
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/c;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 17
    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/aa$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/aa$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRepository\n      \u2026.error)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/v;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/aa;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/v;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
