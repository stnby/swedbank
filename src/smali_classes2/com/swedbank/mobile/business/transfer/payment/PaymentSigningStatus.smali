.class public final enum Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
.super Ljava/lang/Enum;
.source "PaymentRepository.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

.field public static final enum IN_PROGRESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

.field public static final enum SIGNED:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    const-string v2, "IN_PROGRESS"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->IN_PROGRESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    const-string v2, "SIGNED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->SIGNED:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    return-object v0
.end method
