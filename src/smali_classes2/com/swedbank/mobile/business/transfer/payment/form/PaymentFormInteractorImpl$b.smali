.class final Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;
.super Ljava/lang/Object;
.source "PaymentFormInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->j()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/a/a;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/a/a;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    const-string v0, "account"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/a/b;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object p1

    .line 151
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/a/b;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 154
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/swedbank/mobile/business/a/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a(Lcom/swedbank/mobile/business/a/a;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
