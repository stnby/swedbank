.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/k;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/biometric/a/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/biometric/a/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;>;>;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->a:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->b:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->c:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->d:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->e:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->f:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->g:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->h:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->i:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->j:Ljavax/inject/Provider;

    .line 62
    iput-object p11, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->k:Ljavax/inject/Provider;

    .line 63
    iput-object p12, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->l:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/k;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/biometric/a/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/k;"
        }
    .end annotation

    .line 82
    new-instance v13, Lcom/swedbank/mobile/business/transfer/payment/execution/k;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/swedbank/mobile/business/transfer/payment/execution/k;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;
    .locals 14

    .line 68
    new-instance v13, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/c;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/util/l;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/business/general/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/business/general/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->k:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->l:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/swedbank/mobile/architect/business/b;

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;-><init>(Lcom/swedbank/mobile/business/transfer/payment/c;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;)V

    return-object v13
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/k;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    move-result-object v0

    return-object v0
.end method
