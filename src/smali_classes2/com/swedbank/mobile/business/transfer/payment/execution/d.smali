.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/d;
.super Ljava/lang/Object;
.source "ExecutePreparedPayment_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/o;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/n;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->a:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->b:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->c:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->d:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->e:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/execution/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Lio/reactivex/j<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;>;>;>;)",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/d;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/swedbank/mobile/business/transfer/payment/execution/d;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/transfer/payment/execution/c;
    .locals 7

    .line 39
    new-instance v6, Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/business/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/execution/o;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/e/n;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/e/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;-><init>(Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/business/transfer/payment/execution/o;Lcom/swedbank/mobile/business/e/n;Lcom/swedbank/mobile/business/e/b;Lkotlin/e/a/b;)V

    return-object v6
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/d;->a()Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    move-result-object v0

    return-object v0
.end method
