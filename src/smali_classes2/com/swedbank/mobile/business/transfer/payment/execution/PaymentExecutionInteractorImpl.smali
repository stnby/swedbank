.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/challenge/d;
.implements Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/p;",
        ">;",
        "Lcom/swedbank/mobile/business/challenge/d;",
        "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/u;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/transfer/payment/c;

.field private final d:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

.field private final h:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Lcom/swedbank/mobile/business/general/a;

.field private final k:Lcom/swedbank/mobile/business/general/a;

.field private final l:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;"
        }
    .end annotation
.end field

.field private final m:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/biometric/a/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final n:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/c;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/business/general/a;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_execution_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_execution_uuid"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_execution_priority"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/transfer/payment/execution/m;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_execution_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "startPaymentSigningUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "pollPaymentSigningStatusUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/general/a;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_signing_poll_data"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/general/a;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_payment_signing_poll_data"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "signPaymentWithPinUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "signPaymentWithBiometricsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "executePaymentUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/c;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;",
            "Lcom/swedbank/mobile/business/general/a;",
            "Lcom/swedbank/mobile/business/general/a;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/v;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/biometric/a/a;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ">;>;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentExecutionUuid"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputPriority"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startPaymentSigning"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pollPaymentSigningStatus"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pollData"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricPollData"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signPaymentWithPin"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signPaymentWithBiometrics"

    invoke-static {p11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executePayment"

    invoke-static {p12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/payment/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->e:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->f:Lcom/swedbank/mobile/business/util/l;

    iput-object p5, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->g:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    iput-object p6, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/b;

    iput-object p7, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/b;

    iput-object p8, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->j:Lcom/swedbank/mobile/business/general/a;

    iput-object p9, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->k:Lcom/swedbank/mobile/business/general/a;

    iput-object p10, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->l:Lcom/swedbank/mobile/architect/business/b;

    iput-object p11, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->m:Lcom/swedbank/mobile/architect/business/b;

    iput-object p12, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->n:Lcom/swedbank/mobile/architect/business/b;

    .line 61
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->a:Lcom/b/c/c;

    .line 62
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<PinChallengeState>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->b:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/util/l;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->f:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->e:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->n:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->h:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->i:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/general/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->k:Lcom/swedbank/mobile/business/general/a;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/payment/c;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->m:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/execution/p;
    .locals 0

    .line 46
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/execution/p;

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/general/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->j:Lcom/swedbank/mobile/business/general/a;

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->b:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->l:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->b:Lcom/b/c/c;

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/u$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/u$a;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "password"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->b:Lcom/b/c/c;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/u$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/u$b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public i()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->a:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method protected m_()V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/payment/c;

    .line 66
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/c;->b(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$h;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 87
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$i;

    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->g:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$i;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/business/transfer/payment/execution/l;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/l;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/w;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "paymentRepository\n      \u2026istener::paymentExecuted)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
