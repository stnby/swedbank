.class public final enum Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;
.super Ljava/lang/Enum;
.source "PaymentRepository.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

.field public static final enum IN_PROGRESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

.field public static final enum SUCCESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    const-string v2, "IN_PROGRESS"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;->IN_PROGRESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    const-string v2, "SUCCESS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;->SUCCESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;->$VALUES:[Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/transfer/payment/PaymentExecutionCheckStatus;

    return-object v0
.end method
