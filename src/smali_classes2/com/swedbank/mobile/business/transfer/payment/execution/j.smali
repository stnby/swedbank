.class public final synthetic Lcom/swedbank/mobile/business/transfer/payment/execution/j;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic a:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->values()[Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->SIMPLE_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->SMART_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->MOBILE_ID:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->PIN_CALCULATOR:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->UNKNOWN:Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    return-void
.end method
