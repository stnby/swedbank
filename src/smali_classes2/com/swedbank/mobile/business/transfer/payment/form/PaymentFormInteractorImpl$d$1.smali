.class final Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;
.super Ljava/lang/Object;
.source "PaymentFormInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a(Ljava/util/List;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;

.field final synthetic b:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/business/util/l;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_1

    .line 245
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    .line 242
    check-cast p1, Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;->b:Ljava/util/List;

    const-string v1, "accounts"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    check-cast v0, Ljava/lang/Iterable;

    .line 247
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/a/a;

    .line 246
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 248
    :goto_0
    check-cast v1, Lcom/swedbank/mobile/business/a/a;

    .line 116
    invoke-static {v1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 249
    :goto_1
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    return-object p1

    .line 116
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$d$1;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    return-object p1
.end method
