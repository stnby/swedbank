.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lio/reactivex/z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/e;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/z<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/x;)V
    .locals 3
    .param p1    # Lio/reactivex/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x<",
            "Lcom/swedbank/mobile/business/authentication/p;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->j(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/execution/p;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v1

    .line 214
    new-instance v2, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a$1;

    invoke-direct {v2, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$a$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/p;Lio/reactivex/x;)V

    check-cast v2, Lcom/swedbank/mobile/business/biometric/authentication/h;

    .line 212
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/p;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/biometric/authentication/h;)V

    return-void
.end method
