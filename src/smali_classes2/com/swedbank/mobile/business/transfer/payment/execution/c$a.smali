.class final Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;
.super Ljava/lang/Object;
.source "ExecutePreparedPayment.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/c;->a(Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lkotlin/k<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/c;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/b;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/c;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/c;)Lcom/swedbank/mobile/architect/business/a;

    move-result-object v1

    .line 56
    new-instance v2, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$a;

    invoke-direct {v2, v1, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$a;-><init>(Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;)V

    check-cast v2, Lio/reactivex/z;

    invoke-static {v2}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object v1

    const-string v2, "Single.create { emitter \u2026k = emitter::onSuccess)\n}"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v2, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->d(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/c$a;->a(Lkotlin/k;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
