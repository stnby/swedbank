.class final Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;
.super Ljava/lang/Object;
.source "PaymentFormInteractor.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a(Lcom/swedbank/mobile/business/a/a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/a/d;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;)V"
        }
    .end annotation

    .line 155
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1$1;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1$1;-><init>(Ljava/util/List;)V

    .line 160
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;

    iget-object v1, v1, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->c(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/b/c/b;

    move-result-object v1

    .line 242
    invoke-virtual {v1}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    check-cast v1, Lcom/swedbank/mobile/business/util/l;

    .line 244
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1$1;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    goto :goto_1

    .line 245
    :cond_0
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_5

    check-cast v1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "allBalances"

    .line 164
    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 246
    instance-of v2, p1, Ljava/util/Collection;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 247
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/a/d;

    .line 164
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/a/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    :cond_3
    :goto_0
    if-eqz v3, :cond_4

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    goto :goto_1

    .line 165
    :cond_4
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1$1;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 168
    :goto_1
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;->c(Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void

    .line 166
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 242
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 73
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/PaymentFormInteractorImpl$b$1;->a(Ljava/util/List;)V

    return-void
.end method
