.class public final Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PinChallengeInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;
.implements Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/d;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;",
        "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "pin_challenge_code"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;
        .annotation runtime Ljavax/inject/Named;
            value = "pin_challenge_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "challengeCode"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;->a()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "password"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;->a:Ljava/lang/String;

    .line 30
    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
