.class public final Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;
.super Lcom/squareup/moshi/JsonAdapter;
.source "PaymentInputJsonAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/JsonAdapter<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;"
    }
.end annotation


# instance fields
.field private final localDateAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/JsonAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/squareup/moshi/g$a;


# direct methods
.method public constructor <init>(Lcom/squareup/moshi/n;)V
    .locals 9
    .param p1    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "moshi"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/moshi/JsonAdapter;-><init>()V

    const-string v1, "senderAccountId"

    const-string v2, "amount"

    const-string v3, "currency"

    const-string v4, "recipientName"

    const-string v5, "recipientIban"

    const-string v6, "description"

    const-string v7, "referenceNumber"

    const-string v8, "paymentDate"

    .line 17
    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/moshi/g$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/g$a;

    move-result-object v0

    const-string v1, "JsonReader.Options.of(\"s\u2026ceNumber\", \"paymentDate\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    .line 20
    const-class v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "senderAccountId"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<String?>(S\u2026Set(), \"senderAccountId\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 23
    const-class v0, Ljava/math/BigDecimal;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    const-string v1, "moshi.adapter<BigDecimal\u2026ons.emptySet(), \"amount\")"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    .line 26
    const-class v0, Lorg/threeten/bp/LocalDate;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v1

    const-string v2, "paymentDate"

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/String;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object p1

    const-string v0, "moshi.adapter<LocalDate>\u2026mptySet(), \"paymentDate\")"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->localDateAdapter:Lcom/squareup/moshi/JsonAdapter;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/g;)Ljava/lang/Object;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 2
    .param p1    # Lcom/squareup/moshi/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->c()Lcom/squareup/moshi/l;

    const-string v0, "senderAccountId"

    .line 104
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 105
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "amount"

    .line 106
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 107
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "currency"

    .line 108
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 109
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "recipientName"

    .line 110
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 111
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "recipientIban"

    .line 112
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 113
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "description"

    .line 114
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 115
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "referenceNumber"

    .line 116
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 117
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    const-string v0, "paymentDate"

    .line 118
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/l;->a(Ljava/lang/String;)Lcom/squareup/moshi/l;

    .line 119
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->localDateAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->j()Lorg/threeten/bp/LocalDate;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V

    .line 120
    invoke-virtual {p1}, Lcom/squareup/moshi/l;->d()Lcom/squareup/moshi/l;

    return-void

    .line 101
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value was null! Wrap in .nullSafe() to write nullable values."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/l;Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p2, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->a(Lcom/squareup/moshi/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    return-void
.end method

.method public b(Lcom/squareup/moshi/g;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
    .locals 30
    .param p1    # Lcom/squareup/moshi/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "reader"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 31
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 33
    move-object v4, v2

    check-cast v4, Ljava/math/BigDecimal;

    .line 45
    check-cast v2, Lorg/threeten/bp/LocalDate;

    .line 46
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->e()V

    const/4 v5, 0x0

    move-object/from16 v16, v2

    move-object v7, v3

    move-object v9, v7

    move-object v11, v9

    move-object v13, v11

    move-object v15, v13

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    .line 47
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->g()Z

    move-result v17

    if-eqz v17, :cond_1

    move-object/from16 v18, v3

    .line 48
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->options:Lcom/squareup/moshi/g$a;

    invoke-virtual {v1, v3}, Lcom/squareup/moshi/g;->a(Lcom/squareup/moshi/g$a;)I

    move-result v3

    const/16 v17, 0x1

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_1

    .line 77
    :pswitch_0
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->localDateAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/threeten/bp/LocalDate;

    if-eqz v3, :cond_0

    move-object/from16 v16, v3

    goto/16 :goto_1

    :cond_0
    new-instance v2, Lcom/squareup/moshi/JsonDataException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Non-null value \'paymentDate\' was null at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 74
    :pswitch_1
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v15, v3

    move-object/from16 v3, v18

    const/4 v14, 0x1

    goto :goto_0

    .line 70
    :pswitch_2
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v13, v3

    move-object/from16 v3, v18

    const/4 v12, 0x1

    goto :goto_0

    .line 66
    :pswitch_3
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v11, v3

    move-object/from16 v3, v18

    const/4 v10, 0x1

    goto :goto_0

    .line 62
    :pswitch_4
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v9, v3

    move-object/from16 v3, v18

    const/4 v8, 0x1

    goto :goto_0

    .line 58
    :pswitch_5
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v7, v3

    move-object/from16 v3, v18

    const/4 v6, 0x1

    goto/16 :goto_0

    .line 54
    :pswitch_6
    iget-object v2, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableBigDecimalAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    move-object v4, v2

    move-object/from16 v3, v18

    const/4 v2, 0x1

    goto/16 :goto_0

    .line 50
    :pswitch_7
    iget-object v3, v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInputJsonAdapter;->nullableStringAdapter:Lcom/squareup/moshi/JsonAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/moshi/JsonAdapter;->a(Lcom/squareup/moshi/g;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x1

    goto/16 :goto_0

    .line 80
    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->j()V

    .line 81
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->q()V

    :goto_1
    move-object/from16 v3, v18

    goto/16 :goto_0

    :cond_1
    move-object/from16 v18, v3

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/moshi/g;->f()V

    .line 86
    new-instance v17, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0xff

    const/16 v29, 0x0

    move-object/from16 v19, v17

    invoke-direct/range {v19 .. v29}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;ILkotlin/e/b/g;)V

    if-eqz v5, :cond_2

    goto :goto_2

    .line 88
    :cond_2
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v18, v3

    :goto_2
    if-eqz v2, :cond_3

    :goto_3
    move-object/from16 v19, v4

    goto :goto_4

    .line 89
    :cond_3
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v4

    goto :goto_3

    :goto_4
    if-eqz v6, :cond_4

    :goto_5
    move-object/from16 v20, v7

    goto :goto_6

    .line 90
    :cond_4
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :goto_6
    if-eqz v8, :cond_5

    :goto_7
    move-object/from16 v21, v9

    goto :goto_8

    .line 91
    :cond_5
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v9

    goto :goto_7

    :goto_8
    if-eqz v10, :cond_6

    :goto_9
    move-object/from16 v22, v11

    goto :goto_a

    .line 92
    :cond_6
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v11

    goto :goto_9

    :goto_a
    if-eqz v12, :cond_7

    :goto_b
    move-object/from16 v23, v13

    goto :goto_c

    .line 93
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v13

    goto :goto_b

    :goto_c
    if-eqz v14, :cond_8

    :goto_d
    move-object/from16 v24, v15

    goto :goto_e

    .line 94
    :cond_8
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v15

    goto :goto_d

    :goto_e
    if-eqz v16, :cond_9

    :goto_f
    move-object/from16 v25, v16

    goto :goto_10

    .line 95
    :cond_9
    invoke-virtual/range {v17 .. v17}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v16

    goto :goto_f

    .line 87
    :goto_10
    invoke-virtual/range {v17 .. v25}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->a(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v1

    return-object v1

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "GeneratedJsonAdapter(PaymentInput)"

    return-object v0
.end method
