.class final Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a$1;
.super Ljava/lang/Object;
.source "StartPaymentSigning.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;->a(Lcom/swedbank/mobile/business/util/r;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/util/r;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/util/r;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a$1;->a:Lcom/swedbank/mobile/business/util/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/r;)Lcom/swedbank/mobile/business/transfer/payment/execution/t;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/r$b;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;

    .line 29
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a$1;->a:Lcom/swedbank/mobile/business/util/r;

    check-cast v1, Lcom/swedbank/mobile/business/util/r$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/r$b;->a()Ljava/lang/String;

    move-result-object v1

    .line 30
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/r$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/r$b;->b()Ljava/lang/String;

    move-result-object v2

    .line 31
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/r$b;->c()Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    move-result-object v3

    .line 32
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/r$b;->a()Ljava/lang/String;

    move-result-object p1

    .line 28
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/execution/t;

    goto :goto_0

    .line 33
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/r$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 59
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/t$a;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/execution/t;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/r;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a$1;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/r;)Lcom/swedbank/mobile/business/transfer/payment/execution/t;

    move-result-object p1

    return-object p1
.end method
