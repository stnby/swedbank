.class final Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;
.super Ljava/lang/Object;
.source "PollPaymentSigningStatus.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/w;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/y;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/s;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s$b;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/s$b;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;->IN_PROGRESS:Lcom/swedbank/mobile/business/transfer/payment/PaymentSigningStatus;

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/s;)Z

    move-result p1

    return p1
.end method
