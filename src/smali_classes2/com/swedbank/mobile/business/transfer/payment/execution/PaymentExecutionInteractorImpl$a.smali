.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/t;)Lio/reactivex/w;
    .locals 12
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->c()Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/transfer/payment/execution/j;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/SigningMethod;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 354
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 124
    :pswitch_0
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 125
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 126
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 124
    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    .line 354
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    .line 120
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;ILjava/lang/Object;)Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    move-result-object v1

    .line 121
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->a()Ljava/lang/String;

    move-result-object v2

    .line 122
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->d()Ljava/lang/String;

    move-result-object p1

    .line 346
    new-instance v3, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$c;

    invoke-direct {v3, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Ljava/lang/String;)V

    check-cast v3, Ljava/util/concurrent/Callable;

    invoke-static {v3}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object p1

    .line 345
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->m(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/b/c/c;

    move-result-object v3

    .line 352
    invoke-virtual {v3}, Lcom/b/c/c;->j()Lio/reactivex/w;

    move-result-object v3

    .line 351
    new-instance v4, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;

    invoke-direct {v4, v0, v2, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    check-cast v0, Lio/reactivex/aa;

    .line 345
    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Completable\n      .fromC\u2026           }\n          })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 114
    :pswitch_2
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    .line 115
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;ILjava/lang/Object;)Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    move-result-object v1

    .line 116
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->a()Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->d()Ljava/lang/String;

    move-result-object v3

    .line 118
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->b()Ljava/lang/String;

    move-result-object p1

    .line 328
    new-instance v4, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$e;

    invoke-direct {v4, v0, v3, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v4, Ljava/util/concurrent/Callable;

    invoke-static {v4}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object p1

    .line 327
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->f(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v3

    new-instance v4, Lcom/swedbank/mobile/business/transfer/payment/execution/y;

    .line 338
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->k(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/general/a;

    move-result-object v5

    .line 327
    invoke-direct {v4, v2, v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/y;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/general/a;)V

    invoke-interface {v3, v4}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/w;

    .line 336
    new-instance v3, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$f;

    invoke-direct {v3, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 327
    invoke-virtual {p1, v2}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 326
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->l(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 325
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 339
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v1

    .line 340
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v2

    .line 325
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lio/reactivex/o;->f(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Completable\n      .fromC\u2026   errors = emptyList()))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 110
    :pswitch_3
    iget-object v4, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    .line 111
    iget-object v5, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->a()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0xb

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;ILjava/lang/Object;)Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    move-result-object v5

    .line 112
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->a()Ljava/lang/String;

    move-result-object v6

    .line 113
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/t$b;->d()Ljava/lang/String;

    move-result-object v2

    .line 318
    invoke-static {v4}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->h(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/transfer/payment/c;

    move-result-object p1

    .line 321
    invoke-interface {p1}, Lcom/swedbank/mobile/business/transfer/payment/c;->a()Lio/reactivex/w;

    move-result-object p1

    .line 320
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    move-object v1, v0

    move-object v3, v5

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;Lcom/swedbank/mobile/business/transfer/payment/execution/i;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRepository\n      \u2026ingle()\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 129
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/t$a;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 130
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 356
    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    .line 358
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$b;

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 359
    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v3, v1

    goto :goto_0

    .line 360
    :cond_1
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_3

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    :goto_0
    if-eqz v3, :cond_2

    .line 363
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    .line 365
    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 363
    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    goto :goto_1

    .line 367
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 369
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 367
    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    .line 371
    :goto_1
    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    .line 361
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 371
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/t;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$a;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/t;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
