.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/w;
.super Ljava/lang/Object;
.source "PollPaymentSigningStatus.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/payment/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/w;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/y;)Lio/reactivex/w;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/y;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/y;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/ac;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/w;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    .line 16
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/c;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "paymentRepository\n      \u2026en)\n      .toObservable()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/y;->b()Lcom/swedbank/mobile/business/general/a;

    move-result-object p1

    .line 40
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/w$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/w$a;-><init>(Lcom/swedbank/mobile/business/general/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->l(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 41
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/y;->a()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/y;->b()Ljava/util/concurrent/TimeUnit;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lio/reactivex/o;->c(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "repeatWhen { it.flatMap \u2026ime, pollData.delay.unit)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/w$b;

    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 23
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p1

    .line 25
    sget-object v0, Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/w$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRepository\n      \u2026.error)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/y;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/w;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/y;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
