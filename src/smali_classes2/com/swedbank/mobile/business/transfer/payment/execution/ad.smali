.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/ad;
.super Ljava/lang/Object;
.source "StartPaymentSigning.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
        "Lio/reactivex/w<",
        "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/payment/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/payment/execution/ad;)Lcom/swedbank/mobile/business/transfer/payment/c;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/i;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/t;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad;->a:Lcom/swedbank/mobile/business/transfer/payment/c;

    .line 20
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v1

    .line 21
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object p1

    .line 19
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/c;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Lio/reactivex/w;

    move-result-object p1

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/ad$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/ad;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRepository\n      \u2026ingle()\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/ad;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
