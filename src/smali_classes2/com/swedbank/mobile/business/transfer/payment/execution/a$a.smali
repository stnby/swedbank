.class final Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;
.super Ljava/lang/Object;
.source "ExecutePayment.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/a;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Boolean;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/a;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/a;Lcom/swedbank/mobile/business/transfer/payment/execution/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/f;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "isInPowerSaverMode"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a$1;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_0

    .line 32
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/a;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/a;)Lcom/swedbank/mobile/business/transfer/payment/c;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/transfer/payment/c;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/i;)Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/a$a;->a(Ljava/lang/Boolean;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
