.class public final Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;
.super Ljava/lang/Object;
.source "PinChallengeInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;->a:Ljavax/inject/Provider;

    .line 15
    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;",
            ">;)",
            "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;
    .locals 3

    .line 20
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/b;->a()Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/PinChallengeInteractorImpl;

    move-result-object v0

    return-object v0
.end method
