.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->a(Lcom/swedbank/mobile/business/authentication/p;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/a/b;)Lio/reactivex/w;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/biometric/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/a/b;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "signingResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    sget-object v0, Lcom/swedbank/mobile/business/biometric/a/b$b;->a:Lcom/swedbank/mobile/business/biometric/a/b$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->f(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/y;

    .line 338
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;

    iget-object v1, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    iget-object v1, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->e:Ljava/lang/String;

    .line 339
    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;

    iget-object v2, v2, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    iget-object v2, v2, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->g(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/business/general/a;

    move-result-object v2

    .line 334
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/y;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/general/a;)V

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 337
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "pollPaymentSigningStatus\u2026          }\n            }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 203
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/a/b$a;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 204
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 318
    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    .line 320
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$b;

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 321
    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v3, v1

    goto :goto_0

    .line 322
    :cond_1
    instance-of v2, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_3

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    :goto_0
    if-eqz v3, :cond_2

    .line 325
    new-instance v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    .line 327
    invoke-interface {p1}, Lcom/swedbank/mobile/business/util/s;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 325
    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    goto :goto_1

    .line 329
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 331
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 329
    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    .line 333
    :goto_1
    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    .line 323
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 333
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/biometric/a/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;->a(Lcom/swedbank/mobile/business/biometric/a/b;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
