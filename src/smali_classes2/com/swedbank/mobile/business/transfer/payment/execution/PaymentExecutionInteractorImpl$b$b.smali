.class public final Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;
.super Ljava/lang/Object;
.source "PaymentExecutionInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/e;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/p;)Lio/reactivex/w;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/p;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/p$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/authentication/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/p$a;->a()Ljava/security/Signature;

    move-result-object p1

    .line 318
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;

    iget-object v1, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b;->d:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    iget-object v2, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->b:Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;

    .line 319
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;->i(Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl;)Lcom/swedbank/mobile/architect/business/b;

    move-result-object v0

    new-instance v3, Lcom/swedbank/mobile/business/biometric/a/a;

    .line 324
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;->c()Ljava/lang/String;

    move-result-object v4

    .line 325
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/e$b;->a()Ljava/lang/String;

    move-result-object v2

    .line 319
    invoke-direct {v3, v4, v2, p1}, Lcom/swedbank/mobile/business/biometric/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/Signature;)V

    invoke-interface {v0, v3}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 323
    new-instance v0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;

    invoke-direct {v0, v1, p0}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b$1;-><init>(Lcom/swedbank/mobile/business/transfer/payment/execution/i;Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "signPaymentWithBiometric\u2026ingle()\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_0
    instance-of p1, p1, Lcom/swedbank/mobile/business/authentication/p$c;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    .line 185
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/execution/i;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    move-result-object v0

    .line 186
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 184
    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;Ljava/util/List;)V

    .line 317
    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/authentication/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/PaymentExecutionInteractorImpl$b$b;->a(Lcom/swedbank/mobile/business/authentication/p;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
