.class public final Lcom/swedbank/mobile/business/transfer/request/opening/b;
.super Ljava/lang/Object;
.source "PaymentRequestOpeningFlow.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/a/b;


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/authentication/f;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/business/authentication/f;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "paymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationExplanation"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/opening/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/request/opening/b;->b:Lcom/swedbank/mobile/business/authentication/f;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/business/authentication/f;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/b;->b:Lcom/swedbank/mobile/business/authentication/f;

    return-object v0
.end method
