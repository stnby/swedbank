.class final Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;
.super Ljava/lang/Object;
.source "PaymentRequestInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a(Lcom/swedbank/mobile/business/a/c;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/request/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;Lcom/swedbank/mobile/business/transfer/request/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;->b:Lcom/swedbank/mobile/business/transfer/request/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/r;)Lcom/swedbank/mobile/business/util/p;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/r$b;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;->b:Lcom/swedbank/mobile/business/transfer/request/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/r$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/r$b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    .line 128
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    invoke-static {v0, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;Lkotlin/k;)V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    .line 150
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/request/a;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 151
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->c(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/f;

    move-result-object v2

    .line 152
    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->d(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->d()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-interface {v2, v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/request/f;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/request/a;Ljava/lang/String;)V

    .line 130
    sget-object p1, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    goto :goto_0

    .line 132
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/r$a;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/util/p$a;

    check-cast p1, Lcom/swedbank/mobile/business/util/r$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/r$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/p$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/util/r;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;->a(Lcom/swedbank/mobile/business/util/r;)Lcom/swedbank/mobile/business/util/p;

    move-result-object p1

    return-object p1
.end method
