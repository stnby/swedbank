.class public final Lcom/swedbank/mobile/business/transfer/request/opening/a;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "PaymentRequestOpeningFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/transfer/request/opening/b;",
        "Lcom/swedbank/mobile/business/transfer/request/opening/g;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/transfer/request/opening/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/request/opening/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/request/opening/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/request/opening/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/request/opening/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/transfer/request/opening/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/transfer/request/opening/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcom/swedbank/mobile/business/transfer/request/opening/a$a;

    invoke-direct {v0, p1, p2}, Lcom/swedbank/mobile/business/transfer/request/opening/a$a;-><init>(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/request/opening/b;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/business/transfer/request/opening/a;->a(Lkotlin/e/a/a;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 17
    sget-object v0, Lcom/swedbank/mobile/business/transfer/request/opening/a$b;->a:Lcom/swedbank/mobile/business/transfer/request/opening/a$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 18
    sget-object v0, Lcom/swedbank/mobile/business/transfer/request/opening/a$c;->a:Lcom/swedbank/mobile/business/transfer/request/opening/a$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 19
    new-instance v0, Lcom/swedbank/mobile/business/transfer/request/opening/a$d;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/business/transfer/request/opening/a$d;-><init>(Lcom/swedbank/mobile/business/transfer/request/opening/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/transfer/request/opening/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/request/opening/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/transfer/request/opening/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
