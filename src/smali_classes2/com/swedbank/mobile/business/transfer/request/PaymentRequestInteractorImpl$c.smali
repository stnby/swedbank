.class final Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c;
.super Ljava/lang/Object;
.source "PaymentRequestInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;-><init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/transfer/request/e;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/transfer/request/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/o;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 150
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/a/c;

    .line 59
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/a/c;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 151
    :goto_0
    check-cast v1, Lcom/swedbank/mobile/business/a/c;

    if-eqz v1, :cond_2

    goto :goto_1

    .line 60
    :cond_2
    invoke-static {p1}, Lkotlin/a/h;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/a/c;

    :goto_1
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->e(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/b/c/c;

    move-result-object v1

    .line 64
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {v1, v0}, Lcom/b/c/c;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c;->a(Ljava/util/List;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
