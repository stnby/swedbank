.class public final Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PaymentRequestInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/request/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/request/f;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/request/b;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lkotlin/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/request/a;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/transfer/request/e;

.field private final f:Lcom/swedbank/mobile/business/customer/l;

.field private final g:Lcom/swedbank/mobile/business/transfer/request/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/transfer/request/e;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/transfer/request/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/request/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/transfer/request/d;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_request_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->e:Lcom/swedbank/mobile/business/transfer/request/e;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->f:Lcom/swedbank/mobile/business/customer/l;

    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->g:Lcom/swedbank/mobile/business/transfer/request/d;

    .line 53
    iget-object p2, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->f:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/business/a/b;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 54
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->a:Lio/reactivex/o;

    .line 55
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<AccountId>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->b:Lcom/b/c/c;

    .line 56
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->a:Lio/reactivex/o;

    .line 57
    new-instance p2, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)V

    check-cast p2, Lio/reactivex/c/h;

    invoke-virtual {p1, p2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "accountsStream\n      .sw\u2026d }\n            }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->c:Lio/reactivex/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lkotlin/k;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->d:Lkotlin/k;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;Lkotlin/k;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->d:Lkotlin/k;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/e;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->e:Lcom/swedbank/mobile/business/transfer/request/e;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/f;
    .locals 0

    .line 46
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/request/f;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->f:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->b:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->a:Lio/reactivex/o;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance p2, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$a;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$a;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/util/concurrent/Callable;

    invoke-static {p2}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "Single.fromCallable {\n  \u2026       ?: false\n    }\n  }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->b:Lcom/b/c/c;

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->c:Lio/reactivex/o;

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->b()Lio/reactivex/o;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 110
    new-instance v1, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;

    invoke-direct {v1, p0, p1, p2}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "observeSelectedAccount()\u2026      }\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public c()V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->g:Lcom/swedbank/mobile/business/transfer/request/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/request/d;->k()V

    return-void
.end method
