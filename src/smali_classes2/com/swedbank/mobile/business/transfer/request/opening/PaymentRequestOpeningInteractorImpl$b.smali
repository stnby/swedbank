.class public final Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;
.super Lkotlin/e/b/k;
.source "PaymentRequestOpeningInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/util/q<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/q<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;)V"
        }
    .end annotation

    .line 56
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/q$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->c(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/opening/f;

    move-result-object v0

    check-cast p1, Lcom/swedbank/mobile/business/util/q$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/q$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/request/opening/f;->c(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    goto :goto_0

    .line 57
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/q$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->d(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/b/c/c;

    move-result-object v0

    check-cast p1, Lcom/swedbank/mobile/business/util/q$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/q$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/swedbank/mobile/business/util/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;->a(Lcom/swedbank/mobile/business/util/q;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
