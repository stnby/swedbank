.class public final Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "PaymentRequestOpeningInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/request/opening/d;
.implements Lcom/swedbank/mobile/business/transfer/request/opening/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/request/opening/h;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/request/opening/d;",
        "Lcom/swedbank/mobile/business/transfer/request/opening/g;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/transfer/request/e;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/swedbank/mobile/business/transfer/request/opening/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/request/e;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/request/opening/f;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/request/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_request_opening_payment_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/request/opening/f;
        .annotation runtime Ljavax/inject/Named;
            value = "payment_request_opening_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentRequestRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/request/e;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->d:Lcom/swedbank/mobile/business/transfer/request/opening/f;

    .line 40
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<QueryResultError>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/e;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/request/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->c:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/opening/f;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->d:Lcom/swedbank/mobile/business/transfer/request/opening/f;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/b/c/c;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 80
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/e;

    move-result-object v0

    .line 92
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/request/e;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "paymentRequestRepository\u2026thData.Error(it.left()) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v1, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 93
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->d:Lcom/swedbank/mobile/business/transfer/request/opening/f;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/swedbank/mobile/business/transfer/request/opening/f$a;->a(Lcom/swedbank/mobile/business/transfer/request/opening/f;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ILjava/lang/Object;)V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 64
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/e;

    move-result-object v0

    .line 76
    invoke-static {p0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/request/e;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "paymentRequestRepository\u2026thData.Error(it.left()) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v1, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 77
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
