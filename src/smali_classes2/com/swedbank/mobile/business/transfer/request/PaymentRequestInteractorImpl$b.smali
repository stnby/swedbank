.class final Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;
.super Ljava/lang/Object;
.source "PaymentRequestInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/a/c;)Lio/reactivex/w;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/a/c;",
            ")",
            "Lio/reactivex/w<",
            "+",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selectedAccount"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    new-instance v0, Lcom/swedbank/mobile/business/transfer/request/a;

    .line 112
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/c;->b()Ljava/lang/String;

    move-result-object p1

    .line 113
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->b:Ljava/lang/String;

    const-string v2, "EUR"

    .line 115
    iget-object v3, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->c:Ljava/lang/String;

    .line 111
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/request/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lkotlin/k;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 118
    invoke-virtual {p1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/request/a;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$1;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$1;-><init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;Lkotlin/k;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/w;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.fromCallable {\n  \u2026esult.Success\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 122
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl;)Lcom/swedbank/mobile/business/transfer/request/e;

    move-result-object p1

    .line 123
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/transfer/request/e;->a(Lcom/swedbank/mobile/business/transfer/request/a;)Lio/reactivex/w;

    move-result-object p1

    .line 124
    new-instance v1, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;

    invoke-direct {v1, p0, v0}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b$2;-><init>(Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;Lcom/swedbank/mobile/business/transfer/request/a;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "paymentRequestRepository\u2026        }\n              }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/a/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/request/PaymentRequestInteractorImpl$b;->a(Lcom/swedbank/mobile/business/a/c;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
