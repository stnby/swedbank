.class public final Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;
.super Ljava/lang/Object;
.source "PaymentRequestOpeningInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lcom/swedbank/mobile/business/util/q<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;->a:Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/business/util/q$a;
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/swedbank/mobile/business/util/q$a<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/swedbank/mobile/business/util/q$a;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/util/q$a;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/request/opening/PaymentRequestOpeningInteractorImpl$a;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/business/util/q$a;

    move-result-object p1

    return-object p1
.end method
