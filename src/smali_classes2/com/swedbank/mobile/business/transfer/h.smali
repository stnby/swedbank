.class public interface abstract Lcom/swedbank/mobile/business/transfer/h;
.super Ljava/lang/Object;
.source "TransferRouter.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/e;
.implements Lcom/swedbank/mobile/business/i/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/business/transfer/h$a;
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/transfer/payment/form/h;
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/request/opening/g;
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Z)V
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcom/swedbank/mobile/architect/a/h;)V
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract b(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method
