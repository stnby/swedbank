.class public final Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferSuggestedInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/plugins/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/plugins/suggested/d;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/plugins/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/suggested/b;

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Lcom/swedbank/mobile/business/transfer/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/suggested/b;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/transfer/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/suggested/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/g;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_plugin_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "suggestedPaymentsRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->a:Lcom/swedbank/mobile/business/transfer/suggested/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;)Lcom/swedbank/mobile/business/transfer/g;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/g;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->a:Lcom/swedbank/mobile/business/transfer/suggested/b;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/suggested/b;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/plugins/suggested/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->a:Lcom/swedbank/mobile/business/transfer/suggested/b;

    .line 35
    check-cast p1, Lcom/swedbank/mobile/business/transfer/plugins/suggested/b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/plugins/suggested/b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/suggested/b;->c(Ljava/lang/String;)Lio/reactivex/j;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 36
    new-instance p1, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;)V

    move-object v4, p1

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/j;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 45
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_0

    .line 40
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/a;->a:Lcom/swedbank/mobile/business/transfer/plugins/suggested/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/g;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/i/a/c;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/g;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->a:Lcom/swedbank/mobile/business/transfer/suggested/b;

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/suggested/TransferSuggestedInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/suggested/b;->b(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
