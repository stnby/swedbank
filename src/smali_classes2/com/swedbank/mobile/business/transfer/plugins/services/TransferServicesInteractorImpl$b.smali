.class final Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;
.super Ljava/lang/Object;
.source "TransferServicesInteractor.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->i()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v1

    const-string v2, "feature_transfer_services_e_invoices"

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    sget-object v1, Lcom/swedbank/mobile/business/services/a/a;->c:Lcom/swedbank/mobile/business/services/a/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->b(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v1

    const-string v2, "feature_transfer_services_payments_list"

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;->a:Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->a(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    sget-object v1, Lcom/swedbank/mobile/business/services/a/a;->d:Lcom/swedbank/mobile/business/services/a/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;->a()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
