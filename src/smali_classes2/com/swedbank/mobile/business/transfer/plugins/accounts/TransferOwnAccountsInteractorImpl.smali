.class public final Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferOwnAccountsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/plugins/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/plugins/accounts/d;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/plugins/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/a/b;

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Lcom/swedbank/mobile/business/transfer/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/transfer/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/g;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_plugin_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/g;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/g;

    .line 35
    new-instance v12, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 36
    check-cast p1, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;->a()Ljava/lang/String;

    move-result-object v5

    .line 37
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;->c()Ljava/lang/String;

    move-result-object v6

    .line 38
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;->d()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xc7

    const/4 v11, 0x0

    move-object v1, v12

    .line 35
    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;ILkotlin/e/b/g;)V

    invoke-interface {v0, v12}, Lcom/swedbank/mobile/business/transfer/g;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    goto :goto_0

    .line 39
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/b;->a:Lcom/swedbank/mobile/business/transfer/plugins/accounts/b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/g;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/i/a/c;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/g;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/plugins/accounts/TransferOwnAccountsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->e(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
