.class public final Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferServicesInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/plugins/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/plugins/services/c;",
        ">;",
        "Lcom/swedbank/mobile/business/transfer/plugins/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/f/a;

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getIbankServiceLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getIbankServiceLink"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->a:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {v0}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.just(QueryResult.Success)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/plugins/services/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/b;

    check-cast p1, Lcom/swedbank/mobile/business/transfer/plugins/services/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/plugins/services/a;->a()Lcom/swedbank/mobile/business/services/a/a;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 49
    new-instance v0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/transfer/plugins/services/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/plugins/services/c;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 55
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    new-instance v0, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/transfer/plugins/services/TransferServicesInteractorImpl;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/o;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.fromCallable \u2026S_LIST)\n      }\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
