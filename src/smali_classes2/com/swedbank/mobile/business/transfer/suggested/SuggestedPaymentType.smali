.class public final enum Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
.super Ljava/lang/Enum;
.source "SuggestedPaymentsRepository.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

.field public static final enum INVOICE:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

.field public static final enum REGULAR:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;


# instance fields
.field private final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    new-instance v2, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    const-string v3, "REGULAR"

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 19
    invoke-direct {v2, v3, v4, v5}, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->REGULAR:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    aput-object v2, v1, v4

    new-instance v2, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    const-string v3, "INVOICE"

    invoke-direct {v2, v3, v5, v0}, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->INVOICE:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    aput-object v2, v1, v5

    sput-object v1, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->$VALUES:[Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->id:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->$VALUES:[Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    return-object v0
.end method


# virtual methods
.method public final getId()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->id:I

    return v0
.end method
