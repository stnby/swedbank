.class public final Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferDetailedInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/c;
.implements Lcom/swedbank/mobile/business/transfer/detailed/a;
.implements Lcom/swedbank/mobile/business/transfer/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/detailed/d;",
        ">;",
        "Lcom/swedbank/mobile/business/i/c;",
        "Lcom/swedbank/mobile/business/transfer/detailed/a;",
        "Lcom/swedbank/mobile/business/transfer/g;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/a/h;

.field private final b:Lcom/swedbank/mobile/business/transfer/plugins/a;

.field private final c:Lcom/swedbank/mobile/business/transfer/detailed/c;

.field private final d:Lcom/swedbank/mobile/business/i/a/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/detailed/c;Lcom/swedbank/mobile/business/i/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/detailed/c;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/i/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_plugin_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferPlugin"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/detailed/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->d:Lcom/swedbank/mobile/business/i/a/b;

    .line 39
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->d:Lcom/swedbank/mobile/business/i/a/b;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/i/a/b;->g()Lkotlin/e/a/b;

    move-result-object p1

    invoke-interface {p1, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->a:Lcom/swedbank/mobile/architect/a/h;

    .line 40
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->a:Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/plugins/a;

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/plugins/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;)Lcom/swedbank/mobile/business/i/a/b;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->d:Lcom/swedbank/mobile/business/i/a/b;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/detailed/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/detailed/c;->l()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/detailed/d;

    .line 63
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/detailed/d;->a(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/plugins/a;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/plugins/a;->a(Lcom/swedbank/mobile/business/i/a/c;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/detailed/c;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/detailed/c;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->c:Lcom/swedbank/mobile/business/transfer/detailed/c;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/detailed/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/plugins/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/plugins/a;->a()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/e;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->b:Lcom/swedbank/mobile/business/transfer/plugins/a;

    .line 49
    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/plugins/a;->i()Lio/reactivex/o;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "pluginNode\n      .observ\u2026a = it\n        ))\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected m_()V
    .locals 2

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/detailed/d;

    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/detailed/TransferDetailedInteractorImpl;->a:Lcom/swedbank/mobile/architect/a/h;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/transfer/detailed/d;->b(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method
