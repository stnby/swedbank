.class public final Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransferInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/a/d;
.implements Lcom/swedbank/mobile/business/i/c;
.implements Lcom/swedbank/mobile/business/transfer/d;
.implements Lcom/swedbank/mobile/business/transfer/detailed/c;
.implements Lcom/swedbank/mobile/business/transfer/f;
.implements Lcom/swedbank/mobile/business/transfer/g;
.implements Lcom/swedbank/mobile/business/transfer/payment/form/g;
.implements Lcom/swedbank/mobile/business/transfer/request/d;
.implements Lcom/swedbank/mobile/business/transfer/request/opening/f;
.implements Lcom/swedbank/mobile/business/transfer/search/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/transfer/h;",
        ">;",
        "Lcom/swedbank/mobile/business/i/a/d<",
        "Lcom/swedbank/mobile/business/transfer/plugins/a;",
        ">;",
        "Lcom/swedbank/mobile/business/i/c;",
        "Lcom/swedbank/mobile/business/transfer/d;",
        "Lcom/swedbank/mobile/business/transfer/detailed/c;",
        "Lcom/swedbank/mobile/business/transfer/f;",
        "Lcom/swedbank/mobile/business/transfer/g;",
        "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
        "Lcom/swedbank/mobile/business/transfer/request/d;",
        "Lcom/swedbank/mobile/business/transfer/request/opening/f;",
        "Lcom/swedbank/mobile/business/transfer/search/c;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/plugins/a;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/a/b;

.field private final f:Lcom/swedbank/mobile/business/customer/l;

.field private final g:Lcom/swedbank/mobile/business/i/d;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/i/d;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/a/b;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/business/i/d;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generalPluginManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferPlugins"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->e:Lcom/swedbank/mobile/business/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->f:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->g:Lcom/swedbank/mobile/business/i/d;

    iput-object p4, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->h:Ljava/util/List;

    .line 72
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<PaymentInput>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->c:Lcom/b/c/c;

    .line 73
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->d:Lcom/b/c/c;

    .line 76
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->h:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 79
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->h:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->b(Ljava/util/List;)V

    return-void

    .line 76
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There must be at least one transfer plugin"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 89
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 153
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 154
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 91
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/transfer/plugins/a;

    .line 92
    invoke-interface {v2}, Lcom/swedbank/mobile/business/transfer/plugins/a;->a()Lio/reactivex/w;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 156
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 158
    new-instance v0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$e;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$e;-><init>(Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/c/h;

    const/4 v2, 0x0

    .line 165
    new-array v2, v2, [Lio/reactivex/w;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, [Lio/reactivex/aa;

    .line 157
    invoke-static {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.zipArray(\n       \u2026,\n        toTypedArray())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 165
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->g:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/swedbank/mobile/business/i/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/plugins/a;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 101
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/plugins/a;->a(Lcom/swedbank/mobile/business/i/a/c;)V

    return-void

    .line 100
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 116
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 117
    invoke-static {v0, p1, v1, v2, v3}, Lcom/swedbank/mobile/business/transfer/h$a;->a(Lcom/swedbank/mobile/business/transfer/h;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->h()V

    .line 127
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->c:Lcom/b/c/c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;->b()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 128
    iget-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->d:Lcom/b/c/c;

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/h;->b(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->a:Ljava/util/List;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/transfer/plugins/a;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->b:Ljava/util/Map;

    return-void
.end method

.method public b(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentInput"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 182
    new-instance v1, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flatMapMaybe { Maybe.just(routing(it)) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/j;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/transfer/request/opening/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 183
    new-instance v1, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$b;-><init>(Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flatMapMaybe { Maybe.just(routing(it)) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public b()Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/e;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 98
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 168
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 169
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 170
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    .line 171
    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/i/a/a;

    invoke-virtual {v2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 172
    invoke-interface {v4}, Lcom/swedbank/mobile/business/i/a/a;->i()Lio/reactivex/o;

    move-result-object v4

    .line 173
    new-instance v5, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$c;

    invoke-direct {v5, v3, v2}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$c;-><init>(Ljava/lang/String;I)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 175
    sget-object v0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$d;->a:Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl$d;

    check-cast v0, Lio/reactivex/c/h;

    .line 167
    invoke-static {v1, v0}, Lio/reactivex/o;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "pluginNodes.combineLatestData()"

    .line 181
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pluginList"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/i/a/d$a;->a(Lcom/swedbank/mobile/business/i/a/d;Ljava/util/List;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->c()V

    return-void
.end method

.method public c(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 138
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 140
    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/transfer/h;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Z)V

    .line 142
    :cond_0
    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->b()V

    return-void
.end method

.method public e()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->e:Lcom/swedbank/mobile/business/a/b;

    .line 110
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->f:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->f(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public f()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->d:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->c:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public h()V
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->f()V

    return-void
.end method

.method public i()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "plugins"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public j()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/plugins/a;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    const-string v1, "pluginNodes"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public k()V
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->g()V

    return-void
.end method

.method public l()V
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->e()V

    return-void
.end method

.method public m()V
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->a()V

    return-void
.end method

.method protected m_()V
    .locals 4

    .line 83
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    .line 84
    iget-object v1, p0, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->g:Lcom/swedbank/mobile/business/i/d;

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 85
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->i()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 149
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 150
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 151
    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    .line 85
    invoke-interface {v0, v3}, Lcom/swedbank/mobile/business/transfer/h;->b(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    :cond_0
    check-cast v2, Ljava/util/List;

    return-void
.end method

.method public n()V
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/TransferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/h;->d()V

    return-void
.end method
