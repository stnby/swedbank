.class public final Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "AuthenticatedContactInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/a/e;
.implements Lcom/swedbank/mobile/business/contact/auth/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/contact/auth/c;",
        ">;",
        "Lcom/swedbank/mobile/architect/business/a/e;",
        "Lcom/swedbank/mobile/business/contact/auth/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/b/b;

.field private final b:Lcom/swedbank/mobile/business/i/d;

.field private final c:Lcom/swedbank/mobile/architect/business/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lcom/swedbank/mobile/business/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/contact/a;",
            "Lcom/swedbank/mobile/business/contact/b;",
            ">;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/c/b;

.field private final f:Lcom/swedbank/mobile/business/e/i;

.field private final g:Lcom/swedbank/mobile/business/authentication/login/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/authentication/login/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/g;
        .annotation runtime Ljavax/inject/Named;
            value = "getBankContactsUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "executeContactActionUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/authentication/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/i/d;",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lcom/swedbank/mobile/business/b/b;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/contact/a;",
            "Lcom/swedbank/mobile/business/contact/b;",
            ">;",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/business/c/b;",
            "Lcom/swedbank/mobile/business/e/i;",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pluginManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getBankContacts"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executeContactAction"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginCredentialsRepository"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->b:Lcom/swedbank/mobile/business/i/d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    iput-object p3, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/b;

    iput-object p4, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->e:Lcom/swedbank/mobile/business/c/b;

    iput-object p5, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->f:Lcom/swedbank/mobile/business/e/i;

    iput-object p6, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->g:Lcom/swedbank/mobile/business/authentication/login/g;

    .line 35
    iget-object p1, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->c:Lcom/swedbank/mobile/architect/business/g;

    invoke-interface {p1}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/b/b;

    iput-object p1, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->a:Lcom/swedbank/mobile/business/b/b;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->g:Lcom/swedbank/mobile/business/authentication/login/g;

    .line 41
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/g;->b()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/contact/a;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/contact/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "contactAction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->d:Lcom/swedbank/mobile/architect/business/b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/b/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->a:Lcom/swedbank/mobile/business/b/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/b/b;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/b/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->a:Lcom/swedbank/mobile/business/b/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/b/b;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/e/h;",
            "Lcom/swedbank/mobile/business/c/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->f:Lcom/swedbank/mobile/business/e/i;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/i;->c()Lcom/swedbank/mobile/business/e/h;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->e:Lcom/swedbank/mobile/business/c/b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/b;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    return-object v0
.end method

.method protected m_()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->b:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/contact/auth/AuthenticatedContactInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    return-void
.end method
