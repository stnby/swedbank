.class public final Lcom/swedbank/mobile/business/contact/notauth/c$a;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactRouter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/contact/notauth/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/business/contact/notauth/c;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lcom/swedbank/mobile/business/contact/notauth/c;",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/i/c;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/i/c;",
            ")",
            "Lio/reactivex/j<",
            "TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "routerClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/swedbank/mobile/business/i/e;

    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/business/i/e$a;->a(Lcom/swedbank/mobile/business/i/e;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)Lio/reactivex/j;

    move-result-object p0

    return-object p0
.end method
