.class public final Lcom/swedbank/mobile/business/contact/notauth/b;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/contact/notauth/NotAuthenticatedContactInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lcom/swedbank/mobile/business/b/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/contact/a;",
            "Lcom/swedbank/mobile/business/contact/b;",
            ">;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lcom/swedbank/mobile/business/b/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/contact/a;",
            "Lcom/swedbank/mobile/business/contact/b;",
            ">;",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->a:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->b:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->c:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->d:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->e:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/contact/notauth/b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/i/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lcom/swedbank/mobile/business/b/b;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/contact/a;",
            "Lcom/swedbank/mobile/business/contact/b;",
            ">;",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/g;",
            ">;)",
            "Lcom/swedbank/mobile/business/contact/notauth/b;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/swedbank/mobile/business/contact/notauth/b;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/contact/notauth/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/contact/notauth/NotAuthenticatedContactInteractorImpl;
    .locals 8

    .line 48
    new-instance v7, Lcom/swedbank/mobile/business/contact/notauth/NotAuthenticatedContactInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/i/d;

    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/architect/business/g;

    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/architect/business/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/c/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/e/i;

    iget-object v0, p0, Lcom/swedbank/mobile/business/contact/notauth/b;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/authentication/login/g;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/contact/notauth/NotAuthenticatedContactInteractorImpl;-><init>(Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/architect/business/g;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/authentication/login/g;)V

    return-object v7
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/contact/notauth/b;->a()Lcom/swedbank/mobile/business/contact/notauth/NotAuthenticatedContactInteractorImpl;

    move-result-object v0

    return-object v0
.end method
