.class public final Lcom/swedbank/mobile/business/contact/c;
.super Ljava/lang/Object;
.source "ExecuteContactAction.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/business/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/architect/business/b<",
        "Lkotlin/k<",
        "Lcom/swedbank/mobile/business/contact/a;",
        "Lcom/swedbank/mobile/business/contact/b;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getJumpToIbankLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/e;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "getJumpToIbankLink"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/contact/c;->a:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/k;)V
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/contact/a;",
            "+",
            "Lcom/swedbank/mobile/business/contact/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/contact/a;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/contact/b;

    .line 16
    instance-of v1, v0, Lcom/swedbank/mobile/business/contact/a$a;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/contact/a$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/contact/a$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/contact/b;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 17
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/contact/a$d;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/contact/a$d;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/contact/a$d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/contact/b;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 18
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/contact/a$b;

    if-eqz v1, :cond_2

    .line 19
    check-cast v0, Lcom/swedbank/mobile/business/contact/a$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/contact/a$b;->a()Ljava/lang/String;

    move-result-object v1

    .line 20
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/contact/a$b;->b()Ljava/lang/String;

    move-result-object v2

    .line 21
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/contact/a$b;->c()Ljava/lang/String;

    move-result-object v0

    .line 18
    invoke-interface {p1, v1, v2, v0}, Lcom/swedbank/mobile/business/contact/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 22
    :cond_2
    instance-of v1, v0, Lcom/swedbank/mobile/business/contact/a$c;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/business/contact/c;->a:Lcom/swedbank/mobile/architect/business/b;

    check-cast v0, Lcom/swedbank/mobile/business/contact/a$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/contact/a$c;->a()Lcom/swedbank/mobile/business/e;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/contact/c$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/contact/c$a;-><init>(Lcom/swedbank/mobile/business/contact/b;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, p1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    :cond_3
    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/contact/c;->a(Lkotlin/k;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
