.class public final Lcom/swedbank/mobile/business/a/d$a;
.super Ljava/lang/Object;
.source "Accounts.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/a/d;->a()Lcom/swedbank/mobile/business/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/a/d;

.field private final b:Ljava/math/BigDecimal;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/a/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 41
    iput-object p1, p0, Lcom/swedbank/mobile/business/a/d$a;->a:Lcom/swedbank/mobile/business/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/d;->d()Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/business/a/d$a;->b:Ljava/math/BigDecimal;

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/d;->c()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/a/d$a;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public c()Ljava/math/BigDecimal;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/business/a/d$a;->b:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/business/a/d$a;->c:Ljava/lang/String;

    return-object v0
.end method
