.class final Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$c;
.super Lkotlin/e/b/k;
.source "OverviewInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/overview/l;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/l;)V
    .locals 3

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->e(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/overview/o;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/swedbank/mobile/business/overview/l$d;->a:Lcom/swedbank/mobile/business/overview/l$d;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/o;->b()V

    goto :goto_0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/overview/l$e;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/overview/l$e;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/l$e;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/o;->a(Lcom/swedbank/mobile/business/util/e;)V

    goto :goto_0

    .line 92
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/business/overview/l$c;->a:Lcom/swedbank/mobile/business/overview/l$c;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/o;->c()V

    goto :goto_0

    .line 93
    :cond_2
    instance-of v1, p1, Lcom/swedbank/mobile/business/overview/l$b;

    if-eqz v1, :cond_3

    .line 94
    check-cast p1, Lcom/swedbank/mobile/business/overview/l$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/l$b;->a()Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/l$b;->b()Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/l$b;->c()Ljava/lang/String;

    move-result-object p1

    .line 93
    invoke-interface {v0, v1, v2, p1}, Lcom/swedbank/mobile/business/overview/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_3
    sget-object v1, Lcom/swedbank/mobile/business/overview/l$a;->a:Lcom/swedbank/mobile/business/overview/l$a;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/o;->a()V

    :cond_4
    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/swedbank/mobile/business/overview/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$c;->a(Lcom/swedbank/mobile/business/overview/l;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
