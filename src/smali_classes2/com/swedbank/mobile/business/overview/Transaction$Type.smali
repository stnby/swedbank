.class public final enum Lcom/swedbank/mobile/business/overview/Transaction$Type;
.super Ljava/lang/Enum;
.source "Transaction.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/overview/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/overview/Transaction$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum ATM_CASH_DEPOSIT:Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum ATM_CASH_WITHDRAWAL:Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum CURRENCY_EXCHANGE:Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum GENERAL:Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum POS:Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum RESERVATION:Lcom/swedbank/mobile/business/overview/Transaction$Type;

.field public static final enum UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/swedbank/mobile/business/overview/Transaction$Type;

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "GENERAL"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->GENERAL:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "POS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->POS:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "RESERVATION"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->RESERVATION:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "ATM_CASH_WITHDRAWAL"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->ATM_CASH_WITHDRAWAL:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "ATM_CASH_DEPOSIT"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->ATM_CASH_DEPOSIT:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    const-string v2, "CURRENCY_EXCHANGE"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->CURRENCY_EXCHANGE:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/overview/Transaction$Type;->$VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Type;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/Transaction$Type;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/overview/Transaction$Type;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/overview/Transaction$Type;->$VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$Type;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/overview/Transaction$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/overview/Transaction$Type;

    return-object v0
.end method
