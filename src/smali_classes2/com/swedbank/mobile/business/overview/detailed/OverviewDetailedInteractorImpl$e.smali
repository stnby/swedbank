.class public final Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;
.super Ljava/lang/Object;
.source "ListPluginData.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->e()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "[",
        "Ljava/lang/Object;",
        "TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;->a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a([Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # [Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/e;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "dataArray"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p1}, Lkotlin/a/b;->d([Ljava/lang/Object;)Lkotlin/i/e;

    move-result-object p1

    .line 26
    sget-object v0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e$1;->a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e$1;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p1, v0}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object p1

    .line 30
    new-instance v0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e$2;

    invoke-direct {v0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e$2;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object p1

    .line 28
    invoke-static {p1}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
