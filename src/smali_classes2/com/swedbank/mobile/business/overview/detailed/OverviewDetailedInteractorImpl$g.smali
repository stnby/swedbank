.class final Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g;
.super Ljava/lang/Object;
.source "OverviewDetailedInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->f()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g;->a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selectedAccountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g;->a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->m()Ljava/util/Map;

    move-result-object v0

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 205
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 104
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 105
    new-instance v3, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/business/overview/plugins/a$b;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/swedbank/mobile/business/overview/plugins/a;

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/overview/plugins/b;->a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 209
    new-instance p1, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g$a;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g$a;-><init>(Ljava/util/Collection;)V

    check-cast p1, Lio/reactivex/c/h;

    const/4 v0, 0x0

    .line 216
    new-array v0, v0, [Lio/reactivex/w;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, [Lio/reactivex/aa;

    .line 208
    invoke-static {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.zipArray(\n       \u2026,\n        toTypedArray())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 216
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
