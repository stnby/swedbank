.class public final Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewDetailedInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
.implements Lcom/swedbank/mobile/business/i/a/d;
.implements Lcom/swedbank/mobile/business/i/c;
.implements Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;
.implements Lcom/swedbank/mobile/business/overview/currencies/c;
.implements Lcom/swedbank/mobile/business/overview/detailed/b;
.implements Lcom/swedbank/mobile/business/overview/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/detailed/f;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
        "Lcom/swedbank/mobile/business/i/a/d<",
        "Lcom/swedbank/mobile/business/overview/plugins/b;",
        ">;",
        "Lcom/swedbank/mobile/business/i/c;",
        "Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;",
        "Lcom/swedbank/mobile/business/overview/currencies/c;",
        "Lcom/swedbank/mobile/business/overview/detailed/b;",
        "Lcom/swedbank/mobile/business/overview/k;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/overview/plugins/b;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/f/a;

.field private final f:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:Lcom/swedbank/mobile/business/customer/l;

.field private final i:Lcom/swedbank/mobile/business/i/d;

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/util/l;Ljava/lang/String;Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/i/d;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_initial_selected_account_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/a/b;",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/business/i/d;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialSelectedAccountId"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generalPluginManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewPlugins"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->e:Lcom/swedbank/mobile/business/f/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->f:Lcom/swedbank/mobile/business/util/l;

    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->g:Ljava/lang/String;

    iput-object p5, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->h:Lcom/swedbank/mobile/business/customer/l;

    iput-object p6, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->i:Lcom/swedbank/mobile/business/i/d;

    iput-object p7, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j:Ljava/util/List;

    .line 80
    iget-object p2, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->g:Ljava/lang/String;

    invoke-static {p2}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p2

    const-string p3, "BehaviorRelay.createDefa\u2026initialSelectedAccountId)"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->c:Lcom/b/c/b;

    .line 82
    iget-object p2, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->h:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/business/a/b;->d(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 83
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->d:Lio/reactivex/o;

    .line 86
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 89
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->b(Ljava/util/List;)V

    return-void

    .line 86
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There must be at least one overview plugin"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;)Lio/reactivex/o;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->d:Lio/reactivex/o;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;)Lcom/swedbank/mobile/business/overview/detailed/f;
    .locals 0

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/detailed/f;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/Transaction;)Lio/reactivex/j;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/overview/statement/details/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 247
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 199
    instance-of v2, v2, Lcom/swedbank/mobile/business/overview/plugins/statement/e;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 248
    :goto_0
    check-cast v1, Lkotlin/k;

    if-eqz v1, :cond_3

    .line 200
    invoke-virtual {v1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/b;

    if-eqz v0, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/statement/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/e;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lio/reactivex/j;

    move-result-object p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.business.overview.plugins.statement.OverviewStatementNode"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 201
    :cond_3
    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.empty()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public a()V
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/detailed/f;->b()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->i:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/swedbank/mobile/business/i/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/b;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 130
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/b;->a(Lcom/swedbank/mobile/business/i/a/c;)V

    return-void

    .line 129
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/detailed/f;->b()V

    .line 183
    check-cast p1, Lcom/swedbank/mobile/business/overview/a;

    .line 184
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/a;->a()Lcom/swedbank/mobile/business/overview/b;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/overview/detailed/c;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/overview/b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 186
    :pswitch_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/detailed/f;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :pswitch_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/detailed/f;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->c:Lcom/b/c/b;

    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->a:Ljava/util/List;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/overview/plugins/b;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->b:Ljava/util/Map;

    return-void
.end method

.method public b()V
    .locals 1

    .line 164
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    .line 165
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/detailed/f;->c()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->d:Lio/reactivex/o;

    const-wide/16 v1, 0x1

    .line 150
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "accountsStream\n        .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;Ljava/lang/String;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 240
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pluginList"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/i/a/d$a;->a(Lcom/swedbank/mobile/business/i/a/d;Ljava/util/List;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/detailed/f;->a()V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    .line 162
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/detailed/f;->d(Ljava/lang/String;)V

    return-void
.end method

.method public e()Lio/reactivex/o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/overview/detailed/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 122
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 124
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->d:Lio/reactivex/o;

    .line 125
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->m()Ljava/util/Map;

    move-result-object v1

    .line 224
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 225
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 226
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/k;

    .line 227
    invoke-virtual {v3}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/i/a/a;

    invoke-virtual {v3}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 228
    invoke-interface {v5}, Lcom/swedbank/mobile/business/i/a/a;->i()Lio/reactivex/o;

    move-result-object v5

    .line 229
    new-instance v6, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$d;

    invoke-direct {v6, v4, v3}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$d;-><init>(Ljava/lang/String;I)V

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 231
    sget-object v1, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;->a:Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$e;

    check-cast v1, Lio/reactivex/c/h;

    .line 223
    invoke-static {v2, v1}, Lio/reactivex/o;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 126
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "pluginNodes.combineLates\u2026istPluginUnmappedData>())"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    .line 239
    new-instance v2, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$c;

    invoke-direct {v2}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$c;-><init>()V

    check-cast v2, Lio/reactivex/c/c;

    .line 238
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observables\n      .combi\u2026, ::OverviewDetailedData)"

    .line 239
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->c:Lcom/b/c/b;

    .line 100
    invoke-virtual {v0}, Lcom/b/c/b;->j()Lio/reactivex/w;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "selectedAccountStream\n  \u2026ess\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Lio/reactivex/w;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 112
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->m()Ljava/util/Map;

    move-result-object v0

    .line 208
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 209
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 114
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 115
    iget-object v3, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->c:Lcom/b/c/b;

    .line 211
    invoke-virtual {v3}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 115
    check-cast v3, Ljava/lang/String;

    .line 116
    new-instance v4, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/business/overview/plugins/a$b;-><init>(Ljava/lang/String;)V

    check-cast v4, Lcom/swedbank/mobile/business/overview/plugins/a;

    invoke-interface {v2, v4}, Lcom/swedbank/mobile/business/overview/plugins/b;->b(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 212
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 214
    new-instance v0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$h;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$h;-><init>(Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/c/h;

    const/4 v2, 0x0

    .line 221
    new-array v2, v2, [Lio/reactivex/w;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    check-cast v1, [Lio/reactivex/aa;

    .line 213
    invoke-static {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.zipArray(\n       \u2026,\n        toTypedArray())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 221
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->c:Lcom/b/c/b;

    .line 135
    invoke-virtual {v0}, Lcom/b/c/b;->h()Lio/reactivex/o;

    move-result-object v0

    .line 136
    new-instance v1, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "selectedAccountStream\n  \u2026y()\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Z
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->e:Lcom/swedbank/mobile/business/f/a;

    const-string v1, "feature_overview_credit_limit_details"

    .line 146
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public j()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->c:Lcom/b/c/b;

    .line 168
    invoke-virtual {v0}, Lcom/b/c/b;->i()Lio/reactivex/j;

    move-result-object v0

    .line 169
    new-instance v1, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "selectedAccountStream\n  \u2026  }\n            }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public k()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->f:Lcom/swedbank/mobile/business/util/l;

    .line 244
    sget-object v1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_0

    .line 245
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 242
    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/e;

    .line 194
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/detailed/e;->b()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 246
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    return-void

    .line 194
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public l()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "plugins"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public m()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/overview/plugins/b;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    const-string v1, "pluginNodes"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 93
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    .line 94
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->i:Lcom/swedbank/mobile/business/i/d;

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 95
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/detailed/OverviewDetailedInteractorImpl;->l()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 204
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 205
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 206
    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    .line 95
    invoke-interface {v0, v3}, Lcom/swedbank/mobile/business/overview/detailed/f;->b(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    :cond_0
    check-cast v2, Ljava/util/List;

    return-void
.end method
