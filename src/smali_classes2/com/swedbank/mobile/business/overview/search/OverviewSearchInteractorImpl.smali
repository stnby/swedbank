.class public final Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewSearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/a/d;
.implements Lcom/swedbank/mobile/business/overview/search/a;
.implements Lcom/swedbank/mobile/business/overview/search/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/search/g;",
        ">;",
        "Lcom/swedbank/mobile/business/i/a/d<",
        "Lcom/swedbank/mobile/business/overview/search/d;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/search/a;",
        "Lcom/swedbank/mobile/business/overview/search/c;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/overview/search/d;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/overview/search/c;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/search/c;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/search/c;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_search_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_search_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/search/c;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchPlugins"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/overview/search/c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->d:Ljava/util/List;

    .line 48
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->d:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->d:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->b(Ljava/util/List;)V

    return-void

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There must be at least one overview search plugin"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 95
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 96
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    .line 62
    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/search/d;

    .line 63
    invoke-interface {v2, p1}, Lcom/swedbank/mobile/business/overview/search/d;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 61
    invoke-static {v1}, Lio/reactivex/b;->a(Ljava/lang/Iterable;)Lio/reactivex/b;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable\n      .merge\u2026\n      .onErrorComplete()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->c:Lcom/swedbank/mobile/business/overview/search/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/search/c;->a()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/search/g;

    .line 87
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/search/g;->a(Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 122
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/k;

    invoke-virtual {v1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/overview/search/d;

    .line 84
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/overview/search/d;->a(Lcom/swedbank/mobile/business/i/a/c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->a:Ljava/util/List;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/overview/search/d;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->b:Ljava/util/Map;

    return-void
.end method

.method public b()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 99
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 100
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    .line 69
    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/search/d;

    .line 70
    invoke-interface {v2}, Lcom/swedbank/mobile/business/overview/search/d;->b()Lio/reactivex/b;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 68
    invoke-static {v1}, Lio/reactivex/b;->a(Ljava/lang/Iterable;)Lio/reactivex/b;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lio/reactivex/b;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable\n      .merge\u2026\n      .onErrorComplete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pluginList"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/i/a/d$a;->a(Lcom/swedbank/mobile/business/i/a/d;Ljava/util/List;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/e;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 74
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 105
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 106
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    .line 107
    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/i/a/a;

    invoke-virtual {v2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 108
    invoke-interface {v4}, Lcom/swedbank/mobile/business/i/a/a;->i()Lio/reactivex/o;

    move-result-object v4

    .line 109
    new-instance v5, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl$a;

    invoke-direct {v5, v3, v2}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl$a;-><init>(Ljava/lang/String;I)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 111
    sget-object v0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl$b;

    check-cast v0, Lio/reactivex/c/h;

    .line 103
    invoke-static {v1, v0}, Lio/reactivex/o;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "pluginNodes.combineLatestData()"

    .line 117
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 77
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 119
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 120
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    .line 78
    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/search/d;

    .line 79
    invoke-interface {v2}, Lcom/swedbank/mobile/business/overview/search/d;->a()Lio/reactivex/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 77
    invoke-static {v1}, Lio/reactivex/o;->b(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .merge(\u2026erridingInputs()\n      })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "plugins"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public j()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/overview/search/d;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    const-string v1, "pluginNodes"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/search/g;

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/search/OverviewSearchInteractorImpl;->i()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 91
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 92
    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    .line 56
    invoke-interface {v0, v3}, Lcom/swedbank/mobile/business/overview/search/g;->b(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    :cond_0
    check-cast v2, Ljava/util/List;

    return-void
.end method
