.class public final Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewCombinedInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/i/a/d;
.implements Lcom/swedbank/mobile/business/i/c;
.implements Lcom/swedbank/mobile/business/overview/combined/a;
.implements Lcom/swedbank/mobile/business/overview/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/combined/c;",
        ">;",
        "Lcom/swedbank/mobile/business/i/a/d<",
        "Lcom/swedbank/mobile/business/overview/plugins/b;",
        ">;",
        "Lcom/swedbank/mobile/business/i/c;",
        "Lcom/swedbank/mobile/business/overview/combined/a;",
        "Lcom/swedbank/mobile/business/overview/k;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/overview/plugins/b;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/i/d;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/d;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/i/d;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "generalPluginManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewPlugins"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->c:Lcom/swedbank/mobile/business/i/d;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->d:Ljava/util/List;

    .line 44
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->d:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 47
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->d:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->b(Ljava/util/List;)V

    return-void

    .line 44
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "There must be at least one overview plugin"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/Transaction;)Lio/reactivex/j;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/overview/statement/details/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 143
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 89
    instance-of v2, v2, Lcom/swedbank/mobile/business/overview/plugins/statement/e;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 144
    :goto_0
    check-cast v1, Lkotlin/k;

    if-eqz v1, :cond_3

    .line 90
    invoke-virtual {v1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/b;

    if-eqz v0, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/statement/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/e;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lio/reactivex/j;

    move-result-object p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.business.overview.plugins.statement.OverviewStatementNode"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 91
    :cond_3
    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.empty()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public a()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 57
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 98
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 59
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 60
    sget-object v3, Lcom/swedbank/mobile/business/overview/plugins/a$a;->a:Lcom/swedbank/mobile/business/overview/plugins/a$a;

    check-cast v3, Lcom/swedbank/mobile/business/overview/plugins/a;

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/overview/plugins/b;->a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 102
    new-instance v0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$c;-><init>(Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/c/h;

    const/4 v2, 0x0

    .line 109
    new-array v2, v2, [Lio/reactivex/w;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, [Lio/reactivex/aa;

    .line 101
    invoke-static {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.zipArray(\n       \u2026,\n        toTypedArray())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 109
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/swedbank/mobile/architect/business/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginRouter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->c:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/architect/business/e;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/swedbank/mobile/business/i/a/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/b;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 85
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/b;->a(Lcom/swedbank/mobile/business/i/a/c;)V

    return-void

    .line 84
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->a:Ljava/util/List;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/overview/plugins/b;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->b:Ljava/util/Map;

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 111
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 68
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 69
    sget-object v3, Lcom/swedbank/mobile/business/overview/plugins/a$a;->a:Lcom/swedbank/mobile/business/overview/plugins/a$a;

    check-cast v3, Lcom/swedbank/mobile/business/overview/plugins/a;

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/overview/plugins/b;->b(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 115
    new-instance v0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$d;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$d;-><init>(Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/c/h;

    const/4 v2, 0x0

    .line 122
    new-array v2, v2, [Lio/reactivex/w;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, [Lio/reactivex/aa;

    .line 114
    invoke-static {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;[Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.zipArray(\n       \u2026,\n        toTypedArray())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 122
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/i/a/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pluginList"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/i/a/d$a;->a(Lcom/swedbank/mobile/business/i/a/d;Ljava/util/List;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/i/a/e;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 75
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 126
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 127
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    .line 128
    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/i/a/a;

    invoke-virtual {v2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 129
    invoke-interface {v4}, Lcom/swedbank/mobile/business/i/a/a;->i()Lio/reactivex/o;

    move-result-object v4

    .line 130
    new-instance v5, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$a;

    invoke-direct {v5, v3, v2}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$a;-><init>(Ljava/lang/String;I)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 131
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 132
    sget-object v0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl$b;

    check-cast v0, Lio/reactivex/c/h;

    .line 124
    invoke-static {v1, v0}, Lio/reactivex/o;->a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "pluginNodes.combineLatestData()"

    .line 138
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 78
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j()Ljava/util/Map;

    move-result-object v0

    .line 139
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 140
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 80
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/k;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/b;

    .line 81
    invoke-interface {v2}, Lcom/swedbank/mobile/business/overview/plugins/b;->c()Lio/reactivex/o;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 78
    invoke-static {v1}, Lio/reactivex/o;->b(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .merge(\u2026shRequests()\n          })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "plugins"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public j()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/overview/plugins/b;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    const-string v1, "pluginNodes"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected m_()V
    .locals 4

    .line 51
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/combined/c;

    .line 52
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->c:Lcom/swedbank/mobile/business/i/d;

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    .line 53
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/combined/OverviewCombinedInteractorImpl;->i()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 93
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 94
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 95
    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    .line 53
    invoke-interface {v0, v3}, Lcom/swedbank/mobile/business/overview/combined/c;->b(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_0
    check-cast v2, Ljava/util/List;

    return-void
.end method
