.class final Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;
.super Ljava/lang/Object;
.source "OverviewRemoteSearchInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->b()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;)Lio/reactivex/f;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "lastInfo"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;->b()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->c(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/search/e;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->d(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;->a()Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;->b()Ljava/lang/String;

    move-result-object v3

    .line 62
    invoke-interface {v0, v1, v2, v3}, Lcom/swedbank/mobile/business/overview/search/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$1;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$1;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;-><init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    goto :goto_1

    .line 79
    :cond_1
    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object p1

    check-cast p1, Lio/reactivex/f;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;)Lio/reactivex/f;

    move-result-object p1

    return-object p1
.end method
