.class public final Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;
.super Ljava/lang/Object;
.source "OverviewStatementInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/w;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->d(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/n;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->e(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/overview/n;->a(Ljava/lang/String;Ljava/util/List;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;->a(Ljava/util/List;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
