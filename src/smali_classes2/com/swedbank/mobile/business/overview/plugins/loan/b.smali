.class public final Lcom/swedbank/mobile/business/overview/plugins/loan/b;
.super Ljava/lang/Object;
.source "OverviewLoanOfferInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/b;->a:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/b;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/plugins/loan/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/business/overview/plugins/loan/b;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/loan/b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/loan/b;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;
    .locals 3

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;

    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/customer/l;

    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/b;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/b;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;-><init>(Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/architect/business/b;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/loan/b;->a()Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;

    move-result-object v0

    return-object v0
.end method
