.class final Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;
.super Ljava/lang/Object;
.source "OverviewStatementInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->i()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/overview/plugins/statement/c;",
            ">;"
        }
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;Lcom/swedbank/mobile/business/overview/plugins/a;)V

    check-cast v0, Lkotlin/e/a/q;

    .line 127
    instance-of v1, p1, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    if-eqz v1, :cond_0

    sget-object v1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 128
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->h(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/a/a;

    move-result-object v1

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/a$b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/overview/a/a;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v1

    .line 129
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->h(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/a/a;

    move-result-object v2

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/a$b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/swedbank/mobile/business/overview/a/a;->c(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 130
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->c(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/b/c/b;

    move-result-object v2

    check-cast v2, Lio/reactivex/o;

    .line 162
    check-cast v1, Lio/reactivex/s;

    check-cast p1, Lio/reactivex/s;

    check-cast v2, Lio/reactivex/s;

    .line 163
    new-instance v3, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$a;

    invoke-direct {v3, v0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$a;-><init>(Lkotlin/e/a/q;)V

    check-cast v3, Lio/reactivex/c/i;

    .line 162
    invoke-static {v1, p1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/i;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 132
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/business/overview/plugins/a$a;->a:Lcom/swedbank/mobile/business/overview/plugins/a$a;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 133
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->h(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/a/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/overview/a/a;->a()Lio/reactivex/o;

    move-result-object p1

    .line 134
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->f(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lio/reactivex/o;

    move-result-object v1

    new-instance v2, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$1;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "accountIdsStream.switchM\u2026Changed()\n              }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->c(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/b/c/b;

    move-result-object v2

    check-cast v2, Lio/reactivex/o;

    .line 164
    check-cast p1, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    check-cast v2, Lio/reactivex/s;

    .line 165
    new-instance v3, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$b;

    invoke-direct {v3, v0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$b;-><init>(Lkotlin/e/a/q;)V

    check-cast v3, Lio/reactivex/c/i;

    .line 164
    invoke-static {p1, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/i;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    .line 165
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
