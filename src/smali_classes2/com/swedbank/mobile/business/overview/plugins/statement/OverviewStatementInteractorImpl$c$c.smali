.class final Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;
.super Lkotlin/e/b/k;
.source "OverviewStatementInteractor.kt"

# interfaces
.implements Lkotlin/e/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/q<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/overview/Transaction;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lkotlin/s;",
        "Lcom/swedbank/mobile/business/overview/plugins/statement/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;

.field final synthetic b:Lcom/swedbank/mobile/business/overview/plugins/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;Lcom/swedbank/mobile/business/overview/plugins/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;->b:Lcom/swedbank/mobile/business/overview/plugins/a;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ZLkotlin/s;)Lcom/swedbank/mobile/business/overview/plugins/statement/c;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;Z",
            "Lkotlin/s;",
            ")",
            "Lcom/swedbank/mobile/business/overview/plugins/statement/c;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transactions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 2>"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object p3, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;

    iget-object p3, p3, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {p3}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->b(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Ljava/util/Set;

    move-result-object p3

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;->b:Lcom/swedbank/mobile/business/overview/plugins/a;

    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    .line 121
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 124
    :cond_1
    :goto_0
    iget-object p3, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;

    iget-object p3, p3, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    invoke-static {p3}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->i(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Z

    move-result p3

    .line 120
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/statement/c;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/swedbank/mobile/business/overview/plugins/statement/c;-><init>(ZLjava/util/List;ZZ)V

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    check-cast p3, Lkotlin/s;

    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c$c;->a(Ljava/util/List;ZLkotlin/s;)Lcom/swedbank/mobile/business/overview/plugins/statement/c;

    move-result-object p1

    return-object p1
.end method
