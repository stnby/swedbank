.class public final Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewAccountsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
.implements Lcom/swedbank/mobile/business/overview/detailed/e;
.implements Lcom/swedbank/mobile/business/overview/plugins/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/plugins/accounts/e;",
        ">;",
        "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
        "Lcom/swedbank/mobile/business/overview/detailed/e;",
        "Lcom/swedbank/mobile/business/overview/plugins/b;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/a/b;

.field private final d:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/l;

    .line 36
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    .line 37
    iget-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/business/a/b;->d(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 38
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->a:Lio/reactivex/o;

    .line 39
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->b:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;)Lio/reactivex/o;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->a:Lio/reactivex/o;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/accounts/e;
    .locals 0

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->c:Lcom/swedbank/mobile/business/a/b;

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->d:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/a/b;->b(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;->b()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/accounts/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/accounts/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/a;->a()Ljava/lang/String;

    move-result-object p1

    .line 98
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;)Lio/reactivex/o;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v1

    const-string v0, "accountsStream\n        .firstElement()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 109
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/j;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 111
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    goto :goto_0

    .line 51
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/accounts/b;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/accounts/b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/b;->a()Ljava/lang/String;

    move-result-object p1

    .line 114
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;)Lio/reactivex/o;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object v1

    const-string v0, "accountsStream\n        .firstElement()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 122
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/j;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 124
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;->b()V

    .line 89
    check-cast p1, Lcom/swedbank/mobile/business/overview/a;

    .line 90
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/a;->a()Lcom/swedbank/mobile/business/overview/b;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/business/overview/plugins/accounts/c;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/overview/b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 92
    :pswitch_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :pswitch_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/b$a;->a(Lcom/swedbank/mobile/business/overview/plugins/b;Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 2

    .line 70
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;->a()V

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->b:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->a:Lio/reactivex/o;

    return-object v0
.end method
