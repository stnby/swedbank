.class final Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;
.super Ljava/lang/Object;
.source "OverviewRemoteSearchInteractor.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;)Lio/reactivex/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Lcom/swedbank/mobile/business/overview/search/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;

.field final synthetic b:Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->b:Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/search/f;)V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    const-string v1, "newResult"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->b:Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;->a()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/search/f;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->e(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/search/f;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 119
    new-instance v3, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;

    invoke-direct {v3, v1, v2}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 123
    :cond_1
    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->e(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v1, v3, v3, v2, v3}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 70
    :goto_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;

    iget-object v0, v0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 71
    :cond_2
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    .line 72
    :goto_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/search/f;->a()Ljava/util/List;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;

    iget-object v2, v2, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    invoke-static {v2}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;

    move-result-object v2

    new-instance v3, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    .line 74
    check-cast v0, Ljava/util/Collection;

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/h;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 75
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/search/f;->c()Z

    move-result p1

    .line 73
    invoke-direct {v3, v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;-><init>(Ljava/util/List;Z)V

    invoke-virtual {v2, v3}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/business/overview/search/f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a$2;->a(Lcom/swedbank/mobile/business/overview/search/f;)V

    return-void
.end method
