.class final Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$a;
.super Ljava/lang/Object;
.source "OverviewHistorySearchInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->i()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "enabled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->c(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/search/history/d;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/history/d;->a()Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 34
    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(emptyList())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
