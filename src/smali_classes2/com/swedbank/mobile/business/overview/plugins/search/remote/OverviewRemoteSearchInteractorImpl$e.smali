.class public final Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;
.super Lkotlin/e/b/k;
.source "OverviewRemoteSearchInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;->b:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;)V
    .locals 3

    .line 103
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->a()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 118
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 104
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/overview/Transaction;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 119
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/overview/Transaction;

    if-eqz v0, :cond_2

    .line 105
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->f(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;->a(Lcom/swedbank/mobile/business/overview/Transaction;)V

    :cond_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
