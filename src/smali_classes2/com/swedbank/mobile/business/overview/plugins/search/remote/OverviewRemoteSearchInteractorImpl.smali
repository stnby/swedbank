.class public final Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewRemoteSearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/search/d;
.implements Lcom/swedbank/mobile/business/overview/statement/details/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/search/d;",
        "Lcom/swedbank/mobile/business/overview/statement/details/f;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

.field private final b:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/overview/plugins/search/remote/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/overview/search/e;

.field private final e:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/search/e;Lcom/swedbank/mobile/business/customer/l;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/overview/search/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "overviewSearchRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/search/e;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->e:Lcom/swedbank/mobile/business/customer/l;

    .line 30
    new-instance p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    const/4 p2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p1, p2, v0, v1, p2}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;-><init>(Ljava/util/List;ZILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    .line 32
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026rchData>(emptySearchData)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->b:Lcom/b/c/b;

    .line 33
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<LastSearchInfo>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->c:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->b:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/search/e;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/search/e;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->e:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->c:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 37
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$b;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)V

    check-cast p1, Lio/reactivex/c/a;

    invoke-static {p1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026(emptySearchData)\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/search/e;

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->e:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 41
    invoke-interface {v0, v1, p1, v2}, Lcom/swedbank/mobile/business/overview/search/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$d;->a:Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->f(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$c;

    invoke-direct {v1, p1, p0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$c;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "overviewSearchRepository\u2026         .ignoreElement()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/search/d$a;->b(Lcom/swedbank/mobile/business/overview/search/d;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/a;->a()Ljava/lang/String;

    move-result-object p1

    .line 118
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)Lcom/b/c/b;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/b/c/b;->i()Lio/reactivex/j;

    move-result-object v1

    const-string v0, "searchResults\n        .firstElement()"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 126
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/j;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 128
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method public b()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->c:Lcom/b/c/b;

    .line 58
    invoke-virtual {v0}, Lcom/b/c/b;->i()Lio/reactivex/j;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "lastSearchInfoStream\n   \u2026plete()\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->b:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public j()V
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/OverviewRemoteSearchInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;->a()V

    return-void
.end method
