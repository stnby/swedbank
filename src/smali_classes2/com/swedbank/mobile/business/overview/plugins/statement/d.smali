.class public final Lcom/swedbank/mobile/business/overview/plugins/statement/d;
.super Ljava/lang/Object;
.source "OverviewStatementInteractorImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/n;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/a/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->a:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->b:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->c:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->d:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->e:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/business/overview/plugins/statement/d;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/a/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/l;",
            ">;)",
            "Lcom/swedbank/mobile/business/overview/plugins/statement/d;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/swedbank/mobile/business/overview/plugins/statement/d;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/overview/plugins/statement/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;
    .locals 8

    .line 43
    new-instance v7, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/f/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/overview/n;

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/overview/a/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/overview/plugins/a;

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/customer/l;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;-><init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/overview/n;Lcom/swedbank/mobile/business/overview/a/a;Lcom/swedbank/mobile/business/overview/plugins/a;Lcom/swedbank/mobile/business/customer/l;)V

    return-object v7
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/d;->a()Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;

    move-result-object v0

    return-object v0
.end method
