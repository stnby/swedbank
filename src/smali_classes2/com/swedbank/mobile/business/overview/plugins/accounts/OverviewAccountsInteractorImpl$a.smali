.class public final Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;
.super Lkotlin/e/b/k;
.source "OverviewAccountsInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/a/a;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;->b:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accounts"

    .line 78
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 98
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/business/a/a;

    .line 78
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 99
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/a/a;

    if-eqz v0, :cond_2

    .line 79
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;->b(Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/accounts/e;

    move-result-object p1

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/overview/plugins/accounts/e;->a(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/accounts/OverviewAccountsInteractorImpl$a;->a(Ljava/util/List;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
