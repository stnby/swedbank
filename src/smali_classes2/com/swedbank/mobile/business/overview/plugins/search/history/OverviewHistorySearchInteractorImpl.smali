.class public final Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewHistorySearchInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/search/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/plugins/search/history/c;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/search/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/overview/plugins/search/history/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/plugins/search/history/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/search/history/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "searchHistoryRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/plugins/search/history/d;

    const/4 p1, 0x1

    .line 21
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string v0, "BehaviorRelay.createDefault(true)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->a:Lcom/b/c/b;

    .line 22
    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v0, ""

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 23
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<String>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->c:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->a:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;)Lcom/swedbank/mobile/business/overview/plugins/search/history/d;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/plugins/search/history/d;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object p1

    const-string v0, "Completable.fromAction {\u2026cept(input.isBlank())\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->c:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/a;

    if-eqz v0, :cond_2

    .line 43
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "lastInput.get()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 45
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/plugins/search/history/d;

    .line 47
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/search/history/d;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 48
    invoke-static {p1, v1, v1, v0, v1}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    goto :goto_1

    .line 43
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 51
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/search/history/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->c:Lcom/b/c/c;

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/search/history/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/history/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public b()Lio/reactivex/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/search/d$a;->a(Lcom/swedbank/mobile/business/overview/search/d;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;->a:Lcom/b/c/b;

    .line 31
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/plugins/search/history/OverviewHistorySearchInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "searchResultsEnabled\n   \u2026List())\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
