.class public final Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewStatementInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/plugins/statement/e;
.implements Lcom/swedbank/mobile/business/overview/search/c;
.implements Lcom/swedbank/mobile/business/overview/statement/details/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/plugins/statement/f;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/plugins/statement/e;",
        "Lcom/swedbank/mobile/business/overview/search/c;",
        "Lcom/swedbank/mobile/business/overview/statement/details/f;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile g:Z

.field private final h:Lcom/swedbank/mobile/business/overview/n;

.field private final i:Lcom/swedbank/mobile/business/overview/a/a;

.field private final j:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/overview/n;Lcom/swedbank/mobile/business/overview/a/a;Lcom/swedbank/mobile/business/overview/plugins/a;Lcom/swedbank/mobile/business/customer/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/overview/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_statement_initial_account_selection"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialAccountSelection"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->h:Lcom/swedbank/mobile/business/overview/n;

    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->i:Lcom/swedbank/mobile/business/overview/a/a;

    iput-object p6, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j:Lcom/swedbank/mobile/business/customer/l;

    .line 56
    iget-object p3, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {p3}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lcom/swedbank/mobile/business/a/b;->d(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 57
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->a:Lio/reactivex/o;

    .line 58
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->a:Lio/reactivex/o;

    .line 59
    sget-object p3, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$a;

    check-cast p3, Lio/reactivex/c/h;

    invoke-virtual {p1, p3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    const-string p3, "accountsStream\n      .ma\u2026  .distinctUntilChanged()"

    invoke-static {p1, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->b:Lio/reactivex/o;

    const-string p1, "feature_overview_search"

    .line 62
    invoke-interface {p2, p1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->c:Z

    .line 64
    invoke-static {p5}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026(initialAccountSelection)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->d:Lcom/b/c/b;

    .line 65
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {p1}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(Unit)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->e:Lcom/b/c/b;

    .line 67
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->f:Ljava/util/Set;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->d:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;Z)V
    .locals 0

    .line 46
    iput-boolean p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->g:Z

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Ljava/util/Set;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->f:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/b/c/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->e:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/n;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->h:Lcom/swedbank/mobile/business/overview/n;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lio/reactivex/o;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->b:Lio/reactivex/o;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Z
    .locals 0

    .line 46
    iget-boolean p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->g:Z

    return p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/a/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->i:Lcom/swedbank/mobile/business/overview/a/a;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Z
    .locals 0

    .line 46
    iget-boolean p0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->c:Z

    return p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/Transaction;)Lio/reactivex/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/overview/statement/details/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 193
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/Transaction;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "flatMapMaybe { Maybe.just(routing(it)) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    if-eqz v0, :cond_1

    .line 168
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->g(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/b;->a()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 170
    invoke-static {p0, v0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;Z)V

    .line 171
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->h(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/a/a;->b()Lio/reactivex/b;

    move-result-object v0

    .line 166
    :goto_0
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->d(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/overview/n;

    move-result-object v1

    .line 174
    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/overview/plugins/a$b;->a()Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->e(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v3

    .line 166
    invoke-interface {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/n;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v1

    check-cast v1, Lio/reactivex/aa;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    goto :goto_1

    .line 176
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/business/overview/plugins/a$a;->a:Lcom/swedbank/mobile/business/overview/plugins/a$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->f(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)Lio/reactivex/o;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 178
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$f;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    :goto_1
    const-string v1, "when (selection) {\n    /\u2026accountIds)\n        }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$d;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$d;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;Lcom/swedbank/mobile/business/overview/plugins/a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$e;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$e;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;Lcom/swedbank/mobile/business/overview/plugins/a;)V

    check-cast v1, Lio/reactivex/c/b;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/b;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "queryFreshTransactions(s\u2026.accept(Unit)\n          }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 178
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public a()V
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/statement/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/plugins/statement/f;->c()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/statement/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/statement/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/overview/plugins/statement/b;->a:Lcom/swedbank/mobile/business/overview/plugins/statement/b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/statement/f;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/f;->b()V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/statement/f;

    .line 157
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/f;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p1, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->i:Lcom/swedbank/mobile/business/overview/a/a;

    .line 105
    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/a$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/a$b;->a()Ljava/lang/String;

    move-result-object p1

    .line 106
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/overview/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    goto :goto_0

    .line 107
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/overview/plugins/a$a;->a:Lcom/swedbank/mobile/business/overview/plugins/a$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->b:Lio/reactivex/o;

    .line 108
    invoke-virtual {p1}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p1

    .line 109
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$g;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "accountIdsStream\n       \u2026s = accountIds)\n        }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/e$a;->a(Lcom/swedbank/mobile/business/overview/plugins/statement/e;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->d:Lcom/b/c/b;

    .line 117
    new-instance v1, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "accountSelectionStream\n \u2026nction)\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()V
    .locals 1

    .line 154
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/statement/OverviewStatementInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/plugins/statement/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/plugins/statement/f;->a()V

    return-void
.end method
