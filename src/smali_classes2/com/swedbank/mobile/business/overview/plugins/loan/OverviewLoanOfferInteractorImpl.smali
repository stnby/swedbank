.class public final Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewLoanOfferInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/plugins/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/plugins/loan/c;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/plugins/b;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/customer/l;

.field private final b:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getIbankServiceLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectedCustomer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getIbankServiceLink"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object p1, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(QueryResult.Success)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/i/a/c;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/i/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/swedbank/mobile/business/overview/plugins/loan/a;->a:Lcom/swedbank/mobile/business/overview/plugins/loan/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;->b:Lcom/swedbank/mobile/architect/business/b;

    sget-object v0, Lcom/swedbank/mobile/business/services/a/a;->e:Lcom/swedbank/mobile/business/services/a/a;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/w;

    .line 34
    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/overview/plugins/loan/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/plugins/loan/c;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 40
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/plugins/a;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p0, p1}, Lcom/swedbank/mobile/business/overview/plugins/b$a;->a(Lcom/swedbank/mobile/business/overview/plugins/b;Lcom/swedbank/mobile/business/overview/plugins/a;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    invoke-static {p0}, Lcom/swedbank/mobile/business/overview/plugins/b$a;->a(Lcom/swedbank/mobile/business/overview/plugins/b;)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/plugins/loan/OverviewLoanOfferInteractorImpl;->a:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.just(!selecte\u2026tomer.isBusinessCustomer)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
