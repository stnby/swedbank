.class final Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;
.super Ljava/lang/Object;
.source "OverviewInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/business/overview/l;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 53
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/a/b;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/a/b;->f(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->b(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/overview/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/m;->a()Lio/reactivex/o;

    move-result-object v0

    .line 129
    check-cast p1, Lio/reactivex/s;

    check-cast v0, Lio/reactivex/s;

    .line 130
    new-instance v1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$a;-><init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;)V

    check-cast v1, Lio/reactivex/c/c;

    .line 129
    invoke-static {p1, v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    .line 64
    new-instance v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;-><init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 82
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/business/overview/l$e;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/business/overview/l$e;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
