.class public final enum Lcom/swedbank/mobile/business/overview/Transaction$Direction;
.super Ljava/lang/Enum;
.source "Transaction.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/overview/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Direction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/overview/Transaction$Direction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$Direction;

.field public static final enum CREDIT:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

.field public static final enum DEBIT:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

.field public static final enum UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Direction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    const-string v2, "CREDIT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->CREDIT:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    const-string v2, "DEBIT"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->DEBIT:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->$VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$Direction;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/overview/Transaction$Direction;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->$VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/overview/Transaction$Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    return-object v0
.end method
