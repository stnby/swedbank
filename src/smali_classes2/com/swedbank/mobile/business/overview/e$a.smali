.class final synthetic Lcom/swedbank/mobile/business/overview/e$a;
.super Lkotlin/e/b/i;
.source "OverviewFlow.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/e;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/f;)Lcom/swedbank/mobile/architect/business/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/a<",
        "Lio/reactivex/j<",
        "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/root/c;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/root/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "waitUntilAuthenticated"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "waitUntilAuthenticated()Lio/reactivex/Maybe;"

    return-object v0
.end method

.method public final d()Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/e$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/root/c;

    .line 13
    invoke-interface {v0}, Lcom/swedbank/mobile/business/root/c;->i()Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/e$a;->d()Lio/reactivex/j;

    move-result-object v0

    return-object v0
.end method
