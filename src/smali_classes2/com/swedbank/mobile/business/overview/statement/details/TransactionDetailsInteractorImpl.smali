.class public final Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "TransactionDetailsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/statement/details/d;
.implements Lcom/swedbank/mobile/business/overview/statement/details/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/statement/details/h;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/statement/details/d;",
        "Lcom/swedbank/mobile/business/overview/statement/details/g;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/overview/statement/details/f;

.field private final d:Lcom/swedbank/mobile/business/f/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/overview/statement/details/f;Lcom/swedbank/mobile/business/overview/a/a;Lcom/swedbank/mobile/business/f/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation runtime Ljavax/inject/Named;
            value = "transaction_details_input"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/statement/details/f;
        .annotation runtime Ljavax/inject/Named;
            value = "transaction_details_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;",
            "Lcom/swedbank/mobile/business/overview/statement/details/f;",
            "Lcom/swedbank/mobile/business/overview/a/a;",
            "Lcom/swedbank/mobile/business/f/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->b:Lcom/swedbank/mobile/business/util/e;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->c:Lcom/swedbank/mobile/business/overview/statement/details/f;

    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->d:Lcom/swedbank/mobile/business/f/a;

    .line 40
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->b:Lcom/swedbank/mobile/business/util/e;

    .line 68
    instance-of p2, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 42
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string p2, "Observable.just(it.toOptional())"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_0
    instance-of p2, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz p2, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 41
    invoke-interface {p3, p1}, Lcom/swedbank/mobile/business/overview/a/a;->b(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 43
    :goto_0
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->a:Lio/reactivex/o;

    return-void

    .line 41
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;)Lcom/swedbank/mobile/business/overview/statement/details/h;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/statement/details/h;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->d:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->c:Lcom/swedbank/mobile/business/overview/statement/details/f;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/statement/details/f;->j()V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->a:Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/b;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->b()Lio/reactivex/o;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 51
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "observeTransactionDetail\u2026}\n      .ignoreElements()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->b()Lio/reactivex/o;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "observeTransactionDetail\u2026           })\n          }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
