.class final Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$a;
.super Ljava/lang/Object;
.source "TransactionDetailsInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->d()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$a;->a(Lcom/swedbank/mobile/business/util/l;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/business/util/l;)Z
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 69
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/Transaction;->i()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v0, 0x1

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;->b(Lcom/swedbank/mobile/business/overview/statement/details/TransactionDetailsInteractorImpl;)Lcom/swedbank/mobile/business/f/a;

    move-result-object p1

    const-string v2, "feature_transaction_details_to_new_payment"

    .line 62
    invoke-interface {p1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    :goto_1
    return v1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
