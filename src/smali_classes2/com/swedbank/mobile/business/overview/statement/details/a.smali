.class public final Lcom/swedbank/mobile/business/overview/statement/details/a;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "TransactionDetailsFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/overview/statement/details/b;",
        "Lcom/swedbank/mobile/business/overview/statement/details/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/overview/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "overviewFlow"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/a;->a:Lcom/swedbank/mobile/business/overview/e;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/overview/statement/details/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/overview/statement/details/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/statement/details/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/statement/details/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/statement/details/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/overview/statement/details/b;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/overview/statement/details/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/statement/details/a;->a:Lcom/swedbank/mobile/business/overview/e;

    sget-object v1, Lcom/swedbank/mobile/business/overview/f;->a:Lcom/swedbank/mobile/business/overview/f;

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/business/overview/e;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/f;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 16
    new-instance v0, Lcom/swedbank/mobile/business/overview/statement/details/a$a;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/business/overview/statement/details/a$a;-><init>(Lcom/swedbank/mobile/business/overview/statement/details/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/overview/statement/details/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/overview/statement/details/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/statement/details/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
