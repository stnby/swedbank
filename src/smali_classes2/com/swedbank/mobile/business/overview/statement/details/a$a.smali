.class final Lcom/swedbank/mobile/business/overview/statement/details/a$a;
.super Lkotlin/e/b/k;
.source "TransactionDetailsFlow.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/statement/details/a;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/statement/details/b;)Lcom/swedbank/mobile/architect/business/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/overview/k;",
        "Lio/reactivex/j<",
        "Lcom/swedbank/mobile/business/overview/statement/details/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/statement/details/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/statement/details/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/statement/details/a$a;->a:Lcom/swedbank/mobile/business/overview/statement/details/b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/k;)Lio/reactivex/j;
    .locals 14
    .param p1    # Lcom/swedbank/mobile/business/overview/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/k;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/overview/statement/details/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/statement/details/a$a;->a:Lcom/swedbank/mobile/business/overview/statement/details/b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/overview/statement/details/b;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 21
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/d;->a(Lorg/threeten/bp/LocalDate;)Ljava/util/Date;

    move-result-object v4

    .line 22
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    move-object v5, v1

    goto :goto_1

    :cond_0
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v5, "BigDecimal.ZERO"

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :goto_1
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_2
    move-object v6, v1

    goto :goto_3

    :cond_1
    const-string v1, ""

    goto :goto_2

    .line 24
    :goto_3
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_4
    move-object v7, v1

    goto :goto_5

    :cond_2
    const-string v1, ""

    goto :goto_4

    .line 25
    :goto_5
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    :goto_6
    move-object v8, v1

    goto :goto_7

    :cond_3
    const-string v1, ""

    goto :goto_6

    .line 26
    :goto_7
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_8
    move-object v9, v0

    goto :goto_9

    :cond_4
    const-string v0, ""

    goto :goto_8

    .line 27
    :goto_9
    sget-object v10, Lcom/swedbank/mobile/business/overview/Transaction$Type;->GENERAL:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    .line 28
    sget-object v11, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->TRANSACTION:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    .line 29
    sget-object v12, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->DEBIT:Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    const/4 v13, 0x1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/business/overview/Transaction;

    move-object v1, v0

    invoke-direct/range {v1 .. v13}, Lcom/swedbank/mobile/business/overview/Transaction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;Lcom/swedbank/mobile/business/overview/Transaction$Direction;I)V

    .line 16
    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/overview/k;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/overview/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/statement/details/a$a;->a(Lcom/swedbank/mobile/business/overview/k;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
