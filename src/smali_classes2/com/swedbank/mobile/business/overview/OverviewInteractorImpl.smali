.class public final Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/d;
.implements Lcom/swedbank/mobile/business/overview/retry/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/o;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/d;",
        "Lcom/swedbank/mobile/business/overview/retry/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/a/b;

.field private final c:Lcom/swedbank/mobile/business/f/a;

.field private final d:Lcom/swedbank/mobile/business/overview/m;

.field private final e:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/f/a;Lcom/swedbank/mobile/business/overview/m;Lcom/swedbank/mobile/business/customer/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewPreferencesRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->b:Lcom/swedbank/mobile/business/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->c:Lcom/swedbank/mobile/business/f/a;

    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/m;

    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->e:Lcom/swedbank/mobile/business/customer/l;

    .line 38
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/a/b;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->b:Lcom/swedbank/mobile/business/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/overview/m;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/m;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->c:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->e:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/overview/o;
    .locals 0

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/o;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/overview/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 109
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->p_()Lio/reactivex/w;

    move-result-object v0

    .line 110
    sget-object v1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/business/overview/i;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/business/overview/i;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "flow()\n      .flatMapMay\u2026uter::flowToOverviewNode)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->a:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method protected m_()V
    .locals 9

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->e:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v0

    .line 43
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 44
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->a:Lcom/b/c/c;

    check-cast v2, Lio/reactivex/s;

    .line 42
    invoke-static {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 45
    new-instance v2, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    invoke-direct {v2, p0, v0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "Observable.merge(\n      \u2026rtWith(Loading)\n        }"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 129
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
