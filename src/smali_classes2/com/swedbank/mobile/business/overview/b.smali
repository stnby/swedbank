.class public final enum Lcom/swedbank/mobile/business/overview/b;
.super Ljava/lang/Enum;
.source "AccountIbanActions.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/overview/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/business/overview/b;

.field public static final enum b:Lcom/swedbank/mobile/business/overview/b;

.field private static final synthetic c:[Lcom/swedbank/mobile/business/overview/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/business/overview/b;

    new-instance v1, Lcom/swedbank/mobile/business/overview/b;

    const-string v2, "COPY"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/b;->a:Lcom/swedbank/mobile/business/overview/b;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/b;

    const-string v2, "SHARE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/b;->b:Lcom/swedbank/mobile/business/overview/b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/overview/b;->c:[Lcom/swedbank/mobile/business/overview/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/b;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/overview/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/b;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/overview/b;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/overview/b;->c:[Lcom/swedbank/mobile/business/overview/b;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/overview/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/overview/b;

    return-object v0
.end method
