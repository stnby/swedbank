.class public final enum Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
.super Ljava/lang/Enum;
.source "Transaction.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/business/overview/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaymentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

.field public static final enum MASS_PAYMENT:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

.field public static final enum TRANSACTION:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

.field public static final enum UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->UNKNOWN:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    const-string v2, "TRANSACTION"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->TRANSACTION:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    const-string v2, "MASS_PAYMENT"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->MASS_PAYMENT:Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->$VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->$VALUES:[Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/business/overview/Transaction$PaymentType;

    return-object v0
.end method
