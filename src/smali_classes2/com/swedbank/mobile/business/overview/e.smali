.class public final Lcom/swedbank/mobile/business/overview/e;
.super Lcom/swedbank/mobile/architect/business/a/f;
.source "OverviewFlow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/a/f<",
        "Lcom/swedbank/mobile/business/root/c;",
        "Lcom/swedbank/mobile/business/overview/f;",
        "Lcom/swedbank/mobile/business/overview/k;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/a/f;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/swedbank/mobile/architect/business/a/e;Lcom/swedbank/mobile/architect/business/a/b;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/overview/f;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/overview/e;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/f;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/f;)Lcom/swedbank/mobile/architect/business/a/d;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/root/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/root/c;",
            "Lcom/swedbank/mobile/business/overview/f;",
            ")",
            "Lcom/swedbank/mobile/architect/business/a/d<",
            "Lcom/swedbank/mobile/business/overview/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "root"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance p2, Lcom/swedbank/mobile/business/overview/e$a;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/business/overview/e$a;-><init>(Lcom/swedbank/mobile/business/root/c;)V

    check-cast p2, Lkotlin/e/a/a;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/business/overview/e;->a(Lkotlin/e/a/a;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 14
    sget-object p2, Lcom/swedbank/mobile/business/overview/e$b;->a:Lcom/swedbank/mobile/business/overview/e$b;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 15
    sget-object p2, Lcom/swedbank/mobile/business/overview/e$c;->a:Lcom/swedbank/mobile/business/overview/e$c;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    .line 16
    sget-object p2, Lcom/swedbank/mobile/business/overview/e$d;->a:Lcom/swedbank/mobile/business/overview/e$d;

    check-cast p2, Lkotlin/e/a/b;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/architect/business/a/d;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/business/root/c;

    check-cast p2, Lcom/swedbank/mobile/business/overview/f;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/business/overview/e;->a(Lcom/swedbank/mobile/business/root/c;Lcom/swedbank/mobile/business/overview/f;)Lcom/swedbank/mobile/architect/business/a/d;

    move-result-object p1

    return-object p1
.end method
