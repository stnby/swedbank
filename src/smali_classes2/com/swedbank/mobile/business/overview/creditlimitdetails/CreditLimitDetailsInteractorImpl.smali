.class public final Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CreditLimitDetailsInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/creditlimitdetails/e;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;

.field private final e:Lcom/swedbank/mobile/business/a/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/l;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;Lcom/swedbank/mobile/business/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "credit_limit_details_account_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;
        .annotation runtime Ljavax/inject/Named;
            value = "credit_limit_details_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectedCustomer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;

    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->e:Lcom/swedbank/mobile/business/a/b;

    .line 47
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->e:Lcom/swedbank/mobile/business/a/b;

    .line 48
    iget-object p2, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->c:Ljava/lang/String;

    invoke-interface {p1, p2}, Lcom/swedbank/mobile/business/a/b;->c(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    .line 49
    invoke-static {p1}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->a:Lio/reactivex/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;)Lcom/swedbank/mobile/business/overview/creditlimitdetails/e;
    .locals 0

    .line 41
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->a:Lio/reactivex/o;

    .line 52
    new-instance v1, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "accountStream\n      .fla\u2026()\n            })\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;Ljava/lang/String;)V
    .locals 9
    .param p1    # Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->a:Lio/reactivex/o;

    const-wide/16 v1, 0x1

    .line 65
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "accountStream\n        .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;Ljava/lang/String;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 90
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->e:Lcom/swedbank/mobile/business/a/b;

    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;->b()V

    return-void
.end method
