.class final Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;
.super Lkotlin/e/b/k;
.source "CreditLimitDetailsInteractor.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/util/l<",
        "+",
        "Lcom/swedbank/mobile/business/a/a;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;

.field final synthetic b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;

    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->c:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;)V"
        }
    .end annotation

    .line 92
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto/16 :goto_3

    .line 93
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    .line 90
    check-cast p1, Lcom/swedbank/mobile/business/a/a;

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;)Lcom/swedbank/mobile/business/overview/creditlimitdetails/e;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;

    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;

    iget-object v9, p0, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->c:Ljava/lang/String;

    const/4 v4, 0x0

    .line 95
    invoke-static {v1}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;->b(Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->d()Ljava/lang/String;

    move-result-object v7

    .line 96
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->b()Ljava/lang/String;

    move-result-object v8

    .line 97
    sget-object v1, Lcom/swedbank/mobile/business/overview/creditlimitdetails/b;->a:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 102
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 101
    :pswitch_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->e()Lcom/swedbank/mobile/business/a/d;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/d;->f()Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-gez v2, :cond_1

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/d;->f()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v1

    const-string v2, "deposit.abs()"

    :goto_0
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v2, "BigDecimal.ZERO"

    goto :goto_0

    .line 98
    :pswitch_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->e()Lcom/swedbank/mobile/business/a/d;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/d;->d()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/a/d;->e()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    const-string v2, "this.subtract(other)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-ltz v2, :cond_2

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v2, "BigDecimal.ZERO"

    :goto_1
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v1

    const-string v2, "usedCredit.abs()"

    goto :goto_1

    :goto_2
    move-object v5, v1

    .line 103
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->d()Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xc1

    const/4 v13, 0x0

    .line 94
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-object v3, p1

    invoke-direct/range {v3 .. v13}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;ILkotlin/e/b/g;)V

    .line 68
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/e;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    .line 69
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 105
    :goto_3
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    return-void

    .line 69
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/CreditLimitDetailsInteractorImpl$b;->a(Lcom/swedbank/mobile/business/util/l;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
