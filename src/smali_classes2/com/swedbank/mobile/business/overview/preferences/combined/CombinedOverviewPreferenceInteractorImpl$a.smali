.class final Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$a;
.super Ljava/lang/Object;
.source "CombinedOverviewPreferenceInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->m_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Boolean;",
        "Lio/reactivex/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/b;
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "enabled"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$a;->a:Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/overview/m;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/m;->a(Z)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$a;->a(Ljava/lang/Boolean;)Lio/reactivex/b;

    move-result-object p1

    return-object p1
.end method
