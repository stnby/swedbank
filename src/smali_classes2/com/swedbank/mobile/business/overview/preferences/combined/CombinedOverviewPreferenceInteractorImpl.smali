.class public final Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "CombinedOverviewPreferenceInteractor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/preferences/combined/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/overview/m;

.field private final b:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/m;Lcom/swedbank/mobile/business/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "combined_overview_preference_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "overviewPreferencesRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/overview/m;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/business/preferences/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/overview/m;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/overview/m;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;)Lcom/swedbank/mobile/business/preferences/a;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->b:Lcom/swedbank/mobile/business/preferences/a;

    return-object p0
.end method


# virtual methods
.method protected m_()V
    .locals 4

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;->a:Lcom/swedbank/mobile/business/overview/m;

    .line 21
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/m;->a()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 22
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "overviewPreferencesRepos\u2026rview(!enabled)\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v1, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/preferences/combined/CombinedOverviewPreferenceInteractorImpl;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/b;Lkotlin/e/a/b;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 39
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method
