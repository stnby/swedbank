.class public final Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewCurrenciesInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/currencies/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/currencies/d;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/currencies/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/a/b;

.field private final b:Lcom/swedbank/mobile/business/customer/l;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/swedbank/mobile/business/overview/currencies/c;

.field private final e:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/a/b;Lcom/swedbank/mobile/business/customer/l;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/currencies/c;Lcom/swedbank/mobile/architect/business/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_currencies_account_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/overview/currencies/c;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_currencies_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "getIbankServiceLink"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/a/b;",
            "Lcom/swedbank/mobile/business/customer/l;",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Lcom/swedbank/mobile/business/services/a/a;",
            "Lio/reactivex/w<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedCustomer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getIbankServiceLink"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    iput-object p3, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/currencies/c;

    iput-object p5, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/a/b;->c(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl$c;-><init>(Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "accountRepository\n      \u2026()\n            })\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Lio/reactivex/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/util/p;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    .line 58
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 59
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->c:Ljava/lang/String;

    .line 57
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->a:Lcom/swedbank/mobile/business/a/b;

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v1

    .line 64
    iget-object v2, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->c:Ljava/lang/String;

    .line 62
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/business/a/b;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 67
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "accountRepository\n      \u2026e handled.\n      .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()V
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    sget-object v1, Lcom/swedbank/mobile/business/services/a/a;->a:Lcom/swedbank/mobile/business/services/a/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 71
    new-instance v1, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/currencies/d;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl$a;-><init>(Lcom/swedbank/mobile/business/overview/currencies/d;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 84
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public e()V
    .locals 4

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->e:Lcom/swedbank/mobile/architect/business/b;

    sget-object v1, Lcom/swedbank/mobile/business/services/a/a;->b:Lcom/swedbank/mobile/business/services/a/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/w;

    .line 77
    new-instance v1, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl$b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/overview/currencies/d;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl$b;-><init>(Lcom/swedbank/mobile/business/overview/currencies/d;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 86
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/business/c;->a(Lcom/swedbank/mobile/architect/business/c;Lio/reactivex/b/c;)V

    return-void
.end method

.method public f()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/currencies/OverviewCurrenciesInteractorImpl;->d:Lcom/swedbank/mobile/business/overview/currencies/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/currencies/c;->c()V

    return-void
.end method
