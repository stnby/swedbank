.class public final Lcom/swedbank/mobile/business/overview/retry/OverviewRetryInteractorImpl;
.super Lcom/swedbank/mobile/architect/business/c;
.source "OverviewRetryInteractor.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/retry/a;
.implements Lcom/swedbank/mobile/business/overview/retry/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/business/c<",
        "Lcom/swedbank/mobile/business/overview/retry/d;",
        ">;",
        "Lcom/swedbank/mobile/business/overview/retry/a;",
        "Lcom/swedbank/mobile/business/overview/retry/c;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/i/d;

.field private final synthetic b:Lcom/swedbank/mobile/business/overview/retry/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/i/d;Lcom/swedbank/mobile/business/overview/retry/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/d;
        .annotation runtime Ljavax/inject/Named;
            value = "to_logged_in_navigation_item_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/retry/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pluginManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/business/c;-><init>()V

    .line 22
    iput-object p2, p0, Lcom/swedbank/mobile/business/overview/retry/OverviewRetryInteractorImpl;->b:Lcom/swedbank/mobile/business/overview/retry/c;

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/retry/OverviewRetryInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/retry/OverviewRetryInteractorImpl;->b:Lcom/swedbank/mobile/business/overview/retry/c;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/retry/c;->b()V

    return-void
.end method

.method protected m_()V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/retry/OverviewRetryInteractorImpl;->a:Lcom/swedbank/mobile/business/i/d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/retry/OverviewRetryInteractorImpl;->j_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/i/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/business/i/d;->a(Lcom/swedbank/mobile/business/i/e;)V

    return-void
.end method
