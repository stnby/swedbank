.class final Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;
.super Ljava/lang/Object;
.source "OverviewInteractor.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/c;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/overview/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/overview/c;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/business/overview/l;",
            ">;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    sget-object v0, Lcom/swedbank/mobile/business/overview/h;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/c;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 68
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;

    iget-object p1, p1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->a(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/a/b;

    move-result-object p1

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;

    iget-object v0, v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    iget-object v0, v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    invoke-static {v0}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->d(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/customer/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/a/b;->d(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 70
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object p1

    .line 71
    sget-object v0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 67
    :pswitch_0
    sget-object p1, Lcom/swedbank/mobile/business/overview/l$a;->a:Lcom/swedbank/mobile/business/overview/l$a;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object p1, Lcom/swedbank/mobile/business/overview/l$c;->a:Lcom/swedbank/mobile/business/overview/l$c;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/swedbank/mobile/business/overview/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$1;->a(Lcom/swedbank/mobile/business/overview/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
