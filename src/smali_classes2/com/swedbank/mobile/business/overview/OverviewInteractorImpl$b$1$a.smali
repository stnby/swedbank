.class public final Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$a;
.super Ljava/lang/Object;
.source "Observables.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "TT1;TT2;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$a;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)TR;"
        }
    .end annotation

    .line 20
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    .line 251
    iget-object p1, p0, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1$a;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;

    iget-object p1, p1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b$1;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;

    iget-object p1, p1, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl$b;->a:Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;

    invoke-static {p1}, Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;->c(Lcom/swedbank/mobile/business/overview/OverviewInteractorImpl;)Lcom/swedbank/mobile/business/f/a;

    move-result-object p1

    const-string v2, "feature_overview_combined"

    invoke-interface {p1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 253
    sget-object p1, Lcom/swedbank/mobile/business/overview/c;->a:Lcom/swedbank/mobile/business/overview/c;

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 254
    sget-object p1, Lcom/swedbank/mobile/business/overview/c;->c:Lcom/swedbank/mobile/business/overview/c;

    goto :goto_0

    .line 255
    :cond_1
    sget-object p1, Lcom/swedbank/mobile/business/overview/c;->b:Lcom/swedbank/mobile/business/overview/c;

    :goto_0
    return-object p1
.end method
