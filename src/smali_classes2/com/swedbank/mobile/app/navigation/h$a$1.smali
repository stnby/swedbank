.class final Lcom/swedbank/mobile/app/navigation/h$a$1;
.super Lkotlin/e/b/k;
.source "NavigationRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/navigation/h$a;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/navigation/h$a;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/navigation/h$a;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/h$a$1;->a:Lcom/swedbank/mobile/app/navigation/h$a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/navigation/h$a$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/a/h;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p1

    .line 81
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    if-nez p1, :cond_0

    .line 72
    iget-object p1, p0, Lcom/swedbank/mobile/app/navigation/h$a$1;->b:Lio/reactivex/k;

    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/h$a$1;->a:Lcom/swedbank/mobile/app/navigation/h$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/navigation/h$a;->a:Lcom/swedbank/mobile/app/navigation/h;

    invoke-static {v0}, Lcom/swedbank/mobile/app/navigation/h;->a(Lcom/swedbank/mobile/app/navigation/h;)Lcom/b/c/c;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/b/c/c;->i()Lio/reactivex/j;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/swedbank/mobile/app/navigation/h$a$1$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/navigation/h$a$1$a;-><init>(Lcom/swedbank/mobile/app/navigation/h$a$1;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    .line 72
    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Lio/reactivex/b/c;)V

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/h$a$1;->b:Lio/reactivex/k;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/navigation/h$a$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
