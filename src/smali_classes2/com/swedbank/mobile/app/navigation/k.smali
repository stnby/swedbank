.class public final Lcom/swedbank/mobile/app/navigation/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "NavigationViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/navigation/j;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/business/f/a;

.field private final f:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/navigation/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "navigationBar"

    const-string v4, "getNavigationBar()Landroid/widget/LinearLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/navigation/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/k;->e:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/navigation/k;->f:Lio/reactivex/o;

    .line 27
    sget p1, Lcom/swedbank/mobile/core/a$d;->navigation_bar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/k;->b:Lkotlin/f/c;

    .line 29
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<NavigationItemKey>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/k;->d:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/navigation/k;)Landroid/widget/LinearLayout;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/navigation/k;->c()Landroid/widget/LinearLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/navigation/k;)Lcom/b/c/c;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/navigation/k;->d:Lcom/b/c/c;

    return-object p0
.end method

.method private final c()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/navigation/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/k;->f:Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/navigation/m;)V
    .locals 18
    .param p1    # Lcom/swedbank/mobile/app/navigation/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v6, p0

    const-string v0, "viewState"

    move-object/from16 v7, p1

    invoke-static {v7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/navigation/m;->a()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    if-eqz v8, :cond_2

    .line 42
    iget-object v0, v6, Lcom/swedbank/mobile/app/navigation/k;->c:Ljava/util/List;

    invoke-static {v0, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 43
    iput-object v8, v6, Lcom/swedbank/mobile/app/navigation/k;->c:Ljava/util/List;

    .line 83
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/navigation/k;->a(Lcom/swedbank/mobile/app/navigation/k;)Landroid/widget/LinearLayout;

    move-result-object v10

    .line 84
    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 85
    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 86
    move-object v0, v8

    check-cast v0, Ljava/lang/Iterable;

    .line 91
    invoke-static {v0}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/swedbank/mobile/app/navigation/k$a;

    invoke-direct {v1}, Lcom/swedbank/mobile/app/navigation/k$a;-><init>()V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object v0

    .line 93
    invoke-interface {v0}, Lkotlin/i/e;->a()Ljava/util/Iterator;

    move-result-object v17

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/app/navigation/a;

    .line 94
    new-instance v3, Lcom/swedbank/mobile/app/navigation/b;

    const-string v0, "context"

    invoke-static {v5, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x6

    const/16 v16, 0x0

    move-object v11, v3

    move-object v12, v5

    invoke-direct/range {v11 .. v16}, Lcom/swedbank/mobile/app/navigation/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    .line 95
    invoke-interface {v4}, Lcom/swedbank/mobile/app/navigation/a;->c()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/swedbank/mobile/app/navigation/b;->setId(I)V

    .line 96
    invoke-virtual {v3, v4}, Lcom/swedbank/mobile/app/navigation/b;->setTag(Ljava/lang/Object;)V

    .line 97
    move-object v11, v3

    check-cast v11, Landroid/view/View;

    .line 98
    new-instance v12, Lcom/swedbank/mobile/app/navigation/k$b;

    move-object v0, v12

    move-object v1, v4

    move-object v2, v10

    move-object v13, v3

    move-object v3, v5

    move-object v14, v4

    move-object/from16 v4, p0

    move-object v15, v5

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/navigation/k$b;-><init>(Lcom/swedbank/mobile/app/navigation/a;Landroid/widget/LinearLayout;Landroid/content/Context;Lcom/swedbank/mobile/app/navigation/k;Ljava/util/List;)V

    check-cast v12, Landroid/view/View$OnClickListener;

    invoke-virtual {v11, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    new-instance v0, Lcom/swedbank/mobile/app/navigation/k$c;

    invoke-direct {v0, v13}, Lcom/swedbank/mobile/app/navigation/k$c;-><init>(Lcom/swedbank/mobile/app/navigation/b;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction {\u2026etOnClickListener(null) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v6, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 106
    invoke-interface {v14}, Lcom/swedbank/mobile/app/navigation/a;->d()I

    move-result v0

    invoke-interface {v14}, Lcom/swedbank/mobile/app/navigation/a;->c()I

    move-result v1

    invoke-virtual {v13, v0, v1}, Lcom/swedbank/mobile/app/navigation/b;->a(II)V

    .line 107
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const v1, 0x3dcccccd    # 0.1f

    const/4 v2, -0x1

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v10, v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object v5, v15

    goto :goto_1

    .line 84
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Navigation items can be rendered only once"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 47
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/navigation/m;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    move-object v1, v6

    check-cast v1, Lcom/swedbank/mobile/app/navigation/k;

    .line 115
    invoke-static {v1}, Lcom/swedbank/mobile/app/navigation/k;->a(Lcom/swedbank/mobile/app/navigation/k;)Landroid/widget/LinearLayout;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 116
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    :goto_2
    if-ge v9, v2, :cond_4

    .line 117
    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "getChildAt(index)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    check-cast v4, Lcom/swedbank/mobile/app/navigation/a;

    invoke-interface {v4}, Lcom/swedbank/mobile/app/navigation/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.swedbank.mobile.app.navigation.NavigationItemDetails"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/app/navigation/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/navigation/k;->a(Lcom/swedbank/mobile/app/navigation/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/k;->d:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method protected e()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/k;->e:Lcom/swedbank/mobile/business/f/a;

    const-string v1, "feature_white_theme"

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/navigation/k;->c()Landroid/widget/LinearLayout;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/core/a$b;->white:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method
