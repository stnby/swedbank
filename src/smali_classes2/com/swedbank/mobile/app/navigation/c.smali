.class public final Lcom/swedbank/mobile/app/navigation/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "NavigationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/navigation/j;",
        "Lcom/swedbank/mobile/app/navigation/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/navigation/g;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/swedbank/mobile/business/navigation/g;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "to_navigation_plugin_point"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/navigation/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/navigation/a;",
            ">;",
            "Lcom/swedbank/mobile/business/navigation/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "navigationItemDetails"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/c;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/app/navigation/c;->b:Lcom/swedbank/mobile/business/navigation/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/navigation/c;)Lcom/swedbank/mobile/business/navigation/g;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/navigation/c;->b:Lcom/swedbank/mobile/business/navigation/g;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/navigation/c$c;->a:Lcom/swedbank/mobile/app/navigation/c$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/navigation/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/swedbank/mobile/app/navigation/c$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/navigation/c$d;-><init>(Lcom/swedbank/mobile/app/navigation/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/navigation/c$e;->a:Lcom/swedbank/mobile/app/navigation/c$e;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/navigation/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    new-instance v2, Lcom/swedbank/mobile/app/navigation/c$f;

    iget-object v3, p0, Lcom/swedbank/mobile/app/navigation/c;->b:Lcom/swedbank/mobile/business/navigation/g;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/navigation/c$f;-><init>(Lcom/swedbank/mobile/business/navigation/g;)V

    check-cast v2, Lkotlin/e/a/b;

    new-instance v3, Lcom/swedbank/mobile/app/navigation/e;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/navigation/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 33
    new-instance v2, Lcom/swedbank/mobile/app/navigation/m$a$b;

    iget-object v3, p0, Lcom/swedbank/mobile/app/navigation/c;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/navigation/m$a$b;-><init>(Ljava/util/List;)V

    .line 58
    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, "Observable.just(this)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v3, p0, Lcom/swedbank/mobile/app/navigation/c;->b:Lcom/swedbank/mobile/business/navigation/g;

    .line 37
    invoke-interface {v3}, Lcom/swedbank/mobile/business/navigation/g;->a()Lio/reactivex/o;

    move-result-object v3

    .line 38
    sget-object v4, Lcom/swedbank/mobile/app/navigation/c$b;->a:Lcom/swedbank/mobile/app/navigation/c$b;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_0

    new-instance v5, Lcom/swedbank/mobile/app/navigation/f;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/navigation/f;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_0
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 41
    check-cast v0, Lio/reactivex/s;

    .line 42
    check-cast v1, Lio/reactivex/s;

    .line 43
    check-cast v2, Lio/reactivex/s;

    .line 44
    check-cast v3, Lio/reactivex/s;

    .line 40
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026        activeItemStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v1, Lcom/swedbank/mobile/app/navigation/m;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/app/navigation/m;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    .line 47
    new-instance v2, Lcom/swedbank/mobile/app/navigation/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/navigation/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/navigation/c$a;-><init>(Lcom/swedbank/mobile/app/navigation/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 59
    new-instance v3, Lcom/swedbank/mobile/app/navigation/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/navigation/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 60
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
