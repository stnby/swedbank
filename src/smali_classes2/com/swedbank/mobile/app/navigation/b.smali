.class public final Lcom/swedbank/mobile/app/navigation/b;
.super Landroid/widget/LinearLayout;
.source "NavigationItemView.kt"


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 23
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/navigation/b;->setClipToOutline(Z)V

    const/4 p2, 0x1

    .line 24
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/navigation/b;->setOrientation(I)V

    .line 25
    sget p3, Lcom/swedbank/mobile/core/a$c;->ripple_unbound_background:I

    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/app/navigation/b;->setBackgroundResource(I)V

    .line 26
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/navigation/b;->setClickable(Z)V

    .line 27
    sget p3, Lcom/swedbank/mobile/core/a$e;->widget_navigation_item:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    .line 38
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1, p3, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 28
    sget p1, Lcom/swedbank/mobile/core/a$d;->navigation_item_icon:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/navigation/b;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.navigation_item_icon)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/b;->a:Landroid/widget/ImageView;

    .line 29
    sget p1, Lcom/swedbank/mobile/core/a$d;->navigation_item_text:I

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/navigation/b;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.navigation_item_text)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/b;->b:Landroid/widget/TextView;

    return-void

    .line 37
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 16
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 17
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/navigation/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/b;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 34
    iget-object p1, p0, Lcom/swedbank/mobile/app/navigation/b;->b:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
