.class public final Lcom/swedbank/mobile/app/navigation/c/a;
.super Ljava/lang/Object;
.source "NotAuthNavigationBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private final a:Lcom/swedbank/mobile/a/p/c/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/p/c/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/p/c/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/c/a;->a:Lcom/swedbank/mobile/a/p/c/a$a;

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/c/a;->a:Lcom/swedbank/mobile/a/p/c/a$a;

    .line 14
    sget-object v1, Lcom/swedbank/mobile/a/p/c/b;->a:Lcom/swedbank/mobile/a/p/c/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/p/c/a$a;->b(Lcom/swedbank/mobile/a/p/c/b;)Lcom/swedbank/mobile/a/p/c/a$a;

    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/swedbank/mobile/a/p/c/a$a;->a()Lcom/swedbank/mobile/a/p/c/a;

    move-result-object v0

    .line 16
    invoke-interface {v0}, Lcom/swedbank/mobile/a/p/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0
.end method
