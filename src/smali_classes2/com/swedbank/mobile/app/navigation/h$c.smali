.class final Lcom/swedbank/mobile/app/navigation/h$c;
.super Lkotlin/e/b/k;
.source "NavigationRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/navigation/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/navigation/h;

.field final synthetic b:Lcom/swedbank/mobile/architect/a/h;

.field final synthetic c:Lkotlin/e/a/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/navigation/h;Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/h$c;->a:Lcom/swedbank/mobile/app/navigation/h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/navigation/h$c;->b:Lcom/swedbank/mobile/architect/a/h;

    iput-object p3, p0, Lcom/swedbank/mobile/app/navigation/h$c;->c:Lkotlin/e/a/a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 38
    iget-object p1, p0, Lcom/swedbank/mobile/app/navigation/h$c;->a:Lcom/swedbank/mobile/app/navigation/h;

    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/h$c;->b:Lcom/swedbank/mobile/architect/a/h;

    .line 81
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->d(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/h$c;->a:Lcom/swedbank/mobile/app/navigation/h;

    invoke-static {v0}, Lcom/swedbank/mobile/app/navigation/h;->a(Lcom/swedbank/mobile/app/navigation/h;)Lcom/b/c/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 40
    iget-object p1, p0, Lcom/swedbank/mobile/app/navigation/h$c;->c:Lkotlin/e/a/a;

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/navigation/h$c;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
