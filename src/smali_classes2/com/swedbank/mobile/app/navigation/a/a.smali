.class public final Lcom/swedbank/mobile/app/navigation/a/a;
.super Ljava/lang/Object;
.source "AuthNavigationBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/a/i/d;

.field private final b:Lcom/swedbank/mobile/a/p/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/p/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/p/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/a/a;->b:Lcom/swedbank/mobile/a/p/a/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/app/navigation/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/i/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "customerModule"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/app/navigation/a/a;->a:Lcom/swedbank/mobile/a/i/d;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/app/navigation/a/a;->b:Lcom/swedbank/mobile/a/p/a/a$a;

    .line 21
    iget-object v1, p0, Lcom/swedbank/mobile/app/navigation/a/a;->a:Lcom/swedbank/mobile/a/i/d;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/p/a/a$a;->b(Lcom/swedbank/mobile/a/i/d;)Lcom/swedbank/mobile/a/p/a/a$a;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/swedbank/mobile/a/p/a/b;->a:Lcom/swedbank/mobile/a/p/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/p/a/a$a;->b(Lcom/swedbank/mobile/a/p/a/b;)Lcom/swedbank/mobile/a/p/a/a$a;

    move-result-object v0

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/a/p/a/a$a;->a()Lcom/swedbank/mobile/a/p/a/a;

    move-result-object v0

    .line 26
    invoke-interface {v0}, Lcom/swedbank/mobile/a/p/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 21
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Authenticated navigation node cannot be built without customer module"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
