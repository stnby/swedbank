.class final Lcom/swedbank/mobile/app/k/c$c;
.super Ljava/lang/Object;
.source "ForceInformPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/k/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/k/c$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/k/c$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/k/c$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/k/c$c;->a:Lcom/swedbank/mobile/app/k/c$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/inform/d;)Lcom/swedbank/mobile/app/k/j;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/inform/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcom/swedbank/mobile/app/k/j;

    .line 17
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/inform/d;->b()Z

    move-result v1

    .line 18
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/inform/d;->c()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/inform/d;->d()Ljava/lang/String;

    move-result-object v3

    .line 20
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/inform/d;->e()Ljava/lang/String;

    move-result-object p1

    .line 16
    invoke-direct {v0, v2, v3, p1, v1}, Lcom/swedbank/mobile/app/k/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/swedbank/mobile/business/inform/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/k/c$c;->a(Lcom/swedbank/mobile/business/inform/d;)Lcom/swedbank/mobile/app/k/j;

    move-result-object p1

    return-object p1
.end method
