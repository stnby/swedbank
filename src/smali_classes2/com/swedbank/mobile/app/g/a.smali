.class public final Lcom/swedbank/mobile/app/g/a;
.super Ljava/lang/Object;
.source "ActivityLifecycleMonitorImpl.kt"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;
.implements Lcom/swedbank/mobile/business/e/b;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private a:I

.field private final b:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/e/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 2
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/swedbank/mobile/business/e/a;->g:Lcom/swedbank/mobile/business/e/a;

    invoke-static {v0}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefa\u2026tivityLifecycleEvent.NIL)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/g/a;->b:Lcom/b/c/b;

    .line 26
    move-object v0, p0

    check-cast v0, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method

.method private final a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V
    .locals 1

    .line 30
    instance-of v0, p1, Lcom/swedbank/mobile/app/MainActivity;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/swedbank/mobile/app/g/a;->a:I

    invoke-virtual {p1}, Landroid/app/Activity;->hashCode()I

    move-result p1

    if-ne v0, p1, :cond_0

    .line 31
    iget-object p1, p0, Lcom/swedbank/mobile/app/g/a;->b:Lcom/b/c/b;

    invoke-virtual {p1, p2}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/e/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/a;->b:Lcom/b/c/b;

    .line 63
    sget-object v1, Lcom/swedbank/mobile/app/g/a$c;->a:Lcom/swedbank/mobile/app/g/a$c;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lcom/b/c/b;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "lifecycleStream\n      .f\u2026ivityLifecycleEvent.NIL }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Z
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/a;->b:Lcom/b/c/b;

    .line 82
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/swedbank/mobile/business/e/a;

    .line 65
    invoke-static {v0}, Lcom/swedbank/mobile/business/e/c;->a(Lcom/swedbank/mobile/business/e/a;)Z

    move-result v0

    return v0

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/g/a;->a()Lio/reactivex/o;

    move-result-object v0

    .line 69
    sget-object v1, Lcom/swedbank/mobile/app/g/a$a;->a:Lcom/swedbank/mobile/app/g/a$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 79
    sget-object v1, Lcom/swedbank/mobile/app/g/a$b;->a:Lcom/swedbank/mobile/app/g/a$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "observeLifecycleEvents()\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    .line 36
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput p2, p0, Lcom/swedbank/mobile/app/g/a;->a:I

    .line 37
    sget-object p2, Lcom/swedbank/mobile/business/e/a;->a:Lcom/swedbank/mobile/business/e/a;

    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/g/a;->a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 59
    sget-object v0, Lcom/swedbank/mobile/business/e/a;->f:Lcom/swedbank/mobile/business/e/a;

    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/g/a;->a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 49
    sget-object v0, Lcom/swedbank/mobile/business/e/a;->d:Lcom/swedbank/mobile/business/e/a;

    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/g/a;->a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 45
    sget-object v0, Lcom/swedbank/mobile/business/e/a;->c:Lcom/swedbank/mobile/business/e/a;

    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/g/a;->a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 41
    sget-object v0, Lcom/swedbank/mobile/business/e/a;->b:Lcom/swedbank/mobile/business/e/a;

    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/g/a;->a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V

    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 53
    sget-object v0, Lcom/swedbank/mobile/business/e/a;->e:Lcom/swedbank/mobile/business/e/a;

    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/g/a;->a(Landroid/app/Activity;Lcom/swedbank/mobile/business/e/a;)V

    return-void
.end method
