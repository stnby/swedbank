.class final Lcom/swedbank/mobile/app/g/a$a;
.super Ljava/lang/Object;
.source "ActivityLifecycleMonitorImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/g/a;->c()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/g/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/g/a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/g/a$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/g/a$a;->a:Lcom/swedbank/mobile/app/g/a$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/e/a;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/e/a;",
            ">;"
        }
    .end annotation

    const-string v0, "lifecycleStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x1

    .line 72
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 74
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v3

    sget-object v4, Lcom/swedbank/mobile/app/g/a$a$1;->a:Lcom/swedbank/mobile/app/g/a$a$1;

    check-cast v4, Lio/reactivex/c/k;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 76
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v4}, Lio/reactivex/o;->b(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 70
    invoke-static {v2, v3, p1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/g/a$a;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
