.class public final Lcom/swedbank/mobile/app/g/j;
.super Ljava/lang/Object;
.source "DeviceRootedAnalyticsEvent.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/a/a;


# instance fields
.field private final a:Landroid/os/Bundle;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/core/a/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/l;Lcom/swedbank/mobile/core/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootReason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/app/g/j;->b:Lcom/swedbank/mobile/core/a/b;

    const/4 p2, 0x1

    .line 16
    new-array p2, p2, [Lkotlin/k;

    const-string v0, "root_reason"

    .line 17
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/l;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    const/4 v0, 0x0

    aput-object p1, p2, v0

    .line 16
    invoke-static {p2}, Landroidx/core/os/b;->a([Lkotlin/k;)Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/j;->a:Landroid/os/Bundle;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/business/e/l;Lcom/swedbank/mobile/core/a/b;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 14
    sget-object p2, Lcom/swedbank/mobile/core/a/b;->i:Lcom/swedbank/mobile/core/a/b;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/g/j;-><init>(Lcom/swedbank/mobile/business/e/l;Lcom/swedbank/mobile/core/a/b;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/j;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/core/a/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/j;->b:Lcom/swedbank/mobile/core/a/b;

    return-object v0
.end method
