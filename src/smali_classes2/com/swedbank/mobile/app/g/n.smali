.class public final Lcom/swedbank/mobile/app/g/n;
.super Ljava/lang/Object;
.source "LockScreenManagerImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/g/m;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/swedbank/mobile/business/e/i;

.field private final c:Landroid/app/KeyguardManager;

.field private final d:Lcom/swedbank/mobile/business/e/j;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/e/i;Landroid/app/KeyguardManager;Lcom/swedbank/mobile/business/e/j;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/app/KeyguardManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/e/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "app"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keyguardManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "externalActivityManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/n;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/g/n;->b:Lcom/swedbank/mobile/business/e/i;

    iput-object p3, p0, Lcom/swedbank/mobile/app/g/n;->c:Landroid/app/KeyguardManager;

    iput-object p4, p0, Lcom/swedbank/mobile/app/g/n;->d:Lcom/swedbank/mobile/business/e/j;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/g/n;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/g/n;->b:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/g/f;)Lio/reactivex/w;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/g/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/g/f;",
            ")",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authScreenInfo"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/n;->a:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/g/n;->c:Landroid/app/KeyguardManager;

    .line 28
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/g/f;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 29
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/g/f;->b()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 27
    invoke-virtual {v1, v2, p1}, Landroid/app/KeyguardManager;->createConfirmDeviceCredentialIntent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 31
    iget-object v3, p0, Lcom/swedbank/mobile/app/g/n;->d:Lcom/swedbank/mobile/business/e/j;

    const v5, 0xea61

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 32
    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/e/j$a;->a(Lcom/swedbank/mobile/business/e/j;Landroid/content/Intent;ILandroid/os/Bundle;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    .line 33
    sget-object v0, Lcom/swedbank/mobile/app/g/n$a;->a:Lcom/swedbank/mobile/app/g/n$a;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/app/g/o;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/g/o;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "externalActivityManager\n\u2026map(ActivityResult::isOk)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 34
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.just(false)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public a()V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/n;->d:Lcom/swedbank/mobile/business/e/j;

    const v1, 0xea61

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/e/j;->a(I)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/n;->d:Lcom/swedbank/mobile/business/e/j;

    .line 41
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.app.action.SET_NEW_PASSWORD"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/e/j$a;->a(Lcom/swedbank/mobile/business/e/j;Landroid/content/Intent;ILandroid/os/Bundle;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/swedbank/mobile/app/g/n$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/g/n$b;-><init>(Lcom/swedbank/mobile/app/g/n;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "externalActivityManager\n\u2026cureLockScreenEnabled() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
