.class public final Lcom/swedbank/mobile/app/g/c;
.super Ljava/lang/Object;
.source "ActivityManagerImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/device/a;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/e/a/b<",
            "Landroid/app/Activity;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/app/Application;

.field private final e:Lcom/swedbank/mobile/business/e/b;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/e/b;Ljava/util/Set;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "intent_handlers"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentHandlers"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/c;->d:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/g/c;->e:Lcom/swedbank/mobile/business/e/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/g/c;->f:Ljava/util/Set;

    .line 30
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/c;->a:Landroid/os/Handler;

    .line 31
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/c;->b:Lcom/b/c/c;

    .line 32
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<ActivityFunction>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/c;->c:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/g/c;)Lcom/b/c/c;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/g/c;->c:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/g/c;)Landroid/os/Handler;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/g/c;->a:Landroid/os/Handler;

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/b/c/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/b/c/c<",
            "Lkotlin/e/a/b<",
            "Landroid/app/Activity;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/c;->c:Lcom/b/c/c;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/c;->b:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    const-string v0, "extra_intent_handled"

    .line 36
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "extra_intent_handled"

    const/4 v1, 0x1

    .line 37
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/c;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/device/h;

    .line 39
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/data/device/h;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    return-void
.end method

.method public a(Lkotlin/e/a/a;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;",
            "Lkotlin/e/a/b<",
            "-",
            "Landroid/app/Activity;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "activityNotAttachedCallback"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "function"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/swedbank/mobile/app/g/c$a;

    invoke-direct {v0, p0, p2, p1}, Lcom/swedbank/mobile/app/g/c$a;-><init>(Lcom/swedbank/mobile/app/g/c;Lkotlin/e/a/b;Lkotlin/e/a/a;)V

    check-cast v0, Lkotlin/e/a/a;

    .line 76
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {p0}, Lcom/swedbank/mobile/app/g/c;->b(Lcom/swedbank/mobile/app/g/c;)Landroid/os/Handler;

    move-result-object p2

    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 77
    invoke-interface {v0}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    goto :goto_0

    .line 78
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/g/c;->b(Lcom/swedbank/mobile/app/g/c;)Landroid/os/Handler;

    move-result-object p1

    new-instance p2, Lcom/swedbank/mobile/app/g/d;

    invoke-direct {p2, v0}, Lcom/swedbank/mobile/app/g/d;-><init>(Lkotlin/e/a/a;)V

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/c;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()V
    .locals 4

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/c;->e:Lcom/swedbank/mobile/business/e/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/c;->d:Landroid/app/Application;

    .line 64
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/swedbank/mobile/app/g/c;->d:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/swedbank/mobile/app/MainActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x10000000

    .line 65
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
