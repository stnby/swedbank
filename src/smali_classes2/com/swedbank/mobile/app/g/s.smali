.class public final Lcom/swedbank/mobile/app/g/s;
.super Ljava/lang/Object;
.source "SystemNotificationStylerImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/o;


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/g/s;->a:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public a(Landroid/app/NotificationChannel;)Landroid/app/NotificationChannel;
    .locals 2
    .param p1    # Landroid/app/NotificationChannel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1a
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "channel"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/s;->a:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f06002e

    .line 36
    invoke-static {v0, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/NotificationChannel;->setLightColor(I)V

    const/4 v0, 0x1

    .line 27
    invoke-virtual {p1, v0}, Landroid/app/NotificationChannel;->enableLights(Z)V

    const/4 v0, 0x0

    .line 28
    invoke-virtual {p1, v0}, Landroid/app/NotificationChannel;->setLockscreenVisibility(I)V

    return-object p1
.end method

.method public a(Landroidx/core/app/i$d;)Landroidx/core/app/i$d;
    .locals 3
    .param p1    # Landroidx/core/app/i$d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f080111

    .line 18
    invoke-virtual {p1, v0}, Landroidx/core/app/i$d;->a(I)Landroidx/core/app/i$d;

    move-result-object p1

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/s;->a:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f06002f

    .line 32
    invoke-static {v0, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v0

    .line 19
    invoke-virtual {p1, v0}, Landroidx/core/app/i$d;->c(I)Landroidx/core/app/i$d;

    move-result-object p1

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/s;->a:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    const v1, 0x7f06002e

    .line 34
    invoke-static {v0, v1}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v0

    const/16 v1, 0x12c

    const/16 v2, 0x64

    .line 20
    invoke-virtual {p1, v0, v1, v2}, Landroidx/core/app/i$d;->a(III)Landroidx/core/app/i$d;

    move-result-object p1

    const/4 v0, 0x0

    .line 21
    invoke-virtual {p1, v0}, Landroidx/core/app/i$d;->d(I)Landroidx/core/app/i$d;

    move-result-object p1

    const-string v0, "builder\n      .setSmallI\u2026ation.VISIBILITY_PRIVATE)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
