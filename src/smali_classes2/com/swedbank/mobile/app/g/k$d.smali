.class final Lcom/swedbank/mobile/app/g/k$d;
.super Ljava/lang/Object;
.source "ExternalActivityManagerImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/g/k;->a(Landroid/content/Intent;ILandroid/os/Bundle;)Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/g/k$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/g/k$d;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/g/k$d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/g/k$d;->a:Lcom/swedbank/mobile/app/g/k$d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/p;)Lcom/swedbank/mobile/business/e/d;
    .locals 2
    .param p1    # Lkotlin/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/p<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "+",
            "Landroid/content/Intent;",
            ">;)",
            "Lcom/swedbank/mobile/business/e/d;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/swedbank/mobile/business/e/d;

    invoke-virtual {p1}, Lkotlin/p;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-virtual {p1}, Lkotlin/p;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Intent;

    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/business/e/d;-><init>(ILandroid/content/Intent;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lkotlin/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/g/k$d;->a(Lkotlin/p;)Lcom/swedbank/mobile/business/e/d;

    move-result-object p1

    return-object p1
.end method
