.class public final Lcom/swedbank/mobile/app/g/k;
.super Ljava/lang/Object;
.source "ExternalActivityManagerImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/j;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/p<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/p<",
            "Landroid/content/Intent;",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Trip\u2026, ResultCode, Intent?>>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/g/k;->a:Lcom/b/c/c;

    .line 21
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Trip\u2026 RequestCode, Bundle?>>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/g/k;->b:Lcom/b/c/c;

    .line 22
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<RequestCode>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/g/k;->c:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/g/k;)Lcom/b/c/c;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/app/g/k;->b:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/p<",
            "Landroid/content/Intent;",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/k;->b:Lcom/b/c/c;

    .line 35
    sget-object v1, Lcom/swedbank/mobile/app/g/k$b;->a:Lcom/swedbank/mobile/app/g/k$b;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "intentStream\n      .doOn\u2026st code: ${it.second}\") }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)Lio/reactivex/w;
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "I",
            "Landroid/os/Bundle;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/e/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/k;->a:Lcom/b/c/c;

    .line 26
    new-instance v1, Lcom/swedbank/mobile/app/g/k$c;

    invoke-direct {v1, p2}, Lcom/swedbank/mobile/app/g/k$c;-><init>(I)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/g/k$d;->a:Lcom/swedbank/mobile/app/g/k$d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/swedbank/mobile/app/g/k$e;

    invoke-direct {v1, p2}, Lcom/swedbank/mobile/app/g/k$e;-><init>(I)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/swedbank/mobile/app/g/k$f;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/g/k$f;-><init>(Lcom/swedbank/mobile/app/g/k;Landroid/content/Intent;ILandroid/os/Bundle;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/c/g;)Lio/reactivex/w;

    move-result-object p1

    const-string p2, "activityResultStream\n   \u2026 requestCode, options)) }"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(I)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/k;->c:Lcom/b/c/c;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2
    .param p3    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/k;->a:Lcom/b/c/c;

    new-instance v1, Lkotlin/p;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-direct {v1, p1, p2, p3}, Lkotlin/p;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/g/k;->c:Lcom/b/c/c;

    .line 38
    sget-object v1, Lcom/swedbank/mobile/app/g/k$a;->a:Lcom/swedbank/mobile/app/g/k$a;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "intentDismissStream\n    \u2026ith request code: $it\") }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
