.class public final Lcom/swedbank/mobile/app/c/c$e;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f110000

.field public static final abc_action_bar_up_description:I = 0x7f110001

.field public static final abc_action_menu_overflow_description:I = 0x7f110002

.field public static final abc_action_mode_done:I = 0x7f110003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f110004

.field public static final abc_activitychooserview_choose_application:I = 0x7f110005

.field public static final abc_capital_off:I = 0x7f110006

.field public static final abc_capital_on:I = 0x7f110007

.field public static final abc_font_family_body_1_material:I = 0x7f110008

.field public static final abc_font_family_body_2_material:I = 0x7f110009

.field public static final abc_font_family_button_material:I = 0x7f11000a

.field public static final abc_font_family_caption_material:I = 0x7f11000b

.field public static final abc_font_family_display_1_material:I = 0x7f11000c

.field public static final abc_font_family_display_2_material:I = 0x7f11000d

.field public static final abc_font_family_display_3_material:I = 0x7f11000e

.field public static final abc_font_family_display_4_material:I = 0x7f11000f

.field public static final abc_font_family_headline_material:I = 0x7f110010

.field public static final abc_font_family_menu_material:I = 0x7f110011

.field public static final abc_font_family_subhead_material:I = 0x7f110012

.field public static final abc_font_family_title_material:I = 0x7f110013

.field public static final abc_menu_alt_shortcut_label:I = 0x7f110014

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f110015

.field public static final abc_menu_delete_shortcut_label:I = 0x7f110016

.field public static final abc_menu_enter_shortcut_label:I = 0x7f110017

.field public static final abc_menu_function_shortcut_label:I = 0x7f110018

.field public static final abc_menu_meta_shortcut_label:I = 0x7f110019

.field public static final abc_menu_shift_shortcut_label:I = 0x7f11001a

.field public static final abc_menu_space_shortcut_label:I = 0x7f11001b

.field public static final abc_menu_sym_shortcut_label:I = 0x7f11001c

.field public static final abc_prepend_shortcut_label:I = 0x7f11001d

.field public static final abc_search_hint:I = 0x7f11001e

.field public static final abc_searchview_description_clear:I = 0x7f11001f

.field public static final abc_searchview_description_query:I = 0x7f110020

.field public static final abc_searchview_description_search:I = 0x7f110021

.field public static final abc_searchview_description_submit:I = 0x7f110022

.field public static final abc_searchview_description_voice:I = 0x7f110023

.field public static final abc_shareactionprovider_share_with:I = 0x7f110024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f110025

.field public static final abc_toolbar_collapse_description:I = 0x7f110026

.field public static final appbar_scrolling_view_behavior:I = 0x7f11002d

.field public static final biometric_auth_default_instruction_text:I = 0x7f11002f

.field public static final biometric_auth_fingerprint_not_recognized:I = 0x7f110030

.field public static final biometric_auth_fingerprint_recognized:I = 0x7f110031

.field public static final biometric_auth_generic_error:I = 0x7f110032

.field public static final biometric_disabled_confirmation:I = 0x7f110033

.field public static final biometric_disabled_reason_device:I = 0x7f110034

.field public static final biometric_disabled_reason_remote:I = 0x7f110035

.field public static final biometric_disabled_title:I = 0x7f110036

.field public static final biometric_login_auth_title:I = 0x7f110037

.field public static final biometric_login_error_device_problem:I = 0x7f110038

.field public static final biometric_login_error_lockout:I = 0x7f110039

.field public static final biometric_login_error_permanent_lockout:I = 0x7f11003a

.field public static final biometric_login_error_remote:I = 0x7f11003b

.field public static final biometric_login_method_login_btn:I = 0x7f11003c

.field public static final biometric_login_preference_title:I = 0x7f11003d

.field public static final biometric_onboarding_auth_text:I = 0x7f11003e

.field public static final biometric_onboarding_auth_title:I = 0x7f11003f

.field public static final biometric_onboarding_confirmation_description:I = 0x7f110040

.field public static final biometric_onboarding_confirmation_title:I = 0x7f110041

.field public static final biometric_onboarding_error_canceled_action_text:I = 0x7f110042

.field public static final biometric_onboarding_error_canceled_message:I = 0x7f110043

.field public static final biometric_onboarding_error_canceled_title:I = 0x7f110044

.field public static final biometric_onboarding_error_enrolling_action_text:I = 0x7f110045

.field public static final biometric_onboarding_error_enrolling_message:I = 0x7f110046

.field public static final biometric_onboarding_error_enrolling_title:I = 0x7f110047

.field public static final biometric_onboarding_error_fatal_action_text:I = 0x7f110048

.field public static final biometric_onboarding_error_fatal_message:I = 0x7f110049

.field public static final biometric_onboarding_error_fatal_not_enabled_title:I = 0x7f11004a

.field public static final biometric_onboarding_error_fatal_title:I = 0x7f11004b

.field public static final biometric_onboarding_error_retryable_action_text:I = 0x7f11004c

.field public static final biometric_onboarding_error_retryable_message:I = 0x7f11004d

.field public static final biometric_onboarding_error_retryable_title:I = 0x7f11004e

.field public static final biometric_onboarding_error_too_many_attempts_action_text:I = 0x7f11004f

.field public static final biometric_onboarding_error_too_many_attempts_message:I = 0x7f110050

.field public static final biometric_onboarding_error_too_many_attempts_title:I = 0x7f110051

.field public static final bottom_sheet_behavior:I = 0x7f110055

.field public static final character_counter_content_description:I = 0x7f1100df

.field public static final character_counter_pattern:I = 0x7f1100e0

.field public static final common_google_play_services_enable_button:I = 0x7f1100e9

.field public static final common_google_play_services_enable_text:I = 0x7f1100ea

.field public static final common_google_play_services_enable_title:I = 0x7f1100eb

.field public static final common_google_play_services_install_button:I = 0x7f1100ec

.field public static final common_google_play_services_install_text:I = 0x7f1100ed

.field public static final common_google_play_services_install_title:I = 0x7f1100ee

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1100ef

.field public static final common_google_play_services_notification_ticker:I = 0x7f1100f0

.field public static final common_google_play_services_unknown_issue:I = 0x7f1100f1

.field public static final common_google_play_services_unsupported_text:I = 0x7f1100f2

.field public static final common_google_play_services_update_button:I = 0x7f1100f3

.field public static final common_google_play_services_update_text:I = 0x7f1100f4

.field public static final common_google_play_services_update_title:I = 0x7f1100f5

.field public static final common_google_play_services_updating_text:I = 0x7f1100f6

.field public static final common_google_play_services_wear_update_text:I = 0x7f1100f7

.field public static final common_open_on_phone:I = 0x7f1100f8

.field public static final common_signin_button_text:I = 0x7f1100f9

.field public static final common_signin_button_text_long:I = 0x7f1100fa

.field public static final contact_method_appointment_description:I = 0x7f1100fb

.field public static final contact_method_appointment_name:I = 0x7f1100fc

.field public static final contact_method_call_name:I = 0x7f1100fd

.field public static final contact_method_feedback_action_body:I = 0x7f1100fe

.field public static final contact_method_feedback_action_subject:I = 0x7f1100ff

.field public static final contact_method_feedback_name:I = 0x7f110100

.field public static final contact_method_map_description:I = 0x7f110101

.field public static final contact_method_map_name:I = 0x7f110102

.field public static final contact_method_skype_name:I = 0x7f110103

.field public static final contact_method_write_name:I = 0x7f110104

.field public static final contact_separator_feedback_title:I = 0x7f110105

.field public static final contact_separator_general_title:I = 0x7f110106

.field public static final contact_tab_business:I = 0x7f110107

.field public static final contact_tab_private:I = 0x7f110108

.field public static final contact_toolbar_title:I = 0x7f110109

.field public static final error_general_error:I = 0x7f110125

.field public static final error_no_internet:I = 0x7f110126

.field public static final error_server_error:I = 0x7f110127

.field public static final error_server_maintenance:I = 0x7f110128

.field public static final external_biometric_authentication_confirm_action:I = 0x7f110129

.field public static final external_biometric_authentication_control_code:I = 0x7f11012a

.field public static final fab_transformation_scrim_behavior:I = 0x7f11012b

.field public static final fab_transformation_sheet_behavior:I = 0x7f11012c

.field public static final fcm_fallback_notification_channel_label:I = 0x7f11012d

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f11013c

.field public static final login_loading_text:I = 0x7f11014a

.field public static final login_welcome:I = 0x7f110155

.field public static final menu_action_message_center:I = 0x7f11015d

.field public static final menu_action_profile:I = 0x7f11015e

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f11015f

.field public static final navigation_title_contacts:I = 0x7f110161

.field public static final onboarding_cancel:I = 0x7f11016d

.field public static final onboarding_done:I = 0x7f11016e

.field public static final onboarding_intro_action:I = 0x7f11016f

.field public static final onboarding_intro_skip_tour:I = 0x7f110170

.field public static final onboarding_intro_text:I = 0x7f110171

.field public static final onboarding_intro_title:I = 0x7f110172

.field public static final onboarding_text:I = 0x7f110173

.field public static final onboarding_title:I = 0x7f110174

.field public static final onboarding_tour_biometric_text:I = 0x7f110175

.field public static final onboarding_tour_biometric_title:I = 0x7f110176

.field public static final onboarding_tour_cancel:I = 0x7f110177

.field public static final onboarding_tour_finish:I = 0x7f110178

.field public static final onboarding_tour_next:I = 0x7f110179

.field public static final password_toggle_content_description:I = 0x7f1101a5

.field public static final path_password_eye:I = 0x7f1101a6

.field public static final path_password_eye_mask_strike_through:I = 0x7f1101a7

.field public static final path_password_eye_mask_visible:I = 0x7f1101a8

.field public static final path_password_strike_through:I = 0x7f1101a9

.field public static final recurring_login_alternative_login_btn:I = 0x7f1101f2

.field public static final recurring_login_welcome:I = 0x7f1101f3

.field public static final retry_btn:I = 0x7f1101f4

.field public static final root_reason_alcatel:I = 0x7f1101f5

.field public static final root_reason_bbox:I = 0x7f1101f6

.field public static final root_reason_bin:I = 0x7f1101f7

.field public static final root_reason_cloak_apps:I = 0x7f1101f8

.field public static final root_reason_connection:I = 0x7f1101f9

.field public static final root_reason_dang_apps:I = 0x7f1101fa

.field public static final root_reason_dang_props:I = 0x7f1101fb

.field public static final root_reason_debuggable:I = 0x7f1101fc

.field public static final root_reason_device_files:I = 0x7f1101fd

.field public static final root_reason_general:I = 0x7f1101fe

.field public static final root_reason_memory:I = 0x7f1101ff

.field public static final root_reason_mgm_apps:I = 0x7f110200

.field public static final root_reason_proc:I = 0x7f110201

.field public static final root_reason_stacktrace:I = 0x7f110202

.field public static final root_reason_strings:I = 0x7f110203

.field public static final root_reason_su:I = 0x7f110204

.field public static final root_reason_test_keys:I = 0x7f110205

.field public static final search_menu_title:I = 0x7f110208

.field public static final status_bar_notification_info_overflow:I = 0x7f11028f

.field public static final tag_expand_transition_target:I = 0x7f110294

.field public static final tag_wallet_onboarding_step:I = 0x7f110297
