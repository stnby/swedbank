.class public final Lcom/swedbank/mobile/app/c/c/g;
.super Lcom/swedbank/mobile/app/c/a/d$b;
.source "BiometricOnboardingPromptInformation.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/c/c/g;

.field private static final b:I

.field private static final c:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/swedbank/mobile/app/c/c/g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/c/c/g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/c/c/g;->a:Lcom/swedbank/mobile/app/c/c/g;

    .line 7
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_auth_title:I

    sput v0, Lcom/swedbank/mobile/app/c/c/g;->b:I

    .line 8
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_auth_text:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/c/c/g;->c:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 7
    sget v0, Lcom/swedbank/mobile/app/c/c/g;->b:I

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 8
    sget-object v0, Lcom/swedbank/mobile/app/c/c/g;->c:Ljava/lang/Integer;

    return-object v0
.end method
