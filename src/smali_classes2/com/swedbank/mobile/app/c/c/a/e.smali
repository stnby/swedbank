.class public final Lcom/swedbank/mobile/app/c/c/a/e;
.super Lcom/swedbank/mobile/architect/a/h;
.source "BiometricOnboardingErrorHandlerRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/onboarding/error/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/business/e/j;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/j;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_onboarding_error_handler"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_onboarding_error_handler"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/e/j;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "externalActivityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p2, p4, p3}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/a/e;->e:Lcom/swedbank/mobile/business/e/j;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/a/e;->e:Lcom/swedbank/mobile/business/e/j;

    .line 25
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SECURITY_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/business/e/j$a;->a(Lcom/swedbank/mobile/business/e/j;Landroid/content/Intent;ILandroid/os/Bundle;ILjava/lang/Object;)Lio/reactivex/w;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lio/reactivex/w;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "externalActivityManager\n\u2026))\n      .ignoreElement()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
