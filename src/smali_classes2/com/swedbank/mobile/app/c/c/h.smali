.class public final Lcom/swedbank/mobile/app/c/c/h;
.super Lcom/swedbank/mobile/architect/a/h;
.source "BiometricOnboardingRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/onboarding/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/c/a/g;

.field private final f:Lcom/swedbank/mobile/app/c/c/a/a;

.field private final g:Lcom/swedbank/mobile/business/biometric/authentication/h;

.field private final h:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/c/a/g;Lcom/swedbank/mobile/app/c/c/a/a;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/biometric/onboarding/error/e;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/c/a/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/c/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/biometric/onboarding/error/e;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/c/a/g;",
            "Lcom/swedbank/mobile/app/c/c/a/a;",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/error/e;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricAuthenticationPromptBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricOnboardingErrorHandlerBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricOnboardingErrorHandlerListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p5, p7, p6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/h;->e:Lcom/swedbank/mobile/app/c/a/g;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/c/h;->f:Lcom/swedbank/mobile/app/c/c/a/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/c/h;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    iput-object p4, p0, Lcom/swedbank/mobile/app/c/c/h;->h:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 50
    sget-object v0, Lcom/swedbank/mobile/app/c/c/h$a;->a:Lcom/swedbank/mobile/app/c/c/h$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/authentication/p$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/h;->f:Lcom/swedbank/mobile/app/c/c/a/a;

    .line 41
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/c/c/a/a;->a(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/c/c/a/a;

    move-result-object p1

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/h;->h:Lcom/swedbank/mobile/business/biometric/onboarding/error/e;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/c/c/a/a;->a(Lcom/swedbank/mobile/business/biometric/onboarding/error/e;)Lcom/swedbank/mobile/app/c/c/a/a;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/c/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 52
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "authUserId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/h;->e:Lcom/swedbank/mobile/app/c/a/g;

    .line 32
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/c/h;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 33
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/c/a/g;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object p1

    .line 34
    sget-object v0, Lcom/swedbank/mobile/app/c/c/g;->a:Lcom/swedbank/mobile/app/c/c/g;

    check-cast v0, Lcom/swedbank/mobile/app/c/a/d;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/g;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 49
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 53
    sget-object v0, Lcom/swedbank/mobile/app/c/c/h$b;->a:Lcom/swedbank/mobile/app/c/c/h$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
