.class public final Lcom/swedbank/mobile/app/c/c/a/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BiometricOnboardingErrorHandlerViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/c/a/g;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/c/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/c/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "actionBtn"

    const-string v4, "getActionBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/c/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "titleView"

    const-string v4, "getTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/c/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "messageView"

    const-string v4, "getMessageView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/c/c/a/h;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/a/h;->f:Lio/reactivex/o;

    .line 25
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_onboarding_error_handler_cancel_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/a/h;->b:Lkotlin/f/c;

    .line 26
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_onboarding_error_handler_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/a/h;->c:Lkotlin/f/c;

    .line 27
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_onboarding_error_handler_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/a/h;->d:Lkotlin/f/c;

    .line 28
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_onboarding_error_handler_message:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/c/a/h;->e:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/c/a/h;->f()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/c/a/h;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/a/h;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/c/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/c/a/h;->d()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/a/h;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/c/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/c/a/h;->c()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/a/h;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/c/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/a/h;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/c/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/c/a/h;->f:Lio/reactivex/o;

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/c/a/h;->c()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 84
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 32
    invoke-virtual {v0, v2}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeWith(cancelBtn.clicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/c/c/a/k;)V
    .locals 5
    .param p1    # Lcom/swedbank/mobile/app/c/c/a/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/c/a/k;->a()Lcom/swedbank/mobile/business/biometric/onboarding/error/g;

    move-result-object p1

    if-eqz p1, :cond_5

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/c/c/a/h;

    .line 86
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$d;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0x8

    if-eqz v1, :cond_0

    .line 87
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_fatal_title:I

    .line 88
    sget v1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_fatal_message:I

    .line 89
    sget v3, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_fatal_action_text:I

    .line 91
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 92
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 93
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setText(I)V

    .line 94
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 95
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 98
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$e;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 99
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_retryable_title:I

    .line 100
    sget v1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_retryable_message:I

    .line 101
    sget v2, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_retryable_action_text:I

    .line 104
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 105
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 106
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setText(I)V

    .line 107
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 108
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 111
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$a;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_canceled_title:I

    .line 113
    sget v1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_canceled_message:I

    .line 114
    sget v2, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_canceled_action_text:I

    .line 117
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 118
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 119
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setText(I)V

    .line 120
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 121
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 124
    :cond_2
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$c;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_enrolling_title:I

    .line 126
    sget v1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_enrolling_message:I

    .line 127
    sget v2, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_enrolling_action_text:I

    .line 130
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 131
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 132
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setText(I)V

    .line 133
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 134
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 137
    :cond_3
    sget-object v1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;->a:Lcom/swedbank/mobile/business/biometric/onboarding/error/g$f;

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 138
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_too_many_attempts_title:I

    .line 139
    sget v1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_too_many_attempts_message:I

    .line 140
    sget v3, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_too_many_attempts_action_text:I

    .line 142
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 143
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 144
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setText(I)V

    .line 145
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 146
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 149
    :cond_4
    instance-of v1, p1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;

    if-eqz v1, :cond_5

    .line 150
    sget v1, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_fatal_not_enabled_title:I

    .line 151
    check-cast p1, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/onboarding/error/g$b;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object p1

    .line 152
    sget-object v3, Lcom/swedbank/mobile/app/c/c/a/i;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/l;->ordinal()I

    move-result p1

    aget p1, v3, p1

    packed-switch p1, :pswitch_data_0

    .line 169
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_debuggable:I

    goto :goto_0

    .line 168
    :pswitch_1
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_stacktrace:I

    goto :goto_0

    .line 167
    :pswitch_2
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_connection:I

    goto :goto_0

    .line 166
    :pswitch_3
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_device_files:I

    goto :goto_0

    .line 165
    :pswitch_4
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_strings:I

    goto :goto_0

    .line 164
    :pswitch_5
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_proc:I

    goto :goto_0

    .line 163
    :pswitch_6
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_memory:I

    goto :goto_0

    .line 162
    :pswitch_7
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_alcatel:I

    goto :goto_0

    .line 161
    :pswitch_8
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_test_keys:I

    goto :goto_0

    .line 160
    :pswitch_9
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_props:I

    goto :goto_0

    .line 159
    :pswitch_a
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_su:I

    goto :goto_0

    .line 158
    :pswitch_b
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_mgm_apps:I

    goto :goto_0

    .line 157
    :pswitch_c
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_apps:I

    goto :goto_0

    .line 156
    :pswitch_d
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_cloak_apps:I

    goto :goto_0

    .line 155
    :pswitch_e
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_bin:I

    goto :goto_0

    .line 154
    :pswitch_f
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_bbox:I

    goto :goto_0

    .line 153
    :pswitch_10
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_general:I

    .line 171
    :goto_0
    sget v3, Lcom/swedbank/mobile/app/c/c$e;->biometric_onboarding_error_fatal_action_text:I

    .line 182
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(I)V

    .line 183
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->b(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 184
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->c(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setText(I)V

    .line 185
    invoke-static {v0}, Lcom/swedbank/mobile/app/c/c/a/h;->d(Lcom/swedbank/mobile/app/c/c/a/h;)Landroid/widget/Button;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 186
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/app/c/c/a/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/c/a/h;->a(Lcom/swedbank/mobile/app/c/c/a/k;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/c/a/h;->d()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 83
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
