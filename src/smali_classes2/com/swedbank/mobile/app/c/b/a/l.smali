.class public final Lcom/swedbank/mobile/app/c/b/a/l;
.super Ljava/lang/Object;
.source "BiometricLoginMethodViewImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/c/b/a/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/l;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/c/b/a/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/b;",
            ">;)",
            "Lcom/swedbank/mobile/app/c/b/a/l;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/swedbank/mobile/app/c/b/a/l;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/c/b/a/l;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/c/b/a/j;
    .locals 2

    .line 18
    new-instance v0, Lcom/swedbank/mobile/app/c/b/a/j;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/b/a/l;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/e/b;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/c/b/a/j;-><init>(Lcom/swedbank/mobile/business/e/b;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/b/a/l;->a()Lcom/swedbank/mobile/app/c/b/a/j;

    move-result-object v0

    return-object v0
.end method
