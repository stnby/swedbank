.class public final Lcom/swedbank/mobile/app/c/b/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "BiometricLoginRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/login/e;


# instance fields
.field private final e:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/core/a/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/core/a/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/core/a/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    .line 23
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/d;->e:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/b/d;->f:Lcom/swedbank/mobile/core/a/c;

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 8

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/d;->f:Lcom/swedbank/mobile/core/a/c;

    new-instance v7, Lcom/swedbank/mobile/app/c/a/t;

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v7

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/c/a/t;-><init>(JLcom/swedbank/mobile/core/a/b;ILkotlin/e/b/g;)V

    check-cast v7, Lcom/swedbank/mobile/core/a/a;

    invoke-interface {v0, v7}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/biometric/login/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/d;->e:Lcom/swedbank/mobile/architect/business/a/c;

    .line 25
    invoke-static {v0, p1}, Lcom/swedbank/mobile/app/c/a;->a(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/biometric/login/g;)V

    return-void
.end method
