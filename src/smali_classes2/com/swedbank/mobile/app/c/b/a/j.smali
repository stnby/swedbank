.class public final Lcom/swedbank/mobile/app/c/b/a/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BiometricLoginMethodViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/b/a/i;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/d;

.field private final h:Lcom/swedbank/mobile/business/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/b/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loginContainer"

    const-string v4, "getLoginContainer()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/b/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/b/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "biometricLoginBtn"

    const-string v4, "getBiometricLoginBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/b/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/b/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "altLoginBtn"

    const-string v4, "getAltLoginBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/c/b/a/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/e/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->h:Lcom/swedbank/mobile/business/e/b;

    .line 25
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_login_method_login_container:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->c:Lkotlin/f/c;

    .line 26
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_login_method_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->d:Lkotlin/f/c;

    .line 27
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_login_method_login_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->e:Lkotlin/f/c;

    .line 28
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->biometric_login_method_cancel_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->f:Lkotlin/f/c;

    .line 29
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/c/b/a/j$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/c/b/a/j$a;-><init>(Lcom/swedbank/mobile/app/c/b/a/j;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->g:Lkotlin/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/view/View;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->c()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/view/View;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->f()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->i()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->h()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->g()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final g()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final h()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final i()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->g:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->h:Lcom/swedbank/mobile/business/e/b;

    .line 42
    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->a()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 43
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    .line 44
    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/j$b;->a:Lcom/swedbank/mobile/app/c/b/a/j$b;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 45
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->g()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 86
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 40
    invoke-static {v0, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026n\n          .clicks()\n  )"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/c/b/a/m;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/app/c/b/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->n()Landroid/content/Context;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/b/a/m;->b()Lcom/swedbank/mobile/business/biometric/login/a;

    move-result-object v1

    .line 87
    instance-of v2, v1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    const/16 v3, 0x8

    if-eqz v2, :cond_0

    check-cast v1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/biometric/login/a$c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->e(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/b/a/m;->b()Lcom/swedbank/mobile/business/biometric/login/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/swedbank/mobile/app/c/a/a;->a(Lcom/swedbank/mobile/business/biometric/login/a;Landroid/content/Context;)Lcom/swedbank/mobile/app/w/h;

    move-result-object v1

    .line 52
    invoke-static {p0, v0, v1}, Lcom/swedbank/mobile/app/w/d;->a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V

    .line 55
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/b/a/m;->a()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 93
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->a(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/view/View;

    move-result-object p1

    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 94
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->b(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->c(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 96
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->d(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 99
    :cond_2
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->a(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 100
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->b(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->c(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 102
    :cond_3
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->d(Lcom/swedbank/mobile/app/c/b/a/j;)Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/app/c/b/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/b/a/j;->a(Lcom/swedbank/mobile/app/c/b/a/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/b/a/j;->h()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 85
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 3

    .line 82
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 83
    new-instance v1, Lcom/swedbank/mobile/app/c/b/a/j$c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/c/b/a/j$c;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/c/b/a/k;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/c/b/a/k;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 84
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/b/a/j;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    return-void
.end method
