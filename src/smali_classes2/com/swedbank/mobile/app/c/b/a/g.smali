.class public final Lcom/swedbank/mobile/app/c/b/a/g;
.super Lcom/swedbank/mobile/architect/a/h;
.source "BiometricLoginMethodRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/login/method/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/app/c/a/g;

.field private final g:Lcom/swedbank/mobile/business/biometric/authentication/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/app/c/a/g;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/a/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_login_method"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_login_method"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_login_method"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/app/c/a/g;",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p4, p6, p5}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/g;->e:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/b/a/g;->f:Lcom/swedbank/mobile/app/c/a/g;

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/b/a/g;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/b/a/g;)Lcom/swedbank/mobile/app/c/a/g;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/b/a/g;->f:Lcom/swedbank/mobile/app/c/a/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/b/a/g;)Lcom/swedbank/mobile/business/biometric/authentication/h;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/b/a/g;->g:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 54
    sget-object v0, Lcom/swedbank/mobile/app/c/b/a/g$a;->a:Lcom/swedbank/mobile/app/c/b/a/g$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "authenticationResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/b/a/g;->e:Lcom/swedbank/mobile/architect/business/a/c;

    .line 47
    new-instance v1, Lcom/swedbank/mobile/business/navigation/b;

    .line 48
    new-instance v2, Lcom/swedbank/mobile/business/authentication/g$a;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/business/authentication/g$a;-><init>(Lcom/swedbank/mobile/business/authentication/p;)V

    check-cast v2, Lcom/swedbank/mobile/business/authentication/g;

    const/4 p1, 0x0

    const/4 v3, 0x2

    .line 47
    invoke-direct {v1, v2, p1, v3, p1}, Lcom/swedbank/mobile/business/navigation/b;-><init>(Lcom/swedbank/mobile/business/authentication/g;Ljava/lang/String;ILkotlin/e/b/g;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "customerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/c/b/a/g$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/c/b/a/g$b;-><init>(Lcom/swedbank/mobile/app/c/b/a/g;Lcom/swedbank/mobile/business/util/l;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/b/a/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method
