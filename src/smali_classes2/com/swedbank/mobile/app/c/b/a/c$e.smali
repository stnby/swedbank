.class final Lcom/swedbank/mobile/app/c/b/a/c$e;
.super Ljava/lang/Object;
.source "BiometricLoginMethodPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/b/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/c/b/a/c$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/c/b/a/c$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/c/b/a/c$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/c/b/a/c$e;->a:Lcom/swedbank/mobile/app/c/b/a/c$e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/login/a;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/c/b/a/m$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/swedbank/mobile/app/c/b/a/m$a$b;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/c/b/a/m$a$b;-><init>(Lcom/swedbank/mobile/business/biometric/login/a;)V

    .line 33
    sget-object p1, Lcom/swedbank/mobile/app/c/b/a/m$a$a;->a:Lcom/swedbank/mobile/app/c/b/a/m$a$a;

    .line 74
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xdac

    invoke-static {v2, v3, v1}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    .line 73
    new-instance v2, Lcom/swedbank/mobile/app/c/b/a/c$e$a;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/c/b/a/c$e$a;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 72
    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/b/a/c$e;->a(Lcom/swedbank/mobile/business/biometric/login/a;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
