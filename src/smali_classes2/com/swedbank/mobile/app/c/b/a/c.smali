.class public final Lcom/swedbank/mobile/app/c/b/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "BiometricLoginMethodPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/c/b/a/i;",
        "Lcom/swedbank/mobile/app/c/b/a/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/login/method/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/login/method/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/method/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/b/a/c;->a:Lcom/swedbank/mobile/business/biometric/login/method/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/b/a/c;)Lcom/swedbank/mobile/business/biometric/login/method/a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/b/a/c;->a:Lcom/swedbank/mobile/business/biometric/login/method/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 6

    .line 18
    sget-object v0, Lcom/swedbank/mobile/app/c/b/a/c$f;->a:Lcom/swedbank/mobile/app/c/b/a/c$f;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/b/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 19
    new-instance v1, Lcom/swedbank/mobile/app/c/b/a/c$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/c/b/a/c$g;-><init>(Lcom/swedbank/mobile/app/c/b/a/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 23
    sget-object v1, Lcom/swedbank/mobile/app/c/b/a/c$c;->a:Lcom/swedbank/mobile/app/c/b/a/c$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/c/b/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    new-instance v2, Lcom/swedbank/mobile/app/c/b/a/c$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/c/b/a/c$d;-><init>(Lcom/swedbank/mobile/app/c/b/a/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 28
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/b/a/c;->a:Lcom/swedbank/mobile/business/biometric/login/method/a;

    .line 29
    invoke-interface {v2}, Lcom/swedbank/mobile/business/biometric/login/method/a;->f()Lio/reactivex/o;

    move-result-object v2

    .line 30
    sget-object v3, Lcom/swedbank/mobile/app/c/b/a/c$e;->a:Lcom/swedbank/mobile/app/c/b/a/c$e;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 37
    iget-object v3, p0, Lcom/swedbank/mobile/app/c/b/a/c;->a:Lcom/swedbank/mobile/business/biometric/login/method/a;

    .line 38
    invoke-interface {v3}, Lcom/swedbank/mobile/business/biometric/login/method/a;->e()Lio/reactivex/o;

    move-result-object v3

    .line 39
    sget-object v4, Lcom/swedbank/mobile/app/c/b/a/c$b;->a:Lcom/swedbank/mobile/app/c/b/a/c$b;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_0

    new-instance v5, Lcom/swedbank/mobile/app/c/b/a/e;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/c/b/a/e;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_0
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 42
    check-cast v0, Lio/reactivex/s;

    .line 43
    check-cast v1, Lio/reactivex/s;

    .line 44
    check-cast v2, Lio/reactivex/s;

    .line 45
    check-cast v3, Lio/reactivex/s;

    .line 41
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026m,\n        loadingStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v1, Lcom/swedbank/mobile/app/c/b/a/m;

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3, v4}, Lcom/swedbank/mobile/app/c/b/a/m;-><init>(ZLcom/swedbank/mobile/business/biometric/login/a;ILkotlin/e/b/g;)V

    .line 48
    new-instance v2, Lcom/swedbank/mobile/app/c/b/a/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/c/b/a/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/c/b/a/c$a;-><init>(Lcom/swedbank/mobile/app/c/b/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 68
    new-instance v3, Lcom/swedbank/mobile/app/c/b/a/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/c/b/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 69
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
