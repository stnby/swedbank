.class public final Lcom/swedbank/mobile/app/c/a/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "ExternalBiometricAuthenticationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/c/a/a/i;",
        "Lcom/swedbank/mobile/app/c/a/a/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/authentication/external/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/external/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/external/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/c;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/a/c;)Lcom/swedbank/mobile/business/biometric/authentication/external/d;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/a/c;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/d;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 12

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/c;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/d;

    .line 20
    invoke-interface {v0}, Lcom/swedbank/mobile/business/biometric/authentication/external/d;->a()Lio/reactivex/o;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/c$b;->a:Lcom/swedbank/mobile/app/c/a/a/c$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/c$f;->a:Lcom/swedbank/mobile/app/c/a/a/c$f;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/c/a/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    new-instance v2, Lcom/swedbank/mobile/app/c/a/a/c$g;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/c/a/a/c$g;-><init>(Lcom/swedbank/mobile/app/c/a/a/c;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/a/c;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/d;

    .line 34
    invoke-interface {v2}, Lcom/swedbank/mobile/business/biometric/authentication/external/d;->b()Lio/reactivex/o;

    move-result-object v2

    .line 35
    sget-object v3, Lcom/swedbank/mobile/app/c/a/a/c$c;->a:Lcom/swedbank/mobile/app/c/a/a/c$c;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 49
    sget-object v3, Lcom/swedbank/mobile/app/c/a/a/c$d;->a:Lcom/swedbank/mobile/app/c/a/a/c$d;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/c/a/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 50
    new-instance v4, Lcom/swedbank/mobile/app/c/a/a/c$e;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/c/a/a/c$e;-><init>(Lcom/swedbank/mobile/app/c/a/a/c;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 51
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 52
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 55
    check-cast v0, Lio/reactivex/s;

    .line 56
    check-cast v1, Lio/reactivex/s;

    .line 57
    check-cast v2, Lio/reactivex/s;

    .line 58
    check-cast v3, Lio/reactivex/s;

    .line 54
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026am,\n        cancelStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v1, Lcom/swedbank/mobile/app/c/a/a/n;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7f

    const/4 v11, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v11}, Lcom/swedbank/mobile/app/c/a/a/n;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/c/a/a/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/c/a/a/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/c/a/a/c$a;-><init>(Lcom/swedbank/mobile/app/c/a/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 98
    new-instance v3, Lcom/swedbank/mobile/app/c/a/a/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/c/a/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 99
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
