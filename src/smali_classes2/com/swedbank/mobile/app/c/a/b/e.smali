.class public final Lcom/swedbank/mobile/app/c/a/b/e;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BiometricViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/a/b/d;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/b/e;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loginBtn"

    const-string v4, "getLoginBtn()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/b/e;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/b/e;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/c/a/b/e;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 25
    sget v0, Lcom/swedbank/mobile/app/c/c$b;->login_biometric_login_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->c:Lkotlin/f/c;

    .line 26
    sget v0, Lcom/swedbank/mobile/app/c/c$b;->login_biometric_loading:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->d:Lkotlin/f/c;

    .line 27
    sget v0, Lcom/swedbank/mobile/app/c/c$b;->login_biometric_cancel_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->e:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/b/e;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->f()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/a/b/e;)Landroid/widget/Button;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->g()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final c()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/b/e;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/a/b/e;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->c()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/b/e;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final g()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/b/e;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->c()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 89
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/c/a/b/h;)V
    .locals 5
    .param p1    # Lcom/swedbank/mobile/app/c/a/b/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->n()Landroid/content/Context;

    move-result-object v0

    .line 42
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/b/h;->b()Lcom/swedbank/mobile/business/biometric/login/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/swedbank/mobile/app/c/a/a;->a(Lcom/swedbank/mobile/business/biometric/login/a;Landroid/content/Context;)Lcom/swedbank/mobile/app/w/h;

    move-result-object v1

    .line 40
    invoke-static {p0, v0, v1}, Lcom/swedbank/mobile/app/w/d;->a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/b/h;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 95
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->login_loading_text:I

    .line 97
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->c(Lcom/swedbank/mobile/app/c/a/b/e;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v2

    .line 98
    move-object v3, v2

    check-cast v3, Landroid/view/View;

    const/4 v4, 0x4

    .line 99
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-virtual {v2, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 102
    invoke-virtual {v2, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 105
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->a(Lcom/swedbank/mobile/app/c/a/b/e;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->b(Lcom/swedbank/mobile/app/c/a/b/e;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 111
    :cond_0
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_login_method_login_btn:I

    const/4 v2, 0x1

    .line 113
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->c(Lcom/swedbank/mobile/app/c/a/b/e;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v3

    .line 114
    move-object v4, v3

    check-cast v4, Landroid/view/View;

    .line 115
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    invoke-virtual {v3, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 118
    invoke-virtual {v3, v2}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 121
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->a(Lcom/swedbank/mobile/app/c/a/b/e;)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->b(Lcom/swedbank/mobile/app/c/a/b/e;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 44
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/b/h;->b()Lcom/swedbank/mobile/business/biometric/login/a;

    move-result-object p1

    .line 126
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/login/a$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/login/a$c;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 129
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_disabled_title:I

    .line 135
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->c(Lcom/swedbank/mobile/app/c/a/b/e;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    .line 136
    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 137
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 139
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 140
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b/e;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/app/c/a/b/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/b/e;->a(Lcom/swedbank/mobile/app/c/a/b/h;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/b/e;->g()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 90
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/e;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 3

    .line 86
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 87
    new-instance v1, Lcom/swedbank/mobile/app/c/a/b/e$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/c/a/b/e$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/c/a/b/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/c/a/b/f;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 88
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/a/b/e;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    return-void
.end method
