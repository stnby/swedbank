.class public final Lcom/swedbank/mobile/app/c/a/a/f;
.super Lcom/swedbank/mobile/app/c/a/d$a;
.source "ExternalBiometricAuthenticationPromptInformation.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/util/l;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "customerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "controlCode"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/d$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/f;->a:Lcom/swedbank/mobile/business/util/l;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/a/f;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/a/a/f;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/f;->b:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/f;->a:Lcom/swedbank/mobile/business/util/l;

    .line 25
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_0

    .line 26
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    .line 23
    check-cast p1, Ljava/lang/String;

    .line 17
    invoke-static {p1}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 27
    :goto_0
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    .line 18
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1

    .line 17
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->external_biometric_authentication_control_code:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/a/f;->c:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method
