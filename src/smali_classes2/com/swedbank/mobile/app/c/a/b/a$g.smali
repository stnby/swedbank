.class final Lcom/swedbank/mobile/app/c/a/b/a$g;
.super Ljava/lang/Object;
.source "BiometricPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/b/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/c/a/b/a$g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/c/a/b/a$g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/c/a/b/a$g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/c/a/b/a$g;->a:Lcom/swedbank/mobile/app/c/a/b/a$g;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/v;)Lcom/swedbank/mobile/app/c/a/b/h$a$c;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/swedbank/mobile/app/c/a/b/h$a$c;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/c/a/b/h$a$c;-><init>(Z)V

    goto :goto_1

    .line 36
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$e;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 37
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$c;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 38
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_0
    new-instance p1, Lcom/swedbank/mobile/app/c/a/b/h$a$c;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/c/a/b/h$a$c;-><init>(Z)V

    :goto_1
    return-object p1

    .line 39
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No ViewState for LoginState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/business/authentication/v;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/b/a$g;->a(Lcom/swedbank/mobile/business/authentication/v;)Lcom/swedbank/mobile/app/c/a/b/h$a$c;

    move-result-object p1

    return-object p1
.end method
