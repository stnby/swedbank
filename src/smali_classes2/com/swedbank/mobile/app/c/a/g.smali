.class public final Lcom/swedbank/mobile/app/c/a/g;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/biometric/authentication/h;

.field private b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/app/c/a/d;

.field private d:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/a/d/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/d/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/d/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/g;->e:Lcom/swedbank/mobile/a/d/a/a$a;

    .line 18
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/g;->b:Lcom/swedbank/mobile/business/util/l;

    .line 20
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/g;->d:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/swedbank/mobile/app/c/a/g;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/c/a/g;

    .line 40
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/c/a/g;->d:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/app/c/a/g;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/c/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "information"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/c/a/g;

    .line 27
    iput-object p1, v0, Lcom/swedbank/mobile/app/c/a/g;->c:Lcom/swedbank/mobile/app/c/a/d;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/app/c/a/g;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/c/a/g;

    .line 23
    iput-object p1, v0, Lcom/swedbank/mobile/app/c/a/g;->a:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/c/a/g;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "keyBaseName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/c/a/g;

    .line 36
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/c/a/g;->b:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/g;->e:Lcom/swedbank/mobile/a/d/a/a$a;

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/g;->a:Lcom/swedbank/mobile/business/biometric/authentication/h;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/d/a/a$a;->b(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/a/d/a/a$a;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/g;->c:Lcom/swedbank/mobile/app/c/a/d;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/d/a/a$a;->b(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/a/d/a/a$a;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/g;->b:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/d/a/a$a;->d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/g;->d:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/d/a/a$a;->c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/d/a/a$a;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Lcom/swedbank/mobile/a/d/a/a$a;->a()Lcom/swedbank/mobile/a/d/a/a;

    move-result-object v0

    .line 53
    invoke-interface {v0}, Lcom/swedbank/mobile/a/d/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build biometric auth node without knowing how to render it"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 44
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build biometric auth node without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
