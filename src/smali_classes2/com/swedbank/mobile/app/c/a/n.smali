.class public final Lcom/swedbank/mobile/app/c/a/n;
.super Lcom/swedbank/mobile/architect/a/h;
.source "BiometricAuthenticationPromptRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/i;


# instance fields
.field private final e:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p2, p4, p3}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/n;->e:Lcom/swedbank/mobile/architect/business/a/c;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/biometric/login/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/n;->e:Lcom/swedbank/mobile/architect/business/a/c;

    .line 24
    invoke-static {v0, p1}, Lcom/swedbank/mobile/app/c/a;->a(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/biometric/login/g;)V

    return-void
.end method
