.class final Lcom/swedbank/mobile/app/c/a/a/c$c;
.super Ljava/lang/Object;
.source "ExternalBiometricAuthenticationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/c/a/a/c$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/c$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/c/a/a/c$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/c/a/a/c$c;->a:Lcom/swedbank/mobile/app/c/a/a/c$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/authentication/external/i;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/external/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/authentication/external/i;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/c/a/a/n$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$a;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/i$a;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/c/a/a/n$a$f;->a:Lcom/swedbank/mobile/app/c/a/a/n$a$f;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(PartialState.Success)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/biometric/authentication/external/i$b;->a:Lcom/swedbank/mobile/business/biometric/authentication/external/i$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 39
    new-instance p1, Lcom/swedbank/mobile/app/c/a/a/n$a$a;

    invoke-direct {p1, v2}, Lcom/swedbank/mobile/app/c/a/a/n$a$a;-><init>(Z)V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/n$a$a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/c/a/a/n$a$a;-><init>(Z)V

    .line 38
    invoke-static {p1, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(\n       \u2026ialState.Canceled(false))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;

    if-eqz v0, :cond_2

    .line 42
    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/n$a$c;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/c/a/a/n$a$c;-><init>(ZLcom/swedbank/mobile/business/util/e;)V

    .line 41
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(\n       \u2026lse, reason = it.reason))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    if-eqz v0, :cond_3

    .line 44
    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/n$a$c;

    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/i$d;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Lcom/swedbank/mobile/app/c/a/a/n$a$c;-><init>(ZLcom/swedbank/mobile/business/util/e;)V

    .line 45
    sget-object p1, Lcom/swedbank/mobile/app/c/a/a/n$a$b;->a:Lcom/swedbank/mobile/app/c/a/a/n$a$b;

    const-wide/16 v1, 0xdac

    .line 104
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    .line 103
    new-instance v2, Lcom/swedbank/mobile/app/c/a/a/c$c$a;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/c/a/a/c$c$a;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 102
    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/a/c$c;->a(Lcom/swedbank/mobile/business/biometric/authentication/external/i;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
