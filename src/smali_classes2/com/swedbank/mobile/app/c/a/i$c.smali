.class final synthetic Lcom/swedbank/mobile/app/c/a/i$c;
.super Lkotlin/e/b/i;
.source "BiometricAuthenticationPromptPresenter.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/biometric/a;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/d;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/business/biometric/authentication/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/biometric/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/i$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/authentication/d;

    .line 28
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/biometric/authentication/d;->a(Lcom/swedbank/mobile/business/biometric/a;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/business/biometric/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/i$c;->a(Lcom/swedbank/mobile/business/biometric/a;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "authEventReceived"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "authEventReceived(Lcom/swedbank/mobile/business/biometric/BiometricAuthEvent;)V"

    return-object v0
.end method
