.class public final Lcom/swedbank/mobile/app/c/a/a/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ExternalBiometricAuthenticationViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/a/a/i;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "titleView"

    const-string v4, "getTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "messageView"

    const-string v4, "getMessageView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "controlCodeView"

    const-string v4, "getControlCodeView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "actionBtn"

    const-string v4, "getActionBtn()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/c/a/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/c/a/a/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_external_biometric_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggedInCustomerName"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->h:Lio/reactivex/o;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/a/j;->i:Lcom/swedbank/mobile/business/util/l;

    .line 38
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->external_biometric_authentication_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->c:Lkotlin/f/c;

    .line 39
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->external_biometric_authentication_message:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->d:Lkotlin/f/c;

    .line 40
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->external_biometric_authentication_control_code:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->e:Lkotlin/f/c;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->external_biometric_authentication_action_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->f:Lkotlin/f/c;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/c/c$b;->external_biometric_authentication_cancel_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->g:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/widget/TextView;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->c()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/content/Context;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/widget/TextView;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->f()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/widget/TextView;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/c/a/a/j;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->h()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final h()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    return-object v0
.end method

.method private final i()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->h()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 120
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/c/a/a/n;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/c/a/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->a()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->b()Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->c(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/widget/TextView;

    move-result-object v2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->d(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/widget/TextView;

    move-result-object v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->d()Z

    move-result v0

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->g()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 62
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->c()Z

    move-result v4

    .line 65
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->g()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->e()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    const/4 v2, 0x1

    .line 125
    :cond_2
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->e(Lcom/swedbank/mobile/app/c/a/a/j;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object v3

    .line 126
    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->setEnabled(Z)V

    if-eqz v4, :cond_3

    .line 127
    invoke-virtual {v3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a()V

    :cond_3
    if-eqz v0, :cond_4

    .line 129
    invoke-virtual {v3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b()V

    goto :goto_1

    :cond_4
    if-eqz v2, :cond_5

    .line 130
    invoke-virtual {v3}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->c()V

    .line 67
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->n()Landroid/content/Context;

    move-result-object v0

    .line 69
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    if-eqz v1, :cond_6

    goto :goto_2

    .line 68
    :cond_6
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/a/n;->g()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_a

    if-eqz v1, :cond_9

    .line 135
    instance-of p1, v1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz p1, :cond_7

    check-cast v1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/biometric/login/a;

    .line 136
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->b(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/content/Context;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/swedbank/mobile/app/c/a/a;->a(Lcom/swedbank/mobile/business/biometric/login/a;Landroid/content/Context;)Lcom/swedbank/mobile/app/w/h;

    move-result-object p1

    goto/16 :goto_4

    .line 137
    :cond_7
    instance-of p1, v1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz p1, :cond_8

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/e/l;

    const/4 v2, 0x0

    .line 138
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->b(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/content/Context;

    move-result-object v1

    .line 139
    sget-object v3, Lcom/swedbank/mobile/app/c/a/a/k;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/e/l;->ordinal()I

    move-result p1

    aget p1, v3, p1

    packed-switch p1, :pswitch_data_0

    .line 156
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_debuggable:I

    goto :goto_3

    .line 155
    :pswitch_1
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_stacktrace:I

    goto :goto_3

    .line 154
    :pswitch_2
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_connection:I

    goto :goto_3

    .line 153
    :pswitch_3
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_device_files:I

    goto :goto_3

    .line 152
    :pswitch_4
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_strings:I

    goto :goto_3

    .line 151
    :pswitch_5
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_proc:I

    goto :goto_3

    .line 150
    :pswitch_6
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_memory:I

    goto :goto_3

    .line 149
    :pswitch_7
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_alcatel:I

    goto :goto_3

    .line 148
    :pswitch_8
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_test_keys:I

    goto :goto_3

    .line 147
    :pswitch_9
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_props:I

    goto :goto_3

    .line 146
    :pswitch_a
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_su:I

    goto :goto_3

    .line 145
    :pswitch_b
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_mgm_apps:I

    goto :goto_3

    .line 144
    :pswitch_c
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_apps:I

    goto :goto_3

    .line 143
    :pswitch_d
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_cloak_apps:I

    goto :goto_3

    .line 142
    :pswitch_e
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_bin:I

    goto :goto_3

    .line 141
    :pswitch_f
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_bbox:I

    goto :goto_3

    .line 140
    :pswitch_10
    sget p1, Lcom/swedbank/mobile/core/a$f;->root_reason_general:I

    .line 138
    :goto_3
    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    new-instance p1, Lcom/swedbank/mobile/app/w/g;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    .line 158
    :goto_4
    check-cast p1, Lcom/swedbank/mobile/app/w/h;

    goto :goto_5

    .line 138
    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_9
    const/4 p1, 0x0

    :goto_5
    if-eqz p1, :cond_a

    goto :goto_6

    .line 70
    :cond_a
    new-instance p1, Lcom/swedbank/mobile/app/w/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    check-cast p1, Lcom/swedbank/mobile/app/w/h;

    .line 66
    :goto_6
    invoke-static {p0, v0, p1}, Lcom/swedbank/mobile/app/w/d;->a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/app/c/a/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/a/j;->a(Lcom/swedbank/mobile/app/c/a/a/n;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->h:Lio/reactivex/o;

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->i()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 121
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 53
    invoke-virtual {v0, v2}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeWith(cancelBtn.clicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 9

    .line 110
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 111
    new-instance v1, Lcom/swedbank/mobile/app/c/a/a/j$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/c/a/a/j$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/c/a/a/l;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/c/a/a/l;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 112
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/a/a/j;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/j;->i:Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 113
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->a(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    .line 114
    invoke-static {v1, v2}, Landroidx/core/widget/i;->b(Landroid/widget/TextView;I)V

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 116
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/a/j;->b(Lcom/swedbank/mobile/app/c/a/a/j;)Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/swedbank/mobile/app/c/c$e;->recurring_login_welcome:I

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x2

    .line 117
    invoke-static {v0, v7, v3, v8, v3}, Lkotlin/j/n;->a(Ljava/lang/String;CLjava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 115
    :cond_0
    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
