.class final Lcom/swedbank/mobile/app/c/a/a/c$b;
.super Ljava/lang/Object;
.source "ExternalBiometricAuthenticationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/c/a/a/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/c$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/c/a/a/c$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/c/a/a/c$b;->a:Lcom/swedbank/mobile/app/c/a/a/c$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/authentication/external/g;)Lcom/swedbank/mobile/app/c/a/a/n$a$e;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/external/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/n$a$e;

    .line 23
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/g;->b()Ljava/lang/String;

    move-result-object v1

    .line 24
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/authentication/external/g;->c()Ljava/lang/String;

    move-result-object p1

    .line 22
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/c/a/a/n$a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/biometric/authentication/external/g;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/a/c$b;->a(Lcom/swedbank/mobile/business/biometric/authentication/external/g;)Lcom/swedbank/mobile/app/c/a/a/n$a$e;

    move-result-object p1

    return-object p1
.end method
