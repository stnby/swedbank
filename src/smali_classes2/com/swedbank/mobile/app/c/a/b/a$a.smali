.class final synthetic Lcom/swedbank/mobile/app/c/a/b/a$a;
.super Lkotlin/e/b/i;
.source "BiometricPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/b/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/c/a/b/h;",
        "Lcom/swedbank/mobile/app/c/a/b/h$a;",
        "Lcom/swedbank/mobile/app/c/a/b/h;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/c/a/b/a;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/c/a/b/h;Lcom/swedbank/mobile/app/c/a/b/h$a;)Lcom/swedbank/mobile/app/c/a/b/h;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/app/c/a/b/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/a/b/h$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/a$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/c/a/b/a;

    .line 82
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/b/h$a$c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 83
    check-cast p2, Lcom/swedbank/mobile/app/c/a/b/h$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/b/h$a$c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/b/h$a$c;->a()Z

    move-result p2

    .line 83
    invoke-virtual {p1, p2, v2}, Lcom/swedbank/mobile/app/c/a/b/h;->a(ZLcom/swedbank/mobile/business/biometric/login/a;)Lcom/swedbank/mobile/app/c/a/b/h;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    .line 86
    invoke-static {p1, v1, v2, p2, v2}, Lcom/swedbank/mobile/app/c/a/b/h;->a(Lcom/swedbank/mobile/app/c/a/b/h;ZLcom/swedbank/mobile/business/biometric/login/a;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/b/h;

    move-result-object p1

    goto :goto_0

    .line 89
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/b/h$a$b;

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    .line 90
    check-cast p2, Lcom/swedbank/mobile/app/c/a/b/h$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a()Lcom/swedbank/mobile/business/biometric/login/a;

    move-result-object p2

    .line 89
    invoke-static {p1, v1, p2, v3, v2}, Lcom/swedbank/mobile/app/c/a/b/h;->a(Lcom/swedbank/mobile/app/c/a/b/h;ZLcom/swedbank/mobile/business/biometric/login/a;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/b/h;

    move-result-object p1

    goto :goto_0

    .line 91
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/app/c/a/b/h$a$a;->a:Lcom/swedbank/mobile/app/c/a/b/h$a$a;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-static {p1, v1, v2, v3, v2}, Lcom/swedbank/mobile/app/c/a/b/h;->a(Lcom/swedbank/mobile/app/c/a/b/h;ZLcom/swedbank/mobile/business/biometric/login/a;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/b/h;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/swedbank/mobile/app/c/a/b/h;

    check-cast p2, Lcom/swedbank/mobile/app/c/a/b/h$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/c/a/b/a$a;->a(Lcom/swedbank/mobile/app/c/a/b/h;Lcom/swedbank/mobile/app/c/a/b/h$a;)Lcom/swedbank/mobile/app/c/a/b/h;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/c/a/b/a;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/biometric/authentication/login/BiometricViewState;Lcom/swedbank/mobile/app/biometric/authentication/login/BiometricViewState$PartialState;)Lcom/swedbank/mobile/app/biometric/authentication/login/BiometricViewState;"

    return-object v0
.end method
