.class final synthetic Lcom/swedbank/mobile/app/c/a/a/c$a;
.super Lkotlin/e/b/i;
.source "ExternalBiometricAuthenticationPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/c/a/a/n;",
        "Lcom/swedbank/mobile/app/c/a/a/n$a;",
        "Lcom/swedbank/mobile/app/c/a/a/n;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/c/a/a/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/c/a/a/n;Lcom/swedbank/mobile/app/c/a/a/n$a;)Lcom/swedbank/mobile/app/c/a/a/n;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/app/c/a/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/a/a/n$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/c/a/a/c;

    .line 99
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/a/n$a$e;

    if-eqz v0, :cond_0

    .line 100
    check-cast p2, Lcom/swedbank/mobile/app/c/a/a/n$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$e;->a()Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$e;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7c

    const/4 v9, 0x0

    move-object v0, p1

    .line 99
    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto/16 :goto_0

    .line 102
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/a/n$a$a;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 103
    check-cast p2, Lcom/swedbank/mobile/app/c/a/a/n$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$a;->a()Z

    move-result v6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x6b

    const/4 v10, 0x0

    move-object v1, p1

    .line 102
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto/16 :goto_0

    .line 105
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/a/n$a$d;

    if-eqz v0, :cond_3

    .line 106
    check-cast p2, Lcom/swedbank/mobile/app/c/a/a/n$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$d;->a()Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7b

    const/4 v9, 0x0

    move-object v0, p1

    .line 112
    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto :goto_0

    .line 115
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/c/a/a/n$a$f;->a:Lcom/swedbank/mobile/app/c/a/a/n$a$f;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x73

    const/4 v10, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto :goto_0

    .line 118
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/a/n$a$c;

    if-eqz v0, :cond_6

    .line 119
    check-cast p2, Lcom/swedbank/mobile/app/c/a/a/n$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 120
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->b()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x5b

    const/4 v10, 0x0

    move-object v1, p1

    .line 119
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->b()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v0, p1

    .line 122
    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    goto :goto_0

    .line 126
    :cond_6
    sget-object v0, Lcom/swedbank/mobile/app/c/a/a/n$a$b;->a:Lcom/swedbank/mobile/app/c/a/a/n$a$b;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/c/a/a/n;->a(Lcom/swedbank/mobile/app/c/a/a/n;Ljava/lang/String;Ljava/lang/String;ZZZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/app/c/a/a/n;

    check-cast p2, Lcom/swedbank/mobile/app/c/a/a/n$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/c/a/a/c$a;->a(Lcom/swedbank/mobile/app/c/a/a/n;Lcom/swedbank/mobile/app/c/a/a/n$a;)Lcom/swedbank/mobile/app/c/a/a/n;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/c/a/a/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/biometric/authentication/external/ExternalBiometricAuthenticationViewState;Lcom/swedbank/mobile/app/biometric/authentication/external/ExternalBiometricAuthenticationViewState$PartialState;)Lcom/swedbank/mobile/app/biometric/authentication/external/ExternalBiometricAuthenticationViewState;"

    return-object v0
.end method
