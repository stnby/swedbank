.class public final Lcom/swedbank/mobile/app/c/a/b/a;
.super Lcom/swedbank/mobile/architect/a/d;
.source "BiometricPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/c/a/b/d;",
        "Lcom/swedbank/mobile/app/c/a/b/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/b/a;)Lcom/swedbank/mobile/business/authentication/login/h;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 20
    sget-object v0, Lcom/swedbank/mobile/app/c/a/b/a$b;->a:Lcom/swedbank/mobile/app/c/a/b/a$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/a/b/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 21
    new-instance v1, Lcom/swedbank/mobile/app/c/a/b/a$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/c/a/b/a$c;-><init>(Lcom/swedbank/mobile/app/c/a/b/a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 25
    sget-object v1, Lcom/swedbank/mobile/app/c/a/b/a$h;->a:Lcom/swedbank/mobile/app/c/a/b/a$h;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/c/a/b/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 26
    new-instance v2, Lcom/swedbank/mobile/app/c/a/b/a$i;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/c/a/b/a$i;-><init>(Lcom/swedbank/mobile/app/c/a/b/a;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 30
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    .line 31
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/login/h;->b()Lio/reactivex/o;

    move-result-object v2

    .line 32
    sget-object v3, Lcom/swedbank/mobile/app/c/a/b/a$f;->a:Lcom/swedbank/mobile/app/c/a/b/a$f;

    check-cast v3, Lio/reactivex/c/k;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    .line 33
    sget-object v3, Lcom/swedbank/mobile/app/c/a/b/a$g;->a:Lcom/swedbank/mobile/app/c/a/b/a$g;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 43
    iget-object v3, p0, Lcom/swedbank/mobile/app/c/a/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    .line 44
    invoke-interface {v3}, Lcom/swedbank/mobile/business/authentication/login/h;->h_()Lio/reactivex/o;

    move-result-object v3

    .line 46
    sget-object v4, Lcom/swedbank/mobile/app/c/a/b/a$d;->a:Lcom/swedbank/mobile/app/c/a/b/a$d;

    check-cast v4, Lio/reactivex/c/k;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v3

    .line 47
    sget-object v4, Lcom/swedbank/mobile/app/c/a/b/a$e;->a:Lcom/swedbank/mobile/app/c/a/b/a$e;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 55
    check-cast v2, Lio/reactivex/s;

    .line 56
    check-cast v3, Lio/reactivex/s;

    .line 57
    check-cast v1, Lio/reactivex/s;

    .line 58
    check-cast v0, Lio/reactivex/s;

    .line 54
    invoke-static {v2, v3, v1, v0}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026am,\n        cancelStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v1, Lcom/swedbank/mobile/app/c/a/b/h;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-direct {v1, v3, v2, v4, v2}, Lcom/swedbank/mobile/app/c/a/b/h;-><init>(ZLcom/swedbank/mobile/business/biometric/login/a;ILkotlin/e/b/g;)V

    .line 61
    new-instance v2, Lcom/swedbank/mobile/app/c/a/b/a$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/c/a/b/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/c/a/b/a$a;-><init>(Lcom/swedbank/mobile/app/c/a/b/a;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 81
    new-instance v3, Lcom/swedbank/mobile/app/c/a/b/b;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/c/a/b/b;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 82
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
