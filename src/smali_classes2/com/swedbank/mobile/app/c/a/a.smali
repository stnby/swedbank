.class public final Lcom/swedbank/mobile/app/c/a/a;
.super Ljava/lang/Object;
.source "BiometricAuthRendering.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/biometric/login/a;Landroid/content/Context;)Lcom/swedbank/mobile/app/w/h;
    .locals 12
    .param p0    # Lcom/swedbank/mobile/business/biometric/login/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget-object v0, Lcom/swedbank/mobile/business/biometric/login/a$b;->b:Lcom/swedbank/mobile/business/biometric/login/a$b;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p0, Lcom/swedbank/mobile/app/w/g;

    const/4 v2, 0x0

    .line 73
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_login_error_device_problem:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p0

    .line 72
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    check-cast p0, Lcom/swedbank/mobile/app/w/h;

    goto/16 :goto_2

    .line 75
    :cond_0
    instance-of v0, p0, Lcom/swedbank/mobile/business/biometric/login/a$c;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/swedbank/mobile/app/w/g;

    const/4 v2, 0x0

    .line 77
    check-cast p0, Lcom/swedbank/mobile/business/biometric/login/a$c;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/a$c;->a()Z

    move-result p0

    if-eqz p0, :cond_1

    sget p0, Lcom/swedbank/mobile/app/c/c$e;->biometric_login_error_permanent_lockout:I

    goto :goto_0

    .line 78
    :cond_1
    sget p0, Lcom/swedbank/mobile/app/c/c$e;->biometric_login_error_lockout:I

    .line 76
    :goto_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, v0

    .line 75
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    move-object p0, v0

    check-cast p0, Lcom/swedbank/mobile/app/w/h;

    goto/16 :goto_2

    .line 81
    :cond_2
    instance-of v0, p0, Lcom/swedbank/mobile/business/biometric/login/a$d;

    if-eqz v0, :cond_6

    check-cast p0, Lcom/swedbank/mobile/business/biometric/login/a$d;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/biometric/login/a$d;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p0

    .line 226
    instance-of v0, p0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    .line 85
    move-object v0, p0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 86
    invoke-static {p0}, Lcom/swedbank/mobile/app/w/d;->a(Ljava/util/List;)Lkotlin/k;

    move-result-object p0

    .line 87
    invoke-virtual {p0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p0

    move-object v1, p0

    check-cast v1, Ljava/lang/String;

    .line 88
    new-instance p0, Lcom/swedbank/mobile/app/w/g;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    goto :goto_1

    .line 93
    :cond_3
    new-instance p0, Lcom/swedbank/mobile/app/w/g;

    const/4 v7, 0x0

    .line 94
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_login_error_remote:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x5

    const/4 v11, 0x0

    move-object v6, p0

    .line 93
    invoke-direct/range {v6 .. v11}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    goto :goto_1

    .line 227
    :cond_4
    instance-of p1, p0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz p1, :cond_5

    check-cast p0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Throwable;

    .line 82
    new-instance p1, Lcom/swedbank/mobile/app/w/g;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    move-object p0, p1

    .line 228
    :goto_1
    check-cast p0, Lcom/swedbank/mobile/app/w/h;

    goto :goto_2

    .line 82
    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_6
    if-nez p0, :cond_7

    .line 99
    new-instance p0, Lcom/swedbank/mobile/app/w/g;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/w/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    check-cast p0, Lcom/swedbank/mobile/app/w/h;

    :goto_2
    return-object p0

    :cond_7
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final a(Landroid/content/Context;Lcom/swedbank/mobile/business/biometric/a;Lkotlin/k;)V
    .locals 5
    .param p0    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/swedbank/mobile/business/biometric/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/swedbank/mobile/business/biometric/a;",
            "Lkotlin/k<",
            "Lcom/mattprecious/swirl/SwirlView;",
            "+",
            "Landroid/widget/TextView;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$renderBiometricAuthEvent"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instructionViews"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x11

    const/4 v1, 0x1

    if-nez p1, :cond_1

    .line 28
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_auth_default_instruction_text:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "getString(R.string.biome\u2026default_instruction_text)"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    .line 101
    invoke-virtual {p2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mattprecious/swirl/SwirlView;

    invoke-virtual {p2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 v4, 0x0

    .line 103
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 106
    sget v4, Lcom/swedbank/mobile/app/c/c$a;->label_red:I

    .line 108
    invoke-static {p0, v4}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p0

    .line 109
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 110
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p0

    .line 112
    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 114
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    invoke-virtual {v2, v4, p0, p1, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 117
    new-instance p0, Landroid/text/SpannedString;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {p0, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, p0

    check-cast p1, Ljava/lang/CharSequence;

    .line 102
    :cond_0
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    sget-object p0, Lcom/mattprecious/swirl/SwirlView$a;->b:Lcom/mattprecious/swirl/SwirlView$a;

    invoke-virtual {v3, p0, v1}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Z)V

    goto/16 :goto_3

    .line 31
    :cond_1
    instance-of v2, p1, Lcom/swedbank/mobile/business/biometric/a$b;

    if-eqz v2, :cond_2

    .line 32
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_auth_fingerprint_recognized:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string p1, "getString(R.string.biome\u2026h_fingerprint_recognized)"

    invoke-static {p0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/CharSequence;

    .line 127
    invoke-virtual {p2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/mattprecious/swirl/SwirlView;

    invoke-virtual {p2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 128
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    sget-object p0, Lcom/mattprecious/swirl/SwirlView$a;->b:Lcom/mattprecious/swirl/SwirlView$a;

    invoke-virtual {p1, p0, v1}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Z)V

    goto/16 :goto_3

    .line 35
    :cond_2
    instance-of v2, p1, Lcom/swedbank/mobile/business/biometric/a$c;

    if-eqz v2, :cond_7

    .line 36
    check-cast p1, Lcom/swedbank/mobile/business/biometric/a$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/a$c;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 155
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_0

    .line 156
    :cond_3
    instance-of v2, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_6

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    .line 153
    check-cast p1, Lkotlin/k;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 37
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 157
    :goto_0
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    .line 159
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 39
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_auth_fingerprint_not_recognized:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 160
    :cond_4
    instance-of v2, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_5

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 162
    :goto_1
    check-cast p1, Ljava/lang/CharSequence;

    .line 163
    invoke-virtual {p2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mattprecious/swirl/SwirlView;

    invoke-virtual {p2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 166
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 168
    sget v4, Lcom/swedbank/mobile/app/c/c$a;->label_red:I

    .line 170
    invoke-static {p0, v4}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p0

    .line 171
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 172
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p0

    .line 174
    invoke-virtual {v3, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 176
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    invoke-virtual {v3, v4, p0, p1, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 179
    new-instance p0, Landroid/text/SpannedString;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast p0, Ljava/lang/CharSequence;

    .line 164
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    sget-object p0, Lcom/mattprecious/swirl/SwirlView$a;->c:Lcom/mattprecious/swirl/SwirlView$a;

    invoke-virtual {v2, p0, v1}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Z)V

    goto :goto_3

    .line 161
    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 37
    :cond_6
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 43
    :cond_7
    instance-of v2, p1, Lcom/swedbank/mobile/business/biometric/a$a;

    if-eqz v2, :cond_9

    .line 44
    check-cast p1, Lcom/swedbank/mobile/business/biometric/a$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/biometric/a$a;->b()Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_8

    goto :goto_2

    :cond_8
    sget p1, Lcom/swedbank/mobile/app/c/c$e;->biometric_auth_generic_error:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "getString(R.string.biometric_auth_generic_error)"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 189
    :goto_2
    invoke-virtual {p2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mattprecious/swirl/SwirlView;

    invoke-virtual {p2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 192
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 194
    sget v4, Lcom/swedbank/mobile/app/c/c$a;->label_red:I

    .line 196
    invoke-static {p0, v4}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p0

    .line 197
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 198
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p0

    .line 200
    invoke-virtual {v3, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 202
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    invoke-virtual {v3, v4, p0, p1, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 205
    new-instance p0, Landroid/text/SpannedString;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast p0, Ljava/lang/CharSequence;

    .line 190
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    sget-object p0, Lcom/mattprecious/swirl/SwirlView$a;->c:Lcom/mattprecious/swirl/SwirlView$a;

    invoke-virtual {v2, p0, v1}, Lcom/mattprecious/swirl/SwirlView;->a(Lcom/mattprecious/swirl/SwirlView$a;Z)V

    :goto_3
    return-void

    .line 214
    :cond_9
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
