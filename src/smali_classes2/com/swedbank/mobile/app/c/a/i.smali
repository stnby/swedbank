.class public final Lcom/swedbank/mobile/app/c/a/i;
.super Lcom/swedbank/mobile/architect/a/d;
.source "BiometricAuthenticationPromptPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/c/a/p;",
        "Lcom/swedbank/mobile/app/c/a/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/authentication/d;

.field private final b:Lcom/swedbank/mobile/app/c/a/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/authentication/d;Lcom/swedbank/mobile/app/c/a/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/authentication/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "biometric_authentication_prompt_information"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "information"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/i;->a:Lcom/swedbank/mobile/business/biometric/authentication/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/i;->b:Lcom/swedbank/mobile/app/c/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/i;)Lcom/swedbank/mobile/business/biometric/authentication/d;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/i;->a:Lcom/swedbank/mobile/business/biometric/authentication/d;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/c/a/i$f;->a:Lcom/swedbank/mobile/app/c/a/i$f;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/a/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/swedbank/mobile/app/c/a/i$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/c/a/i$g;-><init>(Lcom/swedbank/mobile/app/c/a/i;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/c/a/i$b;->a:Lcom/swedbank/mobile/app/c/a/i$b;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/c/a/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    new-instance v2, Lcom/swedbank/mobile/app/c/a/i$c;

    iget-object v3, p0, Lcom/swedbank/mobile/app/c/a/i;->a:Lcom/swedbank/mobile/business/biometric/authentication/d;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/c/a/i$c;-><init>(Lcom/swedbank/mobile/business/biometric/authentication/d;)V

    check-cast v2, Lkotlin/e/a/b;

    new-instance v3, Lcom/swedbank/mobile/app/c/a/k;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/c/a/k;-><init>(Lkotlin/e/a/b;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 29
    sget-object v2, Lcom/swedbank/mobile/app/c/a/i$d;->a:Lcom/swedbank/mobile/app/c/a/i$d;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/i;->a:Lcom/swedbank/mobile/business/biometric/authentication/d;

    .line 41
    invoke-interface {v2}, Lcom/swedbank/mobile/business/biometric/authentication/d;->a()Lio/reactivex/j;

    move-result-object v2

    .line 42
    sget-object v3, Lcom/swedbank/mobile/app/c/a/i$h;->a:Lcom/swedbank/mobile/app/c/a/i$h;

    check-cast v3, Lkotlin/e/a/b;

    if-eqz v3, :cond_0

    new-instance v4, Lcom/swedbank/mobile/app/c/a/l;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/c/a/l;-><init>(Lkotlin/e/a/b;)V

    move-object v3, v4

    :cond_0
    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object v2

    .line 45
    iget-object v3, p0, Lcom/swedbank/mobile/app/c/a/i;->a:Lcom/swedbank/mobile/business/biometric/authentication/d;

    .line 46
    invoke-interface {v3}, Lcom/swedbank/mobile/business/biometric/authentication/d;->b()Lio/reactivex/o;

    move-result-object v3

    .line 47
    sget-object v4, Lcom/swedbank/mobile/app/c/a/i$e;->a:Lcom/swedbank/mobile/app/c/a/i$e;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_1

    new-instance v5, Lcom/swedbank/mobile/app/c/a/l;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/c/a/l;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_1
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 50
    check-cast v0, Lio/reactivex/s;

    .line 51
    check-cast v1, Lio/reactivex/s;

    .line 52
    check-cast v2, Lio/reactivex/s;

    .line 53
    check-cast v3, Lio/reactivex/s;

    .line 49
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 54
    new-instance v9, Lcom/swedbank/mobile/app/c/a/q;

    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/i;->b:Lcom/swedbank/mobile/app/c/a/d;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/app/c/a/q;-><init>(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    new-instance v1, Lcom/swedbank/mobile/app/c/a/i$a;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/app/c/a/i;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/c/a/i$a;-><init>(Lcom/swedbank/mobile/app/c/a/i;)V

    check-cast v1, Lkotlin/e/a/m;

    new-instance v2, Lcom/swedbank/mobile/app/c/a/j;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/c/a/j;-><init>(Lkotlin/e/a/m;)V

    check-cast v2, Lio/reactivex/c/c;

    invoke-virtual {v0, v9, v2}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026), this::viewStateReduce)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
