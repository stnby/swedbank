.class public final Lcom/swedbank/mobile/app/c/a/e;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BiometricAuthenticationLegacyPromptViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/a/p;
.implements Lcom/swedbank/mobile/core/ui/i;


# instance fields
.field private a:Landroid/app/Dialog;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private b:Lio/reactivex/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private c:Lkotlin/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/k<",
            "Lcom/mattprecious/swirl/SwirlView;",
            "+",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lio/reactivex/b/c;

.field private final g:Lcom/swedbank/mobile/business/biometric/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->g:Lcom/swedbank/mobile/business/biometric/d;

    .line 28
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "Disposables.disposed()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->b:Lio/reactivex/b/c;

    .line 30
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->d:Lcom/b/c/c;

    .line 31
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<BiometricAuthEvent>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->e:Lcom/b/c/c;

    .line 32
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "Disposables.disposed()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->f:Lio/reactivex/b/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/e;)Landroid/content/Context;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/e;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/e;Lkotlin/k;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->c:Lkotlin/k;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/a/e;)Lcom/b/c/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/e;->d:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/app/Dialog;)V
    .locals 0
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 27
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->a:Landroid/app/Dialog;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/c/a/q;)V
    .locals 12
    .param p1    # Lcom/swedbank/mobile/app/c/a/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/e;->d()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_e

    .line 39
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->a()Lcom/swedbank/mobile/app/c/a/d;

    move-result-object v0

    if-eqz v0, :cond_e

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/app/c/a/e;

    .line 107
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/c/a/e;->d()Landroid/app/Dialog;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_d

    .line 108
    invoke-static {v1}, Lcom/swedbank/mobile/app/c/a/e;->a(Lcom/swedbank/mobile/app/c/a/e;)Landroid/content/Context;

    move-result-object v2

    .line 109
    new-instance v5, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-direct {v5, v2}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 112
    sget v6, Lcom/swedbank/mobile/app/c/c$c;->view_biometric_authentication_prompt:I

    const/4 v7, 0x0

    .line 113
    move-object v8, v7

    check-cast v8, Landroid/view/ViewGroup;

    .line 116
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    const-string v10, "LayoutInflater.from(this)"

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-virtual {v9, v6, v8, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_c

    check-cast v6, Landroid/view/ViewGroup;

    .line 118
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 123
    instance-of v9, v0, Lcom/swedbank/mobile/app/c/a/d$b;

    if-eqz v9, :cond_2

    .line 124
    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/app/c/a/d$b;

    invoke-virtual {v9}, Lcom/swedbank/mobile/app/c/a/d$b;->a()I

    move-result v10

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "resources.getString(information.titleResId)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Ljava/lang/CharSequence;

    .line 125
    invoke-virtual {v9}, Lcom/swedbank/mobile/app/c/a/d$b;->b()Ljava/lang/Integer;

    move-result-object v9

    if-eqz v9, :cond_1

    check-cast v9, Ljava/lang/Number;

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    :cond_1
    move-object v8, v7

    :goto_1
    check-cast v8, Ljava/lang/CharSequence;

    .line 126
    check-cast v7, Ljava/lang/CharSequence;

    goto :goto_2

    .line 128
    :cond_2
    instance-of v7, v0, Lcom/swedbank/mobile/app/c/a/d$a;

    if-eqz v7, :cond_b

    .line 129
    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/app/c/a/d$a;

    const-string v9, "resources"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Lcom/swedbank/mobile/app/c/a/d$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 130
    invoke-virtual {v7, v8}, Lcom/swedbank/mobile/app/c/a/d$a;->c(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 131
    invoke-virtual {v7, v8}, Lcom/swedbank/mobile/app/c/a/d$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v7

    move-object v8, v9

    .line 134
    :goto_2
    move-object v9, v6

    check-cast v9, Landroid/view/View;

    sget v11, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_prompt_title:I

    .line 135
    invoke-virtual {v9, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_3

    check-cast v11, Landroid/widget/TextView;

    .line 136
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :cond_3
    sget v10, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_prompt_identity:I

    .line 140
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    if-eqz v10, :cond_6

    check-cast v10, Landroid/widget/TextView;

    .line 141
    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    check-cast v10, Landroid/view/View;

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    :goto_3
    if-eqz v7, :cond_5

    const/4 v7, 0x0

    goto :goto_4

    :cond_5
    const/16 v7, 0x8

    .line 143
    :goto_4
    invoke-virtual {v10, v7}, Landroid/view/View;->setVisibility(I)V

    .line 147
    :cond_6
    sget v7, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_prompt_secondary_text:I

    .line 148
    invoke-virtual {v9, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_9

    check-cast v7, Landroid/widget/TextView;

    .line 149
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    check-cast v7, Landroid/view/View;

    if-eqz v8, :cond_7

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    :goto_5
    if-eqz v3, :cond_8

    goto :goto_6

    :cond_8
    const/16 v4, 0x8

    .line 151
    :goto_6
    invoke-virtual {v7, v4}, Landroid/view/View;->setVisibility(I)V

    .line 156
    :cond_9
    sget v3, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_prompt_icon:I

    invoke-virtual {v6, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mattprecious/swirl/SwirlView;

    .line 157
    sget v4, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_prompt_instruction_text:I

    invoke-virtual {v6, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 158
    invoke-static {v3, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/swedbank/mobile/app/c/a/e;->a(Lcom/swedbank/mobile/app/c/a/e;Lkotlin/k;)V

    .line 159
    invoke-virtual {v5, v9}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 160
    new-instance v3, Lcom/swedbank/mobile/app/c/a/e$a;

    invoke-direct {v3, v1, v2, v0}, Lcom/swedbank/mobile/app/c/a/e$a;-><init>(Lcom/swedbank/mobile/app/c/a/e;Landroid/content/Context;Lcom/swedbank/mobile/app/c/a/d;)V

    check-cast v3, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v5, v3}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 161
    sget v3, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_prompt_cancel:I

    .line 162
    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_a

    check-cast v3, Landroid/widget/Button;

    .line 163
    check-cast v3, Landroid/view/View;

    .line 164
    new-instance v4, Lcom/swedbank/mobile/app/c/a/e$b;

    invoke-direct {v4, v1, v2, v0}, Lcom/swedbank/mobile/app/c/a/e$b;-><init>(Lcom/swedbank/mobile/app/c/a/e;Landroid/content/Context;Lcom/swedbank/mobile/app/c/a/d;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_a
    check-cast v5, Landroid/app/Dialog;

    .line 110
    invoke-virtual {v1, v5}, Lcom/swedbank/mobile/app/c/a/e;->b(Landroid/app/Dialog;)Landroid/app/Dialog;

    goto :goto_7

    .line 131
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 115
    :cond_c
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 107
    :cond_d
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "New dialog can only be rendered once"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 41
    :cond_e
    :goto_7
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/e;->n()Landroid/content/Context;

    move-result-object v0

    .line 42
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->d()Lcom/swedbank/mobile/business/biometric/a;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/e;->c:Lkotlin/k;

    if-nez v2, :cond_f

    const-string v3, "instructionViews"

    invoke-static {v3}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 41
    :cond_f
    invoke-static {v0, v1, v2}, Lcom/swedbank/mobile/app/c/a/a;->a(Landroid/content/Context;Lcom/swedbank/mobile/business/biometric/a;Lkotlin/k;)V

    .line 44
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->b()Ljava/security/Signature;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->c()Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/e;->f:Lio/reactivex/b/c;

    invoke-interface {v1}, Lio/reactivex/b/c;->b()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/e;->g:Lcom/swedbank/mobile/business/biometric/d;

    .line 47
    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/biometric/d;->a(Ljava/security/Signature;)Lio/reactivex/o;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/e;->e:Lcom/b/c/c;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026ubscribe(authEventStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 174
    iput-object v0, p0, Lcom/swedbank/mobile/app/c/a/e;->f:Lio/reactivex/b/c;

    .line 51
    :cond_10
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->c()Z

    move-result p1

    if-nez p1, :cond_11

    .line 52
    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->f:Lio/reactivex/b/c;

    invoke-interface {p1}, Lio/reactivex/b/c;->a()V

    :cond_11
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/e;->b:Lio/reactivex/b/c;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/app/c/a/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/e;->a(Lcom/swedbank/mobile/app/c/a/q;)V

    return-void
.end method

.method public b(Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 1
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/i$a;->a(Lcom/swedbank/mobile/core/ui/i;Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/e;->d:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/e;->e:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public d()Landroid/app/Dialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/e;->a:Landroid/app/Dialog;

    return-object v0
.end method

.method public f()Lio/reactivex/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/e;->b:Lio/reactivex/b/c;

    return-object v0
.end method
