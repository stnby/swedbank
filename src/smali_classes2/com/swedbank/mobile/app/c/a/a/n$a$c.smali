.class public final Lcom/swedbank/mobile/app/c/a/a/n$a$c;
.super Lcom/swedbank/mobile/app/c/a/a/n$a;
.source "ExternalBiometricAuthenticationViewState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/c/a/a/n$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/e/l;",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p2    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/e/l;",
            "+",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reason"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/c/a/a/n$a;-><init>(Lkotlin/e/b/g;)V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->b:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->a:Z

    return v0
.end method

.method public final b()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/e/l;",
            "Lcom/swedbank/mobile/business/biometric/login/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/n$a$c;->b:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method
