.class public final Lcom/swedbank/mobile/app/c/a/a/g;
.super Lcom/swedbank/mobile/architect/a/h;
.source "ExternalBiometricAuthenticationRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/authentication/external/j;


# instance fields
.field private final e:Lcom/swedbank/mobile/data/device/a;

.field private final f:Lcom/swedbank/mobile/business/e/b;

.field private final g:Lcom/swedbank/mobile/app/c/a/g;

.field private final h:Lcom/swedbank/mobile/business/biometric/authentication/h;

.field private final i:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/app/c/a/g;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/c/a/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation runtime Ljavax/inject/Named;
            value = "for_external_biometric_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_external_biometric_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_external_biometric_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_external_biometric_authentication"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/app/c/a/g;",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggedInCustomerName"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p6, p8, p7}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/g;->e:Lcom/swedbank/mobile/data/device/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/a/g;->f:Lcom/swedbank/mobile/business/e/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/a/a/g;->g:Lcom/swedbank/mobile/app/c/a/g;

    iput-object p4, p0, Lcom/swedbank/mobile/app/c/a/a/g;->h:Lcom/swedbank/mobile/business/biometric/authentication/h;

    iput-object p5, p0, Lcom/swedbank/mobile/app/c/a/a/g;->i:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/app/c/a/g;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->g:Lcom/swedbank/mobile/app/c/a/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/business/util/l;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->i:Lcom/swedbank/mobile/business/util/l;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/business/biometric/authentication/h;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->h:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/data/device/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->e:Lcom/swedbank/mobile/data/device/a;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 73
    sget-object v0, Lcom/swedbank/mobile/app/c/a/a/g$c;->a:Lcom/swedbank/mobile/app/c/a/a/g$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "controlCode"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/g$d;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/c/a/a/g$d;-><init>(Lcom/swedbank/mobile/app/c/a/a/g;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/a/a/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 9

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->f:Lcom/swedbank/mobile/business/e/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->e:Lcom/swedbank/mobile/data/device/a;

    .line 54
    invoke-interface {v0}, Lcom/swedbank/mobile/data/device/a;->b()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 55
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "activityManager\n        \u2026unches()\n        .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 56
    new-instance v0, Lcom/swedbank/mobile/app/c/a/a/g$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/c/a/a/g$a;-><init>(Lcom/swedbank/mobile/app/c/a/a/g;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 75
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->e:Lcom/swedbank/mobile/data/device/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/data/device/a;->c()V

    return-void
.end method

.method public c()V
    .locals 4

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/g;->e:Lcom/swedbank/mobile/data/device/a;

    .line 67
    sget-object v1, Lcom/swedbank/mobile/app/c/a/a/g$b;->a:Lcom/swedbank/mobile/app/c/a/a/g$b;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    return-void
.end method
