.class public final Lcom/swedbank/mobile/app/c/a/b;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BiometricAuthenticationEmbeddedViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/a/p;


# instance fields
.field private final a:I

.field private b:Lkotlin/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/k<",
            "Lcom/mattprecious/swirl/SwirlView;",
            "+",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lio/reactivex/b/c;

.field private final f:Lcom/swedbank/mobile/business/biometric/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/d;Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_authentication_prompt"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/d;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "embeddedViewId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b;->f:Lcom/swedbank/mobile/business/biometric/d;

    const/4 p1, 0x0

    const/4 v0, 0x1

    .line 29
    invoke-static {p2, p1, v0, p1}, Lcom/swedbank/mobile/business/util/m;->a(Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/a;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    iput p1, p0, Lcom/swedbank/mobile/app/c/a/b;->a:I

    .line 32
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b;->c:Lcom/b/c/c;

    .line 33
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<BiometricAuthEvent>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b;->d:Lcom/b/c/c;

    .line 34
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object p1

    const-string p2, "Disposables.disposed()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b;->e:Lio/reactivex/b/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/b;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/swedbank/mobile/app/c/a/b;->a:I

    return p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/b;Lkotlin/k;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b;->b:Lkotlin/k;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/a/b;)Landroid/content/Context;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/b;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/a/b;)Lcom/b/c/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/b;->c:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/c/a/q;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/c/a/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b;->b:Lkotlin/k;

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/b;->n()Landroid/content/Context;

    move-result-object v1

    .line 81
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->d()Lcom/swedbank/mobile/business/biometric/a;

    move-result-object v2

    .line 80
    invoke-static {v1, v2, v0}, Lcom/swedbank/mobile/app/c/a/a;->a(Landroid/content/Context;Lcom/swedbank/mobile/business/biometric/a;Lkotlin/k;)V

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->b()Ljava/security/Signature;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/b;->e:Lio/reactivex/b/c;

    invoke-interface {v1}, Lio/reactivex/b/c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/b;->f:Lcom/swedbank/mobile/business/biometric/d;

    .line 88
    invoke-interface {v1, v0}, Lcom/swedbank/mobile/business/biometric/d;->a(Ljava/security/Signature;)Lio/reactivex/o;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/b;->d:Lcom/b/c/c;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "biometricRepository\n    \u2026ubscribe(authEventStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 162
    iput-object v0, p0, Lcom/swedbank/mobile/app/c/a/b;->e:Lio/reactivex/b/c;

    .line 92
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->c()Z

    move-result p1

    if-nez p1, :cond_2

    .line 93
    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/b;->e:Lio/reactivex/b/c;

    invoke-interface {p1}, Lio/reactivex/b/c;->a()V

    :cond_2
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/app/c/a/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/b;->a(Lcom/swedbank/mobile/app/c/a/q;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b;->c:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b;->d:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method protected e()V
    .locals 6

    .line 99
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/b;->o()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_5

    .line 98
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b;->a(Lcom/swedbank/mobile/app/c/a/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 108
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 109
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "getChildAt(index)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v5, 0x8

    .line 110
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 113
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b;->b(Lcom/swedbank/mobile/app/c/a/b;)Landroid/content/Context;

    move-result-object v1

    .line 114
    sget v3, Lcom/swedbank/mobile/app/c/c$c;->view_biometric_authentication_embedded:I

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    .line 118
    :cond_2
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v4, "LayoutInflater.from(this)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-virtual {v1, v3, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 119
    sget v1, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_embedded_icon:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mattprecious/swirl/SwirlView;

    .line 120
    sget v2, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_embedded_instruction_text:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 121
    invoke-static {v1, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/swedbank/mobile/app/c/a/b;->a(Lcom/swedbank/mobile/app/c/a/b;Lkotlin/k;)V

    goto :goto_1

    .line 117
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_4
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b;->c(Lcom/swedbank/mobile/app/c/a/b;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 99
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Root view missing or not a ViewGroup"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected g_()V
    .locals 6

    .line 126
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/b;->o()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 125
    invoke-static {p0}, Lcom/swedbank/mobile/app/c/a/b;->a(Lcom/swedbank/mobile/app/c/a/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 129
    sget v1, Lcom/swedbank/mobile/app/c/c$b;->biometric_auth_embedded_root_view:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 130
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 132
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    .line 133
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "getChildAt(index)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-void

    .line 126
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Root view missing or not a ViewGroup"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
