.class public final Lcom/swedbank/mobile/app/c/a/q;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/c/a/q$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/app/c/a/d;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/security/Signature;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Z

.field private final d:Lcom/swedbank/mobile/business/biometric/a;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/Throwable;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/app/c/a/q;-><init>(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/app/c/a/d;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/biometric/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    iput-object p5, p0, Lcom/swedbank/mobile/app/c/a/q;->e:Ljava/lang/Throwable;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILkotlin/e/b/g;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 8
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/c/a/d;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 9
    move-object p2, v0

    check-cast p2, Ljava/security/Signature;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    const/4 p3, 0x0

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, p3

    :goto_0
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 11
    move-object p4, v0

    check-cast p4, Lcom/swedbank/mobile/business/biometric/a;

    :cond_3
    move-object v2, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 12
    move-object p5, v0

    check-cast p5, Ljava/lang/Throwable;

    :cond_4
    move-object v0, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v1

    move-object p6, v2

    move-object p7, v0

    invoke-direct/range {p2 .. p7}, Lcom/swedbank/mobile/app/c/a/q;-><init>(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/q;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/swedbank/mobile/app/c/a/q;->e:Ljava/lang/Throwable;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/swedbank/mobile/app/c/a/q;->a(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/c/a/q;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/app/c/a/d;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/c/a/q;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/c/a/d;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/security/Signature;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/biometric/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v6, Lcom/swedbank/mobile/app/c/a/q;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/c/a/q;-><init>(Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;)V

    return-object v6
.end method

.method public final b()Ljava/security/Signature;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    return v0
.end method

.method public final d()Lcom/swedbank/mobile/business/biometric/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/c/a/q;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/c/a/q;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    iget-object v3, p1, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    iget-object v3, p1, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    iget-object v3, p1, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->e:Ljava/lang/Throwable;

    iget-object p1, p1, Lcom/swedbank/mobile/app/c/a/q;->e:Ljava/lang/Throwable;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/q;->e:Ljava/lang/Throwable;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BiometricAuthenticationPromptViewState(information="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->a:Lcom/swedbank/mobile/app/c/a/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->b:Ljava/security/Signature;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", scanBiometrics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/c/a/q;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", lastReceivedAuthEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->d:Lcom/swedbank/mobile/business/biometric/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/q;->e:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
