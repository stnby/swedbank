.class public final Lcom/swedbank/mobile/app/c/a/r;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BiometricAuthenticationSystemPromptViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/c/a/p;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lio/reactivex/b/c;

.field private final d:Landroid/app/Application;

.field private final e:Lcom/swedbank/mobile/business/biometric/d;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/biometric/d;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/r;->d:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/r;->e:Lcom/swedbank/mobile/business/biometric/d;

    .line 22
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/r;->a:Lcom/b/c/c;

    .line 23
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<BiometricAuthEvent>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/r;->b:Lcom/b/c/c;

    .line 24
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object p1

    const-string p2, "Disposables.disposed()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/r;->c:Lio/reactivex/b/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/a/r;)Lcom/b/c/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/a/r;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/c/a/q;)V
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/c/a/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->a()Lcom/swedbank/mobile/app/c/a/d;

    move-result-object v0

    .line 31
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->b()Ljava/security/Signature;

    move-result-object v1

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 32
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/r;->c:Lio/reactivex/b/c;

    invoke-interface {v2}, Lio/reactivex/b/c;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/c/a/r;->n()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 38
    instance-of v3, v0, Lcom/swedbank/mobile/app/c/a/d$b;

    if-eqz v3, :cond_1

    .line 39
    check-cast v0, Lcom/swedbank/mobile/app/c/a/d$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/c/a/d$b;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "resources.getString(information.titleResId)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 40
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/c/a/d$b;->b()Ljava/lang/Integer;

    move-result-object v0

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v4

    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    .line 41
    check-cast v4, Ljava/lang/CharSequence;

    move-object v8, v4

    move-object v4, v0

    move-object v0, v8

    goto :goto_1

    .line 43
    :cond_1
    instance-of v3, v0, Lcom/swedbank/mobile/app/c/a/d$a;

    if-eqz v3, :cond_4

    .line 44
    check-cast v0, Lcom/swedbank/mobile/app/c/a/d$a;

    const-string v3, "resources"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/c/a/d$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 45
    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/c/a/d$a;->c(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 46
    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/c/a/d$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 49
    :goto_1
    iget-object v5, p0, Lcom/swedbank/mobile/app/c/a/r;->e:Lcom/swedbank/mobile/business/biometric/d;

    .line 51
    new-instance v6, Landroid/hardware/biometrics/BiometricPrompt$Builder;

    iget-object v7, p0, Lcom/swedbank/mobile/app/c/a/r;->d:Landroid/app/Application;

    check-cast v7, Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/hardware/biometrics/BiometricPrompt$Builder;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {v6, v3}, Landroid/hardware/biometrics/BiometricPrompt$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/hardware/biometrics/BiometricPrompt$Builder;

    move-result-object v3

    if-eqz v0, :cond_2

    .line 54
    invoke-virtual {v3, v0}, Landroid/hardware/biometrics/BiometricPrompt$Builder;->setSubtitle(Ljava/lang/CharSequence;)Landroid/hardware/biometrics/BiometricPrompt$Builder;

    :cond_2
    if-eqz v4, :cond_3

    .line 55
    invoke-virtual {v3, v4}, Landroid/hardware/biometrics/BiometricPrompt$Builder;->setDescription(Ljava/lang/CharSequence;)Landroid/hardware/biometrics/BiometricPrompt$Builder;

    .line 58
    :cond_3
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->onboarding_cancel:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 59
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/r;->d:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getMainExecutor()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 60
    new-instance v4, Lcom/swedbank/mobile/app/c/a/r$a;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/c/a/r$a;-><init>(Lcom/swedbank/mobile/app/c/a/r;)V

    check-cast v4, Landroid/content/DialogInterface$OnClickListener;

    .line 57
    invoke-virtual {v3, v0, v2, v4}, Landroid/hardware/biometrics/BiometricPrompt$Builder;->setNegativeButton(Ljava/lang/CharSequence;Ljava/util/concurrent/Executor;Landroid/content/DialogInterface$OnClickListener;)Landroid/hardware/biometrics/BiometricPrompt$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Landroid/hardware/biometrics/BiometricPrompt$Builder;->build()Landroid/hardware/biometrics/BiometricPrompt;

    move-result-object v0

    const-string v2, "BiometricPrompt.Builder(\u2026})\n              .build()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-interface {v5, v1, v0}, Lcom/swedbank/mobile/business/biometric/d;->a(Ljava/security/Signature;Landroid/hardware/biometrics/BiometricPrompt;)Lio/reactivex/o;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/r;->b:Lcom/b/c/c;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "biometricRepository.auth\u2026ubscribe(authEventStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 76
    iput-object v0, p0, Lcom/swedbank/mobile/app/c/a/r;->c:Lio/reactivex/b/c;

    goto :goto_2

    .line 46
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 69
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/q;->c()Z

    move-result p1

    if-nez p1, :cond_6

    .line 70
    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/r;->c:Lio/reactivex/b/c;

    invoke-interface {p1}, Lio/reactivex/b/c;->a()V

    :cond_6
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/c/a/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/r;->a(Lcom/swedbank/mobile/app/c/a/q;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/r;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/r;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method
