.class final Lcom/swedbank/mobile/app/c/a/i$d;
.super Ljava/lang/Object;
.source "BiometricAuthenticationPromptPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/c/a/i$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/c/a/i$d;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/c/a/i$d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/c/a/i$d;->a:Lcom/swedbank/mobile/app/c/a/i$d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/biometric/a;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/biometric/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/biometric/a;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/c/a/q$a$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/swedbank/mobile/app/c/a/q$a$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/c/a/q$a$a;-><init>(Lcom/swedbank/mobile/business/biometric/a;)V

    .line 32
    instance-of v1, p1, Lcom/swedbank/mobile/business/biometric/a$b;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    instance-of v1, p1, Lcom/swedbank/mobile/business/biometric/a$a;

    if-eqz v1, :cond_1

    :goto_0
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(biometricEventState)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 34
    :cond_1
    instance-of p1, p1, Lcom/swedbank/mobile/business/biometric/a$c;

    if-eqz p1, :cond_2

    .line 36
    new-instance p1, Lcom/swedbank/mobile/app/c/a/q$a$a;

    const/4 v1, 0x0

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/app/c/a/q$a$a;-><init>(Lcom/swedbank/mobile/business/biometric/a;)V

    const-wide/16 v1, 0xdac

    .line 82
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/swedbank/mobile/app/c/a/i$d$a;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/c/a/i$d$a;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 80
    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/business/biometric/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/i$d;->a(Lcom/swedbank/mobile/business/biometric/a;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
