.class final synthetic Lcom/swedbank/mobile/app/c/a/i$a;
.super Lkotlin/e/b/i;
.source "BiometricAuthenticationPromptPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/c/a/q;",
        "Lcom/swedbank/mobile/app/c/a/q$a;",
        "Lcom/swedbank/mobile/app/c/a/q;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/c/a/i;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/q$a;)Lcom/swedbank/mobile/app/c/a/q;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/c/a/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/a/q$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/i$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/c/a/i;

    .line 77
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/q$a$c;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 78
    check-cast p2, Lcom/swedbank/mobile/app/c/a/q$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/q$a$c;->a()Ljava/security/Signature;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v1, p1

    .line 77
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/c/a/q;->a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/q;

    move-result-object p1

    goto :goto_2

    .line 79
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/q$a$b;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 80
    check-cast p2, Lcom/swedbank/mobile/app/c/a/q$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/q$a$b;->a()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    move-object v1, p1

    .line 79
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/c/a/q;->a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/q;

    move-result-object p1

    goto :goto_2

    .line 81
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/c/a/q$a$a;

    if-eqz v0, :cond_6

    check-cast p2, Lcom/swedbank/mobile/app/c/a/q$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/q$a$a;->a()Lcom/swedbank/mobile/business/biometric/a;

    move-result-object v0

    .line 82
    instance-of v1, v0, Lcom/swedbank/mobile/business/biometric/a$b;

    if-eqz v1, :cond_2

    goto :goto_0

    .line 83
    :cond_2
    instance-of v1, v0, Lcom/swedbank/mobile/business/biometric/a$a;

    if-eqz v1, :cond_3

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 85
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/q$a$a;->a()Lcom/swedbank/mobile/business/biometric/a;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x13

    const/4 v9, 0x0

    move-object v2, p1

    .line 83
    invoke-static/range {v2 .. v9}, Lcom/swedbank/mobile/app/c/a/q;->a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/q;

    move-result-object p1

    goto :goto_2

    :cond_3
    if-nez v0, :cond_4

    goto :goto_1

    .line 86
    :cond_4
    instance-of v0, v0, Lcom/swedbank/mobile/business/biometric/a$c;

    if-eqz v0, :cond_5

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 87
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/c/a/q$a$a;->a()Lcom/swedbank/mobile/business/biometric/a;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    move-object v1, p1

    .line 86
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/c/a/q;->a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/d;Ljava/security/Signature;ZLcom/swedbank/mobile/business/biometric/a;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/c/a/q;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/app/c/a/q;

    check-cast p2, Lcom/swedbank/mobile/app/c/a/q$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/c/a/i$a;->a(Lcom/swedbank/mobile/app/c/a/q;Lcom/swedbank/mobile/app/c/a/q$a;)Lcom/swedbank/mobile/app/c/a/q;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/c/a/i;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/biometric/authentication/BiometricAuthenticationPromptViewState;Lcom/swedbank/mobile/app/biometric/authentication/BiometricAuthenticationPromptViewState$PartialState;)Lcom/swedbank/mobile/app/biometric/authentication/BiometricAuthenticationPromptViewState;"

    return-object v0
.end method
