.class public final Lcom/swedbank/mobile/app/c/a/t;
.super Ljava/lang/Object;
.source "BiometricAuthenticationTimeAnalyticsEvent.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/a/a;


# instance fields
.field private final a:Landroid/os/Bundle;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/core/a/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLcom/swedbank/mobile/core/a/b;)V
    .locals 1
    .param p3    # Lcom/swedbank/mobile/core/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/a/t;->b:Lcom/swedbank/mobile/core/a/b;

    const/4 p3, 0x1

    .line 15
    new-array p3, p3, [Lkotlin/k;

    const-string v0, "authentication_time_in_ms"

    .line 16
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, p3, p2

    .line 15
    invoke-static {p3}, Landroidx/core/os/b;->a([Lkotlin/k;)Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/t;->a:Landroid/os/Bundle;

    return-void
.end method

.method public synthetic constructor <init>(JLcom/swedbank/mobile/core/a/b;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 13
    sget-object p3, Lcom/swedbank/mobile/core/a/b;->j:Lcom/swedbank/mobile/core/a/b;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/c/a/t;-><init>(JLcom/swedbank/mobile/core/a/b;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/t;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/core/a/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/t;->b:Lcom/swedbank/mobile/core/a/b;

    return-object v0
.end method
