.class public final Lcom/swedbank/mobile/app/c/a/b/h$a$b;
.super Lcom/swedbank/mobile/app/c/a/b/h$a;
.source "BiometricViewState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/c/a/b/h$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/biometric/login/a;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/biometric/login/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/biometric/login/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/c/a/b/h$a;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a:Lcom/swedbank/mobile/business/biometric/login/a;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/biometric/login/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a:Lcom/swedbank/mobile/business/biometric/login/a;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/app/c/a/b/h$a$b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/app/c/a/b/h$a$b;

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a:Lcom/swedbank/mobile/business/biometric/login/a;

    iget-object p1, p1, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a:Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a:Lcom/swedbank/mobile/business/biometric/login/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error(loginError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/b/h$a$b;->a:Lcom/swedbank/mobile/business/biometric/login/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
