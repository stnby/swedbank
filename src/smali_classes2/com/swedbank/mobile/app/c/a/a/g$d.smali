.class final Lcom/swedbank/mobile/app/c/a/a/g$d;
.super Lkotlin/e/b/k;
.source "ExternalBiometricAuthenticationRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a/a/g;->a(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/c/a/a/g;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/c/a/a/g;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->a:Lcom/swedbank/mobile/app/c/a/a/g;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->c:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 5
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    check-cast p1, Ljava/lang/Iterable;

    .line 74
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 75
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 73
    instance-of v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/i;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 42
    iget-object p1, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->a:Lcom/swedbank/mobile/app/c/a/a/g;

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->a:Lcom/swedbank/mobile/app/c/a/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/c/a/a/g;->a(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/swedbank/mobile/app/c/a/a/f;

    .line 37
    iget-object v2, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->a:Lcom/swedbank/mobile/app/c/a/a/g;

    invoke-static {v2}, Lcom/swedbank/mobile/app/c/a/a/g;->b(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v2

    .line 38
    iget-object v3, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->b:Ljava/lang/String;

    .line 39
    iget-object v4, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->c:Ljava/lang/String;

    .line 36
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/c/a/a/f;-><init>(Lcom/swedbank/mobile/business/util/l;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/app/c/a/d;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/app/c/a/a/g$d;->a:Lcom/swedbank/mobile/app/c/a/a/g;

    invoke-static {v1}, Lcom/swedbank/mobile/app/c/a/a/g;->c(Lcom/swedbank/mobile/app/c/a/a/g;)Lcom/swedbank/mobile/business/biometric/authentication/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/c/a/g;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 78
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/c/a/a/g$d;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
