.class public final Lcom/swedbank/mobile/app/c/a$a$1;
.super Lcom/swedbank/mobile/app/f/c$a;
.source "BiometricRouters.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/c/a$a;->a(Lcom/swedbank/mobile/app/f/a;)Lcom/swedbank/mobile/app/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/c/a$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/c/a$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/app/c/a$a$1;->a:Lcom/swedbank/mobile/app/c/a$a;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/f/c$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_disabled_title:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026biometric_disabled_title)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a$a$1;->a:Lcom/swedbank/mobile/app/c/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/c/a$a;->a:Lcom/swedbank/mobile/business/biometric/login/g;

    .line 22
    sget-object v1, Lcom/swedbank/mobile/business/biometric/login/g$a;->a:Lcom/swedbank/mobile/business/biometric/login/g$a;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_disabled_reason_device:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto/16 :goto_3

    .line 23
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/biometric/login/g$b;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a$a$1;->a:Lcom/swedbank/mobile/app/c/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/c/a$a;->a:Lcom/swedbank/mobile/business/biometric/login/g;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/login/g$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/login/g$b;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    move-object p1, v0

    check-cast p1, Ljava/lang/CharSequence;

    goto/16 :goto_3

    .line 24
    :cond_3
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_disabled_reason_remote:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_3

    .line 25
    :cond_4
    instance-of v0, v0, Lcom/swedbank/mobile/business/biometric/login/g$c;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/swedbank/mobile/app/c/a$a$1;->a:Lcom/swedbank/mobile/app/c/a$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/c/a$a;->a:Lcom/swedbank/mobile/business/biometric/login/g;

    check-cast v0, Lcom/swedbank/mobile/business/biometric/login/g$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/biometric/login/g$c;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object v0

    .line 35
    sget-object v1, Lcom/swedbank/mobile/app/c/b;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/e/l;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 52
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_debuggable:I

    goto :goto_2

    .line 51
    :pswitch_1
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_stacktrace:I

    goto :goto_2

    .line 50
    :pswitch_2
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_connection:I

    goto :goto_2

    .line 49
    :pswitch_3
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_device_files:I

    goto :goto_2

    .line 48
    :pswitch_4
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_strings:I

    goto :goto_2

    .line 47
    :pswitch_5
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_proc:I

    goto :goto_2

    .line 46
    :pswitch_6
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_memory:I

    goto :goto_2

    .line 45
    :pswitch_7
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_alcatel:I

    goto :goto_2

    .line 44
    :pswitch_8
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_test_keys:I

    goto :goto_2

    .line 43
    :pswitch_9
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_props:I

    goto :goto_2

    .line 42
    :pswitch_a
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_su:I

    goto :goto_2

    .line 41
    :pswitch_b
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_mgm_apps:I

    goto :goto_2

    .line 40
    :pswitch_c
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_apps:I

    goto :goto_2

    .line 39
    :pswitch_d
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_cloak_apps:I

    goto :goto_2

    .line 38
    :pswitch_e
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_bin:I

    goto :goto_2

    .line 37
    :pswitch_f
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_bbox:I

    goto :goto_2

    .line 36
    :pswitch_10
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_general:I

    .line 25
    :goto_2
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    :goto_3
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget v0, Lcom/swedbank/mobile/app/c/c$e;->biometric_disabled_confirmation:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026ic_disabled_confirmation)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public d(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method
