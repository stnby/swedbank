.class public final Lcom/swedbank/mobile/app/c/d/e;
.super Lcom/swedbank/mobile/architect/a/h;
.source "BiometricPreferenceRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/biometric/preference/d;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/c/c/a;

.field private final f:Lcom/swedbank/mobile/app/onboarding/c/a;

.field private final g:Lcom/swedbank/mobile/business/biometric/onboarding/e;

.field private final h:Lcom/swedbank/mobile/business/onboarding/loading/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/c/c/a;Lcom/swedbank/mobile/app/onboarding/c/a;Lcom/swedbank/mobile/business/biometric/onboarding/e;Lcom/swedbank/mobile/business/onboarding/loading/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/c/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/onboarding/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/biometric/onboarding/e;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_preference"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/onboarding/loading/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_preference"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_biometric_preference"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/c/c/a;",
            "Lcom/swedbank/mobile/app/onboarding/c/a;",
            "Lcom/swedbank/mobile/business/biometric/onboarding/e;",
            "Lcom/swedbank/mobile/business/onboarding/loading/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "biometricOnboardingBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingLoadingBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricOnboardingListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingLoadingListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    .line 27
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/c/d/e;->e:Lcom/swedbank/mobile/app/c/c/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/c/d/e;->f:Lcom/swedbank/mobile/app/onboarding/c/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/c/d/e;->g:Lcom/swedbank/mobile/business/biometric/onboarding/e;

    iput-object p4, p0, Lcom/swedbank/mobile/app/c/d/e;->h:Lcom/swedbank/mobile/business/onboarding/loading/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/c/d/e;)Lcom/swedbank/mobile/app/onboarding/c/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/d/e;->f:Lcom/swedbank/mobile/app/onboarding/c/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/c/d/e;)Lcom/swedbank/mobile/business/onboarding/loading/c;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/d/e;->h:Lcom/swedbank/mobile/business/onboarding/loading/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/c/d/e;)Lcom/swedbank/mobile/app/c/c/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/d/e;->e:Lcom/swedbank/mobile/app/c/c/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/c/d/e;)Lcom/swedbank/mobile/business/biometric/onboarding/e;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/c/d/e;->g:Lcom/swedbank/mobile/business/biometric/onboarding/e;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 28
    new-instance v0, Lcom/swedbank/mobile/app/c/d/e$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/c/d/e$a;-><init>(Lcom/swedbank/mobile/app/c/d/e;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/d/e;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 37
    new-instance v0, Lcom/swedbank/mobile/app/c/d/e$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/c/d/e$b;-><init>(Lcom/swedbank/mobile/app/c/d/e;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/c/d/e;->b(Lkotlin/e/a/b;)V

    return-void
.end method
