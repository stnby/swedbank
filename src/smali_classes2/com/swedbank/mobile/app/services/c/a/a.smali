.class public final Lcom/swedbank/mobile/app/services/c/a/a;
.super Lcom/swedbank/mobile/app/f/a/d$b;
.source "MPosActionsDialogInformation.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/services/c/a/a;

.field private static final b:I

.field private static final c:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/services/c/a/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/services/c/a/a;->a:Lcom/swedbank/mobile/app/services/c/a/a;

    .line 10
    sget v0, Lcom/swedbank/mobile/app/services/a$f;->services_mpos_manage_actions_title:I

    sput v0, Lcom/swedbank/mobile/app/services/c/a/a;->b:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/app/f/a/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 10
    sget v0, Lcom/swedbank/mobile/app/services/c/a/a;->b:I

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 11
    sget-object v0, Lcom/swedbank/mobile/app/services/c/a/a;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/f/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x2

    .line 13
    new-array v0, v0, [Lcom/swedbank/mobile/app/f/a/a;

    .line 14
    new-instance v1, Lcom/swedbank/mobile/app/f/a/a;

    .line 15
    sget-object v2, Lcom/swedbank/mobile/business/services/plugins/agreements/d;->a:Lcom/swedbank/mobile/business/services/plugins/agreements/d;

    .line 16
    sget v3, Lcom/swedbank/mobile/app/services/a$f;->services_mpos_manage_action_install:I

    invoke-static {v3}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v3

    .line 17
    sget v4, Lcom/swedbank/mobile/app/services/a$c;->ic_mpos_pair:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 14
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 18
    new-instance v1, Lcom/swedbank/mobile/app/f/a/a;

    .line 19
    sget-object v2, Lcom/swedbank/mobile/business/services/plugins/agreements/d;->b:Lcom/swedbank/mobile/business/services/plugins/agreements/d;

    .line 20
    sget v3, Lcom/swedbank/mobile/app/services/a$f;->services_mpos_manage_action_uninstall:I

    invoke-static {v3}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v3

    .line 21
    sget v4, Lcom/swedbank/mobile/app/services/a$c;->ic_mpos_unpair:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 18
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 13
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
