.class public final Lcom/swedbank/mobile/app/services/c/b/a;
.super Ljava/lang/Object;
.source "ServicesIbankRenderers.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/services/c/b/j$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/services/c/b/j$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:I

.field private final c:Lcom/swedbank/mobile/business/c/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "country"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->c:Lcom/swedbank/mobile/business/c/c;

    .line 278
    const-class p1, Lcom/swedbank/mobile/app/services/c/b/j$a;

    invoke-static {p1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->a:Lkotlin/h/b;

    .line 279
    sget p1, Lcom/swedbank/mobile/app/services/a$e;->item_services_header:I

    iput p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->b:I

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/services/c/b/j$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 278
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/c/b/a;->a:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/services/c/b/j$a;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/services/c/b/j$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/services/c/b/j$a;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    sget p3, Lcom/swedbank/mobile/app/services/a$d;->services_group_header_title:I

    invoke-virtual {p2, p3}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 289
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/c/b/j$a;->b()Lcom/swedbank/mobile/business/services/a/b;

    move-result-object p1

    sget-object p3, Lcom/swedbank/mobile/app/services/c/b/b;->e:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/a/b;->ordinal()I

    move-result p1

    aget p1, p3, p1

    packed-switch p1, :pswitch_data_0

    .line 308
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->c:Lcom/swedbank/mobile/business/c/c;

    sget-object p3, Lcom/swedbank/mobile/app/services/c/b/b;->d:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p1

    aget p1, p3, p1

    packed-switch p1, :pswitch_data_1

    .line 312
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 311
    :pswitch_2
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_insurance_lt:I

    goto/16 :goto_0

    .line 310
    :pswitch_3
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_insurance_lv:I

    goto/16 :goto_0

    .line 309
    :pswitch_4
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_insurance_ee:I

    goto/16 :goto_0

    .line 302
    :pswitch_5
    iget-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->c:Lcom/swedbank/mobile/business/c/c;

    sget-object p3, Lcom/swedbank/mobile/app/services/c/b/b;->c:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p1

    aget p1, p3, p1

    packed-switch p1, :pswitch_data_2

    .line 306
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 305
    :pswitch_7
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_life_insurance_lt:I

    goto :goto_0

    .line 304
    :pswitch_8
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_life_insurance_lv:I

    goto :goto_0

    .line 303
    :pswitch_9
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_life_insurance_ee:I

    goto :goto_0

    .line 296
    :pswitch_a
    iget-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->c:Lcom/swedbank/mobile/business/c/c;

    sget-object p3, Lcom/swedbank/mobile/app/services/c/b/b;->b:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p1

    aget p1, p3, p1

    packed-switch p1, :pswitch_data_3

    .line 300
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_b
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 299
    :pswitch_c
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_loans_lt:I

    goto :goto_0

    .line 298
    :pswitch_d
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_loans_lv:I

    goto :goto_0

    .line 297
    :pswitch_e
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_loans_ee:I

    goto :goto_0

    .line 290
    :pswitch_f
    iget-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/a;->c:Lcom/swedbank/mobile/business/c/c;

    sget-object p3, Lcom/swedbank/mobile/app/services/c/b/b;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p1

    aget p1, p3, p1

    packed-switch p1, :pswitch_data_4

    .line 294
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_10
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 293
    :pswitch_11
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_general_lt:I

    goto :goto_0

    .line 292
    :pswitch_12
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_general_lv:I

    goto :goto_0

    .line 291
    :pswitch_13
    sget p1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_header_general_ee:I

    .line 289
    :goto_0
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_a
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 275
    check-cast p1, Lcom/swedbank/mobile/app/services/c/b/j$a;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/services/c/b/a;->a(Lcom/swedbank/mobile/app/services/c/b/j$a;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 279
    iget v0, p0, Lcom/swedbank/mobile/app/services/c/b/a;->b:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 275
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/f$a;->a(Lcom/swedbank/mobile/app/plugins/list/f;)Z

    move-result v0

    return v0
.end method
