.class public final Lcom/swedbank/mobile/app/services/c/a/d;
.super Ljava/lang/Object;
.source "ServicesAgreementsDataMapper.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/services/plugins/agreements/i;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/services/c/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/d;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/services/c/a/d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/services/c/a/d;->a:Lcom/swedbank/mobile/app/services/c/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/services/plugins/agreements/i;)Lcom/swedbank/mobile/app/plugins/list/g;
    .locals 13
    .param p1    # Lcom/swedbank/mobile/business/services/plugins/agreements/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v0, Lcom/swedbank/mobile/app/plugins/list/g;

    .line 11
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/i;->a()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v1, :cond_0

    new-array v1, v3, [Lcom/swedbank/mobile/app/plugins/list/d;

    .line 12
    new-instance v5, Lcom/swedbank/mobile/app/services/c/a/e;

    const/4 v6, 0x3

    invoke-direct {v5, v4, v4, v6, v4}, Lcom/swedbank/mobile/app/services/c/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    check-cast v5, Lcom/swedbank/mobile/app/plugins/list/d;

    aput-object v5, v1, v2

    const/4 v5, 0x1

    .line 13
    new-instance v12, Lcom/swedbank/mobile/app/services/c/a/g;

    .line 14
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/plugins/agreements/i;->b()Z

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x6

    const/4 v11, 0x0

    move-object v6, v12

    .line 13
    invoke-direct/range {v6 .. v11}, Lcom/swedbank/mobile/app/services/c/a/g;-><init>(ZLjava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    check-cast v12, Lcom/swedbank/mobile/app/plugins/list/d;

    aput-object v12, v1, v5

    .line 11
    invoke-static {v1}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 16
    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    .line 9
    :goto_0
    invoke-direct {v0, p1, v2, v3, v4}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;ZILkotlin/e/b/g;)V

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/swedbank/mobile/business/services/plugins/agreements/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/services/c/a/d;->a(Lcom/swedbank/mobile/business/services/plugins/agreements/i;)Lcom/swedbank/mobile/app/plugins/list/g;

    move-result-object p1

    return-object p1
.end method
