.class public final Lcom/swedbank/mobile/app/services/c/a/h;
.super Ljava/lang/Object;
.source "ServicesAgreementsRenderers.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/services/c/a/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/services/c/a/h;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/services/c/a/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/services/c/a/h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/services/c/a/h;->a:Lcom/swedbank/mobile/app/services/c/a/h;

    .line 34
    const-class v0, Lcom/swedbank/mobile/app/services/c/a/g;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/services/c/a/h;->b:Lkotlin/h/b;

    .line 35
    sget v0, Lcom/swedbank/mobile/app/services/a$e;->item_agreements_mpos:I

    sput v0, Lcom/swedbank/mobile/app/services/c/a/h;->c:I

    const/4 v0, 0x1

    .line 36
    sput-boolean v0, Lcom/swedbank/mobile/app/services/c/a/h;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/services/c/a/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    sget-object v0, Lcom/swedbank/mobile/app/services/c/a/h;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/services/c/a/g;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/services/c/a/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/services/c/a/g;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget p3, Lcom/swedbank/mobile/app/services/a$d;->agreements_mpos_main_action_btn:I

    invoke-virtual {p2, p3}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/Button;

    .line 45
    sget v0, Lcom/swedbank/mobile/app/services/a$d;->agreements_mpos_secondary_action_btn:I

    invoke-virtual {p2, v0}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 49
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/c/a/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    sget v0, Lcom/swedbank/mobile/app/services/a$f;->services_mpos_open:I

    invoke-virtual {p3, v0}, Landroid/widget/Button;->setText(I)V

    .line 51
    check-cast p3, Landroid/view/View;

    .line 77
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/h$a;

    invoke-direct {v0, p1, p4}, Lcom/swedbank/mobile/app/services/c/a/h$a;-><init>(Lcom/swedbank/mobile/app/services/c/a/g;Lio/reactivex/c/g;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 56
    :cond_0
    sget v0, Lcom/swedbank/mobile/app/services/a$f;->services_mpos_get_app:I

    invoke-virtual {p3, v0}, Landroid/widget/Button;->setText(I)V

    .line 57
    check-cast p3, Landroid/view/View;

    .line 81
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/h$b;

    invoke-direct {v0, p1, p4}, Lcom/swedbank/mobile/app/services/c/a/h$b;-><init>(Lcom/swedbank/mobile/app/services/c/a/g;Lio/reactivex/c/g;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/c/a/g;->b()Z

    move-result p3

    if-eqz p3, :cond_1

    const/4 p3, 0x0

    .line 67
    invoke-virtual {p2, p3}, Landroid/widget/Button;->setVisibility(I)V

    .line 68
    check-cast p2, Landroid/view/View;

    .line 85
    new-instance p3, Lcom/swedbank/mobile/app/services/c/a/h$c;

    invoke-direct {p3, p1, p4}, Lcom/swedbank/mobile/app/services/c/a/h$c;-><init>(Lcom/swedbank/mobile/app/services/c/a/g;Lio/reactivex/c/g;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_1
    const/16 p1, 0x8

    .line 72
    invoke-virtual {p2, p1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/swedbank/mobile/app/services/c/a/g;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/services/c/a/h;->a(Lcom/swedbank/mobile/app/services/c/a/g;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 35
    sget v0, Lcom/swedbank/mobile/app/services/c/a/h;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 36
    sget-boolean v0, Lcom/swedbank/mobile/app/services/c/a/h;->d:Z

    return v0
.end method
