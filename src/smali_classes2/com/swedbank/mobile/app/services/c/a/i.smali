.class public final Lcom/swedbank/mobile/app/services/c/a/i;
.super Lcom/swedbank/mobile/architect/a/h;
.source "ServicesAgreementsRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/services/plugins/agreements/h;


# instance fields
.field private final e:Landroid/app/Application;

.field private final f:Lcom/swedbank/mobile/app/services/c/a/a/a/a;

.field private final g:Lcom/swedbank/mobile/app/f/a/b;

.field private final h:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

.field private final i:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/app/services/c/a/a/a/a;Lcom/swedbank/mobile/app/f/a/b;Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/services/c/a/a/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_services_agreements"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_services_agreements"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_services_agreements"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/app/services/c/a/a/a/a;",
            "Lcom/swedbank/mobile/app/f/a/b;",
            "Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mPosOnboardingBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mPosOnboardingListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    .line 32
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/c/a/i;->e:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/services/c/a/i;->f:Lcom/swedbank/mobile/app/services/c/a/a/a/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/services/c/a/i;->g:Lcom/swedbank/mobile/app/f/a/b;

    iput-object p4, p0, Lcom/swedbank/mobile/app/services/c/a/i;->h:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

    iput-object p5, p0, Lcom/swedbank/mobile/app/services/c/a/i;->i:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/services/c/a/i;)Lcom/swedbank/mobile/app/services/c/a/a/a/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/c/a/i;->f:Lcom/swedbank/mobile/app/services/c/a/a/a/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/services/c/a/i;)Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/c/a/i;->h:Lcom/swedbank/mobile/business/services/plugins/agreements/onboarding/mpos/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/services/c/a/i;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/c/a/i;->g:Lcom/swedbank/mobile/app/f/a/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/services/c/a/i;)Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/c/a/i;->i:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/i$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/services/c/a/i$d;-><init>(Lcom/swedbank/mobile/app/services/c/a/i;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/services/c/a/i;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/c/a/i;->e:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/swedbank/mobile/app/w/a;->c(Landroid/content/Context;Ljava/lang/String;)Z

    return-void
.end method

.method public a(Ljava/lang/String;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "-",
            "Landroid/content/Intent;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/c/a/i;->e:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/swedbank/mobile/app/w/a;->a(Landroid/content/Context;Ljava/lang/String;Lkotlin/e/a/b;)Z

    return-void
.end method

.method public b()V
    .locals 1

    .line 68
    sget-object v0, Lcom/swedbank/mobile/app/services/c/a/i$b;->a:Lcom/swedbank/mobile/app/services/c/a/i$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 55
    new-instance v0, Lcom/swedbank/mobile/app/services/c/a/i$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/services/c/a/i$c;-><init>(Lcom/swedbank/mobile/app/services/c/a/i;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/services/c/a/i;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 70
    sget-object v0, Lcom/swedbank/mobile/app/services/c/a/i$a;->a:Lcom/swedbank/mobile/app/services/c/a/i$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
