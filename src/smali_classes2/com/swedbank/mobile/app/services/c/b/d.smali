.class public final Lcom/swedbank/mobile/app/services/c/b/d;
.super Ljava/lang/Object;
.source "ServicesIbankRenderers.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/services/c/b/j$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/services/c/b/j$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:I

.field private final c:Z

.field private final d:Lcom/swedbank/mobile/business/c/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/c/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "country"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    .line 28
    const-class p1, Lcom/swedbank/mobile/app/services/c/b/j$b;

    invoke-static {p1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->a:Lkotlin/h/b;

    .line 29
    sget p1, Lcom/swedbank/mobile/app/services/a$e;->item_services_ibank:I

    iput p1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->b:I

    const/4 p1, 0x1

    .line 30
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->c:Z

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/services/c/b/j$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/c/b/d;->a:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/services/c/b/j$b;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/app/services/c/b/j$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/services/c/b/j$b;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object p3, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string v0, "holder.itemView"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 40
    sget v1, Lcom/swedbank/mobile/app/services/a$d;->ibank_service_name:I

    invoke-virtual {p2, v1}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 41
    sget v2, Lcom/swedbank/mobile/app/services/a$d;->ibank_service_description:I

    invoke-virtual {p2, v2}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/c/b/j$b;->b()Lcom/swedbank/mobile/business/services/a/a;

    move-result-object v2

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->r:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/services/a/a;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 150
    new-instance p1, Lkotlin/j;

    const-string p2, "Requested service rendering logic is not implemented"

    invoke-direct {p1, p2}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 143
    :pswitch_0
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->q:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    .line 147
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 146
    :pswitch_2
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_claim_application_lt:I

    goto/16 :goto_0

    .line 145
    :pswitch_3
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_claim_application_lv:I

    goto/16 :goto_0

    .line 144
    :pswitch_4
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_claim_application_ee:I

    goto/16 :goto_0

    .line 137
    :pswitch_5
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->p:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_2

    .line 141
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 140
    :pswitch_7
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_loan_payment_protection_lt:I

    goto/16 :goto_0

    .line 139
    :pswitch_8
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_loan_payment_protection_lv:I

    goto/16 :goto_0

    .line 138
    :pswitch_9
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_loan_payment_protection_ee:I

    goto/16 :goto_0

    .line 131
    :pswitch_a
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->o:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_3

    .line 135
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_b
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 134
    :pswitch_c
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_credit_cover_lt:I

    goto/16 :goto_0

    .line 133
    :pswitch_d
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_credit_cover_lv:I

    goto/16 :goto_0

    .line 132
    :pswitch_e
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_credit_cover_ee:I

    goto/16 :goto_0

    .line 125
    :pswitch_f
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->n:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_4

    .line 129
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_10
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 128
    :pswitch_11
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_travel_insurance_lt:I

    goto/16 :goto_0

    .line 127
    :pswitch_12
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_travel_insurance_lv:I

    goto/16 :goto_0

    .line 126
    :pswitch_13
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_travel_insurance_ee:I

    goto/16 :goto_0

    .line 119
    :pswitch_14
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->m:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_5

    .line 123
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_15
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 122
    :pswitch_16
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_traffic_insurance_lt:I

    goto/16 :goto_0

    .line 121
    :pswitch_17
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_traffic_insurance_lv:I

    goto/16 :goto_0

    .line 120
    :pswitch_18
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_traffic_insurance_ee:I

    goto/16 :goto_0

    .line 113
    :pswitch_19
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->l:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_6

    .line 117
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_1a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 116
    :pswitch_1b
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_casco_insurance_lt:I

    goto/16 :goto_0

    .line 115
    :pswitch_1c
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_casco_insurance_lv:I

    goto/16 :goto_0

    .line 114
    :pswitch_1d
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_casco_insurance_ee:I

    goto/16 :goto_0

    .line 107
    :pswitch_1e
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->k:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_7

    .line 111
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_1f
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 110
    :pswitch_20
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_insurance_lt:I

    goto/16 :goto_0

    .line 109
    :pswitch_21
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_insurance_lv:I

    goto/16 :goto_0

    .line 108
    :pswitch_22
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_insurance_ee:I

    goto/16 :goto_0

    .line 100
    :pswitch_23
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->j:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_8

    .line 104
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_24
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 103
    :pswitch_25
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_private_portfolio_lt:I

    goto/16 :goto_0

    .line 102
    :pswitch_26
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_private_portfolio_lv:I

    goto/16 :goto_0

    .line 101
    :pswitch_27
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_private_portfolio_ee:I

    goto/16 :goto_0

    .line 94
    :pswitch_28
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->i:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_9

    .line 98
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_29
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 97
    :pswitch_2a
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_pension_fund_lt:I

    goto/16 :goto_0

    .line 96
    :pswitch_2b
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_pension_fund_lv:I

    goto/16 :goto_0

    .line 95
    :pswitch_2c
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_pension_fund_ee:I

    goto/16 :goto_0

    .line 88
    :pswitch_2d
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->h:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_a

    .line 92
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_2e
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 91
    :pswitch_2f
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_child_lt:I

    goto/16 :goto_0

    .line 90
    :pswitch_30
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_child_lv:I

    goto/16 :goto_0

    .line 89
    :pswitch_31
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_child_ee:I

    goto/16 :goto_0

    .line 82
    :pswitch_32
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->g:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_b

    .line 86
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_33
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 85
    :pswitch_34
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_life_risk_insurance_lt:I

    goto/16 :goto_0

    .line 84
    :pswitch_35
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_life_risk_insurance_lv:I

    goto/16 :goto_0

    .line 83
    :pswitch_36
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_life_risk_insurance_ee:I

    goto/16 :goto_0

    .line 75
    :pswitch_37
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->f:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_c

    .line 79
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_38
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 78
    :pswitch_39
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_leasing_lt:I

    goto/16 :goto_0

    .line 77
    :pswitch_3a
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_leasing_lv:I

    goto/16 :goto_0

    .line 76
    :pswitch_3b
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_leasing_ee:I

    goto/16 :goto_0

    .line 69
    :pswitch_3c
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->e:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_d

    .line 73
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_3d
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 72
    :pswitch_3e
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_loan_lt:I

    goto/16 :goto_0

    .line 71
    :pswitch_3f
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_loan_lv:I

    goto/16 :goto_0

    .line 70
    :pswitch_40
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_loan_ee:I

    goto/16 :goto_0

    .line 63
    :pswitch_41
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->d:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_e

    .line 67
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_42
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 66
    :pswitch_43
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_mortgage_lt:I

    goto/16 :goto_0

    .line 65
    :pswitch_44
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_mortgage_lv:I

    goto/16 :goto_0

    .line 64
    :pswitch_45
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_mortgage_ee:I

    goto/16 :goto_0

    .line 57
    :pswitch_46
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->c:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_f

    .line 61
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_47
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 60
    :pswitch_48
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_small_loan_lt:I

    goto :goto_0

    .line 59
    :pswitch_49
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_small_loan_lv:I

    goto :goto_0

    .line 58
    :pswitch_4a
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_small_loan_ee:I

    goto :goto_0

    .line 51
    :pswitch_4b
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->b:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_10

    .line 55
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_4c
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 54
    :pswitch_4d
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_small_loan_lt:I

    goto :goto_0

    .line 53
    :pswitch_4e
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_small_loan_lv:I

    goto :goto_0

    .line 52
    :pswitch_4f
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_small_loan_ee:I

    goto :goto_0

    .line 44
    :pswitch_50
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v3, Lcom/swedbank/mobile/app/services/c/b/e;->a:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_11

    .line 48
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_51
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 47
    :pswitch_52
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_currency_rates_lt:I

    goto :goto_0

    .line 46
    :pswitch_53
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_currency_rates_lv:I

    goto :goto_0

    .line 45
    :pswitch_54
    sget v2, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_currency_rates_ee:I

    .line 43
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 153
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/c/b/j$b;->b()Lcom/swedbank/mobile/business/services/a/a;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->J:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/services/a/a;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_12

    .line 260
    new-instance p1, Lkotlin/j;

    const-string p2, "Requested service rendering logic is not implemented"

    invoke-direct {p1, p2}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 253
    :pswitch_55
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->I:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_13

    .line 257
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_56
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 256
    :pswitch_57
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_claim_application_description_lt:I

    goto/16 :goto_1

    .line 255
    :pswitch_58
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_claim_application_description_lv:I

    goto/16 :goto_1

    .line 254
    :pswitch_59
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_claim_application_description_ee:I

    goto/16 :goto_1

    .line 247
    :pswitch_5a
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->H:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_14

    .line 251
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_5b
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 250
    :pswitch_5c
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_loan_payment_protection_description_lt:I

    goto/16 :goto_1

    .line 249
    :pswitch_5d
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_loan_payment_protection_description_lv:I

    goto/16 :goto_1

    .line 248
    :pswitch_5e
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_loan_payment_protection_description_ee:I

    goto/16 :goto_1

    .line 241
    :pswitch_5f
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->G:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_15

    .line 245
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_60
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 244
    :pswitch_61
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_credit_cover_description_lt:I

    goto/16 :goto_1

    .line 243
    :pswitch_62
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_credit_cover_description_lv:I

    goto/16 :goto_1

    .line 242
    :pswitch_63
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_credit_cover_description_ee:I

    goto/16 :goto_1

    .line 235
    :pswitch_64
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->F:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_16

    .line 239
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_65
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 238
    :pswitch_66
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_travel_insurance_description_lt:I

    goto/16 :goto_1

    .line 237
    :pswitch_67
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_travel_insurance_description_lv:I

    goto/16 :goto_1

    .line 236
    :pswitch_68
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_travel_insurance_description_ee:I

    goto/16 :goto_1

    .line 229
    :pswitch_69
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->E:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_17

    .line 233
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_6a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 232
    :pswitch_6b
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_traffic_insurance_description_lt:I

    goto/16 :goto_1

    .line 231
    :pswitch_6c
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_traffic_insurance_description_lv:I

    goto/16 :goto_1

    .line 230
    :pswitch_6d
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_traffic_insurance_description_ee:I

    goto/16 :goto_1

    .line 223
    :pswitch_6e
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->D:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_18

    .line 227
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_6f
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 226
    :pswitch_70
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_casco_insurance_description_lt:I

    goto/16 :goto_1

    .line 225
    :pswitch_71
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_casco_insurance_description_lv:I

    goto/16 :goto_1

    .line 224
    :pswitch_72
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_casco_insurance_description_ee:I

    goto/16 :goto_1

    .line 217
    :pswitch_73
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->C:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_19

    .line 221
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_74
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 220
    :pswitch_75
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_insurance_description_lt:I

    goto/16 :goto_1

    .line 219
    :pswitch_76
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_insurance_description_lv:I

    goto/16 :goto_1

    .line 218
    :pswitch_77
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_insurance_description_ee:I

    goto/16 :goto_1

    .line 210
    :pswitch_78
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->B:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1a

    .line 214
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_79
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 213
    :pswitch_7a
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_private_portfolio_description_lt:I

    goto/16 :goto_1

    .line 212
    :pswitch_7b
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_private_portfolio_description_lv:I

    goto/16 :goto_1

    .line 211
    :pswitch_7c
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_private_portfolio_description_ee:I

    goto/16 :goto_1

    .line 204
    :pswitch_7d
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->A:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1b

    .line 208
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_7e
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 207
    :pswitch_7f
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_pension_fund_description_lt:I

    goto/16 :goto_1

    .line 206
    :pswitch_80
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_pension_fund_description_lv:I

    goto/16 :goto_1

    .line 205
    :pswitch_81
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_pension_fund_description_ee:I

    goto/16 :goto_1

    .line 198
    :pswitch_82
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->z:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1c

    .line 202
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_83
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 201
    :pswitch_84
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_child_description_lt:I

    goto/16 :goto_1

    .line 200
    :pswitch_85
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_child_description_lv:I

    goto/16 :goto_1

    .line 199
    :pswitch_86
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_safe_child_description_ee:I

    goto/16 :goto_1

    .line 192
    :pswitch_87
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->y:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1d

    .line 196
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_88
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 195
    :pswitch_89
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_life_risk_insurance_description_lt:I

    goto/16 :goto_1

    .line 194
    :pswitch_8a
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_life_risk_insurance_description_lv:I

    goto/16 :goto_1

    .line 193
    :pswitch_8b
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_life_risk_insurance_description_ee:I

    goto/16 :goto_1

    .line 185
    :pswitch_8c
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->x:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1e

    .line 189
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_8d
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 188
    :pswitch_8e
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_leasing_description_lt:I

    goto/16 :goto_1

    .line 187
    :pswitch_8f
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_leasing_description_lv:I

    goto/16 :goto_1

    .line 186
    :pswitch_90
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_leasing_description_ee:I

    goto/16 :goto_1

    .line 179
    :pswitch_91
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->w:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1f

    .line 183
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_92
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 182
    :pswitch_93
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_loan_description_lt:I

    goto/16 :goto_1

    .line 181
    :pswitch_94
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_loan_description_lv:I

    goto/16 :goto_1

    .line 180
    :pswitch_95
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_car_loan_description_ee:I

    goto/16 :goto_1

    .line 173
    :pswitch_96
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->v:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_20

    .line 177
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_97
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 176
    :pswitch_98
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_mortgage_description_lt:I

    goto/16 :goto_1

    .line 175
    :pswitch_99
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_mortgage_description_lv:I

    goto/16 :goto_1

    .line 174
    :pswitch_9a
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_mortgage_description_ee:I

    goto/16 :goto_1

    .line 167
    :pswitch_9b
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->u:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_21

    .line 171
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_9c
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 170
    :pswitch_9d
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_small_loan_description_lt:I

    goto :goto_1

    .line 169
    :pswitch_9e
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_small_loan_description_lv:I

    goto :goto_1

    .line 168
    :pswitch_9f
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_home_small_loan_description_ee:I

    goto :goto_1

    .line 161
    :pswitch_a0
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->t:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_22

    .line 165
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_a1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 164
    :pswitch_a2
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_small_loan_description_lt:I

    goto :goto_1

    .line 163
    :pswitch_a3
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_small_loan_description_lv:I

    goto :goto_1

    .line 162
    :pswitch_a4
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_small_loan_description_ee:I

    goto :goto_1

    .line 154
    :pswitch_a5
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c/b/d;->d:Lcom/swedbank/mobile/business/c/c;

    sget-object v2, Lcom/swedbank/mobile/app/services/c/b/e;->s:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_23

    .line 158
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_a6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No service strings exist for UNKNOWN country"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 157
    :pswitch_a7
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_currency_rates_description_lt:I

    goto :goto_1

    .line 156
    :pswitch_a8
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_currency_rates_description_lv:I

    goto :goto_1

    .line 155
    :pswitch_a9
    sget v1, Lcom/swedbank/mobile/app/services/a$f;->services_ibank_currency_rates_description_ee:I

    :goto_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 263
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_0
    const-string v0, ""

    :goto_2
    const-string v1, "when {\n      description\u2026s)\n      else -> \"\"\n    }"

    .line 262
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    move-object v1, p2

    check-cast v1, Landroid/view/View;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :cond_1
    const/16 v2, 0x8

    .line 317
    :goto_3
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 267
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    new-instance p2, Lcom/swedbank/mobile/app/services/c/b/d$a;

    invoke-direct {p2, p4, p1}, Lcom/swedbank/mobile/app/services/c/b/d$a;-><init>(Lio/reactivex/c/g;Lcom/swedbank/mobile/app/services/c/b/j$b;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_50
        :pswitch_4b
        :pswitch_46
        :pswitch_41
        :pswitch_3c
        :pswitch_37
        :pswitch_32
        :pswitch_2d
        :pswitch_28
        :pswitch_23
        :pswitch_1e
        :pswitch_19
        :pswitch_14
        :pswitch_f
        :pswitch_a
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
    .end packed-switch

    :pswitch_data_b
    .packed-switch 0x1
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
    .end packed-switch

    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
    .end packed-switch

    :pswitch_data_d
    .packed-switch 0x1
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
    .end packed-switch

    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
    .end packed-switch

    :pswitch_data_f
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
    .end packed-switch

    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_4f
        :pswitch_4e
        :pswitch_4d
        :pswitch_4c
    .end packed-switch

    :pswitch_data_11
    .packed-switch 0x1
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_51
    .end packed-switch

    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_a5
        :pswitch_a0
        :pswitch_9b
        :pswitch_96
        :pswitch_91
        :pswitch_8c
        :pswitch_87
        :pswitch_82
        :pswitch_7d
        :pswitch_78
        :pswitch_73
        :pswitch_6e
        :pswitch_69
        :pswitch_64
        :pswitch_5f
        :pswitch_5a
        :pswitch_55
    .end packed-switch

    :pswitch_data_13
    .packed-switch 0x1
        :pswitch_59
        :pswitch_58
        :pswitch_57
        :pswitch_56
    .end packed-switch

    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_5e
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
    .end packed-switch

    :pswitch_data_15
    .packed-switch 0x1
        :pswitch_63
        :pswitch_62
        :pswitch_61
        :pswitch_60
    .end packed-switch

    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_68
        :pswitch_67
        :pswitch_66
        :pswitch_65
    .end packed-switch

    :pswitch_data_17
    .packed-switch 0x1
        :pswitch_6d
        :pswitch_6c
        :pswitch_6b
        :pswitch_6a
    .end packed-switch

    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_72
        :pswitch_71
        :pswitch_70
        :pswitch_6f
    .end packed-switch

    :pswitch_data_19
    .packed-switch 0x1
        :pswitch_77
        :pswitch_76
        :pswitch_75
        :pswitch_74
    .end packed-switch

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_7c
        :pswitch_7b
        :pswitch_7a
        :pswitch_79
    .end packed-switch

    :pswitch_data_1b
    .packed-switch 0x1
        :pswitch_81
        :pswitch_80
        :pswitch_7f
        :pswitch_7e
    .end packed-switch

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_86
        :pswitch_85
        :pswitch_84
        :pswitch_83
    .end packed-switch

    :pswitch_data_1d
    .packed-switch 0x1
        :pswitch_8b
        :pswitch_8a
        :pswitch_89
        :pswitch_88
    .end packed-switch

    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_90
        :pswitch_8f
        :pswitch_8e
        :pswitch_8d
    .end packed-switch

    :pswitch_data_1f
    .packed-switch 0x1
        :pswitch_95
        :pswitch_94
        :pswitch_93
        :pswitch_92
    .end packed-switch

    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_9a
        :pswitch_99
        :pswitch_98
        :pswitch_97
    .end packed-switch

    :pswitch_data_21
    .packed-switch 0x1
        :pswitch_9f
        :pswitch_9e
        :pswitch_9d
        :pswitch_9c
    .end packed-switch

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_a4
        :pswitch_a3
        :pswitch_a2
        :pswitch_a1
    .end packed-switch

    :pswitch_data_23
    .packed-switch 0x1
        :pswitch_a9
        :pswitch_a8
        :pswitch_a7
        :pswitch_a6
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/app/services/c/b/j$b;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/services/c/b/d;->a(Lcom/swedbank/mobile/app/services/c/b/j$b;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/swedbank/mobile/app/services/c/b/d;->b:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/services/c/b/d;->c:Z

    return v0
.end method
