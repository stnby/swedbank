.class public final Lcom/swedbank/mobile/app/services/p;
.super Ljava/lang/Object;
.source "ServicesViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/services/p$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/swedbank/mobile/app/plugins/list/b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Z

.field private final d:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/app/w/b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/app/services/p;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)V
    .locals 0
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/w/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/app/plugins/list/b;",
            "Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/app/w/b;",
            ")V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/services/p;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/services/p;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    iput-object p5, p0, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V
    .locals 4

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    const/4 p7, 0x0

    goto :goto_0

    :cond_0
    move p7, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 14
    move-object p2, v1

    check-cast p2, Lcom/swedbank/mobile/app/plugins/list/b;

    :cond_1
    move-object v2, p2

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    move v0, p3

    :goto_1
    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    .line 16
    move-object p4, v1

    check-cast p4, Lcom/swedbank/mobile/business/util/e;

    :cond_3
    move-object v3, p4

    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_4

    .line 17
    move-object p5, v1

    check-cast p5, Lcom/swedbank/mobile/app/w/b;

    :cond_4
    move-object p6, p5

    move-object p1, p0

    move p2, p7

    move-object p3, v2

    move p4, v0

    move-object p5, v3

    invoke-direct/range {p1 .. p6}, Lcom/swedbank/mobile/app/services/p;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/swedbank/mobile/app/services/p;->a:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/swedbank/mobile/app/services/p;->c:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/swedbank/mobile/app/services/p;->a(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)Lcom/swedbank/mobile/app/services/p;
    .locals 7
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/w/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/app/plugins/list/b;",
            "Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/app/w/b;",
            ")",
            "Lcom/swedbank/mobile/app/services/p;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v6, Lcom/swedbank/mobile/app/services/p;

    move-object v0, v6

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/services/p;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)V

    return-object v6
.end method

.method public final a()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/services/p;->a:Z

    return v0
.end method

.method public final b()Lcom/swedbank/mobile/app/plugins/list/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/services/p;->c:Z

    return v0
.end method

.method public final d()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public final e()Lcom/swedbank/mobile/app/w/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lcom/swedbank/mobile/app/services/p;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/app/services/p;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/services/p;->a:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/services/p;->a:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    iget-object v3, p1, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/services/p;->c:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/services/p;->c:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    iget-object v3, p1, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    iget-object p1, p1, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/swedbank/mobile/app/services/p;->a:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/services/p;->c:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_4
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServicesViewState(loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/services/p;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", firstDataQuerySuccessful="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/services/p;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", dataQueryError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->d:Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/p;->e:Lcom/swedbank/mobile/app/w/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
