.class public final Lcom/swedbank/mobile/app/services/c;
.super Ljava/lang/Object;
.source "ServicesBuilder_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/services/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/a$a;",
            ">;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/app/services/c;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/services/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/aa/a$a;",
            ">;)",
            "Lcom/swedbank/mobile/app/services/c;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/swedbank/mobile/app/services/c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/services/c;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/services/b;
    .locals 2

    .line 17
    new-instance v0, Lcom/swedbank/mobile/app/services/b;

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/c;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/a/aa/a$a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/services/b;-><init>(Lcom/swedbank/mobile/a/aa/a$a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/services/c;->a()Lcom/swedbank/mobile/app/services/b;

    move-result-object v0

    return-object v0
.end method
