.class final synthetic Lcom/swedbank/mobile/app/services/e$c;
.super Lkotlin/e/b/i;
.source "ServicesPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/services/e;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/services/p;",
        "Lcom/swedbank/mobile/app/services/p$a;",
        "Lcom/swedbank/mobile/app/services/p;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/services/e;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/services/p;Lcom/swedbank/mobile/app/services/p$a;)Lcom/swedbank/mobile/app/services/p;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/services/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/services/p$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/e$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/services/e;

    .line 139
    instance-of v0, p2, Lcom/swedbank/mobile/app/services/p$a$a;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 140
    new-instance v3, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 141
    check-cast p2, Lcom/swedbank/mobile/app/services/p$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$a;->a()Ljava/util/List;

    move-result-object v0

    .line 142
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$a;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object v1

    .line 143
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$a;->c()Z

    move-result p2

    .line 140
    invoke-direct {v3, v0, v1, p2}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v1, p1

    .line 139
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    goto/16 :goto_0

    .line 144
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/services/p$a$f;

    if-eqz v0, :cond_3

    .line 146
    check-cast p2, Lcom/swedbank/mobile/app/services/p$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/p;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_0

    .line 147
    :cond_1
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$f;->a()Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p1

    .line 151
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    goto/16 :goto_0

    .line 154
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/services/p$a$c;->a:Lcom/swedbank/mobile/app/services/p$a$c;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 155
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/p;->c()Z

    move-result p2

    if-eqz p2, :cond_4

    goto :goto_0

    :cond_4
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1a

    const/4 v7, 0x0

    move-object v0, p1

    .line 160
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    goto :goto_0

    .line 164
    :cond_5
    instance-of v0, p2, Lcom/swedbank/mobile/app/services/p$a$b;

    if-eqz v0, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 165
    check-cast p2, Lcom/swedbank/mobile/app/services/p$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    move-object v1, p1

    .line 164
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    goto :goto_0

    .line 167
    :cond_6
    instance-of v0, p2, Lcom/swedbank/mobile/app/services/p$a$e;

    if-eqz v0, :cond_7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 168
    check-cast p2, Lcom/swedbank/mobile/app/services/p$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/services/p$a$e;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v6

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v1, p1

    .line 167
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    goto :goto_0

    .line 170
    :cond_7
    sget-object v0, Lcom/swedbank/mobile/app/services/p$a$d;->a:Lcom/swedbank/mobile/app/services/p$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_9

    .line 174
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/services/p;->c()Z

    move-result p2

    if-nez p2, :cond_8

    goto :goto_0

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v0, p1

    .line 175
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/services/p;->a(Lcom/swedbank/mobile/app/services/p;ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    :goto_0
    return-object p1

    .line 170
    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/app/services/p;

    check-cast p2, Lcom/swedbank/mobile/app/services/p$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/services/e$c;->a(Lcom/swedbank/mobile/app/services/p;Lcom/swedbank/mobile/app/services/p$a;)Lcom/swedbank/mobile/app/services/p;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/services/e;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/services/ServicesViewState;Lcom/swedbank/mobile/app/services/ServicesViewState$PartialState;)Lcom/swedbank/mobile/app/services/ServicesViewState;"

    return-object v0
.end method
