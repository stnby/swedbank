.class final Lcom/swedbank/mobile/app/services/e$h;
.super Ljava/lang/Object;
.source "ServicesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/services/e;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/services/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/services/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/e$h;->a:Lcom/swedbank/mobile/app/services/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 5
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/services/p$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "forceShowLoading"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/e$h;->a:Lcom/swedbank/mobile/app/services/e;

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/e$h;->a:Lcom/swedbank/mobile/app/services/e;

    invoke-static {v1}, Lcom/swedbank/mobile/app/services/e;->a(Lcom/swedbank/mobile/app/services/e;)Lcom/swedbank/mobile/business/services/g;

    move-result-object v1

    .line 49
    invoke-interface {v1}, Lcom/swedbank/mobile/business/services/g;->a()Lio/reactivex/w;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v1

    .line 51
    new-instance v2, Lcom/swedbank/mobile/app/services/e$h$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/services/e$h$1;-><init>(Lcom/swedbank/mobile/app/services/e$h;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 61
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/services/e$h;->a:Lcom/swedbank/mobile/app/services/e;

    invoke-static {p1}, Lcom/swedbank/mobile/app/services/e;->b(Lcom/swedbank/mobile/app/services/e;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-static {p1, v4, v3, v2}, Lcom/swedbank/mobile/core/ui/x$a;->b(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/services/e$h;->a:Lcom/swedbank/mobile/app/services/e;

    invoke-static {p1}, Lcom/swedbank/mobile/app/services/e;->b(Lcom/swedbank/mobile/app/services/e;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-static {p1, v4, v3, v2}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 60
    :goto_0
    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v1, p1}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v1, "interactor\n             \u2026oading()\n              })"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    new-instance v1, Lcom/swedbank/mobile/app/services/e$j;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/services/e$j;-><init>(Lcom/swedbank/mobile/app/services/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/services/e$h;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
