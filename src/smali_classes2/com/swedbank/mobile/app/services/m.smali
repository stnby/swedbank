.class public final Lcom/swedbank/mobile/app/services/m;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ServicesViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/services/l;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lcom/swedbank/mobile/business/navigation/i;

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenLoadingView"

    const-string v4, "getFullScreenLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "servicesList"

    const-string v4, "getServicesList()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/services/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/navigation/i;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/navigation/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_services_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/navigation/i;",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "navigationItemReselectStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProviders"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->j:Lcom/swedbank/mobile/business/navigation/i;

    iput-object p2, p0, Lcom/swedbank/mobile/app/services/m;->k:Ljava/util/List;

    .line 40
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->c:Lkotlin/f/c;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->services_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->d:Lkotlin/f/c;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->services_refresh:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->e:Lkotlin/f/c;

    .line 43
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->services_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->f:Lkotlin/f/c;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->services_retry_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->g:Lkotlin/f/c;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->services_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->h:Lkotlin/f/c;

    .line 46
    sget p1, Lcom/swedbank/mobile/app/services/a$d;->services_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->i:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/services/m;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/services/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/services/m;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->f()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;
    .locals 0

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/services/m;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/services/m;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->k()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/services/m;)Landroid/widget/TextView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->i()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/services/m;)Landroid/widget/Button;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->j()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    return-object v0
.end method

.method private final i()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final j()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final k()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/services/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 65
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 66
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->j()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 177
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 67
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/services/m;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 64
    invoke-static {v0, v2, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/services/p;)V
    .locals 18
    .param p1    # Lcom/swedbank/mobile/app/services/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const-string v1, "viewState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->f()Landroid/view/View;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->a()Z

    move-result v3

    const/16 v4, 0x8

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    .line 178
    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 74
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->c()Z

    move-result v1

    .line 75
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->b()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v3

    .line 180
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->b(Lcom/swedbank/mobile/app/services/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    const/4 v7, 0x1

    xor-int/2addr v1, v7

    const/4 v8, 0x4

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 181
    :goto_1
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_2

    .line 183
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->a(Lcom/swedbank/mobile/app/services/m;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 77
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->c()Z

    move-result v1

    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez v1, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    .line 191
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->c(Lcom/swedbank/mobile/app/services/m;)Landroid/view/View;

    move-result-object v6

    if-eqz v1, :cond_4

    const/4 v9, 0x0

    goto :goto_3

    :cond_4
    const/16 v9, 0x8

    .line 192
    :goto_3
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 194
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->b(Lcom/swedbank/mobile/app/services/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v6

    if-eqz v1, :cond_5

    .line 197
    invoke-virtual {v6, v5}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 198
    invoke-virtual {v6, v5}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_4

    :cond_5
    if-eqz v3, :cond_6

    .line 201
    invoke-virtual {v6, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 202
    invoke-virtual {v6, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_4

    .line 205
    :cond_6
    invoke-virtual {v6, v5}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 206
    invoke-virtual {v6, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 80
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->c()Z

    move-result v1

    .line 81
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object v3

    .line 82
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/services/p;->e()Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    const/4 v6, 0x0

    if-eqz v1, :cond_c

    if-eqz v2, :cond_7

    .line 227
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context().getString(fata\u2026ror.userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 228
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/swedbank/mobile/app/services/a$f;->retry_btn:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 226
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/app/services/m;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto/16 :goto_7

    :cond_7
    if-eqz v3, :cond_b

    .line 230
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v1

    .line 231
    check-cast v6, Ljava/lang/Integer;

    .line 234
    instance-of v2, v3, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_9

    check-cast v3, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 236
    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_8

    move-object v4, v2

    check-cast v4, Ljava/lang/Iterable;

    const-string v1, "\n"

    move-object v5, v1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 237
    :cond_8
    sget v2, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 239
    :cond_9
    instance-of v2, v3, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_a

    check-cast v3, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    .line 240
    invoke-static {v2}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_5
    const-string v2, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 241
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 243
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/swedbank/mobile/app/services/a$f;->retry_btn:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 229
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/app/services/m;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto/16 :goto_7

    .line 240
    :cond_a
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 244
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    goto/16 :goto_7

    .line 248
    :cond_c
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v2, :cond_d

    .line 251
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 252
    sget v3, Lcom/swedbank/mobile/app/services/a$f;->retry_btn:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 266
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->b(Lcom/swedbank/mobile/app/services/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setVisibility(I)V

    .line 267
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->e(Lcom/swedbank/mobile/app/services/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 268
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->f(Lcom/swedbank/mobile/app/services/m;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->g(Lcom/swedbank/mobile/app/services/m;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    :cond_d
    if-eqz v3, :cond_11

    .line 274
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->d(Lcom/swedbank/mobile/app/services/m;)Landroid/content/Context;

    move-result-object v2

    .line 275
    check-cast v6, Ljava/lang/Integer;

    .line 278
    instance-of v4, v3, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v4, :cond_f

    check-cast v3, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 280
    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v4, v7

    if-eqz v4, :cond_e

    move-object v9, v3

    check-cast v9, Ljava/lang/Iterable;

    const-string v2, "\n"

    move-object v10, v2

    check-cast v10, Ljava/lang/CharSequence;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x3e

    const/16 v17, 0x0

    invoke-static/range {v9 .. v17}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 281
    :cond_e
    sget v3, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 283
    :cond_f
    instance-of v4, v3, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v4, :cond_10

    check-cast v3, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    .line 284
    invoke-static {v3}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    const-string v3, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 285
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 287
    sget v3, Lcom/swedbank/mobile/app/services/a$f;->retry_btn:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 298
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->b(Lcom/swedbank/mobile/app/services/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setVisibility(I)V

    .line 299
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->e(Lcom/swedbank/mobile/app/services/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 300
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->f(Lcom/swedbank/mobile/app/services/m;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->g(Lcom/swedbank/mobile/app/services/m;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 284
    :cond_10
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 311
    :cond_11
    check-cast v6, Ljava/lang/CharSequence;

    .line 320
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/services/m;->e(Lcom/swedbank/mobile/app/services/m;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_7
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/swedbank/mobile/app/services/m;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/swedbank/mobile/app/services/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/services/m;->a(Lcom/swedbank/mobile/app/services/p;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 8

    .line 172
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 173
    new-instance v1, Lcom/swedbank/mobile/app/services/m$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/services/m$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/services/n;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/services/n;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 174
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/services/m;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 52
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x2

    .line 53
    new-array v1, v1, [I

    sget v2, Lcom/swedbank/mobile/app/services/a$a;->brand_orange:I

    const/4 v3, 0x0

    aput v2, v1, v3

    sget v2, Lcom/swedbank/mobile/app/services/a$a;->brand_turqoise:I

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 54
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewStartOffset()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v2

    float-to-int v2, v4

    invoke-virtual {v0, v3, v1, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZII)V

    .line 56
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/services/m;->k:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Ljava/util/List;)V

    .line 57
    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->c()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/services/m;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/m;->j:Lcom/swedbank/mobile/business/navigation/i;

    const-string v1, "feature_services"

    .line 59
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/navigation/i;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v2

    .line 60
    new-instance v0, Lcom/swedbank/mobile/app/services/m$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/services/m$b;-><init>(Lcom/swedbank/mobile/app/services/m;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 175
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void
.end method
