.class public final Lcom/swedbank/mobile/app/services/b/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "NotAuthServicesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/services/b/g;",
        "Lcom/swedbank/mobile/app/services/b/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/services/notauth/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/services/notauth/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/services/notauth/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/b/c;->a:Lcom/swedbank/mobile/business/services/notauth/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/services/b/c;)Lcom/swedbank/mobile/business/services/notauth/a;
    .locals 0

    .line 7
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/b/c;->a:Lcom/swedbank/mobile/business/services/notauth/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .line 12
    sget-object v0, Lcom/swedbank/mobile/app/services/b/c$a;->a:Lcom/swedbank/mobile/app/services/b/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/services/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 13
    new-instance v1, Lcom/swedbank/mobile/app/services/b/c$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/services/b/c$b;-><init>(Lcom/swedbank/mobile/app/services/b/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "action(NotAuthServicesVi\u2026tAuthServicesViewState>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
