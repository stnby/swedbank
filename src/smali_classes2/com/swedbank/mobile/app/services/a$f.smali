.class public final Lcom/swedbank/mobile/app/services/a$f;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/services/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f110000

.field public static final abc_action_bar_up_description:I = 0x7f110001

.field public static final abc_action_menu_overflow_description:I = 0x7f110002

.field public static final abc_action_mode_done:I = 0x7f110003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f110004

.field public static final abc_activitychooserview_choose_application:I = 0x7f110005

.field public static final abc_capital_off:I = 0x7f110006

.field public static final abc_capital_on:I = 0x7f110007

.field public static final abc_font_family_body_1_material:I = 0x7f110008

.field public static final abc_font_family_body_2_material:I = 0x7f110009

.field public static final abc_font_family_button_material:I = 0x7f11000a

.field public static final abc_font_family_caption_material:I = 0x7f11000b

.field public static final abc_font_family_display_1_material:I = 0x7f11000c

.field public static final abc_font_family_display_2_material:I = 0x7f11000d

.field public static final abc_font_family_display_3_material:I = 0x7f11000e

.field public static final abc_font_family_display_4_material:I = 0x7f11000f

.field public static final abc_font_family_headline_material:I = 0x7f110010

.field public static final abc_font_family_menu_material:I = 0x7f110011

.field public static final abc_font_family_subhead_material:I = 0x7f110012

.field public static final abc_font_family_title_material:I = 0x7f110013

.field public static final abc_menu_alt_shortcut_label:I = 0x7f110014

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f110015

.field public static final abc_menu_delete_shortcut_label:I = 0x7f110016

.field public static final abc_menu_enter_shortcut_label:I = 0x7f110017

.field public static final abc_menu_function_shortcut_label:I = 0x7f110018

.field public static final abc_menu_meta_shortcut_label:I = 0x7f110019

.field public static final abc_menu_shift_shortcut_label:I = 0x7f11001a

.field public static final abc_menu_space_shortcut_label:I = 0x7f11001b

.field public static final abc_menu_sym_shortcut_label:I = 0x7f11001c

.field public static final abc_prepend_shortcut_label:I = 0x7f11001d

.field public static final abc_search_hint:I = 0x7f11001e

.field public static final abc_searchview_description_clear:I = 0x7f11001f

.field public static final abc_searchview_description_query:I = 0x7f110020

.field public static final abc_searchview_description_search:I = 0x7f110021

.field public static final abc_searchview_description_submit:I = 0x7f110022

.field public static final abc_searchview_description_voice:I = 0x7f110023

.field public static final abc_shareactionprovider_share_with:I = 0x7f110024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f110025

.field public static final abc_toolbar_collapse_description:I = 0x7f110026

.field public static final appbar_scrolling_view_behavior:I = 0x7f11002d

.field public static final bottom_sheet_behavior:I = 0x7f110055

.field public static final character_counter_content_description:I = 0x7f1100df

.field public static final character_counter_pattern:I = 0x7f1100e0

.field public static final common_google_play_services_enable_button:I = 0x7f1100e9

.field public static final common_google_play_services_enable_text:I = 0x7f1100ea

.field public static final common_google_play_services_enable_title:I = 0x7f1100eb

.field public static final common_google_play_services_install_button:I = 0x7f1100ec

.field public static final common_google_play_services_install_text:I = 0x7f1100ed

.field public static final common_google_play_services_install_title:I = 0x7f1100ee

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1100ef

.field public static final common_google_play_services_notification_ticker:I = 0x7f1100f0

.field public static final common_google_play_services_unknown_issue:I = 0x7f1100f1

.field public static final common_google_play_services_unsupported_text:I = 0x7f1100f2

.field public static final common_google_play_services_update_button:I = 0x7f1100f3

.field public static final common_google_play_services_update_text:I = 0x7f1100f4

.field public static final common_google_play_services_update_title:I = 0x7f1100f5

.field public static final common_google_play_services_updating_text:I = 0x7f1100f6

.field public static final common_google_play_services_wear_update_text:I = 0x7f1100f7

.field public static final common_open_on_phone:I = 0x7f1100f8

.field public static final common_signin_button_text:I = 0x7f1100f9

.field public static final common_signin_button_text_long:I = 0x7f1100fa

.field public static final contact_method_appointment_description:I = 0x7f1100fb

.field public static final contact_method_appointment_name:I = 0x7f1100fc

.field public static final contact_method_call_name:I = 0x7f1100fd

.field public static final contact_method_feedback_action_body:I = 0x7f1100fe

.field public static final contact_method_feedback_action_subject:I = 0x7f1100ff

.field public static final contact_method_feedback_name:I = 0x7f110100

.field public static final contact_method_map_description:I = 0x7f110101

.field public static final contact_method_map_name:I = 0x7f110102

.field public static final contact_method_skype_name:I = 0x7f110103

.field public static final contact_method_write_name:I = 0x7f110104

.field public static final contact_separator_feedback_title:I = 0x7f110105

.field public static final contact_separator_general_title:I = 0x7f110106

.field public static final contact_tab_business:I = 0x7f110107

.field public static final contact_tab_private:I = 0x7f110108

.field public static final contact_toolbar_title:I = 0x7f110109

.field public static final error_general_error:I = 0x7f110125

.field public static final error_no_internet:I = 0x7f110126

.field public static final error_server_error:I = 0x7f110127

.field public static final error_server_maintenance:I = 0x7f110128

.field public static final fab_transformation_scrim_behavior:I = 0x7f11012b

.field public static final fab_transformation_sheet_behavior:I = 0x7f11012c

.field public static final fcm_fallback_notification_channel_label:I = 0x7f11012d

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f11013c

.field public static final login_loading_text:I = 0x7f11014a

.field public static final login_welcome:I = 0x7f110155

.field public static final menu_action_message_center:I = 0x7f11015d

.field public static final menu_action_profile:I = 0x7f11015e

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f11015f

.field public static final navigation_title_contacts:I = 0x7f110161

.field public static final onboarding_cancel:I = 0x7f11016d

.field public static final onboarding_done:I = 0x7f11016e

.field public static final onboarding_intro_action:I = 0x7f11016f

.field public static final onboarding_intro_skip_tour:I = 0x7f110170

.field public static final onboarding_intro_text:I = 0x7f110171

.field public static final onboarding_intro_title:I = 0x7f110172

.field public static final onboarding_text:I = 0x7f110173

.field public static final onboarding_title:I = 0x7f110174

.field public static final onboarding_tour_cancel:I = 0x7f110177

.field public static final onboarding_tour_finish:I = 0x7f110178

.field public static final onboarding_tour_next:I = 0x7f110179

.field public static final password_toggle_content_description:I = 0x7f1101a5

.field public static final path_password_eye:I = 0x7f1101a6

.field public static final path_password_eye_mask_strike_through:I = 0x7f1101a7

.field public static final path_password_eye_mask_visible:I = 0x7f1101a8

.field public static final path_password_strike_through:I = 0x7f1101a9

.field public static final recurring_login_alternative_login_btn:I = 0x7f1101f2

.field public static final recurring_login_welcome:I = 0x7f1101f3

.field public static final retry_btn:I = 0x7f1101f4

.field public static final root_reason_alcatel:I = 0x7f1101f5

.field public static final root_reason_bbox:I = 0x7f1101f6

.field public static final root_reason_bin:I = 0x7f1101f7

.field public static final root_reason_cloak_apps:I = 0x7f1101f8

.field public static final root_reason_connection:I = 0x7f1101f9

.field public static final root_reason_dang_apps:I = 0x7f1101fa

.field public static final root_reason_dang_props:I = 0x7f1101fb

.field public static final root_reason_debuggable:I = 0x7f1101fc

.field public static final root_reason_device_files:I = 0x7f1101fd

.field public static final root_reason_general:I = 0x7f1101fe

.field public static final root_reason_memory:I = 0x7f1101ff

.field public static final root_reason_mgm_apps:I = 0x7f110200

.field public static final root_reason_proc:I = 0x7f110201

.field public static final root_reason_stacktrace:I = 0x7f110202

.field public static final root_reason_strings:I = 0x7f110203

.field public static final root_reason_su:I = 0x7f110204

.field public static final root_reason_test_keys:I = 0x7f110205

.field public static final search_menu_title:I = 0x7f110208

.field public static final services_agreements_header:I = 0x7f110209

.field public static final services_ibank_car_leasing_description_ee:I = 0x7f11020a

.field public static final services_ibank_car_leasing_description_lt:I = 0x7f11020b

.field public static final services_ibank_car_leasing_description_lv:I = 0x7f11020c

.field public static final services_ibank_car_leasing_ee:I = 0x7f11020d

.field public static final services_ibank_car_leasing_lt:I = 0x7f11020e

.field public static final services_ibank_car_leasing_lv:I = 0x7f11020f

.field public static final services_ibank_car_loan_description_ee:I = 0x7f110210

.field public static final services_ibank_car_loan_description_lt:I = 0x7f110211

.field public static final services_ibank_car_loan_description_lv:I = 0x7f110212

.field public static final services_ibank_car_loan_ee:I = 0x7f110213

.field public static final services_ibank_car_loan_lt:I = 0x7f110214

.field public static final services_ibank_car_loan_lv:I = 0x7f110215

.field public static final services_ibank_casco_insurance_description_ee:I = 0x7f110216

.field public static final services_ibank_casco_insurance_description_lt:I = 0x7f110217

.field public static final services_ibank_casco_insurance_description_lv:I = 0x7f110218

.field public static final services_ibank_casco_insurance_ee:I = 0x7f110219

.field public static final services_ibank_casco_insurance_lt:I = 0x7f11021a

.field public static final services_ibank_casco_insurance_lv:I = 0x7f11021b

.field public static final services_ibank_claim_application_description_ee:I = 0x7f11021c

.field public static final services_ibank_claim_application_description_lt:I = 0x7f11021d

.field public static final services_ibank_claim_application_description_lv:I = 0x7f11021e

.field public static final services_ibank_claim_application_ee:I = 0x7f11021f

.field public static final services_ibank_claim_application_lt:I = 0x7f110220

.field public static final services_ibank_claim_application_lv:I = 0x7f110221

.field public static final services_ibank_credit_cover_description_ee:I = 0x7f110222

.field public static final services_ibank_credit_cover_description_lt:I = 0x7f110223

.field public static final services_ibank_credit_cover_description_lv:I = 0x7f110224

.field public static final services_ibank_credit_cover_ee:I = 0x7f110225

.field public static final services_ibank_credit_cover_lt:I = 0x7f110226

.field public static final services_ibank_credit_cover_lv:I = 0x7f110227

.field public static final services_ibank_currency_rates_description_ee:I = 0x7f110228

.field public static final services_ibank_currency_rates_description_lt:I = 0x7f110229

.field public static final services_ibank_currency_rates_description_lv:I = 0x7f11022a

.field public static final services_ibank_currency_rates_ee:I = 0x7f11022b

.field public static final services_ibank_currency_rates_lt:I = 0x7f11022c

.field public static final services_ibank_currency_rates_lv:I = 0x7f11022d

.field public static final services_ibank_header_general_ee:I = 0x7f11022e

.field public static final services_ibank_header_general_lt:I = 0x7f11022f

.field public static final services_ibank_header_general_lv:I = 0x7f110230

.field public static final services_ibank_header_insurance_ee:I = 0x7f110231

.field public static final services_ibank_header_insurance_lt:I = 0x7f110232

.field public static final services_ibank_header_insurance_lv:I = 0x7f110233

.field public static final services_ibank_header_life_insurance_ee:I = 0x7f110234

.field public static final services_ibank_header_life_insurance_lt:I = 0x7f110235

.field public static final services_ibank_header_life_insurance_lv:I = 0x7f110236

.field public static final services_ibank_header_loans_ee:I = 0x7f110237

.field public static final services_ibank_header_loans_lt:I = 0x7f110238

.field public static final services_ibank_header_loans_lv:I = 0x7f110239

.field public static final services_ibank_home_insurance_description_ee:I = 0x7f11023a

.field public static final services_ibank_home_insurance_description_lt:I = 0x7f11023b

.field public static final services_ibank_home_insurance_description_lv:I = 0x7f11023c

.field public static final services_ibank_home_insurance_ee:I = 0x7f11023d

.field public static final services_ibank_home_insurance_lt:I = 0x7f11023e

.field public static final services_ibank_home_insurance_lv:I = 0x7f11023f

.field public static final services_ibank_home_small_loan_description_ee:I = 0x7f110240

.field public static final services_ibank_home_small_loan_description_lt:I = 0x7f110241

.field public static final services_ibank_home_small_loan_description_lv:I = 0x7f110242

.field public static final services_ibank_home_small_loan_ee:I = 0x7f110243

.field public static final services_ibank_home_small_loan_lt:I = 0x7f110244

.field public static final services_ibank_home_small_loan_lv:I = 0x7f110245

.field public static final services_ibank_life_risk_insurance_description_ee:I = 0x7f110246

.field public static final services_ibank_life_risk_insurance_description_lt:I = 0x7f110247

.field public static final services_ibank_life_risk_insurance_description_lv:I = 0x7f110248

.field public static final services_ibank_life_risk_insurance_ee:I = 0x7f110249

.field public static final services_ibank_life_risk_insurance_lt:I = 0x7f11024a

.field public static final services_ibank_life_risk_insurance_lv:I = 0x7f11024b

.field public static final services_ibank_loan_payment_protection_description_ee:I = 0x7f11024c

.field public static final services_ibank_loan_payment_protection_description_lt:I = 0x7f11024d

.field public static final services_ibank_loan_payment_protection_description_lv:I = 0x7f11024e

.field public static final services_ibank_loan_payment_protection_ee:I = 0x7f11024f

.field public static final services_ibank_loan_payment_protection_lt:I = 0x7f110250

.field public static final services_ibank_loan_payment_protection_lv:I = 0x7f110251

.field public static final services_ibank_mortgage_description_ee:I = 0x7f110252

.field public static final services_ibank_mortgage_description_lt:I = 0x7f110253

.field public static final services_ibank_mortgage_description_lv:I = 0x7f110254

.field public static final services_ibank_mortgage_ee:I = 0x7f110255

.field public static final services_ibank_mortgage_lt:I = 0x7f110256

.field public static final services_ibank_mortgage_lv:I = 0x7f110257

.field public static final services_ibank_private_portfolio_description_ee:I = 0x7f110258

.field public static final services_ibank_private_portfolio_description_lt:I = 0x7f110259

.field public static final services_ibank_private_portfolio_description_lv:I = 0x7f11025a

.field public static final services_ibank_private_portfolio_ee:I = 0x7f11025b

.field public static final services_ibank_private_portfolio_lt:I = 0x7f11025c

.field public static final services_ibank_private_portfolio_lv:I = 0x7f11025d

.field public static final services_ibank_safe_child_description_ee:I = 0x7f11025e

.field public static final services_ibank_safe_child_description_lt:I = 0x7f11025f

.field public static final services_ibank_safe_child_description_lv:I = 0x7f110260

.field public static final services_ibank_safe_child_ee:I = 0x7f110261

.field public static final services_ibank_safe_child_lt:I = 0x7f110262

.field public static final services_ibank_safe_child_lv:I = 0x7f110263

.field public static final services_ibank_safe_pension_fund_description_ee:I = 0x7f110264

.field public static final services_ibank_safe_pension_fund_description_lt:I = 0x7f110265

.field public static final services_ibank_safe_pension_fund_description_lv:I = 0x7f110266

.field public static final services_ibank_safe_pension_fund_ee:I = 0x7f110267

.field public static final services_ibank_safe_pension_fund_lt:I = 0x7f110268

.field public static final services_ibank_safe_pension_fund_lv:I = 0x7f110269

.field public static final services_ibank_small_loan_description_ee:I = 0x7f11026a

.field public static final services_ibank_small_loan_description_lt:I = 0x7f11026b

.field public static final services_ibank_small_loan_description_lv:I = 0x7f11026c

.field public static final services_ibank_small_loan_ee:I = 0x7f11026d

.field public static final services_ibank_small_loan_lt:I = 0x7f11026e

.field public static final services_ibank_small_loan_lv:I = 0x7f11026f

.field public static final services_ibank_traffic_insurance_description_ee:I = 0x7f110270

.field public static final services_ibank_traffic_insurance_description_lt:I = 0x7f110271

.field public static final services_ibank_traffic_insurance_description_lv:I = 0x7f110272

.field public static final services_ibank_traffic_insurance_ee:I = 0x7f110273

.field public static final services_ibank_traffic_insurance_lt:I = 0x7f110274

.field public static final services_ibank_traffic_insurance_lv:I = 0x7f110275

.field public static final services_ibank_travel_insurance_description_ee:I = 0x7f110276

.field public static final services_ibank_travel_insurance_description_lt:I = 0x7f110277

.field public static final services_ibank_travel_insurance_description_lv:I = 0x7f110278

.field public static final services_ibank_travel_insurance_ee:I = 0x7f110279

.field public static final services_ibank_travel_insurance_lt:I = 0x7f11027a

.field public static final services_ibank_travel_insurance_lv:I = 0x7f11027b

.field public static final services_mpos_description:I = 0x7f11027c

.field public static final services_mpos_get_app:I = 0x7f11027d

.field public static final services_mpos_manage:I = 0x7f11027e

.field public static final services_mpos_manage_action_install:I = 0x7f11027f

.field public static final services_mpos_manage_action_uninstall:I = 0x7f110280

.field public static final services_mpos_manage_actions_title:I = 0x7f110281

.field public static final services_mpos_onboarding_open_play_store:I = 0x7f110282

.field public static final services_mpos_onboarding_skip:I = 0x7f110283

.field public static final services_mpos_onboarding_text:I = 0x7f110284

.field public static final services_mpos_onboarding_title:I = 0x7f110285

.field public static final services_mpos_open:I = 0x7f110286

.field public static final services_mpos_title:I = 0x7f110287

.field public static final services_navigation_title:I = 0x7f110288

.field public static final services_not_authenticated_log_in:I = 0x7f110289

.field public static final services_not_authenticated_text:I = 0x7f11028a

.field public static final services_not_authenticated_title:I = 0x7f11028b

.field public static final services_not_authenticated_toolbar_title:I = 0x7f11028c

.field public static final services_toolbar_title:I = 0x7f11028d

.field public static final status_bar_notification_info_overflow:I = 0x7f11028f

.field public static final tag_expand_transition_target:I = 0x7f110294

.field public static final tag_wallet_onboarding_step:I = 0x7f110297
