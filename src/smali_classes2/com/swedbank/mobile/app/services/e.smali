.class public final Lcom/swedbank/mobile/app/services/e;
.super Lcom/swedbank/mobile/architect/a/d;
.source "ServicesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/services/l;",
        "Lcom/swedbank/mobile/app/services/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;

.field private final b:Lcom/swedbank/mobile/core/ui/ad;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/services/g;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/swedbank/mobile/business/services/g;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_services_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/services/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;",
            "Lcom/swedbank/mobile/business/services/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataMappers"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/e;->c:Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/services/e;->d:Lcom/swedbank/mobile/business/services/g;

    .line 25
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/e;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 26
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/services/e;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/services/e;)Lcom/swedbank/mobile/business/services/g;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/e;->d:Lcom/swedbank/mobile/business/services/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/services/e;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/e;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/services/e;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/services/e;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/services/e;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v0

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/services/e$i;->a:Lcom/swedbank/mobile/app/services/e$i;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/services/h;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/services/h;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/services/e;->d:Lcom/swedbank/mobile/business/services/g;

    .line 34
    invoke-interface {v1}, Lcom/swedbank/mobile/business/services/g;->b()Lio/reactivex/o;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/swedbank/mobile/app/services/e;->c:Ljava/util/Map;

    .line 138
    new-instance v3, Lcom/swedbank/mobile/app/services/e$a;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/services/e$a;-><init>(Ljava/util/Map;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 139
    new-instance v2, Lcom/swedbank/mobile/app/services/e$b;

    invoke-direct {v2}, Lcom/swedbank/mobile/app/services/e$b;-><init>()V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "map { it.toMappedData(da\u2026t, hasMoreToLoad) }\n    }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 45
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 46
    sget-object v3, Lcom/swedbank/mobile/app/services/e$f;->a:Lcom/swedbank/mobile/app/services/e$f;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/services/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    sget-object v4, Lcom/swedbank/mobile/app/services/e$g;->a:Lcom/swedbank/mobile/app/services/e$g;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 44
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 47
    new-instance v3, Lcom/swedbank/mobile/app/services/e$h;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/services/e$h;-><init>(Lcom/swedbank/mobile/app/services/e;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 67
    sget-object v3, Lcom/swedbank/mobile/app/services/e$d;->a:Lcom/swedbank/mobile/app/services/e$d;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/services/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 68
    new-instance v4, Lcom/swedbank/mobile/app/services/e$e;

    iget-object v5, p0, Lcom/swedbank/mobile/app/services/e;->d:Lcom/swedbank/mobile/business/services/g;

    invoke-direct {v4, v5}, Lcom/swedbank/mobile/app/services/e$e;-><init>(Lcom/swedbank/mobile/business/services/g;)V

    check-cast v4, Lkotlin/e/a/b;

    new-instance v5, Lcom/swedbank/mobile/app/services/g;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/services/g;-><init>(Lkotlin/e/a/b;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v3, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 69
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 70
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 73
    check-cast v0, Lio/reactivex/s;

    .line 74
    check-cast v1, Lio/reactivex/s;

    .line 75
    check-cast v2, Lio/reactivex/s;

    .line 76
    check-cast v3, Lio/reactivex/s;

    .line 72
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026m,\n        actionsStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v1, Lcom/swedbank/mobile/app/services/p;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/swedbank/mobile/app/services/p;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/services/e$c;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/services/e;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/services/e$c;-><init>(Lcom/swedbank/mobile/app/services/e;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 141
    new-instance v3, Lcom/swedbank/mobile/app/services/f;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/services/f;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 142
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
