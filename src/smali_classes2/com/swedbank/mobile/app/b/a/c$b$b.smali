.class public final Lcom/swedbank/mobile/app/b/a/c$b$b;
.super Lkotlin/e/b/k;
.source "AuthenticatedRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/a/c$b;->a(Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/a/h;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/a/c$b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/a/c$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/a/c$b$b;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/navigation/j;

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/a/c$b$b;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/a/c$b$b;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/app/b/a/c$b;->a:Lcom/swedbank/mobile/app/b/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/a/c;->c(Lcom/swedbank/mobile/app/b/a/c;)Lcom/b/c/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/a/c$b$b;->a(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
