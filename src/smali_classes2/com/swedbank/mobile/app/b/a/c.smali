.class public final Lcom/swedbank/mobile/app/b/a/c;
.super Lcom/swedbank/mobile/business/i/f;
.source "AuthenticatedRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/authenticated/d;


# instance fields
.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/navigation/j;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/app/customer/a;

.field private final g:Lcom/swedbank/mobile/app/navigation/a/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/customer/a;Lcom/swedbank/mobile/app/navigation/a/a;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/customer/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/navigation/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authenticated"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/customer/a;",
            "Lcom/swedbank/mobile/app/navigation/a/a;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "customerBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticatedNavigationBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    .line 29
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/a/c;->f:Lcom/swedbank/mobile/app/customer/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/a/c;->g:Lcom/swedbank/mobile/app/navigation/a/a;

    .line 30
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<NavigationNode>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/a/c;->e:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/a/c;)Lcom/swedbank/mobile/app/customer/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/a/c;->f:Lcom/swedbank/mobile/app/customer/a;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/a/c;Lkotlin/e/a/b;)V
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/a/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/a/c;)Lcom/swedbank/mobile/app/navigation/a/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/a/c;->g:Lcom/swedbank/mobile/app/navigation/a/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/a/c;)Lcom/b/c/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/a/c;->e:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/navigation/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/b/a/c$a;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/app/b/a/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/b/a/c$a;-><init>(Lcom/swedbank/mobile/app/b/a/c;)V

    check-cast v0, Lkotlin/e/a/b;

    new-instance v1, Lcom/swedbank/mobile/app/b/a/f;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/b/a/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v1, Lio/reactivex/m;

    invoke-static {v1}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe\n      .create(this\u2026lectedCustomerNavigation)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
