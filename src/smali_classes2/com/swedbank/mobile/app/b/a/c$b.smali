.class public final Lcom/swedbank/mobile/app/b/a/c$b;
.super Lkotlin/e/b/k;
.source "AuthenticatedRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/b/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/a/c;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/a/c;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/a/c$b;->a:Lcom/swedbank/mobile/app/b/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$withInspectingChildren"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    check-cast p1, Ljava/lang/Iterable;

    .line 75
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 37
    instance-of v1, v1, Lcom/swedbank/mobile/business/customer/f;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 77
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-nez v0, :cond_3

    .line 51
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/a/c$b;->a:Lcom/swedbank/mobile/app/b/a/c;

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/a/c$b;->a:Lcom/swedbank/mobile/app/b/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/a/c;->a(Lcom/swedbank/mobile/app/b/a/c;)Lcom/swedbank/mobile/app/customer/a;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/swedbank/mobile/app/b/a/c$b$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/a/c$b$a;-><init>(Lcom/swedbank/mobile/app/b/a/c$b;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/customer/a;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/customer/a;

    move-result-object v0

    .line 45
    new-instance v1, Lcom/swedbank/mobile/app/b/a/c$b$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/a/c$b$b;-><init>(Lcom/swedbank/mobile/app/b/a/c$b;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/customer/a;->b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/customer/a;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/customer/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    if-eqz v1, :cond_2

    .line 55
    new-instance v1, Lcom/swedbank/mobile/app/b/a/c$b$d;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/a/c$b$d;-><init>(Lio/reactivex/k;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/b/a/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/a/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(emitter::onComplete)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    .line 78
    :cond_2
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 59
    :cond_3
    instance-of p1, v0, Lcom/swedbank/mobile/business/customer/f;

    if-eqz p1, :cond_5

    .line 60
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    if-eqz p1, :cond_4

    .line 61
    check-cast v0, Lcom/swedbank/mobile/business/customer/f;

    new-instance p1, Lcom/swedbank/mobile/app/b/a/c$b$c;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/app/b/a/c$b$c;-><init>(Lcom/swedbank/mobile/app/b/a/c$b;)V

    check-cast p1, Lkotlin/e/a/b;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/customer/f;->a(Lkotlin/e/a/b;)V

    :cond_4
    :goto_1
    return-void

    .line 68
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Customer node is not missing and not instance of CustomerRouter"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/a/c$b;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
