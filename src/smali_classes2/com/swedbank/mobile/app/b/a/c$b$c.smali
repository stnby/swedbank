.class public final Lcom/swedbank/mobile/app/b/a/c$b$c;
.super Lkotlin/e/b/k;
.source "AuthenticatedRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/a/c$b;->a(Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/a/h;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/a/c$b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/a/c$b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/a/c$b$c;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/a/c$b$c;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/a/c$b$c;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    iget-object p1, p1, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/a/c$b$c;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/app/b/a/c$b;->a:Lcom/swedbank/mobile/app/b/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/a/c;->c(Lcom/swedbank/mobile/app/b/a/c;)Lcom/b/c/c;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/b/c/c;->i()Lio/reactivex/j;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/swedbank/mobile/app/b/a/c$b$c$1;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/a/c$b$c;->a:Lcom/swedbank/mobile/app/b/a/c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/app/b/a/c$b;->b:Lio/reactivex/k;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/a/c$b$c$1;-><init>(Lio/reactivex/k;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/b/a/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/a/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/j;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    .line 63
    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Lio/reactivex/b/c;)V

    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/a/c$b$c;->a(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
