.class public final Lcom/swedbank/mobile/app/b/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "AuthenticationRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/n;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/f/a;

.field private final f:Lcom/swedbank/mobile/app/d/a;

.field private final g:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private final h:Lcom/swedbank/mobile/business/challenge/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/app/d/a;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/challenge/d;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authentication_node"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/challenge/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authentication_node"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authentication_node"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/app/d/a;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/business/challenge/d;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "confirmationDialogBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challengeBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challengeListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    .line 29
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/c;->e:Lcom/swedbank/mobile/app/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/c;->f:Lcom/swedbank/mobile/app/d/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/c;->g:Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p4, p0, Lcom/swedbank/mobile/app/b/c;->h:Lcom/swedbank/mobile/business/challenge/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/c;)Lcom/swedbank/mobile/app/d/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/c;->f:Lcom/swedbank/mobile/app/d/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/c;)Lcom/swedbank/mobile/business/challenge/d;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/c;->h:Lcom/swedbank/mobile/business/challenge/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/c;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/c;->e:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/b/c;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/c;->g:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 59
    sget-object v0, Lcom/swedbank/mobile/app/b/c$a;->a:Lcom/swedbank/mobile/app/b/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/challenge/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/challenge/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "challengeInfo"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/swedbank/mobile/app/b/c$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/b/c$c;-><init>(Lcom/swedbank/mobile/app/b/c;Lcom/swedbank/mobile/business/challenge/a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 44
    new-instance v0, Lcom/swedbank/mobile/app/b/c$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/c$d;-><init>(Lcom/swedbank/mobile/app/b/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 61
    sget-object v0, Lcom/swedbank/mobile/app/b/c$b;->a:Lcom/swedbank/mobile/app/b/c$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
