.class public final Lcom/swedbank/mobile/app/b/b/b/a;
.super Lcom/swedbank/mobile/architect/a/d;
.source "MobileIdPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/b/b/b/f;",
        "Lcom/swedbank/mobile/app/b/b/b/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/h;

.field private final b:Lcom/swedbank/mobile/app/b/b/u;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/h;Lcom/swedbank/mobile/app/b/b/u;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/b/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commonUserIdStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/b/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/b/a;)Lcom/swedbank/mobile/app/b/b/u;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/b/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/b/a;)Lcom/swedbank/mobile/business/authentication/login/h;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .line 23
    sget-object v0, Lcom/swedbank/mobile/app/b/b/b/a$i;->a:Lcom/swedbank/mobile/app/b/b/b/a$i;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/b/b/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/swedbank/mobile/app/b/b/b/a$j;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/b/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/b/b/a$j;-><init>(Lcom/swedbank/mobile/app/b/b/u;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/b/b/b/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/b/c;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/b/b/b/a$k;->a:Lcom/swedbank/mobile/app/b/b/b/a$k;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/b/b/b/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    sget-object v3, Lcom/swedbank/mobile/business/authentication/login/m;->c:Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/authentication/login/h;->a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/w;

    move-result-object v2

    .line 34
    new-instance v3, Lcom/swedbank/mobile/app/b/b/b/a$l;

    invoke-direct {v3, p0, v1}, Lcom/swedbank/mobile/app/b/b/b/a$l;-><init>(Lcom/swedbank/mobile/app/b/b/b/a;Lio/reactivex/o;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 54
    new-instance v3, Lcom/swedbank/mobile/app/b/b/b/a$m;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/b/b/b/a$m;-><init>(Lcom/swedbank/mobile/app/b/b/b/a;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 63
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/b/b/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    .line 64
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/b/b/u;->a()Lio/reactivex/o;

    move-result-object v3

    .line 65
    invoke-virtual {v3}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    .line 66
    sget-object v4, Lcom/swedbank/mobile/app/b/b/b/a$n;->a:Lcom/swedbank/mobile/app/b/b/b/a$n;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_0

    new-instance v5, Lcom/swedbank/mobile/app/b/b/b/d;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/b/b/b/d;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_0
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 69
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    .line 70
    sget-object v4, Lcom/swedbank/mobile/app/b/b/b/a$h;->a:Lcom/swedbank/mobile/app/b/b/b/a$h;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_1

    new-instance v5, Lcom/swedbank/mobile/app/b/b/b/d;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/b/b/b/d;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_1
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v1, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 72
    check-cast v3, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    invoke-static {v3, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 74
    sget-object v2, Lcom/swedbank/mobile/app/b/b/b/a$f;->a:Lcom/swedbank/mobile/app/b/b/b/a$f;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/b/b/b/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 75
    new-instance v3, Lcom/swedbank/mobile/app/b/b/b/a$g;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/b/b/b/a$g;-><init>(Lcom/swedbank/mobile/app/b/b/b/a;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 81
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 82
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 84
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/b/b/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    invoke-interface {v3}, Lcom/swedbank/mobile/business/authentication/login/h;->b()Lio/reactivex/o;

    move-result-object v3

    .line 85
    sget-object v4, Lcom/swedbank/mobile/app/b/b/b/a$d;->a:Lcom/swedbank/mobile/app/b/b/b/a$d;

    check-cast v4, Lio/reactivex/c/k;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v3

    .line 86
    sget-object v4, Lcom/swedbank/mobile/app/b/b/b/a$e;->a:Lcom/swedbank/mobile/app/b/b/b/a$e;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 96
    sget-object v4, Lcom/swedbank/mobile/app/b/b/b/a$b;->a:Lcom/swedbank/mobile/app/b/b/b/a$b;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/b/b/b/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 97
    new-instance v5, Lcom/swedbank/mobile/app/b/b/b/a$c;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/b/b/b/a$c;-><init>(Lcom/swedbank/mobile/app/b/b/b/a;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 98
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 99
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    const/4 v5, 0x5

    .line 101
    new-array v5, v5, [Lio/reactivex/s;

    const/4 v6, 0x0

    .line 102
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v5, v6

    const/4 v0, 0x1

    .line 103
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v5, v0

    const/4 v0, 0x2

    .line 104
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v5, v0

    const/4 v0, 0x3

    .line 105
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v5, v0

    const/4 v0, 0x4

    .line 106
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v5, v0

    .line 101
    invoke-static {v5}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026 loginCancellationStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v1, Lcom/swedbank/mobile/app/b/b/b/j;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/b/b/b/j;-><init>(Ljava/lang/String;Ljava/lang/String;ZZILkotlin/e/b/g;)V

    .line 109
    new-instance v2, Lcom/swedbank/mobile/app/b/b/b/a$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/b/b/b/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/b/b/b/a$a;-><init>(Lcom/swedbank/mobile/app/b/b/b/a;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 138
    new-instance v3, Lcom/swedbank/mobile/app/b/b/b/b;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/b/b/b/b;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 139
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
