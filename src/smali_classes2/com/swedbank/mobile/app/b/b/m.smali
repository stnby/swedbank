.class public final Lcom/swedbank/mobile/app/b/b/m;
.super Lcom/swedbank/mobile/architect/a/h;
.source "LoginRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/login/n;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/b/a;

.field private final f:Lcom/swedbank/mobile/app/c/b/a;

.field private final g:Lcom/swedbank/mobile/app/c/a/g;

.field private final h:Lcom/swedbank/mobile/business/authentication/o;

.field private final i:Lcom/swedbank/mobile/business/authentication/j;

.field private final j:Lcom/swedbank/mobile/business/authentication/login/l;

.field private final k:Lcom/swedbank/mobile/business/biometric/authentication/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/a;Lcom/swedbank/mobile/app/c/b/a;Lcom/swedbank/mobile/app/c/a/g;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/c/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/c/a/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "login_authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/authentication/j;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/authentication/login/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/a;",
            "Lcom/swedbank/mobile/app/c/b/a;",
            "Lcom/swedbank/mobile/app/c/a/g;",
            "Lcom/swedbank/mobile/business/authentication/o;",
            "Lcom/swedbank/mobile/business/authentication/j;",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticationBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricLoginBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginAuthenticationStream"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricLoginListener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptListener"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p8, p10, p9}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/m;->e:Lcom/swedbank/mobile/app/b/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/m;->f:Lcom/swedbank/mobile/app/c/b/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/m;->g:Lcom/swedbank/mobile/app/c/a/g;

    iput-object p4, p0, Lcom/swedbank/mobile/app/b/b/m;->h:Lcom/swedbank/mobile/business/authentication/o;

    iput-object p5, p0, Lcom/swedbank/mobile/app/b/b/m;->i:Lcom/swedbank/mobile/business/authentication/j;

    iput-object p6, p0, Lcom/swedbank/mobile/app/b/b/m;->j:Lcom/swedbank/mobile/business/authentication/login/l;

    iput-object p7, p0, Lcom/swedbank/mobile/app/b/b/m;->k:Lcom/swedbank/mobile/business/biometric/authentication/h;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/m;)Lcom/swedbank/mobile/app/c/b/a;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/m;->f:Lcom/swedbank/mobile/app/c/b/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/m;)Lcom/swedbank/mobile/business/authentication/login/l;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/m;->j:Lcom/swedbank/mobile/business/authentication/login/l;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 71
    sget-object v0, Lcom/swedbank/mobile/app/b/b/m$a;->a:Lcom/swedbank/mobile/app/b/b/m$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/e;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "authenticationCredentials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/m;->e:Lcom/swedbank/mobile/app/b/a;

    .line 40
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/m;->h:Lcom/swedbank/mobile/business/authentication/o;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/a;->a(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/app/b/a;

    move-result-object v0

    .line 41
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/b/a;->a(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/app/b/a;

    move-result-object p1

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/m;->i:Lcom/swedbank/mobile/business/authentication/j;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/b/a;->a(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/app/b/a;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 70
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "authenticationResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/swedbank/mobile/app/b/b/m$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/b/b/m$d;-><init>(Lcom/swedbank/mobile/app/b/b/m;Lcom/swedbank/mobile/business/authentication/p;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/b/m;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "customerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/m;->g:Lcom/swedbank/mobile/app/c/a/g;

    .line 50
    new-instance v1, Lcom/swedbank/mobile/app/c/b/c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/c/b/c;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    check-cast v1, Lcom/swedbank/mobile/app/c/a/d;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object p1

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/m;->k:Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/a/g;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 73
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 74
    sget-object v0, Lcom/swedbank/mobile/app/b/b/m$c;->a:Lcom/swedbank/mobile/app/b/b/m$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 76
    sget-object v0, Lcom/swedbank/mobile/app/b/b/m$b;->a:Lcom/swedbank/mobile/app/b/b/m$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
