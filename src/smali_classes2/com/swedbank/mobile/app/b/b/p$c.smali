.class public final Lcom/swedbank/mobile/app/b/b/p$c;
.super Ljava/lang/Object;
.source "KeyboardUtil.kt"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/p;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/Window;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$c;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$c;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/p$c;->c:Landroid/view/Window;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    .line 66
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 68
    iget-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$c;->a:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 71
    iget-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$c;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p3, "decorView.context"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const-string p3, "decorView.context.resources"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 72
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr p2, p1

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    .line 77
    iget-object p3, p0, Lcom/swedbank/mobile/app/b/b/p$c;->b:Landroid/view/View;

    int-to-float p2, p2

    neg-float p2, p2

    invoke-virtual {p3, p2}, Landroid/view/View;->setTranslationY(F)V

    .line 78
    iget-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$c;->b:Landroid/view/View;

    .line 121
    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result p3

    .line 122
    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result p4

    .line 123
    invoke-virtual {p2}, Landroid/view/View;->getPaddingRight()I

    move-result p5

    .line 126
    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 81
    :cond_0
    iget-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$c;->b:Landroid/view/View;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Landroid/view/View;->setTranslationY(F)V

    .line 82
    iget-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$c;->b:Landroid/view/View;

    .line 128
    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result p3

    .line 129
    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result p4

    .line 130
    invoke-virtual {p2}, Landroid/view/View;->getPaddingRight()I

    move-result p5

    .line 133
    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 86
    :goto_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$c;->c:Landroid/view/Window;

    const-string p2, "window"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p2

    iget p2, p2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 p3, 0x10

    if-eq p2, p3, :cond_1

    .line 136
    invoke-virtual {p1, p3}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_1
    return-void
.end method
