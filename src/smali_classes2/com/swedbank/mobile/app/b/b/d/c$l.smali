.class final Lcom/swedbank/mobile/app/b/b/d/c$l;
.super Ljava/lang/Object;
.source "SmartIdPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/d/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/b/d/c;

.field final synthetic b:Lio/reactivex/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/d/c;Lio/reactivex/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/c$l;->a:Lcom/swedbank/mobile/app/b/b/d/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/d/c$l;->b:Lio/reactivex/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/b/b/d/l$a;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->e()Ljava/lang/String;

    move-result-object p1

    .line 35
    sget-object v1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 36
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/d/c$l;->a:Lcom/swedbank/mobile/app/b/b/d/c;

    invoke-static {v2}, Lcom/swedbank/mobile/app/b/b/d/c;->a(Lcom/swedbank/mobile/app/b/b/d/c;)Lcom/swedbank/mobile/app/b/b/u;

    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/b/b/u;->a()Lio/reactivex/o;

    move-result-object v2

    .line 38
    invoke-virtual {v2, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    .line 39
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    const-string v3, "commonUserIdStream\n     \u2026  .distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/b/d/c$l;->b:Lio/reactivex/o;

    .line 41
    invoke-virtual {v3, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    const-string v4, "sharedPersonalCodeStream\u2026  .distinctUntilChanged()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {v1, v2, v3}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 43
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v1

    .line 44
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    if-eqz p1, :cond_0

    move-object v2, p1

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 46
    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    new-instance v3, Lkotlin/p;

    invoke-direct {v3, v0, v2, p1}, Lkotlin/p;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    sget-object p1, Lcom/swedbank/mobile/app/b/b/d/c$l$1;->a:Lcom/swedbank/mobile/app/b/b/d/c$l$1;

    check-cast p1, Lio/reactivex/c/c;

    .line 46
    invoke-virtual {v1, v3, p1}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    .line 54
    new-instance v0, Lcom/swedbank/mobile/app/b/b/d/c$l$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/b/d/c$l$2;-><init>(Lcom/swedbank/mobile/app/b/b/d/c$l;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/d/c$l;->a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
