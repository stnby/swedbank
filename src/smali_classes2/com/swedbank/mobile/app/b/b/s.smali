.class public final Lcom/swedbank/mobile/app/b/b/s;
.super Ljava/lang/Object;
.source "LoginViewImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/b/b/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/s;->a:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/s;->b:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/s;->c:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/swedbank/mobile/app/b/b/s;->d:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/swedbank/mobile/app/b/b/s;->e:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/s;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/b/b/s;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/swedbank/mobile/app/b/b/s;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/b/b/s;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/b/b/p;
    .locals 7

    .line 39
    new-instance v6, Lcom/swedbank/mobile/app/b/b/p;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/s;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/o;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/s;->b:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/s;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/util/l;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/s;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/List;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/s;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/util/l;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/b/b/p;-><init>(Lio/reactivex/o;Ljavax/inject/Provider;Lcom/swedbank/mobile/business/util/l;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;)V

    return-object v6
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/s;->a()Lcom/swedbank/mobile/app/b/b/p;

    move-result-object v0

    return-object v0
.end method
