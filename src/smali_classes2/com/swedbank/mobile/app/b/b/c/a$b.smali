.class final Lcom/swedbank/mobile/app/b/b/c/a$b;
.super Ljava/lang/Object;
.source "PinCalculatorPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/c/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/b/c/a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/c/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/c/a$b;->a:Lcom/swedbank/mobile/app/b/b/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/b/b/c/i$a$b;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->d()Ljava/lang/String;

    move-result-object p1

    .line 57
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 58
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/c/a$b;->a:Lcom/swedbank/mobile/app/b/b/c/a;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/b/c/a;->b(Lcom/swedbank/mobile/app/b/b/c/a;)Lcom/swedbank/mobile/app/b/b/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/b/b/u;->a()Lio/reactivex/o;

    move-result-object v1

    .line 59
    invoke-virtual {v1, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    const-string v1, "commonUserIdStream.obser\u2026  .distinctUntilChanged()"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/c/a$b;->a:Lcom/swedbank/mobile/app/b/b/c/a;

    sget-object v2, Lcom/swedbank/mobile/app/b/b/c/a$b$1;->a:Lcom/swedbank/mobile/app/b/b/c/a$b$1;

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/app/b/b/c/a;->a(Lcom/swedbank/mobile/app/b/b/c/a;Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, ""

    .line 62
    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v2, "action(PinCalculatorView\u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {v0, p1, v1}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    .line 64
    new-instance v0, Lcom/swedbank/mobile/app/b/b/c/a$b$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/b/c/a$b$2;-><init>(Lcom/swedbank/mobile/app/b/b/c/a$b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/c/a$b;->a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
