.class public final Lcom/swedbank/mobile/app/b/b/i;
.super Lcom/swedbank/mobile/architect/a/d;
.source "LoginPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/b/b/o;",
        "Lcom/swedbank/mobile/app/b/b/t;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/ad;

.field private final b:Lcom/swedbank/mobile/business/authentication/login/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/i;->b:Lcom/swedbank/mobile/business/authentication/login/h;

    .line 20
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/i;->a:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/i;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/i;->a:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/i;)Lcom/swedbank/mobile/business/authentication/login/h;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/i;->b:Lcom/swedbank/mobile/business/authentication/login/h;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 13

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/i;->b:Lcom/swedbank/mobile/business/authentication/login/h;

    .line 24
    invoke-interface {v0}, Lcom/swedbank/mobile/business/authentication/login/h;->e()Lio/reactivex/w;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    .line 26
    sget-object v1, Lcom/swedbank/mobile/app/b/b/i$d;->a:Lcom/swedbank/mobile/app/b/b/i$d;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/b/b/k;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/k;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/b/b/i$i;->a:Lcom/swedbank/mobile/app/b/b/i$i;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/b/b/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 29
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 30
    sget-object v4, Lcom/swedbank/mobile/app/b/b/i$j;->a:Lcom/swedbank/mobile/app/b/b/i$j;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v1, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 32
    iget-object v4, p0, Lcom/swedbank/mobile/app/b/b/i;->b:Lcom/swedbank/mobile/business/authentication/login/h;

    .line 33
    invoke-interface {v4}, Lcom/swedbank/mobile/business/authentication/login/h;->b()Lio/reactivex/o;

    move-result-object v4

    .line 34
    sget-object v5, Lcom/swedbank/mobile/app/b/b/i$e;->a:Lcom/swedbank/mobile/app/b/b/i$e;

    check-cast v5, Lio/reactivex/c/k;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v4

    .line 35
    new-instance v5, Lcom/swedbank/mobile/app/b/b/i$f;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/b/b/i$f;-><init>(Lcom/swedbank/mobile/app/b/b/i;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    .line 58
    sget-object v5, Lcom/swedbank/mobile/app/b/b/i$g;->a:Lcom/swedbank/mobile/app/b/b/i$g;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/b/b/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 59
    new-instance v6, Lcom/swedbank/mobile/app/b/b/i$h;

    invoke-direct {v6, p0}, Lcom/swedbank/mobile/app/b/b/i$h;-><init>(Lcom/swedbank/mobile/app/b/b/i;)V

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v5

    .line 64
    sget-object v6, Lcom/swedbank/mobile/app/b/b/i$b;->a:Lcom/swedbank/mobile/app/b/b/i$b;

    check-cast v6, Lkotlin/e/a/b;

    invoke-virtual {p0, v6}, Lcom/swedbank/mobile/app/b/b/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v6

    .line 65
    new-instance v7, Lcom/swedbank/mobile/app/b/b/i$c;

    invoke-direct {v7, p0}, Lcom/swedbank/mobile/app/b/b/i$c;-><init>(Lcom/swedbank/mobile/app/b/b/i;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v6, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v6

    .line 66
    invoke-virtual {v6}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v6

    .line 67
    invoke-virtual {v6}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v6

    const/4 v7, 0x5

    .line 69
    new-array v7, v7, [Lio/reactivex/s;

    const/4 v8, 0x0

    .line 70
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v7, v8

    const/4 v0, 0x1

    .line 71
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v7, v0

    const/4 v0, 0x2

    .line 72
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v7, v0

    const/4 v0, 0x3

    .line 73
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v7, v0

    const/4 v0, 0x4

    .line 74
    check-cast v6, Lio/reactivex/s;

    aput-object v6, v7, v0

    .line 69
    invoke-static {v7}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026    backNavigationStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v1, Lcom/swedbank/mobile/app/b/b/t;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3f

    const/4 v12, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v12}, Lcom/swedbank/mobile/app/b/b/t;-><init>(Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    .line 77
    new-instance v4, Lcom/swedbank/mobile/app/b/b/i$a;

    move-object v5, p0

    check-cast v5, Lcom/swedbank/mobile/app/b/b/i;

    invoke-direct {v4, v5}, Lcom/swedbank/mobile/app/b/b/i$a;-><init>(Lcom/swedbank/mobile/app/b/b/i;)V

    check-cast v4, Lkotlin/e/a/m;

    .line 108
    new-instance v5, Lcom/swedbank/mobile/app/b/b/j;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/b/b/j;-><init>(Lkotlin/e/a/m;)V

    check-cast v5, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v5}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 109
    invoke-virtual {v0, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
