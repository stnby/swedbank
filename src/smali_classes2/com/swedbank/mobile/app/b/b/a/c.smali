.class public final Lcom/swedbank/mobile/app/b/b/a/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "LoginHandlingRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/login/handling/d;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/b/b/b;

.field private final f:Lcom/swedbank/mobile/app/b/a;

.field private final g:Lcom/swedbank/mobile/app/c/b/a;

.field private final h:Lcom/swedbank/mobile/business/authentication/login/l;

.field private final i:Lcom/swedbank/mobile/business/authentication/j;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/b/b;Lcom/swedbank/mobile/app/b/a;Lcom/swedbank/mobile/app/c/b/a;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/business/authentication/j;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/b/b/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/c/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/authentication/login/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login_handling"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/authentication/j;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login_handling"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login_handling"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/b/b;",
            "Lcom/swedbank/mobile/app/b/a;",
            "Lcom/swedbank/mobile/app/c/b/a;",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            "Lcom/swedbank/mobile/business/authentication/j;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loginBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricLoginBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    .line 27
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/a/c;->e:Lcom/swedbank/mobile/app/b/b/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/a/c;->f:Lcom/swedbank/mobile/app/b/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/a/c;->g:Lcom/swedbank/mobile/app/c/b/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/b/b/a/c;->h:Lcom/swedbank/mobile/business/authentication/login/l;

    iput-object p5, p0, Lcom/swedbank/mobile/app/b/b/a/c;->i:Lcom/swedbank/mobile/business/authentication/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "credentials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/a/c;->f:Lcom/swedbank/mobile/app/b/a;

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/a/c;->i:Lcom/swedbank/mobile/business/authentication/j;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/a;->a(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/app/b/a;

    move-result-object v0

    .line 43
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/b/a;->a(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/app/b/a;

    move-result-object p1

    .line 44
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/app/b/a;->a(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/app/b/a;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 56
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/f;Lcom/swedbank/mobile/business/util/l;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/authentication/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/f;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "loggedInCustomerName"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/a/c;->e:Lcom/swedbank/mobile/app/b/b/b;

    .line 32
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/a/c;->h:Lcom/swedbank/mobile/business/authentication/login/l;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/b/b;->a(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/app/b/b/b;

    move-result-object v0

    .line 33
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/b/b/b;->a(Lcom/swedbank/mobile/business/authentication/f;)Lcom/swedbank/mobile/app/b/b/b;

    move-result-object p1

    .line 34
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/app/b/b/b;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/b/b/b;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 55
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/p;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "authenticationResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/a/c;->g:Lcom/swedbank/mobile/app/c/b/a;

    .line 49
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/c/b/a;->a(Lcom/swedbank/mobile/business/authentication/p;)Lcom/swedbank/mobile/app/c/b/a;

    move-result-object p1

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/a/c;->h:Lcom/swedbank/mobile/business/authentication/login/l;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/c/b/a;->a(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/app/c/b/a;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/c/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 57
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method
