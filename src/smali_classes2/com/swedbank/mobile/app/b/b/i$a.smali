.class final synthetic Lcom/swedbank/mobile/app/b/b/i$a;
.super Lkotlin/e/b/i;
.source "LoginPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/b/b/t;",
        "Lcom/swedbank/mobile/app/b/b/t$a;",
        "Lcom/swedbank/mobile/app/b/b/t;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/i;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/app/b/b/t$a;)Lcom/swedbank/mobile/app/b/b/t;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/b/b/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/b/t$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/i$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/b/b/i;

    .line 109
    instance-of v0, p2, Lcom/swedbank/mobile/app/b/b/t$a$g;

    if-eqz v0, :cond_0

    .line 110
    check-cast p2, Lcom/swedbank/mobile/app/b/b/t$a$g;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/t$a$g;->a()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    move-object v0, p1

    .line 109
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    goto/16 :goto_0

    .line 111
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/app/b/b/t$a$b;->a:Lcom/swedbank/mobile/app/b/b/t$a$b;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    goto/16 :goto_0

    .line 113
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/app/b/b/t$a$e;->a:Lcom/swedbank/mobile/app/b/b/t$a$e;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    goto :goto_0

    .line 118
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/b/b/t$a$f;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 120
    check-cast p2, Lcom/swedbank/mobile/app/b/b/t$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/t$a$f;->b()Ljava/lang/String;

    move-result-object v5

    .line 121
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/t$a$f;->a()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x25

    const/4 v9, 0x0

    move-object v1, p1

    .line 118
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    goto :goto_0

    .line 122
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/b/b/t$a$a;->a:Lcom/swedbank/mobile/app/b/b/t$a$a;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3d

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    goto :goto_0

    .line 124
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/b/b/t$a$d;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    .line 125
    check-cast p2, Lcom/swedbank/mobile/app/b/b/t$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/t$a$d;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/16 v8, 0x1d

    const/4 v9, 0x0

    move-object v1, p1

    .line 124
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    goto :goto_0

    .line 127
    :cond_5
    sget-object v0, Lcom/swedbank/mobile/app/b/b/t$a$c;->a:Lcom/swedbank/mobile/app/b/b/t$a$c;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/b/b/t;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/business/authentication/login/m;ZZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/b/b/t;

    check-cast p2, Lcom/swedbank/mobile/app/b/b/t$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/b/b/i$a;->a(Lcom/swedbank/mobile/app/b/b/t;Lcom/swedbank/mobile/app/b/b/t$a;)Lcom/swedbank/mobile/app/b/b/t;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/b/b/i;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/authentication/login/LoginViewState;Lcom/swedbank/mobile/app/authentication/login/LoginViewState$PartialState;)Lcom/swedbank/mobile/app/authentication/login/LoginViewState;"

    return-object v0
.end method
