.class public final Lcom/swedbank/mobile/app/b/b/b;
.super Ljava/lang/Object;
.source "LoginBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/authentication/login/l;

.field private b:Lcom/swedbank/mobile/architect/a/b/c;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/a/c/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/b;->e:Lcom/swedbank/mobile/a/c/b/a$a;

    .line 18
    sget-object p1, Lcom/swedbank/mobile/architect/a/b/c;->b:Lcom/swedbank/mobile/architect/a/b/c;

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/b;->b:Lcom/swedbank/mobile/architect/a/b/c;

    .line 19
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/b;->c:Lcom/swedbank/mobile/business/util/l;

    .line 20
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/b;->d:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/f;)Lcom/swedbank/mobile/app/b/b/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/b;

    .line 31
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/b/b/b;->c:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/app/b/b/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/b;

    .line 23
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/b/b;->a:Lcom/swedbank/mobile/business/authentication/login/l;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/b/b/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/app/b/b/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loggedInCustomerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/b;

    .line 35
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/b/b;->d:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/b;->e:Lcom/swedbank/mobile/a/c/b/a$a;

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/b;->a:Lcom/swedbank/mobile/business/authentication/login/l;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/b/a$a;->b(Lcom/swedbank/mobile/business/authentication/login/l;)Lcom/swedbank/mobile/a/c/b/a$a;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/b;->b:Lcom/swedbank/mobile/architect/a/b/c;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/b/a$a;->b(Lcom/swedbank/mobile/architect/a/b/c;)Lcom/swedbank/mobile/a/c/b/a$a;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/b;->c:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/b/a$a;->d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a$a;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/b;->d:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/b/a$a;->c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a$a;

    move-result-object v0

    .line 45
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/b/a$a;->a()Lcom/swedbank/mobile/a/c/b/a;

    move-result-object v0

    .line 46
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Login node built with no listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
