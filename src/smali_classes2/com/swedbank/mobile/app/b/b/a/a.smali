.class public final Lcom/swedbank/mobile/app/b/b/a/a;
.super Ljava/lang/Object;
.source "LoginHandlingBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/authentication/login/handling/b;

.field private b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/a/c/b/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/b/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/b/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/a/a;->c:Lcom/swedbank/mobile/a/c/b/a/a$a;

    .line 16
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/a/a;->b:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/login/handling/b;)Lcom/swedbank/mobile/app/b/b/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/handling/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/a/a;

    .line 19
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/b/a/a;->a:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/b/b/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/b/b/a/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/b/a/a;

    .line 23
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/b/a/a;->b:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/a/a;->c:Lcom/swedbank/mobile/a/c/b/a/a$a;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/a/a;->a:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/b/a/a$a;->b(Lcom/swedbank/mobile/business/authentication/login/handling/b;)Lcom/swedbank/mobile/a/c/b/a/a$a;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/a/a;->b:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/b/a/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/b/a/a$a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/b/a/a$a;->a()Lcom/swedbank/mobile/a/c/b/a/a;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/b/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build login handling node without a listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
