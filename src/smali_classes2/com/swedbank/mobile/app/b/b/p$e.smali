.class final Lcom/swedbank/mobile/app/b/b/p$e;
.super Lkotlin/e/b/k;
.source "LoginViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/p;->a(Lcom/swedbank/mobile/business/util/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/CharSequence;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/b/p;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/p;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$e;->a:Lcom/swedbank/mobile/app/b/b/p;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/b/b/p$e;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p$e;->a:Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b/p;->d(Lcom/swedbank/mobile/app/b/b/p;)Landroid/widget/TextView;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    check-cast v0, Landroid/view/View;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/16 v3, 0x8

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const/16 p1, 0x8

    .line 197
    :goto_1
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$e;->a:Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/b/p;->e(Lcom/swedbank/mobile/app/b/b/p;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$e;->a:Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/b/p;->f(Lcom/swedbank/mobile/app/b/b/p;)Landroid/view/View;

    move-result-object p1

    if-eqz p2, :cond_3

    invoke-static {p2}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_2

    :cond_2
    const/4 p2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p2, 0x1

    :goto_3
    xor-int/2addr p2, v1

    if-eqz p2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v2, 0x8

    .line 199
    :goto_4
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
