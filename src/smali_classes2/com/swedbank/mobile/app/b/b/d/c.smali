.class public final Lcom/swedbank/mobile/app/b/b/d/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "SmartIdPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/b/b/d/h;",
        "Lcom/swedbank/mobile/app/b/b/d/l;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/h;

.field private final b:Lcom/swedbank/mobile/app/b/b/u;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/h;Lcom/swedbank/mobile/app/b/b/u;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/b/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commonUserIdStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/c;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/d/c;->b:Lcom/swedbank/mobile/app/b/b/u;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/d/c;)Lcom/swedbank/mobile/app/b/b/u;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/d/c;->b:Lcom/swedbank/mobile/app/b/b/u;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/d/c;)Lcom/swedbank/mobile/business/authentication/login/h;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/d/c;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 23
    sget-object v0, Lcom/swedbank/mobile/app/b/b/d/c$i;->a:Lcom/swedbank/mobile/app/b/b/d/c$i;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/b/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/swedbank/mobile/app/b/b/d/c$j;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/d/c;->b:Lcom/swedbank/mobile/app/b/b/u;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/b/d/c$j;-><init>(Lcom/swedbank/mobile/app/b/b/u;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/b/b/d/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/d/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/c$k;->a:Lcom/swedbank/mobile/app/b/b/d/c$k;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/b/b/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/d/c;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    sget-object v3, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-interface {v2, v3}, Lcom/swedbank/mobile/business/authentication/login/h;->a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/w;

    move-result-object v2

    .line 34
    new-instance v3, Lcom/swedbank/mobile/app/b/b/d/c$l;

    invoke-direct {v3, p0, v1}, Lcom/swedbank/mobile/app/b/b/d/c$l;-><init>(Lcom/swedbank/mobile/app/b/b/d/c;Lio/reactivex/o;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, "interactor.getSavedCrede\u2026              }\n        }"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/b/d/c;->b:Lcom/swedbank/mobile/app/b/b/u;

    .line 67
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/b/b/u;->a()Lio/reactivex/o;

    move-result-object v3

    .line 68
    invoke-virtual {v3}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    .line 69
    sget-object v4, Lcom/swedbank/mobile/app/b/b/d/c$m;->a:Lcom/swedbank/mobile/app/b/b/d/c$m;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_0

    new-instance v5, Lcom/swedbank/mobile/app/b/b/d/f;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/b/b/d/f;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_0
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 72
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    .line 73
    sget-object v4, Lcom/swedbank/mobile/app/b/b/d/c$h;->a:Lcom/swedbank/mobile/app/b/b/d/c$h;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_1

    new-instance v5, Lcom/swedbank/mobile/app/b/b/d/f;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/b/b/d/f;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_1
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v1, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 75
    check-cast v3, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    invoke-static {v3, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 77
    sget-object v2, Lcom/swedbank/mobile/app/b/b/d/c$f;->a:Lcom/swedbank/mobile/app/b/b/d/c$f;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/b/b/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 78
    new-instance v3, Lcom/swedbank/mobile/app/b/b/d/c$g;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/b/b/d/c$g;-><init>(Lcom/swedbank/mobile/app/b/b/d/c;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 84
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 85
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 87
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/b/d/c;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    .line 88
    invoke-interface {v3}, Lcom/swedbank/mobile/business/authentication/login/h;->b()Lio/reactivex/o;

    move-result-object v3

    .line 89
    sget-object v4, Lcom/swedbank/mobile/app/b/b/d/c$d;->a:Lcom/swedbank/mobile/app/b/b/d/c$d;

    check-cast v4, Lio/reactivex/c/k;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v3

    .line 90
    sget-object v4, Lcom/swedbank/mobile/app/b/b/d/c$e;->a:Lcom/swedbank/mobile/app/b/b/d/c$e;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 100
    sget-object v4, Lcom/swedbank/mobile/app/b/b/d/c$b;->a:Lcom/swedbank/mobile/app/b/b/d/c$b;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/b/b/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 101
    new-instance v5, Lcom/swedbank/mobile/app/b/b/d/c$c;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/b/b/d/c$c;-><init>(Lcom/swedbank/mobile/app/b/b/d/c;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 102
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 103
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    const/4 v5, 0x5

    .line 105
    new-array v5, v5, [Lio/reactivex/s;

    const/4 v6, 0x0

    .line 106
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v5, v6

    const/4 v1, 0x1

    .line 107
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v5, v1

    const/4 v0, 0x2

    .line 108
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v5, v0

    const/4 v0, 0x3

    .line 109
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v5, v0

    const/4 v0, 0x4

    .line 110
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v5, v0

    .line 105
    invoke-static {v5}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026 loginCancellationStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v1, Lcom/swedbank/mobile/app/b/b/d/l;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/swedbank/mobile/app/b/b/d/l;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/e/b/g;)V

    .line 113
    new-instance v2, Lcom/swedbank/mobile/app/b/b/d/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/b/b/d/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/b/b/d/c$a;-><init>(Lcom/swedbank/mobile/app/b/b/d/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 143
    new-instance v3, Lcom/swedbank/mobile/app/b/b/d/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/b/b/d/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 144
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
