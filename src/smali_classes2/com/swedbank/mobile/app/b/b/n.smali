.class public final Lcom/swedbank/mobile/app/b/b/n;
.super Ljava/lang/Object;
.source "LoginRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/b/b/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/j;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/n;->a:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/n;->b:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/n;->c:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/swedbank/mobile/app/b/b/n;->d:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/swedbank/mobile/app/b/b/n;->e:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/swedbank/mobile/app/b/b/n;->f:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/swedbank/mobile/app/b/b/n;->g:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/swedbank/mobile/app/b/b/n;->h:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/swedbank/mobile/app/b/b/n;->i:Ljavax/inject/Provider;

    .line 56
    iput-object p10, p0, Lcom/swedbank/mobile/app/b/b/n;->j:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/n;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/c/a/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/o;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/login/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/biometric/authentication/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/b/b/n;"
        }
    .end annotation

    .line 74
    new-instance v11, Lcom/swedbank/mobile/app/b/b/n;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/swedbank/mobile/app/b/b/n;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/b/b/m;
    .locals 12

    .line 61
    new-instance v11, Lcom/swedbank/mobile/app/b/b/m;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/app/b/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/app/c/b/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/app/c/a/g;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/authentication/o;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/authentication/j;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/authentication/login/l;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/business/biometric/authentication/h;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/architect/business/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/architect/a/b/f;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/n;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/swedbank/mobile/architect/a/b/g;

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/swedbank/mobile/app/b/b/m;-><init>(Lcom/swedbank/mobile/app/b/a;Lcom/swedbank/mobile/app/c/b/a;Lcom/swedbank/mobile/app/c/a/g;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/business/authentication/j;Lcom/swedbank/mobile/business/authentication/login/l;Lcom/swedbank/mobile/business/biometric/authentication/h;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v11
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/n;->a()Lcom/swedbank/mobile/app/b/b/m;

    move-result-object v0

    return-object v0
.end method
