.class public final Lcom/swedbank/mobile/app/b/b/c/a;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PinCalculatorPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/b/b/c/e;",
        "Lcom/swedbank/mobile/app/b/b/c/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/login/h;

.field private final b:Lcom/swedbank/mobile/app/b/b/u;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/h;Lcom/swedbank/mobile/app/b/b/u;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/b/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "commonUserIdStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/c/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/c/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/c/a;)Lcom/swedbank/mobile/business/authentication/login/h;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/c/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/c/a;Lkotlin/e/a/b;)Lio/reactivex/o;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/c/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/c/a;)Lcom/swedbank/mobile/app/b/b/u;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/c/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 23
    sget-object v0, Lcom/swedbank/mobile/app/b/b/c/a$i;->a:Lcom/swedbank/mobile/app/b/b/c/a$i;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/b/c/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/swedbank/mobile/app/b/b/c/a$j;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/c/a;->b:Lcom/swedbank/mobile/app/b/b/u;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/b/c/a$j;-><init>(Lcom/swedbank/mobile/app/b/b/u;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/b/b/c/c;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/c/c;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/a$g;->a:Lcom/swedbank/mobile/app/b/b/c/a$g;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/b/b/c/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 29
    new-instance v2, Lcom/swedbank/mobile/app/b/b/c/a$h;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/b/b/c/a$h;-><init>(Lcom/swedbank/mobile/app/b/b/c/a;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 38
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/c/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/login/h;->b()Lio/reactivex/o;

    move-result-object v2

    .line 39
    sget-object v3, Lcom/swedbank/mobile/app/b/b/c/a$e;->a:Lcom/swedbank/mobile/app/b/b/c/a$e;

    check-cast v3, Lio/reactivex/c/k;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    .line 40
    sget-object v3, Lcom/swedbank/mobile/app/b/b/c/a$f;->a:Lcom/swedbank/mobile/app/b/b/c/a$f;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 50
    sget-object v3, Lcom/swedbank/mobile/app/b/b/c/a$c;->a:Lcom/swedbank/mobile/app/b/b/c/a$c;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/b/b/c/a;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 51
    new-instance v4, Lcom/swedbank/mobile/app/b/b/c/a$d;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/b/b/c/a$d;-><init>(Lcom/swedbank/mobile/app/b/b/c/a;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 52
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 53
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 55
    iget-object v4, p0, Lcom/swedbank/mobile/app/b/b/c/a;->a:Lcom/swedbank/mobile/business/authentication/login/h;

    sget-object v5, Lcom/swedbank/mobile/business/authentication/login/m;->d:Lcom/swedbank/mobile/business/authentication/login/m;

    invoke-interface {v4, v5}, Lcom/swedbank/mobile/business/authentication/login/h;->a(Lcom/swedbank/mobile/business/authentication/login/m;)Lio/reactivex/w;

    move-result-object v4

    .line 56
    new-instance v5, Lcom/swedbank/mobile/app/b/b/c/a$b;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/b/b/c/a$b;-><init>(Lcom/swedbank/mobile/app/b/b/c/a;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v5, "interactor.getSavedCrede\u2026              }\n        }"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x5

    .line 74
    new-array v5, v5, [Lio/reactivex/s;

    .line 75
    check-cast v0, Lio/reactivex/s;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    .line 76
    check-cast v1, Lio/reactivex/s;

    const/4 v0, 0x1

    aput-object v1, v5, v0

    .line 77
    check-cast v2, Lio/reactivex/s;

    const/4 v0, 0x2

    aput-object v2, v5, v0

    .line 78
    check-cast v3, Lio/reactivex/s;

    const/4 v0, 0x3

    aput-object v3, v5, v0

    .line 79
    check-cast v4, Lio/reactivex/s;

    const/4 v0, 0x4

    aput-object v4, v5, v0

    .line 74
    invoke-static {v5}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026ream,\n        formStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v1, Lcom/swedbank/mobile/app/b/b/c/i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/app/b/b/c/i;-><init>(Ljava/lang/CharSequence;ZZILkotlin/e/b/g;)V

    .line 82
    new-instance v2, Lcom/swedbank/mobile/app/b/b/c/a$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/b/b/c/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/b/b/c/a$a;-><init>(Lcom/swedbank/mobile/app/b/b/c/a;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 98
    new-instance v3, Lcom/swedbank/mobile/app/b/b/c/b;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/b/b/c/b;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 99
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
