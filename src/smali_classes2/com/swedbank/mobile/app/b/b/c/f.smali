.class public final Lcom/swedbank/mobile/app/b/b/c/f;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PinCalculatorViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/b/b/c/e;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/c/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userIdLayout"

    const-string v4, "getUserIdLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/c/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userIdView"

    const-string v4, "getUserIdView()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/c/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "pinCodeView"

    const-string v4, "getPinCodeView()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/c/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loginBtn"

    const-string v4, "getLoginBtn()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/c/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/c/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    const v0, 0x7f0a017c

    .line 24
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->b:Lkotlin/f/c;

    const v0, 0x7f0a017b

    .line 25
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->c:Lkotlin/f/c;

    const v0, 0x7f0a0176

    .line 26
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->d:Lkotlin/f/c;

    const v0, 0x7f0a017a

    .line 27
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->e:Lkotlin/f/c;

    const v0, 0x7f0a0179

    .line 28
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->f:Lkotlin/f/c;

    const v0, 0x7f0a0175

    .line 29
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->g:Lkotlin/f/c;

    const/4 v0, 0x1

    .line 31
    iput-boolean v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->h:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/c/f;Z)V
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/b/b/c/f;->h:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/c/f;)Z
    .locals 0

    .line 22
    iget-boolean p0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->h:Z

    return p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/c/f;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->i()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/b/c/f;)Landroid/widget/EditText;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->g()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/b/b/c/f;)Landroid/widget/EditText;
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->h()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final f()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final g()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final h()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final i()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-object v0
.end method

.method private final j()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final k()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/c/f;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->g()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 34
    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/swedbank/mobile/app/b/b/c/f$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/c/f$b;-><init>(Lcom/swedbank/mobile/app/b/b/c/f;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f$c;->a:Lcom/swedbank/mobile/app/b/b/c/f$c;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/b/b/c/g;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/c/g;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "userIdView\n      .textCh\u2026p(CharSequence::toString)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/b/b/c/i;)V
    .locals 6
    .param p1    # Lcom/swedbank/mobile/app/b/b/c/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->f()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/c/i;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 66
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/c/i;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const p1, 0x7f11014a

    .line 105
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->b(Lcom/swedbank/mobile/app/b/b/c/f;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    .line 106
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x4

    .line 107
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 109
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 110
    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 71
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->g()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 72
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->h()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 73
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->j()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 74
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->k()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    :cond_1
    const v0, 0x7f11014f

    .line 79
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/c/i;->c()Z

    move-result v1

    .line 113
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->b(Lcom/swedbank/mobile/app/b/b/c/f;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v4

    .line 114
    move-object v5, v4

    check-cast v5, Landroid/view/View;

    .line 115
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 117
    invoke-virtual {v4, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 118
    invoke-virtual {v4, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 80
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->g()Landroid/widget/EditText;

    move-result-object v0

    .line 81
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 82
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/c/i;->a()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 83
    iput-boolean v3, p0, Lcom/swedbank/mobile/app/b/b/c/f;->h:Z

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/c/i;->a()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    .line 88
    :cond_2
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->h()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 89
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->j()Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 90
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->k()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/app/b/b/c/i;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/c/f;->a(Lcom/swedbank/mobile/app/b/b/c/i;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->h()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 48
    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/swedbank/mobile/app/b/b/c/f$a;->a:Lcom/swedbank/mobile/app/b/b/c/f$a;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/b/b/c/g;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/c/g;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "pinCodeView\n      .textC\u2026p(CharSequence::toString)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->i()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 104
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 57
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->h()Landroid/widget/EditText;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;)Lio/reactivex/o;

    move-result-object v0

    .line 59
    new-instance v2, Lcom/swedbank/mobile/app/b/b/c/f$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/b/b/c/f$d;-><init>(Lcom/swedbank/mobile/app/b/b/c/f;)V

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 55
    invoke-static {v1, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/swedbank/mobile/app/b/b/c/f$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/c/f$e;-><init>(Lcom/swedbank/mobile/app/b/b/c/f;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/swedbank/mobile/app/b/b/c/f$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/c/f$f;-><init>(Lcom/swedbank/mobile/app/b/b/c/f;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .merge(\u2026otView().hideKeyboard() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/c/f;->k()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 103
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
