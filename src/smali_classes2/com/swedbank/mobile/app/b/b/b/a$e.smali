.class final Lcom/swedbank/mobile/app/b/b/b/a$e;
.super Ljava/lang/Object;
.source "MobileIdPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/b/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/b/b/b/a$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/b/b/b/a$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/b/b/b/a$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/b/b/b/a$e;->a:Lcom/swedbank/mobile/app/b/b/b/a$e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/v;)Lcom/swedbank/mobile/app/b/b/b/j$a;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/b/b/b/j$a$b;->a:Lcom/swedbank/mobile/app/b/b/b/j$a$b;

    check-cast p1, Lcom/swedbank/mobile/app/b/b/b/j$a;

    goto :goto_1

    .line 89
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$e;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 90
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$c;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 91
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_0
    sget-object p1, Lcom/swedbank/mobile/app/b/b/b/j$a$a;->a:Lcom/swedbank/mobile/app/b/b/b/j$a$a;

    check-cast p1, Lcom/swedbank/mobile/app/b/b/b/j$a;

    :goto_1
    return-object p1

    .line 92
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No ViewState for LoginState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/authentication/v;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/b/a$e;->a(Lcom/swedbank/mobile/business/authentication/v;)Lcom/swedbank/mobile/app/b/b/b/j$a;

    move-result-object p1

    return-object p1
.end method
