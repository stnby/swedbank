.class public final Lcom/swedbank/mobile/app/b/b/p$f;
.super Landroidx/n/a/b$j;
.source "LoginViewImpl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/b/b/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/b/p;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/google/android/material/tabs/TabLayout;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/b/p;Ljava/util/List;Lcom/google/android/material/tabs/TabLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$f;->a:Lcom/swedbank/mobile/app/b/b/p;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/p$f;->b:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/p$f;->c:Lcom/google/android/material/tabs/TabLayout;

    .line 123
    invoke-direct {p0}, Landroidx/n/a/b$j;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p$f;->a:Lcom/swedbank/mobile/app/b/b/p;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/b/b/p;->o()Landroid/view/View;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    :goto_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_1

    .line 200
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 201
    check-cast v0, Landroid/app/Activity;

    goto :goto_1

    .line 203
    :cond_0
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context.baseContext"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_4

    .line 206
    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    new-instance v1, Landroid/view/View;

    move-object v2, v0

    check-cast v2, Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_2
    const-string v2, "activity.currentFocus ?: View(activity)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    check-cast v0, Landroid/content/Context;

    const-string v2, "input_method"

    .line 211
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 209
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_3

    .line 211
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 126
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p$f;->c:Lcom/google/android/material/tabs/TabLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object p1

    if-eqz p1, :cond_5

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->isSelected()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->select()V

    :cond_5
    return-void
.end method
