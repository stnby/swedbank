.class public final Lcom/swedbank/mobile/app/b/b/d/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "SmartIdViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/b/b/d/h;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private j:Z

.field private k:Z

.field private final l:Landroid/text/InputFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "smartIdLayout"

    const-string v4, "getSmartIdLayout()Landroidx/constraintlayout/widget/ConstraintLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userIdLayout"

    const-string v4, "getUserIdLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userIdView"

    const-string v4, "getUserIdView()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "personalCodeLayout"

    const-string v4, "getPersonalCodeLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "personalCodeView"

    const-string v4, "getPersonalCodeView()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loginBtn"

    const-string v4, "getLoginBtn()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/text/InputFilter;)V
    .locals 1
    .param p1    # Landroid/text/InputFilter;
        .annotation runtime Ljavax/inject/Named;
            value = "personal_code_formatter"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "personalCodeFormatter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->l:Landroid/text/InputFilter;

    const p1, 0x7f0a017e

    .line 29
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->b:Lkotlin/f/c;

    const p1, 0x7f0a0184

    .line 30
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->c:Lkotlin/f/c;

    const p1, 0x7f0a0183

    .line 31
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->d:Lkotlin/f/c;

    const p1, 0x7f0a0182

    .line 32
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->e:Lkotlin/f/c;

    const p1, 0x7f0a0181

    .line 33
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->f:Lkotlin/f/c;

    const p1, 0x7f0a0180

    .line 34
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->g:Lkotlin/f/c;

    const p1, 0x7f0a017f

    .line 35
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->h:Lkotlin/f/c;

    const p1, 0x7f0a017d

    .line 36
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->i:Lkotlin/f/c;

    const/4 p1, 0x1

    .line 38
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->j:Z

    .line 39
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->k:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/d/i;Z)V
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->j:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/d/i;)Z
    .locals 0

    .line 26
    iget-boolean p0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->j:Z

    return p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/d/i;Z)V
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/b/b/d/i;->k:Z

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/d/i;)Z
    .locals 0

    .line 26
    iget-boolean p0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->k:Z

    return p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/b/d/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->k()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/b/b/d/i;)Landroid/widget/EditText;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->h()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/b/b/d/i;)Landroid/widget/EditText;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->j()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/b/b/d/i;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->i()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/b/b/d/i;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->f()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p0

    return-object p0
.end method

.method private final g()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final h()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final i()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final j()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final k()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-object v0
.end method

.method private final p()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final q()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/d/i;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->h()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/swedbank/mobile/app/b/b/d/i$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/d/i$c;-><init>(Lcom/swedbank/mobile/app/b/b/d/i;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i$d;->a:Lcom/swedbank/mobile/app/b/b/d/i$d;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/b/b/d/j;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/d/j;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "userIdView\n      .textCh\u2026p(CharSequence::toString)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/b/b/d/l;)V
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/b/b/d/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->g()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/d/l;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 87
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/d/l;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const p1, 0x7f11014a

    .line 151
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->c(Lcom/swedbank/mobile/app/b/b/d/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    .line 152
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x4

    .line 153
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 155
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 156
    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 92
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->h()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 93
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->j()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 94
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->p()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->q()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_4

    :cond_1
    const v0, 0x7f110151

    .line 100
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/d/l;->d()Z

    move-result v1

    .line 159
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->c(Lcom/swedbank/mobile/app/b/b/d/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v4

    .line 160
    move-object v5, v4

    check-cast v5, Landroid/view/View;

    .line 161
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 163
    invoke-virtual {v4, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 164
    invoke-virtual {v4, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 101
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/d/l;->a()Ljava/lang/String;

    move-result-object v0

    .line 167
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->d(Lcom/swedbank/mobile/app/b/b/d/i;)Landroid/widget/EditText;

    move-result-object v1

    .line 168
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 169
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v2

    if-eqz v4, :cond_2

    .line 170
    invoke-static {p0, v3}, Lcom/swedbank/mobile/app/b/b/d/i;->a(Lcom/swedbank/mobile/app/b/b/d/i;Z)V

    .line 171
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 103
    :cond_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/d/l;->c()Z

    move-result v0

    .line 104
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/d/l;->b()Ljava/lang/String;

    move-result-object p1

    .line 175
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->f(Lcom/swedbank/mobile/app/b/b/d/i;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    .line 176
    move-object v4, v1

    check-cast v4, Landroid/view/View;

    .line 177
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_3

    const/4 v5, 0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_4

    if-eqz v0, :cond_4

    .line 178
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->g(Lcom/swedbank/mobile/app/b/b/d/i;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v5

    new-instance v7, Landroid/animation/LayoutTransition;

    invoke-direct {v7}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v5, v7}, Landroidx/constraintlayout/widget/ConstraintLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    :cond_4
    if-eqz v0, :cond_5

    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const/16 v0, 0x8

    .line 181
    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 184
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 186
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->e(Lcom/swedbank/mobile/app/b/b/d/i;)Landroid/widget/EditText;

    move-result-object v0

    .line 187
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 188
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_7

    .line 189
    invoke-static {p0, v3}, Lcom/swedbank/mobile/app/b/b/d/i;->b(Lcom/swedbank/mobile/app/b/b/d/i;Z)V

    .line 190
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 191
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    .line 105
    :cond_7
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->p()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->q()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v6}, Landroid/widget/Button;->setVisibility(I)V

    :goto_4
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/app/b/b/d/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/d/i;->a(Lcom/swedbank/mobile/app/b/b/d/l;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 59
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->j()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 62
    new-instance v1, Lcom/swedbank/mobile/app/b/b/d/i$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/d/i$a;-><init>(Lcom/swedbank/mobile/app/b/b/d/i;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 71
    sget-object v1, Lcom/swedbank/mobile/app/b/b/d/i$b;->a:Lcom/swedbank/mobile/app/b/b/d/i$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/b/b/d/j;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/d/j;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "personalCodeView\n      .\u2026p(CharSequence::toString)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 77
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->k()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 150
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->j()Landroid/widget/EditText;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;)Lio/reactivex/o;

    move-result-object v0

    .line 80
    new-instance v2, Lcom/swedbank/mobile/app/b/b/d/i$e;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/b/b/d/i$e;-><init>(Lcom/swedbank/mobile/app/b/b/d/i;)V

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 76
    invoke-static {v1, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/swedbank/mobile/app/b/b/d/i$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/d/i$f;-><init>(Lcom/swedbank/mobile/app/b/b/d/i;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/swedbank/mobile/app/b/b/d/i$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/b/d/i$g;-><init>(Lcom/swedbank/mobile/app/b/b/d/i;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .merge(\u2026otView().hideKeyboard() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 73
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->q()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 149
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method protected e()V
    .locals 4

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/d/i;->j()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/d/i;->l:Landroid/text/InputFilter;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;[Landroid/text/InputFilter;)V

    return-void
.end method
