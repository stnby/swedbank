.class final Lcom/swedbank/mobile/app/b/b/i$f;
.super Ljava/lang/Object;
.source "LoginPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/b/i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/i$f;->a:Lcom/swedbank/mobile/app/b/b/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/v;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/b/b/t$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/b/b/t$a$e;->a:Lcom/swedbank/mobile/app/b/b/t$a$e;

    check-cast p1, Lcom/swedbank/mobile/app/b/b/t$a;

    goto :goto_0

    .line 38
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$e;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/v$e;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v$e;->b()Ljava/util/List;

    move-result-object p1

    .line 40
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/d;->a(Ljava/util/List;)Lkotlin/k;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 42
    new-instance v1, Lcom/swedbank/mobile/app/b/b/t$a$f;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/app/b/b/t$a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/app/b/b/t$a;

    goto :goto_0

    .line 46
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/swedbank/mobile/app/b/b/t$a$a;->a:Lcom/swedbank/mobile/app/b/b/t$a$a;

    check-cast p1, Lcom/swedbank/mobile/app/b/b/t$a;

    goto :goto_0

    .line 47
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$c;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/swedbank/mobile/app/b/b/t$a$d;

    check-cast p1, Lcom/swedbank/mobile/business/authentication/v$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v$c;->b()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/b/b/t$a$d;-><init>(Lcom/swedbank/mobile/app/w/b;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/b/b/t$a;

    .line 50
    :goto_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/b/b/t$a$d;

    if-nez v0, :cond_4

    instance-of v0, p1, Lcom/swedbank/mobile/app/b/b/t$a$f;

    if-eqz v0, :cond_3

    goto :goto_1

    .line 55
    :cond_3
    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(viewState)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 51
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/i$f;->a:Lcom/swedbank/mobile/app/b/b/i;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b/i;->a(Lcom/swedbank/mobile/app/b/b/i;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/swedbank/mobile/app/b/b/t$a$c;->a:Lcom/swedbank/mobile/app/b/b/t$a$c;

    const-wide/16 v2, 0x1b58

    .line 51
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/swedbank/mobile/core/ui/ad;->a(Ljava/lang/Object;Ljava/lang/Object;J)Lio/reactivex/o;

    move-result-object p1

    :goto_2
    return-object p1

    .line 48
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No ViewState for LoginState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/authentication/v;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/i$f;->a(Lcom/swedbank/mobile/business/authentication/v;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
