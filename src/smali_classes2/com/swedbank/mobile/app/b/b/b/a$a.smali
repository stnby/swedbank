.class final synthetic Lcom/swedbank/mobile/app/b/b/b/a$a;
.super Lkotlin/e/b/i;
.source "MobileIdPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/b/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/b/b/b/j;",
        "Lcom/swedbank/mobile/app/b/b/b/j$a;",
        "Lcom/swedbank/mobile/app/b/b/b/j;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/b/a;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/b/b/b/j;Lcom/swedbank/mobile/app/b/b/b/j$a;)Lcom/swedbank/mobile/app/b/b/b/j;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/b/b/b/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/b/b/j$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/b/a$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/b/b/b/a;

    .line 139
    instance-of v1, p2, Lcom/swedbank/mobile/app/b/b/b/j$a$d;

    if-eqz v1, :cond_0

    .line 140
    check-cast p2, Lcom/swedbank/mobile/app/b/b/b/j$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$d;->a()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$d;->b()Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$d;->c()Z

    move-result v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    .line 139
    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/b/b/b/j;->a(Lcom/swedbank/mobile/app/b/b/b/j;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/b/j;

    move-result-object p1

    goto/16 :goto_0

    .line 143
    :cond_0
    instance-of v1, p2, Lcom/swedbank/mobile/app/b/b/b/j$a$e;

    if-eqz v1, :cond_1

    .line 144
    check-cast p2, Lcom/swedbank/mobile/app/b/b/b/j$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$e;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 145
    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b/b/a;->b(Lcom/swedbank/mobile/app/b/b/b/a;)Lcom/swedbank/mobile/business/authentication/login/h;

    move-result-object v0

    .line 146
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$e;->a()Ljava/lang/String;

    move-result-object p2

    .line 147
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/b/j;->b()Ljava/lang/String;

    move-result-object v1

    .line 148
    sget-object v4, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    .line 145
    invoke-interface {v0, p2, v1, v4}, Lcom/swedbank/mobile/business/authentication/login/h;->b(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/authentication/login/m;)Z

    move-result v4

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object v1, p1

    .line 143
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/b/b/b/j;->a(Lcom/swedbank/mobile/app/b/b/b/j;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/b/j;

    move-result-object p1

    goto :goto_0

    .line 149
    :cond_1
    instance-of v1, p2, Lcom/swedbank/mobile/app/b/b/b/j$a$c;

    if-eqz v1, :cond_2

    const/4 v3, 0x0

    .line 150
    check-cast p2, Lcom/swedbank/mobile/app/b/b/b/j$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$c;->a()Ljava/lang/String;

    move-result-object v4

    .line 151
    invoke-static {v0}, Lcom/swedbank/mobile/app/b/b/b/a;->b(Lcom/swedbank/mobile/app/b/b/b/a;)Lcom/swedbank/mobile/business/authentication/login/h;

    move-result-object v0

    .line 152
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/b/j;->a()Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/b/b/j$a$c;->a()Ljava/lang/String;

    move-result-object p2

    .line 154
    sget-object v2, Lcom/swedbank/mobile/business/authentication/login/m;->b:Lcom/swedbank/mobile/business/authentication/login/m;

    .line 151
    invoke-interface {v0, v1, p2, v2}, Lcom/swedbank/mobile/business/authentication/login/h;->b(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/authentication/login/m;)Z

    move-result v5

    const/4 v6, 0x0

    const/16 v7, 0x9

    const/4 v8, 0x0

    move-object v2, p1

    .line 149
    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/app/b/b/b/j;->a(Lcom/swedbank/mobile/app/b/b/b/j;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/b/j;

    move-result-object p1

    goto :goto_0

    .line 155
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/app/b/b/b/j$a$b;->a:Lcom/swedbank/mobile/app/b/b/b/j$a$b;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/b/b/b/j;->a(Lcom/swedbank/mobile/app/b/b/b/j;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/b/j;

    move-result-object p1

    goto :goto_0

    .line 157
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/b/b/b/j$a$a;->a:Lcom/swedbank/mobile/app/b/b/b/j$a$a;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/b/b/b/j;->a(Lcom/swedbank/mobile/app/b/b/b/j;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/swedbank/mobile/app/b/b/b/j;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/b/b/b/j;

    check-cast p2, Lcom/swedbank/mobile/app/b/b/b/j$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/b/b/b/a$a;->a(Lcom/swedbank/mobile/app/b/b/b/j;Lcom/swedbank/mobile/app/b/b/b/j$a;)Lcom/swedbank/mobile/app/b/b/b/j;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/b/b/b/a;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/authentication/login/mobileid/MobileIdViewState;Lcom/swedbank/mobile/app/authentication/login/mobileid/MobileIdViewState$PartialState;)Lcom/swedbank/mobile/app/authentication/login/mobileid/MobileIdViewState;"

    return-object v0
.end method
