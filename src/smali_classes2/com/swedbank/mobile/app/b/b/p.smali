.class public final Lcom/swedbank/mobile/app/b/b/p;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "LoginViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/b/b/o;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final m:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "welcomeView"

    const-string v4, "getWelcomeView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "instructionView"

    const-string v4, "getInstructionView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "messageLayout"

    const-string v4, "getMessageLayout()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "messageView"

    const-string v4, "getMessageView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loginPager"

    const-string v4, "getLoginPager()Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/b/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loginTabs"

    const-string v4, "getLoginTabs()Lcom/google/android/material/tabs/TabLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;Ljavax/inject/Provider;Lcom/swedbank/mobile/business/util/l;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "logged_in_customer_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/authentication/login/m;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "greetingFormatterProvider"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggedInCustomerName"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginMethods"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "explanation"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->k:Lio/reactivex/o;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/p;->l:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/b/p;->m:Lcom/swedbank/mobile/business/util/l;

    iput-object p4, p0, Lcom/swedbank/mobile/app/b/b/p;->n:Ljava/util/List;

    iput-object p5, p0, Lcom/swedbank/mobile/app/b/b/p;->o:Lcom/swedbank/mobile/business/util/l;

    const p1, 0x7f0a02dd

    .line 50
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->c:Lkotlin/f/c;

    const p1, 0x7f0a0188

    .line 51
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->d:Lkotlin/f/c;

    const p1, 0x7f0a0169

    .line 52
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->e:Lkotlin/f/c;

    const p1, 0x7f0a016b

    .line 53
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->f:Lkotlin/f/c;

    const p1, 0x7f0a016a

    .line 54
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->g:Lkotlin/f/c;

    const p1, 0x7f0a0174

    .line 55
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->h:Lkotlin/f/c;

    const p1, 0x7f0a0185

    .line 56
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->i:Lkotlin/f/c;

    .line 59
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->j:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/b/p;)Landroid/widget/TextView;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ">;)V"
        }
    .end annotation

    .line 85
    new-instance v0, Lcom/swedbank/mobile/app/b/b/p$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/b/p$e;-><init>(Lcom/swedbank/mobile/app/b/b/p;)V

    .line 94
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/p;->n()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 300
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    const v4, 0x7f110145

    if-eqz v2, :cond_0

    .line 98
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 97
    invoke-virtual {v0, p1, v3}, Lcom/swedbank/mobile/app/b/b/p$e;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 301
    :cond_0
    instance-of v2, p1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_6

    check-cast p1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/f;

    .line 103
    instance-of v2, p1, Lcom/swedbank/mobile/business/authentication/f$b;

    if-eqz v2, :cond_3

    .line 104
    check-cast p1, Lcom/swedbank/mobile/business/authentication/f$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/f$b;->a()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :cond_1
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 106
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/f$b;->b()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_2
    check-cast v3, Ljava/lang/CharSequence;

    .line 103
    invoke-virtual {v0, v2, v3}, Lcom/swedbank/mobile/app/b/b/p$e;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 107
    :cond_3
    instance-of v2, p1, Lcom/swedbank/mobile/business/authentication/f$a;

    if-eqz v2, :cond_5

    .line 108
    check-cast p1, Lcom/swedbank/mobile/business/authentication/f$a;

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/business/authentication/f$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_4

    goto :goto_0

    .line 109
    :cond_4
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 110
    :goto_0
    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/business/authentication/f$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 107
    invoke-virtual {v0, v2, p1}, Lcom/swedbank/mobile/app/b/b/p$e;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_5
    :goto_1
    return-void

    .line 112
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/b/p;)Landroid/content/Context;
    .locals 0

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/p;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/b/p;)Ljavax/inject/Provider;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/p;->l:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/b/b/p;)Landroid/widget/TextView;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->h()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/b/b/p;)Landroid/widget/TextView;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->j()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/b/b/p;)Landroid/view/View;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->i()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/b/b/p;)Lcom/google/android/material/tabs/TabLayout;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->p()Lcom/google/android/material/tabs/TabLayout;

    move-result-object p0

    return-object p0
.end method

.method private final h()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/b/b/p;)Ljava/util/List;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/b/p;->n:Ljava/util/List;

    return-object p0
.end method

.method private final i()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/b/b/p;)Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->k()Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;

    move-result-object p0

    return-object p0
.end method

.method private final j()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final k()Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;

    return-object v0
.end method

.method private final p()Lcom/google/android/material/tabs/TabLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/b/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/tabs/TabLayout;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 167
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/p;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/b/b/t;)V
    .locals 6
    .param p1    # Lcom/swedbank/mobile/app/b/b/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->k()Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;

    move-result-object v0

    .line 175
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/t;->c()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/p;->n:Ljava/util/List;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/t;->a()Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->a(IZ)V

    .line 178
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/t;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->setSwipingEnabled(Z)V

    .line 181
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->p()Lcom/google/android/material/tabs/TabLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/b/t;->b()Z

    move-result v1

    .line 305
    invoke-virtual {v0, v2}, Lcom/google/android/material/tabs/TabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Landroid/view/ViewGroup;

    .line 307
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    :goto_0
    if-ge v2, v3, :cond_1

    .line 308
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "getChildAt(index)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    invoke-virtual {v4, v1}, Landroid/view/View;->setClickable(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/p;->n()Landroid/content/Context;

    move-result-object v0

    .line 185
    check-cast p1, Lcom/swedbank/mobile/app/w/h;

    .line 183
    invoke-static {p0, v0, p1}, Lcom/swedbank/mobile/app/w/d;->a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V

    .line 187
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->j:Lcom/b/c/c;

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void

    .line 305
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 43
    check-cast p1, Lcom/swedbank/mobile/app/b/b/t;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/p;->a(Lcom/swedbank/mobile/app/b/b/t;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->k:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/b/p;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->j:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 9

    .line 197
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 198
    new-instance v1, Lcom/swedbank/mobile/app/b/b/p$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/b/b/p$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/b/b/r;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/b/r;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 199
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/b/p;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->m:Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->a(Lcom/swedbank/mobile/app/b/b/p;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    .line 201
    invoke-static {v1, v2}, Landroidx/core/widget/i;->b(Landroid/widget/TextView;I)V

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 203
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->b(Lcom/swedbank/mobile/app/b/b/p;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f1101f3

    .line 204
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x2

    .line 209
    invoke-static {v0, v7, v3, v8, v3}, Lkotlin/j/n;->a(Ljava/lang/String;CLjava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 208
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->c(Lcom/swedbank/mobile/app/b/b/p;)Ljavax/inject/Provider;

    move-result-object v7

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    const-string v8, "greetingFormatterProvider.get()"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lkotlin/e/a/b;

    .line 210
    invoke-interface {v7, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v6

    .line 203
    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202
    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 211
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->b(Lcom/swedbank/mobile/app/b/b/p;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f110155

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p;->o:Lcom/swedbank/mobile/business/util/l;

    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/b/b/p;->a(Lcom/swedbank/mobile/business/util/l;)V

    .line 214
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->g(Lcom/swedbank/mobile/app/b/b/p;)Lcom/google/android/material/tabs/TabLayout;

    move-result-object v0

    .line 215
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->h(Lcom/swedbank/mobile/app/b/b/p;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 216
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/authentication/login/m;

    .line 219
    sget-object v4, Lcom/swedbank/mobile/app/b/b/q;->a:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 236
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No tab for login method "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/authentication/login/m;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_1
    const v2, 0x7f110150

    const v4, 0x7f0800d4

    goto :goto_2

    :pswitch_2
    const v2, 0x7f11014d

    const v4, 0x7f0800c6

    goto :goto_2

    :pswitch_3
    const v2, 0x7f110153

    const v4, 0x7f0800dd

    goto :goto_2

    :pswitch_4
    const v2, 0x7f110143

    const v4, 0x7f08009c

    .line 238
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout;->newTab()Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v5

    .line 241
    invoke-virtual {v5, v2}, Lcom/google/android/material/tabs/TabLayout$Tab;->setText(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v2

    .line 240
    invoke-virtual {v2, v4}, Lcom/google/android/material/tabs/TabLayout$Tab;->setIcon(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v2

    .line 238
    invoke-virtual {v0, v2}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    goto :goto_1

    .line 246
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->g(Lcom/swedbank/mobile/app/b/b/p;)Lcom/google/android/material/tabs/TabLayout;

    move-result-object v0

    .line 247
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->h(Lcom/swedbank/mobile/app/b/b/p;)Ljava/util/List;

    move-result-object v1

    .line 248
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/b/p;->i(Lcom/swedbank/mobile/app/b/b/p;)Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;

    move-result-object v2

    .line 249
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->setOffscreenPageLimit(I)V

    .line 250
    new-instance v4, Lcom/swedbank/mobile/app/b/b/g;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/app/b/b/g;-><init>(Ljava/util/List;)V

    check-cast v4, Landroidx/n/a/a;

    invoke-virtual {v2, v4}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->setAdapter(Landroidx/n/a/a;)V

    .line 251
    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->getCurrentItem()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/google/android/material/tabs/TabLayout$Tab;->select()V

    .line 252
    :cond_2
    new-instance v4, Lcom/swedbank/mobile/app/b/b/p$f;

    invoke-direct {v4, p0, v1, v0}, Lcom/swedbank/mobile/app/b/b/p$f;-><init>(Lcom/swedbank/mobile/app/b/b/p;Ljava/util/List;Lcom/google/android/material/tabs/TabLayout;)V

    check-cast v4, Landroidx/n/a/b$f;

    invoke-virtual {v2, v4}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->a(Landroidx/n/a/b$f;)V

    .line 258
    new-instance v1, Lcom/swedbank/mobile/app/b/b/p$g;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/b/p$g;-><init>(Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;)V

    check-cast v1, Lcom/google/android/material/tabs/TabLayout$BaseOnTabSelectedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$BaseOnTabSelectedListener;)V

    .line 67
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/p;->o()Landroid/view/View;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "rootView.context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    :goto_3
    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_4

    .line 268
    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_3

    .line 269
    move-object v3, v1

    check-cast v3, Landroid/app/Activity;

    goto :goto_4

    .line 271
    :cond_3
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context.baseContext"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    :goto_4
    if-eqz v3, :cond_6

    .line 274
    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const-string v2, "window"

    .line 275
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const-string v3, "window.decorView"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    new-instance v3, Lcom/swedbank/mobile/app/b/b/p$c;

    invoke-direct {v3, v2, v0, v1}, Lcom/swedbank/mobile/app/b/b/p$c;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/Window;)V

    check-cast v3, Landroid/view/View$OnLayoutChangeListener;

    .line 277
    invoke-virtual {v2, v3}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 287
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 v4, 0x10

    if-eq v0, v4, :cond_5

    .line 288
    invoke-virtual {v1, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 291
    :cond_5
    new-instance v0, Lcom/swedbank/mobile/app/b/b/p$d;

    invoke-direct {v0, v2, v3, v1}, Lcom/swedbank/mobile/app/b/b/p$d;-><init>(Landroid/view/View;Landroid/view/View$OnLayoutChangeListener;Landroid/view/Window;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction {\u2026INPUT_ADJUST_NOTHING)\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_5

    .line 265
    :cond_6
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    :goto_5
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 68
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/p;->o()Landroid/view/View;

    move-result-object v0

    .line 294
    new-instance v1, Lcom/swedbank/mobile/app/b/b/p$b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/b/b/p$b;-><init>(Landroid/view/View;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/app/b/b/r;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/b/b/r;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction(rootView::hideKeyboard)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
