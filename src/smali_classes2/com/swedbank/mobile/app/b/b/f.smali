.class public final Lcom/swedbank/mobile/app/b/b/f;
.super Ljava/lang/Object;
.source "LoginMethodsRegistry_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/b/b/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;>;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/f;->a:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/f;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/b/b/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;>;>;)",
            "Lcom/swedbank/mobile/app/b/b/f;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/swedbank/mobile/app/b/b/f;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/b/b/f;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/b/b/d;
    .locals 3

    .line 24
    new-instance v0, Lcom/swedbank/mobile/app/b/b/d;

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/b/f;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/c/a;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/f;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/g;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/b/b/d;-><init>(Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/architect/business/g;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/b/f;->a()Lcom/swedbank/mobile/app/b/b/d;

    move-result-object v0

    return-object v0
.end method
