.class public final Lcom/swedbank/mobile/app/b/b/p$g;
.super Ljava/lang/Object;
.source "LoginViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/ai;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/b/b/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;)V
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/p$g;->a:Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabReselected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 1
    .param p1    # Lcom/google/android/material/tabs/TabLayout$Tab;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tab"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/ai$a;->a(Lcom/swedbank/mobile/core/ui/ai;Lcom/google/android/material/tabs/TabLayout$Tab;)V

    return-void
.end method

.method public onTabSelected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 2
    .param p1    # Lcom/google/android/material/tabs/TabLayout$Tab;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tab"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/b/p$g;->a:Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;

    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->getPosition()I

    move-result p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/swedbank/mobile/core/ui/widget/ToggleableSwipingViewPager;->a(IZ)V

    return-void
.end method

.method public onTabUnselected(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 1
    .param p1    # Lcom/google/android/material/tabs/TabLayout$Tab;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tab"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/ai$a;->b(Lcom/swedbank/mobile/core/ui/ai;Lcom/google/android/material/tabs/TabLayout$Tab;)V

    return-void
.end method
