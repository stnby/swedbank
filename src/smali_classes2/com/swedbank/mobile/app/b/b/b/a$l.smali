.class final Lcom/swedbank/mobile/app/b/b/b/a$l;
.super Ljava/lang/Object;
.source "MobileIdPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/b/b/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/b/b/a;

.field final synthetic b:Lio/reactivex/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/b/b/a;Lio/reactivex/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/b/b/a$l;->a:Lcom/swedbank/mobile/app/b/b/b/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/b/b/a$l;->b:Lio/reactivex/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/login/s;",
            ")",
            "Lio/reactivex/o<",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/s;->e()Ljava/lang/String;

    move-result-object p1

    .line 35
    sget-object v1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 36
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/b/b/a$l;->a:Lcom/swedbank/mobile/app/b/b/b/a;

    invoke-static {v2}, Lcom/swedbank/mobile/app/b/b/b/a;->a(Lcom/swedbank/mobile/app/b/b/b/a;)Lcom/swedbank/mobile/app/b/b/u;

    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/b/b/u;->a()Lio/reactivex/o;

    move-result-object v2

    .line 38
    invoke-virtual {v2, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    .line 39
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    const-string v3, "commonUserIdStream\n     \u2026  .distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/b/b/a$l;->b:Lio/reactivex/o;

    .line 41
    invoke-virtual {v3, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    const-string v4, "sharedPhoneNrStream\n    \u2026  .distinctUntilChanged()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {v1, v2, v3}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 43
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v1

    .line 44
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 46
    invoke-static {v0, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/app/b/b/b/a$l$1;->a:Lcom/swedbank/mobile/app/b/b/b/a$l$1;

    check-cast v0, Lio/reactivex/c/c;

    invoke-virtual {v1, p1, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/authentication/login/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/b/b/a$l;->a(Lcom/swedbank/mobile/business/authentication/login/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
