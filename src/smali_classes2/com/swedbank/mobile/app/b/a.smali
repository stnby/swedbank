.class public final Lcom/swedbank/mobile/app/b/a;
.super Ljava/lang/Object;
.source "AuthenticationBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/authentication/e;

.field private b:Lcom/swedbank/mobile/business/authentication/o;

.field private c:Lcom/swedbank/mobile/business/authentication/j;

.field private final d:Lcom/swedbank/mobile/a/c/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/a;->d:Lcom/swedbank/mobile/a/c/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/app/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "credentials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/a;

    .line 20
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/a;->a:Lcom/swedbank/mobile/business/authentication/e;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/app/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/a;

    .line 28
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/a;->c:Lcom/swedbank/mobile/business/authentication/j;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/app/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authenticationStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/a;

    .line 24
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/a;->b:Lcom/swedbank/mobile/business/authentication/o;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/a;->d:Lcom/swedbank/mobile/a/c/a$a;

    .line 32
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/a;->a:Lcom/swedbank/mobile/business/authentication/e;

    if-eqz v1, :cond_2

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/a$a;->b(Lcom/swedbank/mobile/business/authentication/e;)Lcom/swedbank/mobile/a/c/a$a;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/a;->b:Lcom/swedbank/mobile/business/authentication/o;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/a$a;->b(Lcom/swedbank/mobile/business/authentication/o;)Lcom/swedbank/mobile/a/c/a$a;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/a;->c:Lcom/swedbank/mobile/business/authentication/j;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/swedbank/mobile/app/b/a$a;

    invoke-direct {v1}, Lcom/swedbank/mobile/app/b/a$a;-><init>()V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/j;

    :goto_0
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/a$a;->b(Lcom/swedbank/mobile/business/authentication/j;)Lcom/swedbank/mobile/a/c/a$a;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/a$a;->a()Lcom/swedbank/mobile/a/c/a;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 35
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build authentication node without authentication stream"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 32
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build authentication node without credentials"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
