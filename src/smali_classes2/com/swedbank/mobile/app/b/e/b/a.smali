.class public final Lcom/swedbank/mobile/app/b/e/b/a;
.super Ljava/lang/Object;
.source "ScopedSessionBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/authentication/session/l;

.field private b:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/a/c/e/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/e/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/e/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/e/b/a;->c:Lcom/swedbank/mobile/a/c/e/b/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/app/b/e/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/b/a;

    .line 20
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/e/b/a;->a:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object v0
.end method

.method public final a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/b/e/b/a;
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/s;",
            ">;)",
            "Lcom/swedbank/mobile/app/b/e/b/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "sessionEstablishedCallback"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/b/a;

    .line 16
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/e/b/a;->b:Lkotlin/e/a/b;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/e/b/a;->c:Lcom/swedbank/mobile/a/c/e/b/a$a;

    .line 24
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/b/a;->b:Lkotlin/e/a/b;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/e/b/a$a;->b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/c/e/b/a$a;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/b/a;->a:Lcom/swedbank/mobile/business/authentication/session/l;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/e/b/a$a;->b(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/a/c/e/b/a$a;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/e/b/a$a;->a()Lcom/swedbank/mobile/a/c/e/b/a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/e/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Scoped session node cannot be built without a listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 24
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Scoped session node cannot be built without session establishment callback"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
