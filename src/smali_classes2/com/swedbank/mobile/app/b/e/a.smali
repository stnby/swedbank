.class public final Lcom/swedbank/mobile/app/b/e/a;
.super Ljava/lang/Object;
.source "SessionBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/swedbank/mobile/business/authentication/session/l;

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/a/c/e/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/e/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/e/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/e/a;->d:Lcom/swedbank/mobile/a/c/e/a$a;

    .line 18
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/e/a;->c:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/g;)Lcom/swedbank/mobile/app/b/e/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/g;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/a;

    .line 29
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/b/e/a;->c:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/app/b/e/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/a;

    .line 25
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/e/a;->b:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object v0
.end method

.method public final a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/b/e/a;
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)",
            "Lcom/swedbank/mobile/app/b/e/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "buildCallback"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/e/a;

    .line 21
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/e/a;->a:Lkotlin/e/a/b;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/e/a;->d:Lcom/swedbank/mobile/a/c/e/a$a;

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/a;->a:Lkotlin/e/a/b;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/e/a$a;->b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/c/e/a$a;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/a;->b:Lcom/swedbank/mobile/business/authentication/session/l;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/e/a$a;->b(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/a/c/e/a$a;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/a;->c:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/e/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/c/e/a$a;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/e/a$a;->a()Lcom/swedbank/mobile/a/c/e/a;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/e/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Session node built with no listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 33
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Session node built with no callback for building nodes for further routing"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
