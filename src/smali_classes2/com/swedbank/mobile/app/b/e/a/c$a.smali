.class final Lcom/swedbank/mobile/app/b/e/a/c$a;
.super Lkotlin/e/b/k;
.source "SessionRefreshRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/e/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/e/a/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/e/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/e/a/c$a;->a:Lcom/swedbank/mobile/app/b/e/a/c;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    check-cast p1, Ljava/lang/Iterable;

    .line 31
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 32
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 30
    instance-of v0, v0, Lcom/swedbank/mobile/business/navigation/m;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 24
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/e/a/c$a;->a:Lcom/swedbank/mobile/app/b/e/a/c;

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/e/a/c$a;->a:Lcom/swedbank/mobile/app/b/e/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/e/a/c;->a(Lcom/swedbank/mobile/app/b/e/a/c;)Lcom/swedbank/mobile/app/navigation/b/a;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/navigation/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 35
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/e/a/c$a;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
