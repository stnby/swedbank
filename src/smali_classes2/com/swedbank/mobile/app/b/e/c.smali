.class public final Lcom/swedbank/mobile/app/b/e/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "SessionRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/session/n;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/b/b/a/a;

.field private final f:Lcom/swedbank/mobile/app/b/e/a/a;

.field private final g:Lcom/swedbank/mobile/a/c/e/b;

.field private final h:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/business/authentication/session/refresh/e;

.field private final j:Lcom/swedbank/mobile/business/authentication/login/handling/b;

.field private final k:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/b/a/a;Lcom/swedbank/mobile/app/b/e/a/a;Lcom/swedbank/mobile/a/c/e/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/authentication/session/refresh/e;Lcom/swedbank/mobile/business/authentication/login/handling/b;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 14
    .param p1    # Lcom/swedbank/mobile/app/b/b/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/a/c/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "session_child_node_build_callback"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/authentication/session/refresh/e;
        .annotation runtime Ljavax/inject/Named;
            value = "for_session"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/authentication/login/handling/b;
        .annotation runtime Ljavax/inject/Named;
            value = "for_session"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_session"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_session"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/b/a/a;",
            "Lcom/swedbank/mobile/app/b/e/a/a;",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/c/e/b;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/session/refresh/e;",
            "Lcom/swedbank/mobile/business/authentication/login/handling/b;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v6, p0

    move-object v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    const-string v0, "loginHandlingBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionRefreshBuilder"

    invoke-static {v8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionModule"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeBuildCallback"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionRefreshListener"

    invoke-static {v11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loginHandlingListener"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationInput"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    move-object/from16 v1, p8

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    move-object/from16 v2, p9

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    .line 37
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object v7, v6, Lcom/swedbank/mobile/app/b/e/c;->e:Lcom/swedbank/mobile/app/b/b/a/a;

    iput-object v8, v6, Lcom/swedbank/mobile/app/b/e/c;->f:Lcom/swedbank/mobile/app/b/e/a/a;

    iput-object v9, v6, Lcom/swedbank/mobile/app/b/e/c;->g:Lcom/swedbank/mobile/a/c/e/b;

    iput-object v10, v6, Lcom/swedbank/mobile/app/b/e/c;->h:Lkotlin/e/a/b;

    iput-object v11, v6, Lcom/swedbank/mobile/app/b/e/c;->i:Lcom/swedbank/mobile/business/authentication/session/refresh/e;

    iput-object v12, v6, Lcom/swedbank/mobile/app/b/e/c;->j:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    iput-object v13, v6, Lcom/swedbank/mobile/app/b/e/c;->k:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/e/c;)Lcom/swedbank/mobile/app/b/e/a/a;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/e/c;->f:Lcom/swedbank/mobile/app/b/e/a/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/e/c;)Lcom/swedbank/mobile/business/authentication/session/refresh/e;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/e/c;->i:Lcom/swedbank/mobile/business/authentication/session/refresh/e;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/e/c;->e:Lcom/swedbank/mobile/app/b/b/a/a;

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/c;->j:Lcom/swedbank/mobile/business/authentication/login/handling/b;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/b/a/a;->a(Lcom/swedbank/mobile/business/authentication/login/handling/b;)Lcom/swedbank/mobile/app/b/b/a/a;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/c;->k:Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/b/a/a;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/b/b/a/a;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/b/b/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 66
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/swedbank/mobile/app/b/e/c$b;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/b/e/c$b;-><init>(Lkotlin/e/a/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/e/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 46
    sget-object v0, Lcom/swedbank/mobile/app/b/e/c$c;->a:Lcom/swedbank/mobile/app/b/e/c$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/e/c;->c(Lkotlin/e/a/b;)V

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/e/c;->h:Lkotlin/e/a/b;

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/e/c;->g:Lcom/swedbank/mobile/a/c/e/b;

    invoke-interface {v0, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 67
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 54
    new-instance v0, Lcom/swedbank/mobile/app/b/e/c$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/e/c$d;-><init>(Lcom/swedbank/mobile/app/b/e/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/e/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 63
    sget-object v0, Lcom/swedbank/mobile/app/b/e/c$a;->a:Lcom/swedbank/mobile/app/b/e/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/e/c;->c(Lkotlin/e/a/b;)V

    return-void
.end method
