.class public final Lcom/swedbank/mobile/app/b/c/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "NotAuthenticatedRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/notauth/e;


# instance fields
.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/navigation/j;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/app/b/c/g;

.field private final g:Lcom/swedbank/mobile/app/navigation/c/a;

.field private final h:Lcom/swedbank/mobile/app/navigation/c/a/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/c/g;Lcom/swedbank/mobile/app/navigation/c/a;Lcom/swedbank/mobile/app/navigation/c/a/a;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/b/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/navigation/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/navigation/c/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/c/g;",
            "Lcom/swedbank/mobile/app/navigation/c/a;",
            "Lcom/swedbank/mobile/app/navigation/c/a/a;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "routeToNotAuthAlternativeLogin"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notAuthenticatedNavigationBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurringAuthNavigationBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    .line 28
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/c/d;->f:Lcom/swedbank/mobile/app/b/c/g;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/c/d;->g:Lcom/swedbank/mobile/app/navigation/c/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/c/d;->h:Lcom/swedbank/mobile/app/navigation/c/a/a;

    .line 29
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<NavigationNode>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/c/d;->e:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/c/d;)Lcom/swedbank/mobile/app/navigation/c/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/c/d;->g:Lcom/swedbank/mobile/app/navigation/c/a;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/c/d;Lkotlin/e/a/b;)V
    .locals 0

    .line 22
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/c/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/c/d;)Lcom/b/c/c;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/c/d;->e:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 31
    new-instance v0, Lcom/swedbank/mobile/app/b/c/d$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/c/d$b;-><init>(Lcom/swedbank/mobile/app/b/c/d;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/c/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/l;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "loggedInCustomerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/c/d;->h:Lcom/swedbank/mobile/app/navigation/c/a/a;

    .line 41
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/navigation/c/a/a;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/navigation/c/a/a;

    move-result-object p1

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/c/d;->f:Lcom/swedbank/mobile/app/b/c/g;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/navigation/c/a/a;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/navigation/c/a/a;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/navigation/c/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/c/d;->e:Lcom/b/c/c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 62
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public b()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/navigation/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    new-instance v0, Lcom/swedbank/mobile/app/b/c/d$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/c/d$a;-><init>(Lcom/swedbank/mobile/app/b/c/d;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026      }\n      }\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
