.class public final Lcom/swedbank/mobile/app/b/c/g;
.super Ljava/lang/Object;
.source "RouteToNotAuthAlternativeLogin.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Lio/reactivex/b/c;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/c/g;->a:Lcom/swedbank/mobile/architect/business/a/c;

    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/b/c;)V
    .locals 4
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/c/g;->a:Lcom/swedbank/mobile/architect/business/a/c;

    new-instance v0, Lcom/swedbank/mobile/business/navigation/b;

    .line 14
    new-instance v1, Lcom/swedbank/mobile/business/authentication/g$c;

    .line 15
    sget-object v2, Lcom/swedbank/mobile/app/b/c/a;->a:Lcom/swedbank/mobile/app/b/c/a;

    check-cast v2, Lcom/swedbank/mobile/business/authentication/f;

    .line 14
    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/authentication/g$c;-><init>(Lcom/swedbank/mobile/business/authentication/f;)V

    check-cast v1, Lcom/swedbank/mobile/business/authentication/g;

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 13
    invoke-direct {v0, v1, v2, v3, v2}, Lcom/swedbank/mobile/business/navigation/b;-><init>(Lcom/swedbank/mobile/business/authentication/g;Ljava/lang/String;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lio/reactivex/b/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/c/g;->a(Lio/reactivex/b/c;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
