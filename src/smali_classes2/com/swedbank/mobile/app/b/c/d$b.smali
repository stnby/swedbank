.class final Lcom/swedbank/mobile/app/b/c/d$b;
.super Lkotlin/e/b/k;
.source "NotAuthenticatedRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/c/d;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/c/d;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/c/d;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/c/d$b;->a:Lcom/swedbank/mobile/app/b/c/d;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 4
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    check-cast p1, Ljava/lang/Iterable;

    .line 62
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 63
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 64
    instance-of v3, v0, Lcom/swedbank/mobile/business/navigation/m;

    if-eqz v3, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/navigation/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/navigation/m;->c()Ljava/lang/String;

    move-result-object v0

    const-string v3, "navigation_not_auth"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_3
    :goto_1
    if-nez v2, :cond_4

    .line 33
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/c/d$b;->a:Lcom/swedbank/mobile/app/b/c/d;

    invoke-static {p1}, Lcom/swedbank/mobile/app/b/c/d;->a(Lcom/swedbank/mobile/app/b/c/d;)Lcom/swedbank/mobile/app/navigation/c/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/navigation/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/c/d$b;->a:Lcom/swedbank/mobile/app/b/c/d;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/c/d;->b(Lcom/swedbank/mobile/app/b/c/d;)Lcom/b/c/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/c/d$b;->a:Lcom/swedbank/mobile/app/b/c/d;

    .line 67
    invoke-static {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_4
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/c/d$b;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
