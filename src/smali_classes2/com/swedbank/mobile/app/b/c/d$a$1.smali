.class final Lcom/swedbank/mobile/app/b/c/d$a$1;
.super Lkotlin/e/b/k;
.source "NotAuthenticatedRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/c/d$a;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/c/d$a;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/c/d$a;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/c/d$a$1;->a:Lcom/swedbank/mobile/app/b/c/d$a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/c/d$a$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    check-cast p1, Ljava/lang/Iterable;

    .line 62
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 50
    instance-of v1, v1, Lcom/swedbank/mobile/business/navigation/m;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 64
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-nez v0, :cond_2

    .line 52
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/c/d$a$1;->b:Lio/reactivex/k;

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/c/d$a$1;->a:Lcom/swedbank/mobile/app/b/c/d$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/b/c/d$a;->a:Lcom/swedbank/mobile/app/b/c/d;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/c/d;->b(Lcom/swedbank/mobile/app/b/c/d;)Lcom/b/c/c;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/b/c/c;->i()Lio/reactivex/j;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/swedbank/mobile/app/b/c/d$a$1$a;

    iget-object v2, p0, Lcom/swedbank/mobile/app/b/c/d$a$1;->b:Lio/reactivex/k;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/b/c/d$a$1$a;-><init>(Lio/reactivex/k;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/b/c/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/c/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/j;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    .line 52
    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Lio/reactivex/b/c;)V

    goto :goto_1

    .line 55
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/c/d$a$1;->b:Lio/reactivex/k;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/c/d$a$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
