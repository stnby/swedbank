.class public final Lcom/swedbank/mobile/app/b/d/e;
.super Lcom/swedbank/mobile/business/i/f;
.source "RecurringLoginRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/recurring/d;
.implements Lcom/swedbank/mobile/business/util/c;


# instance fields
.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lio/reactivex/b/c;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/app/b/d/g;


# direct methods
.method public constructor <init>(Lkotlin/e/a/b;Lcom/swedbank/mobile/app/b/d/g;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "route_to_alternative_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/d/g;
        .annotation runtime Ljavax/inject/Named;
            value = "for_recurring_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_recurring_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_recurring_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lio/reactivex/b/c;",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/app/b/d/g;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "routeToAlternativeLogin"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tab"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p3, p5, p4}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/e;->f:Lkotlin/e/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/d/e;->g:Lcom/swedbank/mobile/app/b/d/g;

    .line 25
    iget-object p1, p0, Lcom/swedbank/mobile/app/b/d/e;->g:Lcom/swedbank/mobile/app/b/d/g;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/d/g;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/e;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b()V
    .locals 3

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/e;->f:Lkotlin/e/a/b;

    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.disposed()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
