.class final synthetic Lcom/swedbank/mobile/app/b/d/b/c$a;
.super Lkotlin/e/b/i;
.source "PollingLoginMethodPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/d/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/b/d/b/m;",
        "Lcom/swedbank/mobile/app/b/d/b/m$a;",
        "Lcom/swedbank/mobile/app/b/d/b/m;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/d/b/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/b/d/b/m;Lcom/swedbank/mobile/app/b/d/b/m$a;)Lcom/swedbank/mobile/app/b/d/b/m;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/b/d/b/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/d/b/m$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/b/d/b/c;

    .line 91
    sget-object v0, Lcom/swedbank/mobile/app/b/d/b/m$a$d;->a:Lcom/swedbank/mobile/app/b/d/b/m$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v0, v0}, Lcom/swedbank/mobile/app/b/d/b/m;->a(ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;)Lcom/swedbank/mobile/app/b/d/b/m;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/app/b/d/b/m$a$a;->a:Lcom/swedbank/mobile/app/b/d/b/m$a$a;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/b/d/b/m;->a(Lcom/swedbank/mobile/app/b/d/b/m;ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/d/b/m;

    move-result-object p1

    goto :goto_0

    .line 98
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/b/d/b/m$a$e;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    .line 100
    check-cast p2, Lcom/swedbank/mobile/app/b/d/b/m$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/d/b/m$a$e;->b()Ljava/lang/String;

    move-result-object v3

    .line 101
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/d/b/m$a$e;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p1

    .line 98
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/b/d/b/m;->a(Lcom/swedbank/mobile/app/b/d/b/m;ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/d/b/m;

    move-result-object p1

    goto :goto_0

    .line 102
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/b/d/b/m$a$c;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 104
    check-cast p2, Lcom/swedbank/mobile/app/b/d/b/m$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/b/d/b/m$a$c;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v1, p1

    .line 102
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/b/d/b/m;->a(Lcom/swedbank/mobile/app/b/d/b/m;ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/d/b/m;

    move-result-object p1

    goto :goto_0

    .line 105
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/b/d/b/m$a$b;->a:Lcom/swedbank/mobile/app/b/d/b/m$a$b;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/b/d/b/m;->a(Lcom/swedbank/mobile/app/b/d/b/m;ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/b/d/b/m;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/app/b/d/b/m;

    check-cast p2, Lcom/swedbank/mobile/app/b/d/b/m$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/b/d/b/c$a;->a(Lcom/swedbank/mobile/app/b/d/b/m;Lcom/swedbank/mobile/app/b/d/b/m$a;)Lcom/swedbank/mobile/app/b/d/b/m;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/b/d/b/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/authentication/recurring/polling/PollingLoginMethodViewState;Lcom/swedbank/mobile/app/authentication/recurring/polling/PollingLoginMethodViewState$PartialState;)Lcom/swedbank/mobile/app/authentication/recurring/polling/PollingLoginMethodViewState;"

    return-object v0
.end method
