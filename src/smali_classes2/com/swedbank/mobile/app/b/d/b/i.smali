.class public final Lcom/swedbank/mobile/app/b/d/b/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PollingLoginMethodViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/b/d/b/h;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/d;

.field private final g:Lcom/swedbank/mobile/business/authentication/login/m;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/b/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "illustration"

    const-string v4, "getIllustration()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/b/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "actionBtn"

    const-string v4, "getActionBtn()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/b/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/b/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "altLoginBtn"

    const-string v4, "getAltLoginBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/b/d/b/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/login/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/login/m;
        .annotation runtime Ljavax/inject/Named;
            value = "for_polling_login_method"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "lastUsedLoginMethod"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/i;->g:Lcom/swedbank/mobile/business/authentication/login/m;

    const p1, 0x7f0a025a

    .line 27
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/i;->c:Lkotlin/f/c;

    const p1, 0x7f0a0258

    .line 28
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/i;->d:Lkotlin/f/c;

    const p1, 0x7f0a0259

    .line 29
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/i;->e:Lkotlin/f/c;

    .line 30
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/b/d/b/i$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/b/d/b/i$a;-><init>(Lcom/swedbank/mobile/app/b/d/b/i;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/i;->f:Lkotlin/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->f()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object p0

    return-object p0
.end method

.method private final a(Z)V
    .locals 4

    const/4 v0, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    const p1, 0x7f11014a

    .line 113
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->a(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v3

    .line 114
    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setShimmering(Z)V

    .line 115
    invoke-virtual {v3, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 116
    invoke-virtual {v3, v2}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 58
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->h()Landroid/widget/Button;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 59
    :cond_0
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->g()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 119
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->b(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object p1

    sget-object v3, Lcom/swedbank/mobile/app/b/d/b/j;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result p1

    aget p1, v3, p1

    packed-switch p1, :pswitch_data_0

    .line 122
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No button text for login method "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->b(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_0
    const p1, 0x7f11014b

    goto :goto_0

    :pswitch_1
    const p1, 0x7f110151

    .line 125
    :goto_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->a(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v3

    .line 126
    invoke-virtual {v3, v2}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setShimmering(Z)V

    .line 127
    invoke-virtual {v3, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 128
    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 62
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->h()Landroid/widget/Button;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 63
    :cond_2
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->g()Landroid/widget/Button;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/business/authentication/login/m;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->g:Lcom/swedbank/mobile/business/authentication/login/m;

    return-object p0
.end method

.method private final c()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/b/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/d/b/i;)Landroid/widget/ImageView;
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->c()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method private final f()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/b/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-object v0
.end method

.method private final g()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/b/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final h()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->f:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/b/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->f()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 111
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/b/d/b/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/b/d/b/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/d/b/m;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/b/d/b/i;->a(Z)V

    .line 48
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->n()Landroid/content/Context;

    move-result-object v0

    .line 49
    check-cast p1, Lcom/swedbank/mobile/app/w/h;

    .line 47
    invoke-static {p0, v0, p1}, Lcom/swedbank/mobile/app/w/d;->a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/i;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/app/b/d/b/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/d/b/i;->a(Lcom/swedbank/mobile/app/b/d/b/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->g()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 112
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 4

    .line 90
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 91
    new-instance v1, Lcom/swedbank/mobile/app/b/d/b/i$b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/b/d/b/i$b;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/b/d/b/k;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/d/b/k;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 92
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/d/b/i;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/i;->g:Lcom/swedbank/mobile/business/authentication/login/m;

    .line 93
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->c(Lcom/swedbank/mobile/app/b/d/b/i;)Landroid/widget/ImageView;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/app/b/d/b/j;->b:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 96
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No illustration for login method "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :pswitch_0
    const v0, 0x7f0800c4

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0800db

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v0, 0x0

    .line 100
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->b(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/app/b/d/b/j;->a:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/authentication/login/m;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No button text for login method "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->b(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/business/authentication/login/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_2
    const v1, 0x7f11014b

    goto :goto_1

    :pswitch_3
    const v1, 0x7f110151

    :goto_1
    const/4 v2, 0x1

    .line 106
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/b/i;->a(Lcom/swedbank/mobile/app/b/d/b/i;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v3

    .line 107
    invoke-virtual {v3, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setShimmering(Z)V

    .line 108
    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 109
    invoke-virtual {v3, v2}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
