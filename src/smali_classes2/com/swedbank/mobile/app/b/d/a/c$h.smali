.class final Lcom/swedbank/mobile/app/b/d/a/c$h;
.super Ljava/lang/Object;
.source "PinCalculatorLoginMethodPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/d/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/d/a/c;

.field final synthetic b:Lcom/b/c/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/d/a/c;Lcom/b/c/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/a/c$h;->a:Lcom/swedbank/mobile/app/b/d/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/d/a/c$h;->b:Lcom/b/c/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/o;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/b/d/a/m$a;",
            ">;"
        }
    .end annotation

    const-string v0, "currentInput"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/c$h;->a:Lcom/swedbank/mobile/app/b/d/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/b/d/a/c;->a(Lcom/swedbank/mobile/app/b/d/a/c;)Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Lcom/swedbank/mobile/app/b/d/a/m$a$d;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/b/d/a/m$a$d;-><init>(Z)V

    .line 143
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/c$h$1;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/app/b/d/a/c$h$1;-><init>(Lcom/swedbank/mobile/app/b/d/a/c$h;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_2

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/c$h;->a:Lcom/swedbank/mobile/app/b/d/a/c;

    .line 144
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_1

    const-string v3, "\u25cf "

    .line 155
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    rsub-int/lit8 p1, p1, 0x8

    :goto_1
    if-ge v1, p1, :cond_2

    const-string v2, "\u25cb "

    .line 158
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 160
    :cond_2
    move-object p1, v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/j/n;->d(Ljava/lang/CharSequence;)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 153
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "StringBuilder(PIN_LENGTH\u2026stIndex)\n    }.toString()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/swedbank/mobile/app/b/d/a/m$a$c;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/b/d/a/m$a$c;-><init>(Ljava/lang/String;)V

    .line 161
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/d/a/c$h;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
