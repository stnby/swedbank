.class public final Lcom/swedbank/mobile/app/b/d/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "RecurringLoginViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/b/d/h;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/app/b/d/g;

.field private final h:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "rootView"

    const-string v4, "getRootView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "welcomeView"

    const-string v4, "getWelcomeView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "alternativeLoginBtn"

    const-string v4, "getAlternativeLoginBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/b/d/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Lcom/swedbank/mobile/app/b/d/g;Lcom/swedbank/mobile/business/util/l;)V
    .locals 1
    .param p1    # Ljavax/inject/Provider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/b/d/g;
        .annotation runtime Ljavax/inject/Named;
            value = "for_recurring_login"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "logged_in_customer_name"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/app/b/d/g;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "greetingFormatterProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tab"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggedInCustomerName"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/i;->f:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/d/i;->g:Lcom/swedbank/mobile/app/b/d/g;

    iput-object p3, p0, Lcom/swedbank/mobile/app/b/d/i;->h:Lcom/swedbank/mobile/business/util/l;

    .line 34
    sget p1, Lcom/swedbank/mobile/core/a$d;->recurring_login_view_root:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/i;->b:Lkotlin/f/c;

    .line 35
    sget p1, Lcom/swedbank/mobile/core/a$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/i;->c:Lkotlin/f/c;

    .line 36
    sget p1, Lcom/swedbank/mobile/core/a$d;->recurring_login_welcome_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/i;->d:Lkotlin/f/c;

    .line 37
    sget p1, Lcom/swedbank/mobile/core/a$d;->recurring_login_alternative_login_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/i;->e:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/d/i;)Landroid/view/View;
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/i;->b()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final b()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/i;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/d/i;)Landroidx/appcompat/widget/Toolbar;
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/i;->c()Landroidx/appcompat/widget/Toolbar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/d/i;)Landroid/widget/TextView;
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/i;->d()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/i;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/b/d/i;)Landroid/content/Context;
    .locals 0

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/d/i;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/i;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/b/d/i;)Ljavax/inject/Provider;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/d/i;->f:Ljavax/inject/Provider;

    return-object p0
.end method

.method private final f()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/i;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/i;->f()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 87
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/b/d/k;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/b/d/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/swedbank/mobile/app/b/d/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/d/i;->a(Lcom/swedbank/mobile/app/b/d/k;)V

    return-void
.end method

.method protected e()V
    .locals 9

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/i;->g:Lcom/swedbank/mobile/app/b/d/g;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/b/d/g;->b()I

    move-result v0

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/i;->g:Lcom/swedbank/mobile/app/b/d/g;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/b/d/g;->c()I

    move-result v1

    .line 71
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/i;->a(Lcom/swedbank/mobile/app/b/d/i;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 72
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/i;->b(Lcom/swedbank/mobile/app/b/d/i;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/i;->h:Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 74
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/i;->c(Lcom/swedbank/mobile/app/b/d/i;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    .line 75
    invoke-static {v1, v2}, Landroidx/core/widget/i;->b(Landroid/widget/TextView;I)V

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/i;->d(Lcom/swedbank/mobile/app/b/d/i;)Landroid/content/Context;

    move-result-object v4

    .line 78
    sget v5, Lcom/swedbank/mobile/core/a$f;->recurring_login_welcome:I

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x2

    .line 83
    invoke-static {v0, v7, v3, v8, v3}, Lkotlin/j/n;->a(Ljava/lang/String;CLjava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/i;->e(Lcom/swedbank/mobile/app/b/d/i;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    const-string v7, "greetingFormatterProvider.get()"

    invoke-static {v3, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lkotlin/e/a/b;

    .line 84
    invoke-interface {v3, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v6

    .line 77
    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 76
    :cond_0
    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
