.class public final Lcom/swedbank/mobile/app/b/d/b/f;
.super Lcom/swedbank/mobile/architect/a/h;
.source "PollingLoginMethodRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/authentication/recurring/polling/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/business/authentication/o;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/authentication/o;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/authentication/o;
        .annotation runtime Ljavax/inject/Named;
            value = "polling_login_method_authentication_stream"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_polling_login_method"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_polling_login_method"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/o;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p3, p5, p4}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/f;->e:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/b/d/b/f;->f:Lcom/swedbank/mobile/business/authentication/o;

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/authentication/e;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "credentials"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/b/f;->e:Lcom/swedbank/mobile/architect/business/a/c;

    .line 28
    new-instance v1, Lcom/swedbank/mobile/business/navigation/b;

    .line 29
    new-instance v2, Lcom/swedbank/mobile/business/authentication/g$b;

    .line 31
    iget-object v3, p0, Lcom/swedbank/mobile/app/b/d/b/f;->f:Lcom/swedbank/mobile/business/authentication/o;

    .line 29
    invoke-direct {v2, p1, v3}, Lcom/swedbank/mobile/business/authentication/g$b;-><init>(Lcom/swedbank/mobile/business/authentication/e;Lcom/swedbank/mobile/business/authentication/o;)V

    check-cast v2, Lcom/swedbank/mobile/business/authentication/g;

    const/4 p1, 0x0

    const/4 v3, 0x2

    .line 28
    invoke-direct {v1, v2, p1, v3, p1}, Lcom/swedbank/mobile/business/navigation/b;-><init>(Lcom/swedbank/mobile/business/authentication/g;Ljava/lang/String;ILkotlin/e/b/g;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return-void
.end method
