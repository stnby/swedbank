.class public final Lcom/swedbank/mobile/app/b/d/a/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PinCalculatorLoginMethodViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/b/d/a/i;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private final m:Lkotlin/f/c;

.field private final n:Lkotlin/f/c;

.field private final o:Lkotlin/f/c;

.field private final p:Lkotlin/f/c;

.field private final q:Lkotlin/f/c;

.field private final r:Lkotlin/d;

.field private final s:Lkotlin/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x11

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "instructionsView"

    const-string v4, "getInstructionsView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "inputView"

    const-string v4, "getInputView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num0"

    const-string v4, "getNum0()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num1"

    const-string v4, "getNum1()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num2"

    const-string v4, "getNum2()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num3"

    const-string v4, "getNum3()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num4"

    const-string v4, "getNum4()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num5"

    const-string v4, "getNum5()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num6"

    const-string v4, "getNum6()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num7"

    const-string v4, "getNum7()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num8"

    const-string v4, "getNum8()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "num9"

    const-string v4, "getNum9()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "del"

    const-string v4, "getDel()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "altLoginBtn"

    const-string v4, "getAltLoginBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "inputGroup"

    const-string v4, "getInputGroup()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    const v0, 0x7f0a0251

    .line 22
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->c:Lkotlin/f/c;

    const v0, 0x7f0a0253

    .line 23
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->d:Lkotlin/f/c;

    const v0, 0x7f0a0244

    .line 24
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->e:Lkotlin/f/c;

    const v0, 0x7f0a0245

    .line 25
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->f:Lkotlin/f/c;

    const v0, 0x7f0a0246

    .line 26
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->g:Lkotlin/f/c;

    const v0, 0x7f0a0247

    .line 27
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->h:Lkotlin/f/c;

    const v0, 0x7f0a0248

    .line 28
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->i:Lkotlin/f/c;

    const v0, 0x7f0a0249

    .line 29
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->j:Lkotlin/f/c;

    const v0, 0x7f0a024a

    .line 30
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->k:Lkotlin/f/c;

    const v0, 0x7f0a024b

    .line 31
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->l:Lkotlin/f/c;

    const v0, 0x7f0a024c

    .line 32
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->m:Lkotlin/f/c;

    const v0, 0x7f0a024d

    .line 33
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->n:Lkotlin/f/c;

    const v0, 0x7f0a024f

    .line 34
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->o:Lkotlin/f/c;

    const v0, 0x7f0a0252

    .line 35
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->p:Lkotlin/f/c;

    const v0, 0x7f0a024e

    .line 36
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->q:Lkotlin/f/c;

    .line 37
    sget-object v0, Lkotlin/i;->c:Lkotlin/i;

    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/j$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/d/a/j$a;-><init>(Lcom/swedbank/mobile/app/b/d/a/j;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v1}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->r:Lkotlin/d;

    .line 40
    sget-object v0, Lkotlin/i;->c:Lkotlin/i;

    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/j$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/d/a/j$b;-><init>(Lcom/swedbank/mobile/app/b/d/a/j;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-static {v0, v1}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->s:Lkotlin/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/TextView;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/b/d/a/j;)Ljava/util/List;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->z()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/Button;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->y()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->w()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/Button;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->x()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/TextView;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->f()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->h()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final h()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->i()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final i()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->j()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final j()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->k()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final k()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->p()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->q()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->r()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->s()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic o(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->t()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final p()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic p(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->u()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final q()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic q(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->v()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final r()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final s()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->l:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final t()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->m:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final u()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->n:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final v()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->o:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final w()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->p:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final x()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->q:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final y()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->r:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final z()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->s:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j;->a:[Lkotlin/h/g;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/16 v0, 0xa

    .line 49
    new-array v0, v0, [Lio/reactivex/s;

    .line 50
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->h()Landroid/view/View;

    move-result-object v1

    .line 97
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 50
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$c;->a:Lcom/swedbank/mobile/app/b/d/a/j$c;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 51
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->i()Landroid/view/View;

    move-result-object v1

    .line 98
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 51
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$e;->a:Lcom/swedbank/mobile/app/b/d/a/j$e;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 52
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->j()Landroid/view/View;

    move-result-object v1

    .line 99
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 52
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$f;->a:Lcom/swedbank/mobile/app/b/d/a/j$f;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->k()Landroid/view/View;

    move-result-object v1

    .line 100
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 53
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$g;->a:Lcom/swedbank/mobile/app/b/d/a/j$g;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->p()Landroid/view/View;

    move-result-object v1

    .line 101
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 54
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$h;->a:Lcom/swedbank/mobile/app/b/d/a/j$h;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 55
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->q()Landroid/view/View;

    move-result-object v1

    .line 102
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 55
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$i;->a:Lcom/swedbank/mobile/app/b/d/a/j$i;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 56
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->r()Landroid/view/View;

    move-result-object v1

    .line 103
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 56
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$j;->a:Lcom/swedbank/mobile/app/b/d/a/j$j;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 57
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->s()Landroid/view/View;

    move-result-object v1

    .line 104
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 57
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$k;->a:Lcom/swedbank/mobile/app/b/d/a/j$k;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 58
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->t()Landroid/view/View;

    move-result-object v1

    .line 105
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 58
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$l;->a:Lcom/swedbank/mobile/app/b/d/a/j$l;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 59
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->u()Landroid/view/View;

    move-result-object v1

    .line 106
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 59
    sget-object v1, Lcom/swedbank/mobile/app/b/d/a/j$d;->a:Lcom/swedbank/mobile/app/b/d/a/j$d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 49
    invoke-static {v0}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026.clicks().map { \"9\" }\n  )"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/b/d/a/m;)V
    .locals 5
    .param p1    # Lcom/swedbank/mobile/app/b/d/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/d/a/m;->a()Ljava/lang/String;

    move-result-object v0

    .line 109
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 110
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->a(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/d/a/m;->b()Z

    move-result v0

    const/16 v1, 0xa

    const/4 v3, 0x4

    if-eqz v0, :cond_4

    .line 114
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->b(Lcom/swedbank/mobile/app/b/d/a/j;)Ljava/util/List;

    move-result-object v0

    .line 115
    check-cast v0, Ljava/lang/Iterable;

    .line 116
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 117
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 118
    check-cast v1, Landroid/view/View;

    .line 115
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v4, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 119
    :cond_2
    check-cast v4, Ljava/util/List;

    .line 120
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->c(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 121
    :cond_3
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->d(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->e(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 124
    :cond_4
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->b(Lcom/swedbank/mobile/app/b/d/a/j;)Ljava/util/List;

    move-result-object v0

    .line 125
    check-cast v0, Ljava/lang/Iterable;

    .line 126
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 127
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 128
    check-cast v1, Landroid/view/View;

    .line 125
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v4, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 129
    :cond_5
    check-cast v4, Ljava/util/List;

    .line 130
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->c(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 131
    :cond_6
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->d(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 132
    invoke-static {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->e(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 69
    :goto_3
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->n()Landroid/content/Context;

    move-result-object v0

    .line 70
    check-cast p1, Lcom/swedbank/mobile/app/w/h;

    .line 68
    invoke-static {p0, v0, p1}, Lcom/swedbank/mobile/app/w/d;->a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/a/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/b/d/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/d/a/j;->a(Lcom/swedbank/mobile/app/b/d/a/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->v()Landroid/view/View;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    invoke-direct {p0}, Lcom/swedbank/mobile/app/b/d/a/j;->x()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 108
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 3

    .line 94
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 95
    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/j$m;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/b/d/a/j$m;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/b/d/a/k;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/b/d/a/k;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 96
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/d/a/j;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    return-void
.end method
