.class final Lcom/swedbank/mobile/app/b/d/b/c$e;
.super Ljava/lang/Object;
.source "PollingLoginMethodPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/d/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/b/d/b/c$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/b/d/b/c$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/b/d/b/c$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/b/d/b/c$e;->a:Lcom/swedbank/mobile/app/b/d/b/c$e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/v;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/b/d/b/m$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/b/d/b/m$a$d;->a:Lcom/swedbank/mobile/app/b/d/b/m$a$d;

    check-cast p1, Lcom/swedbank/mobile/app/b/d/b/m$a;

    goto :goto_0

    .line 37
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/swedbank/mobile/app/b/d/b/m$a$a;->a:Lcom/swedbank/mobile/app/b/d/b/m$a$a;

    check-cast p1, Lcom/swedbank/mobile/app/b/d/b/m$a;

    goto :goto_0

    .line 38
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$e;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/authentication/v$e;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v$e;->b()Ljava/util/List;

    move-result-object p1

    .line 40
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/d;->a(Ljava/util/List;)Lkotlin/k;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 42
    new-instance v1, Lcom/swedbank/mobile/app/b/d/b/m$a$e;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/app/b/d/b/m$a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/app/b/d/b/m$a;

    goto :goto_0

    .line 46
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$c;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/swedbank/mobile/app/b/d/b/m$a$c;

    check-cast p1, Lcom/swedbank/mobile/business/authentication/v$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v$c;->b()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/b/d/b/m$a$c;-><init>(Lcom/swedbank/mobile/app/w/b;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/b/d/b/m$a;

    .line 50
    :goto_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/b/d/b/m$a$c;

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/app/b/d/b/m$a$e;

    if-eqz v0, :cond_4

    .line 52
    :goto_1
    sget-object v0, Lcom/swedbank/mobile/app/b/d/b/m$a$b;->a:Lcom/swedbank/mobile/app/b/d/b/m$a$b;

    const-wide/16 v1, 0xdac

    .line 96
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    .line 95
    new-instance v2, Lcom/swedbank/mobile/app/b/d/b/c$e$a;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/b/d/b/c$e$a;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 94
    invoke-virtual {v0, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 53
    :cond_4
    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(viewState)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    .line 47
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No ViewState for LoginState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/business/authentication/v;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/d/b/c$e;->a(Lcom/swedbank/mobile/business/authentication/v;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
