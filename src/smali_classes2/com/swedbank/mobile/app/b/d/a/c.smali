.class public final Lcom/swedbank/mobile/app/b/d/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PinCalculatorLoginMethodPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/b/d/a/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/b/d/a/i;",
        "Lcom/swedbank/mobile/app/b/d/a/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/a/c;->a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/d/a/c;)Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/d/a/c;->a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 13

    .line 25
    sget-object v0, Lcom/swedbank/mobile/app/b/d/a/c$i;->a:Lcom/swedbank/mobile/app/b/d/a/c$i;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/d/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/c$j;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/d/a/c$j;-><init>(Lcom/swedbank/mobile/app/b/d/a/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 30
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v1

    const-string v2, "PublishRelay.create<InputChange>()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v2, Lcom/swedbank/mobile/app/b/d/a/c$c;->a:Lcom/swedbank/mobile/app/b/d/a/c$c;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/b/d/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    sget-object v3, Lcom/swedbank/mobile/app/b/d/a/c$d;->a:Lcom/swedbank/mobile/app/b/d/a/c$d;

    check-cast v3, Lkotlin/e/a/b;

    if-eqz v3, :cond_0

    new-instance v4, Lcom/swedbank/mobile/app/b/d/a/e;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/b/d/a/e;-><init>(Lkotlin/e/a/b;)V

    move-object v3, v4

    :cond_0
    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 33
    sget-object v3, Lcom/swedbank/mobile/app/b/d/a/c$e;->a:Lcom/swedbank/mobile/app/b/d/a/c$e;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/b/d/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    sget-object v4, Lcom/swedbank/mobile/app/b/d/a/c$f;->a:Lcom/swedbank/mobile/app/b/d/a/c$f;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 36
    invoke-static {}, Lio/reactivex/j/a;->c()Lio/reactivex/v;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/b/c/c;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v4

    check-cast v4, Lio/reactivex/s;

    .line 31
    invoke-static {v2, v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, ""

    .line 37
    sget-object v4, Lcom/swedbank/mobile/app/b/d/a/c$g;->a:Lcom/swedbank/mobile/app/b/d/a/c$g;

    check-cast v4, Lio/reactivex/c/c;

    invoke-virtual {v2, v3, v4}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v2

    const-wide/16 v3, 0x1

    .line 47
    invoke-virtual {v2, v3, v4}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v2

    .line 48
    new-instance v5, Lcom/swedbank/mobile/app/b/d/a/c$h;

    invoke-direct {v5, p0, v1}, Lcom/swedbank/mobile/app/b/d/a/c$h;-><init>(Lcom/swedbank/mobile/app/b/d/a/c;Lcom/b/c/c;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v2, v5}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 62
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/d/a/c;->a:Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;

    .line 63
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/recurring/pincalculator/a;->a()Lio/reactivex/o;

    move-result-object v2

    .line 64
    sget-object v5, Lcom/swedbank/mobile/app/b/d/a/c$k;->a:Lcom/swedbank/mobile/app/b/d/a/c$k;

    check-cast v5, Lio/reactivex/c/k;

    invoke-virtual {v2, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    .line 65
    sget-object v5, Lcom/swedbank/mobile/app/b/d/a/c$l;->a:Lcom/swedbank/mobile/app/b/d/a/c$l;

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v2, v5}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 89
    check-cast v0, Lio/reactivex/s;

    .line 90
    check-cast v1, Lio/reactivex/s;

    .line 91
    check-cast v2, Lio/reactivex/s;

    .line 88
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026     loginProgressStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/m;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1f

    const/4 v12, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v12}, Lcom/swedbank/mobile/app/b/d/a/m;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    .line 94
    new-instance v2, Lcom/swedbank/mobile/app/b/d/a/c$b;

    move-object v5, p0

    check-cast v5, Lcom/swedbank/mobile/app/b/d/a/c;

    invoke-direct {v2, v5}, Lcom/swedbank/mobile/app/b/d/a/c$b;-><init>(Lcom/swedbank/mobile/app/b/d/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 143
    new-instance v5, Lcom/swedbank/mobile/app/b/d/a/d;

    invoke-direct {v5, v2}, Lcom/swedbank/mobile/app/b/d/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v5, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v5}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 144
    invoke-virtual {v0, v3, v4}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
