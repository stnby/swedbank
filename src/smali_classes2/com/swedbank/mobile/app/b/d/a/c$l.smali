.class final Lcom/swedbank/mobile/app/b/d/a/c$l;
.super Ljava/lang/Object;
.source "PinCalculatorLoginMethodPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/d/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/b/d/a/c$l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/b/d/a/c$l;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/b/d/a/c$l;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/b/d/a/c$l;->a:Lcom/swedbank/mobile/app/b/d/a/c$l;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/authentication/v;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/authentication/v;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/v;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/b/d/a/m$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "loginState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$d;->a:Lcom/swedbank/mobile/business/authentication/v$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/swedbank/mobile/app/b/d/a/m$a$d;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/b/d/a/m$a$d;-><init>(Z)V

    check-cast p1, Lcom/swedbank/mobile/app/b/d/a/m$a;

    goto :goto_0

    .line 68
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/business/authentication/v$b;->a:Lcom/swedbank/mobile/business/authentication/v$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/swedbank/mobile/app/b/d/a/m$a$d;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/b/d/a/m$a$d;-><init>(Z)V

    check-cast p1, Lcom/swedbank/mobile/app/b/d/a/m$a;

    goto :goto_0

    .line 69
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$e;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/authentication/v$e;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v$e;->b()Ljava/util/List;

    move-result-object p1

    .line 71
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/d;->a(Ljava/util/List;)Lkotlin/k;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 73
    new-instance v1, Lcom/swedbank/mobile/app/b/d/a/m$a$e;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/app/b/d/a/m$a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    move-object p1, v1

    check-cast p1, Lcom/swedbank/mobile/app/b/d/a/m$a;

    goto :goto_0

    .line 77
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/authentication/v$c;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/swedbank/mobile/app/b/d/a/m$a$b;

    check-cast p1, Lcom/swedbank/mobile/business/authentication/v$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/authentication/v$c;->b()Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/b/d/a/m$a$b;-><init>(Lcom/swedbank/mobile/app/w/b;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/b/d/a/m$a;

    .line 81
    :goto_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/b/d/a/m$a$b;

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/app/b/d/a/m$a$e;

    if-eqz v0, :cond_4

    .line 83
    :goto_1
    sget-object v0, Lcom/swedbank/mobile/app/b/d/a/m$a$a;->a:Lcom/swedbank/mobile/app/b/d/a/m$a$a;

    const-wide/16 v1, 0xdac

    .line 149
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    .line 148
    new-instance v2, Lcom/swedbank/mobile/app/b/d/a/c$l$a;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/b/d/a/c$l$a;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 147
    invoke-virtual {v0, p1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 84
    :cond_4
    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(viewState)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1

    .line 78
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No ViewState for LoginState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/business/authentication/v;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/b/d/a/c$l;->a(Lcom/swedbank/mobile/business/authentication/v;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
