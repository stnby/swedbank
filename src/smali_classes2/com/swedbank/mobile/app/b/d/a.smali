.class public final Lcom/swedbank/mobile/app/b/d/a;
.super Ljava/lang/Object;
.source "RecurringLoginBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/app/b/d/g;

.field private final b:Lcom/swedbank/mobile/a/c/d/d$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/c/d/d$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/d/d$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/a;->b:Lcom/swedbank/mobile/a/c/d/d$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/b/d/g;)Lcom/swedbank/mobile/app/b/d/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/b/d/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "tab"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/b/d/a;

    .line 14
    iput-object p1, v0, Lcom/swedbank/mobile/app/b/d/a;->a:Lcom/swedbank/mobile/app/b/d/g;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/app/b/d/a;->b:Lcom/swedbank/mobile/a/c/d/d$a;

    .line 18
    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a;->a:Lcom/swedbank/mobile/app/b/d/g;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/c/d/d$a;->b(Lcom/swedbank/mobile/app/b/d/g;)Lcom/swedbank/mobile/a/c/d/d$a;

    move-result-object v0

    .line 21
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/d/d$a;->a()Lcom/swedbank/mobile/a/c/d/d;

    move-result-object v0

    .line 22
    invoke-interface {v0}, Lcom/swedbank/mobile/a/c/d/d;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 18
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build recurring login node without initiating tab"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
