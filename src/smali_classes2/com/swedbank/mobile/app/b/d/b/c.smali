.class public final Lcom/swedbank/mobile/app/b/d/b/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PollingLoginMethodPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/b/d/b/h;",
        "Lcom/swedbank/mobile/app/b/d/b/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/authentication/recurring/polling/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/authentication/recurring/polling/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/recurring/polling/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/b/c;->a:Lcom/swedbank/mobile/business/authentication/recurring/polling/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/b/d/b/c;)Lcom/swedbank/mobile/business/authentication/recurring/polling/a;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/b/d/b/c;->a:Lcom/swedbank/mobile/business/authentication/recurring/polling/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .line 21
    sget-object v0, Lcom/swedbank/mobile/app/b/d/b/c$b;->a:Lcom/swedbank/mobile/app/b/d/b/c$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/b/d/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/swedbank/mobile/app/b/d/b/c$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/b/d/b/c$c;-><init>(Lcom/swedbank/mobile/app/b/d/b/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 26
    sget-object v1, Lcom/swedbank/mobile/app/b/d/b/c$f;->a:Lcom/swedbank/mobile/app/b/d/b/c$f;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/b/d/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 27
    new-instance v2, Lcom/swedbank/mobile/app/b/d/b/c$g;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/b/d/b/c$g;-><init>(Lcom/swedbank/mobile/app/b/d/b/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 31
    iget-object v2, p0, Lcom/swedbank/mobile/app/b/d/b/c;->a:Lcom/swedbank/mobile/business/authentication/recurring/polling/a;

    .line 32
    invoke-interface {v2}, Lcom/swedbank/mobile/business/authentication/recurring/polling/a;->a()Lio/reactivex/o;

    move-result-object v2

    .line 33
    sget-object v3, Lcom/swedbank/mobile/app/b/d/b/c$d;->a:Lcom/swedbank/mobile/app/b/d/b/c$d;

    check-cast v3, Lio/reactivex/c/k;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    .line 34
    sget-object v3, Lcom/swedbank/mobile/app/b/d/b/c$e;->a:Lcom/swedbank/mobile/app/b/d/b/c$e;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 58
    check-cast v2, Lio/reactivex/s;

    .line 59
    check-cast v0, Lio/reactivex/s;

    .line 60
    check-cast v1, Lio/reactivex/s;

    .line 57
    invoke-static {v2, v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026eam,\n        loginStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    new-instance v1, Lcom/swedbank/mobile/app/b/d/b/m;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/b/d/b/m;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    .line 63
    new-instance v2, Lcom/swedbank/mobile/app/b/d/b/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/b/d/b/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/b/d/b/c$a;-><init>(Lcom/swedbank/mobile/app/b/d/b/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 90
    new-instance v3, Lcom/swedbank/mobile/app/b/d/b/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/b/d/b/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 91
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
