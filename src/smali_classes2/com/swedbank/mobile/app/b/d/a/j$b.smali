.class final Lcom/swedbank/mobile/app/b/d/a/j$b;
.super Lkotlin/e/b/k;
.source "PinCalculatorLoginMethodViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/b/d/a/j;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Ljava/util/List<",
        "+",
        "Landroid/view/View;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/b/d/a/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/b/d/a/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/16 v0, 0xd

    .line 41
    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->f(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/TextView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->a(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/widget/TextView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->g(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->h(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->i(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->j(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->k(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->l(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->m(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->n(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->o(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->p(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/swedbank/mobile/app/b/d/a/j$b;->a:Lcom/swedbank/mobile/app/b/d/a/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/b/d/a/j;->q(Lcom/swedbank/mobile/app/b/d/a/j;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xc

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/b/d/a/j$b;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
