.class public final Lcom/swedbank/mobile/app/q/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "UserPrivacyOnboardingPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/q/a/h;",
        "Lcom/swedbank/mobile/app/q/a/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/privacy/onboarding/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/privacy/onboarding/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/privacy/onboarding/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/q/a/c;->a:Lcom/swedbank/mobile/business/privacy/onboarding/c;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .line 12
    sget-object v0, Lcom/swedbank/mobile/app/q/a/c$a;->a:Lcom/swedbank/mobile/app/q/a/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/q/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 13
    new-instance v1, Lcom/swedbank/mobile/app/q/a/c$b;

    iget-object v2, p0, Lcom/swedbank/mobile/app/q/a/c;->a:Lcom/swedbank/mobile/business/privacy/onboarding/c;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/q/a/c$b;-><init>(Lcom/swedbank/mobile/business/privacy/onboarding/c;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/q/a/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/q/a/d;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "action(UserPrivacyOnboar\u2026acyOnboardingViewState>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
