.class public final Lcom/swedbank/mobile/app/q/a/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "UserPrivacyOnboardingViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/q/a/h;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/q/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "pluginContainer"

    const-string v4, "getPluginContainer()Landroid/view/ViewGroup;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/q/a/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    const v0, 0x7f0a01c9

    .line 20
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/q/a/i;->b:Lkotlin/f/c;

    const/4 v0, 0x1

    .line 21
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefa\u2026DEFAULT_USER_DATA_SAVING)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/q/a/i;->c:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/q/a/i;)Lcom/b/c/b;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/q/a/i;->c:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/q/a/i;Z)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/q/a/i;->a(Z)V

    return-void
.end method

.method private final a(Z)V
    .locals 4

    .line 41
    invoke-direct {p0}, Lcom/swedbank/mobile/app/q/a/i;->b()Landroid/view/ViewGroup;

    move-result-object v0

    if-nez p1, :cond_2

    .line 43
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "rootView\n          .context"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0d00ac

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 66
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v3, "LayoutInflater.from(this)"

    invoke-static {p1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const p1, 0x7f0a01d6

    .line 47
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 48
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private final b()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/q/a/i;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/q/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/q/a/i;->c:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/q/a/k;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/q/a/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/q/a/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/q/a/i;->a(Lcom/swedbank/mobile/app/q/a/k;)V

    return-void
.end method

.method protected e()V
    .locals 5

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/q/a/i;->b()Landroid/view/ViewGroup;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 57
    :goto_0
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v4, "LayoutInflater.from(this)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f0d00ab

    .line 56
    invoke-virtual {v1, v4, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 28
    check-cast v0, Landroid/view/View;

    const v1, 0x7f0a01d7

    .line 58
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    .line 29
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/Boolean;)V

    .line 30
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 59
    new-instance v2, Lcom/swedbank/mobile/app/q/a/i$a;

    invoke-direct {v2, v0, p0}, Lcom/swedbank/mobile/app/q/a/i$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Lcom/swedbank/mobile/app/q/a/i;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void

    .line 56
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
