.class public final Lcom/swedbank/mobile/app/discovery/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "FeatureDiscoveryViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/discovery/h;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/i;->b:Lio/reactivex/o;

    .line 23
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<FeatureKey>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/i;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/discovery/i;)Lcom/b/c/c;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/discovery/i;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/discovery/i;)Lio/reactivex/o;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/discovery/i;->b:Lio/reactivex/o;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/i;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/discovery/k;)V
    .locals 21
    .param p1    # Lcom/swedbank/mobile/app/discovery/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/discovery/k;->a()Lcom/swedbank/mobile/business/discovery/d;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 30
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/discovery/d;->a()Ljava/lang/String;

    move-result-object v7

    .line 31
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/discovery/d;->b()I

    move-result v6

    .line 32
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/discovery/d;->c()Ljava/lang/CharSequence;

    move-result-object v10

    .line 33
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/discovery/d;->d()Ljava/lang/CharSequence;

    move-result-object v0

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/discovery/i;->o()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    move-object v3, v1

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    .line 81
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "hierarchyRoot.context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0d0084

    const/4 v4, 0x1

    .line 84
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v5, "LayoutInflater.from(this)"

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const v1, 0x7f0a0124

    .line 85
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Landroid/view/ViewGroup;

    const v1, 0x7f0a0123

    .line 86
    invoke-virtual {v11, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    .line 87
    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 89
    new-instance v14, Lcom/swedbank/mobile/app/discovery/i$a;

    move-object v1, v14

    move-object v2, v12

    move-object v4, v11

    move-object/from16 v5, p0

    move-object v8, v10

    move-object v9, v0

    invoke-direct/range {v1 .. v9}, Lcom/swedbank/mobile/app/discovery/i$a;-><init>(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/swedbank/mobile/app/discovery/i;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object v1, v14

    check-cast v1, Lkotlin/e/a/b;

    .line 96
    new-instance v2, Lcom/swedbank/mobile/app/discovery/i$b;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/discovery/i$b;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v11, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/discovery/i;->b(Lcom/swedbank/mobile/app/discovery/i;)Lio/reactivex/o;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x3

    const/16 v20, 0x0

    move-object/from16 v18, v1

    .line 102
    invoke-static/range {v15 .. v20}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "rootView"

    .line 101
    invoke-static {v11, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Landroid/view/View;

    invoke-static {v1, v11}, Lcom/swedbank/mobile/core/ui/v;->a(Lio/reactivex/b/c;Landroid/view/View;)V

    const-string v1, "targetView"

    .line 106
    invoke-static {v13, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {v12, v13, v10, v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 83
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Root view missing or not a ViewGroup"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/discovery/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/discovery/i;->a(Lcom/swedbank/mobile/app/discovery/k;)V

    return-void
.end method
