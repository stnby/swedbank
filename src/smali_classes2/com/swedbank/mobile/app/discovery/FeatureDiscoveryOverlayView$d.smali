.class final Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;
.super Lkotlin/e/b/k;
.source "FeatureDiscoveryOverlayView.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/view/View;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

.field final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->b:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->b:Landroid/view/View;

    .line 262
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 263
    invoke-static {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->c(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)[I

    move-result-object v2

    .line 264
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 265
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v4

    mul-float v3, v3, v4

    .line 266
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v5

    mul-float v4, v4, v5

    const/4 v5, 0x0

    .line 267
    aget v6, v2, v5

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v7, v3

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    const/4 v7, 0x1

    .line 268
    aget v9, v2, v7

    int-to-float v9, v9

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v4

    div-float/2addr v0, v8

    add-float/2addr v9, v0

    .line 269
    new-instance v0, Landroid/graphics/RectF;

    .line 272
    aget v10, v2, v5

    int-to-float v10, v10

    add-float/2addr v10, v3

    .line 273
    aget v2, v2, v7

    int-to-float v2, v2

    add-float/2addr v2, v4

    .line 269
    invoke-direct {v0, v6, v9, v10, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 274
    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 275
    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    const/high16 v4, -0x40800000    # -1.0f

    mul-float v3, v3, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    mul-float v2, v2, v4

    invoke-virtual {v0, v3, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 276
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 277
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    .line 278
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    .line 279
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    div-float/2addr v0, v8

    const-string v4, "metrics"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v4, 0x14

    int-to-float v4, v4

    .line 280
    invoke-static {v7, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    add-float/2addr v0, v1

    .line 281
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredWidth()I

    move-result p1

    const v4, 0x3f83d70a    # 1.03f

    const v6, 0x3f333333    # 0.7f

    if-lt v1, p1, :cond_3

    int-to-float v1, v1

    div-float v9, v1, v8

    cmpl-float v9, v9, v3

    if-ltz v9, :cond_1

    mul-float v1, v1, v6

    sub-float/2addr v1, v3

    goto :goto_0

    :cond_1
    mul-float v6, v6, v1

    sub-float/2addr v1, v3

    sub-float v1, v6, v1

    :goto_0
    int-to-float p1, p1

    div-float v6, p1, v8

    cmpl-float v6, v6, v2

    if-ltz v6, :cond_2

    mul-float p1, p1, v4

    sub-float/2addr p1, v2

    goto :goto_1

    :cond_2
    mul-float v4, v4, p1

    sub-float/2addr p1, v2

    sub-float p1, v4, p1

    .line 301
    :goto_1
    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto :goto_4

    :cond_3
    int-to-float p1, p1

    div-float v9, p1, v8

    cmpl-float v9, v9, v2

    if-ltz v9, :cond_4

    mul-float p1, p1, v6

    sub-float/2addr p1, v2

    goto :goto_2

    :cond_4
    mul-float v6, v6, p1

    sub-float/2addr p1, v2

    sub-float p1, v6, p1

    :goto_2
    int-to-float v1, v1

    div-float v6, v1, v8

    cmpl-float v6, v6, v3

    if-ltz v6, :cond_5

    mul-float v1, v1, v4

    sub-float/2addr v1, v3

    goto :goto_3

    :cond_5
    mul-float v4, v4, v1

    sub-float/2addr v1, v3

    sub-float v1, v4, v1

    .line 321
    :goto_3
    invoke-static {p1, v1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 324
    :goto_4
    new-instance v1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;

    invoke-direct {v1, v2, v3, v0, p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;-><init>(FFFF)V

    .line 58
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    invoke-static {p1, v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;)V

    .line 59
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    .line 60
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b()F

    move-result v0

    .line 61
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c()F

    move-result v2

    .line 329
    new-instance v3, Landroidx/constraintlayout/widget/b;

    invoke-direct {v3}, Landroidx/constraintlayout/widget/b;-><init>()V

    .line 330
    move-object v4, p1

    check-cast v4, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v3, v4}, Landroidx/constraintlayout/widget/b;->a(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 334
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v8

    const v6, 0x7f0a0122

    const v8, 0x7f0a0125

    const/4 v9, 0x4

    const v10, 0x7f0a0126

    const/4 v11, 0x3

    cmpl-float p1, p1, v0

    if-ltz p1, :cond_6

    add-float/2addr v0, v2

    float-to-int p1, v0

    .line 335
    invoke-virtual {v3, v10, p1}, Landroidx/constraintlayout/widget/b;->a(II)V

    .line 336
    invoke-virtual {v3, v8, v11, v10, v9}, Landroidx/constraintlayout/widget/b;->a(IIII)V

    .line 337
    invoke-virtual {v3, v6, v11, v8, v9}, Landroidx/constraintlayout/widget/b;->a(IIII)V

    goto :goto_5

    :cond_6
    sub-float/2addr v0, v2

    float-to-int p1, v0

    .line 340
    invoke-virtual {v3, v10, p1}, Landroidx/constraintlayout/widget/b;->a(II)V

    .line 341
    invoke-virtual {v3, v6, v9, v10, v11}, Landroidx/constraintlayout/widget/b;->a(IIII)V

    .line 342
    invoke-virtual {v3, v8, v9, v6, v11}, Landroidx/constraintlayout/widget/b;->a(IIII)V

    .line 346
    :goto_5
    invoke-virtual {v3, v4}, Landroidx/constraintlayout/widget/b;->b(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 62
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    .line 63
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a()F

    move-result v0

    .line 64
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b()F

    move-result v2

    .line 65
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c()F

    move-result v3

    .line 355
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 356
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredHeight()I

    move-result v8

    .line 357
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 359
    invoke-static {v6, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    const-string v8, "Bitmap.createBitmap(width, height, config)"

    invoke-static {v6, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 360
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 361
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "context"

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v10, 0x7f060066

    .line 363
    invoke-static {v9, v10}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v9

    .line 364
    invoke-virtual {v8, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 366
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 367
    new-instance v10, Landroid/graphics/PorterDuffXfermode;

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v10, v12}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    check-cast v10, Landroid/graphics/Xfermode;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 368
    sget-object v10, Lkotlin/s;->a:Lkotlin/s;

    .line 369
    invoke-virtual {v8, v0, v2, v3, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 374
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 375
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 354
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v4, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 66
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    .line 67
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a()F

    move-result v0

    .line 68
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b()F

    move-result v2

    .line 69
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d()F

    move-result v3

    .line 383
    new-instance v4, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;

    invoke-direct {v4, v3, v0, v2}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;-><init>(FFF)V

    check-cast v4, Landroid/view/ViewOutlineProvider;

    invoke-virtual {p1, v4}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 392
    invoke-virtual {p1, v7}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->setClipToOutline(Z)V

    .line 70
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    .line 71
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a()F

    move-result v0

    .line 72
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b()F

    move-result v2

    .line 73
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c()F

    move-result v3

    .line 74
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d()F

    move-result v1

    .line 396
    invoke-static {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)Landroid/widget/TextView;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setAlpha(F)V

    .line 397
    invoke-static {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->b(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setAlpha(F)V

    .line 398
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 400
    new-array v6, v11, [Landroid/animation/Animator;

    .line 401
    move-object v8, p1

    check-cast v8, Landroid/view/View;

    float-to-int v0, v0

    float-to-int v2, v2

    invoke-static {v8, v0, v2, v3, v1}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    .line 406
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    aput-object v0, v6, v5

    .line 418
    invoke-static {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)Landroid/widget/TextView;

    move-result-object v0

    .line 419
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    .line 412
    invoke-static {v0, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v8, 0x64

    .line 423
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    const-wide/16 v10, 0xc8

    .line 424
    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 425
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    .line 417
    check-cast v0, Landroid/animation/Animator;

    aput-object v0, v6, v7

    .line 432
    invoke-static {p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->b(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)Landroid/widget/TextView;

    move-result-object p1

    .line 433
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    .line 426
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    .line 437
    invoke-virtual {p1, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 438
    invoke-virtual {p1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 439
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 431
    check-cast p1, Landroid/animation/Animator;

    aput-object p1, v6, v2

    .line 400
    invoke-virtual {v4, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 399
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    .line 75
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;->a(Landroid/view/View;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
