.class final synthetic Lcom/swedbank/mobile/app/discovery/c$c;
.super Lkotlin/e/b/i;
.source "FeatureDiscoveryPresenter.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/discovery/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/discovery/d;",
        "Lcom/swedbank/mobile/app/discovery/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/discovery/c$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/discovery/c$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/discovery/c$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/discovery/c$c;->a:Lcom/swedbank/mobile/app/discovery/c$c;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/discovery/d;)Lcom/swedbank/mobile/app/discovery/k;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/discovery/d;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lcom/swedbank/mobile/app/discovery/k;

    .line 15
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/discovery/k;-><init>(Lcom/swedbank/mobile/business/discovery/d;)V

    return-object v0
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/discovery/k;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/swedbank/mobile/business/discovery/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/discovery/c$c;->a(Lcom/swedbank/mobile/business/discovery/d;)Lcom/swedbank/mobile/app/discovery/k;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>(Lcom/swedbank/mobile/business/discovery/FeatureDiscoveryRequest;)V"

    return-object v0
.end method
