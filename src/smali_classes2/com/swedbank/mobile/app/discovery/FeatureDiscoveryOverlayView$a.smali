.class final Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;
.super Ljava/lang/Object;
.source "FeatureDiscoveryOverlayView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:F

.field private final b:F

.field private final c:F

.field private final d:F


# direct methods
.method public constructor <init>(FFFF)V
    .locals 0

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a:F

    iput p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b:F

    iput p3, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c:F

    iput p4, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d:F

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .line 256
    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a:F

    return v0
.end method

.method public final b()F
    .locals 1

    .line 257
    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b:F

    return v0
.end method

.method public final c()F
    .locals 1

    .line 258
    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c:F

    return v0
.end method

.method public final d()F
    .locals 1

    .line 259
    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d:F

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;

    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a:F

    iget v1, p1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b:F

    iget v1, p1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c:F

    iget v1, p1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d:F

    iget p1, p1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RevealTarget(centerX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", centerY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", startRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", endRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
