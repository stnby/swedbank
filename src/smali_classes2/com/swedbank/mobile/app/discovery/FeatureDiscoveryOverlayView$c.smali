.class public final Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;
.super Ljava/lang/Object;
.source "View.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lkotlin/e/a/b;

.field final synthetic c:Landroid/view/ViewTreeObserver;


# direct methods
.method public constructor <init>(Landroid/view/View;Lkotlin/e/a/b;Landroid/view/ViewTreeObserver;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->b:Lkotlin/e/a/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->c:Landroid/view/ViewTreeObserver;

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->b:Lkotlin/e/a/b;

    iget-object v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->c:Landroid/view/ViewTreeObserver;

    const-string v1, "vto"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->c:Landroid/view/ViewTreeObserver;

    move-object v1, p0

    check-cast v1, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :goto_0
    const/4 v0, 0x1

    return v0
.end method
