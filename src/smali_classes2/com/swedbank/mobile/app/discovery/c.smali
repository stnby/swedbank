.class public final Lcom/swedbank/mobile/app/discovery/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "FeatureDiscoveryPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/discovery/h;",
        "Lcom/swedbank/mobile/app/discovery/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/discovery/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/discovery/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/discovery/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/c;->a:Lcom/swedbank/mobile/business/discovery/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/discovery/c;)Lcom/swedbank/mobile/business/discovery/a;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/swedbank/mobile/app/discovery/c;->a:Lcom/swedbank/mobile/business/discovery/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/c;->a:Lcom/swedbank/mobile/business/discovery/a;

    .line 14
    invoke-interface {v0}, Lcom/swedbank/mobile/business/discovery/a;->a()Lio/reactivex/o;

    move-result-object v0

    .line 15
    sget-object v1, Lcom/swedbank/mobile/app/discovery/c$c;->a:Lcom/swedbank/mobile/app/discovery/c$c;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/discovery/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/discovery/d;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 17
    sget-object v1, Lcom/swedbank/mobile/app/discovery/c$a;->a:Lcom/swedbank/mobile/app/discovery/c$a;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/discovery/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 18
    new-instance v2, Lcom/swedbank/mobile/app/discovery/c$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/discovery/c$b;-><init>(Lcom/swedbank/mobile/app/discovery/c;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    check-cast v0, Lio/reactivex/s;

    .line 25
    check-cast v1, Lio/reactivex/s;

    .line 23
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026tream,\n        ackStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
