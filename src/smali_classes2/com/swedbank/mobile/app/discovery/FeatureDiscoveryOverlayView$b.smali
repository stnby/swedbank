.class public final Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;
.super Landroid/view/ViewOutlineProvider;
.source "FeatureDiscoveryOverlayView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:F

.field final synthetic b:F

.field final synthetic c:F


# direct methods
.method public constructor <init>(FFF)V
    .locals 0

    .line 152
    iput p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->a:F

    iput p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->b:F

    iput p3, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->c:F

    invoke-direct {p0}, Landroid/view/ViewOutlineProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/view/View;Landroid/graphics/Outline;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Outline;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "outline"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->a:F

    const/high16 v0, 0x40000000    # 2.0f

    mul-float p1, p1, v0

    .line 155
    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, p1, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 156
    iget p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->b:F

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->a:F

    sub-float/2addr p1, v1

    iget v1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->c:F

    iget v2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$b;->a:F

    sub-float/2addr v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 262
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 263
    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 158
    invoke-virtual {p2, p1}, Landroid/graphics/Outline;->setOval(Landroid/graphics/Rect;)V

    return-void
.end method
