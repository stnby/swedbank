.class public final Lcom/swedbank/mobile/app/discovery/i$a;
.super Lkotlin/e/b/k;
.source "FeatureDiscoveryViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/discovery/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lkotlin/s;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Landroid/view/ViewGroup;

.field final synthetic d:Lcom/swedbank/mobile/app/discovery/i;

.field final synthetic e:I

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/CharSequence;

.field final synthetic h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/swedbank/mobile/app/discovery/i;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/i$a;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/discovery/i$a;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/swedbank/mobile/app/discovery/i$a;->c:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/swedbank/mobile/app/discovery/i$a;->d:Lcom/swedbank/mobile/app/discovery/i;

    iput p5, p0, Lcom/swedbank/mobile/app/discovery/i$a;->e:I

    iput-object p6, p0, Lcom/swedbank/mobile/app/discovery/i$a;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/swedbank/mobile/app/discovery/i$a;->g:Ljava/lang/CharSequence;

    iput-object p8, p0, Lcom/swedbank/mobile/app/discovery/i$a;->h:Ljava/lang/CharSequence;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)V
    .locals 1
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/i$a;->d:Lcom/swedbank/mobile/app/discovery/i;

    invoke-static {p1}, Lcom/swedbank/mobile/app/discovery/i;->a(Lcom/swedbank/mobile/app/discovery/i;)Lcom/b/c/c;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/i$a;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 52
    iget-object p1, p0, Lcom/swedbank/mobile/app/discovery/i$a;->a:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    new-instance v0, Lcom/swedbank/mobile/app/discovery/i$a$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/discovery/i$a$1;-><init>(Lcom/swedbank/mobile/app/discovery/i$a;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->a(Lkotlin/e/a/a;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/discovery/i$a;->a(Lkotlin/s;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
