.class public final Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "FeatureDiscoveryOverlayView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;
    }
.end annotation


# static fields
.field static final synthetic g:[Lkotlin/h/g;


# instance fields
.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:[I

.field private k:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "titleView"

    const-string v4, "getTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "descriptionView"

    const-string v4, "getDescriptionView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->g:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f0a0125

    .line 37
    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->h:Lkotlin/f/c;

    const p2, 0x7f0a0122

    .line 38
    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->i:Lkotlin/f/c;

    const/4 p2, 0x2

    .line 40
    new-array p2, p2, [I

    iput-object p2, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->j:[I

    .line 349
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string p2, "LayoutInflater.from(this)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    move-object p2, p0

    check-cast p2, Landroid/view/ViewGroup;

    const/4 p3, 0x1

    const v0, 0x7f0d00bd

    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string p2, "resources"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    const-string p2, "resources.displayMetrics"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p2, 0x8

    int-to-float p2, p2

    .line 350
    invoke-static {p3, p2, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->setElevation(F)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 34
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 35
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)Landroid/widget/TextView;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getTitleView()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->k:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)Landroid/widget/TextView;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getDescriptionView()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;)[I
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->j:[I

    return-object p0
.end method

.method private final getDescriptionView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->g:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTitleView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->g:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "forView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getDescriptionView()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    new-instance p2, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;

    invoke-direct {p2, p0, p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$d;-><init>(Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;Landroid/view/View;)V

    check-cast p2, Lkotlin/e/a/b;

    .line 79
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredWidth()I

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->getMeasuredHeight()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 80
    :cond_0
    invoke-interface {p2, p0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 262
    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    .line 263
    new-instance p3, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;

    invoke-direct {p3, p0, p2, p1}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$c;-><init>(Landroid/view/View;Lkotlin/e/a/b;Landroid/view/ViewTreeObserver;)V

    check-cast p3, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {p1, p3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :goto_1
    return-void
.end method

.method public final a(Lkotlin/e/a/a;)V
    .locals 5
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "afterAnimation"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView;->k:Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;

    if-eqz v0, :cond_0

    .line 86
    move-object v1, p0

    check-cast v1, Landroid/view/View;

    .line 87
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->a()F

    move-result v2

    float-to-int v2, v2

    .line 88
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->b()F

    move-result v3

    float-to-int v3, v3

    .line 89
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->d()F

    move-result v4

    .line 90
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/discovery/FeatureDiscoveryOverlayView$a;->c()F

    move-result v0

    .line 86
    invoke-static {v1, v2, v3, v4, v0}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    const-wide/16 v1, 0x96

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 274
    new-instance v1, Lcom/swedbank/mobile/core/ui/c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/core/ui/c;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 95
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    :cond_0
    return-void
.end method
