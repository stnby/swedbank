.class public final Lcom/swedbank/mobile/app/h/a/d;
.super Ljava/lang/Object;
.source "ShortcutProviderImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/h/a/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/a/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/app/h/a/d;->a:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/swedbank/mobile/app/h/a/d;->b:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/swedbank/mobile/app/h/a/d;->c:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/swedbank/mobile/app/h/a/d;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/h/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/a/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;)",
            "Lcom/swedbank/mobile/app/h/a/d;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/swedbank/mobile/app/h/a/d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/h/a/d;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/h/a/c;
    .locals 5

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/h/a/c;

    iget-object v1, p0, Lcom/swedbank/mobile/app/h/a/d;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/swedbank/mobile/app/h/a/d;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/a/c;

    iget-object v3, p0, Lcom/swedbank/mobile/app/h/a/d;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/core/a/c;

    iget-object v4, p0, Lcom/swedbank/mobile/app/h/a/d;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/e/i;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/h/a/c;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/core/a/c;Lcom/swedbank/mobile/business/e/i;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/h/a/d;->a()Lcom/swedbank/mobile/app/h/a/c;

    move-result-object v0

    return-object v0
.end method
