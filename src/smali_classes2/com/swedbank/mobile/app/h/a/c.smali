.class public final Lcom/swedbank/mobile/app/h/a/c;
.super Ljava/lang/Object;
.source "ShortcutProviderImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/external/shortcut/f;
.implements Lcom/swedbank/mobile/data/device/h;


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/core/a/c;

.field private final d:Lcom/swedbank/mobile/business/e/i;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/core/a/c;Lcom/swedbank/mobile/business/e/i;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/core/a/c;",
            "Lcom/swedbank/mobile/business/e/i;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flowManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/h/a/c;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/h/a/c;->b:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p3, p0, Lcom/swedbank/mobile/app/h/a/c;->c:Lcom/swedbank/mobile/core/a/c;

    iput-object p4, p0, Lcom/swedbank/mobile/app/h/a/c;->d:Lcom/swedbank/mobile/business/e/i;

    return-void
.end method


# virtual methods
.method public a(ILcom/swedbank/mobile/business/cards/a;)Landroid/content/pm/ShortcutInfo;
    .locals 6
    .param p2    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/a;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lkotlin/j/n;->e(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 38
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/h/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 39
    iget-object v2, p0, Lcom/swedbank/mobile/app/h/a/c;->d:Lcom/swedbank/mobile/business/e/i;

    .line 40
    invoke-interface {v2}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    .line 41
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "shortcut_extra_card_id"

    .line 42
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "deviceRepository\n       \u2026UT_EXTRA_CARD_ID, cardId)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v3, Landroid/content/pm/ShortcutInfo$Builder;

    iget-object v4, p0, Lcom/swedbank/mobile/app/h/a/c;->a:Landroid/app/Application;

    check-cast v4, Landroid/content/Context;

    invoke-direct {v3, v4, v1}, Landroid/content/pm/ShortcutInfo$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 45
    invoke-virtual {v3, p1}, Landroid/content/pm/ShortcutInfo$Builder;->setRank(I)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object p1

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "**** "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/content/pm/ShortcutInfo$Builder;->setShortLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object p1

    .line 47
    iget-object v1, p0, Lcom/swedbank/mobile/app/h/a/c;->a:Landroid/app/Application;

    sget v3, Lcom/swedbank/mobile/app/h/a$a;->shortcut_pay_with_card:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/content/pm/ShortcutInfo$Builder;->setLongLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object p1

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/h/a/c;->a:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    sget-object v1, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/a;->i()Lcom/swedbank/mobile/business/cards/g;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/swedbank/mobile/app/cards/g;->a(Lcom/swedbank/mobile/business/cards/g;)I

    move-result p2

    invoke-static {v0, p2}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/content/pm/ShortcutInfo$Builder;->setIcon(Landroid/graphics/drawable/Icon;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1, v2}, Landroid/content/pm/ShortcutInfo$Builder;->setIntent(Landroid/content/Intent;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Landroid/content/pm/ShortcutInfo$Builder;->build()Landroid/content/pm/ShortcutInfo;

    move-result-object p1

    const-string p2, "ShortcutInfo.Builder(app\u2026yIntent)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "with(card) {\n    val car\u2026ent)\n        .build()\n  }"

    .line 36
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "card_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortcut_extra_card_id"

    .line 54
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const-string v0, "shortcut_extra_card_id"

    .line 55
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/swedbank/mobile/app/h/a/c;->b:Lcom/swedbank/mobile/architect/business/a/c;

    new-instance v1, Lcom/swedbank/mobile/business/external/shortcut/c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/external/shortcut/c;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    .line 57
    iget-object p1, p0, Lcom/swedbank/mobile/app/h/a/c;->c:Lcom/swedbank/mobile/core/a/c;

    sget-object v0, Lcom/swedbank/mobile/app/cards/y;->a:Lcom/swedbank/mobile/app/cards/y;

    check-cast v0, Lcom/swedbank/mobile/core/a/a;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    return v1

    :cond_1
    return v1
.end method
