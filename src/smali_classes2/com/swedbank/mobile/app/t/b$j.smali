.class final synthetic Lcom/swedbank/mobile/app/t/b$j;
.super Lkotlin/e/b/i;
.source "RootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/t/b;->f()Lio/reactivex/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lio/reactivex/x<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/b;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/t/b;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/t/b;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/x;)V
    .locals 2
    .param p1    # Lio/reactivex/x;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/b;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$j;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/t/b;

    .line 231
    new-instance v1, Lcom/swedbank/mobile/app/t/b$l;

    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/app/t/b$l;-><init>(Lcom/swedbank/mobile/app/t/b;Lio/reactivex/x;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;Lkotlin/e/a/b;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lio/reactivex/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b$j;->a(Lio/reactivex/x;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "walletPaymentBackground"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "walletPaymentBackground(Lio/reactivex/SingleEmitter;)V"

    return-object v0
.end method
