.class final Lcom/swedbank/mobile/app/t/b$a$a;
.super Lkotlin/e/b/k;
.source "RootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/t/b$a;->a(Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/a/c/e/b;",
        "Lcom/swedbank/mobile/app/b/a/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/t/b$a;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/t/b$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/t/b$a$a;->a:Lcom/swedbank/mobile/app/t/b$a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/app/b/a/c;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/a/c/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "sessionModule"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$a$a;->a:Lcom/swedbank/mobile/app/t/b$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    .line 231
    sget-object v1, Lcom/swedbank/mobile/app/t/c;->a:Lcom/swedbank/mobile/app/t/c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$a$a;->a:Lcom/swedbank/mobile/app/t/b$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v0}, Lcom/swedbank/mobile/app/t/b;->f(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/b/a/a;

    move-result-object v0

    .line 130
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/b/a/a;->a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/app/b/a/a;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/b/a/a;->b()Lcom/swedbank/mobile/a/c/a/a;

    move-result-object p1

    .line 133
    invoke-interface {p1}, Lcom/swedbank/mobile/a/c/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/b/a/c;

    .line 134
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a$a;->a:Lcom/swedbank/mobile/app/t/b$a;

    iget-object v1, v1, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v1}, Lcom/swedbank/mobile/app/t/b;->g(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/a/c/a/b;

    move-result-object v1

    .line 135
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/a/c/a/b;->a(Lcom/swedbank/mobile/a/c/a/a;)Lio/reactivex/b/c;

    move-result-object p1

    .line 134
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/b/a/c;->a(Lio/reactivex/b/c;)V

    .line 136
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/b/a/c;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/authentication/authenticated/c;

    .line 137
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a$a;->a:Lcom/swedbank/mobile/app/t/b$a;

    iget-object v1, v1, Lcom/swedbank/mobile/app/t/b$a;->c:Lio/reactivex/k;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a$a;->a:Lcom/swedbank/mobile/app/t/b$a;

    iget-object v1, v1, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v1}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;)Lcom/b/c/c;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/a/c/e/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b$a$a;->a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/app/b/a/c;

    move-result-object p1

    return-object p1
.end method
