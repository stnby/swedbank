.class final Lcom/swedbank/mobile/app/t/b$d;
.super Lkotlin/e/b/k;
.source "RootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/a/h;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/t/b;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/session/n;

.field final synthetic c:Lio/reactivex/k;

.field final synthetic d:Lkotlin/e/a/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/t/b$d;->a:Lcom/swedbank/mobile/app/t/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/t/b$d;->b:Lcom/swedbank/mobile/business/authentication/session/n;

    iput-object p3, p0, Lcom/swedbank/mobile/app/t/b$d;->c:Lio/reactivex/k;

    iput-object p4, p0, Lcom/swedbank/mobile/app/t/b$d;->d:Lkotlin/e/a/b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$d;->c:Lio/reactivex/k;

    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$d;->d:Lkotlin/e/a/b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    invoke-interface {v1, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 170
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$d;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {p1}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;)Lcom/b/c/c;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/b/c/c;->i()Lio/reactivex/j;

    move-result-object p1

    .line 172
    new-instance v0, Lcom/swedbank/mobile/app/t/b$d$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/t/b$d$b;-><init>(Lcom/swedbank/mobile/app/t/b$d;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {p1, v0}, Lio/reactivex/j;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    .line 173
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$d;->c:Lio/reactivex/k;

    invoke-interface {v0, p1}, Lio/reactivex/k;->a(Lio/reactivex/b/c;)V

    .line 174
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$d;->b:Lcom/swedbank/mobile/business/authentication/session/n;

    if-eqz v0, :cond_1

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    const-string v1, "authenticatedNodeAcquisitionDisposable"

    .line 175
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    .line 176
    new-instance p1, Lcom/swedbank/mobile/app/t/b$d$a;

    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$d;->c:Lio/reactivex/k;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/app/t/b$d$a;-><init>(Lio/reactivex/k;)V

    check-cast p1, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/app/t/d;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/t/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object p1

    const-string v1, "Disposables.fromAction(emitter::onComplete)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    :goto_0
    return-void

    .line 174
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.architect.app.Router"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b$d;->a(Lcom/swedbank/mobile/architect/a/h;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
