.class final Lcom/swedbank/mobile/app/t/b$a;
.super Lkotlin/e/b/k;
.source "RootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/t/b;->a(Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/t/b;

.field final synthetic b:Lcom/swedbank/mobile/business/authentication/g;

.field final synthetic c:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/g;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/t/b$a;->b:Lcom/swedbank/mobile/business/authentication/g;

    iput-object p3, p0, Lcom/swedbank/mobile/app/t/b$a;->c:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    check-cast p1, Ljava/lang/Iterable;

    .line 231
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 122
    instance-of v1, v1, Lcom/swedbank/mobile/business/authentication/session/n;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 233
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-nez v0, :cond_3

    .line 143
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    .line 124
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v0}, Lcom/swedbank/mobile/app/t/b;->d(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/b/e/a;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v1}, Lcom/swedbank/mobile/app/t/b;->e(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/business/authentication/session/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/e/a;->a(Lcom/swedbank/mobile/business/authentication/session/l;)Lcom/swedbank/mobile/app/b/e/a;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a;->b:Lcom/swedbank/mobile/business/authentication/g;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/e/a;->a(Lcom/swedbank/mobile/business/authentication/g;)Lcom/swedbank/mobile/app/b/e/a;

    move-result-object v0

    .line 127
    new-instance v1, Lcom/swedbank/mobile/app/t/b$a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/t/b$a$a;-><init>(Lcom/swedbank/mobile/app/t/b$a;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/b/e/a;->a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/b/e/a;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/b/e/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a;->c:Lio/reactivex/k;

    if-eqz v1, :cond_2

    .line 147
    new-instance v1, Lcom/swedbank/mobile/app/t/b$a$b;

    iget-object v2, p0, Lcom/swedbank/mobile/app/t/b$a;->c:Lio/reactivex/k;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/t/b$a$b;-><init>(Lio/reactivex/k;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/t/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/t/d;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(emitter::onComplete)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    .line 234
    :cond_2
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 151
    :cond_3
    instance-of p1, v0, Lcom/swedbank/mobile/business/authentication/session/n;

    if-eqz p1, :cond_5

    .line 152
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$a;->c:Lio/reactivex/k;

    if-eqz p1, :cond_4

    .line 153
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$a;->a:Lcom/swedbank/mobile/app/t/b;

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/n;

    .line 154
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$a;->c:Lio/reactivex/k;

    .line 155
    sget-object v2, Lcom/swedbank/mobile/app/t/b$a$c;->a:Lcom/swedbank/mobile/app/t/b$a$c;

    check-cast v2, Lkotlin/e/a/b;

    .line 153
    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V

    :cond_4
    :goto_1
    return-void

    .line 157
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Session node is not missing and not instance of SessionRouter"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b$a;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
