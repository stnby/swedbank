.class public final Lcom/swedbank/mobile/app/t/b$l;
.super Lkotlin/e/b/k;
.source "RootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/t/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/t/b;

.field final synthetic b:Lio/reactivex/x;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/t/b;Lio/reactivex/x;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/t/b$l;->a:Lcom/swedbank/mobile/app/t/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/t/b$l;->b:Lio/reactivex/x;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$withInspectingChildren"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    check-cast p1, Ljava/lang/Iterable;

    .line 231
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 192
    instance-of v1, v1, Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 233
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-nez v0, :cond_3

    .line 197
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$l;->a:Lcom/swedbank/mobile/app/t/b;

    .line 194
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$l;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v0}, Lcom/swedbank/mobile/app/t/b;->h(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/cards/f/b/b/b;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$l;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v1}, Lcom/swedbank/mobile/app/t/b;->i(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/b/b;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/a;)Lcom/swedbank/mobile/app/cards/f/b/b/b;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/b/b/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$l;->b:Lio/reactivex/x;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    .line 234
    :cond_2
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 199
    :cond_3
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$l;->b:Lio/reactivex/x;

    if-eqz p1, :cond_4

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b$l;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
