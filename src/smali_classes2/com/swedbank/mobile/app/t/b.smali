.class public final Lcom/swedbank/mobile/app/t/b;
.super Lcom/swedbank/mobile/business/i/f;
.source "RootRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/j/d;
.implements Lcom/swedbank/mobile/business/root/d;


# instance fields
.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/authentication/notauth/d;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/data/device/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/app/b/e/a;

.field private final j:Lcom/swedbank/mobile/app/b/a/a;

.field private final k:Lcom/swedbank/mobile/app/b/c/b;

.field private final l:Lcom/swedbank/mobile/app/cards/f/b/b/b;

.field private final m:Lcom/swedbank/mobile/app/n/a;

.field private final n:Lcom/swedbank/mobile/a/c/a/b;

.field private final o:Lcom/swedbank/mobile/app/g/g;

.field private final p:Lcom/swedbank/mobile/core/a/c;

.field private final q:Lcom/swedbank/mobile/business/authentication/session/l;

.field private final r:Lcom/swedbank/mobile/business/cards/wallet/payment/a;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/app/b/e/a;Lcom/swedbank/mobile/app/b/a/a;Lcom/swedbank/mobile/app/b/c/b;Lcom/swedbank/mobile/app/cards/f/b/b/b;Lcom/swedbank/mobile/app/n/a;Lcom/swedbank/mobile/a/c/a/b;Lcom/swedbank/mobile/app/g/g;Lcom/swedbank/mobile/core/a/c;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/cards/wallet/payment/a;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 19
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/b/e/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/b/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/b/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/app/cards/f/b/b/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/app/n/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/a/c/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/app/g/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_root"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/business/cards/wallet/payment/a;
        .annotation runtime Ljavax/inject/Named;
            value = "for_root"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p13    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_root"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p14    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/app/b/e/a;",
            "Lcom/swedbank/mobile/app/b/a/a;",
            "Lcom/swedbank/mobile/app/b/c/b;",
            "Lcom/swedbank/mobile/app/cards/f/b/b/b;",
            "Lcom/swedbank/mobile/app/n/a;",
            "Lcom/swedbank/mobile/a/c/a/b;",
            "Lcom/swedbank/mobile/app/g/g;",
            "Lcom/swedbank/mobile/core/a/c;",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/a;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    move-object/from16 v5, p10

    move-object/from16 v4, p11

    move-object/from16 v3, p12

    const-string v0, "context"

    invoke-static {v7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityManager"

    invoke-static {v8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionBuilder"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticatedBuilder"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notAuthenticatedBuilder"

    invoke-static {v11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentBackgroundBuilder"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notifyUserBuilder"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticatedComponentHandler"

    invoke-static {v14, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backManager"

    invoke-static {v15, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsManager"

    invoke-static {v5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionListener"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentBackgroundListener"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    move-object/from16 v1, p13

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    move-object/from16 v2, p14

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v17, 0x4

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v3, v16

    move/from16 v4, v17

    move-object/from16 v5, v18

    .line 61
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object v7, v6, Lcom/swedbank/mobile/app/t/b;->g:Landroid/app/Application;

    iput-object v8, v6, Lcom/swedbank/mobile/app/t/b;->h:Lcom/swedbank/mobile/data/device/a;

    iput-object v9, v6, Lcom/swedbank/mobile/app/t/b;->i:Lcom/swedbank/mobile/app/b/e/a;

    iput-object v10, v6, Lcom/swedbank/mobile/app/t/b;->j:Lcom/swedbank/mobile/app/b/a/a;

    iput-object v11, v6, Lcom/swedbank/mobile/app/t/b;->k:Lcom/swedbank/mobile/app/b/c/b;

    iput-object v12, v6, Lcom/swedbank/mobile/app/t/b;->l:Lcom/swedbank/mobile/app/cards/f/b/b/b;

    iput-object v13, v6, Lcom/swedbank/mobile/app/t/b;->m:Lcom/swedbank/mobile/app/n/a;

    iput-object v14, v6, Lcom/swedbank/mobile/app/t/b;->n:Lcom/swedbank/mobile/a/c/a/b;

    iput-object v15, v6, Lcom/swedbank/mobile/app/t/b;->o:Lcom/swedbank/mobile/app/g/g;

    move-object/from16 v0, p10

    iput-object v0, v6, Lcom/swedbank/mobile/app/t/b;->p:Lcom/swedbank/mobile/core/a/c;

    move-object/from16 v0, p11

    iput-object v0, v6, Lcom/swedbank/mobile/app/t/b;->q:Lcom/swedbank/mobile/business/authentication/session/l;

    move-object/from16 v0, p12

    iput-object v0, v6, Lcom/swedbank/mobile/app/t/b;->r:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    .line 62
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<AuthenticatedNode>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v6, Lcom/swedbank/mobile/app/t/b;->e:Lcom/b/c/c;

    .line 63
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<NotAuthenticatedNode>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v6, Lcom/swedbank/mobile/app/t/b;->f:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/t/b;)Lcom/b/c/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->e:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/t/b;Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/t/b;->a(Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;)V

    return-void
.end method

.method static synthetic a(Lcom/swedbank/mobile/app/t/b;Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 119
    check-cast p1, Lio/reactivex/k;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/t/b;->a(Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/t/b;Lkotlin/e/a/b;)V
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method private final a(Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/session/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/swedbank/mobile/business/authentication/session/n;",
            "Lio/reactivex/k<",
            "TT;>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            "+TT;>;)V"
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/swedbank/mobile/app/t/b$d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/t/b$d;-><init>(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/business/authentication/session/n;->a(Lkotlin/e/a/b;)V

    return-void
.end method

.method private final a(Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/k<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ")V"
        }
    .end annotation

    .line 121
    new-instance v0, Lcom/swedbank/mobile/app/t/b$a;

    invoke-direct {v0, p0, p2, p1}, Lcom/swedbank/mobile/app/t/b$a;-><init>(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/g;Lio/reactivex/k;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/t/b;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/t/b;)Lcom/b/c/c;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->f:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/b/c/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->k:Lcom/swedbank/mobile/app/b/c/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/b/e/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->i:Lcom/swedbank/mobile/app/b/e/a;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/business/authentication/session/l;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->q:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/b/a/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->j:Lcom/swedbank/mobile/app/b/a/a;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/a/c/a/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->n:Lcom/swedbank/mobile/a/c/a/b;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/cards/f/b/b/b;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->l:Lcom/swedbank/mobile/app/cards/f/b/b/b;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/business/cards/wallet/payment/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->r:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/t/b;)Lcom/swedbank/mobile/app/n/a;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/swedbank/mobile/app/t/b;->m:Lcom/swedbank/mobile/app/n/a;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/util/e<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            "Lcom/swedbank/mobile/business/authentication/notauth/d;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 65
    new-instance v0, Lcom/swedbank/mobile/app/t/b$h;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/t/b$h;-><init>(Lcom/swedbank/mobile/app/t/b;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026tionDisposable)\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/f;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/f;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/f;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 106
    new-instance v0, Lcom/swedbank/mobile/app/t/b$f;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/t/b$f;-><init>(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/f;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.create { emitter -\u2026ut(explanation)\n    )\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/authentication/g;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/authentication/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/authentication/g;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/authentication/authenticated/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/swedbank/mobile/app/t/b$g;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/t/b$g;-><init>(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/g;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe\n      .create { em\u2026edState(emitter, input) }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/e/l;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/e/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootReason"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b;->p:Lcom/swedbank/mobile/core/a/c;

    new-instance v1, Lcom/swedbank/mobile/app/g/j;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v1, p1, v2, v3, v2}, Lcom/swedbank/mobile/app/g/j;-><init>(Lcom/swedbank/mobile/business/e/l;Lcom/swedbank/mobile/core/a/b;ILkotlin/e/b/g;)V

    check-cast v1, Lcom/swedbank/mobile/core/a/a;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 91
    new-instance v0, Lcom/swedbank/mobile/app/t/b$k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/t/b$k;-><init>(Lcom/swedbank/mobile/app/t/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/t/b;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {p0, p1}, Lcom/swedbank/mobile/app/j/d$a;->a(Lcom/swedbank/mobile/app/j/d;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 3

    .line 102
    new-instance v0, Lcom/swedbank/mobile/business/authentication/g$c;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, v2}, Lcom/swedbank/mobile/business/authentication/g$c;-><init>(Lcom/swedbank/mobile/business/authentication/f;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/business/authentication/g;

    .line 101
    invoke-static {p0, v2, v0, v1, v2}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;Lio/reactivex/k;Lcom/swedbank/mobile/business/authentication/g;ILjava/lang/Object;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 231
    sget-object v0, Lcom/swedbank/mobile/app/t/b$b;->a:Lcom/swedbank/mobile/app/t/b$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public e()V
    .locals 2

    const/4 v0, 0x0

    .line 233
    check-cast v0, Lio/reactivex/x;

    .line 234
    new-instance v1, Lcom/swedbank/mobile/app/t/b$l;

    invoke-direct {v1, p0, v0}, Lcom/swedbank/mobile/app/t/b$l;-><init>(Lcom/swedbank/mobile/app/t/b;Lio/reactivex/x;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {p0, v1}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;Lkotlin/e/a/b;)V

    return-void
.end method

.method public f()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 188
    new-instance v0, Lcom/swedbank/mobile/app/t/b$j;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/app/t/b;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/t/b$j;-><init>(Lcom/swedbank/mobile/app/t/b;)V

    check-cast v0, Lkotlin/e/a/b;

    new-instance v1, Lcom/swedbank/mobile/app/t/e;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/t/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v1, Lio/reactivex/z;

    invoke-static {v1}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single\n      .create(thi\u2026:walletPaymentBackground)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()V
    .locals 1

    .line 238
    sget-object v0, Lcom/swedbank/mobile/app/t/b$c;->a:Lcom/swedbank/mobile/app/t/b$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public h()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/notify/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 213
    new-instance v0, Lcom/swedbank/mobile/app/t/b$i;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/t/b$i;-><init>(Lcom/swedbank/mobile/app/t/b;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026      }\n      }\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Landroid/app/Application;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b;->g:Landroid/app/Application;

    return-object v0
.end method

.method public j()Lcom/swedbank/mobile/data/device/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b;->h:Lcom/swedbank/mobile/data/device/a;

    return-object v0
.end method

.method public k()Lio/reactivex/b;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 228
    new-instance v0, Lcom/swedbank/mobile/app/t/b$e;

    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b;->o:Lcom/swedbank/mobile/app/g/g;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/t/b$e;-><init>(Lcom/swedbank/mobile/app/g/g;)V

    check-cast v0, Lkotlin/e/a/a;

    new-instance v1, Lcom/swedbank/mobile/app/t/f;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/t/f;-><init>(Lkotlin/e/a/a;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable\n      .fromA\u2026r::requestActivityFinish)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
