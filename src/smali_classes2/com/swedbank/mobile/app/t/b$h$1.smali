.class final Lcom/swedbank/mobile/app/t/b$h$1;
.super Lkotlin/e/b/k;
.source "RootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/t/b$h;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/t/b$h;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/t/b$h;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/t/b$h$1;->a:Lcom/swedbank/mobile/app/t/b$h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/t/b$h$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    check-cast p1, Ljava/lang/Iterable;

    .line 231
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 69
    instance-of v1, v0, Lcom/swedbank/mobile/business/authentication/session/n;

    if-eqz v1, :cond_1

    .line 70
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$h$1;->a:Lcom/swedbank/mobile/app/t/b$h;

    iget-object p1, p1, Lcom/swedbank/mobile/app/t/b$h;->a:Lcom/swedbank/mobile/app/t/b;

    check-cast v0, Lcom/swedbank/mobile/business/authentication/session/n;

    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$h$1;->b:Lio/reactivex/k;

    const-string v2, "emitter"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/swedbank/mobile/app/t/b$h$1$a;->a:Lcom/swedbank/mobile/app/t/b$h$1$a;

    check-cast v2, Lkotlin/e/a/b;

    invoke-static {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;Lcom/swedbank/mobile/business/authentication/session/n;Lio/reactivex/k;Lkotlin/e/a/b;)V

    return-void

    .line 75
    :cond_1
    instance-of v1, v0, Lcom/swedbank/mobile/business/authentication/notauth/e;

    if-eqz v1, :cond_0

    .line 76
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$h$1;->b:Lio/reactivex/k;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    return-void

    .line 83
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/app/t/b$h$1;->a:Lcom/swedbank/mobile/app/t/b$h;

    iget-object p1, p1, Lcom/swedbank/mobile/app/t/b$h;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {p1}, Lcom/swedbank/mobile/app/t/b;->a(Lcom/swedbank/mobile/app/t/b;)Lcom/b/c/c;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/app/t/b$h$1$b;->a:Lcom/swedbank/mobile/app/t/b$h$1$b;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/swedbank/mobile/app/t/h;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/t/h;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_3
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 84
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$h$1;->a:Lcom/swedbank/mobile/app/t/b$h;

    iget-object v0, v0, Lcom/swedbank/mobile/app/t/b$h;->a:Lcom/swedbank/mobile/app/t/b;

    invoke-static {v0}, Lcom/swedbank/mobile/app/t/b;->b(Lcom/swedbank/mobile/app/t/b;)Lcom/b/c/c;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/app/t/b$h$1$c;->a:Lcom/swedbank/mobile/app/t/b$h$1$c;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_4

    new-instance v2, Lcom/swedbank/mobile/app/t/h;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/t/h;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_4
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 82
    invoke-static {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lio/reactivex/o;->i()Lio/reactivex/j;

    move-result-object p1

    .line 86
    new-instance v0, Lcom/swedbank/mobile/app/t/b$h$1$d;

    iget-object v1, p0, Lcom/swedbank/mobile/app/t/b$h$1;->b:Lio/reactivex/k;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/t/b$h$1$d;-><init>(Lio/reactivex/k;)V

    check-cast v0, Lkotlin/e/a/b;

    new-instance v1, Lcom/swedbank/mobile/app/t/g;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/t/g;-><init>(Lkotlin/e/a/b;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {p1, v1}, Lio/reactivex/j;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object p1

    .line 87
    iget-object v0, p0, Lcom/swedbank/mobile/app/t/b$h$1;->b:Lio/reactivex/k;

    invoke-interface {v0, p1}, Lio/reactivex/k;->a(Lio/reactivex/b/c;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/t/b$h$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
