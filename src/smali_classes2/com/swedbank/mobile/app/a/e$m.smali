.class public final Lcom/swedbank/mobile/app/a/e$m;
.super Lkotlin/e/b/k;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/app/a/e$b;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/a/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/a/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/e$m;->a:Lcom/swedbank/mobile/app/a/e;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/a/e$b;)V
    .locals 3

    .line 112
    instance-of v0, p1, Lcom/swedbank/mobile/app/a/e$b$c;

    if-eqz v0, :cond_0

    .line 113
    check-cast p1, Lcom/swedbank/mobile/app/a/e$b$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/a/e$b$c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/a/e$b$c;->c()Ljava/util/Map;

    move-result-object p1

    .line 114
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Logging state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/swedbank/mobile/app/a/e$m;->a:Lcom/swedbank/mobile/app/a/e;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a/e;->a(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/app/a/a;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/swedbank/mobile/app/a/a;->b(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 117
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/a/e$b$a;

    if-eqz v0, :cond_1

    .line 118
    check-cast p1, Lcom/swedbank/mobile/app/a/e$b$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/a/e$b$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/a/e$b$a;->b()Ljava/util/Map;

    move-result-object p1

    .line 119
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Logging action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    iget-object v1, p0, Lcom/swedbank/mobile/app/a/e$m;->a:Lcom/swedbank/mobile/app/a/e;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a/e;->a(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/app/a/a;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/swedbank/mobile/app/a/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/swedbank/mobile/app/a/e$b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$m;->a(Lcom/swedbank/mobile/app/a/e$b;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
