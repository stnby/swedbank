.class public final Lcom/swedbank/mobile/app/a/e$e;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lio/reactivex/c/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/d<",
        "Lkotlin/k<",
        "+",
        "Lcom/swedbank/mobile/app/a/e$b$c;",
        "+",
        "Lcom/swedbank/mobile/app/a/e$a;",
        ">;",
        "Lkotlin/k<",
        "+",
        "Lcom/swedbank/mobile/app/a/e$b$c;",
        "+",
        "Lcom/swedbank/mobile/app/a/e$a;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/a/e$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/a/e$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/a/e$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/a/e$e;->a:Lcom/swedbank/mobile/app/a/e$e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 47
    check-cast p1, Lkotlin/k;

    check-cast p2, Lkotlin/k;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/a/e$e;->a(Lkotlin/k;Lkotlin/k;)Z

    move-result p1

    return p1
.end method

.method public final a(Lkotlin/k;Lkotlin/k;)Z
    .locals 1
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/app/a/e$b$c;",
            "Lcom/swedbank/mobile/app/a/e$a;",
            ">;",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/app/a/e$b$c;",
            "Lcom/swedbank/mobile/app/a/e$a;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<name for destructuring parameter 1>"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/a/e$a;

    invoke-virtual {p2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/app/a/e$a;

    .line 133
    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
