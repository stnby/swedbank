.class public final Lcom/swedbank/mobile/app/a/e;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/a/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/a/e$a;,
        Lcom/swedbank/mobile/app/a/e$b;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private a:Lio/reactivex/b/c;

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/app/a/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/app/Application;

.field private final d:Lcom/swedbank/mobile/architect/business/a;

.field private final e:Lcom/swedbank/mobile/business/c/b;

.field private final f:Lcom/swedbank/mobile/business/c/a;

.field private final g:Lcom/swedbank/mobile/business/e/i;

.field private final h:Lcom/swedbank/mobile/business/firebase/c;

.field private final i:Lcom/swedbank/mobile/business/f/a;

.field private final j:Lcom/squareup/moshi/n;

.field private final k:Lcom/swedbank/mobile/app/a/a;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/f/a;Lcom/squareup/moshi/n;Lcom/swedbank/mobile/app/a/a;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/firebase/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/app/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessTree"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appPreferenceRepository"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseRepository"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureRepository"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adobeAnalytics"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/e;->c:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/a/e;->d:Lcom/swedbank/mobile/architect/business/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/a/e;->e:Lcom/swedbank/mobile/business/c/b;

    iput-object p4, p0, Lcom/swedbank/mobile/app/a/e;->f:Lcom/swedbank/mobile/business/c/a;

    iput-object p5, p0, Lcom/swedbank/mobile/app/a/e;->g:Lcom/swedbank/mobile/business/e/i;

    iput-object p6, p0, Lcom/swedbank/mobile/app/a/e;->h:Lcom/swedbank/mobile/business/firebase/c;

    iput-object p7, p0, Lcom/swedbank/mobile/app/a/e;->i:Lcom/swedbank/mobile/business/f/a;

    iput-object p8, p0, Lcom/swedbank/mobile/app/a/e;->j:Lcom/squareup/moshi/n;

    iput-object p9, p0, Lcom/swedbank/mobile/app/a/e;->k:Lcom/swedbank/mobile/app/a/a;

    .line 59
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<ClickEvent>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/e;->b:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/app/a/a;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->k:Lcom/swedbank/mobile/app/a/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/firebase/c;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->h:Lcom/swedbank/mobile/business/firebase/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/a/e;)Lcom/b/c/c;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->b:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/a/e;)Landroid/app/Application;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->c:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/architect/business/a;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->d:Lcom/swedbank/mobile/architect/business/a;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/a/e;)Lcom/squareup/moshi/n;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->j:Lcom/squareup/moshi/n;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->i:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/c/b;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->e:Lcom/swedbank/mobile/business/c/b;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/c/a;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->f:Lcom/swedbank/mobile/business/c/a;

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/a/e;->g:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/b/c;
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 256
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->g(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v0

    const-string v1, "feature_adobe_analytics"

    .line 257
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/app/a/e;->k:Lcom/swedbank/mobile/app/a/a;

    invoke-interface {v0}, Lcom/swedbank/mobile/app/a/a;->a()V

    .line 259
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->g(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v0

    const-string v1, "feature_adobe_analytics"

    .line 260
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->e(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/architect/business/a;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/business/a;->a()Lio/reactivex/o;

    move-result-object v0

    .line 275
    sget-object v1, Lcom/swedbank/mobile/app/a/e$g;->a:Lcom/swedbank/mobile/app/a/e$g;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 274
    new-instance v1, Lcom/swedbank/mobile/app/a/e$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/a/e$h;-><init>(Lcom/swedbank/mobile/app/a/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 273
    sget-object v1, Lcom/swedbank/mobile/app/a/e$i;->a:Lcom/swedbank/mobile/app/a/e$i;

    check-cast v1, Lio/reactivex/c/c;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 272
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/v;)Lio/reactivex/o;

    move-result-object v0

    .line 271
    new-instance v1, Lcom/swedbank/mobile/app/a/e$j;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/app/a/e;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/a/e$j;-><init>(Lcom/swedbank/mobile/app/a/e;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/a/h;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/a/h;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 270
    sget-object v1, Lcom/swedbank/mobile/app/a/e$k;->a:Lcom/swedbank/mobile/app/a/e$k;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 268
    new-instance v1, Lcom/swedbank/mobile/app/a/e$l;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/a/e$l;-><init>(Lcom/swedbank/mobile/app/a/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v0, "observeStateChanges()\n  \u2026Stream.cast()))\n        }"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    new-instance v0, Lcom/swedbank/mobile/app/a/e$m;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/a/e$m;-><init>(Lcom/swedbank/mobile/app/a/e;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v4, 0x0

    .line 278
    new-instance v0, Lcom/swedbank/mobile/app/a/e$n;

    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->b(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/firebase/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/a/e$n;-><init>(Lcom/swedbank/mobile/business/firebase/c;)V

    move-object v3, v0

    check-cast v3, Lkotlin/e/a/b;

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 267
    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 66
    :goto_0
    iput-object v0, p0, Lcom/swedbank/mobile/app/a/e;->a:Lio/reactivex/b/c;

    return-object v0
.end method

.method public a(ILcom/swedbank/mobile/core/a/f;)V
    .locals 9
    .param p2    # Lcom/swedbank/mobile/core/a/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "analyticsViewType"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 331
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->g(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v0

    const-string v1, "feature_adobe_analytics"

    .line 332
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/a/e;->b:Lcom/b/c/c;

    new-instance v8, Lcom/swedbank/mobile/app/a/e$a;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v8

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/app/a/e$a;-><init>(ILcom/swedbank/mobile/core/a/f;JILkotlin/e/b/g;)V

    invoke-virtual {v0, v8}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 329
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->g(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v0

    const-string v1, "feature_adobe_analytics"

    .line 330
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 83
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const/4 v0, -0x1

    if-ne v2, v0, :cond_1

    return-void

    .line 87
    :cond_1
    instance-of v0, p1, Landroid/widget/Button;

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    instance-of p1, p1, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    if-eqz p1, :cond_3

    :goto_0
    sget-object p1, Lcom/swedbank/mobile/core/a/f;->a:Lcom/swedbank/mobile/core/a/f;

    :goto_1
    move-object v3, p1

    goto :goto_2

    .line 88
    :cond_3
    sget-object p1, Lcom/swedbank/mobile/core/a/f;->b:Lcom/swedbank/mobile/core/a/f;

    goto :goto_1

    .line 90
    :goto_2
    iget-object p1, p0, Lcom/swedbank/mobile/app/a/e;->b:Lcom/b/c/c;

    new-instance v0, Lcom/swedbank/mobile/app/a/e$a;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/app/a/e$a;-><init>(ILcom/swedbank/mobile/core/a/f;JILkotlin/e/b/g;)V

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/a/a;)V
    .locals 6
    .param p1    # Lcom/swedbank/mobile/core/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-interface {p1}, Lcom/swedbank/mobile/core/a/a;->b()Lcom/swedbank/mobile/core/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/a/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 72
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Logging analytics event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lcom/swedbank/mobile/app/a/e;->h:Lcom/swedbank/mobile/business/firebase/c;

    invoke-interface {p1}, Lcom/swedbank/mobile/core/a/a;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/swedbank/mobile/business/firebase/c;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 283
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->g(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v1

    const-string v2, "feature_adobe_analytics"

    .line 284
    invoke-interface {v1, v2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 75
    invoke-interface {p1}, Lcom/swedbank/mobile/core/a/a;->a()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 291
    new-instance v1, Landroidx/c/a;

    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v2

    invoke-direct {v1, v2}, Landroidx/c/a;-><init>(I)V

    .line 292
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Ljava/lang/Iterable;

    .line 293
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 294
    move-object v4, v1

    check-cast v4, Landroidx/c/g;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 295
    invoke-virtual {v4, v3, v5}, Landroidx/c/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 291
    :cond_0
    check-cast v1, Ljava/util/Map;

    goto :goto_1

    .line 290
    :cond_1
    new-instance p1, Landroidx/c/a;

    invoke-direct {p1}, Landroidx/c/a;-><init>()V

    move-object v1, p1

    check-cast v1, Ljava/util/Map;

    .line 299
    :goto_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->h(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/c/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/b;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object p1

    .line 300
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).toLowerCase()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->i(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v3

    .line 306
    invoke-interface {v3}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/c/g;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 304
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "(this as java.lang.String).toLowerCase()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "app.platform.bankId"

    .line 310
    sget-object v5, Lcom/swedbank/mobile/app/a/f;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result p1

    aget p1, v5, p1

    packed-switch p1, :pswitch_data_0

    .line 313
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const-string p1, "0210"

    goto :goto_2

    :pswitch_1
    const-string p1, "0110"

    goto :goto_2

    :pswitch_2
    const-string p1, "0010"

    :goto_2
    invoke-interface {v1, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "app.platform.name"

    .line 315
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "-android-private"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "app.platform.deviceType"

    const-string v4, "app-native"

    .line 316
    invoke-interface {v1, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "app.platform.version"

    .line 317
    invoke-static {p0}, Lcom/swedbank/mobile/app/a/e;->j(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v4

    invoke-interface {v4}, Lcom/swedbank/mobile/business/e/i;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "app.platform.country"

    .line 318
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "app.platform.language"

    .line 319
    invoke-interface {v1, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "app.user.type"

    const-string v2, "private"

    .line 320
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string v2, "app.weekday"

    .line 322
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v3

    const-string v4, "dayOfWeek"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "app.day"

    .line 323
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "app.month"

    .line 324
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object p1, p0, Lcom/swedbank/mobile/app/a/e;->k:Lcom/swedbank/mobile/app/a/a;

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/app/a/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_3

    .line 304
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 300
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
