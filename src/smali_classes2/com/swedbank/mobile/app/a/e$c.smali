.class public final Lcom/swedbank/mobile/app/a/e$c;
.super Lkotlin/e/b/k;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/a/e$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/a/e$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/a/e$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/a/e$c;->a:Lcom/swedbank/mobile/app/a/e$c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->b()Ljava/lang/Class;

    move-result-object p1

    if-eqz p1, :cond_0

    const-class v0, Lcom/swedbank/mobile/business/navigation/j;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$c;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
