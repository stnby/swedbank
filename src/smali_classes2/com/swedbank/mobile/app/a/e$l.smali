.class public final Lcom/swedbank/mobile/app/a/e$l;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/a/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/a/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/e$l;->a:Lcom/swedbank/mobile/app/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/a/e$b;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/a/e$b;",
            ">;"
        }
    .end annotation

    const-string v0, "stateStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    move-object v0, p1

    check-cast v0, Lio/reactivex/s;

    .line 107
    iget-object v1, p0, Lcom/swedbank/mobile/app/a/e$l;->a:Lcom/swedbank/mobile/app/a/e;

    .line 256
    const-class v2, Lcom/swedbank/mobile/app/a/e$b$c;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->a(Ljava/lang/Class;)Lio/reactivex/o;

    move-result-object p1

    const-string v2, "cast(R::class.java)"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    sget-object v2, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 267
    invoke-static {v1}, Lcom/swedbank/mobile/app/a/e;->c(Lcom/swedbank/mobile/app/a/e;)Lcom/b/c/c;

    move-result-object v3

    check-cast v3, Lio/reactivex/o;

    .line 265
    invoke-virtual {v2, p1, v3}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    .line 264
    sget-object v2, Lcom/swedbank/mobile/app/a/e$e;->a:Lcom/swedbank/mobile/app/a/e$e;

    check-cast v2, Lio/reactivex/c/d;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object p1

    .line 263
    new-instance v2, Lcom/swedbank/mobile/app/a/e$f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/a/e$f;-><init>(Lcom/swedbank/mobile/app/a/e;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 268
    check-cast p1, Lio/reactivex/s;

    .line 105
    invoke-static {v0, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$l;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
