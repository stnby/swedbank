.class public final Lcom/swedbank/mobile/app/a/e$i;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lio/reactivex/c/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/c<",
        "Lkotlin/p<",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        ">;",
        "Lkotlin/p<",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        ">;",
        "Lkotlin/p<",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/a/e$i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/a/e$i;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/a/e$i;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/a/e$i;->a:Lcom/swedbank/mobile/app/a/e$i;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lkotlin/p;

    check-cast p2, Lkotlin/p;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/a/e$i;->a(Lkotlin/p;Lkotlin/p;)Lkotlin/p;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlin/p;Lkotlin/p;)Lkotlin/p;
    .locals 2
    .param p1    # Lkotlin/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/p<",
            "+",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "+",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;",
            "Lkotlin/p<",
            "+",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "+",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;)",
            "Lkotlin/p<",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<name for destructuring parameter 1>"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/architect/business/metadata/a;

    invoke-virtual {p2}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/business/metadata/a;

    invoke-virtual {p2}, Lkotlin/p;->f()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    .line 156
    new-instance v1, Lkotlin/p;

    invoke-direct {v1, p1, v0, p2}, Lkotlin/p;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
