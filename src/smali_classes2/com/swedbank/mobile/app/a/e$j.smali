.class public final synthetic Lcom/swedbank/mobile/app/a/e$j;
.super Lkotlin/e/b/i;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lkotlin/p<",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/a;",
        "+",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        ">;",
        "Lcom/swedbank/mobile/app/a/e$b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/a/e;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/p;)Lcom/swedbank/mobile/app/a/e$b;
    .locals 10
    .param p1    # Lkotlin/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/p<",
            "+",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "+",
            "Lcom/swedbank/mobile/architect/business/metadata/a;",
            "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
            ">;)",
            "Lcom/swedbank/mobile/app/a/e$b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/e$j;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/a/e;

    .line 256
    invoke-virtual {p1}, Lkotlin/p;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/architect/business/metadata/a;

    invoke-virtual {p1}, Lkotlin/p;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/architect/business/metadata/a;

    invoke-virtual {p1}, Lkotlin/p;->f()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    .line 257
    instance-of v3, v2, Lcom/swedbank/mobile/architect/business/metadata/a$b;

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/business/metadata/a;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1, v1}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/lang/String;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v4

    :goto_0
    if-eqz v1, :cond_1

    sget-object p1, Lcom/swedbank/mobile/app/a/e$b$b;->a:Lcom/swedbank/mobile/app/a/e$b$b;

    check-cast p1, Lcom/swedbank/mobile/app/a/e$b;

    goto/16 :goto_5

    .line 260
    :cond_1
    instance-of v1, v2, Lcom/swedbank/mobile/architect/business/metadata/a$a;

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/business/metadata/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/lang/String;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v1

    goto :goto_2

    :cond_2
    if-eqz v3, :cond_c

    .line 262
    invoke-virtual {v2}, Lcom/swedbank/mobile/architect/business/metadata/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Ljava/lang/String;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 263
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    move-object v1, v4

    :goto_1
    if-eqz v1, :cond_4

    goto :goto_2

    .line 264
    :cond_4
    sget-object v1, Lcom/swedbank/mobile/app/a/g;->a:Lkotlin/h/i;

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {p1, v1}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 265
    invoke-static {v1}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v2

    if-nez v2, :cond_5

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_2

    :cond_6
    move-object v1, v4

    :goto_2
    if-eqz v1, :cond_b

    .line 272
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 274
    move-object v2, v4

    check-cast v2, Landroid/os/Bundle;

    .line 277
    new-instance v2, Landroidx/c/a;

    invoke-direct {v2}, Landroidx/c/a;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 286
    invoke-static {v0}, Lcom/swedbank/mobile/app/a/e;->h(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/c/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/c/b;->j()Lcom/swedbank/mobile/business/c/c;

    move-result-object v5

    .line 287
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "(this as java.lang.String).toLowerCase()"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    invoke-static {v0}, Lcom/swedbank/mobile/app/a/e;->i(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/c/a;

    move-result-object v7

    .line 293
    invoke-interface {v7}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/swedbank/mobile/business/c/g;->a()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 291
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "(this as java.lang.String).toLowerCase()"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "app.platform.bankId"

    .line 297
    sget-object v9, Lcom/swedbank/mobile/app/a/f;->a:[I

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/c/c;->ordinal()I

    move-result v5

    aget v5, v9, v5

    packed-switch v5, :pswitch_data_0

    .line 300
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const-string v5, "0210"

    goto :goto_3

    :pswitch_1
    const-string v5, "0110"

    goto :goto_3

    :pswitch_2
    const-string v5, "0010"

    :goto_3
    invoke-interface {v2, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.platform.name"

    .line 302
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "-android-private"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.platform.deviceType"

    const-string v8, "app-native"

    .line 303
    invoke-interface {v2, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.platform.version"

    .line 304
    invoke-static {v0}, Lcom/swedbank/mobile/app/a/e;->j(Lcom/swedbank/mobile/app/a/e;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v8

    invoke-interface {v8}, Lcom/swedbank/mobile/business/e/i;->b()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.platform.country"

    .line 305
    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.platform.language"

    .line 306
    invoke-interface {v2, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.user.type"

    const-string v7, "private"

    .line 307
    invoke-interface {v2, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v5

    const-string v7, "app.weekday"

    .line 309
    invoke-virtual {v5}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v8

    const-string v9, "dayOfWeek"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "app.day"

    .line 310
    invoke-virtual {v5}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "app.month"

    .line 311
    invoke-virtual {v5}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "app.page.name"

    .line 314
    invoke-interface {v2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "app/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    sget-object v6, Lcom/swedbank/mobile/app/a/e$c;->a:Lcom/swedbank/mobile/app/a/e$c;

    check-cast v6, Lkotlin/e/a/b;

    invoke-static {p1, v6}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 328
    new-instance v4, Lcom/swedbank/mobile/app/a/e$d;

    invoke-direct {v4, v6}, Lcom/swedbank/mobile/app/a/e$d;-><init>(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V

    check-cast v4, Lkotlin/e/a/b;

    invoke-static {v6, v4}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Lkotlin/e/a/b;)Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    move-result-object v4

    if-nez v4, :cond_7

    move-object v4, v6

    :cond_7
    if-eqz v4, :cond_8

    .line 315
    invoke-static {v4, v3}, Lcom/swedbank/mobile/business/util/j;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    goto :goto_4

    :cond_8
    const-string v3, ""

    :goto_4
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "&&channel"

    .line 333
    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "app.page.channel"

    .line 334
    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "app.platform.nodetree"

    .line 336
    invoke-static {v0}, Lcom/swedbank/mobile/app/a/e;->f(Lcom/swedbank/mobile/app/a/e;)Lcom/squareup/moshi/n;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->a(Lcom/squareup/moshi/n;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    new-instance p1, Lcom/swedbank/mobile/app/a/e$b$c;

    invoke-direct {p1, v1, v2}, Lcom/swedbank/mobile/app/a/e$b$c;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    check-cast p1, Lcom/swedbank/mobile/app/a/e$b;

    goto :goto_5

    .line 291
    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 287
    :cond_a
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 270
    :cond_b
    sget-object p1, Lcom/swedbank/mobile/app/a/e$b$b;->a:Lcom/swedbank/mobile/app/a/e$b$b;

    check-cast p1, Lcom/swedbank/mobile/app/a/e$b;

    :goto_5
    return-object p1

    .line 269
    :cond_c
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/a/e;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lkotlin/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$j;->a(Lkotlin/p;)Lcom/swedbank/mobile/app/a/e$b;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "state"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "state(Lkotlin/Triple;)Lcom/swedbank/mobile/app/analytics/AnalyticsManagerImpl$TrackingEvent;"

    return-object v0
.end method
