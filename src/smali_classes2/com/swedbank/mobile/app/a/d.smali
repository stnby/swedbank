.class public abstract Lcom/swedbank/mobile/app/a/d;
.super Ljava/lang/Object;
.source "AnalyticsEventTrackers.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/a/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/core/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "analyticsManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/d;->a:Lcom/swedbank/mobile/core/a/c;

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)Lcom/swedbank/mobile/core/a/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputType;)",
            "Lcom/swedbank/mobile/core/a/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method protected b(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputType;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/core/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    new-instance p1, Lkotlin/j;

    const-string v0, "No event got created"

    invoke-direct {p1, v0}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    invoke-static {p1}, Lio/reactivex/w;->a(Ljava/lang/Throwable;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.error(NotImplemen\u2026(\"No event got created\"))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputType;)V"
        }
    .end annotation

    .line 33
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/d;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/core/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    iget-object p1, p0, Lcom/swedbank/mobile/app/a/d;->a:Lcom/swedbank/mobile/core/a/c;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/d;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object p1

    .line 37
    new-instance v0, Lcom/swedbank/mobile/app/a/d$a;

    iget-object v1, p0, Lcom/swedbank/mobile/app/a/d;->a:Lcom/swedbank/mobile/core/a/c;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/a/d$a;-><init>(Lcom/swedbank/mobile/core/a/c;)V

    check-cast v0, Lkotlin/e/a/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/w;Lkotlin/e/a/b;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    :goto_0
    return-void
.end method
