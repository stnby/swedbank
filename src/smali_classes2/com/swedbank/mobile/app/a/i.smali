.class public final Lcom/swedbank/mobile/app/a/i;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/a/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/a;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/swedbank/mobile/app/a/i;->a:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/swedbank/mobile/app/a/i;->b:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/swedbank/mobile/app/a/i;->c:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/swedbank/mobile/app/a/i;->d:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/swedbank/mobile/app/a/i;->e:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/swedbank/mobile/app/a/i;->f:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/swedbank/mobile/app/a/i;->g:Ljavax/inject/Provider;

    .line 48
    iput-object p8, p0, Lcom/swedbank/mobile/app/a/i;->h:Ljavax/inject/Provider;

    .line 49
    iput-object p9, p0, Lcom/swedbank/mobile/app/a/i;->i:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/a/i;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/e/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/firebase/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/moshi/n;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/a/i;"
        }
    .end annotation

    .line 64
    new-instance v10, Lcom/swedbank/mobile/app/a/i;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/app/a/i;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/a/e;
    .locals 11

    .line 54
    new-instance v10, Lcom/swedbank/mobile/app/a/e;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/architect/business/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/c/b;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/c/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/e/i;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/firebase/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/business/f/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/moshi/n;

    iget-object v0, p0, Lcom/swedbank/mobile/app/a/i;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/app/a/a;

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/app/a/e;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/architect/business/a;Lcom/swedbank/mobile/business/c/b;Lcom/swedbank/mobile/business/c/a;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/firebase/c;Lcom/swedbank/mobile/business/f/a;Lcom/squareup/moshi/n;Lcom/swedbank/mobile/app/a/a;)V

    return-object v10
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/a/i;->a()Lcom/swedbank/mobile/app/a/e;

    move-result-object v0

    return-object v0
.end method
