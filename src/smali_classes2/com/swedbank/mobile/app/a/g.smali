.class public final synthetic Lcom/swedbank/mobile/app/a/g;
.super Lkotlin/e/b/s;
.source "AnalyticsManagerImpl.kt"


# static fields
.field public static final a:Lkotlin/h/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/a/g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/a/g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/a/g;->a:Lkotlin/h/i;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lkotlin/e/b/s;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    check-cast p1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    .line 173
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->g()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string v0, "rootView"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string v0, "getRootView()Z"

    return-object v0
.end method
