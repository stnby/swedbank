.class public final Lcom/swedbank/mobile/app/a/e$d;
.super Lkotlin/e/b/k;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/e$d;->a:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)Z
    .locals 2
    .param p1    # Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/swedbank/mobile/app/a/e$d;->a:Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;->h()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$d;->a(Lcom/swedbank/mobile/architect/business/metadata/NodeMetadata;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
