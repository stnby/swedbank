.class public final Lcom/swedbank/mobile/app/a/e$k;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Lcom/swedbank/mobile/app/a/e$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/a/e$k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/a/e$k;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/a/e$k;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/a/e$k;->a:Lcom/swedbank/mobile/app/a/e$k;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/a/e$b;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/a/e$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    sget-object v0, Lcom/swedbank/mobile/app/a/e$b$b;->a:Lcom/swedbank/mobile/app/a/e$b$b;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 47
    check-cast p1, Lcom/swedbank/mobile/app/a/e$b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$k;->a(Lcom/swedbank/mobile/app/a/e$b;)Z

    move-result p1

    return p1
.end method
