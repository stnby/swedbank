.class public final Lcom/swedbank/mobile/app/a/e$f;
.super Ljava/lang/Object;
.source "AnalyticsManagerImpl.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/a/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/a/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/a/e$f;->a:Lcom/swedbank/mobile/app/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lcom/swedbank/mobile/app/a/e$b$a;
    .locals 4
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/app/a/e$b$c;",
            "Lcom/swedbank/mobile/app/a/e$a;",
            ">;)",
            "Lcom/swedbank/mobile/app/a/e$b$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/a/e$b$c;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/a/e$a;

    .line 135
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/a/e$b$c;->a()Ljava/util/Map;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/swedbank/mobile/app/a/e$f;->a:Lcom/swedbank/mobile/app/a/e;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a/e;->d(Lcom/swedbank/mobile/app/a/e;)Landroid/app/Application;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/app/a/e$a;->a(Landroid/app/Application;)Ljava/lang/String;

    move-result-object v1

    .line 137
    new-instance v2, Landroidx/c/a;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Landroidx/c/a;-><init>(I)V

    .line 138
    invoke-virtual {v2, v0}, Landroidx/c/a;->putAll(Ljava/util/Map;)V

    const-string v0, "app.click.event"

    const-string v3, "1"

    .line 139
    invoke-virtual {v2, v0, v3}, Landroidx/c/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "app.click.name"

    .line 140
    invoke-virtual {v2, v0, v1}, Landroidx/c/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "app.click.type"

    .line 141
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/a/e$a;->a()Lcom/swedbank/mobile/core/a/f;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/a/f;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v0, p1}, Landroidx/c/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    new-instance p1, Lcom/swedbank/mobile/app/a/e$b$a;

    check-cast v2, Ljava/util/Map;

    invoke-direct {p1, v1, v2}, Lcom/swedbank/mobile/app/a/e$b$a;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/a/e$f;->a(Lkotlin/k;)Lcom/swedbank/mobile/app/a/e$b$a;

    move-result-object p1

    return-object p1
.end method
