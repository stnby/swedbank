.class public final Lcom/swedbank/mobile/app/i/a/n;
.super Ljava/lang/Object;
.source "BleedingEdgeUserPreferenceViewState.kt"


# instance fields
.field private final a:Lkotlin/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/k<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/app/i/a/n;-><init>(Lkotlin/k;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/k;)V
    .locals 0
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;)V"
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/n;->a:Lkotlin/k;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/k;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 8
    check-cast p1, Lkotlin/k;

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/i/a/n;-><init>(Lkotlin/k;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/k<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/n;->a:Lkotlin/k;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/swedbank/mobile/app/i/a/n;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/app/i/a/n;

    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/n;->a:Lkotlin/k;

    iget-object p1, p1, Lcom/swedbank/mobile/app/i/a/n;->a:Lkotlin/k;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/n;->a:Lkotlin/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BleedingEdgeUserPreferenceViewState(userPropertiesData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/i/a/n;->a:Lkotlin/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
