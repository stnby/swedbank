.class public final Lcom/swedbank/mobile/app/i/a/o;
.super Landroidx/recyclerview/widget/RecyclerView$a;
.source "UserPropertiesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/swedbank/mobile/app/i/a/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroidx/recyclerview/widget/f$b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 16
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    .line 17
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<UserPropertyItem>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->a:Lcom/b/c/c;

    .line 18
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/i/a/o;)Lcom/b/c/c;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/i/a/o;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/i/a/p;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "parent\n        .context"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const-string v0, "LayoutInflater.from(this)"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d005b

    const/4 v1, 0x0

    .line 67
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 41
    new-instance p2, Lcom/swedbank/mobile/app/i/a/p;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/app/i/a/p;-><init>(Landroid/view/View;)V

    return-object p2

    .line 67
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/i/a/p;I)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/i/a/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/app/i/a/q;

    .line 46
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/app/i/a/p;->a(Lcom/swedbank/mobile/app/i/a/q;)V

    .line 48
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/i/a/p;->a()Landroid/widget/ImageButton;

    move-result-object p1

    .line 49
    check-cast p1, Landroid/view/View;

    .line 69
    new-instance v0, Lcom/swedbank/mobile/app/i/a/o$a;

    invoke-direct {v0, p0, p2}, Lcom/swedbank/mobile/app/i/a/o$a;-><init>(Lcom/swedbank/mobile/app/i/a/o;Lcom/swedbank/mobile/app/i/a/q;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Lkotlin/k;)V
    .locals 2
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/f$b;

    .line 26
    iget-object v1, p0, Lcom/swedbank/mobile/app/i/a/o;->b:Ljava/util/List;

    .line 27
    iput-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->b:Ljava/util/List;

    .line 28
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->c:Landroidx/recyclerview/widget/f$b;

    if-eq v0, p1, :cond_2

    .line 31
    move-object v0, p0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/f$b;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_1

    .line 29
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/i/a/o;->notifyDataSetChanged()V

    .line 33
    :cond_2
    :goto_1
    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/o;->c:Landroidx/recyclerview/widget/f$b;

    return-void
.end method

.method public getItemCount()I
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$x;I)V
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/app/i/a/p;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/i/a/o;->a(Lcom/swedbank/mobile/app/i/a/p;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$x;
    .locals 0

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/i/a/o;->a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/i/a/p;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$x;

    return-object p1
.end method
