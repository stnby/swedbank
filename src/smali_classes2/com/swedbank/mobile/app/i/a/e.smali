.class public final Lcom/swedbank/mobile/app/i/a/e;
.super Lcom/swedbank/mobile/architect/a/d;
.source "BleedingEdgeUserPreferencePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/i/a/k;",
        "Lcom/swedbank/mobile/app/i/a/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/firebase/bleedingedge/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/firebase/bleedingedge/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/firebase/bleedingedge/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/e;->a:Lcom/swedbank/mobile/business/firebase/bleedingedge/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/i/a/e;)Lcom/swedbank/mobile/business/firebase/bleedingedge/a;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/app/i/a/e;->a:Lcom/swedbank/mobile/business/firebase/bleedingedge/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 15
    sget-object v0, Lcom/swedbank/mobile/app/i/a/e$a;->a:Lcom/swedbank/mobile/app/i/a/e$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/i/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/swedbank/mobile/app/i/a/e$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/i/a/e$b;-><init>(Lcom/swedbank/mobile/app/i/a/e;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 20
    sget-object v1, Lcom/swedbank/mobile/app/i/a/e$c;->a:Lcom/swedbank/mobile/app/i/a/e$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/i/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 21
    new-instance v2, Lcom/swedbank/mobile/app/i/a/e$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/i/a/e$d;-><init>(Lcom/swedbank/mobile/app/i/a/e;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 22
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 25
    sget-object v2, Lcom/swedbank/mobile/app/i/a/e$e;->a:Lcom/swedbank/mobile/app/i/a/e$e;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/i/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 26
    new-instance v3, Lcom/swedbank/mobile/app/i/a/e$f;

    iget-object v4, p0, Lcom/swedbank/mobile/app/i/a/e;->a:Lcom/swedbank/mobile/business/firebase/bleedingedge/a;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/i/a/e$f;-><init>(Lcom/swedbank/mobile/business/firebase/bleedingedge/a;)V

    check-cast v3, Lkotlin/e/a/b;

    new-instance v4, Lcom/swedbank/mobile/app/i/a/g;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/i/a/g;-><init>(Lkotlin/e/a/b;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 30
    sget-object v3, Lcom/swedbank/mobile/app/i/a/e$i;->a:Lcom/swedbank/mobile/app/i/a/e$i;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/i/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 31
    new-instance v4, Lcom/swedbank/mobile/app/i/a/e$j;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/i/a/e$j;-><init>(Lcom/swedbank/mobile/app/i/a/e;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 32
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 33
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 35
    iget-object v4, p0, Lcom/swedbank/mobile/app/i/a/e;->a:Lcom/swedbank/mobile/business/firebase/bleedingedge/a;

    .line 36
    invoke-interface {v4}, Lcom/swedbank/mobile/business/firebase/bleedingedge/a;->a()Lio/reactivex/o;

    move-result-object v4

    .line 37
    sget-object v5, Lcom/swedbank/mobile/app/i/a/e$g;->a:Lcom/swedbank/mobile/app/i/a/e$g;

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v5, "interactor\n        .obse\u2026ty::toUserPropertyItem) }"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {}, Lcom/swedbank/mobile/app/i/a/f;->a()Lkotlin/e/a/m;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v4, v7, v5, v6, v7}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v4

    .line 39
    sget-object v5, Lcom/swedbank/mobile/app/i/a/e$h;->a:Lcom/swedbank/mobile/app/i/a/e$h;

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const/4 v5, 0x5

    .line 43
    new-array v5, v5, [Lio/reactivex/s;

    .line 44
    check-cast v0, Lio/reactivex/s;

    const/4 v7, 0x0

    aput-object v0, v5, v7

    .line 45
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v5, v6

    .line 46
    check-cast v2, Lio/reactivex/s;

    const/4 v0, 0x2

    aput-object v2, v5, v0

    .line 47
    check-cast v3, Lio/reactivex/s;

    const/4 v0, 0x3

    aput-object v3, v5, v0

    .line 48
    check-cast v4, Lio/reactivex/s;

    const/4 v0, 0x4

    aput-object v4, v5, v0

    .line 43
    invoke-static {v5}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026    userPropertiesStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
