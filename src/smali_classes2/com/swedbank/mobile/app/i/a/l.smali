.class public final Lcom/swedbank/mobile/app/i/a/l;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BleedingEdgeUserPreferenceViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/i/a/k;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lcom/swedbank/mobile/app/i/a/o;

.field private final f:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/i/a/l;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/i/a/l;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userPropertyInput"

    const-string v4, "getUserPropertyInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/i/a/l;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "userPropertiesList"

    const-string v4, "getUserPropertiesList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/i/a/l;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/l;->f:Lio/reactivex/o;

    const p1, 0x7f0a02dd

    .line 24
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/l;->b:Lkotlin/f/c;

    const p1, 0x7f0a006e

    .line 25
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/l;->c:Lkotlin/f/c;

    const p1, 0x7f0a0070

    .line 26
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/l;->d:Lkotlin/f/c;

    .line 28
    new-instance p1, Lcom/swedbank/mobile/app/i/a/o;

    invoke-direct {p1}, Lcom/swedbank/mobile/app/i/a/o;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/i/a/l;->e:Lcom/swedbank/mobile/app/i/a/o;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/i/a/l;)Landroid/widget/EditText;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/app/i/a/l;->g()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/l;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/i/a/l;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final g()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/l;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/i/a/l;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final h()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/l;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/i/a/l;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/l;->f:Lio/reactivex/o;

    check-cast v0, Lio/reactivex/s;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/i/a/l;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n      .merge(\u2026olbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/i/a/n;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/i/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/i/a/n;->a()Lkotlin/k;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/l;->e:Lcom/swedbank/mobile/app/i/a/o;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/i/a/o;->a(Lkotlin/k;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/app/i/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/i/a/l;->a(Lcom/swedbank/mobile/app/i/a/n;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/app/i/a/l;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    const v1, 0x7f0a0035

    .line 47
    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;I)Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    invoke-direct {p0}, Lcom/swedbank/mobile/app/i/a/l;->g()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 53
    invoke-static {v0, v1, v2, v1}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/swedbank/mobile/app/i/a/l$a;->a:Lcom/swedbank/mobile/app/i/a/l$a;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/swedbank/mobile/app/i/a/l$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/i/a/l$b;-><init>(Lcom/swedbank/mobile/app/i/a/l;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "userPropertyInput\n      \u2026ull\n        input\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/app/i/a/l;->e:Lcom/swedbank/mobile/app/i/a/o;

    .line 50
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/i/a/o;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()V
    .locals 4

    .line 31
    invoke-direct {p0}, Lcom/swedbank/mobile/app/i/a/l;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->inflateMenu(I)V

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/i/a/l;->h()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/i/a/l;->n()Landroid/content/Context;

    move-result-object v1

    .line 34
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v2, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 35
    move-object v3, v2

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 36
    new-instance v3, Landroidx/recyclerview/widget/g;

    invoke-virtual {v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->g()I

    move-result v2

    invoke-direct {v3, v1, v2}, Landroidx/recyclerview/widget/g;-><init>(Landroid/content/Context;I)V

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 38
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$f;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Landroidx/recyclerview/widget/e;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/e;->a(Z)V

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/i/a/l;->e:Lcom/swedbank/mobile/app/i/a/o;

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void

    .line 38
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
