.class final Lcom/swedbank/mobile/app/i/a/e$h;
.super Ljava/lang/Object;
.source "BleedingEdgeUserPreferencePresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/i/a/e;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/i/a/e$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/i/a/e$h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/i/a/e$h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/i/a/e$h;->a:Lcom/swedbank/mobile/app/i/a/e$h;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/i/a/n;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/core/ui/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/j<",
            "Lcom/swedbank/mobile/app/i/a/q;",
            ">;)",
            "Lcom/swedbank/mobile/app/i/a/n;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/app/i/a/n;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    invoke-static {v1, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/i/a/n;-><init>(Lkotlin/k;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/core/ui/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/i/a/e$h;->a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/i/a/n;

    move-result-object p1

    return-object p1
.end method
