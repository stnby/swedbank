.class final Lcom/swedbank/mobile/app/d/c$a;
.super Ljava/lang/Object;
.source "ChallengePresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/d/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/d/c$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/d/c$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/d/c$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/d/c$a;->a:Lcom/swedbank/mobile/app/d/c$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/challenge/a;)Lcom/swedbank/mobile/app/d/j;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/business/challenge/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcom/swedbank/mobile/app/d/j;

    .line 17
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/challenge/a;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "----"

    .line 18
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/challenge/a;->b()Ljava/util/List;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/Iterable;

    const-string p1, "\n"

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 16
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/d/j;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/swedbank/mobile/business/challenge/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/d/c$a;->a(Lcom/swedbank/mobile/business/challenge/a;)Lcom/swedbank/mobile/app/d/j;

    move-result-object p1

    return-object p1
.end method
