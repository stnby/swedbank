.class public final Lcom/swedbank/mobile/app/d/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "ChallengePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/d/g;",
        "Lcom/swedbank/mobile/app/d/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/challenge/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/challenge/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/challenge/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/d/c;->a:Lcom/swedbank/mobile/business/challenge/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/d/c;)Lcom/swedbank/mobile/business/challenge/b;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/swedbank/mobile/app/d/c;->a:Lcom/swedbank/mobile/business/challenge/b;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/app/d/c;->a:Lcom/swedbank/mobile/business/challenge/b;

    .line 14
    invoke-interface {v0}, Lcom/swedbank/mobile/business/challenge/b;->a()Lio/reactivex/w;

    move-result-object v0

    .line 15
    sget-object v1, Lcom/swedbank/mobile/app/d/c$a;->a:Lcom/swedbank/mobile/app/d/c$a;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    .line 22
    sget-object v1, Lcom/swedbank/mobile/app/d/c$b;->a:Lcom/swedbank/mobile/app/d/c$b;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 23
    new-instance v2, Lcom/swedbank/mobile/app/d/c$c;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/d/c$c;-><init>(Lcom/swedbank/mobile/app/d/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 28
    check-cast v0, Lio/reactivex/s;

    .line 29
    check-cast v1, Lio/reactivex/s;

    .line 27
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026      declineClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
