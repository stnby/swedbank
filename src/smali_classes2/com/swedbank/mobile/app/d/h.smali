.class public final Lcom/swedbank/mobile/app/d/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ChallengeViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/d/g;
.implements Lcom/swedbank/mobile/core/ui/i;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/app/Dialog;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private c:Lio/reactivex/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 20
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/d/h;->a:Lcom/b/c/c;

    .line 22
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/d/h;->c:Lio/reactivex/b/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/d/h;)Landroid/content/Context;
    .locals 0

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/d/h;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/d/h;Landroid/widget/TextView;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/app/d/h;->d:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/d/h;)Landroid/widget/TextView;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/d/h;->d:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/d/h;Landroid/widget/TextView;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/app/d/h;->e:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/d/h;)Landroid/widget/TextView;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/d/h;->e:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/d/h;)Lcom/b/c/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/d/h;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;
    .locals 1
    .param p1    # Landroidx/appcompat/app/b$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/i$a;->a(Lcom/swedbank/mobile/core/ui/i;Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;

    move-result-object p1

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/d/h;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 21
    iput-object p1, p0, Lcom/swedbank/mobile/app/d/h;->b:Landroid/app/Dialog;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/d/j;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/d/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/d/h;->b()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_3

    .line 32
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/d/j;->a()Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/d/j;->b()Ljava/lang/String;

    move-result-object p1

    .line 66
    invoke-static {p0}, Lcom/swedbank/mobile/app/d/h;->a(Lcom/swedbank/mobile/app/d/h;)Landroid/content/Context;

    move-result-object v1

    .line 67
    sget v2, Lcom/swedbank/mobile/core/a$e;->view_challenge:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 69
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const-string v6, "LayoutInflater.from(this)"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v5, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Landroid/view/ViewGroup;

    .line 70
    sget v3, Lcom/swedbank/mobile/core/a$d;->challenge_status:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {p0, v3}, Lcom/swedbank/mobile/app/d/h;->a(Lcom/swedbank/mobile/app/d/h;Landroid/widget/TextView;)V

    .line 71
    sget v3, Lcom/swedbank/mobile/core/a$d;->challenge_info:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {p0, v3}, Lcom/swedbank/mobile/app/d/h;->b(Lcom/swedbank/mobile/app/d/h;Landroid/widget/TextView;)V

    .line 75
    invoke-static {p0}, Lcom/swedbank/mobile/app/d/h;->c(Lcom/swedbank/mobile/app/d/h;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/d/h;->b(Lcom/swedbank/mobile/app/d/h;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    :cond_1
    new-instance p1, Landroidx/appcompat/app/b$a;

    invoke-direct {p1, v1}, Landroidx/appcompat/app/b$a;-><init>(Landroid/content/Context;)V

    .line 87
    check-cast v2, Landroid/view/View;

    invoke-virtual {p1, v2}, Landroidx/appcompat/app/b$a;->b(Landroid/view/View;)Landroidx/appcompat/app/b$a;

    move-result-object p1

    .line 86
    new-instance v0, Lcom/swedbank/mobile/app/d/h$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/d/h$a;-><init>(Lcom/swedbank/mobile/app/d/h;)V

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/b$a;->a(Landroid/content/DialogInterface$OnCancelListener;)Landroidx/appcompat/app/b$a;

    move-result-object p1

    const/4 v0, 0x1

    .line 85
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/b$a;->a(Z)Landroidx/appcompat/app/b$a;

    move-result-object p1

    const-string v1, "AlertDialog.Builder(cont\u2026     .setCancelable(true)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/d/h;->a(Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;

    move-result-object p1

    .line 92
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/b;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 68
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 36
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/d/j;->a()Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/d/j;->b()Ljava/lang/String;

    move-result-object p1

    .line 95
    invoke-static {p0}, Lcom/swedbank/mobile/app/d/h;->c(Lcom/swedbank/mobile/app/d/h;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_4
    invoke-static {p0}, Lcom/swedbank/mobile/app/d/h;->b(Lcom/swedbank/mobile/app/d/h;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/app/d/h;->c:Lio/reactivex/b/c;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/d/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/d/h;->a(Lcom/swedbank/mobile/app/d/j;)V

    return-void
.end method

.method public b()Landroid/app/Dialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/d/h;->b:Landroid/app/Dialog;

    return-object v0
.end method

.method public f()Lio/reactivex/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/app/d/h;->c:Lio/reactivex/b/c;

    return-object v0
.end method
