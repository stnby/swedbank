.class public final Lcom/swedbank/mobile/app/e/c;
.super Ljava/lang/Object;
.source "ClipboardImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/e/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/ClipboardManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/ClipboardManager;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/app/e/c;->a:Ljavax/inject/Provider;

    .line 17
    iput-object p2, p0, Lcom/swedbank/mobile/app/e/c;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/e/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/ClipboardManager;",
            ">;)",
            "Lcom/swedbank/mobile/app/e/c;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/swedbank/mobile/app/e/c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/e/c;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/e/b;
    .locals 3

    .line 22
    new-instance v0, Lcom/swedbank/mobile/app/e/b;

    iget-object v1, p0, Lcom/swedbank/mobile/app/e/c;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/swedbank/mobile/app/e/c;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ClipboardManager;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/e/b;-><init>(Landroid/app/Application;Landroid/content/ClipboardManager;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/e/c;->a()Lcom/swedbank/mobile/app/e/b;

    move-result-object v0

    return-object v0
.end method
