.class public final Lcom/swedbank/mobile/app/MainActivity;
.super Landroidx/appcompat/app/c;
.source "MainActivity.kt"


# instance fields
.field public k:Lcom/swedbank/mobile/architect/a/b/g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public l:Lcom/swedbank/mobile/app/g/h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public m:Lcom/swedbank/mobile/app/g/c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public n:Lcom/swedbank/mobile/app/g/q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public o:Lcom/swedbank/mobile/app/g/k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public p:Lcom/swedbank/mobile/business/c/a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final q:Lio/reactivex/b/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Landroidx/appcompat/app/c;-><init>()V

    .line 39
    new-instance v0, Lio/reactivex/b/b;

    invoke-direct {v0}, Lio/reactivex/b/b;-><init>()V

    iput-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->q:Lio/reactivex/b/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/MainActivity;->q:Lio/reactivex/b/b;

    return-object p0
.end method


# virtual methods
.method public final k()Lcom/swedbank/mobile/business/c/a;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->p:Lcom/swedbank/mobile/business/c/a;

    if-nez v0, :cond_0

    const-string v1, "appPreferenceRepository"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final l()V
    .locals 9

    .line 56
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->l:Lcom/swedbank/mobile/app/g/h;

    if-nez v0, :cond_0

    const-string v1, "backManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 57
    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/g/h;->d()Lio/reactivex/o;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 58
    new-instance v0, Lcom/swedbank/mobile/app/MainActivity$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/MainActivity$a;-><init>(Lcom/swedbank/mobile/app/MainActivity;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 157
    invoke-static {p0}, Lcom/swedbank/mobile/app/MainActivity;->a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->n:Lcom/swedbank/mobile/app/g/q;

    if-nez v0, :cond_1

    const-string v1, "permissionManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 62
    :cond_1
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/g/q;->a()Lio/reactivex/o;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 63
    new-instance v0, Lcom/swedbank/mobile/app/MainActivity$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/MainActivity$b;-><init>(Lcom/swedbank/mobile/app/MainActivity;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 160
    invoke-static {p0}, Lcom/swedbank/mobile/app/MainActivity;->a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->m:Lcom/swedbank/mobile/app/g/c;

    if-nez v0, :cond_2

    const-string v1, "activityManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 71
    :cond_2
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/g/c;->a()Lcom/b/c/c;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/o;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 72
    new-instance v0, Lcom/swedbank/mobile/app/MainActivity$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/MainActivity$c;-><init>(Lcom/swedbank/mobile/app/MainActivity;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 163
    invoke-static {p0}, Lcom/swedbank/mobile/app/MainActivity;->a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->o:Lcom/swedbank/mobile/app/g/k;

    if-nez v0, :cond_3

    const-string v1, "externalActivityManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 80
    :cond_3
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/g/k;->a()Lio/reactivex/o;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 81
    new-instance v0, Lcom/swedbank/mobile/app/MainActivity$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/MainActivity$d;-><init>(Lcom/swedbank/mobile/app/MainActivity;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 166
    invoke-static {p0}, Lcom/swedbank/mobile/app/MainActivity;->a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    .line 89
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->o:Lcom/swedbank/mobile/app/g/k;

    if-nez v0, :cond_4

    const-string v1, "externalActivityManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 90
    :cond_4
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/g/k;->b()Lio/reactivex/o;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 91
    new-instance v0, Lcom/swedbank/mobile/app/MainActivity$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/MainActivity$e;-><init>(Lcom/swedbank/mobile/app/MainActivity;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 169
    invoke-static {p0}, Lcom/swedbank/mobile/app/MainActivity;->a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->p:Lcom/swedbank/mobile/business/c/a;

    if-nez v0, :cond_5

    const-string v1, "appPreferenceRepository"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 99
    :cond_5
    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->c()Lio/reactivex/o;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 101
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "appPreferenceRepository\n\u2026hanged()\n        .skip(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 102
    new-instance v0, Lcom/swedbank/mobile/app/MainActivity$f;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/MainActivity$f;-><init>(Lcom/swedbank/mobile/app/MainActivity;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 172
    invoke-static {p0}, Lcom/swedbank/mobile/app/MainActivity;->a(Lcom/swedbank/mobile/app/MainActivity;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p3    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 134
    invoke-super {p0, p1, p2, p3}, Landroidx/appcompat/app/c;->onActivityResult(IILandroid/content/Intent;)V

    .line 135
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->o:Lcom/swedbank/mobile/app/g/k;

    if-nez v0, :cond_0

    const-string v1, "externalActivityManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/swedbank/mobile/app/g/k;->a(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->l:Lcom/swedbank/mobile/app/g/h;

    if-nez v0, :cond_0

    const-string v1, "backManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/g/h;->b()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 42
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/swedbank/mobile/app/SwedbankApplication;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/SwedbankApplication;->a()Lcom/swedbank/mobile/a/b/n;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/swedbank/mobile/a/b/n;->a(Lcom/swedbank/mobile/app/MainActivity;)V

    .line 150
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/MainActivity;->k()Lcom/swedbank/mobile/business/c/a;

    move-result-object v0

    .line 151
    invoke-interface {v0}, Lcom/swedbank/mobile/business/c/a;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object v1

    .line 152
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/c/a;->a(Lcom/swedbank/mobile/business/c/g;)V

    .line 153
    move-object v2, p0

    check-cast v2, Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/swedbank/mobile/business/c/a;->a(Landroid/content/Context;Lcom/swedbank/mobile/business/c/g;)V

    const v0, 0x7f120196

    .line 44
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/MainActivity;->setTheme(I)V

    .line 45
    invoke-super {p0, p1}, Landroidx/appcompat/app/c;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d001c

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/MainActivity;->k:Lcom/swedbank/mobile/architect/a/b/g;

    if-nez v1, :cond_0

    const-string v2, "viewManager"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-static {p0, v0, v1, p1}, Lcom/swedbank/mobile/architect/a/b/h;->a(Landroid/app/Activity;ILcom/swedbank/mobile/architect/a/b/g;Landroid/os/Bundle;)Landroid/view/ViewGroup;

    .line 48
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/MainActivity;->l()V

    .line 49
    iget-object p1, p0, Lcom/swedbank/mobile/app/MainActivity;->m:Lcom/swedbank/mobile/app/g/c;

    if-nez p1, :cond_1

    const-string v0, "activityManager"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/g/c;->a(Landroid/content/Intent;)V

    return-void

    .line 42
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.app.SwedbankApplication"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 122
    invoke-super {p0}, Landroidx/appcompat/app/c;->onDestroy()V

    .line 123
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->q:Lio/reactivex/b/b;

    invoke-virtual {v0}, Lio/reactivex/b/b;->a()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 117
    invoke-super {p0, p1}, Landroidx/appcompat/app/c;->onNewIntent(Landroid/content/Intent;)V

    .line 118
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->m:Lcom/swedbank/mobile/app/g/c;

    if-nez v0, :cond_0

    const-string v1, "activityManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/g/c;->a(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 112
    invoke-super {p0}, Landroidx/appcompat/app/c;->onPause()V

    .line 113
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2
    .param p2    # [Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "permissions"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "grantResults"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-super {p0, p1, p2, p3}, Landroidx/appcompat/app/c;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 130
    iget-object v0, p0, Lcom/swedbank/mobile/app/MainActivity;->n:Lcom/swedbank/mobile/app/g/q;

    if-nez v0, :cond_0

    const-string v1, "permissionManager"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    move-object v1, p0

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/swedbank/mobile/app/g/q;->a(Landroid/app/Activity;I[Ljava/lang/String;[I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 107
    invoke-super {p0}, Landroidx/appcompat/app/c;->onResume()V

    .line 108
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method
