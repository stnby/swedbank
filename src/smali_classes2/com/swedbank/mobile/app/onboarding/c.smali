.class public final Lcom/swedbank/mobile/app/onboarding/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "OnboardingPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/onboarding/j;",
        "Lcom/swedbank/mobile/app/onboarding/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/onboarding/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/c;->a:Lcom/swedbank/mobile/business/onboarding/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/onboarding/c;)Lcom/swedbank/mobile/business/onboarding/a;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/c;->a:Lcom/swedbank/mobile/business/onboarding/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 13
    sget-object v0, Lcom/swedbank/mobile/app/onboarding/c$a;->a:Lcom/swedbank/mobile/app/onboarding/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/onboarding/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 14
    new-instance v1, Lcom/swedbank/mobile/app/onboarding/c$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/onboarding/c$b;-><init>(Lcom/swedbank/mobile/app/onboarding/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 18
    sget-object v1, Lcom/swedbank/mobile/app/onboarding/c$c;->a:Lcom/swedbank/mobile/app/onboarding/c$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/onboarding/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 19
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 20
    new-instance v2, Lcom/swedbank/mobile/app/onboarding/c$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/onboarding/c$d;-><init>(Lcom/swedbank/mobile/app/onboarding/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 21
    sget-object v2, Lcom/swedbank/mobile/app/onboarding/c$e;->a:Lcom/swedbank/mobile/app/onboarding/c$e;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    check-cast v0, Lio/reactivex/s;

    .line 25
    check-cast v1, Lio/reactivex/s;

    .line 23
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026am,\n        finishStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
