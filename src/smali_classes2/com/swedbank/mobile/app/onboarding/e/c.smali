.class public final Lcom/swedbank/mobile/app/onboarding/e/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OnboardingWatcherRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/watcher/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/onboarding/a;

.field private final f:Lcom/swedbank/mobile/app/onboarding/b/a;

.field private final g:Lcom/swedbank/mobile/business/onboarding/d;

.field private final h:Lcom/swedbank/mobile/business/onboarding/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/onboarding/a;Lcom/swedbank/mobile/app/onboarding/b/a;Lcom/swedbank/mobile/business/onboarding/d;Lcom/swedbank/mobile/business/onboarding/g;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/onboarding/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/onboarding/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/onboarding/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_watcher"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_watcher"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_watcher"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/onboarding/a;",
            "Lcom/swedbank/mobile/app/onboarding/b/a;",
            "Lcom/swedbank/mobile/business/onboarding/d;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingIntroBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingTourListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    .line 28
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->e:Lcom/swedbank/mobile/app/onboarding/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->f:Lcom/swedbank/mobile/app/onboarding/b/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->g:Lcom/swedbank/mobile/business/onboarding/d;

    iput-object p4, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->h:Lcom/swedbank/mobile/business/onboarding/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/onboarding/e/c;)Lcom/swedbank/mobile/app/onboarding/b/a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->f:Lcom/swedbank/mobile/app/onboarding/b/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/onboarding/e/c;)Lcom/swedbank/mobile/business/onboarding/g;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->h:Lcom/swedbank/mobile/business/onboarding/g;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/onboarding/e/c;)Lcom/swedbank/mobile/app/onboarding/a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->e:Lcom/swedbank/mobile/app/onboarding/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/onboarding/e/c;)Lcom/swedbank/mobile/business/onboarding/d;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/e/c;->g:Lcom/swedbank/mobile/business/onboarding/d;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 52
    sget-object v0, Lcom/swedbank/mobile/app/onboarding/e/c$a;->a:Lcom/swedbank/mobile/app/onboarding/e/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onboardingPluginKeys"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/swedbank/mobile/app/onboarding/e/c$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/onboarding/e/c$c;-><init>(Lcom/swedbank/mobile/app/onboarding/e/c;Ljava/util/List;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/onboarding/e/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onboardingPluginKeys"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/swedbank/mobile/app/onboarding/e/c$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/onboarding/e/c$b;-><init>(Lcom/swedbank/mobile/app/onboarding/e/c;Ljava/util/List;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/onboarding/e/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method
