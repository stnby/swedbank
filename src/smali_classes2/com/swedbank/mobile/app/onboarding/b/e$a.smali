.class final Lcom/swedbank/mobile/app/onboarding/b/e$a;
.super Lkotlin/e/b/k;
.source "OnboardingIntroRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/onboarding/b/e;->a(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/onboarding/b/e;

.field final synthetic b:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/onboarding/b/e;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->a:Lcom/swedbank/mobile/app/onboarding/b/e;

    iput-object p2, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->b:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 7
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    check-cast p1, Ljava/lang/Iterable;

    .line 49
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 48
    instance-of v0, v0, Lcom/swedbank/mobile/business/onboarding/tour/d;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_5

    .line 38
    iget-object p1, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->a:Lcom/swedbank/mobile/app/onboarding/b/e;

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->a:Lcom/swedbank/mobile/app/onboarding/b/e;

    invoke-static {v0}, Lcom/swedbank/mobile/app/onboarding/b/e;->a(Lcom/swedbank/mobile/app/onboarding/b/e;)Lcom/swedbank/mobile/app/onboarding/d/a;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->a:Lcom/swedbank/mobile/app/onboarding/b/e;

    iget-object v2, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->b:Ljava/util/List;

    .line 53
    invoke-static {v1}, Lcom/swedbank/mobile/app/onboarding/b/e;->c(Lcom/swedbank/mobile/app/onboarding/b/e;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-static {v1}, Lcom/swedbank/mobile/app/onboarding/b/e;->d(Lcom/swedbank/mobile/app/onboarding/b/e;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 55
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 56
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/swedbank/mobile/app/onboarding/i;

    .line 53
    invoke-virtual {v6}, Lcom/swedbank/mobile/app/onboarding/i;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 57
    :cond_4
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 53
    invoke-static {v3, v4}, Lkotlin/a/h;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 58
    new-instance v2, Lcom/swedbank/mobile/app/onboarding/b/e$b;

    invoke-direct {v2}, Lcom/swedbank/mobile/app/onboarding/b/e$b;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v1, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/onboarding/d/a;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/onboarding/d/a;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/b/e$a;->a:Lcom/swedbank/mobile/app/onboarding/b/e;

    invoke-static {v1}, Lcom/swedbank/mobile/app/onboarding/b/e;->b(Lcom/swedbank/mobile/app/onboarding/b/e;)Lcom/swedbank/mobile/business/onboarding/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/onboarding/d/a;->a(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/app/onboarding/d/a;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/onboarding/d/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 59
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_5
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/onboarding/b/e$a;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
