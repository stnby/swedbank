.class public final Lcom/swedbank/mobile/app/onboarding/b/e;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OnboardingIntroRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/intro/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/onboarding/d/a;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/business/onboarding/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/onboarding/d/a;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/onboarding/g;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/onboarding/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_intro"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_tour_extra_items"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_intro_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_intro"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_onboarding_intro"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/onboarding/d/a;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;",
            "Lcom/swedbank/mobile/business/onboarding/g;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingTourBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainTourDetails"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allExtraTourDetails"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingTourListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p5, p7, p6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->e:Lcom/swedbank/mobile/app/onboarding/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->f:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->g:Ljava/util/List;

    iput-object p4, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->h:Lcom/swedbank/mobile/business/onboarding/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/onboarding/b/e;)Lcom/swedbank/mobile/app/onboarding/d/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->e:Lcom/swedbank/mobile/app/onboarding/d/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/onboarding/b/e;)Lcom/swedbank/mobile/business/onboarding/g;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->h:Lcom/swedbank/mobile/business/onboarding/g;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/onboarding/b/e;)Ljava/util/List;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->f:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/onboarding/b/e;)Ljava/util/List;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/b/e;->g:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "additionalTourItemIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/onboarding/b/e$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/onboarding/b/e$a;-><init>(Lcom/swedbank/mobile/app/onboarding/b/e;Ljava/util/List;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/onboarding/b/e;->b(Lkotlin/e/a/b;)V

    return-void
.end method
