.class public final Lcom/swedbank/mobile/app/onboarding/b/a;
.super Ljava/lang/Object;
.source "OnboardingIntroBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/swedbank/mobile/business/onboarding/g;

.field private final c:Lcom/swedbank/mobile/a/s/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/s/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/s/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/b/a;->c:Lcom/swedbank/mobile/a/s/b/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/app/onboarding/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/onboarding/b/a;

    .line 21
    iput-object p1, v0, Lcom/swedbank/mobile/app/onboarding/b/a;->b:Lcom/swedbank/mobile/business/onboarding/g;

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/swedbank/mobile/app/onboarding/b/a;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/app/onboarding/b/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "onboardingPluginKeys"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/onboarding/b/a;

    .line 17
    iput-object p1, v0, Lcom/swedbank/mobile/app/onboarding/b/a;->a:Ljava/util/List;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/b/a;->c:Lcom/swedbank/mobile/a/s/b/a$a;

    .line 25
    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/b/a;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/s/b/a$a;->b(Ljava/util/List;)Lcom/swedbank/mobile/a/s/b/a$a;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/b/a;->b:Lcom/swedbank/mobile/business/onboarding/g;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/s/b/a$a;->b(Lcom/swedbank/mobile/business/onboarding/g;)Lcom/swedbank/mobile/a/s/b/a$a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/s/b/a$a;->a()Lcom/swedbank/mobile/a/s/b/a;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/a/s/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display onboarding intro without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 25
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display onboarding intro without plugin keys"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
