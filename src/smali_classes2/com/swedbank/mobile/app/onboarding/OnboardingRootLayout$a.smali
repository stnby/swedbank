.class public final Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;
.super Ljava/lang/Object;
.source "OnboardingRootLayout.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/os/Parcelable;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Z)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->a:Landroid/os/Parcelable;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Parcelable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->a:Landroid/os/Parcelable;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->a:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->b:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
