.class public final Lcom/swedbank/mobile/app/onboarding/g;
.super Ljava/lang/Object;
.source "OnboardingStreamImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/onboarding/f;


# instance fields
.field private final a:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 21
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefa\u2026DEFAULT_USER_DATA_SAVING)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->a:Lcom/b/c/b;

    .line 22
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->b:Lcom/b/c/c;

    .line 23
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<OnboardingPluginKey>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->c:Lcom/b/c/c;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Lio/reactivex/b;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "requiredPluginKeysToFinish"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->c:Lcom/b/c/c;

    .line 46
    new-instance v1, Landroidx/c/b;

    invoke-direct {v1}, Landroidx/c/b;-><init>()V

    sget-object v2, Lcom/swedbank/mobile/app/onboarding/g$a;->a:Lcom/swedbank/mobile/app/onboarding/g$a;

    check-cast v2, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v2}, Lcom/b/c/c;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/swedbank/mobile/app/onboarding/g$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/onboarding/g$b;-><init>(Ljava/util/List;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->c(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 51
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object p1

    const-string v0, "pluginFinishStream\n     \u2026)\n      .ignoreElements()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->a:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "pluginKey"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->c:Lcom/b/c/c;

    invoke-virtual {v0, p1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->a:Lcom/b/c/b;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b()Z
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->a:Lcom/b/c/b;

    .line 54
    invoke-virtual {v0}, Lcom/b/c/b;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public c()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->b:Lcom/b/c/c;

    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public d()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/g;->b:Lcom/b/c/c;

    .line 36
    invoke-virtual {v0}, Lcom/b/c/c;->j()Lio/reactivex/w;

    move-result-object v0

    .line 37
    invoke-static {}, Lio/reactivex/j/a;->b()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/w;->a(Lio/reactivex/v;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "finishStream\n      .firs\u2026bserveOn(Schedulers.io())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
