.class public final Lcom/swedbank/mobile/app/onboarding/a/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OnboardingErrorHandlerViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/onboarding/a/g;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "retryBtn"

    const-string v4, "getRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "titleView"

    const-string v4, "getTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/a/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "messageView"

    const-string v4, "getMessageView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/onboarding/a/h;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 14
    sget v0, Lcom/swedbank/mobile/core/a$d;->onboarding_error_handler_cancel_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->b:Lkotlin/f/c;

    .line 15
    sget v0, Lcom/swedbank/mobile/core/a$d;->onboarding_error_handler_retry_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->c:Lkotlin/f/c;

    .line 16
    sget v0, Lcom/swedbank/mobile/core/a$d;->onboarding_error_handler_title:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->d:Lkotlin/f/c;

    .line 17
    sget v0, Lcom/swedbank/mobile/core/a$d;->onboarding_error_handler_message:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->e:Lkotlin/f/c;

    return-void
.end method

.method private final c()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final d()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final f()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/a/h;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/a/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->c()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 41
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/onboarding/a/j;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/onboarding/a/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/a/j;->a()Lcom/swedbank/mobile/app/onboarding/a/k;

    move-result-object p1

    .line 25
    instance-of v0, p1, Lcom/swedbank/mobile/app/onboarding/a/k$b;

    if-eqz v0, :cond_0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->f()Landroid/widget/TextView;

    move-result-object v0

    check-cast p1, Lcom/swedbank/mobile/app/onboarding/a/k$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/a/k$b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->g()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/a/k$b;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->d()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/a/k$b;->c()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 30
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/onboarding/a/k$a;

    if-eqz v0, :cond_1

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->f()Landroid/widget/TextView;

    move-result-object v1

    check-cast p1, Lcom/swedbank/mobile/app/onboarding/a/k$a;

    const-string v2, "resources"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/onboarding/a/k$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->g()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/onboarding/a/k$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->d()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/onboarding/a/k$a;->c(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/app/onboarding/a/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/onboarding/a/h;->a(Lcom/swedbank/mobile/app/onboarding/a/j;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/a/h;->d()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 42
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
