.class public final Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;
.super Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;
.source "OnboardingRootLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;
    }
.end annotation


# static fields
.field static final synthetic g:[Lkotlin/h/g;


# instance fields
.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentViews"

    const-string v4, "getContentViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingViews"

    const-string v4, "getLoadingViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->g:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    sget p1, Lcom/swedbank/mobile/core/a$d;->onboarding_content_views:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->h:Lkotlin/f/c;

    .line 20
    sget p1, Lcom/swedbank/mobile/core/a$d;->onboarding_loading_views:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->i:Lkotlin/f/c;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 16
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 17
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final getContentViews()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getLoadingViews()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 32
    instance-of v0, p1, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;

    if-nez v0, :cond_0

    .line 33
    invoke-super {p0, p1}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 36
    :cond_0
    check-cast p1, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->a()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 37
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;->b()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->setLoading(Z)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    invoke-super {p0}, Lcom/swedbank/mobile/core/ui/widget/FitSystemWindowsConstraintLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->getLoadingViews()Landroid/view/View;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 27
    :goto_0
    new-instance v2, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;

    invoke-direct {v2, v0, v1}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout$a;-><init>(Landroid/os/Parcelable;Z)V

    check-cast v2, Landroid/os/Parcelable;

    return-object v2
.end method

.method public final setLoading(Z)V
    .locals 3

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->getContentViews()Landroid/view/View;

    move-result-object v0

    xor-int/lit8 v1, p1, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 46
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/OnboardingRootLayout;->getLoadingViews()Landroid/view/View;

    move-result-object v0

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    const/4 v2, 0x4

    .line 48
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
