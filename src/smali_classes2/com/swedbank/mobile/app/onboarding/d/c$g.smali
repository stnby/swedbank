.class final Lcom/swedbank/mobile/app/onboarding/d/c$g;
.super Ljava/lang/Object;
.source "OnboardingTourPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/onboarding/d/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/onboarding/d/c$g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/onboarding/d/c$g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/onboarding/d/c$g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/onboarding/d/c$g;->a:Lcom/swedbank/mobile/app/onboarding/d/c$g;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/onboarding/tour/c;)Lcom/swedbank/mobile/app/onboarding/d/j;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/onboarding/tour/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/swedbank/mobile/app/onboarding/d/j;

    .line 19
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/onboarding/tour/c;->a()I

    move-result v1

    .line 20
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/onboarding/tour/c;->b()Z

    move-result p1

    .line 18
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/onboarding/d/j;-><init>(IZ)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/onboarding/tour/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/onboarding/d/c$g;->a(Lcom/swedbank/mobile/business/onboarding/tour/c;)Lcom/swedbank/mobile/app/onboarding/d/j;

    move-result-object p1

    return-object p1
.end method
