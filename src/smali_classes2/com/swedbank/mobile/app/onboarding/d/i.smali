.class public final Lcom/swedbank/mobile/app/onboarding/d/i;
.super Ljava/lang/Object;
.source "OnboardingTourViewImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/onboarding/d/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/i;->a:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/swedbank/mobile/app/onboarding/d/i;->b:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/onboarding/d/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/onboarding/d/i;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/swedbank/mobile/app/onboarding/d/i;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/onboarding/d/i;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/onboarding/d/h;
    .locals 3

    .line 24
    new-instance v0, Lcom/swedbank/mobile/app/onboarding/d/h;

    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/d/i;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/swedbank/mobile/app/onboarding/d/i;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/o;

    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/onboarding/d/h;-><init>(Ljava/util/List;Lio/reactivex/o;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/onboarding/d/i;->a()Lcom/swedbank/mobile/app/onboarding/d/h;

    move-result-object v0

    return-object v0
.end method
