.class public final Lcom/swedbank/mobile/app/onboarding/d/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OnboardingTourViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/onboarding/d/g;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private f:Z

.field private final g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/airbnb/lottie/d;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/core/ui/widget/k;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/d/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "animationView"

    const-string v4, "getAnimationView()Lcom/airbnb/lottie/LottieAnimationView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/d/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "viewPager"

    const-string v4, "getViewPager()Landroidx/viewpager2/widget/ViewPager2;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/d/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "nextBtn"

    const-string v4, "getNextBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/onboarding/d/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "skipBtn"

    const-string v4, "getSkipBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/onboarding/d/h;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "onboarding_tour_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/onboarding/i;",
            ">;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tourDetails"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->i:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->j:Lio/reactivex/o;

    const p1, 0x7f0a01cf

    .line 33
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->b:Lkotlin/f/c;

    const p1, 0x7f0a01d5

    .line 34
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->c:Lkotlin/f/c;

    const p1, 0x7f0a01d0

    .line 35
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->d:Lkotlin/f/c;

    const p1, 0x7f0a01d2

    .line 36
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->e:Lkotlin/f/c;

    const/4 p1, 0x1

    .line 38
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->f:Z

    .line 39
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->g:Landroid/util/SparseArray;

    .line 42
    iget-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->i:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 122
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 123
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/l;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/core/ui/widget/i;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_0
    check-cast p2, Ljava/util/List;

    const p1, 0x7f0d0042

    .line 45
    sget-object v0, Lcom/swedbank/mobile/app/onboarding/d/h$a;->a:Lcom/swedbank/mobile/app/onboarding/d/h$a;

    check-cast v0, Lkotlin/e/a/m;

    .line 43
    invoke-static {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/l;->b(ILkotlin/e/a/m;)Landroid/util/SparseArray;

    move-result-object p1

    .line 41
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/k;

    invoke-direct {v0, p2, p1}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->h:Lcom/swedbank/mobile/core/ui/widget/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/onboarding/d/h;)Landroid/content/Context;
    .locals 0

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/onboarding/d/h;)Landroid/util/SparseArray;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->g:Landroid/util/SparseArray;

    return-object p0
.end method

.method private final d()Lcom/airbnb/lottie/LottieAnimationView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/d/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    return-object v0
.end method

.method private final f()Landroidx/viewpager2/widget/ViewPager2;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/d/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/viewpager2/widget/ViewPager2;

    return-object v0
.end method

.method private final g()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/d/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final h()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/onboarding/d/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 59
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->g()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 109
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/onboarding/d/j;)V
    .locals 5
    .param p1    # Lcom/swedbank/mobile/app/onboarding/d/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->d()Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/d/j;->a()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/onboarding/i;

    .line 112
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/onboarding/i;->f()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    const/4 v2, -0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    .line 111
    :goto_0
    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    .line 115
    invoke-static {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->b(Lcom/swedbank/mobile/app/onboarding/d/h;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/onboarding/i;->e()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/airbnb/lottie/d;

    if-eqz v2, :cond_1

    .line 116
    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setComposition(Lcom/airbnb/lottie/d;)V

    goto :goto_1

    .line 117
    :cond_1
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/onboarding/i;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 118
    :goto_1
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->a()V

    .line 67
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->f()Landroidx/viewpager2/widget/ViewPager2;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/d/j;->a()I

    move-result v1

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->f:Z

    xor-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroidx/viewpager2/widget/ViewPager2;->a(IZ)V

    const/4 v0, 0x0

    .line 68
    iput-boolean v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->f:Z

    .line 69
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->g()Landroid/widget/Button;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/d/j;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7f110178

    goto :goto_2

    :cond_2
    const p1, 0x7f110179

    .line 69
    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/swedbank/mobile/app/onboarding/d/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/onboarding/d/h;->a(Lcom/swedbank/mobile/app/onboarding/d/j;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->j:Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->h()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 110
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method protected e()V
    .locals 4

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->i:Ljava/util/List;

    .line 97
    check-cast v0, Ljava/lang/Iterable;

    .line 98
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/onboarding/i;

    .line 99
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/onboarding/i;->e()I

    move-result v1

    .line 103
    invoke-static {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->a(Lcom/swedbank/mobile/app/onboarding/d/h;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/airbnb/lottie/e;->a(Landroid/content/Context;I)Lcom/airbnb/lottie/l;

    move-result-object v2

    .line 102
    new-instance v3, Lcom/swedbank/mobile/app/onboarding/d/h$b;

    invoke-direct {v3, v1, p0}, Lcom/swedbank/mobile/app/onboarding/d/h$b;-><init>(ILcom/swedbank/mobile/app/onboarding/d/h;)V

    check-cast v3, Lcom/airbnb/lottie/h;

    invoke-virtual {v2, v3}, Lcom/airbnb/lottie/l;->a(Lcom/airbnb/lottie/h;)Lcom/airbnb/lottie/l;

    goto :goto_0

    .line 53
    :cond_0
    invoke-direct {p0}, Lcom/swedbank/mobile/app/onboarding/d/h;->f()Landroidx/viewpager2/widget/ViewPager2;

    move-result-object v0

    const/4 v1, 0x0

    .line 54
    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->setUserInputEnabled(Z)V

    .line 55
    iget-object v1, p0, Lcom/swedbank/mobile/app/onboarding/d/h;->h:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method
