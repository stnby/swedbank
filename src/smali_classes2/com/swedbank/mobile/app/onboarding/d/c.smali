.class public final Lcom/swedbank/mobile/app/onboarding/d/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "OnboardingTourPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/onboarding/d/g;",
        "Lcom/swedbank/mobile/app/onboarding/d/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/onboarding/tour/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/onboarding/tour/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/tour/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/onboarding/d/c;->a:Lcom/swedbank/mobile/business/onboarding/tour/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/onboarding/d/c;)Lcom/swedbank/mobile/business/onboarding/tour/a;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/swedbank/mobile/app/onboarding/d/c;->a:Lcom/swedbank/mobile/business/onboarding/tour/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/app/onboarding/d/c;->a:Lcom/swedbank/mobile/business/onboarding/tour/a;

    .line 16
    invoke-interface {v0}, Lcom/swedbank/mobile/business/onboarding/tour/a;->a()Lio/reactivex/o;

    move-result-object v0

    .line 17
    sget-object v1, Lcom/swedbank/mobile/app/onboarding/d/c$g;->a:Lcom/swedbank/mobile/app/onboarding/d/c$g;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/swedbank/mobile/app/onboarding/d/c$a;->a:Lcom/swedbank/mobile/app/onboarding/d/c$a;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/onboarding/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 25
    new-instance v2, Lcom/swedbank/mobile/app/onboarding/d/c$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/onboarding/d/c$b;-><init>(Lcom/swedbank/mobile/app/onboarding/d/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 29
    sget-object v2, Lcom/swedbank/mobile/app/onboarding/d/c$c;->a:Lcom/swedbank/mobile/app/onboarding/d/c$c;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/onboarding/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 30
    new-instance v3, Lcom/swedbank/mobile/app/onboarding/d/c$d;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/onboarding/d/c$d;-><init>(Lcom/swedbank/mobile/app/onboarding/d/c;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 31
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 34
    sget-object v3, Lcom/swedbank/mobile/app/onboarding/d/c$e;->a:Lcom/swedbank/mobile/app/onboarding/d/c$e;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/onboarding/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 35
    new-instance v4, Lcom/swedbank/mobile/app/onboarding/d/c$f;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/onboarding/d/c$f;-><init>(Lcom/swedbank/mobile/app/onboarding/d/c;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 36
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 37
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 40
    check-cast v0, Lio/reactivex/s;

    .line 41
    check-cast v1, Lio/reactivex/s;

    .line 42
    check-cast v2, Lio/reactivex/s;

    .line 43
    check-cast v3, Lio/reactivex/s;

    .line 39
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026        prevClicksStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
