.class public final Lcom/swedbank/mobile/app/contact/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ContactViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/contact/g;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/app/contact/b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/core/ui/widget/k;

.field private final h:Lcom/swedbank/mobile/core/ui/widget/k;

.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/contact/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "tabLayout"

    const-string v4, "getTabLayout()Lcom/google/android/material/tabs/TabLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/contact/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "viewPager"

    const-string v4, "getViewPager()Lcom/swedbank/mobile/app/contact/ContactViewPager;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/contact/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "privateList"

    const-string v4, "getPrivateList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/contact/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "businessList"

    const-string v4, "getBusinessList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/contact/h;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 7
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "contact_preselected_private"
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/contact/h;->i:Z

    .line 29
    sget p1, Lcom/swedbank/mobile/core/a$d;->contact_tabs:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/h;->b:Lkotlin/f/c;

    .line 30
    sget p1, Lcom/swedbank/mobile/core/a$d;->contact_view_pager:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/h;->c:Lkotlin/f/c;

    .line 31
    sget p1, Lcom/swedbank/mobile/core/a$d;->contact_list_private:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/h;->d:Lkotlin/f/c;

    .line 32
    sget p1, Lcom/swedbank/mobile/core/a$d;->contact_list_business:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/h;->e:Lkotlin/f/c;

    .line 34
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<ContactMethod>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/h;->f:Lcom/b/c/c;

    const/4 p1, 0x2

    .line 148
    new-array v0, p1, [Lkotlin/k;

    const/4 v1, 0x1

    .line 149
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 153
    sget v3, Lcom/swedbank/mobile/core/a$e;->item_list_separator:I

    .line 154
    sget-object v4, Lcom/swedbank/mobile/app/contact/h$b;->a:Lcom/swedbank/mobile/app/contact/h$b;

    check-cast v4, Lkotlin/e/a/m;

    .line 152
    invoke-static {v3, v4}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    .line 149
    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 155
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 162
    sget v4, Lcom/swedbank/mobile/core/a$e;->item_contact_method:I

    .line 163
    new-instance v5, Lcom/swedbank/mobile/app/contact/h$a;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/contact/h$a;-><init>(Lcom/swedbank/mobile/app/contact/h;)V

    check-cast v5, Lkotlin/e/a/m;

    .line 161
    invoke-static {v4, v5}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v4

    .line 155
    invoke-static {v2, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, v0, v1

    .line 148
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object v0

    .line 147
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/k;

    const/4 v4, 0x0

    invoke-direct {v2, v4, v0, v1, v4}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    .line 155
    iput-object v2, p0, Lcom/swedbank/mobile/app/contact/h;->g:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 172
    new-array v0, p1, [Lkotlin/k;

    .line 173
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 177
    sget v5, Lcom/swedbank/mobile/core/a$e;->item_list_separator:I

    .line 178
    sget-object v6, Lcom/swedbank/mobile/app/contact/h$b;->a:Lcom/swedbank/mobile/app/contact/h$b;

    check-cast v6, Lkotlin/e/a/m;

    .line 176
    invoke-static {v5, v6}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v5

    .line 173
    invoke-static {v2, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, v0, v3

    .line 179
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 186
    sget v2, Lcom/swedbank/mobile/core/a$e;->item_contact_method:I

    .line 187
    new-instance v3, Lcom/swedbank/mobile/app/contact/h$a;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/contact/h$a;-><init>(Lcom/swedbank/mobile/app/contact/h;)V

    check-cast v3, Lkotlin/e/a/m;

    .line 185
    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v2

    .line 179
    invoke-static {p1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    aput-object p1, v0, v1

    .line 172
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object p1

    .line 171
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/k;

    invoke-direct {v0, v4, p1, v1, v4}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    .line 179
    iput-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->h:Lcom/swedbank/mobile/core/ui/widget/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/contact/h;)Lcom/b/c/c;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/app/contact/h;->f:Lcom/b/c/c;

    return-object p0
.end method

.method private final b()Lcom/google/android/material/tabs/TabLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/contact/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/tabs/TabLayout;

    return-object v0
.end method

.method private final c()Lcom/swedbank/mobile/app/contact/ContactViewPager;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/contact/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/contact/ContactViewPager;

    return-object v0
.end method

.method private final d()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/contact/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final f()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/contact/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/contact/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->f:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/contact/j;)V
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/contact/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->g:Lcom/swedbank/mobile/core/ui/widget/k;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/contact/j;->a()Ljava/util/List;

    move-result-object v1

    .line 104
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 106
    check-cast v1, Ljava/lang/Iterable;

    .line 107
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x2

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/app/contact/a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/contact/a;->b()Lcom/swedbank/mobile/app/contact/f;

    move-result-object v7

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/contact/a;->c()Ljava/util/List;

    move-result-object v4

    .line 108
    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-virtual {v7}, Lcom/swedbank/mobile/app/contact/f;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v8, v7, v5}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    check-cast v4, Ljava/lang/Iterable;

    .line 110
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/app/contact/b;

    .line 111
    new-instance v7, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v7, v5, v6}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_1
    check-cast v2, Ljava/util/List;

    const/4 v1, 0x0

    .line 50
    invoke-static {v0, v2, v1, v6, v1}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/h;->h:Lcom/swedbank/mobile/core/ui/widget/k;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/contact/j;->b()Ljava/util/List;

    move-result-object p1

    .line 117
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 119
    check-cast p1, Ljava/lang/Iterable;

    .line 120
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/app/contact/a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/contact/a;->b()Lcom/swedbank/mobile/app/contact/f;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/contact/a;->c()Ljava/util/List;

    move-result-object v3

    .line 121
    new-instance v7, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/contact/f;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v7, v4, v5}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    check-cast v3, Ljava/lang/Iterable;

    .line 123
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/app/contact/b;

    .line 124
    new-instance v7, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v7, v4, v6}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    :cond_3
    check-cast v2, Ljava/util/List;

    .line 51
    invoke-static {v0, v2, v1, v6, v1}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/app/contact/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/h;->a(Lcom/swedbank/mobile/app/contact/j;)V

    return-void
.end method

.method protected e()V
    .locals 12

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/app/contact/h;->c()Lcom/swedbank/mobile/app/contact/ContactViewPager;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/swedbank/mobile/app/contact/e;

    invoke-virtual {p0}, Lcom/swedbank/mobile/app/contact/h;->n()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/contact/e;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroidx/n/a/a;

    .line 41
    iget-boolean v2, p0, Lcom/swedbank/mobile/app/contact/h;->i:Z

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    .line 39
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/app/contact/ContactViewPager;->a(Landroidx/n/a/a;I)V

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/app/contact/h;->b()Lcom/google/android/material/tabs/TabLayout;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/contact/h;->c()Lcom/swedbank/mobile/app/contact/ContactViewPager;

    move-result-object v1

    check-cast v1, Landroidx/n/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->setupWithViewPager(Landroidx/n/a/b;)V

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/contact/h;->d()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/contact/h;->g:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 92
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 93
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/f;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v4, "context"

    invoke-static {v5, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/g$c;

    new-array v8, v3, [I

    const/4 v11, 0x0

    aput v3, v8, v11

    invoke-direct {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v8, v4

    check-cast v8, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v9, 0x6

    const/4 v10, 0x0

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 96
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/app/contact/h;->f()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/contact/h;->h:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 98
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 99
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/f;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v4, "context"

    invoke-static {v5, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/g$c;

    new-array v8, v3, [I

    aput v3, v8, v11

    invoke-direct {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v8, v4

    check-cast v8, Lcom/swedbank/mobile/core/ui/widget/g;

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 102
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method
