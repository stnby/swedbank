.class public final Lcom/swedbank/mobile/app/contact/h$a;
.super Lkotlin/e/b/k;
.source "ContactViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/contact/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/contact/b;",
        ">;",
        "Lcom/swedbank/mobile/app/contact/b;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/contact/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/contact/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/h$a;->a:Lcom/swedbank/mobile/app/contact/h;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/contact/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/contact/h$a;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/contact/b;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/contact/b;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/contact/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/contact/b;",
            ">;",
            "Lcom/swedbank/mobile/app/contact/b;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget v0, Lcom/swedbank/mobile/core/a$d;->contact_method_icon:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/contact/b;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    sget v0, Lcom/swedbank/mobile/core/a$d;->contact_method_title:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/contact/b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 87
    sget v0, Lcom/swedbank/mobile/core/a$d;->contact_method_subtitle:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/contact/b;->c()Lkotlin/e/a/a;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/swedbank/mobile/app/contact/h$a$a;

    invoke-direct {v1, p2, p0, p1}, Lcom/swedbank/mobile/app/contact/h$a$a;-><init>(Lcom/swedbank/mobile/app/contact/b;Lcom/swedbank/mobile/app/contact/h$a;Lcom/swedbank/mobile/core/ui/widget/j;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
