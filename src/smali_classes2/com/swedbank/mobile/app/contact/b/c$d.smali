.class final Lcom/swedbank/mobile/app/contact/b/c$d;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/contact/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/contact/b/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/contact/b/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/b/c$d;->a:Lcom/swedbank/mobile/app/contact/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/contact/j$a$a;",
            ">;"
        }
    .end annotation

    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/app/contact/b/c$d;->a:Lcom/swedbank/mobile/app/contact/b/c;

    iget-object v2, p0, Lcom/swedbank/mobile/app/contact/b/c$d;->a:Lcom/swedbank/mobile/app/contact/b/c;

    invoke-static {v2}, Lcom/swedbank/mobile/app/contact/b/c;->a(Lcom/swedbank/mobile/app/contact/b/c;)Lcom/swedbank/mobile/business/contact/notauth/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/business/contact/notauth/a;->b()Ljava/util/List;

    move-result-object v2

    .line 138
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lio/reactivex/o;->a(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v2

    .line 137
    sget-object v3, Lcom/swedbank/mobile/app/contact/b/c$e;->a:Lcom/swedbank/mobile/app/contact/b/c$e;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->g(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 136
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/c$f;

    invoke-direct {v3, v1, p1}, Lcom/swedbank/mobile/app/contact/b/c$f;-><init>(Lcom/swedbank/mobile/app/contact/b/c;Lcom/swedbank/mobile/business/util/l;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 135
    sget-object v2, Lcom/swedbank/mobile/app/contact/b/c$i;->a:Lcom/swedbank/mobile/app/contact/b/c$i;

    check-cast v2, Ljava/util/Comparator;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Ljava/util/Comparator;)Lio/reactivex/w;

    move-result-object v1

    const-string v2, "Observable\n      .fromIt\u2026.compareTo(group1.type) }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/aa;

    .line 42
    iget-object v2, p0, Lcom/swedbank/mobile/app/contact/b/c$d;->a:Lcom/swedbank/mobile/app/contact/b/c;

    iget-object v3, p0, Lcom/swedbank/mobile/app/contact/b/c$d;->a:Lcom/swedbank/mobile/app/contact/b/c;

    invoke-static {v3}, Lcom/swedbank/mobile/app/contact/b/c;->a(Lcom/swedbank/mobile/app/contact/b/c;)Lcom/swedbank/mobile/business/contact/notauth/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/swedbank/mobile/business/contact/notauth/a;->i()Ljava/util/List;

    move-result-object v3

    .line 151
    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lio/reactivex/o;->a(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v3

    .line 150
    sget-object v4, Lcom/swedbank/mobile/app/contact/b/c$e;->a:Lcom/swedbank/mobile/app/contact/b/c$e;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->g(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 149
    new-instance v4, Lcom/swedbank/mobile/app/contact/b/c$f;

    invoke-direct {v4, v2, p1}, Lcom/swedbank/mobile/app/contact/b/c$f;-><init>(Lcom/swedbank/mobile/app/contact/b/c;Lcom/swedbank/mobile/business/util/l;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 148
    sget-object v2, Lcom/swedbank/mobile/app/contact/b/c$i;->a:Lcom/swedbank/mobile/app/contact/b/c$i;

    check-cast v2, Ljava/util/Comparator;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->a(Ljava/util/Comparator;)Lio/reactivex/w;

    move-result-object p1

    const-string v2, "Observable\n      .fromIt\u2026.compareTo(group1.type) }"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/aa;

    .line 40
    invoke-virtual {v0, v1, p1}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object p1

    .line 43
    sget-object v0, Lcom/swedbank/mobile/app/contact/b/c$d$1;->a:Lcom/swedbank/mobile/app/contact/b/c$d$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/b/c$d;->a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
