.class public final Lcom/swedbank/mobile/app/contact/b/c$f$1;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/contact/b/c$f;->a(Lio/reactivex/e/b;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/contact/b/c$f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/contact/b/c$f;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/b/c$f$1;->a:Lcom/swedbank/mobile/app/contact/b/c$f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/b/a;)Lcom/swedbank/mobile/app/contact/b;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/business/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/b/c$f$1;->a:Lcom/swedbank/mobile/app/contact/b/c$f;

    iget-object v4, v0, Lcom/swedbank/mobile/app/contact/b/c$f;->a:Lcom/swedbank/mobile/app/contact/b/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/b/c$f$1;->a:Lcom/swedbank/mobile/app/contact/b/c$f;

    iget-object v6, v0, Lcom/swedbank/mobile/app/contact/b/c$f;->b:Lcom/swedbank/mobile/business/util/l;

    .line 127
    instance-of v0, p1, Lcom/swedbank/mobile/business/b/a$e;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swedbank/mobile/app/contact/b;

    .line 128
    sget v1, Lcom/swedbank/mobile/core/a$f;->contact_method_call_name:I

    .line 129
    sget v2, Lcom/swedbank/mobile/core/a$c;->ic_contact_call_phone:I

    .line 130
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/c$j;

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/contact/b/c$j;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v3, Lkotlin/e/a/a;

    .line 131
    new-instance v4, Lcom/swedbank/mobile/app/contact/b/c$m;

    invoke-direct {v4, p1}, Lcom/swedbank/mobile/app/contact/b/c$m;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v4, Lkotlin/e/a/a;

    .line 127
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/contact/b;-><init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V

    goto/16 :goto_0

    .line 132
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/b/a$f;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/app/contact/b;

    .line 133
    sget v1, Lcom/swedbank/mobile/core/a$f;->contact_method_skype_name:I

    .line 134
    sget v2, Lcom/swedbank/mobile/core/a$c;->ic_contact_call_skype:I

    .line 135
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/c$n;

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/contact/b/c$n;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v3, Lkotlin/e/a/a;

    .line 136
    new-instance v4, Lcom/swedbank/mobile/app/contact/b/c$o;

    invoke-direct {v4, p1}, Lcom/swedbank/mobile/app/contact/b/c$o;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v4, Lkotlin/e/a/a;

    .line 132
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/contact/b;-><init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V

    goto/16 :goto_0

    .line 137
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/b/a$b;

    if-eqz v0, :cond_2

    invoke-static {v4}, Lcom/swedbank/mobile/app/contact/b/c;->a(Lcom/swedbank/mobile/app/contact/b/c;)Lcom/swedbank/mobile/business/contact/notauth/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/contact/notauth/a;->j()Lkotlin/k;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/business/e/h;

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/c/c;

    .line 138
    new-instance v0, Lcom/swedbank/mobile/app/contact/b;

    .line 139
    sget v7, Lcom/swedbank/mobile/core/a$f;->contact_method_feedback_name:I

    .line 140
    sget v8, Lcom/swedbank/mobile/core/a$c;->ic_contact_send_email:I

    .line 141
    new-instance v1, Lcom/swedbank/mobile/app/contact/b/c$g;

    invoke-direct {v1, v4, p1, v6}, Lcom/swedbank/mobile/app/contact/b/c$g;-><init>(Lcom/swedbank/mobile/app/contact/b/c;Lcom/swedbank/mobile/business/b/a;Lcom/swedbank/mobile/business/util/l;)V

    move-object v9, v1

    check-cast v9, Lkotlin/e/a/a;

    .line 142
    new-instance v10, Lcom/swedbank/mobile/app/contact/b/c$h;

    move-object v1, v10

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/contact/b/c$h;-><init>(Lcom/swedbank/mobile/business/e/h;Lcom/swedbank/mobile/business/c/c;Lcom/swedbank/mobile/app/contact/b/c;Lcom/swedbank/mobile/business/b/a;Lcom/swedbank/mobile/business/util/l;)V

    check-cast v10, Lkotlin/e/a/a;

    .line 138
    invoke-direct {v0, v7, v8, v9, v10}, Lcom/swedbank/mobile/app/contact/b;-><init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V

    goto :goto_0

    .line 153
    :cond_2
    instance-of v0, p1, Lcom/swedbank/mobile/business/b/a$c;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/swedbank/mobile/app/contact/b;

    .line 154
    sget v1, Lcom/swedbank/mobile/core/a$f;->contact_method_write_name:I

    .line 155
    sget v2, Lcom/swedbank/mobile/core/a$c;->ic_contact_send_email:I

    .line 156
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/c$p;

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/contact/b/c$p;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v3, Lkotlin/e/a/a;

    .line 157
    new-instance v4, Lcom/swedbank/mobile/app/contact/b/c$q;

    invoke-direct {v4, p1}, Lcom/swedbank/mobile/app/contact/b/c$q;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v4, Lkotlin/e/a/a;

    .line 153
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/contact/b;-><init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V

    goto :goto_0

    .line 158
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/business/b/a$d;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/swedbank/mobile/app/contact/b;

    .line 159
    sget v1, Lcom/swedbank/mobile/core/a$f;->contact_method_map_name:I

    .line 160
    sget v2, Lcom/swedbank/mobile/core/a$c;->ic_contact_map:I

    .line 161
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/c$r;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/contact/b/c$r;-><init>(Lcom/swedbank/mobile/app/contact/b/c;)V

    check-cast v3, Lkotlin/e/a/a;

    .line 162
    new-instance v4, Lcom/swedbank/mobile/app/contact/b/c$s;

    invoke-direct {v4, p1}, Lcom/swedbank/mobile/app/contact/b/c$s;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v4, Lkotlin/e/a/a;

    .line 158
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/contact/b;-><init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V

    goto :goto_0

    .line 163
    :cond_4
    instance-of v0, p1, Lcom/swedbank/mobile/business/b/a$a;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/swedbank/mobile/app/contact/b;

    .line 164
    sget v1, Lcom/swedbank/mobile/core/a$f;->contact_method_appointment_name:I

    .line 165
    sget v2, Lcom/swedbank/mobile/core/a$c;->ic_contact_appointment:I

    .line 166
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/c$k;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/contact/b/c$k;-><init>(Lcom/swedbank/mobile/app/contact/b/c;)V

    check-cast v3, Lkotlin/e/a/a;

    .line 167
    new-instance v4, Lcom/swedbank/mobile/app/contact/b/c$l;

    invoke-direct {v4, p1}, Lcom/swedbank/mobile/app/contact/b/c$l;-><init>(Lcom/swedbank/mobile/business/b/a;)V

    check-cast v4, Lkotlin/e/a/a;

    .line 163
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/contact/b;-><init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V

    :goto_0
    return-object v0

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/business/b/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/b/c$f$1;->a(Lcom/swedbank/mobile/business/b/a;)Lcom/swedbank/mobile/app/contact/b;

    move-result-object p1

    return-object p1
.end method
