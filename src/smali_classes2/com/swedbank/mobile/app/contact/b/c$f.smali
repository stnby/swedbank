.class public final Lcom/swedbank/mobile/app/contact/b/c$f;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/contact/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/contact/b/c;

.field final synthetic b:Lcom/swedbank/mobile/business/util/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/contact/b/c;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/b/c$f;->a:Lcom/swedbank/mobile/app/contact/b/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/contact/b/c$f;->b:Lcom/swedbank/mobile/business/util/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/e/b;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lio/reactivex/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/e/b<",
            "Lcom/swedbank/mobile/app/contact/f;",
            "Lcom/swedbank/mobile/business/b/a;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/contact/a;",
            ">;"
        }
    .end annotation

    const-string v0, "group"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/swedbank/mobile/app/contact/b/c$f$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/contact/b/c$f$1;-><init>(Lcom/swedbank/mobile/app/contact/b/c$f;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/e/b;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lio/reactivex/o;->p()Lio/reactivex/w;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/swedbank/mobile/app/contact/b/c$f$2;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/contact/b/c$f$2;-><init>(Lio/reactivex/e/b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lio/reactivex/e/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/b/c$f;->a(Lio/reactivex/e/b;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
