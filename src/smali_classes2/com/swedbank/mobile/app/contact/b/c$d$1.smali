.class final Lcom/swedbank/mobile/app/contact/b/c$d$1;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/contact/b/c$d;->a(Lcom/swedbank/mobile/business/util/l;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/contact/b/c$d$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/contact/b/c$d$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/contact/b/c$d$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/contact/b/c$d$1;->a:Lcom/swedbank/mobile/app/contact/b/c$d$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lcom/swedbank/mobile/app/contact/j$a$a;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/contact/a;",
            ">;+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/contact/a;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/contact/j$a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 44
    new-instance v1, Lcom/swedbank/mobile/app/contact/j$a$a;

    const-string v2, "privateContacts"

    .line 45
    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "businessContacts"

    .line 46
    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/app/contact/j$a$a;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/b/c$d$1;->a(Lkotlin/k;)Lcom/swedbank/mobile/app/contact/j$a$a;

    move-result-object p1

    return-object p1
.end method
