.class public final Lcom/swedbank/mobile/app/contact/b/c$e;
.super Ljava/lang/Object;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/contact/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TK;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/contact/b/c$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/contact/b/c$e;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/contact/b/c$e;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/contact/b/c$e;->a:Lcom/swedbank/mobile/app/contact/b/c$e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/b/a;)Lcom/swedbank/mobile/app/contact/f;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    instance-of p1, p1, Lcom/swedbank/mobile/business/b/a$b;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/contact/f;->a:Lcom/swedbank/mobile/app/contact/f;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/swedbank/mobile/app/contact/f;->b:Lcom/swedbank/mobile/app/contact/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/business/b/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/b/c$e;->a(Lcom/swedbank/mobile/business/b/a;)Lcom/swedbank/mobile/app/contact/f;

    move-result-object p1

    return-object p1
.end method
