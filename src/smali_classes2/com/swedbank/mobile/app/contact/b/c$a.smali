.class final synthetic Lcom/swedbank/mobile/app/contact/b/c$a;
.super Lkotlin/e/b/i;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/contact/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/contact/j;",
        "Lcom/swedbank/mobile/app/contact/j$a;",
        "Lcom/swedbank/mobile/app/contact/j;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/contact/b/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/contact/j;Lcom/swedbank/mobile/app/contact/j$a;)Lcom/swedbank/mobile/app/contact/j;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/contact/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/contact/j$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/b/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/contact/b/c;

    .line 127
    instance-of v0, p2, Lcom/swedbank/mobile/app/contact/j$a$a;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 128
    check-cast p2, Lcom/swedbank/mobile/app/contact/j$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/contact/j$a$a;->a()Ljava/util/List;

    move-result-object v3

    .line 129
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/contact/j$a$a;->b()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    .line 127
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/contact/j;->a(Lcom/swedbank/mobile/app/contact/j;Ljava/lang/Throwable;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/swedbank/mobile/app/contact/j;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/app/contact/j;

    check-cast p2, Lcom/swedbank/mobile/app/contact/j$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/contact/b/c$a;->a(Lcom/swedbank/mobile/app/contact/j;Lcom/swedbank/mobile/app/contact/j$a;)Lcom/swedbank/mobile/app/contact/j;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/contact/b/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/contact/ContactViewState;Lcom/swedbank/mobile/app/contact/ContactViewState$PartialState;)Lcom/swedbank/mobile/app/contact/ContactViewState;"

    return-object v0
.end method
