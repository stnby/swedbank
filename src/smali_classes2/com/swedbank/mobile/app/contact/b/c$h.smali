.class public final Lcom/swedbank/mobile/app/contact/b/c$h;
.super Lkotlin/e/b/k;
.source "NotAuthenticatedContactPresenter.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/contact/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/a<",
        "Lcom/swedbank/mobile/business/contact/a$b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/e/h;

.field final synthetic b:Lcom/swedbank/mobile/business/c/c;

.field final synthetic c:Lcom/swedbank/mobile/app/contact/b/c;

.field final synthetic d:Lcom/swedbank/mobile/business/b/a;

.field final synthetic e:Lcom/swedbank/mobile/business/util/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/e/h;Lcom/swedbank/mobile/business/c/c;Lcom/swedbank/mobile/app/contact/b/c;Lcom/swedbank/mobile/business/b/a;Lcom/swedbank/mobile/business/util/l;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->a:Lcom/swedbank/mobile/business/e/h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->b:Lcom/swedbank/mobile/business/c/c;

    iput-object p3, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->c:Lcom/swedbank/mobile/app/contact/b/c;

    iput-object p4, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->d:Lcom/swedbank/mobile/business/b/a;

    iput-object p5, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->e:Lcom/swedbank/mobile/business/util/l;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/business/contact/a$b;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->d:Lcom/swedbank/mobile/business/b/a;

    check-cast v0, Lcom/swedbank/mobile/business/b/a$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/b/a$b;->a()Ljava/lang/String;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->c:Lcom/swedbank/mobile/app/contact/b/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/contact/b/c;->b(Lcom/swedbank/mobile/app/contact/b/c;)Landroid/app/Application;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/core/a$f;->contact_method_feedback_action_subject:I

    invoke-virtual {v1, v2}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 102
    iget-object v2, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->c:Lcom/swedbank/mobile/app/contact/b/c;

    invoke-static {v2}, Lcom/swedbank/mobile/app/contact/b/c;->b(Lcom/swedbank/mobile/app/contact/b/c;)Landroid/app/Application;

    move-result-object v2

    sget v3, Lcom/swedbank/mobile/core/a$f;->contact_method_feedback_action_body:I

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    .line 103
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->a:Lcom/swedbank/mobile/business/e/h;

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/e/h;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->b:Lcom/swedbank/mobile/business/c/c;

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/c/c;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 104
    iget-object v5, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->a:Lcom/swedbank/mobile/business/e/h;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/e/h;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    .line 105
    iget-object v5, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->a:Lcom/swedbank/mobile/business/e/h;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/e/h;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    aput-object v5, v4, v6

    .line 106
    iget-object v5, p0, Lcom/swedbank/mobile/app/contact/b/c$h;->e:Lcom/swedbank/mobile/business/util/l;

    .line 128
    sget-object v6, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v5, ""

    goto :goto_0

    .line 129
    :cond_0
    instance-of v6, v5, Lcom/swedbank/mobile/business/util/n;

    if-eqz v6, :cond_1

    check-cast v5, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v5

    :goto_0
    const/4 v6, 0x3

    aput-object v5, v4, v6

    .line 102
    invoke-virtual {v2, v3, v4}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 99
    new-instance v3, Lcom/swedbank/mobile/business/contact/a$b;

    invoke-direct {v3, v0, v1, v2}, Lcom/swedbank/mobile/business/contact/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 130
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/contact/b/c$h;->a()Lcom/swedbank/mobile/business/contact/a$b;

    move-result-object v0

    return-object v0
.end method
