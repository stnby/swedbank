.class public Lcom/swedbank/mobile/app/contact/b/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "NotAuthenticatedContactPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/contact/g;",
        "Lcom/swedbank/mobile/app/contact/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/swedbank/mobile/business/contact/notauth/a;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/contact/notauth/a;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/contact/notauth/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/b/c;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/contact/b/c;->b:Lcom/swedbank/mobile/business/contact/notauth/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/contact/b/c;)Lcom/swedbank/mobile/business/contact/notauth/a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/contact/b/c;->b:Lcom/swedbank/mobile/business/contact/notauth/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/contact/b/c;)Landroid/app/Application;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/contact/b/c;->a:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 31
    sget-object v0, Lcom/swedbank/mobile/app/contact/b/c$b;->a:Lcom/swedbank/mobile/app/contact/b/c$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/contact/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/swedbank/mobile/app/contact/b/c$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/contact/b/c$c;-><init>(Lcom/swedbank/mobile/app/contact/b/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/contact/b/c;->b:Lcom/swedbank/mobile/business/contact/notauth/a;

    .line 37
    invoke-interface {v1}, Lcom/swedbank/mobile/business/contact/notauth/a;->a()Lio/reactivex/w;

    move-result-object v1

    .line 38
    new-instance v2, Lcom/swedbank/mobile/app/contact/b/c$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/contact/b/c$d;-><init>(Lcom/swedbank/mobile/app/contact/b/c;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "interactor\n        .getL\u2026.toObservable()\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v2, Lkotlin/e/b/x;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lkotlin/e/b/x;-><init>(I)V

    .line 52
    check-cast v0, Lio/reactivex/s;

    invoke-virtual {v2, v0}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 53
    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v2, v1}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 54
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/contact/b/c;->c()[Lio/reactivex/o;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/e/b/x;->a(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lkotlin/e/b/x;->a()I

    move-result v0

    new-array v0, v0, [Lio/reactivex/s;

    invoke-virtual {v2, v0}, Lkotlin/e/b/x;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/s;

    .line 51
    invoke-static {v0}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026*bindAdditionalActions())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/contact/b/c;->b()Lcom/swedbank/mobile/app/contact/j;

    move-result-object v1

    new-instance v2, Lcom/swedbank/mobile/app/contact/b/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/contact/b/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/contact/b/c$a;-><init>(Lcom/swedbank/mobile/app/contact/b/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 128
    new-instance v3, Lcom/swedbank/mobile/app/contact/b/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/contact/b/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 129
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method

.method protected b()Lcom/swedbank/mobile/app/contact/j;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    new-instance v6, Lcom/swedbank/mobile/app/contact/j;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/contact/j;-><init>(Ljava/lang/Throwable;Ljava/util/List;Ljava/util/List;ILkotlin/e/b/g;)V

    return-object v6
.end method

.method protected c()[Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/contact/j$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x0

    .line 127
    new-array v0, v0, [Lio/reactivex/o;

    return-object v0
.end method
