.class public final enum Lcom/swedbank/mobile/app/contact/f;
.super Ljava/lang/Enum;
.source "ContactMethod.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/app/contact/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/app/contact/f;

.field public static final enum b:Lcom/swedbank/mobile/app/contact/f;

.field private static final synthetic c:[Lcom/swedbank/mobile/app/contact/f;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/swedbank/mobile/app/contact/f;

    new-instance v1, Lcom/swedbank/mobile/app/contact/f;

    const-string v2, "FEEDBACK"

    .line 21
    sget v3, Lcom/swedbank/mobile/core/a$f;->contact_separator_feedback_title:I

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/app/contact/f;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/app/contact/f;->a:Lcom/swedbank/mobile/app/contact/f;

    aput-object v1, v0, v4

    new-instance v1, Lcom/swedbank/mobile/app/contact/f;

    const-string v2, "GENERAL"

    .line 22
    sget v3, Lcom/swedbank/mobile/core/a$f;->contact_separator_general_title:I

    const/4 v4, 0x1

    invoke-direct {v1, v2, v4, v3}, Lcom/swedbank/mobile/app/contact/f;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/swedbank/mobile/app/contact/f;->b:Lcom/swedbank/mobile/app/contact/f;

    aput-object v1, v0, v4

    sput-object v0, Lcom/swedbank/mobile/app/contact/f;->c:[Lcom/swedbank/mobile/app/contact/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/swedbank/mobile/app/contact/f;->d:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/app/contact/f;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/contact/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/app/contact/f;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/app/contact/f;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/app/contact/f;->c:[Lcom/swedbank/mobile/app/contact/f;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/app/contact/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/app/contact/f;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/swedbank/mobile/app/contact/f;->d:I

    return v0
.end method

.method public final a(Ljava/util/List;)Lcom/swedbank/mobile/app/contact/a;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/contact/b;",
            ">;)",
            "Lcom/swedbank/mobile/app/contact/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "methods"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/swedbank/mobile/app/contact/a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/contact/a;-><init>(Lcom/swedbank/mobile/app/contact/f;Ljava/util/List;)V

    return-object v0
.end method
