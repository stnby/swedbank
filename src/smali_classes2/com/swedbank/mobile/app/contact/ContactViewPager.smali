.class public final Lcom/swedbank/mobile/app/contact/ContactViewPager;
.super Landroidx/n/a/b;
.source "ContactViewPager.kt"


# instance fields
.field private d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/swedbank/mobile/app/contact/ContactViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1, p2}, Landroidx/n/a/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 12
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/contact/ContactViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a(Landroidx/n/a/a;I)V
    .locals 1
    .param p1    # Landroidx/n/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/ContactViewPager;->setAdapter(Landroidx/n/a/a;)V

    .line 28
    iget-object p1, p0, Lcom/swedbank/mobile/app/contact/ContactViewPager;->d:Landroid/util/SparseArray;

    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/ContactViewPager;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 31
    invoke-virtual {p0, p2, p1}, Lcom/swedbank/mobile/app/contact/ContactViewPager;->a(IZ)V

    :goto_0
    const/4 p1, 0x0

    .line 34
    check-cast p1, Landroid/util/SparseArray;

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/ContactViewPager;->d:Landroid/util/SparseArray;

    return-void
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .param p1    # Landroid/util/SparseArray;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-super {p0, p1}, Landroidx/n/a/b;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/contact/ContactViewPager;->getAdapter()Landroidx/n/a/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 19
    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/ContactViewPager;->d:Landroid/util/SparseArray;

    :cond_0
    return-void
.end method
