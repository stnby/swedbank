.class public final Lcom/swedbank/mobile/app/contact/b;
.super Ljava/lang/Object;
.source "ContactMethod.kt"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/contact/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILkotlin/e/a/a;Lkotlin/e/a/a;)V
    .locals 1
    .param p3    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lkotlin/e/a/a<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/e/a/a<",
            "+",
            "Lcom/swedbank/mobile/business/contact/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "description"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/swedbank/mobile/app/contact/b;->a:I

    iput p2, p0, Lcom/swedbank/mobile/app/contact/b;->b:I

    iput-object p3, p0, Lcom/swedbank/mobile/app/contact/b;->c:Lkotlin/e/a/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/contact/b;->d:Lkotlin/e/a/a;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/swedbank/mobile/app/contact/b;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/swedbank/mobile/app/contact/b;->b:I

    return v0
.end method

.method public final c()Lkotlin/e/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/a<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/b;->c:Lkotlin/e/a/a;

    return-object v0
.end method

.method public final d()Lkotlin/e/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/contact/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/b;->d:Lkotlin/e/a/a;

    return-object v0
.end method
