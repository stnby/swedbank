.class final Lcom/swedbank/mobile/app/contact/a/e$a;
.super Lkotlin/e/b/k;
.source "AuthenticatedContactRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/contact/a/e;->a(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/contact/a/e;

.field final synthetic b:Lkotlin/h/b;

.field final synthetic c:Lkotlin/e/a/b;

.field final synthetic d:Lcom/swedbank/mobile/business/i/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/contact/a/e;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->a:Lcom/swedbank/mobile/app/contact/a/e;

    iput-object p2, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->b:Lkotlin/h/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->c:Lkotlin/e/a/b;

    iput-object p4, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->d:Lcom/swedbank/mobile/business/i/c;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->b:Lkotlin/h/b;

    .line 42
    invoke-static {v0}, Lkotlin/e/a;->a(Lkotlin/h/b;)Ljava/lang/Class;

    move-result-object v0

    .line 43
    check-cast p1, Ljava/lang/Iterable;

    .line 44
    move-object v1, p1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    .line 45
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    if-nez v2, :cond_3

    .line 35
    iget-object p1, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->a:Lcom/swedbank/mobile/app/contact/a/e;

    iget-object v0, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->c:Lkotlin/e/a/b;

    iget-object v1, p0, Lcom/swedbank/mobile/app/contact/a/e$a;->d:Lcom/swedbank/mobile/business/i/c;

    invoke-interface {v0, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 48
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/contact/a/e$a;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
