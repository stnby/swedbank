.class public final Lcom/swedbank/mobile/app/contact/a/e;
.super Lcom/swedbank/mobile/app/contact/d;
.source "AuthenticatedContactRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/contact/auth/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authenticated_contact"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_authenticated_contact"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/data/device/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/app/contact/d;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->e(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/i/c;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/i/c;",
            ")V"
        }
    .end annotation

    const-string v0, "routerClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/swedbank/mobile/app/contact/a/e$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/contact/a/e$a;-><init>(Lcom/swedbank/mobile/app/contact/a/e;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/contact/a/e;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/i/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcom/swedbank/mobile/architect/business/a/e;",
            ">(",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/i/c;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lcom/swedbank/mobile/business/i/c;",
            ")",
            "Lio/reactivex/j<",
            "TN;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "routerClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0, p1, p2, p3}, Lcom/swedbank/mobile/business/contact/auth/c$a;->a(Lcom/swedbank/mobile/business/contact/auth/c;Lkotlin/h/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/business/i/c;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
