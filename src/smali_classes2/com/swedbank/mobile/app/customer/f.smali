.class public final Lcom/swedbank/mobile/app/customer/f;
.super Ljava/lang/Object;
.source "Customers.kt"


# static fields
.field private static final a:[Ljava/util/regex/Pattern;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xa

    .line 25
    new-array v0, v0, [Ljava/util/regex/Pattern;

    const-string v1, "as"

    .line 26
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ys"

    .line 27
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "is"

    .line 28
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    const-string v1, "us"

    .line 29
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v5, 0x3

    aput-object v1, v0, v5

    const-string v1, "\u0117"

    .line 30
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v6, 0x4

    aput-object v1, v0, v6

    const-string v1, "AS"

    .line 31
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v7, 0x5

    aput-object v1, v0, v7

    const-string v1, "YS"

    .line 32
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v7, 0x6

    aput-object v1, v0, v7

    const-string v1, "IS"

    .line 33
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/4 v7, 0x7

    aput-object v1, v0, v7

    const-string v1, "US"

    .line 34
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/16 v7, 0x8

    aput-object v1, v0, v7

    const-string v1, "\u0116"

    .line 35
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const/16 v7, 0x9

    aput-object v1, v0, v7

    .line 25
    sput-object v0, Lcom/swedbank/mobile/app/customer/f;->a:[Ljava/util/regex/Pattern;

    const-string v8, "ai$1"

    const-string v9, "y$1"

    const-string v10, "i$1"

    const-string v11, "au$1"

    const-string v12, "e$1"

    const-string v13, "AI$1"

    const-string v14, "Y$1"

    const-string v15, "I$1"

    const-string v16, "AU$1"

    const-string v17, "E$1"

    .line 37
    filled-new-array/range {v8 .. v17}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/customer/f;->b:[Ljava/lang/String;

    .line 49
    new-array v0, v6, [Ljava/util/regex/Pattern;

    const-string v1, "s"

    .line 50
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "\u0161"

    .line 51
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "S"

    .line 52
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "\u0160"

    .line 53
    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    aput-object v1, v0, v5

    .line 49
    sput-object v0, Lcom/swedbank/mobile/app/customer/f;->c:[Ljava/util/regex/Pattern;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$capitalizeCustomerName"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const-string p0, " "

    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    const-string p0, " "

    .line 13
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    .line 14
    sget-object p0, Lcom/swedbank/mobile/app/customer/f$a;->a:Lcom/swedbank/mobile/app/customer/f$a;

    move-object v6, p0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    .line 12
    invoke-static/range {v0 .. v8}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    const-string p0, "-"

    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    const-string p0, "-"

    .line 17
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    .line 18
    sget-object p0, Lcom/swedbank/mobile/app/customer/f$b;->a:Lcom/swedbank/mobile/app/customer/f$b;

    move-object v6, p0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 16
    invoke-static/range {v0 .. v8}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a()[Ljava/util/regex/Pattern;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/app/customer/f;->a:[Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private static final b(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 1

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "([ _\\-,;.]|$)"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p0

    const-string v0, "Pattern.compile(\"$regexp([ _\\\\-,;.]|$)\")"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final synthetic b()[Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/app/customer/f;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic c()[Ljava/util/regex/Pattern;
    .locals 1

    .line 1
    sget-object v0, Lcom/swedbank/mobile/app/customer/f;->c:[Ljava/util/regex/Pattern;

    return-object v0
.end method
