.class public final Lcom/swedbank/mobile/app/customer/selection/g;
.super Lcom/swedbank/mobile/architect/a/h;
.source "CustomerSelectionRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/j/d;
.implements Lcom/swedbank/mobile/business/customer/selection/d;


# instance fields
.field private final e:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/data/device/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/app/p/e;

.field private final h:Lcom/swedbank/mobile/business/preferences/d;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/app/p/e;Lcom/swedbank/mobile/business/preferences/d;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/p/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/preferences/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_customer_selection"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_customer_selection"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_customer_selection"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/app/p/e;",
            "Lcom/swedbank/mobile/business/preferences/d;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferencesBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferencesListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p5, p7, p6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/g;->e:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/customer/selection/g;->f:Lcom/swedbank/mobile/data/device/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/customer/selection/g;->g:Lcom/swedbank/mobile/app/p/e;

    iput-object p4, p0, Lcom/swedbank/mobile/app/customer/selection/g;->h:Lcom/swedbank/mobile/business/preferences/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/customer/selection/g;)Lcom/swedbank/mobile/app/p/e;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/g;->g:Lcom/swedbank/mobile/app/p/e;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/customer/selection/g;)Lcom/swedbank/mobile/business/preferences/d;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/g;->h:Lcom/swedbank/mobile/business/preferences/d;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 30
    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/g$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/customer/selection/g$b;-><init>(Lcom/swedbank/mobile/app/customer/selection/g;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/selection/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 42
    sget-object v0, Lcom/swedbank/mobile/app/customer/selection/g$a;->a:Lcom/swedbank/mobile/app/customer/selection/g$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0, p1}, Lcom/swedbank/mobile/app/j/d$a;->a(Lcom/swedbank/mobile/app/j/d;Ljava/lang/String;)V

    return-void
.end method

.method public i()Landroid/app/Application;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/g;->e:Landroid/app/Application;

    return-object v0
.end method

.method public j()Lcom/swedbank/mobile/data/device/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/g;->f:Lcom/swedbank/mobile/data/device/a;

    return-object v0
.end method
