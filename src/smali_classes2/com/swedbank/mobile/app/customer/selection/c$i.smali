.class final Lcom/swedbank/mobile/app/customer/selection/c$i;
.super Ljava/lang/Object;
.source "CustomerSelectionPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/customer/selection/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/customer/selection/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/customer/selection/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 4
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/customer/selection/m$a;",
            ">;"
        }
    .end annotation

    const-string v0, "userInitiated"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/business/customer/selection/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/customer/selection/a;->b()Lio/reactivex/w;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/swedbank/mobile/app/customer/selection/c$i$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/customer/selection/c$i$1;-><init>(Lcom/swedbank/mobile/app/customer/selection/c$i;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 49
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    .line 50
    iget-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/customer/selection/c;->b(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-static {p1, v3, v2, v1}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 56
    sget-object v1, Lcom/swedbank/mobile/app/customer/selection/m$a$g;->a:Lcom/swedbank/mobile/app/customer/selection/m$a$g;

    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 49
    invoke-static {p1, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 57
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/customer/selection/c;->b(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-static {p1, v3, v2, v1}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 48
    :goto_0
    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v0, p1}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 59
    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/c$i$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/customer/selection/c$i$2;-><init>(Lcom/swedbank/mobile/app/customer/selection/c$i;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/c$i;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
