.class public final Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e;
.super Lkotlin/e/b/k;
.source "CustomerSelectionListView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/business/customer/a;",
        ">;",
        "Lcom/swedbank/mobile/business/customer/a;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e;->a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/business/customer/a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/business/customer/a;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/business/customer/a;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/business/customer/a;",
            ">;",
            "Lcom/swedbank/mobile/business/customer/a;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customer"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0a00ff

    .line 74
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    .line 76
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/a;->e()Z

    move-result v2

    .line 75
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Ljava/lang/String;Z)V

    const v0, 0x7f0a0101

    .line 78
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 79
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0100

    .line 80
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/customer/a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 81
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object p1

    .line 119
    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e$a;

    invoke-direct {v0, p0, p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e$a;-><init>(Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e;Lcom/swedbank/mobile/business/customer/a;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
