.class public final Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;
.super Lkotlin/e/b/k;
.source "CustomerSelectionListView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;",
        ">;",
        "Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;->a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;",
            ">;",
            "Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0a00fc

    .line 88
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 89
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x7f0a00fd

    .line 90
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;->c()Lkotlin/e/a/b;

    move-result-object p2

    .line 119
    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d$a;

    invoke-direct {v0, p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d$a;-><init>(Lkotlin/e/a/b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
