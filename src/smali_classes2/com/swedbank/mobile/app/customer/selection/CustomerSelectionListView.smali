.class public final Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "CustomerSelectionListView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;,
        Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$c;,
        Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/customer/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/core/ui/widget/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p2

    const-string p3, "PublishRelay.create()"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->a:Lcom/b/c/c;

    const/4 p2, 0x4

    .line 34
    new-array p3, p2, [Lkotlin/k;

    const/4 v0, 0x1

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 160
    sget-object v2, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$g;->a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$g;

    check-cast v2, Lkotlin/e/a/m;

    const v3, 0x7f0d0040

    .line 158
    invoke-static {v3, v2}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v2

    .line 35
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, p3, v2

    const/4 v1, 0x2

    .line 36
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 163
    new-instance v3, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$e;-><init>(Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;)V

    check-cast v3, Lkotlin/e/a/m;

    const v4, 0x7f0d003c

    .line 161
    invoke-static {v4, v3}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    .line 36
    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, p3, v0

    const/4 v2, 0x3

    .line 37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 176
    sget-object v4, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;->a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$d;

    check-cast v4, Lkotlin/e/a/m;

    const v5, 0x7f0d003a

    .line 174
    invoke-static {v5, v4}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v4

    .line 37
    invoke-static {v3, v4}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v3

    aput-object v3, p3, v1

    .line 38
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 185
    sget-object v3, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$f;->a:Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$f;

    check-cast v3, Lkotlin/e/a/m;

    const v4, 0x7f0d003b

    .line 183
    invoke-static {v4, v3}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    .line 38
    invoke-static {p2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p2

    aput-object p2, p3, v2

    .line 34
    invoke-static {p3}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object p2

    .line 33
    new-instance p3, Lcom/swedbank/mobile/core/ui/widget/k;

    const/4 v2, 0x0

    invoke-direct {p3, v2, p2, v0, v2}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object p3, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->b:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 43
    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p2, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 44
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/f;

    new-instance p3, Lcom/swedbank/mobile/core/ui/widget/g$c;

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    invoke-direct {p3, v0}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v6, p3

    check-cast v6, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 48
    iget-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->b:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void

    :array_0
    .array-data 4
        0x1
        0x4
    .end array-data
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 29
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 30
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;)Lcom/b/c/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/customer/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 9
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "groups"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->b:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 119
    check-cast p1, Ljava/lang/Iterable;

    .line 120
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 127
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 128
    check-cast v2, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 129
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;->c()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    if-eqz v3, :cond_2

    .line 130
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;->c()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v5, v4

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 131
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    new-instance v6, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v6, v5, v4}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_0
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;->c()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 133
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 134
    move-object v6, v3

    check-cast v6, Ljava/util/Collection;

    .line 135
    new-instance v7, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;->a()I

    move-result v8

    invoke-direct {v7, v5, v8}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 136
    :cond_1
    move-object v2, v3

    check-cast v2, Ljava/util/Collection;

    .line 130
    check-cast v3, Ljava/util/List;

    goto :goto_2

    .line 138
    :cond_2
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v3

    :goto_2
    check-cast v3, Ljava/lang/Iterable;

    .line 139
    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 141
    :cond_3
    check-cast v1, Ljava/util/List;

    const/4 p1, 0x2

    const/4 v2, 0x0

    .line 52
    invoke-static {v0, v1, v2, p1, v2}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    return-void
.end method
