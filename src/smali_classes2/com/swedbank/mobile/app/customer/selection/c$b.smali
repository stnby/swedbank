.class final Lcom/swedbank/mobile/app/customer/selection/c$b;
.super Ljava/lang/Object;
.source "CustomerSelectionPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/customer/selection/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/customer/selection/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/c$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/customer/selection/c$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/customer/selection/c$b;->a:Lcom/swedbank/mobile/app/customer/selection/c$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lcom/swedbank/mobile/app/customer/selection/m$a$a;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/customer/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/customer/selection/m$a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 146
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/swedbank/mobile/business/customer/a;

    .line 71
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/customer/a;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 147
    :goto_0
    check-cast v1, Lcom/swedbank/mobile/business/customer/a;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/customer/a;->b()Ljava/lang/String;

    move-result-object v2

    .line 69
    :cond_2
    new-instance v0, Lcom/swedbank/mobile/app/customer/selection/m$a$a;

    invoke-direct {v0, p1, v2}, Lcom/swedbank/mobile/app/customer/selection/m$a$a;-><init>(Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/c$b;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/customer/selection/m$a$a;

    move-result-object p1

    return-object p1
.end method
