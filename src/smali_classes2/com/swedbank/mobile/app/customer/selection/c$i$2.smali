.class final Lcom/swedbank/mobile/app/customer/selection/c$i$2;
.super Ljava/lang/Object;
.source "CustomerSelectionPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/customer/selection/c$i;->a(Ljava/lang/Boolean;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/s<",
        "+",
        "Lcom/swedbank/mobile/app/customer/selection/m$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/customer/selection/c$i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/customer/selection/c$i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c$i$2;->a:Lcom/swedbank/mobile/app/customer/selection/c$i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/o;
    .locals 9
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/customer/selection/m$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/c$i$2;->a:Lcom/swedbank/mobile/app/customer/selection/c$i;

    iget-object v0, v0, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    .line 61
    new-instance v1, Lcom/swedbank/mobile/app/customer/selection/m$a$f;

    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/customer/selection/m$a$f;-><init>(Lcom/swedbank/mobile/app/w/b;)V

    move-object v3, v1

    check-cast v3, Lcom/swedbank/mobile/app/customer/selection/m$a;

    .line 62
    sget-object p1, Lcom/swedbank/mobile/app/customer/selection/m$a$e;->a:Lcom/swedbank/mobile/app/customer/selection/m$a$e;

    move-object v4, p1

    check-cast v4, Lcom/swedbank/mobile/app/customer/selection/m$a;

    .line 146
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/selection/c;->c(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v2

    const-wide/16 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 149
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/selection/c;->b(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "messageRenderStream.disp\u2026derStream.loading(false))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/c$i$2;->a(Ljava/lang/Throwable;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
