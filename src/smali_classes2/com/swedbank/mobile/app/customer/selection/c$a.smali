.class final synthetic Lcom/swedbank/mobile/app/customer/selection/c$a;
.super Lkotlin/e/b/i;
.source "CustomerSelectionPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/customer/selection/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/customer/selection/m;",
        "Lcom/swedbank/mobile/app/customer/selection/m$a;",
        "Lcom/swedbank/mobile/app/customer/selection/m;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/customer/selection/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/customer/selection/m;Lcom/swedbank/mobile/app/customer/selection/m$a;)Lcom/swedbank/mobile/app/customer/selection/m;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/customer/selection/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/customer/selection/m$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/customer/selection/c;

    .line 147
    instance-of v0, p2, Lcom/swedbank/mobile/app/customer/selection/m$a$a;

    if-eqz v0, :cond_0

    .line 148
    check-cast p2, Lcom/swedbank/mobile/app/customer/selection/m$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/m$a$a;->b()Ljava/lang/String;

    move-result-object v1

    .line 149
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/m$a$a;->a()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3c

    const/4 v8, 0x0

    move-object v0, p1

    .line 147
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    goto/16 :goto_0

    .line 150
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/customer/selection/m$a$g;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x23

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    goto/16 :goto_0

    .line 154
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/customer/selection/m$a$c;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 155
    check-cast p2, Lcom/swedbank/mobile/app/customer/selection/m$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/m$a$c;->a()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v1, p1

    .line 154
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    goto :goto_0

    .line 156
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/customer/selection/m$a$b;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 157
    check-cast p2, Lcom/swedbank/mobile/app/customer/selection/m$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/m$a$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x33

    const/4 v9, 0x0

    move-object v1, p1

    .line 156
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    goto :goto_0

    .line 159
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/customer/selection/m$a$d;->a:Lcom/swedbank/mobile/app/customer/selection/m$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x37

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    goto :goto_0

    .line 161
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/customer/selection/m$a$f;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 162
    check-cast p2, Lcom/swedbank/mobile/app/customer/selection/m$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/customer/selection/m$a$f;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    move-object v1, p1

    .line 161
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    goto :goto_0

    .line 163
    :cond_5
    sget-object v0, Lcom/swedbank/mobile/app/customer/selection/m$a$e;->a:Lcom/swedbank/mobile/app/customer/selection/m$a$e;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x2f

    const/4 v8, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/customer/selection/m;->a(Lcom/swedbank/mobile/app/customer/selection/m;Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/customer/selection/m;

    check-cast p2, Lcom/swedbank/mobile/app/customer/selection/m$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/customer/selection/c$a;->a(Lcom/swedbank/mobile/app/customer/selection/m;Lcom/swedbank/mobile/app/customer/selection/m$a;)Lcom/swedbank/mobile/app/customer/selection/m;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionViewState;Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionViewState$PartialState;)Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionViewState;"

    return-object v0
.end method
