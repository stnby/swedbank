.class public final Lcom/swedbank/mobile/app/customer/selection/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "CustomerSelectionViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/customer/selection/i;
.implements Lcom/swedbank/mobile/architect/a/b/i;
.implements Lcom/swedbank/mobile/core/ui/ao;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/b/a;",
        "Lcom/swedbank/mobile/app/customer/selection/i;",
        "Lcom/swedbank/mobile/architect/a/b/i<",
        "Lcom/swedbank/mobile/app/customer/selection/m;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/ao<",
        "Lcom/swedbank/mobile/app/customer/selection/m;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/widget/u;"
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/core/ui/aj;

.field private final j:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/customer/selection/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/customer/selection/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/customer/selection/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "listView"

    const-string v4, "getListView()Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/customer/selection/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/core/ui/aj;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/aj;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/aj;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transitionAwareRenderer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->i:Lcom/swedbank/mobile/core/ui/aj;

    iput-object p2, p0, Lcom/swedbank/mobile/app/customer/selection/j;->j:Lio/reactivex/o;

    const p1, 0x7f0a02dd

    .line 38
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->c:Lkotlin/f/c;

    const p1, 0x7f0a0103

    .line 39
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->d:Lkotlin/f/c;

    const p1, 0x7f0a00fb

    .line 40
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->e:Lkotlin/f/c;

    .line 42
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->f:Lcom/b/c/c;

    .line 43
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->g:Lcom/b/c/c;

    .line 44
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->h:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/customer/selection/j;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->j()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/customer/selection/j;)Landroidx/appcompat/widget/Toolbar;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->i()Landroidx/appcompat/widget/Toolbar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/customer/selection/j;)Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->k()Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;
    .locals 0

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/customer/selection/j;)Lcom/b/c/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->g:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/customer/selection/j;)Lcom/b/c/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->f:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/customer/selection/j;)Lcom/b/c/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->h:Lcom/b/c/c;

    return-object p0
.end method

.method private final i()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/customer/selection/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final j()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/customer/selection/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final k()Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/customer/selection/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->j()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 65
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 63
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/customer/selection/m;)V
    .locals 18
    .param p1    # Lcom/swedbank/mobile/app/customer/selection/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const-string v1, "viewState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/customer/selection/m;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 75
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/customer/selection/m;->c()Z

    move-result v3

    .line 184
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->a(Lcom/swedbank/mobile/app/customer/selection/j;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 77
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/customer/selection/m;->a()Ljava/lang/String;

    move-result-object v3

    .line 78
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-le v4, v6, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v3, :cond_3

    if-eqz v4, :cond_2

    .line 192
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->b(Lcom/swedbank/mobile/app/customer/selection/j;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v3

    const v4, 0x7f110121

    invoke-virtual {v3, v4}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    goto :goto_2

    .line 193
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->b(Lcom/swedbank/mobile/app/customer/selection/j;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v4

    invoke-static {v3}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 81
    :cond_3
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/customer/selection/m;->f()Ljava/lang/String;

    move-result-object v3

    .line 198
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v6, :cond_4

    goto :goto_3

    :cond_4
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v1

    .line 199
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->c(Lcom/swedbank/mobile/app/customer/selection/j;)Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;

    move-result-object v4

    .line 208
    new-instance v7, Ljava/util/ArrayList;

    const/4 v8, 0x4

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 209
    move-object v8, v1

    check-cast v8, Ljava/lang/Iterable;

    .line 210
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 211
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 212
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    .line 213
    move-object v12, v11

    check-cast v12, Lcom/swedbank/mobile/business/customer/a;

    .line 209
    invoke-virtual {v12}, Lcom/swedbank/mobile/business/customer/a;->e()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 214
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 216
    :cond_5
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 219
    :cond_6
    new-instance v8, Lkotlin/k;

    invoke-direct {v8, v9, v10}, Lkotlin/k;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v8}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v9

    .line 209
    check-cast v9, Ljava/util/List;

    invoke-virtual {v8}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 221
    move-object v10, v8

    check-cast v10, Ljava/util/Collection;

    invoke-interface {v10}, Ljava/util/Collection;->isEmpty()Z

    move-result v11

    xor-int/2addr v11, v6

    const/4 v12, 0x2

    if-eqz v11, :cond_7

    move-object v11, v9

    check-cast v11, Ljava/util/Collection;

    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    move-result v11

    xor-int/2addr v11, v6

    if-eqz v11, :cond_7

    .line 222
    new-instance v10, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 224
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v11

    const v13, 0x7f11011f

    invoke-virtual {v11, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 222
    invoke-direct {v10, v12, v11, v8}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    new-instance v8, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 228
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f11011d

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 226
    invoke-direct {v8, v12, v10, v9}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 231
    :cond_7
    invoke-interface {v10}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    xor-int/2addr v10, v6

    const v11, 0x7f110120

    if-eqz v10, :cond_8

    new-instance v9, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 233
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 231
    invoke-direct {v9, v12, v10, v8}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 235
    :cond_8
    move-object v8, v9

    check-cast v8, Ljava/util/Collection;

    invoke-interface {v8}, Ljava/util/Collection;->isEmpty()Z

    move-result v8

    xor-int/2addr v8, v6

    if-eqz v8, :cond_9

    new-instance v8, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 237
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 235
    invoke-direct {v8, v12, v10, v9}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_9
    :goto_5
    new-instance v8, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 242
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f11011e

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 243
    new-array v10, v12, [Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;

    .line 244
    new-instance v11, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;

    const v12, 0x7f0800d7

    .line 246
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f11011b

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "context().getString(R.st\u2026er_selection_preferences)"

    invoke-static {v13, v14}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    new-instance v14, Lcom/swedbank/mobile/app/customer/selection/j$a;

    invoke-direct {v14, v0, v1, v3}, Lcom/swedbank/mobile/app/customer/selection/j$a;-><init>(Lcom/swedbank/mobile/app/customer/selection/j;Ljava/util/List;Ljava/lang/String;)V

    check-cast v14, Lkotlin/e/a/b;

    .line 244
    invoke-direct {v11, v12, v13, v14}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;-><init>(ILjava/lang/String;Lkotlin/e/a/b;)V

    aput-object v11, v10, v5

    .line 248
    new-instance v11, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;

    const v12, 0x7f0800be

    .line 250
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f11011a

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "context().getString(R.st\u2026stomer_selection_log_out)"

    invoke-static {v13, v14}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    new-instance v14, Lcom/swedbank/mobile/app/customer/selection/j$b;

    invoke-direct {v14, v0, v1, v3}, Lcom/swedbank/mobile/app/customer/selection/j$b;-><init>(Lcom/swedbank/mobile/app/customer/selection/j;Ljava/util/List;Ljava/lang/String;)V

    check-cast v14, Lkotlin/e/a/b;

    .line 248
    invoke-direct {v11, v12, v13, v14}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;-><init>(ILjava/lang/String;Lkotlin/e/a/b;)V

    aput-object v11, v10, v6

    .line 243
    invoke-static {v10}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x3

    .line 240
    invoke-direct {v8, v11, v9, v10}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    new-instance v8, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    .line 254
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f11011c

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 256
    new-instance v10, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;

    const v12, 0x7f0800ba

    .line 258
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f110119

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "context().getString(R.st\u2026n_data_processing_policy)"

    invoke-static {v13, v14}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    new-instance v14, Lcom/swedbank/mobile/app/customer/selection/j$c;

    invoke-direct {v14, v0, v1, v3}, Lcom/swedbank/mobile/app/customer/selection/j$c;-><init>(Lcom/swedbank/mobile/app/customer/selection/j;Ljava/util/List;Ljava/lang/String;)V

    check-cast v14, Lkotlin/e/a/b;

    .line 256
    invoke-direct {v10, v12, v13, v14}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$a;-><init>(ILjava/lang/String;Lkotlin/e/a/b;)V

    .line 255
    invoke-static {v10}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 252
    invoke-direct {v8, v11, v9, v1}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_a

    .line 261
    new-instance v1, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;

    const/4 v13, 0x4

    const/4 v14, 0x0

    .line 263
    new-instance v8, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$c;

    .line 264
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d(Lcom/swedbank/mobile/app/customer/selection/j;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f110118

    new-array v11, v6, [Ljava/lang/Object;

    aput-object v3, v11, v5

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "context().getString(R.st\u2026election_app_version, it)"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-direct {v8, v3}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$c;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    const/16 v16, 0x2

    const/16 v17, 0x0

    move-object v12, v1

    .line 261
    invoke-direct/range {v12 .. v17}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView$b;-><init>(ILjava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_a
    check-cast v7, Ljava/util/List;

    .line 199
    invoke-virtual {v4, v7}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->a(Ljava/util/List;)V

    .line 83
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->n()Landroid/content/Context;

    move-result-object v1

    .line 84
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/customer/selection/m;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object v3

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/customer/selection/m;->e()Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    if-eqz v3, :cond_e

    const v2, 0x7f110125

    .line 277
    instance-of v4, v3, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v4, :cond_c

    check-cast v3, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 279
    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v4, v6

    if-eqz v4, :cond_b

    move-object v5, v3

    check-cast v5, Ljava/lang/Iterable;

    const-string v2, "\n"

    move-object v6, v2

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 280
    :cond_b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 282
    :cond_c
    instance-of v2, v3, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_d

    check-cast v3, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    .line 283
    invoke-static {v2}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    const-string v3, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 284
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 286
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    const v4, 0x7f1101f4

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v3, v1}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 271
    invoke-virtual {v0, v2, v3}, Lcom/swedbank/mobile/app/customer/selection/j;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_7

    .line 283
    :cond_d
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_e
    if-eqz v2, :cond_f

    .line 288
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(fatalError.userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 289
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v6, v3}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v2, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 287
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/app/customer/selection/j;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_7

    .line 290
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/customer/selection/j;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :goto_7
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Lcom/swedbank/mobile/app/customer/selection/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/j;->b(Lcom/swedbank/mobile/app/customer/selection/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->j:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->i()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/app/customer/selection/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/customer/selection/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->i:Lcom/swedbank/mobile/core/ui/aj;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/ui/aj;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Lcom/swedbank/mobile/app/customer/selection/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/j;->a(Lcom/swedbank/mobile/app/customer/selection/m;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->g:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 3

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->i:Lcom/swedbank/mobile/core/ui/aj;

    .line 55
    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a;

    const/4 v2, 0x1

    .line 54
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/core/ui/aj;->a(Lcom/swedbank/mobile/architect/a/b/a;Z)V

    .line 181
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 182
    new-instance v1, Lcom/swedbank/mobile/app/customer/selection/j$d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/customer/selection/j$d;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/customer/selection/k;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/customer/selection/k;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 183
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/selection/j;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 58
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->i()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    const v1, 0x7f0800a2

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(I)V

    .line 59
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->i()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->k()Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 60
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->j()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    return-void

    :array_0
    .array-data 4
        0x7f06002e
        0x7f060030
    .end array-data
.end method

.method public f()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->h:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/j;->f:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/customer/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 71
    invoke-direct {p0}, Lcom/swedbank/mobile/app/customer/selection/j;->k()Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/customer/selection/CustomerSelectionListView;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
