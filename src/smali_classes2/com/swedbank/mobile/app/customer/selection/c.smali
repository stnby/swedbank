.class public final Lcom/swedbank/mobile/app/customer/selection/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "CustomerSelectionPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/customer/selection/i;",
        "Lcom/swedbank/mobile/app/customer/selection/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;

.field private final b:Lcom/swedbank/mobile/core/ui/ad;

.field private final c:Lcom/swedbank/mobile/business/customer/selection/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/selection/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/selection/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c;->c:Lcom/swedbank/mobile/business/customer/selection/a;

    .line 20
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 21
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/business/customer/selection/a;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/c;->c:Lcom/swedbank/mobile/business/customer/selection/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/c;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/selection/c;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 11

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/c;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v0

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/customer/selection/c$l;->a:Lcom/swedbank/mobile/app/customer/selection/c$l;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/customer/selection/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/customer/selection/e;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const/4 v1, 0x0

    .line 35
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 36
    sget-object v3, Lcom/swedbank/mobile/app/customer/selection/c$g;->a:Lcom/swedbank/mobile/app/customer/selection/c$g;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    sget-object v4, Lcom/swedbank/mobile/app/customer/selection/c$h;->a:Lcom/swedbank/mobile/app/customer/selection/c$h;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 34
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 37
    new-instance v3, Lcom/swedbank/mobile/app/customer/selection/c$i;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/customer/selection/c$i;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 66
    iget-object v3, p0, Lcom/swedbank/mobile/app/customer/selection/c;->c:Lcom/swedbank/mobile/business/customer/selection/a;

    .line 67
    invoke-interface {v3}, Lcom/swedbank/mobile/business/customer/selection/a;->c()Lio/reactivex/o;

    move-result-object v3

    .line 68
    sget-object v4, Lcom/swedbank/mobile/app/customer/selection/c$b;->a:Lcom/swedbank/mobile/app/customer/selection/c$b;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 74
    sget-object v4, Lcom/swedbank/mobile/app/customer/selection/c$e;->a:Lcom/swedbank/mobile/app/customer/selection/c$e;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 75
    new-instance v5, Lcom/swedbank/mobile/app/customer/selection/c$f;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/customer/selection/c$f;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v4

    .line 76
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    .line 78
    sget-object v5, Lcom/swedbank/mobile/app/customer/selection/c$o;->a:Lcom/swedbank/mobile/app/customer/selection/c$o;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 79
    new-instance v6, Lcom/swedbank/mobile/app/customer/selection/c$p;

    invoke-direct {v6, p0}, Lcom/swedbank/mobile/app/customer/selection/c$p;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 80
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 81
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 83
    sget-object v6, Lcom/swedbank/mobile/app/customer/selection/c$j;->a:Lcom/swedbank/mobile/app/customer/selection/c$j;

    check-cast v6, Lkotlin/e/a/b;

    invoke-virtual {p0, v6}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v6

    .line 84
    new-instance v7, Lcom/swedbank/mobile/app/customer/selection/c$k;

    invoke-direct {v7, p0}, Lcom/swedbank/mobile/app/customer/selection/c$k;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v6, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v6

    .line 85
    invoke-virtual {v6}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v6

    .line 86
    invoke-virtual {v6}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v6

    .line 88
    sget-object v7, Lcom/swedbank/mobile/app/customer/selection/c$m;->a:Lcom/swedbank/mobile/app/customer/selection/c$m;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {p0, v7}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    .line 89
    new-instance v8, Lcom/swedbank/mobile/app/customer/selection/c$n;

    invoke-direct {v8, p0}, Lcom/swedbank/mobile/app/customer/selection/c$n;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v8, Lio/reactivex/c/g;

    invoke-virtual {v7, v8}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v7

    .line 90
    invoke-virtual {v7}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v7

    .line 91
    invoke-virtual {v7}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v7

    .line 93
    sget-object v8, Lcom/swedbank/mobile/app/customer/selection/c$c;->a:Lcom/swedbank/mobile/app/customer/selection/c$c;

    check-cast v8, Lkotlin/e/a/b;

    invoke-virtual {p0, v8}, Lcom/swedbank/mobile/app/customer/selection/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v8

    .line 94
    new-instance v9, Lcom/swedbank/mobile/app/customer/selection/c$d;

    invoke-direct {v9, p0}, Lcom/swedbank/mobile/app/customer/selection/c$d;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v9, Lio/reactivex/c/g;

    invoke-virtual {v8, v9}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v8

    .line 95
    invoke-virtual {v8}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v8

    .line 96
    invoke-virtual {v8}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v8

    const/16 v9, 0x8

    .line 98
    new-array v9, v9, [Lio/reactivex/s;

    .line 99
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v9, v1

    const/4 v0, 0x1

    .line 100
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v9, v0

    const/4 v0, 0x2

    .line 101
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v9, v0

    const/4 v0, 0x3

    .line 102
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v9, v0

    const/4 v0, 0x4

    .line 103
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v9, v0

    const/4 v0, 0x5

    .line 104
    check-cast v6, Lio/reactivex/s;

    aput-object v6, v9, v0

    const/4 v0, 0x6

    .line 105
    check-cast v7, Lio/reactivex/s;

    aput-object v7, v9, v0

    const/4 v0, 0x7

    .line 106
    check-cast v8, Lio/reactivex/s;

    aput-object v8, v9, v0

    .line 98
    invoke-static {v9}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026\n        backClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v1, Lcom/swedbank/mobile/app/customer/selection/m;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 109
    iget-object v2, p0, Lcom/swedbank/mobile/app/customer/selection/c;->c:Lcom/swedbank/mobile/business/customer/selection/a;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/customer/selection/a;->a()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x1f

    const/4 v10, 0x0

    move-object v2, v1

    .line 108
    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/app/customer/selection/m;-><init>(Ljava/lang/String;Ljava/util/List;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 110
    new-instance v2, Lcom/swedbank/mobile/app/customer/selection/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/customer/selection/c$a;-><init>(Lcom/swedbank/mobile/app/customer/selection/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 146
    new-instance v3, Lcom/swedbank/mobile/app/customer/selection/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/customer/selection/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 147
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
