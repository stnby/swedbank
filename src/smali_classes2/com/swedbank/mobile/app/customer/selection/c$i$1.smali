.class final Lcom/swedbank/mobile/app/customer/selection/c$i$1;
.super Ljava/lang/Object;
.source "CustomerSelectionPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/customer/selection/c$i;->a(Ljava/lang/Boolean;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/customer/selection/c$i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/customer/selection/c$i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c$i$1;->a:Lcom/swedbank/mobile/app/customer/selection/c$i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/s;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/s<",
            "+",
            "Lcom/swedbank/mobile/app/customer/selection/m$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/customer/selection/c$i$1;->a:Lcom/swedbank/mobile/app/customer/selection/c$i;

    iget-object p1, p1, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/customer/selection/c;->b(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    goto :goto_0

    .line 43
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/selection/c$i$1;->a:Lcom/swedbank/mobile/app/customer/selection/c$i;

    iget-object v0, v0, Lcom/swedbank/mobile/app/customer/selection/c$i;->a:Lcom/swedbank/mobile/app/customer/selection/c;

    .line 44
    new-instance v2, Lcom/swedbank/mobile/app/customer/selection/m$a$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/customer/selection/m$a$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/app/customer/selection/m$a;

    .line 45
    sget-object p1, Lcom/swedbank/mobile/app/customer/selection/m$a$d;->a:Lcom/swedbank/mobile/app/customer/selection/m$a$d;

    move-object v5, p1

    check-cast v5, Lcom/swedbank/mobile/app/customer/selection/m$a;

    .line 146
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/selection/c;->c(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v3

    const-wide/16 v6, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 149
    invoke-static {v0}, Lcom/swedbank/mobile/app/customer/selection/c;->b(Lcom/swedbank/mobile/app/customer/selection/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "messageRenderStream.disp\u2026derStream.loading(false))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/s;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/selection/c$i$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/s;

    move-result-object p1

    return-object p1
.end method
