.class final synthetic Lcom/swedbank/mobile/app/customer/d$a;
.super Lkotlin/e/b/i;
.source "CustomerRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/customer/d;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/a/h;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/customer/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/customer/d$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/customer/d$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/customer/d$a;->a:Lcom/swedbank/mobile/app/customer/d$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 2

    const-class v0, Lcom/swedbank/mobile/a/p/b/c;

    const-string v1, "app_ltRelease"

    invoke-static {v0, v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/architect/a/h;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    instance-of v0, p1, Lcom/swedbank/mobile/business/navigation/m;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/swedbank/mobile/business/navigation/m;

    invoke-interface {p1}, Lcom/swedbank/mobile/business/navigation/m;->c()Ljava/lang/String;

    move-result-object p1

    const-string v0, "navigation_loading"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/d$a;->a(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "isLoadingNavigation"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "isLoadingNavigation(Lcom/swedbank/mobile/architect/app/Router;)Z"

    return-object v0
.end method
