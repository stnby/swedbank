.class public final Lcom/swedbank/mobile/app/customer/a;
.super Ljava/lang/Object;
.source "CustomerBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/a/i/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/i/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/i/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a;->c:Lcom/swedbank/mobile/a/i/a$a;

    .line 12
    sget-object p1, Lcom/swedbank/mobile/app/customer/a$a;->a:Lcom/swedbank/mobile/app/customer/a$a;

    check-cast p1, Lkotlin/e/a/b;

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a;->b:Lkotlin/e/a/b;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/customer/a;
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)",
            "Lcom/swedbank/mobile/app/customer/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "buildCallback"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a;->a:Lkotlin/e/a/b;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/a;->c:Lcom/swedbank/mobile/a/i/a$a;

    .line 25
    iget-object v1, p0, Lcom/swedbank/mobile/app/customer/a;->a:Lkotlin/e/a/b;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/i/a$a;->d(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/i/a$a;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/swedbank/mobile/app/customer/a;->b:Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/i/a$a;->c(Lkotlin/e/a/b;)Lcom/swedbank/mobile/a/i/a$a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/i/a$a;->a()Lcom/swedbank/mobile/a/i/a;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/a/i/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Customer node built with no callback for when further routing node is attached"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 25
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Customer node built with no callback for building nodes for further routing"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final b(Lkotlin/e/a/b;)Lcom/swedbank/mobile/app/customer/a;
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;)",
            "Lcom/swedbank/mobile/app/customer/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "attachedCallback"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a;->b:Lkotlin/e/a/b;

    return-object p0
.end method
