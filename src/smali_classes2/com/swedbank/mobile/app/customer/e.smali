.class public final Lcom/swedbank/mobile/app/customer/e;
.super Ljava/lang/Object;
.source "CustomerRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/customer/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/s/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/g;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/retry/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/s/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/retry/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/e;->a:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/swedbank/mobile/app/customer/e;->b:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/swedbank/mobile/app/customer/e;->c:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/swedbank/mobile/app/customer/e;->d:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/swedbank/mobile/app/customer/e;->e:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/swedbank/mobile/app/customer/e;->f:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/swedbank/mobile/app/customer/e;->g:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/swedbank/mobile/app/customer/e;->h:Ljavax/inject/Provider;

    .line 54
    iput-object p9, p0, Lcom/swedbank/mobile/app/customer/e;->i:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/customer/e;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/navigation/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/s/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/retry/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/customer/e;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/swedbank/mobile/app/customer/e;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/app/customer/e;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/customer/d;
    .locals 11

    .line 59
    new-instance v10, Lcom/swedbank/mobile/app/customer/d;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/app/navigation/b/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/app/s/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/customer/e;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/customer/g;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/general/retry/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lkotlin/e/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/architect/business/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/e;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/architect/a/b/g;

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/app/customer/d;-><init>(Lcom/swedbank/mobile/app/navigation/b/a;Lcom/swedbank/mobile/app/s/a;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/customer/g;Lcom/swedbank/mobile/business/general/retry/c;Lkotlin/e/a/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v10
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/customer/e;->a()Lcom/swedbank/mobile/app/customer/d;

    move-result-object v0

    return-object v0
.end method
