.class public final Lcom/swedbank/mobile/app/customer/a/d;
.super Ljava/lang/Object;
.source "ProfilePresenter_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/customer/a/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/profile/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/profile/a;",
            ">;)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a/d;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/customer/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/customer/profile/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/customer/a/d;"
        }
    .end annotation

    .line 21
    new-instance v0, Lcom/swedbank/mobile/app/customer/a/d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/customer/a/d;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/customer/a/c;
    .locals 2

    .line 17
    new-instance v0, Lcom/swedbank/mobile/app/customer/a/c;

    iget-object v1, p0, Lcom/swedbank/mobile/app/customer/a/d;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/customer/profile/a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/customer/a/c;-><init>(Lcom/swedbank/mobile/business/customer/profile/a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/customer/a/d;->a()Lcom/swedbank/mobile/app/customer/a/c;

    move-result-object v0

    return-object v0
.end method
