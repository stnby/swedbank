.class public final Lcom/swedbank/mobile/app/customer/a/e;
.super Lcom/swedbank/mobile/architect/a/h;
.source "ProfileRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/customer/profile/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/customer/selection/a;

.field private final f:Lcom/swedbank/mobile/business/customer/selection/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/customer/selection/a;Lcom/swedbank/mobile/business/customer/selection/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/customer/selection/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/customer/selection/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_profile"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_profile"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_profile"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/customer/selection/a;",
            "Lcom/swedbank/mobile/business/customer/selection/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "customerSelectionBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerSelectionListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p3, p5, p4}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a/e;->e:Lcom/swedbank/mobile/app/customer/selection/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/customer/a/e;->f:Lcom/swedbank/mobile/business/customer/selection/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/customer/a/e;)Lcom/swedbank/mobile/app/customer/selection/a;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/a/e;->e:Lcom/swedbank/mobile/app/customer/selection/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/customer/a/e;)Lcom/swedbank/mobile/business/customer/selection/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/a/e;->f:Lcom/swedbank/mobile/business/customer/selection/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 25
    new-instance v0, Lcom/swedbank/mobile/app/customer/a/e$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/customer/a/e$b;-><init>(Lcom/swedbank/mobile/app/customer/a/e;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/a/e;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 37
    sget-object v0, Lcom/swedbank/mobile/app/customer/a/e$a;->a:Lcom/swedbank/mobile/app/customer/a/e$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
