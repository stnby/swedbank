.class public final Lcom/swedbank/mobile/app/customer/a/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ProfileViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/customer/a/g;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/customer/l;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/customer/l;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/customer/l;
        .annotation runtime Ljavax/inject/Named;
            value = "selected_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectedCustomer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a/h;->b:Lcom/swedbank/mobile/business/customer/l;

    .line 25
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/customer/a/h;->a:Lcom/b/c/c;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/a/h;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/customer/a/j;)Ljava/lang/Void;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/customer/a/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No view state to render"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/app/customer/a/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/customer/a/h;->a(Lcom/swedbank/mobile/app/customer/a/j;)Ljava/lang/Void;

    return-void
.end method

.method protected e()V
    .locals 10

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/customer/a/h;->o()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Landroid/view/ViewGroup;

    .line 53
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    .line 54
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "getChildAt(index)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f0a02dd

    .line 29
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroidx/appcompat/widget/Toolbar;

    if-eqz v4, :cond_0

    .line 30
    invoke-virtual {v4}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v5

    const v6, 0x7f0a018d

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-nez v5, :cond_0

    .line 31
    invoke-virtual {v4}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v5

    const/16 v7, 0xa

    const v8, 0x7f11015e

    invoke-interface {v5, v2, v6, v7, v8}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v5

    .line 33
    new-instance v6, Lcom/swedbank/mobile/core/ui/widget/b;

    .line 34
    invoke-virtual {v4}, Landroidx/appcompat/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "context"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v8, p0, Lcom/swedbank/mobile/app/customer/a/h;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/customer/l;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/swedbank/mobile/core/ui/widget/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 36
    iget-object v9, p0, Lcom/swedbank/mobile/app/customer/a/h;->b:Lcom/swedbank/mobile/business/customer/l;

    invoke-virtual {v9}, Lcom/swedbank/mobile/business/customer/l;->a()Z

    move-result v9

    .line 33
    invoke-direct {v6, v7, v8, v9}, Lcom/swedbank/mobile/core/ui/widget/b;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    check-cast v6, Landroid/graphics/drawable/Drawable;

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    const/4 v6, 0x2

    .line 37
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const-string v6, "menu.add(NONE, R.id.menu\u2026N_ALWAYS)\n              }"

    .line 32
    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 39
    invoke-static {v5, v7, v6, v7}, Lcom/b/b/d/b;->a(Landroid/view/MenuItem;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v5

    .line 40
    iget-object v6, p0, Lcom/swedbank/mobile/app/customer/a/h;->a:Lcom/b/c/c;

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v5

    const-string v6, "menu.add(NONE, R.id.menu\u2026(customerSelectionStream)"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    check-cast v4, Landroid/view/View;

    invoke-static {v5, v4}, Lcom/swedbank/mobile/core/ui/v;->a(Lio/reactivex/b/c;Landroid/view/View;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void

    .line 28
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
