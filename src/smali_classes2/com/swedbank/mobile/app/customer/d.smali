.class public final Lcom/swedbank/mobile/app/customer/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "CustomerRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/customer/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/navigation/b/a;

.field private final f:Lcom/swedbank/mobile/app/s/a;

.field private final g:Lcom/swedbank/mobile/business/customer/e;

.field private final h:Lcom/swedbank/mobile/business/customer/g;

.field private final i:Lcom/swedbank/mobile/business/general/retry/c;

.field private final j:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/navigation/b/a;Lcom/swedbank/mobile/app/s/a;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/customer/g;Lcom/swedbank/mobile/business/general/retry/c;Lkotlin/e/a/b;Lkotlin/e/a/b;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 14
    .param p1    # Lcom/swedbank/mobile/app/navigation/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/s/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/customer/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/customer/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/general/retry/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "customer_child_node_build_callback"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lkotlin/e/a/b;
        .annotation runtime Ljavax/inject/Named;
            value = "customer_child_node_attached_callback"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_customer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/navigation/b/a;",
            "Lcom/swedbank/mobile/app/s/a;",
            "Lcom/swedbank/mobile/business/customer/e;",
            "Lcom/swedbank/mobile/business/customer/g;",
            "Lcom/swedbank/mobile/business/general/retry/c;",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/a/i/d;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v6, p0

    move-object v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    const-string v0, "loadingNavigationBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retryBuilder"

    invoke-static {v8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerRepository"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getMobileAgreementId"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retryListener"

    invoke-static {v11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeBuildCallback"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nodeAttachedCallback"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    move-object/from16 v1, p8

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    move-object/from16 v2, p9

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    .line 35
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object v7, v6, Lcom/swedbank/mobile/app/customer/d;->e:Lcom/swedbank/mobile/app/navigation/b/a;

    iput-object v8, v6, Lcom/swedbank/mobile/app/customer/d;->f:Lcom/swedbank/mobile/app/s/a;

    iput-object v9, v6, Lcom/swedbank/mobile/app/customer/d;->g:Lcom/swedbank/mobile/business/customer/e;

    iput-object v10, v6, Lcom/swedbank/mobile/app/customer/d;->h:Lcom/swedbank/mobile/business/customer/g;

    iput-object v11, v6, Lcom/swedbank/mobile/app/customer/d;->i:Lcom/swedbank/mobile/business/general/retry/c;

    iput-object v12, v6, Lcom/swedbank/mobile/app/customer/d;->j:Lkotlin/e/a/b;

    iput-object v13, v6, Lcom/swedbank/mobile/app/customer/d;->k:Lkotlin/e/a/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/customer/d;)Lcom/swedbank/mobile/app/navigation/b/a;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/d;->e:Lcom/swedbank/mobile/app/navigation/b/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/customer/d;)Lcom/swedbank/mobile/app/s/a;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/d;->f:Lcom/swedbank/mobile/app/s/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/customer/d;)Lcom/swedbank/mobile/business/general/retry/c;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/customer/d;->i:Lcom/swedbank/mobile/business/general/retry/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 36
    new-instance v0, Lcom/swedbank/mobile/app/customer/d$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/customer/d$c;-><init>(Lcom/swedbank/mobile/app/customer/d;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/customer/a;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/customer/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "customer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/swedbank/mobile/a/i/d;

    .line 58
    invoke-static {p1}, Lcom/swedbank/mobile/business/customer/n;->a(Lcom/swedbank/mobile/business/customer/a;)Lcom/swedbank/mobile/business/customer/l;

    move-result-object p1

    .line 59
    iget-object v1, p0, Lcom/swedbank/mobile/app/customer/d;->g:Lcom/swedbank/mobile/business/customer/e;

    .line 60
    iget-object v2, p0, Lcom/swedbank/mobile/app/customer/d;->h:Lcom/swedbank/mobile/business/customer/g;

    .line 57
    invoke-direct {v0, p1, v1, v2}, Lcom/swedbank/mobile/a/i/d;-><init>(Lcom/swedbank/mobile/business/customer/l;Lcom/swedbank/mobile/business/customer/e;Lcom/swedbank/mobile/business/customer/g;)V

    .line 61
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/customer/d;->r()V

    .line 62
    iget-object p1, p0, Lcom/swedbank/mobile/app/customer/d;->j:Lkotlin/e/a/b;

    invoke-interface {p1, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    .line 72
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/app/customer/d;->k:Lkotlin/e/a/b;

    invoke-interface {v0, p1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/architect/a/h;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/swedbank/mobile/app/customer/d$b;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/customer/d$b;-><init>(Lkotlin/e/a/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 44
    sget-object v0, Lcom/swedbank/mobile/app/customer/d$a;->a:Lcom/swedbank/mobile/app/customer/d$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/d;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 46
    new-instance v0, Lcom/swedbank/mobile/app/customer/d$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/customer/d$d;-><init>(Lcom/swedbank/mobile/app/customer/d;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/customer/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method
