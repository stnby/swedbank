.class final Lcom/swedbank/mobile/app/n/d$a$1;
.super Lkotlin/e/b/k;
.source "NotifyUserRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/n/d$a;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/n/d$a;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/n/d$a;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/n/d$a$1;->a:Lcom/swedbank/mobile/app/n/d$a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/n/d$a$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    check-cast p1, Ljava/lang/Iterable;

    .line 57
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/architect/a/h;

    .line 34
    instance-of v2, v2, Lcom/swedbank/mobile/business/general/confirmation/e;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 59
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-eqz v0, :cond_2

    .line 35
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/general/confirmation/d;

    :cond_2
    if-eqz v1, :cond_4

    .line 37
    invoke-interface {v1}, Lcom/swedbank/mobile/business/general/confirmation/d;->i()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/app/n/d$a$1;->a:Lcom/swedbank/mobile/app/n/d$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/n/d$a;->b:Lcom/swedbank/mobile/business/notify/c;

    check-cast v0, Lcom/swedbank/mobile/app/n/c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/n/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_3

    goto :goto_1

    .line 47
    :cond_3
    iget-object p1, p0, Lcom/swedbank/mobile/app/n/d$a$1;->b:Lio/reactivex/k;

    invoke-interface {p1, v1}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_2

    .line 45
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/swedbank/mobile/app/n/d$a$1;->a:Lcom/swedbank/mobile/app/n/d$a;

    iget-object p1, p1, Lcom/swedbank/mobile/app/n/d$a;->a:Lcom/swedbank/mobile/app/n/d;

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/n/d$a$1;->a:Lcom/swedbank/mobile/app/n/d$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/n/d$a;->a:Lcom/swedbank/mobile/app/n/d;

    invoke-static {v0}, Lcom/swedbank/mobile/app/n/d;->a(Lcom/swedbank/mobile/app/n/d;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/app/n/d$a$1;->a:Lcom/swedbank/mobile/app/n/d$a;

    iget-object v1, v1, Lcom/swedbank/mobile/app/n/d$a;->b:Lcom/swedbank/mobile/business/notify/c;

    check-cast v1, Lcom/swedbank/mobile/app/n/c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/n/c;->b()Lkotlin/e/a/b;

    move-result-object v1

    invoke-interface {v1, v0}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/f/a;

    .line 39
    new-instance v1, Lcom/swedbank/mobile/app/n/d$a$1$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/n/d$a$1$a;-><init>(Lcom/swedbank/mobile/app/n/d$a$1;)V

    check-cast v1, Lcom/swedbank/mobile/business/general/confirmation/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/swedbank/mobile/app/n/d$a$1;->a:Lcom/swedbank/mobile/app/n/d$a;

    iget-object v1, v1, Lcom/swedbank/mobile/app/n/d$a;->b:Lcom/swedbank/mobile/business/notify/c;

    check-cast v1, Lcom/swedbank/mobile/app/n/c;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/n/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/app/n/d$a$1;->b:Lio/reactivex/k;

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 60
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :goto_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/n/d$a$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
