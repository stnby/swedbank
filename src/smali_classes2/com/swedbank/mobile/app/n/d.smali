.class public final Lcom/swedbank/mobile/app/n/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "NotifyUserRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/notify/g;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/f/a;

.field private final f:Lcom/swedbank/mobile/business/notify/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/business/notify/a;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/notify/a;
        .annotation runtime Ljavax/inject/Named;
            value = "for_notify_user"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_notify_user"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/business/notify/a;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "confirmationDialogBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "childListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    .line 27
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/n/d;->e:Lcom/swedbank/mobile/app/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/n/d;->f:Lcom/swedbank/mobile/business/notify/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/n/d;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/n/d;->e:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/n/d;Lkotlin/e/a/b;)V
    .locals 0

    .line 22
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/n/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/n/d;)Lcom/swedbank/mobile/business/notify/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/n/d;->f:Lcom/swedbank/mobile/business/notify/a;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/notify/c;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/notify/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/notify/c;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p1, Lcom/swedbank/mobile/app/n/c;

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Lcom/swedbank/mobile/app/n/d$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/n/d$a;-><init>(Lcom/swedbank/mobile/app/n/d;Lcom/swedbank/mobile/business/notify/c;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.create { emitter -\u2026}\n        }\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 30
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Notify user flow input must be of type "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v0, Lcom/swedbank/mobile/app/n/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tag"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p1}, Lcom/swedbank/mobile/app/f/g;->a(Ljava/lang/String;)Lkotlin/e/a/b;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/n/d;->c(Lkotlin/e/a/b;)V

    return-void
.end method
