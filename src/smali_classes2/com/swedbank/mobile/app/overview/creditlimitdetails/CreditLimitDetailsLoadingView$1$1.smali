.class public final Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "CreditLimitDetailsLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;

.field final synthetic b:Landroid/graphics/Paint;

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:F

.field final synthetic f:F

.field final synthetic g:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;Landroid/graphics/Paint;FFFFILandroid/view/View;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Paint;",
            "FFFFI",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    move-object v8, p0

    move-object v0, p1

    .line 38
    iput-object v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;

    move-object v0, p2

    iput-object v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->b:Landroid/graphics/Paint;

    move v0, p3

    iput v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->c:F

    move v0, p4

    iput v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->d:F

    move v0, p5

    iput v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->e:F

    move v0, p6

    iput v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->f:F

    move/from16 v0, p7

    iput v0, v8, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->g:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, p0

    move-object/from16 v1, p8

    move/from16 v2, p9

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;)V
    .locals 2
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    int-to-float p1, p1

    int-to-float p2, p2

    const/4 v1, 0x0

    .line 41
    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 42
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->b:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, p1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method

.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPaint"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    int-to-float p2, p2

    .line 48
    iget v1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->c:F

    sub-float v1, p2, v1

    const/4 v2, 0x2

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float p1, p1

    .line 49
    iget v3, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->d:F

    sub-float/2addr p1, v3

    .line 50
    iget v3, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->e:F

    sub-float/2addr p2, v3

    div-float/2addr p2, v2

    .line 53
    iget v2, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->f:F

    const/4 v3, 0x0

    add-float/2addr v2, v3

    iget v4, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->c:F

    add-float/2addr v4, v1

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 54
    invoke-virtual {p3, v0, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 57
    iget v1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->d:F

    add-float/2addr v1, p1

    iget v2, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;->e:F

    add-float/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 58
    invoke-virtual {p3, v0, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method
