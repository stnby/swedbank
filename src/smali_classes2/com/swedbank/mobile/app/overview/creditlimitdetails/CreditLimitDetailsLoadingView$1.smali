.class final Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;
.super Lkotlin/e/b/k;
.source "CreditLimitDetailsLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;->b:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;
    .locals 11
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "metrics"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;->b:Landroid/content/Context;

    sget v0, Lcom/swedbank/mobile/app/overview/i$a;->background_color:I

    .line 79
    invoke-static {p1, v0}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v10

    .line 32
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    const/4 p1, -0x1

    invoke-virtual {v3, p1}, Landroid/graphics/Paint;->setColor(I)V

    const/16 p1, 0x24

    int-to-float p1, p1

    const/4 v0, 0x1

    .line 80
    invoke-static {v0, p1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    const/16 p1, 0x5a

    int-to-float p1, p1

    .line 81
    invoke-static {v0, p1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    const/16 v1, 0x12

    int-to-float v1, v1

    .line 82
    invoke-static {v0, v1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    .line 83
    invoke-static {v0, p1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 38
    new-instance p1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;

    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    move-object v9, p2

    check-cast v9, Landroid/view/View;

    move-object v1, p1

    move-object v2, p0

    move v8, v10

    invoke-direct/range {v1 .. v10}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;Landroid/graphics/Paint;FFFFILandroid/view/View;I)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
