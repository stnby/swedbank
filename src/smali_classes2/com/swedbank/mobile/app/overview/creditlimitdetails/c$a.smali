.class final synthetic Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$a;
.super Lkotlin/e/b/i;
.source "CreditLimitDetailsPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;",
        "Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a;",
        "Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;

    .line 94
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;

    if-eqz v0, :cond_0

    .line 95
    check-cast p2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;->a()Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;->b()Ljava/math/BigDecimal;

    move-result-object v2

    .line 97
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;->c()Ljava/math/BigDecimal;

    move-result-object v3

    .line 98
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x30

    const/4 v8, 0x0

    move-object v0, p1

    .line 94
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/String;ZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    move-result-object p1

    goto :goto_0

    .line 100
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$d;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0xf

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/String;ZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    move-result-object p1

    goto :goto_0

    .line 104
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$c;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$c;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/String;ZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    move-result-object p1

    goto :goto_0

    .line 107
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$b;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 109
    check-cast p2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/16 v8, 0xf

    const/4 v9, 0x0

    move-object v1, p1

    .line 107
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/String;ZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    check-cast p2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$a;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsViewState;Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsViewState$PartialState;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsViewState;"

    return-object v0
.end method
