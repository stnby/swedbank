.class public final Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "CreditLimitDetailsViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/creditlimitdetails/h;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private final m:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0xb

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentViews"

    const-string v4, "getContentViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "totalUsedCreditLimitView"

    const-string v4, "getTotalUsedCreditLimitView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "totalUsedCreditLimitRepayBtn"

    const-string v4, "getTotalUsedCreditLimitRepayBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "totalUsedCreditLimitLoading"

    const-string v4, "getTotalUsedCreditLimitLoading()Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "interestAccruingAmountView"

    const-string v4, "getInterestAccruingAmountView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "interestAccruingAmountRepayBtn"

    const-string v4, "getInterestAccruingAmountRepayBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "interestAccruingAmountLoading"

    const-string v4, "getInterestAccruingAmountLoading()Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->m:Lio/reactivex/o;

    .line 31
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->b:Lkotlin/f/c;

    .line 32
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_content_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->c:Lkotlin/f/c;

    .line 33
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_used_credit_limit:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->d:Lkotlin/f/c;

    .line 34
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_repay_used_credit_limit_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->e:Lkotlin/f/c;

    .line 35
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_used_credit_limit_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->f:Lkotlin/f/c;

    .line 36
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_interest_accruing_amount:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->g:Lkotlin/f/c;

    .line 37
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_repay_interest_accruing_amount_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->h:Lkotlin/f/c;

    .line 38
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_interest_accruing_amount_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->i:Lkotlin/f/c;

    .line 39
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_retry_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->j:Lkotlin/f/c;

    .line 40
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->k:Lkotlin/f/c;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->credit_limit_details_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->l:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->i()Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/Button;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->h()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->p()Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->j()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/Button;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->k()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/view/View;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->f()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/view/View;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->s()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final h()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->q()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final i()Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    return-object v0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/content/Context;
    .locals 0

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final j()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final k()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final p()Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    return-object v0
.end method

.method private final q()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final r()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final s()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->l:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a:[Lkotlin/h/g;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->h()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 89
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    .line 46
    sget-object v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i$a;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/i$a;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->k()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 90
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    .line 47
    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i$b;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/i$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v2, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 45
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026TEREST_ACCRUING_AMOUNT })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->e()Z

    move-result v0

    .line 92
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/16 v4, 0x8

    .line 93
    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->b(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v4, 0x4

    if-eqz v0, :cond_1

    const/4 v5, 0x4

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    .line 96
    :goto_1
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 98
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->c(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v0, :cond_2

    const/4 v5, 0x4

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    .line 99
    :goto_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->d(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/CreditLimitDetailsLoadingView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v0, :cond_3

    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    const/16 v5, 0x8

    .line 102
    :goto_3
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 104
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->e(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v0, :cond_4

    const/4 v5, 0x4

    goto :goto_4

    :cond_4
    const/4 v5, 0x0

    .line 105
    :goto_4
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 107
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->f(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v0, :cond_5

    goto :goto_5

    :cond_5
    const/4 v4, 0x0

    .line 108
    :goto_5
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 114
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->e()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-nez v0, :cond_8

    .line 115
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->b(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->b()Ljava/math/BigDecimal;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-static {v5, v3, v3, v7, v6}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->c(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->b()Ljava/math/BigDecimal;

    move-result-object v4

    sget-object v8, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v4, v8}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v4

    if-lez v4, :cond_6

    const/4 v4, 0x1

    goto :goto_6

    :cond_6
    const/4 v4, 0x0

    :goto_6
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 117
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->e(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->c()Ljava/math/BigDecimal;

    move-result-object v8

    invoke-static {v8, v3, v3, v7, v6}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->f(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->c()Ljava/math/BigDecimal;

    move-result-object v4

    sget-object v5, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v4

    if-lez v4, :cond_7

    const/4 v4, 0x1

    goto :goto_7

    :cond_7
    const/4 v4, 0x0

    :goto_7
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 55
    :cond_8
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    if-eqz p1, :cond_9

    const/4 v0, 0x1

    goto :goto_8

    :cond_9
    const/4 v0, 0x0

    .line 124
    :goto_8
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->g(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/view/View;

    move-result-object v4

    xor-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_a

    const/4 v5, 0x0

    goto :goto_9

    :cond_a
    const/16 v5, 0x8

    .line 125
    :goto_9
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 127
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->h(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/view/View;

    move-result-object v4

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    .line 128
    :cond_b
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_f

    .line 131
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->i(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/widget/TextView;

    move-result-object v0

    .line 132
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->j(Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;)Landroid/content/Context;

    move-result-object v2

    .line 133
    sget v3, Lcom/swedbank/mobile/app/overview/i$f;->credit_limit_repay_general_loading_error:I

    .line 136
    instance-of v4, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v4, :cond_d

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 138
    move-object v4, p1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v1, v4

    if-eqz v1, :cond_c

    move-object v4, p1

    check-cast v4, Ljava/lang/Iterable;

    const-string p1, "\n"

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_a

    .line 139
    :cond_c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_a

    .line 141
    :cond_d
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_e

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 142
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p1

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_a
    const-string v1, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 143
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 144
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    .line 142
    :cond_e
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_f
    :goto_b
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->r()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 91
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->m:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/i;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
