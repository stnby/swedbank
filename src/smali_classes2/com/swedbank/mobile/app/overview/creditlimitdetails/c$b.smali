.class final Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;
.super Ljava/lang/Object;
.source "CreditLimitDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/a/a;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->e()Lcom/swedbank/mobile/business/a/d;

    move-result-object v0

    .line 27
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->g()Ljava/lang/String;

    move-result-object p1

    .line 93
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/d;->d()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/d;->e()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    const-string v2, "this.subtract(other)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-ltz v2, :cond_0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v2, "BigDecimal.ZERO"

    :goto_0
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v1

    const-string v2, "usedCredit.abs()"

    goto :goto_0

    .line 95
    :goto_1
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/d;->f()Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-gez v2, :cond_1

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/d;->f()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v2

    const-string v3, "deposit.abs()"

    :goto_2
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_1
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v3, "BigDecimal.ZERO"

    goto :goto_2

    .line 30
    :goto_3
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/a/d;->c()Ljava/lang/String;

    move-result-object v0

    .line 26
    new-instance v3, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;

    invoke-direct {v3, p1, v1, v2, v0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/String;)V

    return-object v3
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/a/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;->a(Lcom/swedbank/mobile/business/a/a;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/k$a$a;

    move-result-object p1

    return-object p1
.end method
