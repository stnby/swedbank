.class final Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;
.super Ljava/lang/Object;
.source "CreditLimitDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;)V
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;

    move-result-object v0

    const-string v1, "creditRepaymentType"

    .line 51
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->b(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)Landroid/app/Application;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/overview/i$f;->credit_limit_repay_form_description:I

    invoke-virtual {v1, v2}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026t_repay_form_description)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;->a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;->a(Lcom/swedbank/mobile/business/overview/creditlimitdetails/f;)V

    return-void
.end method
