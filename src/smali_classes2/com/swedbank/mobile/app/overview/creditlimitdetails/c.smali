.class public final Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "CreditLimitDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/overview/creditlimitdetails/h;",
        "Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)Landroid/app/Application;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 14

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->b:Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;

    .line 23
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/creditlimitdetails/a;->a()Lio/reactivex/o;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 33
    sget-object v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$c;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 34
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 35
    new-instance v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$d;-><init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 47
    sget-object v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$g;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$g;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    const-wide/16 v3, 0x1

    .line 48
    invoke-virtual {v2, v3, v4}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v2

    .line 49
    new-instance v5, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$h;-><init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v2, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 54
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 55
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 57
    sget-object v5, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$e;->a:Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$e;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 58
    new-instance v6, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$f;

    invoke-direct {v6, p0}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$f;-><init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)V

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 59
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 60
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 63
    check-cast v0, Lio/reactivex/s;

    .line 64
    check-cast v1, Lio/reactivex/s;

    .line 65
    check-cast v2, Lio/reactivex/s;

    .line 66
    check-cast v5, Lio/reactivex/s;

    .line 62
    invoke-static {v0, v1, v2, v5}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026\n        backClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v1, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3f

    const/4 v13, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v13}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/k;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/lang/String;ZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$a;

    move-object v5, p0

    check-cast v5, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;

    invoke-direct {v2, v5}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/c$a;-><init>(Lcom/swedbank/mobile/app/overview/creditlimitdetails/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 93
    new-instance v5, Lcom/swedbank/mobile/app/overview/creditlimitdetails/d;

    invoke-direct {v5, v2}, Lcom/swedbank/mobile/app/overview/creditlimitdetails/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v5, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v5}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 94
    invoke-virtual {v0, v3, v4}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
