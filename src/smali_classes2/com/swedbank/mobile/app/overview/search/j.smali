.class public final Lcom/swedbank/mobile/app/overview/search/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OverviewSearchViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/search/i;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Z

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "input"

    const-string v4, "getInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "inputLoading"

    const-string v4, "getInputLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "clearBtn"

    const-string v4, "getClearBtn()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "resultsView"

    const-string v4, "getResultsView()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/search/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Ljava/util/List;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_search_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProviders"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/j;->h:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/search/j;->i:Lio/reactivex/o;

    .line 33
    sget p2, Lcom/swedbank/mobile/app/overview/i$d;->toolbar:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/j;->b:Lkotlin/f/c;

    .line 34
    sget p2, Lcom/swedbank/mobile/app/overview/i$d;->overview_search_input:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/j;->c:Lkotlin/f/c;

    .line 35
    sget p2, Lcom/swedbank/mobile/app/overview/i$d;->overview_search_input_loading:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/j;->d:Lkotlin/f/c;

    .line 36
    sget p2, Lcom/swedbank/mobile/app/overview/i$d;->overview_search_clear_btn:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/j;->e:Lkotlin/f/c;

    .line 37
    sget p2, Lcom/swedbank/mobile/app/overview/i$d;->overview_search_results:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/j;->f:Lkotlin/f/c;

    const-string p2, "feature_overview_search_debounced"

    .line 39
    invoke-interface {p1, p2}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/overview/search/j;->g:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/search/j;)Landroid/widget/EditText;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->g()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/search/j;)Landroid/view/View;
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->i()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final g()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final h()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final i()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final j()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->i:Lio/reactivex/o;

    .line 55
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeW\u2026olbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/search/o;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/overview/search/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/search/o;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->g()Landroid/widget/EditText;

    move-result-object v1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Ljava/lang/CharSequence;)V

    .line 92
    :cond_0
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->j()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/search/o;->b()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 93
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->h()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/search/o;->a()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    const/4 p1, 0x4

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 130
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/app/overview/search/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/search/j;->a(Lcom/swedbank/mobile/app/overview/search/o;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 59
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/search/j;->g:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->g()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 62
    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j$d;->a:Lcom/swedbank/mobile/app/overview/search/j$d;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/overview/search/l;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/search/l;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/swedbank/mobile/app/overview/search/j$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/overview/search/j$e;-><init>(Lcom/swedbank/mobile/app/overview/search/j;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    .line 66
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/o;->b(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "input\n        .textChang\u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 69
    :cond_1
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->g()Landroid/widget/EditText;

    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/ap;->b(Landroid/widget/EditText;)Lio/reactivex/o;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/swedbank/mobile/app/overview/search/j$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/overview/search/j$f;-><init>(Lcom/swedbank/mobile/app/overview/search/j;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/swedbank/mobile/app/overview/search/j$g;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/overview/search/j$g;-><init>(Lcom/swedbank/mobile/app/overview/search/j;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 73
    sget-object v1, Lcom/swedbank/mobile/app/overview/search/j$h;->a:Lcom/swedbank/mobile/app/overview/search/j$h;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_2

    new-instance v2, Lcom/swedbank/mobile/app/overview/search/m;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/search/m;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_2
    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 76
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->g()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 77
    invoke-static {v1}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v1

    .line 79
    sget-object v2, Lcom/swedbank/mobile/app/overview/search/j$i;->a:Lcom/swedbank/mobile/app/overview/search/j$i;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_3

    new-instance v3, Lcom/swedbank/mobile/app/overview/search/l;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/search/l;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_3
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 80
    new-instance v2, Lcom/swedbank/mobile/app/overview/search/j$j;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/overview/search/j$j;-><init>(Lcom/swedbank/mobile/app/overview/search/j;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 83
    sget-object v2, Lcom/swedbank/mobile/app/overview/search/j$k;->a:Lcom/swedbank/mobile/app/overview/search/j$k;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_4

    new-instance v3, Lcom/swedbank/mobile/app/overview/search/m;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/search/m;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_4
    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 67
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026r(CharSequence::isEmpty))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 86
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->j()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->j()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->c()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()V
    .locals 8

    .line 42
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/search/j;->o()Landroid/view/View;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "rootView.context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    :goto_0
    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_1

    .line 100
    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 101
    check-cast v1, Landroid/app/Activity;

    goto :goto_1

    .line 103
    :cond_0
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context.baseContext"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    .line 106
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const-string v2, "window"

    .line 107
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const-string v3, "window.decorView"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v3, Lcom/swedbank/mobile/app/overview/search/j$b;

    invoke-direct {v3, v2, v0, v1}, Lcom/swedbank/mobile/app/overview/search/j$b;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/Window;)V

    check-cast v3, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 109
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 111
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 v5, 0x10

    if-eq v4, v5, :cond_2

    .line 112
    invoke-virtual {v1, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 115
    :cond_2
    new-instance v4, Lcom/swedbank/mobile/app/overview/search/j$c;

    invoke-direct {v4, v2, v3, v1}, Lcom/swedbank/mobile/app/overview/search/j$c;-><init>(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/Window;)V

    check-cast v4, Lio/reactivex/c/a;

    invoke-static {v4}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction {\u2026INPUT_ADJUST_NOTHING)\n  }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 97
    :cond_3
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.disposed()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    :goto_2
    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 118
    new-instance v1, Lcom/swedbank/mobile/app/overview/search/j$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/overview/search/j$a;-><init>(Landroid/view/View;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/app/overview/search/k;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/overview/search/k;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction(rootView::hideKeyboard)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->g()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 121
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "input_method"

    .line 125
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 123
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->j()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/j;->h:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Ljava/util/List;)V

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/search/j;->i()Landroid/view/View;

    move-result-object v0

    .line 127
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    move-object v2, v1

    check-cast v2, Lio/reactivex/o;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 50
    new-instance v0, Lcom/swedbank/mobile/app/overview/search/j$l;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/search/j$l;-><init>(Lcom/swedbank/mobile/app/overview/search/j;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 128
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void

    .line 125
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
