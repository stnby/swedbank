.class final synthetic Lcom/swedbank/mobile/app/overview/search/c$c;
.super Lkotlin/e/b/i;
.source "OverviewSearchPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/search/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/overview/search/o;",
        "Lcom/swedbank/mobile/app/overview/search/o$a;",
        "Lcom/swedbank/mobile/app/overview/search/o;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/search/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/search/o;Lcom/swedbank/mobile/app/overview/search/o$a;)Lcom/swedbank/mobile/app/overview/search/o;
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/overview/search/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/overview/search/o$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/c$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/overview/search/c;

    .line 97
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/search/o$a$a;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/swedbank/mobile/app/overview/search/o$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/search/o$a$a;->a()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/search/o;->a(Lcom/swedbank/mobile/app/overview/search/o;ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/search/o;

    move-result-object p1

    goto :goto_0

    .line 98
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/search/o$a$b;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    check-cast p2, Lcom/swedbank/mobile/app/overview/search/o$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/search/o$a$b;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/overview/search/o;->a(Lcom/swedbank/mobile/app/overview/search/o;ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/search/o;

    move-result-object p1

    goto :goto_0

    .line 99
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/search/o$a$c;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    new-instance v3, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 100
    check-cast p2, Lcom/swedbank/mobile/app/overview/search/o$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/search/o$a$c;->a()Ljava/util/List;

    move-result-object v0

    .line 101
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/search/o$a$c;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object v1

    .line 102
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/search/o$a$c;->c()Z

    move-result p2

    .line 99
    invoke-direct {v3, v0, v1, p2}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/overview/search/o;->a(Lcom/swedbank/mobile/app/overview/search/o;ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/search/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/overview/search/o;

    check-cast p2, Lcom/swedbank/mobile/app/overview/search/o$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/search/c$c;->a(Lcom/swedbank/mobile/app/overview/search/o;Lcom/swedbank/mobile/app/overview/search/o$a;)Lcom/swedbank/mobile/app/overview/search/o;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/overview/search/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/overview/search/OverviewSearchViewState;Lcom/swedbank/mobile/app/overview/search/OverviewSearchViewState$PartialState;)Lcom/swedbank/mobile/app/overview/search/OverviewSearchViewState;"

    return-object v0
.end method
