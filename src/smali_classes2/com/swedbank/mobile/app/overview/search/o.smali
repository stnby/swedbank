.class public final Lcom/swedbank/mobile/app/overview/search/o;
.super Ljava/lang/Object;
.source "OverviewSearchViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/search/o$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/swedbank/mobile/app/plugins/list/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/search/o;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "searchResults"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILkotlin/e/b/g;)V
    .locals 2

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    const/4 v1, 0x0

    if-eqz p5, :cond_1

    .line 10
    new-instance p2, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 11
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p5

    .line 10
    invoke-direct {p2, p5, v1, v0}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 14
    move-object p3, v1

    check-cast p3, Ljava/lang/String;

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/search/o;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/overview/search/o;ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/search/o;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/search/o;->a(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/search/o;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/search/o;
    .locals 1
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "searchResults"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/app/overview/search/o;

    invoke-direct {v0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/search/o;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    return v0
.end method

.method public final b()Lcom/swedbank/mobile/app/plugins/list/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/overview/search/o;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/overview/search/o;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    iget-object v3, p1, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OverviewSearchViewState(loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", searchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->b:Lcom/swedbank/mobile/app/plugins/list/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", overridingInput="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/search/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
