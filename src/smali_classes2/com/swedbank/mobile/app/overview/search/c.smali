.class public final Lcom/swedbank/mobile/app/overview/search/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "OverviewSearchPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/overview/search/i;",
        "Lcom/swedbank/mobile/app/overview/search/o;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/overview/search/a;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/swedbank/mobile/business/overview/search/a;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_search_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/search/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;",
            "Lcom/swedbank/mobile/business/overview/search/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataMappers"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/search/c;->a:Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/search/c;->b:Lcom/swedbank/mobile/business/overview/search/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/search/c;)Lcom/swedbank/mobile/business/overview/search/a;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/search/c;->b:Lcom/swedbank/mobile/business/overview/search/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/c;->b:Lcom/swedbank/mobile/business/overview/search/a;

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/search/a;->e()Lio/reactivex/o;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/overview/search/c$f;->a:Lcom/swedbank/mobile/app/overview/search/c$f;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/overview/search/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 29
    new-instance v2, Lcom/swedbank/mobile/app/overview/search/c$g;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/overview/search/c$g;-><init>(Lcom/swedbank/mobile/app/overview/search/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/swedbank/mobile/app/overview/search/c$h;->a:Lcom/swedbank/mobile/app/overview/search/c$h;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/overview/search/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 36
    move-object v3, v0

    check-cast v3, Lio/reactivex/s;

    .line 34
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    .line 38
    new-instance v3, Lcom/swedbank/mobile/app/overview/search/c$i;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/overview/search/c$i;-><init>(Lcom/swedbank/mobile/app/overview/search/c;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 45
    sget-object v3, Lcom/swedbank/mobile/app/overview/search/c$j;->a:Lcom/swedbank/mobile/app/overview/search/c$j;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/overview/search/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 46
    new-instance v4, Lcom/swedbank/mobile/app/overview/search/c$k;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/overview/search/c$k;-><init>(Lcom/swedbank/mobile/app/overview/search/c;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->n(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v3

    .line 47
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 49
    iget-object v4, p0, Lcom/swedbank/mobile/app/overview/search/c;->b:Lcom/swedbank/mobile/business/overview/search/a;

    .line 50
    invoke-interface {v4}, Lcom/swedbank/mobile/business/overview/search/a;->c()Lio/reactivex/o;

    move-result-object v4

    .line 51
    iget-object v5, p0, Lcom/swedbank/mobile/app/overview/search/c;->a:Ljava/util/Map;

    .line 96
    new-instance v6, Lcom/swedbank/mobile/app/overview/search/c$a;

    invoke-direct {v6, v5}, Lcom/swedbank/mobile/app/overview/search/c$a;-><init>(Ljava/util/Map;)V

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v4, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    .line 97
    new-instance v5, Lcom/swedbank/mobile/app/overview/search/c$b;

    invoke-direct {v5}, Lcom/swedbank/mobile/app/overview/search/c$b;-><init>()V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v5, "map { it.toMappedData(da\u2026t, hasMoreToLoad) }\n    }"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget-object v5, Lcom/swedbank/mobile/app/overview/search/c$l;->a:Lcom/swedbank/mobile/app/overview/search/c$l;

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v0, v5}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 66
    sget-object v5, Lcom/swedbank/mobile/app/overview/search/c$d;->a:Lcom/swedbank/mobile/app/overview/search/c$d;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/overview/search/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 67
    new-instance v6, Lcom/swedbank/mobile/app/overview/search/c$e;

    iget-object v7, p0, Lcom/swedbank/mobile/app/overview/search/c;->b:Lcom/swedbank/mobile/business/overview/search/a;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/overview/search/c$e;-><init>(Lcom/swedbank/mobile/business/overview/search/a;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/overview/search/e;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/overview/search/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 68
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 69
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    const/4 v6, 0x6

    .line 71
    new-array v6, v6, [Lio/reactivex/s;

    .line 72
    check-cast v1, Lio/reactivex/s;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    .line 73
    check-cast v2, Lio/reactivex/s;

    const/4 v1, 0x1

    aput-object v2, v6, v1

    .line 74
    check-cast v3, Lio/reactivex/s;

    const/4 v1, 0x2

    aput-object v3, v6, v1

    .line 75
    check-cast v4, Lio/reactivex/s;

    const/4 v1, 0x3

    aput-object v4, v6, v1

    .line 76
    check-cast v0, Lio/reactivex/s;

    const/4 v1, 0x4

    aput-object v0, v6, v1

    .line 77
    check-cast v5, Lio/reactivex/s;

    const/4 v0, 0x5

    aput-object v5, v6, v0

    .line 71
    invoke-static {v6}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026m,\n        actionsStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v1, Lcom/swedbank/mobile/app/overview/search/o;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/app/overview/search/o;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;Ljava/lang/String;ILkotlin/e/b/g;)V

    .line 80
    new-instance v2, Lcom/swedbank/mobile/app/overview/search/c$c;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/overview/search/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/overview/search/c$c;-><init>(Lcom/swedbank/mobile/app/overview/search/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 99
    new-instance v3, Lcom/swedbank/mobile/app/overview/search/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/search/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 100
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
