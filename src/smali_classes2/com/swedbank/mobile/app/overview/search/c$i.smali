.class final Lcom/swedbank/mobile/app/overview/search/c$i;
.super Ljava/lang/Object;
.source "OverviewSearchPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/search/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/search/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/search/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/search/c$i;->a:Lcom/swedbank/mobile/app/overview/search/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/o;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/search/o$a$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/search/c$i;->a:Lcom/swedbank/mobile/app/overview/search/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/search/c;->a(Lcom/swedbank/mobile/app/overview/search/c;)Lcom/swedbank/mobile/business/overview/search/a;

    move-result-object v0

    .line 40
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/overview/search/a;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object p1

    .line 41
    new-instance v0, Lcom/swedbank/mobile/app/overview/search/o$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/overview/search/o$a$a;-><init>(Z)V

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 42
    new-instance v0, Lcom/swedbank/mobile/app/overview/search/o$a$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/overview/search/o$a$a;-><init>(Z)V

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/search/c$i;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
