.class public final Lcom/swedbank/mobile/app/overview/g;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OverviewRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/o;


# instance fields
.field private final e:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Lcom/swedbank/mobile/business/overview/k;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/app/Application;

.field private final g:Lcom/swedbank/mobile/app/overview/detailed/g;

.field private final h:Lcom/swedbank/mobile/app/overview/a/a;

.field private final i:Lcom/swedbank/mobile/app/overview/b/a;

.field private final j:Lcom/swedbank/mobile/app/overview/f/a;

.field private final k:Lcom/swedbank/mobile/business/overview/retry/c;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/app/overview/detailed/g;Lcom/swedbank/mobile/app/overview/a/a;Lcom/swedbank/mobile/app/overview/b/a;Lcom/swedbank/mobile/app/overview/f/a;Lcom/swedbank/mobile/business/overview/retry/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/overview/detailed/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/overview/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/overview/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/overview/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/overview/retry/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/app/overview/detailed/g;",
            "Lcom/swedbank/mobile/app/overview/a/a;",
            "Lcom/swedbank/mobile/app/overview/b/a;",
            "Lcom/swedbank/mobile/app/overview/f/a;",
            "Lcom/swedbank/mobile/business/overview/retry/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "app"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detailedBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combinedBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loadingBuilder"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retryBuilder"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retryListener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p7

    move-object v3, p8

    .line 41
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/g;->f:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/g;->g:Lcom/swedbank/mobile/app/overview/detailed/g;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/g;->h:Lcom/swedbank/mobile/app/overview/a/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/g;->i:Lcom/swedbank/mobile/app/overview/b/a;

    iput-object p5, p0, Lcom/swedbank/mobile/app/overview/g;->j:Lcom/swedbank/mobile/app/overview/f/a;

    iput-object p6, p0, Lcom/swedbank/mobile/app/overview/g;->k:Lcom/swedbank/mobile/business/overview/retry/c;

    .line 42
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<OverviewNode>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/g;->e:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/app/overview/detailed/g;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->g:Lcom/swedbank/mobile/app/overview/detailed/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/g;)Lcom/b/c/b;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->e:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/app/overview/a/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->h:Lcom/swedbank/mobile/app/overview/a/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/app/overview/b/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->i:Lcom/swedbank/mobile/app/overview/b/a;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/app/overview/f/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->j:Lcom/swedbank/mobile/app/overview/f/a;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/business/overview/retry/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->k:Lcom/swedbank/mobile/business/overview/retry/c;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/overview/g;)Landroid/app/Application;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/g;->f:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 59
    new-instance v0, Lcom/swedbank/mobile/app/overview/g$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/g$a;-><init>(Lcom/swedbank/mobile/app/overview/g;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v0, Lcom/swedbank/mobile/app/overview/g$e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/overview/g$e;-><init>(Lcom/swedbank/mobile/app/overview/g;Lcom/swedbank/mobile/business/util/e;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "selectedAccountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iban"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/swedbank/mobile/app/overview/g$b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/g$b;-><init>(Lcom/swedbank/mobile/app/overview/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 68
    new-instance v0, Lcom/swedbank/mobile/app/overview/g$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/g$d;-><init>(Lcom/swedbank/mobile/app/overview/g;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 76
    new-instance v0, Lcom/swedbank/mobile/app/overview/g$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/g$c;-><init>(Lcom/swedbank/mobile/app/overview/g;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/overview/k;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/g;->e:Lcom/b/c/b;

    invoke-virtual {v0}, Lcom/b/c/b;->i()Lio/reactivex/j;

    move-result-object v0

    const-string v1, "overviewNodeStream.firstElement()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
