.class public final Lcom/swedbank/mobile/app/overview/d/a/f;
.super Ljava/lang/Object;
.source "OverviewAccountsDataMapper.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/a/a;",
        ">;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/a/f;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/a/f;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/a/f;->a:Lcom/swedbank/mobile/app/overview/d/a/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Lcom/swedbank/mobile/app/plugins/list/g;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/plugins/list/g;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unmappedData"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    check-cast p1, Ljava/lang/Iterable;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 14
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 15
    check-cast v1, Lcom/swedbank/mobile/business/a/a;

    .line 10
    invoke-static {v1}, Lcom/swedbank/mobile/app/overview/d/a/b;->a(Lcom/swedbank/mobile/business/a/a;)Lcom/swedbank/mobile/app/overview/d/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 16
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 11
    new-instance p1, Lcom/swedbank/mobile/app/plugins/list/g;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;ZILkotlin/e/b/g;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/d/a/f;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/plugins/list/g;

    move-result-object p1

    return-object p1
.end method
