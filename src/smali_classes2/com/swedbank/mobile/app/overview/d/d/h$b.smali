.class public final Lcom/swedbank/mobile/app/overview/d/d/h$b;
.super Lcom/swedbank/mobile/app/overview/d/d/h;
.source "OverviewStatementItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/d/d/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/d/d/h$b$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/h$b$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/d/h$b$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 64
    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/app/overview/d/d/h;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->b:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ZILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const-string p1, "headerItem"

    .line 62
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/d/d/h$b;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 63
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/swedbank/mobile/app/overview/d/d/h$b;->b:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
