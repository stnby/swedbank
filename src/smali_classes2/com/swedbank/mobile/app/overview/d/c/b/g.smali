.class public final Lcom/swedbank/mobile/app/overview/d/c/b/g;
.super Ljava/lang/Object;
.source "SearchTransactionItem.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/d/c/b/f;
    .locals 15
    .param p0    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toSearchTransactionItem"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/c/b/f;

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->d()Ljava/lang/String;

    move-result-object v3

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->e()Ljava/util/Date;

    move-result-object v4

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->f()Ljava/math/BigDecimal;

    move-result-object v5

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->g()Ljava/lang/String;

    move-result-object v6

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->h()Ljava/lang/String;

    move-result-object v7

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->j()Ljava/lang/String;

    move-result-object v8

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->k()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v9

    .line 37
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->l()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v10

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->a()Z

    move-result v11

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->m()I

    move-result v12

    const/4 v2, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object v1, v0

    .line 29
    invoke-direct/range {v1 .. v14}, Lcom/swedbank/mobile/app/overview/d/c/b/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$Direction;ZIILkotlin/e/b/g;)V

    return-object v0
.end method
