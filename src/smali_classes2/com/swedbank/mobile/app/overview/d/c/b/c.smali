.class public final Lcom/swedbank/mobile/app/overview/d/c/b/c;
.super Ljava/lang/Object;
.source "OverviewRemoteSearchDataMapper.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/c/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/c/b/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/c/b/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/c;->a:Lcom/swedbank/mobile/app/overview/d/c/b/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;)Lcom/swedbank/mobile/app/plugins/list/g;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unmappedData"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 15
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->a()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 32
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 33
    move-object v4, v0

    check-cast v4, Ljava/util/Collection;

    check-cast v3, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 15
    invoke-static {v3}, Lcom/swedbank/mobile/app/overview/d/c/b/g;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/d/c/b/f;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 34
    :cond_0
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    .line 16
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 17
    new-instance v2, Lcom/swedbank/mobile/app/overview/d/d/h$c;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v1, v3}, Lcom/swedbank/mobile/app/overview/d/d/h$c;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    :cond_1
    new-instance v1, Lcom/swedbank/mobile/app/plugins/list/g;

    .line 21
    check-cast v0, Ljava/util/List;

    .line 22
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;->b()Z

    move-result p1

    .line 20
    invoke-direct {v1, v0, p1}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;Z)V

    goto :goto_1

    .line 25
    :cond_2
    new-instance v1, Lcom/swedbank/mobile/app/plugins/list/g;

    .line 26
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    .line 25
    invoke-direct {v1, p1, v0}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;Z)V

    :goto_1
    return-object v1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/d/c/b/c;->a(Lcom/swedbank/mobile/business/overview/plugins/search/remote/c;)Lcom/swedbank/mobile/app/plugins/list/g;

    move-result-object p1

    return-object p1
.end method
