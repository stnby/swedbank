.class public abstract Lcom/swedbank/mobile/app/overview/d/d/h$e;
.super Lcom/swedbank/mobile/app/overview/d/d/h;
.source "OverviewStatementItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/d/d/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/d/d/h$e$c;,
        Lcom/swedbank/mobile/app/overview/d/d/h$e$d;,
        Lcom/swedbank/mobile/app/overview/d/d/h$e$b;,
        Lcom/swedbank/mobile/app/overview/d/d/h$e$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 43
    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/app/overview/d/d/h;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/d/h$e;->a:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 42
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UUID.randomUUID().toString()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/overview/d/d/h$e;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/h$e;->a:Ljava/lang/String;

    return-object v0
.end method
