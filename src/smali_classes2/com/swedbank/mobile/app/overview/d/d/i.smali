.class public final Lcom/swedbank/mobile/app/overview/d/d/i;
.super Ljava/lang/Object;
.source "OverviewStatementItem.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/d/d/h$f;
    .locals 12
    .param p0    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toStatementViewModel"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/h$f;

    .line 83
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->d()Ljava/lang/String;

    move-result-object v2

    .line 84
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->e()Ljava/util/Date;

    move-result-object v3

    .line 85
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->f()Ljava/math/BigDecimal;

    move-result-object v4

    .line 86
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->g()Ljava/lang/String;

    move-result-object v5

    .line 87
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->h()Ljava/lang/String;

    move-result-object v6

    .line 88
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->j()Ljava/lang/String;

    move-result-object v7

    .line 89
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->k()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v8

    .line 90
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->l()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v9

    .line 91
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->a()Z

    move-result v10

    .line 92
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->m()I

    move-result v11

    move-object v1, v0

    .line 82
    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/d/d/h$f;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Type;Lcom/swedbank/mobile/business/overview/Transaction$Direction;ZI)V

    return-object v0
.end method
