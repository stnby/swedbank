.class public final Lcom/swedbank/mobile/app/overview/d/d/g;
.super Ljava/lang/Object;
.source "OverviewStatementDataMapper.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/overview/plugins/statement/c;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/swedbank/mobile/app/overview/d/d/g;-><init>(Lkotlin/e/a/a;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/e/a/a;)V
    .locals 1
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lorg/threeten/bp/LocalDate;",
            ">;)V"
        }
    .end annotation

    const-string v0, "today"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/d/g;->a:Lkotlin/e/a/a;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/a/a;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 15
    sget-object p1, Lcom/swedbank/mobile/app/overview/d/d/g$1;->a:Lcom/swedbank/mobile/app/overview/d/d/g$1;

    check-cast p1, Lkotlin/e/a/a;

    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/overview/d/d/g;-><init>(Lkotlin/e/a/a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/plugins/statement/c;)Lcom/swedbank/mobile/app/plugins/list/g;
    .locals 13
    .param p1    # Lcom/swedbank/mobile/business/overview/plugins/statement/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unmappedData"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/c;->b()Ljava/util/List;

    move-result-object v0

    .line 21
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 22
    new-instance v2, Lcom/swedbank/mobile/app/overview/d/d/h$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/c;->d()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v2, v5, v3, v4, v5}, Lcom/swedbank/mobile/app/overview/d/d/h$b;-><init>(Ljava/lang/String;ZILkotlin/e/b/g;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/c;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 25
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/h$a;

    invoke-direct {v0, v5, v4, v5}, Lcom/swedbank/mobile/app/overview/d/d/h$a;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 29
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 30
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/h$d;

    invoke-direct {v0, v5, v4, v5}, Lcom/swedbank/mobile/app/overview/d/d/h$d;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 34
    :cond_1
    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/d/d/g;->a:Lkotlin/e/a/a;

    invoke-interface {v2}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/threeten/bp/LocalDate;

    const-wide/16 v6, 0x1

    .line 35
    invoke-virtual {v2, v6, v7}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 36
    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v8

    int-to-long v8, v8

    sub-long/2addr v8, v6

    invoke-virtual {v2, v8, v9}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v6

    const-string v7, "today.minusDays(today.dayOfMonth - 1L)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, v2}, Lcom/swedbank/mobile/business/util/d;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Lcom/swedbank/mobile/business/util/h;

    move-result-object v6

    .line 38
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    .line 39
    check-cast v0, Ljava/lang/Iterable;

    .line 105
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 106
    move-object v9, v8

    check-cast v9, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 40
    invoke-virtual {v9}, Lcom/swedbank/mobile/business/overview/Transaction;->e()Ljava/util/Date;

    move-result-object v9

    invoke-static {v9}, Lcom/swedbank/mobile/business/util/d;->a(Ljava/util/Date;)Lorg/threeten/bp/LocalDate;

    move-result-object v9

    .line 42
    invoke-static {v9, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v9, Lcom/swedbank/mobile/app/overview/d/d/r$c;->a:Lcom/swedbank/mobile/app/overview/d/d/r$c;

    check-cast v9, Lcom/swedbank/mobile/app/overview/d/d/r;

    goto :goto_1

    .line 43
    :cond_2
    invoke-static {v9, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    sget-object v9, Lcom/swedbank/mobile/app/overview/d/d/r$d;->a:Lcom/swedbank/mobile/app/overview/d/d/r$d;

    check-cast v9, Lcom/swedbank/mobile/app/overview/d/d/r;

    goto :goto_1

    .line 44
    :cond_3
    move-object v10, v9

    check-cast v10, Ljava/lang/Comparable;

    invoke-virtual {v6, v10}, Lcom/swedbank/mobile/business/util/h;->a(Ljava/lang/Comparable;)Z

    move-result v10

    if-eqz v10, :cond_4

    sget-object v9, Lcom/swedbank/mobile/app/overview/d/d/r$b;->a:Lcom/swedbank/mobile/app/overview/d/d/r$b;

    check-cast v9, Lcom/swedbank/mobile/app/overview/d/d/r;

    goto :goto_1

    .line 45
    :cond_4
    new-instance v10, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    invoke-virtual {v9}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v11

    const-string v12, "date.month"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v9

    invoke-direct {v10, v11, v9}, Lcom/swedbank/mobile/app/overview/d/d/r$a;-><init>(Lorg/threeten/bp/Month;I)V

    move-object v9, v10

    check-cast v9, Lcom/swedbank/mobile/app/overview/d/d/r;

    .line 107
    :goto_1
    move-object v10, v7

    check-cast v10, Ljava/util/Map;

    .line 108
    invoke-interface {v10, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_5

    .line 107
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 111
    invoke-interface {v10, v9, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_5
    check-cast v11, Ljava/util/List;

    .line 115
    invoke-interface {v11, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    :cond_6
    move-object v0, v7

    check-cast v0, Ljava/util/Map;

    .line 49
    invoke-virtual {v7}, Ljava/util/TreeMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    const-string v2, "transactionGroups\n      \u2026      .descendingKeySet()"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 118
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/app/overview/d/d/r;

    .line 51
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/overview/d/d/r;->a()Lcom/swedbank/mobile/app/overview/d/d/h$e;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-virtual {v7, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_7

    check-cast v2, Ljava/lang/Iterable;

    move-object v3, v1

    check-cast v3, Ljava/util/Collection;

    .line 119
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 120
    check-cast v6, Lcom/swedbank/mobile/business/overview/Transaction;

    .line 52
    invoke-static {v6}, Lcom/swedbank/mobile/app/overview/d/d/i;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/d/d/h$f;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 121
    :cond_8
    check-cast v3, Ljava/util/ArrayList;

    goto :goto_2

    .line 55
    :cond_9
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/c;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 56
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/h$c;

    invoke-direct {v0, v5, v4, v5}, Lcom/swedbank/mobile/app/overview/d/d/h$c;-><init>(Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_a
    :goto_4
    new-instance v0, Lcom/swedbank/mobile/app/plugins/list/g;

    .line 60
    check-cast v1, Ljava/util/List;

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/overview/plugins/statement/c;->c()Z

    move-result p1

    .line 59
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/overview/plugins/statement/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/d/d/g;->a(Lcom/swedbank/mobile/business/overview/plugins/statement/c;)Lcom/swedbank/mobile/app/plugins/list/g;

    move-result-object p1

    return-object p1
.end method
