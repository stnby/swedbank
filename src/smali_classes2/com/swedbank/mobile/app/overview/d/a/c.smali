.class public final Lcom/swedbank/mobile/app/overview/d/a/c;
.super Ljava/lang/Object;
.source "OverviewAccountRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/overview/d/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/a/c;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/a/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/a/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/a/c;->a:Lcom/swedbank/mobile/app/overview/d/a/c;

    .line 22
    const-class v0, Lcom/swedbank/mobile/app/overview/d/a/a;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/a/c;->b:Lkotlin/h/b;

    .line 23
    sget v0, Lcom/swedbank/mobile/app/overview/i$e;->item_overview_account:I

    sput v0, Lcom/swedbank/mobile/app/overview/d/a/c;->c:I

    const/4 v0, 0x1

    .line 24
    sput-boolean v0, Lcom/swedbank/mobile/app/overview/d/a/c;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/a/c;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/d/a/a;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/overview/d/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/a/a;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionConsumer"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string v1, "holder.itemView"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget v1, Lcom/swedbank/mobile/app/overview/i$d;->transaction_account_name:I

    invoke-virtual {p2, v1}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 34
    sget v2, Lcom/swedbank/mobile/app/overview/i$d;->transaction_account_available_funds:I

    invoke-virtual {p2, v2}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 36
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/a/a;->b()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/c;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    sget-object p2, Lcom/swedbank/mobile/app/overview/d/a/c;->a:Lcom/swedbank/mobile/app/overview/d/a/c;

    .line 77
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/a/a;->e()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/Parcelable;

    if-eqz p2, :cond_1

    if-eqz p2, :cond_0

    .line 79
    check-cast p2, Lcom/swedbank/mobile/app/overview/d/a/g;

    .line 80
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/d/a/g;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    goto :goto_0

    .line 79
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.app.overview.plugins.accounts.OverviewAccountsPersistableData"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_2

    .line 42
    sget-object p2, Lcom/swedbank/mobile/app/overview/d/a/c;->a:Lcom/swedbank/mobile/app/overview/d/a/c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/a/a;->e()Ljava/lang/String;

    move-result-object p2

    .line 88
    invoke-interface {p3, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/Parcelable;

    .line 43
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/swedbank/mobile/app/overview/i$f;->tag_expand_transition_target:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    .line 45
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 89
    :goto_1
    new-instance p2, Lcom/swedbank/mobile/app/overview/d/a/c$a;

    invoke-direct {p2, v0, p1, p3, p4}, Lcom/swedbank/mobile/app/overview/d/a/c$a;-><init>(Landroid/view/View;Lcom/swedbank/mobile/app/overview/d/a/a;Ljava/util/Map;Lio/reactivex/c/g;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    new-instance p2, Lcom/swedbank/mobile/app/overview/d/a/c$b;

    invoke-direct {p2, p1, p3, p4}, Lcom/swedbank/mobile/app/overview/d/a/c$b;-><init>(Lcom/swedbank/mobile/app/overview/d/a/a;Ljava/util/Map;Lio/reactivex/c/g;)V

    check-cast p2, Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/a/a;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/a/c;->a(Lcom/swedbank/mobile/app/overview/d/a/a;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 23
    sget v0, Lcom/swedbank/mobile/app/overview/d/a/c;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 24
    sget-boolean v0, Lcom/swedbank/mobile/app/overview/d/a/c;->d:Z

    return v0
.end method
