.class public final Lcom/swedbank/mobile/app/overview/d/d/r$a;
.super Lcom/swedbank/mobile/app/overview/d/d/r;
.source "OverviewStatementDataMapper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/d/d/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lorg/threeten/bp/Month;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Lorg/threeten/bp/Month;I)V
    .locals 1
    .param p1    # Lorg/threeten/bp/Month;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "month"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 97
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/overview/d/d/r;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    iput p2, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/overview/d/d/h$e;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 99
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/h$e$a;

    .line 100
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    .line 101
    iget v2, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    .line 99
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/overview/d/d/h$e$a;-><init>(Lorg/threeten/bp/Month;I)V

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/d/h$e;

    return-object v0
.end method

.method public final b()Lorg/threeten/bp/Month;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    iget-object v3, p1, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    iget p1, p1, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    if-ne v1, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Month(month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->a:Lorg/threeten/bp/Month;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
