.class public final Lcom/swedbank/mobile/app/overview/d/a/c$a;
.super Lcom/swedbank/mobile/core/ui/h;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/d/a/c;->a(Lcom/swedbank/mobile/app/overview/d/a/a;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/swedbank/mobile/app/overview/d/a/a;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lio/reactivex/c/g;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/swedbank/mobile/app/overview/d/a/a;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->b:Lcom/swedbank/mobile/app/overview/d/a/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->c:Ljava/util/Map;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->d:Lio/reactivex/c/g;

    .line 113
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/overview/i$f;->tag_expand_transition_target:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 302
    sget-object p1, Lcom/swedbank/mobile/app/overview/d/a/c;->a:Lcom/swedbank/mobile/app/overview/d/a/c;

    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->c:Ljava/util/Map;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->b:Lcom/swedbank/mobile/app/overview/d/a/a;

    .line 303
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/a/a;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/swedbank/mobile/app/overview/d/a/g;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/overview/d/a/g;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Parcelable;

    .line 304
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->d:Lio/reactivex/c/g;

    new-instance v0, Lcom/swedbank/mobile/business/overview/plugins/accounts/a;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/d/a/c$a;->b:Lcom/swedbank/mobile/app/overview/d/a/a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/overview/d/a/a;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/swedbank/mobile/business/overview/plugins/accounts/a;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-interface {p1, v0}, Lio/reactivex/c/g;->b(Ljava/lang/Object;)V

    return-void
.end method
