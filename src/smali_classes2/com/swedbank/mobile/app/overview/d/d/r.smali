.class public abstract Lcom/swedbank/mobile/app/overview/d/d/r;
.super Ljava/lang/Object;
.source "OverviewStatementDataMapper.kt"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/d/d/r$c;,
        Lcom/swedbank/mobile/app/overview/d/d/r$d;,
        Lcom/swedbank/mobile/app/overview/d/d/r$b;,
        Lcom/swedbank/mobile/app/overview/d/d/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/swedbank/mobile/app/overview/d/d/r;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/d/d/r;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/app/overview/d/d/r;)I
    .locals 5
    .param p1    # Lcom/swedbank/mobile/app/overview/d/d/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/d/r;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    goto/16 :goto_1

    .line 71
    :cond_0
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/r$c;->a:Lcom/swedbank/mobile/app/overview/d/d/r$c;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    .line 72
    :cond_2
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/r$d;->a:Lcom/swedbank/mobile/app/overview/d/d/r$d;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/r$c;->a:Lcom/swedbank/mobile/app/overview/d/d/r$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_3
    :goto_0
    const/4 v2, -0x1

    goto :goto_1

    .line 73
    :cond_4
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/r$b;->a:Lcom/swedbank/mobile/app/overview/d/d/r$b;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/r$c;->a:Lcom/swedbank/mobile/app/overview/d/d/r$c;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/r$d;->a:Lcom/swedbank/mobile/app/overview/d/d/r$d;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 74
    :cond_5
    instance-of v0, p0, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    if-eqz v0, :cond_3

    instance-of v0, p1, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    if-eqz v0, :cond_3

    .line 75
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->c()I

    move-result v1

    check-cast p1, Lcom/swedbank/mobile/app/overview/d/d/r$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->c()I

    move-result v3

    if-eq v1, v3, :cond_6

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->c()I

    move-result p1

    sub-int v2, v0, p1

    goto :goto_1

    .line 76
    :cond_6
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b()Lorg/threeten/bp/Month;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b()Lorg/threeten/bp/Month;

    move-result-object v3

    if-eq v1, v3, :cond_7

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b()Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/d/r$a;->b()Lorg/threeten/bp/Month;

    move-result-object p1

    check-cast p1, Ljava/lang/Enum;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/Month;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    :cond_7
    :goto_1
    return v2
.end method

.method public abstract a()Lcom/swedbank/mobile/app/overview/d/d/h$e;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 66
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/d/r;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/d/d/r;->a(Lcom/swedbank/mobile/app/overview/d/d/r;)I

    move-result p1

    return p1
.end method
