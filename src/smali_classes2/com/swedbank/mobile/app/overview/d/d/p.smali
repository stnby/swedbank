.class public final Lcom/swedbank/mobile/app/overview/d/d/p;
.super Ljava/lang/Object;
.source "StatementSupportItemRenderers.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/overview/d/d/h$e$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/d/p;


# instance fields
.field private final synthetic b:Lcom/swedbank/mobile/app/overview/d/d/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/p;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/d/p;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/p;->a:Lcom/swedbank/mobile/app/overview/d/d/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v6, Lcom/swedbank/mobile/app/overview/d/d/m;

    .line 46
    const-class v0, Lcom/swedbank/mobile/app/overview/d/d/h$e$d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    .line 47
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/p$1;->a:Lcom/swedbank/mobile/app/overview/d/d/p$1;

    move-object v3, v0

    check-cast v3, Lkotlin/e/a/m;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v6

    .line 45
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/d/d/m;-><init>(Lkotlin/h/b;ILkotlin/e/a/m;ILkotlin/e/b/g;)V

    iput-object v6, p0, Lcom/swedbank/mobile/app/overview/d/d/p;->b:Lcom/swedbank/mobile/app/overview/d/d/m;

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/d/h$e$d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/p;->b:Lcom/swedbank/mobile/app/overview/d/d/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/m;->a()Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/d/d/h$e$d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/d/d/h$e$d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/d/h$e$d;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionConsumer"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/p;->b:Lcom/swedbank/mobile/app/overview/d/d/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/d/m;->a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 45
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/d/h$e$d;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/d/p;->a(Lcom/swedbank/mobile/app/overview/d/d/h$e$d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/p;->b:Lcom/swedbank/mobile/app/overview/d/d/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/m;->b()I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/p;->b:Lcom/swedbank/mobile/app/overview/d/d/m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/d/d/m;->c()Z

    move-result v0

    return v0
.end method
