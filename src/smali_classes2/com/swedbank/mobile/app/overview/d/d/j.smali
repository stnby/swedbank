.class public final Lcom/swedbank/mobile/app/overview/d/d/j;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OverviewStatementRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/plugins/statement/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/overview/statement/a/b;

.field private final f:Lcom/swedbank/mobile/app/overview/search/a;

.field private final g:Lcom/swedbank/mobile/business/overview/statement/details/f;

.field private final h:Lcom/swedbank/mobile/business/overview/search/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/overview/statement/a/b;Lcom/swedbank/mobile/app/overview/search/a;Lcom/swedbank/mobile/business/overview/statement/details/f;Lcom/swedbank/mobile/business/overview/search/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/overview/statement/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/overview/search/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/statement/details/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_statement"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/overview/search/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_statement"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_statement"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/statement/a/b;",
            "Lcom/swedbank/mobile/app/overview/search/a;",
            "Lcom/swedbank/mobile/business/overview/statement/details/f;",
            "Lcom/swedbank/mobile/business/overview/search/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transactionDetailsBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewSearchBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionDetailsListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewSearchListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    .line 30
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->e:Lcom/swedbank/mobile/app/overview/statement/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->f:Lcom/swedbank/mobile/app/overview/search/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->g:Lcom/swedbank/mobile/business/overview/statement/details/f;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->h:Lcom/swedbank/mobile/business/overview/search/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/d/d/j;)Lcom/swedbank/mobile/app/overview/statement/a/b;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->e:Lcom/swedbank/mobile/app/overview/statement/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/d/d/j;)Lcom/swedbank/mobile/business/overview/statement/details/f;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->g:Lcom/swedbank/mobile/business/overview/statement/details/f;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/d/d/j;)Lcom/swedbank/mobile/app/overview/search/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->f:Lcom/swedbank/mobile/app/overview/search/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/d/d/j;)Lcom/swedbank/mobile/business/overview/search/c;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->h:Lcom/swedbank/mobile/business/overview/search/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/business/overview/statement/details/g;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->e:Lcom/swedbank/mobile/app/overview/statement/a/b;

    .line 43
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/overview/statement/a/b;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/statement/a/b;

    move-result-object p1

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/j;->g:Lcom/swedbank/mobile/business/overview/statement/details/f;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/overview/statement/a/b;->a(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/app/overview/statement/a/b;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/statement/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 65
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->b(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    .line 46
    check-cast v0, Lcom/swedbank/mobile/business/overview/statement/details/g;

    .line 47
    new-instance v1, Lcom/swedbank/mobile/app/overview/d/d/j$c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/overview/d/d/j$c;-><init>(Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/overview/d/d/j;->c(Lkotlin/e/a/b;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .line 66
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/j$a;->a:Lcom/swedbank/mobile/app/overview/d/d/j$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/j$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/overview/d/d/j$d;-><init>(Lcom/swedbank/mobile/app/overview/d/d/j;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/d/d/j;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 53
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/j$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/d/d/j$e;-><init>(Lcom/swedbank/mobile/app/overview/d/d/j;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/d/d/j;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 68
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/j$b;->a:Lcom/swedbank/mobile/app/overview/d/d/j$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
