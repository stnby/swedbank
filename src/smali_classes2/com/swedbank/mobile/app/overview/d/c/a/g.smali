.class public final Lcom/swedbank/mobile/app/overview/d/c/a/g;
.super Ljava/lang/Object;
.source "SearchHistoryRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/overview/d/c/a/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/c/a/g;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/c/a/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/c/a/g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;->a:Lcom/swedbank/mobile/app/overview/d/c/a/g;

    .line 15
    const-class v0, Lcom/swedbank/mobile/app/overview/d/c/a/f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;->b:Lkotlin/h/b;

    .line 16
    sget v0, Lcom/swedbank/mobile/app/overview/i$e;->item_overview_history:I

    sput v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;->c:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/c/a/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 15
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/d/c/a/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/d/c/a/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/c/a/f;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object p3, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string v0, "holder.itemView"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget v0, Lcom/swedbank/mobile/app/overview/i$d;->overview_history_keyword:I

    invoke-virtual {p2, v0}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 27
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/c/a/f;->b()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    new-instance p2, Lcom/swedbank/mobile/app/overview/d/c/a/g$a;

    invoke-direct {p2, p4, p1}, Lcom/swedbank/mobile/app/overview/d/c/a/g$a;-><init>(Lio/reactivex/c/g;Lcom/swedbank/mobile/app/overview/d/c/a/f;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/c/a/f;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/c/a/g;->a(Lcom/swedbank/mobile/app/overview/d/c/a/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 16
    sget v0, Lcom/swedbank/mobile/app/overview/d/c/a/g;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 14
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/f$a;->a(Lcom/swedbank/mobile/app/plugins/list/f;)Z

    move-result v0

    return v0
.end method
