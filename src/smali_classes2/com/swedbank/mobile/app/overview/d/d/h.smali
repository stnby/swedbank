.class public abstract Lcom/swedbank/mobile/app/overview/d/d/h;
.super Ljava/lang/Object;
.source "OverviewStatementItem.kt"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/swedbank/mobile/app/plugins/list/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/d/d/h$f;,
        Lcom/swedbank/mobile/app/overview/d/d/h$e;,
        Lcom/swedbank/mobile/app/overview/d/d/h$b;,
        Lcom/swedbank/mobile/app/overview/d/d/h$d;,
        Lcom/swedbank/mobile/app/overview/d/d/h$c;,
        Lcom/swedbank/mobile/app/overview/d/d/h$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/d/h;->a:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, "feature_overview_statement"

    .line 17
    :cond_0
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/overview/d/d/h;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public e()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/d/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 23
    instance-of v0, p1, Lcom/swedbank/mobile/app/plugins/list/d;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/swedbank/mobile/app/plugins/list/e;->a()Lkotlin/e/a/m;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 20
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->hashCode()I

    move-result v0

    return v0
.end method
