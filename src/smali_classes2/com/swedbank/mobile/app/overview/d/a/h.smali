.class public final Lcom/swedbank/mobile/app/overview/d/a/h;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OverviewAccountsRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/plugins/accounts/e;


# instance fields
.field private final e:Lcom/swedbank/mobile/data/device/a;

.field private final f:Lcom/swedbank/mobile/app/e/a;

.field private final g:Lcom/swedbank/mobile/app/f/a/b;

.field private final h:Lcom/swedbank/mobile/app/overview/detailed/g;

.field private final i:Lcom/swedbank/mobile/business/overview/detailed/e;

.field private final j:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/app/e/a;Lcom/swedbank/mobile/app/f/a/b;Lcom/swedbank/mobile/app/overview/detailed/g;Lcom/swedbank/mobile/business/overview/detailed/e;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/e/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/overview/detailed/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/overview/detailed/e;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_accounts"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_accounts"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_accounts"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/app/e/a;",
            "Lcom/swedbank/mobile/app/f/a/b;",
            "Lcom/swedbank/mobile/app/overview/detailed/g;",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewDetailedBuilder"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewDetailedListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogListener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p7

    move-object v3, p8

    .line 37
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->e:Lcom/swedbank/mobile/data/device/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->f:Lcom/swedbank/mobile/app/e/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->g:Lcom/swedbank/mobile/app/f/a/b;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->h:Lcom/swedbank/mobile/app/overview/detailed/g;

    iput-object p5, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->i:Lcom/swedbank/mobile/business/overview/detailed/e;

    iput-object p6, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->j:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/d/a/h;)Lcom/swedbank/mobile/app/overview/detailed/g;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->h:Lcom/swedbank/mobile/app/overview/detailed/g;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/d/a/h;)Lcom/swedbank/mobile/business/overview/detailed/e;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->i:Lcom/swedbank/mobile/business/overview/detailed/e;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/d/a/h;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->g:Lcom/swedbank/mobile/app/f/a/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/d/a/h;)Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->j:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 76
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/a/h$b;->a:Lcom/swedbank/mobile/app/overview/d/a/h$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/a/h$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/overview/d/a/h$c;-><init>(Lcom/swedbank/mobile/app/overview/d/a/h;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/d/a/h;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "selectedAccountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iban"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/a/h$d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/d/a/h$d;-><init>(Lcom/swedbank/mobile/app/overview/d/a/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/d/a/h;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 78
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/a/h$a;->a:Lcom/swedbank/mobile/app/overview/d/a/h$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->f:Lcom/swedbank/mobile/app/e/a;

    .line 68
    sget v1, Lcom/swedbank/mobile/app/overview/i$f;->overview_account_iban_copied_to_clipboard:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/app/e/a;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/d/a/h;->e:Lcom/swedbank/mobile/data/device/a;

    .line 71
    new-instance v1, Lcom/swedbank/mobile/app/overview/d/a/h$e;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/overview/d/a/h$e;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2, p1}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    return-void
.end method
