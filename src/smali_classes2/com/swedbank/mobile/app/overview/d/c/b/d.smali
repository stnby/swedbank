.class public final Lcom/swedbank/mobile/app/overview/d/c/b/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OverviewRemoteSearchRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/plugins/search/remote/e;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/overview/statement/a/b;

.field private final f:Lcom/swedbank/mobile/business/overview/statement/details/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/overview/statement/a/b;Lcom/swedbank/mobile/business/overview/statement/details/f;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/overview/statement/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/statement/details/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_remote_search"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_remote_search"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/statement/a/b;",
            "Lcom/swedbank/mobile/business/overview/statement/details/f;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transactionDetailsBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionDetailsListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    .line 23
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/d/c/b/d;->e:Lcom/swedbank/mobile/app/overview/statement/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/d/c/b/d;->f:Lcom/swedbank/mobile/business/overview/statement/details/f;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/d/c/b/d;)Lcom/swedbank/mobile/app/overview/statement/a/b;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/c/b/d;->e:Lcom/swedbank/mobile/app/overview/statement/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/d/c/b/d;)Lcom/swedbank/mobile/business/overview/statement/details/f;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/d/c/b/d;->f:Lcom/swedbank/mobile/business/overview/statement/details/f;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 37
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/d$a;->a:Lcom/swedbank/mobile/app/overview/d/c/b/d$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/overview/Transaction;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/c/b/d$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/overview/d/c/b/d$b;-><init>(Lcom/swedbank/mobile/app/overview/d/c/b/d;Lcom/swedbank/mobile/business/overview/Transaction;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/d/c/b/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method
