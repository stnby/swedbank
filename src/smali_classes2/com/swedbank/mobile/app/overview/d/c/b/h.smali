.class public final Lcom/swedbank/mobile/app/overview/d/c/b/h;
.super Ljava/lang/Object;
.source "SearchTransactionRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/overview/d/c/b/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/c/b/h;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/c/b/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true

.field private static final e:Ljava/text/SimpleDateFormat;

.field private static final f:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/c/b/h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->a:Lcom/swedbank/mobile/app/overview/d/c/b/h;

    .line 18
    const-class v0, Lcom/swedbank/mobile/app/overview/d/c/b/f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->b:Lkotlin/h/b;

    .line 19
    sget v0, Lcom/swedbank/mobile/app/overview/i$e;->item_overview_transaction:I

    sput v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->c:I

    const/4 v0, 0x1

    .line 20
    sput-boolean v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->d:Z

    .line 56
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd.MM.yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->e:Ljava/text/SimpleDateFormat;

    .line 57
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd.MM.yyyy HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->f:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/c/b/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/d/c/b/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/app/overview/d/c/b/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/c/b/f;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object p3, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string v0, "holder.itemView"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget v0, Lcom/swedbank/mobile/app/overview/i$d;->transaction_description:I

    invoke-virtual {p2, v0}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 35
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "rootView.context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v2, Lcom/swedbank/mobile/app/overview/d/c/b/h;->e:Ljava/text/SimpleDateFormat;

    .line 38
    sget-object v3, Lcom/swedbank/mobile/app/overview/d/c/b/h;->f:Ljava/text/SimpleDateFormat;

    .line 34
    invoke-virtual {p1, v1, p2, v2, v3}, Lcom/swedbank/mobile/app/overview/d/c/b/f;->a(Landroid/content/Context;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)V

    .line 39
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/c/b/f;->g()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    const/4 v1, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 40
    :goto_0
    move-object v2, v0

    check-cast v2, Landroid/view/View;

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    .line 50
    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p2, :cond_2

    .line 42
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/d/c/b/f;->g()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_2
    new-instance p2, Lcom/swedbank/mobile/app/overview/d/c/b/h$a;

    invoke-direct {p2, p4, p1}, Lcom/swedbank/mobile/app/overview/d/c/b/h$a;-><init>(Lio/reactivex/c/g;Lcom/swedbank/mobile/app/overview/d/c/b/f;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/c/b/f;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/c/b/h;->a(Lcom/swedbank/mobile/app/overview/d/c/b/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 19
    sget v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 20
    sget-boolean v0, Lcom/swedbank/mobile/app/overview/d/c/b/h;->d:Z

    return v0
.end method
