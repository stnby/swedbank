.class public final Lcom/swedbank/mobile/app/overview/d/d/s$a;
.super Ljava/lang/Object;
.source "TransactionRenderableItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/d/d/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/swedbank/mobile/app/overview/d/d/s;Landroid/content/Context;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/text/SimpleDateFormat;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/text/SimpleDateFormat;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateTimeFormatter"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    sget v1, Lcom/swedbank/mobile/app/overview/i$d;->transaction_title:I

    invoke-virtual {p2, v1}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 41
    sget v2, Lcom/swedbank/mobile/app/overview/i$d;->transaction_date:I

    invoke-virtual {p2, v2}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 42
    sget v3, Lcom/swedbank/mobile/app/overview/i$d;->transaction_amount:I

    invoke-virtual {p2, v3}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 43
    sget v4, Lcom/swedbank/mobile/app/overview/i$d;->transaction_type:I

    invoke-virtual {p2, v4}, Lcom/swedbank/mobile/app/plugins/list/h;->a(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 46
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->h()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object v4

    sget-object v5, Lcom/swedbank/mobile/app/overview/d/d/t;->a:[I

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/overview/Transaction$Type;->ordinal()I

    move-result v4

    aget v4, v5, v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch v4, :pswitch_data_0

    .line 53
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->f()Ljava/lang/String;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-lez v7, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    .line 52
    :pswitch_0
    sget v4, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_transaction_title_cash_withdrawal:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    goto :goto_3

    .line 50
    :pswitch_1
    sget v4, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_transaction_title_cash_deposit:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    goto :goto_3

    .line 48
    :pswitch_2
    sget v4, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_transaction_title_currency_exchange:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    goto :goto_3

    :cond_0
    const/4 v7, 0x0

    :goto_0
    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    .line 53
    :goto_2
    check-cast v4, Ljava/lang/CharSequence;

    goto :goto_3

    :cond_2
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->g()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 46
    :goto_3
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->b()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/d;->c(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->b()Ljava/util/Date;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    goto :goto_4

    .line 57
    :cond_3
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->b()Ljava/util/Date;

    move-result-object p3

    invoke-virtual {p4, p3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    .line 55
    :goto_4
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->c()Ljava/math/BigDecimal;

    move-result-object p3

    .line 61
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->d()Ljava/lang/String;

    move-result-object p4

    .line 62
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->i()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v1

    .line 59
    invoke-static {p1, p3, p4, v1}, Lcom/swedbank/mobile/util/a;->a(Landroid/content/Context;Ljava/math/BigDecimal;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Direction;)Landroid/text/SpannedString;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->j()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 66
    invoke-virtual {p2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 67
    sget p1, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_transaction_mass_payments_count:I

    new-array p3, v5, [Ljava/lang/Object;

    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->k()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, p3, v6

    invoke-virtual {v0, p1, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 69
    :cond_4
    invoke-interface {p0}, Lcom/swedbank/mobile/app/overview/d/d/s;->h()Lcom/swedbank/mobile/business/overview/Transaction$Type;

    move-result-object p0

    sget-object p1, Lcom/swedbank/mobile/business/overview/Transaction$Type;->RESERVATION:Lcom/swedbank/mobile/business/overview/Transaction$Type;

    if-ne p0, p1, :cond_5

    .line 70
    invoke-virtual {p2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    sget p0, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_transaction_reserved:I

    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    :cond_5
    const/4 p0, 0x4

    .line 73
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
