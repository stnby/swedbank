.class public final Lcom/swedbank/mobile/app/overview/d/d/q;
.super Ljava/lang/Object;
.source "StatementTransactionRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/overview/d/d/h$f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/d/q;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/d/h$f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true

.field private static final e:Ljava/text/SimpleDateFormat;

.field private static final f:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 16
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/q;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/d/q;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/q;->a:Lcom/swedbank/mobile/app/overview/d/d/q;

    .line 17
    const-class v0, Lcom/swedbank/mobile/app/overview/d/d/h$f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/q;->b:Lkotlin/h/b;

    .line 18
    sget v0, Lcom/swedbank/mobile/app/overview/i$e;->item_overview_transaction:I

    sput v0, Lcom/swedbank/mobile/app/overview/d/d/q;->c:I

    const/4 v0, 0x1

    .line 19
    sput-boolean v0, Lcom/swedbank/mobile/app/overview/d/d/q;->d:Z

    .line 45
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd.MM.yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/q;->e:Ljava/text/SimpleDateFormat;

    .line 46
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd.MM.yyyy HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/q;->f:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/d/h$f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/q;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/d/d/h$f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/overview/d/d/h$f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/d/h$f;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object p3, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string v0, "holder.itemView"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "rootView.context"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget-object v1, Lcom/swedbank/mobile/app/overview/d/d/q;->e:Ljava/text/SimpleDateFormat;

    .line 35
    sget-object v2, Lcom/swedbank/mobile/app/overview/d/d/q;->f:Ljava/text/SimpleDateFormat;

    .line 31
    invoke-virtual {p1, v0, p2, v1, v2}, Lcom/swedbank/mobile/app/overview/d/d/h$f;->a(Landroid/content/Context;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)V

    .line 41
    new-instance p2, Lcom/swedbank/mobile/app/overview/d/d/q$a;

    invoke-direct {p2, p4, p1}, Lcom/swedbank/mobile/app/overview/d/d/q$a;-><init>(Lio/reactivex/c/g;Lcom/swedbank/mobile/app/overview/d/d/h$f;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/d/h$f;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/d/q;->a(Lcom/swedbank/mobile/app/overview/d/d/h$f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 18
    sget v0, Lcom/swedbank/mobile/app/overview/d/d/q;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 19
    sget-boolean v0, Lcom/swedbank/mobile/app/overview/d/d/q;->d:Z

    return v0
.end method
