.class public final Lcom/swedbank/mobile/app/overview/d/d/c;
.super Ljava/lang/Object;
.source "StatementSupportItemRenderers.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/overview/d/d/h$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/d/c;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/d/h$c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 94
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/d/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/d/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/c;->a:Lcom/swedbank/mobile/app/overview/d/d/c;

    .line 95
    const-class v0, Lcom/swedbank/mobile/app/overview/d/d/h$c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/d/c;->b:Lkotlin/h/b;

    .line 96
    sget v0, Lcom/swedbank/mobile/app/overview/i$e;->item_overview_loading:I

    sput v0, Lcom/swedbank/mobile/app/overview/d/d/c;->c:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/overview/d/d/h$c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 95
    sget-object v0, Lcom/swedbank/mobile/app/overview/d/d/c;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/d/d/h$c;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/d/d/h$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/overview/d/d/h$c;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionConsumer"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/plugins/list/f$a;->a(Lcom/swedbank/mobile/app/plugins/list/f;Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 94
    check-cast p1, Lcom/swedbank/mobile/app/overview/d/d/h$c;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/d/d/c;->a(Lcom/swedbank/mobile/app/overview/d/d/h$c;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 96
    sget v0, Lcom/swedbank/mobile/app/overview/d/d/c;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 94
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/f$a;->a(Lcom/swedbank/mobile/app/plugins/list/f;)Z

    move-result v0

    return v0
.end method
