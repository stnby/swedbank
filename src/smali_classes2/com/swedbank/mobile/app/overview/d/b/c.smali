.class public final Lcom/swedbank/mobile/app/overview/d/b/c;
.super Ljava/lang/Object;
.source "OverviewLoanOfferDataMapper.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/Boolean;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/d/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/swedbank/mobile/app/overview/d/b/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/d/b/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/d/b/c;->a:Lcom/swedbank/mobile/app/overview/d/b/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)Lcom/swedbank/mobile/app/plugins/list/g;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 9
    new-instance v0, Lcom/swedbank/mobile/app/plugins/list/g;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 11
    new-instance p1, Lcom/swedbank/mobile/app/overview/d/b/d;

    const/4 v2, 0x3

    invoke-direct {p1, v1, v1, v2, v1}, Lcom/swedbank/mobile/app/overview/d/b/d;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {p1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 12
    :cond_0
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 9
    invoke-direct {v0, p1, v2, v3, v1}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;ZILkotlin/e/b/g;)V

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/d/b/c;->a(Z)Lcom/swedbank/mobile/app/plugins/list/g;

    move-result-object p1

    return-object p1
.end method
