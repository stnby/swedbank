.class final Lcom/swedbank/mobile/app/overview/detailed/j$j$b;
.super Ljava/lang/Object;
.source "OverviewDetailedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/detailed/j$j;->b(Ljava/lang/Object;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

.field final synthetic b:Lcom/swedbank/mobile/core/ui/x;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/detailed/j$j;Lcom/swedbank/mobile/core/ui/x;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;->b:Lcom/swedbank/mobile/core/ui/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 5
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/detailed/x$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "forceShowLoading"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

    iget-object v0, v0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

    iget-object v0, v0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/detailed/j;->b(Lcom/swedbank/mobile/app/overview/detailed/j;)Lcom/swedbank/mobile/business/overview/detailed/b;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/detailed/b;->f()Lio/reactivex/w;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/j$j$b$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/overview/detailed/j$j$b$1;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j$j$b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;->b:Lcom/swedbank/mobile/core/ui/x;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 110
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/x$a$f;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/overview/detailed/x$a$f;-><init>(Z)V

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 108
    invoke-static {v1, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v0, p1}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "interactor\n             \u2026))\n                    ))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/j$v;->a:Lcom/swedbank/mobile/app/overview/detailed/j$v;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
