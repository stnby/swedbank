.class public final Lcom/swedbank/mobile/app/overview/detailed/g;
.super Ljava/lang/Object;
.source "OverviewDetailedBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "+",
            "Lcom/swedbank/mobile/business/overview/detailed/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/a/u/d/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/u/d/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/u/d/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->d:Lcom/swedbank/mobile/a/u/d/a$a;

    .line 17
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->a:Lcom/swedbank/mobile/business/util/l;

    .line 18
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->b:Lcom/swedbank/mobile/business/util/l;

    .line 19
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->c:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/detailed/i;)Lcom/swedbank/mobile/app/overview/detailed/g;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/detailed/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "initialData"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/detailed/g;

    .line 26
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/detailed/g;->b:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/overview/detailed/e;)Lcom/swedbank/mobile/app/overview/detailed/g;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/detailed/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/detailed/g;

    .line 30
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/detailed/g;->c:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/detailed/g;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/detailed/g;

    .line 22
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/detailed/g;->a:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->d:Lcom/swedbank/mobile/a/u/d/a$a;

    .line 34
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->a:Lcom/swedbank/mobile/business/util/l;

    sget-object v2, Lcom/swedbank/mobile/app/overview/detailed/g$a;->a:Lcom/swedbank/mobile/app/overview/detailed/g$a;

    check-cast v2, Lkotlin/e/a/a;

    invoke-static {v1, v2}, Lcom/swedbank/mobile/business/util/m;->a(Lcom/swedbank/mobile/business/util/l;Lkotlin/e/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/d/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/u/d/a$a;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->b:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/d/a$a;->d(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/d/a$a;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->c:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/d/a$a;->c(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/d/a$a;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/g;->c:Lcom/swedbank/mobile/business/util/l;

    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/d/a$a;->b(Z)Lcom/swedbank/mobile/a/u/d/a$a;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/d/a$a;->a()Lcom/swedbank/mobile/a/u/d/a;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/d/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0
.end method
