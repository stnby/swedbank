.class public final Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "BalanceRecyclerView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/app/overview/detailed/c;

.field private final b:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private c:Ljava/lang/Integer;

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance p2, Lcom/swedbank/mobile/app/overview/detailed/c;

    invoke-direct {p2}, Lcom/swedbank/mobile/app/overview/detailed/c;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    .line 23
    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3, p3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->b:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 26
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p2

    const-string p3, "PublishRelay.create<Unit>()"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->d:Lcom/b/c/c;

    .line 29
    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->b:Landroidx/recyclerview/widget/LinearLayoutManager;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    const/4 p2, 0x1

    .line 30
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->setHasFixedSize(Z)V

    .line 31
    new-instance p2, Landroidx/recyclerview/widget/o;

    invoke-direct {p2}, Landroidx/recyclerview/widget/o;-><init>()V

    move-object p3, p0

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/o;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 32
    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 33
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/p;

    iget-object p3, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->b:Landroidx/recyclerview/widget/LinearLayoutManager;

    move-object v2, p3

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$i;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p2

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/core/ui/widget/p;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView$i;Ljava/lang/Integer;ILkotlin/e/b/g;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 19
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 20
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final d()I
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->b:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->o()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/c;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;Lcom/swedbank/mobile/business/util/l;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/detailed/a;",
            ">;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialSelectedAccountId"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/overview/detailed/c;->a(Ljava/util/List;)V

    .line 95
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_2

    .line 96
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/business/util/n;

    if-eqz v0, :cond_4

    check-cast p2, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object p2

    .line 93
    check-cast p2, Ljava/lang/String;

    const/4 v0, 0x0

    .line 98
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 99
    check-cast v1, Lcom/swedbank/mobile/app/overview/detailed/a;

    .line 50
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/overview/detailed/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    :goto_1
    if-eq v0, v2, :cond_3

    .line 53
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->scrollToPosition(I)V

    .line 56
    :cond_3
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->d:Lcom/b/c/c;

    sget-object p2, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {p1, p2}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 57
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    .line 104
    :goto_2
    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    .line 58
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->c:Ljava/lang/Integer;

    if-eqz p1, :cond_5

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    const/4 p2, 0x0

    .line 59
    check-cast p2, Ljava/lang/Integer;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->c:Ljava/lang/Integer;

    .line 60
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->scrollToPosition(I)V

    goto :goto_3

    .line 57
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_5
    :goto_3
    return-void
.end method

.method public final b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/c;->c()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/b/c/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->d:Lcom/b/c/c;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 73
    instance-of v0, p1, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;

    if-nez v0, :cond_0

    .line 74
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 77
    :cond_0
    check-cast p1, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;->a()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 78
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 80
    move-object v1, v0

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->c:Ljava/lang/Integer;

    .line 82
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;->b()Ljava/util/List;

    move-result-object p1

    .line 83
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    .line 81
    invoke-virtual {p0, p1, v0}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a(Ljava/util/List;Lcom/swedbank/mobile/business/util/l;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 67
    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;

    .line 68
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a:Lcom/swedbank/mobile/app/overview/detailed/c;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/overview/detailed/c;->a()Ljava/util/List;

    move-result-object v2

    .line 70
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->d()I

    move-result v3

    .line 67
    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView$a;-><init>(Landroid/os/Parcelable;Ljava/util/List;I)V

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method
