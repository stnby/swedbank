.class final Lcom/swedbank/mobile/app/overview/detailed/j$j$d;
.super Ljava/lang/Object;
.source "OverviewDetailedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/detailed/j$j;->b(Ljava/lang/Object;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/detailed/j$j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/detailed/j$j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$d;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/detailed/x$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$d;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

    iget-object p1, p1, Lcom/swedbank/mobile/app/overview/detailed/j$j;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    .line 117
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j$d;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j;

    iget-object p1, p1, Lcom/swedbank/mobile/app/overview/detailed/j$j;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/detailed/j;->b(Lcom/swedbank/mobile/app/overview/detailed/j;)Lcom/swedbank/mobile/business/overview/detailed/b;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/overview/detailed/b;->g()Lio/reactivex/w;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 119
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/j$j$d$1;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j$d$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 126
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/x$a$h;->a:Lcom/swedbank/mobile/app/overview/detailed/x$a$h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "interactor.queryOlderDat\u2026lState.OlderItemsLoading)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/j$v;->a:Lcom/swedbank/mobile/app/overview/detailed/j$v;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/j$j$d;->a(Lkotlin/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
