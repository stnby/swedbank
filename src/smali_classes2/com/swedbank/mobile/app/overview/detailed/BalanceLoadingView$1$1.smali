.class public final Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "BalanceLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:F


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1;FFFFLandroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFFF",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 36
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->a:Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1;

    iput p2, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->b:F

    iput p3, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->c:F

    iput p4, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->d:F

    iput p5, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->e:F

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPaint"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p1, p1

    const v0, 0x3ee66666    # 0.45f

    mul-float v0, v0, p1

    const v1, 0x3ea8f5c3    # 0.33f

    mul-float v1, v1, p1

    sub-float v2, p1, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr p1, v1

    div-float/2addr p1, v3

    int-to-float p2, p2

    .line 42
    iget v3, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->b:F

    sub-float/2addr p2, v3

    const v3, 0x3f2b851f    # 0.67f

    mul-float p2, p2, v3

    .line 44
    new-instance v3, Landroid/graphics/RectF;

    add-float/2addr v0, v2

    iget v4, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->c:F

    add-float/2addr v4, p2

    invoke-direct {v3, v2, p2, v0, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 45
    invoke-virtual {p3, v3, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 47
    iget p2, v3, Landroid/graphics/RectF;->bottom:F

    iget v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->d:F

    add-float/2addr p2, v0

    add-float/2addr v1, p1

    .line 48
    iget v0, p0, Lcom/swedbank/mobile/app/overview/detailed/BalanceLoadingView$1$1;->e:F

    add-float/2addr v0, p2

    invoke-virtual {v3, p1, p2, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 49
    invoke-virtual {p3, v3, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method
