.class public final Lcom/swedbank/mobile/app/overview/detailed/q;
.super Ljava/lang/Object;
.source "OverviewDetailedRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/overview/detailed/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/e/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->a:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->b:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->c:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->d:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->e:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->f:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->g:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->h:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->i:Ljavax/inject/Provider;

    .line 60
    iput-object p10, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->j:Ljavax/inject/Provider;

    .line 61
    iput-object p11, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->k:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/detailed/q;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/e/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/currencies/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/overview/detailed/q;"
        }
    .end annotation

    .line 79
    new-instance v12, Lcom/swedbank/mobile/app/overview/detailed/q;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/q;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/overview/detailed/p;
    .locals 13

    .line 66
    new-instance v12, Lcom/swedbank/mobile/app/overview/detailed/p;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/data/device/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/app/e/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/app/f/a/b;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/app/overview/currencies/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/business/overview/currencies/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->i:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/architect/business/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->j:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/swedbank/mobile/architect/a/b/f;

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/q;->k:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/swedbank/mobile/architect/a/b/g;

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/p;-><init>(Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/app/e/a;Lcom/swedbank/mobile/app/f/a/b;Lcom/swedbank/mobile/app/overview/currencies/c;Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/business/overview/currencies/c;Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v12
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/detailed/q;->a()Lcom/swedbank/mobile/app/overview/detailed/p;

    move-result-object v0

    return-object v0
.end method
