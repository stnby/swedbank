.class public final Lcom/swedbank/mobile/app/overview/detailed/c;
.super Landroidx/recyclerview/widget/RecyclerView$a;
.source "BalanceAdapter.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/widget/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/swedbank/mobile/app/overview/detailed/f;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/widget/q<",
        "Lcom/swedbank/mobile/app/overview/detailed/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/detailed/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    .line 27
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/c;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/swedbank/mobile/app/overview/detailed/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/overview/detailed/a;

    return-object p1
.end method

.method public a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/overview/detailed/f;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "parent.context"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/swedbank/mobile/app/overview/i$e;->item_balance:I

    .line 115
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 114
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 30
    new-instance p2, Lcom/swedbank/mobile/app/overview/detailed/f;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/app/overview/detailed/f;-><init>(Landroid/view/View;)V

    return-object p2

    .line 114
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/detailed/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/c;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/detailed/f;I)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/detailed/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/c;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/a;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/app/overview/detailed/f;->a(Lcom/swedbank/mobile/app/overview/detailed/a;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/detailed/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/c;->a:Ljava/util/List;

    .line 41
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/detailed/c;->notifyDataSetChanged()V

    return-void
.end method

.method public final b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    invoke-static {}, Lcom/swedbank/mobile/app/overview/detailed/d;->a()Lcom/b/c/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public synthetic b(I)Ljava/lang/Object;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/c;->a(I)Lcom/swedbank/mobile/app/overview/detailed/a;

    move-result-object p1

    return-object p1
.end method

.method public final c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    invoke-static {}, Lcom/swedbank/mobile/app/overview/detailed/d;->b()Lcom/b/c/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$x;I)V
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/app/overview/detailed/f;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/detailed/c;->a(Lcom/swedbank/mobile/app/overview/detailed/f;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$x;
    .locals 0

    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/detailed/c;->a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/overview/detailed/f;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$x;

    return-object p1
.end method
