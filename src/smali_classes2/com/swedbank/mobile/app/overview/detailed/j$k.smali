.class final Lcom/swedbank/mobile/app/overview/detailed/j$k;
.super Ljava/lang/Object;
.source "OverviewDetailedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/detailed/j;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/detailed/j;

.field final synthetic b:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/detailed/j;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$k;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/overview/detailed/j$k;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/overview/detailed/a;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/detailed/x$a;",
            ">;"
        }
    .end annotation

    const-string v0, "dataStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 61
    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/j$k$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/detailed/j$k$1;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j$k;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "dataStream.map { it.acco\u2026ditLimitDetailsEnabled) }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/k;->a:Lkotlin/h/i;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/n;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/detailed/n;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v1, "dataStream\n             \u2026tailedData::unmappedData)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$k;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lcom/swedbank/mobile/app/overview/detailed/j;)Ljava/util/Map;

    move-result-object v1

    .line 216
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/j$k$a;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/detailed/j$k$a;-><init>(Ljava/util/Map;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {p1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 217
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/j$k$b;

    invoke-direct {v1}, Lcom/swedbank/mobile/app/overview/detailed/j$k$b;-><init>()V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v1, "map { it.toMappedData(da\u2026t, hasMoreToLoad) }\n    }"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    check-cast v0, Lio/reactivex/s;

    check-cast p1, Lio/reactivex/s;

    .line 220
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/j$k$c;

    invoke-direct {v1}, Lcom/swedbank/mobile/app/overview/detailed/j$k$c;-><init>()V

    check-cast v1, Lio/reactivex/c/c;

    .line 219
    invoke-static {v0, p1, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/j$k;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
