.class public final Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;
.super Ljava/lang/Object;
.source "OverviewDetailedLayout.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/os/Parcelable;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/CharSequence;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/CharSequence;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final d:I

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->a:Landroid/os/Parcelable;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->b:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->c:Ljava/lang/CharSequence;

    iput p4, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->d:I

    iput p5, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->e:I

    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Parcelable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->a:Landroid/os/Parcelable;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->e:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->a:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->b:Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-static {p2, p1, v0}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->c:Ljava/lang/CharSequence;

    invoke-static {p2, p1, v0}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget p2, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->d:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/swedbank/mobile/app/overview/detailed/OverviewDetailedLayout$a;->e:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
