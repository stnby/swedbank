.class final Lcom/swedbank/mobile/app/overview/detailed/j$j;
.super Ljava/lang/Object;
.source "OverviewDetailedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/detailed/j;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/detailed/j;

.field final synthetic b:Lio/reactivex/o;

.field final synthetic c:Lio/reactivex/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/detailed/j;Lio/reactivex/o;Lio/reactivex/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->a:Lcom/swedbank/mobile/app/overview/detailed/j;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->b:Lio/reactivex/o;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->c:Lio/reactivex/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/j$j;->b(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public final b(Ljava/lang/Object;)Lio/reactivex/o;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/detailed/x$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    .line 86
    invoke-interface {p1}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v0

    .line 87
    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/j$j$c;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j$c;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/n;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/detailed/n;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const/4 v1, 0x0

    .line 94
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 95
    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->b:Lio/reactivex/o;

    sget-object v3, Lcom/swedbank/mobile/app/overview/detailed/j$j$a;->a:Lcom/swedbank/mobile/app/overview/detailed/j$j$a;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 93
    invoke-static {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 96
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/j$j$b;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j$j;Lcom/swedbank/mobile/core/ui/x;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 115
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/j$j;->c:Lio/reactivex/o;

    .line 116
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/j$j$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/overview/detailed/j$j$d;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j$j;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 131
    check-cast v0, Lio/reactivex/s;

    .line 132
    check-cast p1, Lio/reactivex/s;

    .line 133
    check-cast v1, Lio/reactivex/s;

    .line 130
    invoke-static {v0, p1, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
