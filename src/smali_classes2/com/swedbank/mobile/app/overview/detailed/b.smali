.class public final Lcom/swedbank/mobile/app/overview/detailed/b;
.super Ljava/lang/Object;
.source "AccountOverview.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/a/a;Z)Lcom/swedbank/mobile/app/overview/detailed/a;
    .locals 12
    .param p0    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toAccountOverview"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->c()Ljava/lang/String;

    move-result-object v2

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->e()Lcom/swedbank/mobile/business/a/d;

    move-result-object v3

    .line 36
    new-instance v11, Lcom/swedbank/mobile/app/overview/detailed/e;

    .line 37
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/d;->c()Ljava/lang/String;

    move-result-object v5

    .line 38
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/d;->d()Ljava/math/BigDecimal;

    move-result-object v6

    .line 39
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/d;->e()Ljava/math/BigDecimal;

    move-result-object v7

    .line 40
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/d;->i()Z

    move-result v8

    .line 42
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->f()I

    move-result p0

    const/4 v3, 0x1

    if-le p0, v3, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    const/4 v10, 0x0

    :goto_0
    move-object v4, v11

    move v9, p1

    .line 36
    invoke-direct/range {v4 .. v10}, Lcom/swedbank/mobile/app/overview/detailed/e;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZZZ)V

    .line 31
    new-instance p0, Lcom/swedbank/mobile/app/overview/detailed/a;

    invoke-direct {p0, v0, v1, v2, v11}, Lcom/swedbank/mobile/app/overview/detailed/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/overview/detailed/e;)V

    return-object p0
.end method

.method public static final a(Ljava/util/List;Z)Ljava/util/List;
    .locals 2
    .param p0    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/a;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/detailed/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toAccountOverviews"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    check-cast p0, Ljava/lang/Iterable;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 46
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 47
    check-cast v1, Lcom/swedbank/mobile/business/a/a;

    .line 29
    invoke-static {v1, p1}, Lcom/swedbank/mobile/app/overview/detailed/b;->a(Lcom/swedbank/mobile/business/a/a;Z)Lcom/swedbank/mobile/app/overview/detailed/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
