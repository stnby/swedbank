.class public final Lcom/swedbank/mobile/app/overview/detailed/f;
.super Landroidx/recyclerview/widget/RecyclerView$x;
.source "BalanceAdapter.kt"


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "availableFundsTextView"

    const-string v4, "getAvailableFundsTextView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "creditLimitTextView"

    const-string v4, "getCreditLimitTextView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/f;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "allCurrenciesLink"

    const-string v4, "getAllCurrenciesLink()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/detailed/f;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$x;-><init>(Landroid/view/View;)V

    .line 52
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->balance_item_available_funds:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->b:Lkotlin/f/c;

    .line 53
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->balance_item_credit_limit:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->c:Lkotlin/f/c;

    .line 54
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->balance_item_all_currencies:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->d:Lkotlin/f/c;

    return-void
.end method

.method private final a()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final b()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final c()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/f;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/detailed/a;)V
    .locals 12
    .param p1    # Lcom/swedbank/mobile/app/overview/detailed/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "account"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/detailed/a;->d()Lcom/swedbank/mobile/app/overview/detailed/e;

    move-result-object v0

    .line 57
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/f;->a()Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    .line 58
    invoke-static {v1, v2}, Landroidx/core/widget/i;->b(Landroid/widget/TextView;I)V

    .line 114
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->b()Ljava/math/BigDecimal;

    move-result-object v3

    .line 116
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x3

    .line 118
    invoke-static {v3, v6, v6, v7, v5}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v7, ".00"

    const/4 v8, 0x2

    .line 119
    invoke-static {v3, v7, v6, v8, v5}, Lkotlin/j/n;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v2

    const-string v9, "."

    .line 120
    invoke-static {v3, v9, v5, v8, v5}, Lkotlin/j/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    invoke-virtual {v4, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 121
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    const v10, 0x3e99999a    # 0.3f

    invoke-direct {v9, v10}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 122
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    if-eqz v7, :cond_0

    const-string v7, "."

    .line 125
    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v4, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    const-string v11, "."

    invoke-static {v3, v11, v5, v8, v5}, Lkotlin/j/n;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v7, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    const-string v3, " "

    .line 127
    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->a()Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v3, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 129
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v7, 0x11

    invoke-virtual {v4, v9, v10, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 132
    new-instance v3, Landroid/text/SpannedString;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v3, v4}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    .line 133
    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/f;->b()Landroid/widget/TextView;

    move-result-object v1

    .line 63
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->d()Z

    move-result v3

    const/16 v4, 0x8

    if-eqz v3, :cond_2

    .line 64
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    invoke-static {v1, v2}, Landroidx/core/widget/i;->b(Landroid/widget/TextView;I)V

    .line 135
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->c()Ljava/math/BigDecimal;

    move-result-object v3

    .line 136
    iget-object v7, p0, Lcom/swedbank/mobile/app/overview/detailed/f;->itemView:Landroid/view/View;

    const-string v9, "itemView"

    invoke-static {v7, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 137
    sget v9, Lcom/swedbank/mobile/app/overview/i$f;->overview_credit_limit:I

    new-array v2, v2, [Ljava/lang/Object;

    .line 138
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3, v6, v6, v8, v5}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 136
    invoke-virtual {v7, v9, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "with(credit) {\n    itemV\u2026= false)} $currency\")\n  }"

    .line 135
    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    sget v2, Lcom/swedbank/mobile/app/overview/i$g;->TextAppearance_Swedbank_Link_Small:I

    invoke-static {v1, v2}, Landroidx/core/widget/i;->a(Landroid/widget/TextView;I)V

    .line 69
    check-cast v1, Landroid/view/View;

    .line 140
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/f$a;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/f$a;-><init>(Lcom/swedbank/mobile/app/overview/detailed/e;Lcom/swedbank/mobile/app/overview/detailed/f;Lcom/swedbank/mobile/app/overview/detailed/a;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 73
    :cond_1
    sget v2, Lcom/swedbank/mobile/app/overview/i$g;->TextAppearance_Swedbank_Text_Small_Faded:I

    invoke-static {v1, v2}, Landroidx/core/widget/i;->a(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 76
    :cond_2
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    :goto_0
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/f;->c()Landroid/widget/TextView;

    move-result-object v1

    .line 81
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->d()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/e;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 82
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    check-cast v1, Landroid/view/View;

    .line 144
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/f$b;

    invoke-direct {v2, v0, p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/f$b;-><init>(Lcom/swedbank/mobile/app/overview/detailed/e;Lcom/swedbank/mobile/app/overview/detailed/f;Lcom/swedbank/mobile/app/overview/detailed/a;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method
