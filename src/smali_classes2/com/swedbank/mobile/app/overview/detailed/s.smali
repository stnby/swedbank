.class public final Lcom/swedbank/mobile/app/overview/detailed/s;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OverviewDetailedViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/detailed/r;
.implements Lcom/swedbank/mobile/architect/a/b/i;
.implements Lcom/swedbank/mobile/core/ui/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/b/a;",
        "Lcom/swedbank/mobile/app/overview/detailed/r;",
        "Lcom/swedbank/mobile/architect/a/b/i<",
        "Lcom/swedbank/mobile/app/overview/detailed/x;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/ao<",
        "Lcom/swedbank/mobile/app/overview/detailed/x;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private k:F

.field private l:Lcom/swedbank/mobile/core/ui/widget/t;

.field private m:Lcom/swedbank/mobile/core/ui/widget/t;

.field private final n:Lcom/swedbank/mobile/core/ui/aj;

.field private final o:Lcom/swedbank/mobile/business/navigation/i;

.field private final p:Z

.field private final q:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final s:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "appBarLayout"

    const-string v4, "getAppBarLayout()Lcom/google/android/material/appbar/AppBarLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "balanceLoading"

    const-string v4, "getBalanceLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "balanceList"

    const-string v4, "getBalanceList()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/detailed/s;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "overviewList"

    const-string v4, "getOverviewList()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/business/navigation/i;ZLcom/swedbank/mobile/business/util/l;Ljava/util/List;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/aj;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/navigation/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_independent"
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/aj;",
            "Lcom/swedbank/mobile/business/navigation/i;",
            "Z",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/app/overview/detailed/i;",
            ">;",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transitionAwareRenderer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationItemReselectStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialData"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProviders"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->n:Lcom/swedbank/mobile/core/ui/aj;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->o:Lcom/swedbank/mobile/business/navigation/i;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->p:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->q:Lcom/swedbank/mobile/business/util/l;

    iput-object p5, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->r:Ljava/util/List;

    iput-object p6, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->s:Lio/reactivex/o;

    .line 57
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_app_bar_layout:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->b:Lkotlin/f/c;

    .line 58
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->c:Lkotlin/f/c;

    .line 59
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_balance_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->d:Lkotlin/f/c;

    .line 60
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_balance_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->e:Lkotlin/f/c;

    .line 61
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_retry_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->f:Lkotlin/f/c;

    .line 62
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->g:Lkotlin/f/c;

    .line 63
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->h:Lkotlin/f/c;

    .line 64
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_detailed_list_refresh:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->i:Lkotlin/f/c;

    .line 65
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_detailed_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->j:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->w()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method private final a(Landroidx/appcompat/widget/Toolbar;)Z
    .locals 1
    .param p1    # Landroidx/appcompat/widget/Toolbar;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 292
    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getTag()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "restored_from_state"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->q()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->v()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroidx/appcompat/widget/Toolbar;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->r()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/content/Context;
    .locals 0

    .line 47
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->u()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/widget/TextView;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->s()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/widget/Button;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->t()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/google/android/material/appbar/AppBarLayout;
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->k()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/app/overview/detailed/s;)F
    .locals 0

    .line 47
    iget p0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->k:F

    return p0
.end method

.method private final k()Lcom/google/android/material/appbar/AppBarLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/appbar/AppBarLayout;

    return-object v0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 1

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->m:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez p0, :cond_0

    const-string v0, "olderItemLoadErrorRenderer"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 1

    .line 47
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->l:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez p0, :cond_0

    const-string v0, "mainErrorRenderer"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method private final p()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final q()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final r()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    return-object v0
.end method

.method private final s()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final t()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final u()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final v()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final w()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/s;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 111
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->r()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 112
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/o;->a(Landroidx/recyclerview/widget/RecyclerView;)Lio/reactivex/o;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/swedbank/mobile/app/overview/detailed/u;->a:Lkotlin/h/i;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/v;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/detailed/v;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "balanceList\n      .obser\u2026countOverview::accountId)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/detailed/x;)V
    .locals 20
    .param p1    # Lcom/swedbank/mobile/app/overview/detailed/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->a()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 135
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->a()Z

    move-result v5

    .line 320
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->b(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v6

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/16 v7, 0x8

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/16 v0, 0x8

    .line 321
    :goto_2
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 323
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->c(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 324
    invoke-virtual {v0, v5}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 325
    invoke-virtual {v0, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 137
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->c()Lcom/swedbank/mobile/app/overview/detailed/a;

    move-result-object v0

    .line 329
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->d(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v5

    if-eqz v0, :cond_4

    .line 331
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/a;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 340
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v5, v0}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 341
    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 344
    :cond_3
    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, ""

    .line 345
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v5, v0}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 139
    :cond_4
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->d()Ljava/util/List;

    move-result-object v0

    .line 140
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->b()Lcom/swedbank/mobile/business/util/l;

    move-result-object v5

    .line 350
    move-object v6, v0

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    xor-int/2addr v6, v2

    const/4 v8, 0x4

    if-eqz v6, :cond_5

    .line 351
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->e(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->setVisibility(I)V

    .line 352
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->e(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v6

    invoke-virtual {v6, v0, v5}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a(Ljava/util/List;Lcom/swedbank/mobile/business/util/l;)V

    goto :goto_4

    .line 356
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->e(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->setVisibility(I)V

    .line 142
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->e()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v0

    .line 364
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->c(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-eqz v4, :cond_6

    goto :goto_5

    :cond_6
    const/4 v8, 0x0

    .line 365
    :goto_5
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 367
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->a(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 146
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    .line 147
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->g()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->h()Lcom/swedbank/mobile/app/w/b;

    move-result-object v1

    .line 376
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->f(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/content/Context;

    move-result-object v6

    const/4 v8, 0x0

    if-eqz v0, :cond_a

    .line 380
    sget v9, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_general_loading_error:I

    .line 383
    instance-of v10, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v10, :cond_8

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 385
    move-object v10, v0

    check-cast v10, Ljava/util/Collection;

    invoke-interface {v10}, Ljava/util/Collection;->isEmpty()Z

    move-result v10

    xor-int/2addr v10, v2

    if-eqz v10, :cond_7

    move-object v11, v0

    check-cast v11, Ljava/lang/Iterable;

    const-string v0, "\n"

    move-object v12, v0

    check-cast v12, Ljava/lang/CharSequence;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x3e

    const/16 v19, 0x0

    invoke-static/range {v11 .. v19}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 386
    :cond_7
    invoke-virtual {v6, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 388
    :cond_8
    instance-of v9, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v9, :cond_9

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 389
    invoke-static {v0}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v9, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v0, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    const-string v9, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 390
    invoke-static {v0, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_a

    goto :goto_8

    .line 389
    :cond_9
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_a
    if-eqz v1, :cond_b

    .line 394
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_b
    move-object v0, v8

    :goto_7
    check-cast v0, Ljava/lang/CharSequence;

    :goto_8
    if-eqz v4, :cond_d

    const/4 v1, 0x0

    if-eqz v0, :cond_c

    .line 399
    sget v2, Lcom/swedbank/mobile/app/overview/i$f;->overview_retry_btn:I

    invoke-virtual {v6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 404
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    .line 406
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 407
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->h(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->i(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 412
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v4, v0, :cond_15

    .line 413
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->j(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/material/appbar/AppBarLayout;->setElevation(F)V

    goto/16 :goto_b

    .line 420
    :cond_c
    sget v0, Lcom/swedbank/mobile/app/overview/i$f;->overview_no_accounts:I

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 421
    sget v2, Lcom/swedbank/mobile/app/overview/i$f;->overview_retry_btn:I

    invoke-virtual {v6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 426
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    .line 428
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 429
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->h(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->i(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 434
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v4, v0, :cond_15

    .line 435
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->j(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/material/appbar/AppBarLayout;->setElevation(F)V

    goto/16 :goto_b

    .line 453
    :cond_d
    move-object v1, v8

    check-cast v1, Ljava/lang/CharSequence;

    .line 456
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 462
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 464
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->g(Lcom/swedbank/mobile/app/overview/detailed/s;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v1, v3, :cond_e

    .line 465
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->j(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v1

    .line 467
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->k(Lcom/swedbank/mobile/app/overview/detailed/s;)F

    move-result v3

    .line 465
    invoke-virtual {v1, v3}, Lcom/google/android/material/appbar/AppBarLayout;->setElevation(F)V

    :cond_e
    if-eqz v0, :cond_f

    .line 476
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->l(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 477
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->m(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    .line 478
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/s$d;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/overview/detailed/s$d;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 479
    new-instance v3, Lcom/swedbank/mobile/app/overview/detailed/s$e;

    invoke-direct {v3, v0, v6}, Lcom/swedbank/mobile/app/overview/detailed/s$e;-><init>(Ljava/lang/CharSequence;Landroid/content/Context;)V

    check-cast v3, Lkotlin/e/a/b;

    .line 477
    invoke-virtual {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/t;->a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V

    goto/16 :goto_b

    .line 486
    :cond_f
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->m(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    if-eqz v5, :cond_13

    .line 489
    sget v0, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_general_loading_error:I

    .line 492
    instance-of v1, v5, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_11

    check-cast v5, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 494
    move-object v3, v1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v2, v3

    if-eqz v2, :cond_10

    move-object v7, v1

    check-cast v7, Ljava/lang/Iterable;

    const-string v0, "\n"

    move-object v8, v0

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3e

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 495
    :cond_10
    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 497
    :cond_11
    instance-of v0, v5, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_12

    check-cast v5, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 498
    invoke-static {v0}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_9
    const-string v1, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 499
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v0

    check-cast v8, Ljava/lang/CharSequence;

    goto :goto_a

    .line 498
    :cond_12
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_13
    :goto_a
    if-eqz v8, :cond_14

    .line 502
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->l(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v0

    .line 503
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/s$f;

    invoke-direct {v1, v8}, Lcom/swedbank/mobile/app/overview/detailed/s$f;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lkotlin/e/a/b;

    .line 504
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/s$g;

    invoke-direct {v2, v8, v6}, Lcom/swedbank/mobile/app/overview/detailed/s$g;-><init>(Ljava/lang/CharSequence;Landroid/content/Context;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 502
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/t;->a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V

    goto :goto_b

    .line 511
    :cond_14
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->l(Lcom/swedbank/mobile/app/overview/detailed/s;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :cond_15
    :goto_b
    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 47
    check-cast p1, Lcom/swedbank/mobile/app/overview/detailed/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/s;->b(Lcom/swedbank/mobile/app/overview/detailed/x;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 115
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->r()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/app/overview/detailed/x;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/detailed/x;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->n:Lcom/swedbank/mobile/core/ui/aj;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/ui/aj;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 47
    check-cast p1, Lcom/swedbank/mobile/app/overview/detailed/x;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/s;->a(Lcom/swedbank/mobile/app/overview/detailed/x;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 117
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->r()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 120
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 319
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 121
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v2}, Lcom/b/b/d/d;->a(Landroid/view/View;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 119
    invoke-static {v1, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026    toolbar.longClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected e()V
    .locals 8

    .line 72
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->n:Lcom/swedbank/mobile/core/ui/aj;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v2, v4}, Lcom/swedbank/mobile/core/ui/aj$a;->a(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/architect/a/b/a;ZILjava/lang/Object;)V

    .line 295
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 296
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/s$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/overview/detailed/s$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v4, Lcom/swedbank/mobile/app/overview/detailed/t;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/app/overview/detailed/t;-><init>(Lkotlin/e/a/a;)V

    check-cast v4, Lio/reactivex/c/a;

    invoke-static {v4}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v4, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 297
    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->l:Lcom/swedbank/mobile/core/ui/widget/t;

    .line 298
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 299
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/s$b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/overview/detailed/s$b;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v4, Lcom/swedbank/mobile/app/overview/detailed/t;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/app/overview/detailed/t;-><init>(Lkotlin/e/a/a;)V

    check-cast v4, Lio/reactivex/c/a;

    invoke-static {v4}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v4, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 300
    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->m:Lcom/swedbank/mobile/core/ui/widget/t;

    .line 75
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/overview/i$b;->toolbar_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->k:F

    .line 76
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    .line 77
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->p:Z

    if-nez v1, :cond_0

    sget v1, Lcom/swedbank/mobile/app/overview/i$c;->ic_close:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(I)V

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->q:Lcom/swedbank/mobile/business/util/l;

    .line 303
    sget-object v4, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 301
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_1

    .line 304
    :cond_1
    instance-of v4, v1, Lcom/swedbank/mobile/business/util/n;

    if-eqz v4, :cond_5

    check-cast v1, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v1

    .line 301
    check-cast v1, Lcom/swedbank/mobile/app/overview/detailed/i;

    .line 82
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/swedbank/mobile/app/overview/detailed/s;->a(Landroidx/appcompat/widget/Toolbar;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 83
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/overview/detailed/i;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/overview/detailed/i;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 307
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 308
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 311
    :cond_2
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    const-string v1, ""

    .line 312
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 85
    :cond_3
    :goto_0
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 315
    :goto_1
    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    .line 87
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->v()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 88
    new-array v1, v2, [I

    sget v2, Lcom/swedbank/mobile/app/overview/i$a;->brand_orange:I

    aput v2, v1, v3

    const/4 v2, 0x1

    sget v4, Lcom/swedbank/mobile/app/overview/i$a;->brand_turqoise:I

    aput v4, v1, v2

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 89
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewStartOffset()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f8ccccd    # 1.1f

    mul-float v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v2

    int-to-float v2, v2

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float v2, v2, v4

    float-to-int v2, v2

    invoke-virtual {v0, v3, v1, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZII)V

    .line 91
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->w()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->r:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Ljava/util/List;)V

    .line 92
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->p:Z

    if-eqz v0, :cond_4

    .line 93
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->o:Lcom/swedbank/mobile/business/navigation/i;

    const-string v1, "feature_overview"

    .line 94
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/navigation/i;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 95
    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/s$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/detailed/s$c;-><init>(Lcom/swedbank/mobile/app/overview/detailed/s;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 316
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    :cond_4
    return-void

    .line 85
    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public f()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 124
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->s:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :cond_0
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.empty()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 128
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->r()Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/BalanceRecyclerView;->c()Lcom/b/c/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 100
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->w()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 103
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->v()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 104
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->l:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v1, :cond_0

    const-string v2, "mainErrorRenderer"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 105
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->t()Landroid/widget/Button;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 318
    new-instance v3, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v3, Lio/reactivex/o;

    check-cast v3, Lio/reactivex/s;

    .line 102
    invoke-static {v0, v1, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026  errorRetryBtn.clicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 108
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/detailed/s;->w()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->c()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 109
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/detailed/s;->m:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v1, :cond_0

    const-string v2, "olderItemLoadErrorRenderer"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 107
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
