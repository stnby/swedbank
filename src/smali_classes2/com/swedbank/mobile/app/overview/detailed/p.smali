.class public final Lcom/swedbank/mobile/app/overview/detailed/p;
.super Lcom/swedbank/mobile/business/i/f;
.source "OverviewDetailedRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/overview/detailed/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/data/device/a;

.field private final f:Lcom/swedbank/mobile/app/e/a;

.field private final g:Lcom/swedbank/mobile/app/f/a/b;

.field private final h:Lcom/swedbank/mobile/app/overview/currencies/c;

.field private final i:Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;

.field private final j:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

.field private final k:Lcom/swedbank/mobile/business/overview/currencies/c;

.field private final l:Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/app/e/a;Lcom/swedbank/mobile/app/f/a/b;Lcom/swedbank/mobile/app/overview/currencies/c;Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/business/overview/currencies/c;Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/e/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/overview/currencies/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/overview/currencies/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/app/e/a;",
            "Lcom/swedbank/mobile/app/f/a/b;",
            "Lcom/swedbank/mobile/app/overview/currencies/c;",
            "Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            "Lcom/swedbank/mobile/business/overview/currencies/c;",
            "Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currenciesBuilder"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creditLimitDetailsBuilder"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogListener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currenciesListener"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creditLimitDetailsListener"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p9, p11, p10}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->e:Lcom/swedbank/mobile/data/device/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->f:Lcom/swedbank/mobile/app/e/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->g:Lcom/swedbank/mobile/app/f/a/b;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->h:Lcom/swedbank/mobile/app/overview/currencies/c;

    iput-object p5, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->i:Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;

    iput-object p6, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->j:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    iput-object p7, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->k:Lcom/swedbank/mobile/business/overview/currencies/c;

    iput-object p8, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->l:Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/detailed/p;)Lcom/swedbank/mobile/app/overview/currencies/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->h:Lcom/swedbank/mobile/app/overview/currencies/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/detailed/p;)Lcom/swedbank/mobile/business/overview/currencies/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->k:Lcom/swedbank/mobile/business/overview/currencies/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/detailed/p;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->g:Lcom/swedbank/mobile/app/f/a/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/detailed/p;)Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->j:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/detailed/p;)Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->i:Lcom/swedbank/mobile/app/overview/creditlimitdetails/a;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/overview/detailed/p;)Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->l:Lcom/swedbank/mobile/business/overview/creditlimitdetails/d;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 97
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/p$c;->a:Lcom/swedbank/mobile/app/overview/detailed/p$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/p$d;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/p$d;-><init>(Lcom/swedbank/mobile/app/overview/detailed/p;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/detailed/p;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountName"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/p$f;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/overview/detailed/p$f;-><init>(Lcom/swedbank/mobile/app/overview/detailed/p;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/detailed/p;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 99
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/p$a;->a:Lcom/swedbank/mobile/app/overview/detailed/p$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->f:Lcom/swedbank/mobile/app/e/a;

    .line 76
    sget v1, Lcom/swedbank/mobile/app/overview/i$f;->overview_account_iban_copied_to_clipboard:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/swedbank/mobile/app/e/a;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 101
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/p$b;->a:Lcom/swedbank/mobile/app/overview/detailed/p$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/detailed/p;->e:Lcom/swedbank/mobile/data/device/a;

    .line 79
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/p$g;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/overview/detailed/p$g;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2, p1}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/p$e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/overview/detailed/p$e;-><init>(Lcom/swedbank/mobile/app/overview/detailed/p;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/detailed/p;->b(Lkotlin/e/a/b;)V

    return-void
.end method
