.class public final Lcom/swedbank/mobile/app/overview/detailed/j;
.super Lcom/swedbank/mobile/architect/a/d;
.source "OverviewDetailedPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/overview/detailed/r;",
        "Lcom/swedbank/mobile/app/overview/detailed/x;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/overview/detailed/b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;Lcom/swedbank/mobile/business/overview/detailed/b;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "overview_detailed_initial_selected_account_id"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/overview/detailed/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;",
            "Lcom/swedbank/mobile/business/overview/detailed/b;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "initialSelectedAccountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataMappers"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/detailed/j;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/detailed/j;->b:Ljava/util/Map;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/detailed/j;Lcom/swedbank/mobile/app/overview/detailed/x;Lcom/swedbank/mobile/app/overview/detailed/x$a;)Lcom/swedbank/mobile/app/overview/detailed/x;
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lcom/swedbank/mobile/app/overview/detailed/x;Lcom/swedbank/mobile/app/overview/detailed/x$a;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p0

    return-object p0
.end method

.method private final a(Lcom/swedbank/mobile/app/overview/detailed/x;Lcom/swedbank/mobile/app/overview/detailed/x$a;)Lcom/swedbank/mobile/app/overview/detailed/x;
    .locals 12

    .line 184
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$e;

    if-eqz v0, :cond_0

    .line 185
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$e;->a()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xfe

    const/4 v10, 0x0

    move-object v0, p1

    .line 184
    invoke-static/range {v0 .. v10}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto/16 :goto_2

    .line 186
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$f;

    if-eqz v0, :cond_3

    .line 187
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$f;->a()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->a()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, 0x1

    const/4 v1, 0x1

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    move-object v0, p1

    .line 186
    invoke-static/range {v0 .. v10}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto/16 :goto_2

    .line 191
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/x$a$d;->a:Lcom/swedbank/mobile/app/overview/detailed/x$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    .line 192
    sget-object p2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    move-object v3, p2

    check-cast v3, Lcom/swedbank/mobile/business/util/l;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfd

    const/4 v11, 0x0

    move-object v1, p1

    .line 191
    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto/16 :goto_2

    .line 193
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$i;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 194
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$i;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$i;->a()Lcom/swedbank/mobile/app/overview/detailed/a;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfb

    const/4 v11, 0x0

    move-object v1, p1

    .line 193
    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto/16 :goto_2

    .line 195
    :cond_5
    sget-object v0, Lcom/swedbank/mobile/app/overview/detailed/x$a$h;->a:Lcom/swedbank/mobile/app/overview/detailed/x$a$h;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 196
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/detailed/x;->g()Lcom/swedbank/mobile/business/util/e;

    move-result-object p2

    if-eqz p2, :cond_a

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xbf

    const/4 v10, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v10}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto :goto_2

    .line 200
    :cond_6
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$a;

    if-eqz v0, :cond_7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 201
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$a;->a()Ljava/util/List;

    move-result-object v5

    .line 202
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$a;->b()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xe7

    const/4 v11, 0x0

    move-object v1, p1

    .line 200
    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto :goto_2

    .line 203
    :cond_7
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$c;

    if-eqz v0, :cond_8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 205
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xde

    const/4 v11, 0x0

    move-object v1, p1

    .line 203
    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto :goto_2

    .line 206
    :cond_8
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$g;

    if-eqz v0, :cond_9

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 207
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$g;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$g;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0xbf

    const/4 v11, 0x0

    move-object v1, p1

    .line 206
    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    goto :goto_2

    .line 208
    :cond_9
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$b;

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 210
    check-cast p2, Lcom/swedbank/mobile/app/overview/detailed/x$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/detailed/x$a$b;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v9

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    move-object v1, p1

    .line 208
    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/detailed/x;->a(Lcom/swedbank/mobile/app/overview/detailed/x;ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/detailed/x;

    move-result-object p1

    :cond_a
    :goto_2
    return-object p1

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/detailed/j;)Ljava/util/Map;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/j;->b:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/detailed/j;)Lcom/swedbank/mobile/business/overview/detailed/b;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 24

    move-object/from16 v0, p0

    .line 33
    iget-object v1, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/overview/detailed/b;->i()Z

    move-result v1

    .line 34
    sget-object v2, Lcom/swedbank/mobile/app/overview/detailed/j$o;->a:Lcom/swedbank/mobile/app/overview/detailed/j$o;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 35
    sget-object v3, Lcom/swedbank/mobile/app/overview/detailed/j$n;->a:Lcom/swedbank/mobile/app/overview/detailed/j$n;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 37
    iget-object v4, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    .line 38
    invoke-interface {v4}, Lcom/swedbank/mobile/business/overview/detailed/b;->h()Lio/reactivex/o;

    move-result-object v4

    .line 39
    sget-object v5, Lcom/swedbank/mobile/app/overview/detailed/j$s;->a:Lcom/swedbank/mobile/app/overview/detailed/j$s;

    check-cast v5, Lio/reactivex/c/d;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v4

    .line 40
    invoke-virtual {v4}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object v4

    .line 42
    sget-object v5, Lcom/swedbank/mobile/app/overview/detailed/j$p;->a:Lcom/swedbank/mobile/app/overview/detailed/j$p;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {v0, v5}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 43
    new-instance v6, Lcom/swedbank/mobile/app/overview/detailed/j$q;

    iget-object v7, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/overview/detailed/j$q;-><init>(Lcom/swedbank/mobile/business/overview/detailed/b;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/overview/detailed/m;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/overview/detailed/m;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 44
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 45
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 48
    new-instance v6, Lcom/swedbank/mobile/app/overview/detailed/j$r;

    invoke-direct {v6, v1}, Lcom/swedbank/mobile/app/overview/detailed/j$r;-><init>(Z)V

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v4, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v6

    .line 54
    sget-object v7, Lcom/swedbank/mobile/app/overview/detailed/j$l;->a:Lcom/swedbank/mobile/app/overview/detailed/j$l;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {v0, v7}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    .line 55
    sget-object v8, Lcom/swedbank/mobile/app/overview/detailed/j$m;->a:Lcom/swedbank/mobile/app/overview/detailed/j$m;

    check-cast v8, Lio/reactivex/c/h;

    invoke-virtual {v7, v8}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v7

    .line 57
    iget-object v8, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    .line 58
    invoke-interface {v8}, Lcom/swedbank/mobile/business/overview/detailed/b;->e()Lio/reactivex/o;

    move-result-object v8

    .line 59
    new-instance v9, Lcom/swedbank/mobile/app/overview/detailed/j$k;

    invoke-direct {v9, v0, v1}, Lcom/swedbank/mobile/app/overview/detailed/j$k;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j;Z)V

    check-cast v9, Lio/reactivex/c/h;

    invoke-virtual {v8, v9}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 80
    sget-object v8, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v8}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v8

    check-cast v8, Lio/reactivex/s;

    .line 81
    check-cast v4, Lio/reactivex/s;

    .line 79
    invoke-static {v8, v4}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v4

    .line 82
    new-instance v8, Lcom/swedbank/mobile/app/overview/detailed/j$j;

    invoke-direct {v8, v0, v2, v3}, Lcom/swedbank/mobile/app/overview/detailed/j$j;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j;Lio/reactivex/o;Lio/reactivex/o;)V

    check-cast v8, Lio/reactivex/c/h;

    invoke-virtual {v4, v8}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 136
    sget-object v3, Lcom/swedbank/mobile/app/overview/detailed/j$b;->a:Lcom/swedbank/mobile/app/overview/detailed/j$b;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 137
    new-instance v4, Lcom/swedbank/mobile/app/overview/detailed/j$c;

    iget-object v8, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    invoke-direct {v4, v8}, Lcom/swedbank/mobile/app/overview/detailed/j$c;-><init>(Lcom/swedbank/mobile/business/overview/detailed/b;)V

    check-cast v4, Lkotlin/e/a/b;

    new-instance v8, Lcom/swedbank/mobile/app/overview/detailed/m;

    invoke-direct {v8, v4}, Lcom/swedbank/mobile/app/overview/detailed/m;-><init>(Lkotlin/e/a/b;)V

    check-cast v8, Lio/reactivex/c/g;

    invoke-virtual {v3, v8}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 138
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 139
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 141
    sget-object v4, Lcom/swedbank/mobile/app/overview/detailed/j$h;->a:Lcom/swedbank/mobile/app/overview/detailed/j$h;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {v0, v4}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 142
    new-instance v8, Lcom/swedbank/mobile/app/overview/detailed/j$i;

    iget-object v9, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    invoke-direct {v8, v9}, Lcom/swedbank/mobile/app/overview/detailed/j$i;-><init>(Lcom/swedbank/mobile/business/overview/detailed/b;)V

    check-cast v8, Lkotlin/e/a/b;

    new-instance v9, Lcom/swedbank/mobile/app/overview/detailed/m;

    invoke-direct {v9, v8}, Lcom/swedbank/mobile/app/overview/detailed/m;-><init>(Lkotlin/e/a/b;)V

    check-cast v9, Lio/reactivex/c/g;

    invoke-virtual {v4, v9}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 143
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 144
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    .line 146
    sget-object v8, Lcom/swedbank/mobile/app/overview/detailed/j$f;->a:Lcom/swedbank/mobile/app/overview/detailed/j$f;

    check-cast v8, Lkotlin/e/a/b;

    invoke-virtual {v0, v8}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v8

    .line 147
    new-instance v9, Lcom/swedbank/mobile/app/overview/detailed/j$g;

    iget-object v10, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->c:Lcom/swedbank/mobile/business/overview/detailed/b;

    invoke-direct {v9, v10}, Lcom/swedbank/mobile/app/overview/detailed/j$g;-><init>(Lcom/swedbank/mobile/business/overview/detailed/b;)V

    check-cast v9, Lkotlin/e/a/b;

    new-instance v10, Lcom/swedbank/mobile/app/overview/detailed/m;

    invoke-direct {v10, v9}, Lcom/swedbank/mobile/app/overview/detailed/m;-><init>(Lkotlin/e/a/b;)V

    check-cast v10, Lio/reactivex/c/g;

    invoke-virtual {v8, v10}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v8

    .line 148
    invoke-virtual {v8}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v8

    .line 149
    invoke-virtual {v8}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v8

    .line 151
    sget-object v9, Lcom/swedbank/mobile/app/overview/detailed/j$t;->a:Lcom/swedbank/mobile/app/overview/detailed/j$t;

    check-cast v9, Lkotlin/e/a/b;

    invoke-virtual {v0, v9}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v9

    .line 152
    new-instance v10, Lcom/swedbank/mobile/app/overview/detailed/j$u;

    invoke-direct {v10, v0}, Lcom/swedbank/mobile/app/overview/detailed/j$u;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j;)V

    check-cast v10, Lio/reactivex/c/h;

    invoke-virtual {v9, v10}, Lio/reactivex/o;->n(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v9

    .line 153
    invoke-virtual {v9}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v9

    .line 155
    sget-object v10, Lcom/swedbank/mobile/app/overview/detailed/j$d;->a:Lcom/swedbank/mobile/app/overview/detailed/j$d;

    check-cast v10, Lkotlin/e/a/b;

    invoke-virtual {v0, v10}, Lcom/swedbank/mobile/app/overview/detailed/j;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v10

    .line 156
    new-instance v11, Lcom/swedbank/mobile/app/overview/detailed/j$e;

    invoke-direct {v11, v0}, Lcom/swedbank/mobile/app/overview/detailed/j$e;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j;)V

    check-cast v11, Lio/reactivex/c/g;

    invoke-virtual {v10, v11}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v10

    .line 157
    invoke-virtual {v10}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v10

    .line 158
    invoke-virtual {v10}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v10

    .line 160
    new-instance v15, Lcom/swedbank/mobile/app/overview/detailed/x;

    iget-object v11, v0, Lcom/swedbank/mobile/app/overview/detailed/j;->a:Ljava/lang/String;

    invoke-static {v11}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v13

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0xfd

    const/16 v22, 0x0

    move-object v11, v15

    move-object/from16 v23, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move/from16 v20, v21

    move-object/from16 v21, v22

    invoke-direct/range {v11 .. v21}, Lcom/swedbank/mobile/app/overview/detailed/x;-><init>(ZLcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/app/overview/detailed/a;Ljava/util/List;Lcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    const/16 v11, 0xa

    .line 162
    new-array v11, v11, [Lio/reactivex/s;

    .line 163
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v11, v12

    .line 164
    check-cast v6, Lio/reactivex/s;

    const/4 v5, 0x1

    aput-object v6, v11, v5

    .line 165
    check-cast v7, Lio/reactivex/s;

    const/4 v5, 0x2

    aput-object v7, v11, v5

    .line 166
    check-cast v1, Lio/reactivex/s;

    const/4 v5, 0x3

    aput-object v1, v11, v5

    .line 167
    check-cast v2, Lio/reactivex/s;

    const/4 v1, 0x4

    aput-object v2, v11, v1

    .line 168
    check-cast v3, Lio/reactivex/s;

    const/4 v1, 0x5

    aput-object v3, v11, v1

    .line 169
    check-cast v4, Lio/reactivex/s;

    const/4 v1, 0x6

    aput-object v4, v11, v1

    .line 170
    check-cast v8, Lio/reactivex/s;

    const/4 v1, 0x7

    aput-object v8, v11, v1

    .line 171
    check-cast v9, Lio/reactivex/s;

    const/16 v1, 0x8

    aput-object v9, v11, v1

    .line 172
    check-cast v10, Lio/reactivex/s;

    const/16 v1, 0x9

    aput-object v10, v11, v1

    .line 162
    invoke-static {v11}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.mergeArray(\n \u2026\n        backClickStream)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance v2, Lcom/swedbank/mobile/app/overview/detailed/j$a;

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/app/overview/detailed/j;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/overview/detailed/j$a;-><init>(Lcom/swedbank/mobile/app/overview/detailed/j;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 216
    new-instance v3, Lcom/swedbank/mobile/app/overview/detailed/l;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/detailed/l;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    move-object/from16 v2, v23

    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 217
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-static {v0, v1}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
