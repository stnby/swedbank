.class final Lcom/swedbank/mobile/app/overview/detailed/j$s;
.super Ljava/lang/Object;
.source "OverviewDetailedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/detailed/j;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/d<",
        "Lcom/swedbank/mobile/business/a/a;",
        "Lcom/swedbank/mobile/business/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/detailed/j$s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/overview/detailed/j$s;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/detailed/j$s;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/detailed/j$s;->a:Lcom/swedbank/mobile/app/overview/detailed/j$s;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/a/a;Lcom/swedbank/mobile/business/a/a;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "prev"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curr"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/business/a/a;

    check-cast p2, Lcom/swedbank/mobile/business/a/a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/detailed/j$s;->a(Lcom/swedbank/mobile/business/a/a;Lcom/swedbank/mobile/business/a/a;)Z

    move-result p1

    return p1
.end method
