.class final Lcom/swedbank/mobile/app/overview/g$b;
.super Lkotlin/e/b/k;
.source "OverviewRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/g;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/g$b;->a:Lcom/swedbank/mobile/app/overview/g;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/g$b;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/g$b;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/g$b;->d:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 4
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    check-cast p1, Ljava/lang/Iterable;

    .line 103
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 102
    instance-of v0, v0, Lcom/swedbank/mobile/business/overview/detailed/f;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 53
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/g$b;->a:Lcom/swedbank/mobile/app/overview/g;

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/g$b;->a:Lcom/swedbank/mobile/app/overview/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/g;->a(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/app/overview/detailed/g;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/g$b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/overview/detailed/g;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/detailed/g;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/swedbank/mobile/app/overview/detailed/i;

    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/g$b;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/swedbank/mobile/app/overview/g$b;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/overview/detailed/i;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/overview/detailed/g;->a(Lcom/swedbank/mobile/app/overview/detailed/i;)Lcom/swedbank/mobile/app/overview/detailed/g;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/detailed/g;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 107
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->d(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    .line 50
    check-cast p1, Lcom/swedbank/mobile/business/overview/k;

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/g$b;->a:Lcom/swedbank/mobile/app/overview/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/g;->b(Lcom/swedbank/mobile/app/overview/g;)Lcom/b/c/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/g$b;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
