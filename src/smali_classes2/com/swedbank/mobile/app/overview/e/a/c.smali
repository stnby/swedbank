.class public final Lcom/swedbank/mobile/app/overview/e/a/c;
.super Lcom/swedbank/mobile/app/p/c$b;
.source "CombinedOverviewPreferencePlugin.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/app/overview/e/a/a;

.field private final b:Lcom/swedbank/mobile/business/overview/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/overview/e/a/a;Lcom/swedbank/mobile/business/overview/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "nodeBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overviewPreferencesRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/c$b;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/e/a/c;->a:Lcom/swedbank/mobile/app/overview/e/a/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/e/a/c;->b:Lcom/swedbank/mobile/business/overview/m;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/e/a/c;)Lcom/swedbank/mobile/app/overview/e/a/a;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/e/a/c;->a:Lcom/swedbank/mobile/app/overview/e/a/a;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "feature_overview_combined"

    return-object v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x62

    return v0
.end method

.method public c()Lcom/swedbank/mobile/app/p/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    sget-object v0, Lcom/swedbank/mobile/app/p/a;->c:Lcom/swedbank/mobile/app/p/a;

    return-object v0
.end method

.method public d()I
    .locals 1

    .line 28
    sget v0, Lcom/swedbank/mobile/app/overview/i$f;->enable_combined_overview_preference_title:I

    return v0
.end method

.method public e()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    const-class v0, Lcom/swedbank/mobile/business/overview/preferences/combined/b;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    new-instance v0, Lcom/swedbank/mobile/app/overview/e/a/c$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/e/a/c$a;-><init>(Lcom/swedbank/mobile/app/overview/e/a/c;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/e/a/c;->b:Lcom/swedbank/mobile/business/overview/m;

    .line 37
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/m;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method
