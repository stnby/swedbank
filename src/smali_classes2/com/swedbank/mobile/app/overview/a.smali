.class public final Lcom/swedbank/mobile/app/overview/a;
.super Lcom/swedbank/mobile/app/f/a/d$a;
.source "AccountIbanActionsDialogInformation.kt"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "accountIban"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/app/f/a/d$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/a;->a:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/f/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x2

    .line 18
    new-array v0, v0, [Lcom/swedbank/mobile/app/f/a/a;

    .line 19
    new-instance v1, Lcom/swedbank/mobile/app/f/a/a;

    .line 20
    new-instance v2, Lcom/swedbank/mobile/business/overview/a;

    sget-object v3, Lcom/swedbank/mobile/business/overview/b;->a:Lcom/swedbank/mobile/business/overview/b;

    iget-object v4, p0, Lcom/swedbank/mobile/app/overview/a;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/swedbank/mobile/business/overview/a;-><init>(Lcom/swedbank/mobile/business/overview/b;Ljava/lang/String;)V

    .line 21
    sget v3, Lcom/swedbank/mobile/app/overview/i$f;->overview_account_iban_action_dialog_copy:I

    invoke-static {v3}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v3

    .line 22
    sget v4, Lcom/swedbank/mobile/app/overview/i$c;->ic_copy:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 19
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 23
    new-instance v1, Lcom/swedbank/mobile/app/f/a/a;

    .line 24
    new-instance v2, Lcom/swedbank/mobile/business/overview/a;

    sget-object v3, Lcom/swedbank/mobile/business/overview/b;->b:Lcom/swedbank/mobile/business/overview/b;

    iget-object v4, p0, Lcom/swedbank/mobile/app/overview/a;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/swedbank/mobile/business/overview/a;-><init>(Lcom/swedbank/mobile/business/overview/b;Ljava/lang/String;)V

    .line 25
    sget v3, Lcom/swedbank/mobile/app/overview/i$f;->overview_account_iban_action_dialog_share:I

    invoke-static {v3}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v3

    .line 26
    sget v4, Lcom/swedbank/mobile/app/overview/i$c;->ic_share:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 23
    invoke-direct {v1, v2, v3, v4}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 18
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
