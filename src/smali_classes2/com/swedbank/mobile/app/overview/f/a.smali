.class public final Lcom/swedbank/mobile/app/overview/f/a;
.super Ljava/lang/Object;
.source "OverviewRetryBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/overview/retry/c;

.field private b:Lcom/swedbank/mobile/app/overview/f/c;

.field private final c:Lcom/swedbank/mobile/a/u/j/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/u/j/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/u/j/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/f/a;->c:Lcom/swedbank/mobile/a/u/j/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/f/c;)Lcom/swedbank/mobile/app/overview/f/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/f/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "information"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/f/a;->b:Lcom/swedbank/mobile/app/overview/f/c;

    return-object p0
.end method

.method public final a(Lcom/swedbank/mobile/business/overview/retry/c;)Lcom/swedbank/mobile/app/overview/f/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/retry/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/f/a;->a:Lcom/swedbank/mobile/business/overview/retry/c;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/f/a;->c:Lcom/swedbank/mobile/a/u/j/a$a;

    .line 26
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/f/a;->a:Lcom/swedbank/mobile/business/overview/retry/c;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/j/a$a;->b(Lcom/swedbank/mobile/business/overview/retry/c;)Lcom/swedbank/mobile/a/u/j/a$a;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/f/a;->b:Lcom/swedbank/mobile/app/overview/f/c;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/j/a$a;->b(Lcom/swedbank/mobile/app/overview/f/c;)Lcom/swedbank/mobile/a/u/j/a$a;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/j/a$a;->a()Lcom/swedbank/mobile/a/u/j/a;

    move-result-object v0

    .line 33
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/j/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 29
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display overview retry node without knowing how to render it"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 26
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display overview retry node without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
