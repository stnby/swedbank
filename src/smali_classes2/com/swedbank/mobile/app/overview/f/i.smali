.class public final Lcom/swedbank/mobile/app/overview/f/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OverviewRetryViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/f/h;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/f/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "retryTitle"

    const-string v4, "getRetryTitle()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/f/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "retryBtn"

    const-string v4, "getRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/f/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 14
    sget v0, Lcom/swedbank/mobile/app/overview/i$d;->overview_retry_title:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/f/i;->b:Lkotlin/f/c;

    .line 15
    sget v0, Lcom/swedbank/mobile/app/overview/i$d;->overview_retry_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/f/i;->c:Lkotlin/f/c;

    return-void
.end method

.method private final b()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/f/i;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/f/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final c()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/f/i;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/f/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/f/i;->c()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 36
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/overview/f/k;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/overview/f/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/f/k;->a()Lcom/swedbank/mobile/app/overview/f/c;

    move-result-object p1

    .line 22
    instance-of v0, p1, Lcom/swedbank/mobile/app/overview/f/c$b;

    if-eqz v0, :cond_0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/f/i;->b()Landroid/widget/TextView;

    move-result-object v0

    check-cast p1, Lcom/swedbank/mobile/app/overview/f/c$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/f/c$b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/f/i;->c()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/f/c$b;->b()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 26
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/overview/f/c$a;

    if-eqz v0, :cond_1

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/f/i;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/f/i;->b()Landroid/widget/TextView;

    move-result-object v1

    check-cast p1, Lcom/swedbank/mobile/app/overview/f/c$a;

    const-string v2, "resources"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/overview/f/c$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/f/i;->c()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/overview/f/c$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/app/overview/f/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/f/i;->a(Lcom/swedbank/mobile/app/overview/f/k;)V

    return-void
.end method
