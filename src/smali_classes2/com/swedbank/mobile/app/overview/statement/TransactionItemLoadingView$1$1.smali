.class public final Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "TransactionItemLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:F


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;FFFFLandroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFFF",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->a:Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;

    iput p2, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->b:F

    iput p3, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->c:F

    iput p4, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->d:F

    iput p5, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p2, "canvas"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "itemPaint"

    invoke-static {p4, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget p2, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->b:F

    iget v0, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->c:F

    sub-float/2addr p2, v0

    iget v0, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->d:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v0, v0, v1

    sub-float/2addr p2, v0

    div-float/2addr p2, v1

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    iget v2, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    int-to-float p1, p1

    const v3, 0x3ea3d70a    # 0.32f

    mul-float v3, v3, p1

    add-float/2addr v2, v3

    iget v3, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->d:F

    add-float/2addr v3, p2

    invoke-direct {v0, v1, p2, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 35
    invoke-virtual {p3, v0, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 38
    iget v1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    sub-float v1, p1, v1

    const v2, 0x3e3851ec    # 0.18f

    mul-float v2, v2, p1

    sub-float/2addr v1, v2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    sub-float v3, p1, v3

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 39
    invoke-virtual {p3, v0, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 42
    iget v1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->d:F

    iget v2, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->c:F

    add-float/2addr v1, v2

    add-float/2addr p2, v1

    .line 43
    iget v1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    iget v2, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->e:F

    const v3, 0x3e6b851f    # 0.23f

    mul-float p1, p1, v3

    add-float/2addr v2, p1

    iget p1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;->d:F

    add-float/2addr p1, p2

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 44
    invoke-virtual {p3, v0, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-void
.end method
