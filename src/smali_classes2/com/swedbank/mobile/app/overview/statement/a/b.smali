.class public final Lcom/swedbank/mobile/app/overview/statement/a/b;
.super Ljava/lang/Object;
.source "TransactionDetailsBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/swedbank/mobile/business/overview/statement/details/f;

.field private final c:Lcom/swedbank/mobile/a/u/l/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/u/l/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/u/l/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/b;->c:Lcom/swedbank/mobile/a/u/l/a/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/statement/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/statement/a/b;

    .line 25
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/statement/a/b;->a:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/app/overview/statement/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/statement/details/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/statement/a/b;

    .line 29
    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/statement/a/b;->b:Lcom/swedbank/mobile/business/overview/statement/details/f;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/statement/a/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/statement/a/b;

    .line 21
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/statement/a/b;->a:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/b;->c:Lcom/swedbank/mobile/a/u/l/a/a$a;

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/b;->a:Lcom/swedbank/mobile/business/util/e;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/l/a/a$a;->b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/u/l/a/a$a;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/b;->b:Lcom/swedbank/mobile/business/overview/statement/details/f;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/l/a/a$a;->b(Lcom/swedbank/mobile/business/overview/statement/details/f;)Lcom/swedbank/mobile/a/u/l/a/a$a;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/l/a/a$a;->a()Lcom/swedbank/mobile/a/u/l/a/a;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/l/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display transaction details without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 33
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display transaction details without transaction data"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
