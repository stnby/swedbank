.class public final Lcom/swedbank/mobile/app/overview/statement/a/n;
.super Ljava/lang/Object;
.source "TransactionDetailsViewState.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/app/overview/statement/a/a;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Z

.field private final c:Lcom/swedbank/mobile/app/w/b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/statement/a/n;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/a;ZLcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/app/overview/statement/a/a;ZLcom/swedbank/mobile/app/w/b;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/app/overview/statement/a/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/w/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->a:Lcom/swedbank/mobile/app/overview/statement/a/a;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->c:Lcom/swedbank/mobile/app/w/b;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/app/overview/statement/a/a;ZLcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 7
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/overview/statement/a/a;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 9
    move-object p3, v0

    check-cast p3, Lcom/swedbank/mobile/app/w/b;

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/statement/a/n;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/a;ZLcom/swedbank/mobile/app/w/b;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/app/overview/statement/a/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->a:Lcom/swedbank/mobile/app/overview/statement/a/a;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->b:Z

    return v0
.end method

.method public final c()Lcom/swedbank/mobile/app/w/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->c:Lcom/swedbank/mobile/app/w/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/overview/statement/a/n;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/overview/statement/a/n;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->a:Lcom/swedbank/mobile/app/overview/statement/a/a;

    iget-object v3, p1, Lcom/swedbank/mobile/app/overview/statement/a/n;->a:Lcom/swedbank/mobile/app/overview/statement/a/a;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->b:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/overview/statement/a/n;->b:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->c:Lcom/swedbank/mobile/app/w/b;

    iget-object p1, p1, Lcom/swedbank/mobile/app/overview/statement/a/n;->c:Lcom/swedbank/mobile/app/w/b;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->a:Lcom/swedbank/mobile/app/overview/statement/a/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->b:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->c:Lcom/swedbank/mobile/app/w/b;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransactionDetailsViewState(transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->a:Lcom/swedbank/mobile/app/overview/statement/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newPaymentEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/n;->c:Lcom/swedbank/mobile/app/w/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
