.class public final Lcom/swedbank/mobile/app/overview/statement/a/e;
.super Lcom/swedbank/mobile/architect/a/d;
.source "TransactionDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/overview/statement/a/i;",
        "Lcom/swedbank/mobile/app/overview/statement/a/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/overview/statement/details/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/statement/details/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/statement/details/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/e;->a:Lcom/swedbank/mobile/business/overview/statement/details/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/statement/a/e;)Lcom/swedbank/mobile/business/overview/statement/details/d;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/statement/a/e;->a:Lcom/swedbank/mobile/business/overview/statement/details/d;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 17
    sget-object v0, Lcom/swedbank/mobile/app/overview/statement/a/e$a;->a:Lcom/swedbank/mobile/app/overview/statement/a/e$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/overview/statement/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 18
    new-instance v1, Lcom/swedbank/mobile/app/overview/statement/a/e$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/overview/statement/a/e$b;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/e;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 22
    sget-object v1, Lcom/swedbank/mobile/app/overview/statement/a/e$c;->a:Lcom/swedbank/mobile/app/overview/statement/a/e$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/overview/statement/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 23
    new-instance v2, Lcom/swedbank/mobile/app/overview/statement/a/e$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/overview/statement/a/e$d;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/e;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->n(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 28
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 29
    sget-object v3, Lcom/swedbank/mobile/app/overview/statement/a/e$e;->a:Lcom/swedbank/mobile/app/overview/statement/a/e$e;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/overview/statement/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 27
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 30
    new-instance v3, Lcom/swedbank/mobile/app/overview/statement/a/e$f;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/overview/statement/a/e$f;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/e;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 48
    check-cast v0, Lio/reactivex/s;

    .line 49
    check-cast v1, Lio/reactivex/s;

    .line 50
    check-cast v2, Lio/reactivex/s;

    .line 47
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026transactionDetailsStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
