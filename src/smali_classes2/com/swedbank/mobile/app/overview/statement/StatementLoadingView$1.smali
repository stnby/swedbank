.class final Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;
.super Lkotlin/e/b/k;
.source "StatementLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;->b:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;
    .locals 18
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "resources"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "metrics"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v2, v15, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;->b:Landroid/content/Context;

    sget v3, Lcom/swedbank/mobile/app/overview/i$a;->background_color:I

    .line 127
    invoke-static {v2, v3}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v16

    .line 31
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    const/4 v2, -0x1

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/16 v2, 0x28

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/ag;->a(I)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    sget v2, Lcom/swedbank/mobile/app/overview/i$b;->list_item_separator_horizontal_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v6, v2

    .line 34
    new-instance v8, Lcom/swedbank/mobile/core/ui/m;

    invoke-direct {v8, v0, v6, v5}, Lcom/swedbank/mobile/core/ui/m;-><init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V

    .line 38
    sget v2, Lcom/swedbank/mobile/app/overview/i$b;->overview_statement_content_horizontal_margin:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v10, v2

    const/16 v2, 0x8

    int-to-float v2, v2

    const/4 v3, 0x1

    .line 128
    invoke-static {v3, v2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v11

    .line 40
    sget v2, Lcom/swedbank/mobile/app/overview/i$b;->list_item_separator_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    const/16 v4, 0x10

    int-to-float v4, v4

    const/4 v9, 0x2

    .line 129
    invoke-static {v9, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v12

    .line 42
    sget v4, Lcom/swedbank/mobile/app/overview/i$b;->overview_statement_transaction_item_min_height:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    int-to-float v0, v3

    .line 130
    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    const/16 v0, 0xc

    int-to-float v0, v0

    .line 131
    invoke-static {v9, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    .line 46
    new-instance v17, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;

    iget-object v0, v15, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView;

    move-object v14, v0

    check-cast v14, Landroid/view/View;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move v9, v12

    move v12, v13

    move/from16 v13, v16

    move/from16 v15, v16

    invoke-direct/range {v0 .. v15}, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;FFFLandroid/graphics/Paint;FLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFILandroid/view/View;I)V

    return-object v17
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
