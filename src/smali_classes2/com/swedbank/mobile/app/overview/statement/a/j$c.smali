.class public final Lcom/swedbank/mobile/app/overview/statement/a/j$c;
.super Lkotlin/e/b/k;
.source "TransactionDetailsViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/statement/a/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/core/ui/widget/s;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/statement/a/j;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/overview/statement/a/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j$c;->a:Lcom/swedbank/mobile/app/overview/statement/a/j;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/statement/a/j$c;->b:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/widget/s;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j$c;->b:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    .line 179
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/statement/a/j$c;->a:Lcom/swedbank/mobile/app/overview/statement/a/j;

    invoke-static {v2}, Lcom/swedbank/mobile/app/overview/statement/a/j;->c(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_retry_btn:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/swedbank/mobile/core/ui/widget/s$c;

    const/4 v2, 0x0

    .line 209
    check-cast v2, Ljava/lang/CharSequence;

    .line 212
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/s;->a(Ljava/lang/CharSequence;)Lcom/swedbank/mobile/core/ui/widget/s;

    .line 213
    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/core/ui/widget/s;->b(Ljava/lang/CharSequence;)Lcom/swedbank/mobile/core/ui/widget/s;

    .line 214
    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/core/ui/widget/s;->a(Lcom/swedbank/mobile/core/ui/widget/s$c;)Lcom/swedbank/mobile/core/ui/widget/s;

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/statement/a/j$c;->a(Lcom/swedbank/mobile/core/ui/widget/s;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
