.class public final Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "StatementLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Landroid/graphics/Paint;

.field final synthetic f:F

.field final synthetic g:Landroid/graphics/Paint;

.field final synthetic h:Lcom/swedbank/mobile/core/ui/m;

.field final synthetic i:F

.field final synthetic j:F

.field final synthetic k:F

.field final synthetic l:F

.field final synthetic m:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;FFFLandroid/graphics/Paint;FLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFILandroid/view/View;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFF",
            "Landroid/graphics/Paint;",
            "F",
            "Landroid/graphics/Paint;",
            "Lcom/swedbank/mobile/core/ui/m;",
            "FFFFI",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    .line 46
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->a:Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1;

    iput p2, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->b:F

    iput p3, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->c:F

    iput p4, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->d:F

    iput-object p5, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->e:Landroid/graphics/Paint;

    iput p6, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->f:F

    iput-object p7, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->g:Landroid/graphics/Paint;

    iput-object p8, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->h:Lcom/swedbank/mobile/core/ui/m;

    iput p9, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->i:F

    iput p10, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    iput p11, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->k:F

    iput p12, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->l:F

    iput p13, p0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->m:I

    const/4 p4, 0x0

    const/4 p5, 0x0

    const/4 p6, 0x0

    const/16 p7, 0x1c

    const/4 p8, 0x0

    move-object p1, p0

    move-object p2, p14

    move p3, p15

    invoke-direct/range {p1 .. p8}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;)V
    .locals 16
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    const-string v3, "canvas"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v3, v1

    .line 49
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 57
    :goto_0
    iget v7, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->b:F

    .line 59
    iget v8, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->c:F

    .line 60
    iget v9, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->d:F

    .line 63
    iget-object v10, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->e:Landroid/graphics/Paint;

    .line 64
    iget v11, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->f:F

    .line 65
    iget-object v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->g:Landroid/graphics/Paint;

    .line 66
    iget-object v13, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->h:Lcom/swedbank/mobile/core/ui/m;

    add-float/2addr v7, v6

    .line 126
    invoke-virtual {v13}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v14

    add-float/2addr v14, v7

    add-float v15, v14, v9

    .line 127
    invoke-virtual {v4, v5, v14, v3, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 128
    invoke-virtual {v2, v4, v12}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v15, 0x0

    :goto_1
    const/4 v14, 0x1

    if-ge v15, v14, :cond_0

    .line 131
    iget v14, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v14, v8

    add-float v0, v14, v9

    .line 132
    invoke-virtual {v4, v5, v14, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 133
    invoke-virtual {v2, v4, v12}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    goto :goto_1

    :cond_0
    float-to-int v0, v7

    const/4 v12, 0x0

    .line 140
    invoke-virtual {v13, v12, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->a(IIILandroid/graphics/Canvas;)V

    .line 147
    invoke-virtual {v13}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v0

    add-float/2addr v7, v0

    iput v7, v4, Landroid/graphics/RectF;->bottom:F

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v14, :cond_1

    .line 149
    iget v7, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v7, v9

    sub-float v12, v3, v11

    add-float v15, v7, v8

    .line 150
    invoke-virtual {v4, v11, v7, v12, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 151
    invoke-virtual {v2, v4, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 155
    :cond_1
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v9

    float-to-int v7, v0

    const/4 v8, 0x0

    .line 156
    invoke-virtual {v13, v8, v7, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->b(IIILandroid/graphics/Canvas;)V

    .line 162
    invoke-virtual {v13}, Lcom/swedbank/mobile/core/ui/m;->b()F

    move-result v7

    add-float/2addr v0, v7

    add-float/2addr v6, v0

    move/from16 v0, p2

    int-to-float v7, v0

    cmpg-float v7, v6, v7

    if-ltz v7, :cond_2

    return-void

    :cond_2
    move-object/from16 v0, p0

    goto :goto_0
.end method

.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 16
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    const-string v3, "canvas"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "itemPaint"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    const/4 v4, 0x0

    .line 79
    :cond_0
    iget v5, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->b:F

    .line 80
    iget v6, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->i:F

    .line 81
    iget v7, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    sub-float/2addr v5, v6

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v5, v8

    add-float/2addr v5, v4

    move/from16 v9, p1

    int-to-float v10, v9

    const v11, 0x3df5c28f    # 0.12f

    mul-float v11, v11, v10

    add-float/2addr v11, v7

    add-float/2addr v6, v5

    .line 164
    invoke-virtual {v3, v7, v5, v11, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 165
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 86
    iget v5, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->b:F

    add-float/2addr v5, v4

    iget-object v6, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->h:Lcom/swedbank/mobile/core/ui/m;

    invoke-virtual {v6}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x2

    :goto_0
    if-ge v6, v7, :cond_1

    .line 89
    iget v11, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->d:F

    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->k:F

    sub-float/2addr v11, v12

    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->l:F

    mul-float v12, v12, v8

    sub-float/2addr v11, v12

    div-float/2addr v11, v8

    add-float/2addr v11, v5

    .line 90
    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    iget v13, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    const v14, 0x3ea3d70a    # 0.32f

    mul-float v14, v14, v10

    add-float/2addr v13, v14

    iget v14, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->l:F

    add-float/2addr v14, v11

    invoke-virtual {v3, v12, v11, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 91
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 94
    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    sub-float v12, v10, v12

    const v13, 0x3e3851ec    # 0.18f

    mul-float v13, v13, v10

    sub-float/2addr v12, v13

    iget v13, v3, Landroid/graphics/RectF;->top:F

    iget v14, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    sub-float v14, v10, v14

    iget v15, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v12, v13, v14, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 95
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 98
    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->l:F

    iget v13, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->k:F

    add-float/2addr v12, v13

    add-float/2addr v11, v12

    .line 99
    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    iget v13, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->j:F

    const v14, 0x3e6b851f    # 0.23f

    mul-float v14, v14, v10

    add-float/2addr v13, v14

    iget v14, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->l:F

    add-float/2addr v14, v11

    invoke-virtual {v3, v12, v11, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 100
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 102
    iget v11, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->d:F

    iget v12, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->c:F

    add-float/2addr v11, v12

    add-float/2addr v5, v11

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 105
    :cond_1
    iget-object v6, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->h:Lcom/swedbank/mobile/core/ui/m;

    invoke-virtual {v6}, Lcom/swedbank/mobile/core/ui/m;->b()F

    move-result v6

    add-float/2addr v5, v6

    iget v6, v0, Lcom/swedbank/mobile/app/overview/statement/StatementLoadingView$1$1;->c:F

    sub-float/2addr v5, v6

    add-float/2addr v4, v5

    move/from16 v5, p2

    int-to-float v6, v5

    cmpg-float v6, v4, v6

    if-ltz v6, :cond_0

    return-void
.end method
