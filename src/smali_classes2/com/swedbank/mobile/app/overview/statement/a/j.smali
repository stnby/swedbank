.class public final Lcom/swedbank/mobile/app/overview/statement/a/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "TransactionDetailsViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/statement/a/i;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/text/SimpleDateFormat;

.field private i:Lcom/swedbank/mobile/core/ui/widget/t;

.field private final j:Lcom/swedbank/mobile/core/ui/widget/k;

.field private final k:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/statement/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/statement/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "transactionDetailsList"

    const-string v4, "getTransactionDetailsList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/statement/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/statement/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/statement/a/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/statement/a/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->k:Lio/reactivex/o;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->b:Lkotlin/f/c;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->transaction_details_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->c:Lkotlin/f/c;

    .line 43
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->transaction_details_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->d:Lkotlin/f/c;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->transaction_details_retry_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->e:Lkotlin/f/c;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->transaction_details_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->f:Lkotlin/f/c;

    .line 47
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->g:Lcom/b/c/c;

    .line 49
    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string v0, "dd.MM.yyyy (EEEE)"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->h:Ljava/text/SimpleDateFormat;

    .line 51
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/k;

    const/4 v0, 0x2

    .line 52
    new-array v0, v0, [Lkotlin/k;

    const/4 v1, 0x0

    .line 53
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 54
    sget v3, Lcom/swedbank/mobile/app/overview/i$e;->item_transaction_detail:I

    .line 55
    sget-object v4, Lcom/swedbank/mobile/app/overview/statement/a/j$d;->a:Lcom/swedbank/mobile/app/overview/statement/a/j$d;

    check-cast v4, Lkotlin/e/a/m;

    .line 53
    invoke-static {v3, v4}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 60
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 61
    sget v3, Lcom/swedbank/mobile/app/overview/i$e;->item_transaction_action_buttons:I

    .line 62
    new-instance v4, Lcom/swedbank/mobile/app/overview/statement/a/j$e;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/overview/statement/a/j$e;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/j;)V

    check-cast v4, Lkotlin/e/a/m;

    .line 60
    invoke-static {v3, v4}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, v0, v1

    .line 52
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object v0

    const/4 v2, 0x0

    .line 51
    invoke-direct {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->j:Lcom/swedbank/mobile/core/ui/widget/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroidx/appcompat/widget/Toolbar;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->f()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/content/Context;
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/statement/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/statement/a/j;)Ljava/text/SimpleDateFormat;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->h:Ljava/text/SimpleDateFormat;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/swedbank/mobile/core/ui/widget/k;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->j:Lcom/swedbank/mobile/core/ui/widget/k;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/view/View;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->g()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/statement/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final g()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/statement/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->i:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez p0, :cond_0

    const-string v0, "snackbarRenderer"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method private final h()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/statement/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/widget/TextView;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->h()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final i()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/statement/a/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/b/c/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->g:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 82
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->i()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 212
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    check-cast v1, Lio/reactivex/s;

    .line 83
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->i:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v2, "snackbarRenderer"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 81
    invoke-static {v1, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/statement/a/n;)V
    .locals 12
    .param p1    # Lcom/swedbank/mobile/app/overview/statement/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/statement/a/n;->a()Lcom/swedbank/mobile/app/overview/statement/a/a;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->h()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 213
    :goto_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->a(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v2

    if-eqz v0, :cond_1

    .line 214
    sget v0, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_mass_payment_title:I

    goto :goto_1

    .line 215
    :cond_1
    sget v0, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_title:I

    .line 213
    :goto_1
    invoke-virtual {v2, v0}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 91
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/statement/a/n;->a()Lcom/swedbank/mobile/app/overview/statement/a/a;

    move-result-object v0

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/statement/a/n;->b()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_10

    .line 222
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->b(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 224
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->c(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/content/Context;

    move-result-object v5

    .line 225
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 226
    new-instance v7, Ljava/util/ArrayList;

    const/4 v8, 0x6

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 227
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->g()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v8

    sget-object v9, Lcom/swedbank/mobile/app/overview/statement/a/k;->a:[I

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/overview/Transaction$Direction;->ordinal()I

    move-result v8

    aget v8, v9, v8

    if-eq v8, v4, :cond_7

    .line 286
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_date:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->d(Lcom/swedbank/mobile/app/overview/statement/a/j;)Ljava/text/SimpleDateFormat;

    move-result-object v9

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->a()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v8

    .line 288
    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v9, v8, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 286
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_beneficiary_name:I

    .line 291
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->b()Ljava/lang/String;

    move-result-object v9

    .line 293
    move-object v10, v9

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-lez v10, :cond_2

    const/4 v10, 0x1

    goto :goto_2

    :cond_2
    const/4 v10, 0x0

    :goto_2
    if-eqz v10, :cond_3

    .line 294
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v8

    .line 295
    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v9, v8, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 294
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    :cond_3
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_beneficiary_account:I

    .line 308
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->c()Ljava/lang/String;

    move-result-object v9

    .line 310
    move-object v10, v9

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-lez v10, :cond_4

    const/4 v10, 0x1

    goto :goto_3

    :cond_4
    const/4 v10, 0x0

    :goto_3
    if-eqz v10, :cond_5

    .line 311
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v8

    .line 312
    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v9, v8, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 311
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    :cond_5
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_amount:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->d()Ljava/math/BigDecimal;

    move-result-object v9

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->g()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v11

    invoke-static {v5, v9, v10, v11}, Lcom/swedbank/mobile/util/a;->a(Landroid/content/Context;Ljava/math/BigDecimal;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Direction;)Landroid/text/SpannedString;

    move-result-object v5

    invoke-static {v8, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v5

    .line 325
    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v8, v5, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 323
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    sget v5, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_description:I

    .line 328
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->f()Ljava/lang/String;

    move-result-object v8

    .line 330
    move-object v9, v8

    check-cast v9, Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-lez v9, :cond_6

    const/4 v9, 0x1

    goto :goto_4

    :cond_6
    const/4 v9, 0x0

    :goto_4
    if-eqz v9, :cond_d

    .line 331
    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v8}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v5

    .line 332
    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v8, v5, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 331
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 229
    :cond_7
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_date:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->d(Lcom/swedbank/mobile/app/overview/statement/a/j;)Ljava/text/SimpleDateFormat;

    move-result-object v9

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->a()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v8

    .line 231
    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v9, v8, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 229
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_payer_name:I

    .line 234
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->b()Ljava/lang/String;

    move-result-object v9

    .line 236
    move-object v10, v9

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-lez v10, :cond_8

    const/4 v10, 0x1

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    :goto_5
    if-eqz v10, :cond_9

    .line 237
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v8

    .line 238
    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v9, v8, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 237
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    :cond_9
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_payer_account:I

    .line 251
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->c()Ljava/lang/String;

    move-result-object v9

    .line 253
    move-object v10, v9

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-lez v10, :cond_a

    const/4 v10, 0x1

    goto :goto_6

    :cond_a
    const/4 v10, 0x0

    :goto_6
    if-eqz v10, :cond_b

    .line 254
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v9}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v8

    .line 255
    new-instance v9, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v9, v8, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 254
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    :cond_b
    sget v8, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_amount:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->d()Ljava/math/BigDecimal;

    move-result-object v9

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->g()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v11

    invoke-static {v5, v9, v10, v11}, Lcom/swedbank/mobile/util/a;->a(Landroid/content/Context;Ljava/math/BigDecimal;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Direction;)Landroid/text/SpannedString;

    move-result-object v5

    invoke-static {v8, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v5

    .line 268
    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v8, v5, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 266
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    sget v5, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_description:I

    .line 271
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->f()Ljava/lang/String;

    move-result-object v8

    .line 273
    move-object v9, v8

    check-cast v9, Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-lez v9, :cond_c

    const/4 v9, 0x1

    goto :goto_7

    :cond_c
    const/4 v9, 0x0

    :goto_7
    if-eqz v9, :cond_d

    .line 274
    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v8}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v5

    .line 275
    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v8, v5, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 274
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    :cond_d
    :goto_8
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->h()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 345
    sget v5, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_number_of_payments:I

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/statement/a/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    .line 347
    new-instance v5, Lcom/swedbank/mobile/core/ui/widget/i;

    invoke-direct {v5, v0, v1}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 345
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    if-eqz v2, :cond_f

    .line 349
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/i;

    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-direct {v0, v2, v4}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_f
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 352
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->e(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/swedbank/mobile/core/ui/widget/k;

    move-result-object v0

    check-cast v7, Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v0, v7, v3, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    .line 353
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 354
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_9

    .line 356
    :cond_10
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->b(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 94
    :goto_9
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/statement/a/n;->c()Lcom/swedbank/mobile/app/w/b;

    move-result-object v0

    .line 95
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/statement/a/n;->a()Lcom/swedbank/mobile/app/overview/statement/a/a;

    move-result-object p1

    if-eqz p1, :cond_11

    goto :goto_a

    :cond_11
    const/4 v4, 0x0

    :goto_a
    if-eqz v0, :cond_12

    .line 363
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p1

    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->c(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_12
    const/16 p1, 0x8

    if-eqz v3, :cond_14

    if-eqz v4, :cond_13

    .line 367
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->f(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 368
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->g(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p1

    .line 369
    new-instance v0, Lcom/swedbank/mobile/app/overview/statement/a/j$b;

    invoke-direct {v0, v3}, Lcom/swedbank/mobile/app/overview/statement/a/j$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    .line 370
    new-instance v1, Lcom/swedbank/mobile/app/overview/statement/a/j$c;

    invoke-direct {v1, p0, v3}, Lcom/swedbank/mobile/app/overview/statement/a/j$c;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/j;Ljava/lang/String;)V

    check-cast v1, Lkotlin/e/a/b;

    .line 368
    invoke-virtual {p1, v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V

    goto :goto_b

    .line 378
    :cond_13
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->g(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 379
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->f(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 380
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->h(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/widget/TextView;

    move-result-object p1

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    .line 384
    :cond_14
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->f(Lcom/swedbank/mobile/app/overview/statement/a/j;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 385
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->g(Lcom/swedbank/mobile/app/overview/statement/a/j;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 96
    :goto_b
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/app/overview/statement/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/statement/a/j;->a(Lcom/swedbank/mobile/app/overview/statement/a/n;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->k:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->g:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method protected e()V
    .locals 9

    .line 209
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 210
    new-instance v1, Lcom/swedbank/mobile/app/overview/statement/a/j$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/overview/statement/a/j$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/overview/statement/a/l;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/statement/a/l;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 211
    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->i:Lcom/swedbank/mobile/core/ui/widget/t;

    .line 71
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/statement/a/j;->f()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    .line 72
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 73
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/f;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v2, "context"

    invoke-static {v3, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/g$c;

    const/4 v4, 0x1

    new-array v5, v4, [I

    const/4 v6, 0x0

    aput v4, v5, v6

    invoke-direct {v2, v5}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v6, v2

    check-cast v6, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v2, v1

    .line 73
    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 75
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j;->j:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method
