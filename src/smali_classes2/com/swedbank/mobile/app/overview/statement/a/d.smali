.class public final Lcom/swedbank/mobile/app/overview/statement/a/d;
.super Ljava/lang/Object;
.source "TransactionDetails.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/statement/a/a;
    .locals 12
    .param p0    # Lcom/swedbank/mobile/business/overview/Transaction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toDetailsViewModel"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/swedbank/mobile/app/overview/statement/a/a;

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->c()Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->e()Ljava/util/Date;

    move-result-object v3

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->h()Ljava/lang/String;

    move-result-object v4

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->i()Ljava/lang/String;

    move-result-object v5

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->f()Ljava/math/BigDecimal;

    move-result-object v6

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->g()Ljava/lang/String;

    move-result-object v7

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->j()Ljava/lang/String;

    move-result-object v8

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->l()Lcom/swedbank/mobile/business/overview/Transaction$Direction;

    move-result-object v9

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->a()Z

    move-result v10

    .line 32
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/overview/Transaction;->m()I

    move-result v11

    move-object v1, v0

    .line 22
    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/app/overview/statement/a/a;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/business/overview/Transaction$Direction;ZI)V

    return-object v0
.end method
