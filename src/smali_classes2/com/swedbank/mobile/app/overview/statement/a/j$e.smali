.class final Lcom/swedbank/mobile/app/overview/statement/a/j$e;
.super Lkotlin/e/b/k;
.source "TransactionDetailsViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/statement/a/j;-><init>(Lio/reactivex/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lkotlin/s;",
        ">;",
        "Lkotlin/s;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/statement/a/j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/statement/a/j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/a/j$e;->a:Lcom/swedbank/mobile/app/overview/statement/a/j;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lkotlin/s;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/statement/a/j$e;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lkotlin/s;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lkotlin/s;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lkotlin/s;",
            ">;",
            "Lkotlin/s;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget p2, Lcom/swedbank/mobile/app/overview/i$d;->transaction_details_new_payment_btn:I

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object p1

    .line 209
    new-instance p2, Lcom/swedbank/mobile/app/overview/statement/a/j$e$a;

    invoke-direct {p2, p0}, Lcom/swedbank/mobile/app/overview/statement/a/j$e$a;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/j$e;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
