.class final Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;
.super Ljava/lang/Object;
.source "TransactionDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/statement/a/e$f;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;->a:Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lcom/swedbank/mobile/app/overview/statement/a/n;
    .locals 13
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/overview/Transaction;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/swedbank/mobile/app/overview/statement/a/n;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 36
    instance-of p1, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/app/overview/statement/a/n;

    .line 37
    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/overview/Transaction;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/statement/a/d;->a(Lcom/swedbank/mobile/business/overview/Transaction;)Lcom/swedbank/mobile/app/overview/statement/a/a;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    .line 36
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/overview/statement/a/n;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/a;ZLcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    goto :goto_0

    .line 39
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/app/overview/statement/a/n;

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v0, Lcom/swedbank/mobile/app/w/b$d;

    .line 40
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    check-cast v1, Ljava/lang/Throwable;

    .line 41
    sget v2, Lcom/swedbank/mobile/app/overview/i$f;->transaction_details_loading_technical_error:I

    .line 39
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/app/w/b$d;-><init>(Ljava/lang/Throwable;I)V

    move-object v10, v0

    check-cast v10, Lcom/swedbank/mobile/app/w/b;

    const/4 v11, 0x3

    const/4 v12, 0x0

    move-object v7, p1

    invoke-direct/range {v7 .. v12}, Lcom/swedbank/mobile/app/overview/statement/a/n;-><init>(Lcom/swedbank/mobile/app/overview/statement/a/a;ZLcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/statement/a/e$f$1;->a(Lkotlin/k;)Lcom/swedbank/mobile/app/overview/statement/a/n;

    move-result-object p1

    return-object p1
.end method
