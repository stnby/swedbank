.class final Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;
.super Lkotlin/e/b/k;
.source "TransactionItemLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;
    .locals 8
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "metrics"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget v0, Lcom/swedbank/mobile/app/overview/i$b;->overview_statement_content_horizontal_margin:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v6, v0

    const/16 v0, 0x8

    int-to-float v0, v0

    const/4 v1, 0x1

    .line 64
    invoke-static {v1, v0, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    .line 27
    sget v0, Lcom/swedbank/mobile/app/overview/i$b;->overview_statement_transaction_item_min_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float v3, p1

    const/16 p1, 0xc

    int-to-float p1, p1

    const/4 v0, 0x2

    .line 65
    invoke-static {v0, p1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    .line 30
    new-instance p1, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;

    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView;

    move-object v7, p2

    check-cast v7, Landroid/view/View;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;FFFFLandroid/view/View;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/statement/TransactionItemLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
