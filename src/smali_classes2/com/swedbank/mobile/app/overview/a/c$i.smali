.class final Lcom/swedbank/mobile/app/overview/a/c$i;
.super Ljava/lang/Object;
.source "OverviewCombinedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/a/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/c$i;->a:Lcom/swedbank/mobile/app/overview/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 5
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/a/n$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "forceShowLoading"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/c$i;->a:Lcom/swedbank/mobile/app/overview/a/c;

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/c$i;->a:Lcom/swedbank/mobile/app/overview/a/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/a/c;->a(Lcom/swedbank/mobile/app/overview/a/c;)Lcom/swedbank/mobile/business/overview/combined/a;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/combined/a;->a()Lio/reactivex/w;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/swedbank/mobile/app/overview/a/c$i$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/overview/a/c$i$1;-><init>(Lcom/swedbank/mobile/app/overview/a/c$i;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/a/c$i;->a:Lcom/swedbank/mobile/app/overview/a/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/overview/a/c;->b(Lcom/swedbank/mobile/app/overview/a/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 64
    new-instance v2, Lcom/swedbank/mobile/app/overview/a/n$a$e;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/overview/a/n$a$e;-><init>(Z)V

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 62
    invoke-static {v1, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v0, p1}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "interactor\n             \u2026oading))\n              ))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/swedbank/mobile/app/overview/a/c$m;->a:Lcom/swedbank/mobile/app/overview/a/c$m;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/a/c$i;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
