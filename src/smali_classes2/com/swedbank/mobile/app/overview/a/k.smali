.class public final Lcom/swedbank/mobile/app/overview/a/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OverviewCombinedViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/a/j;
.implements Lcom/swedbank/mobile/architect/a/b/i;
.implements Lcom/swedbank/mobile/core/ui/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/b/a;",
        "Lcom/swedbank/mobile/app/overview/a/j;",
        "Lcom/swedbank/mobile/architect/a/b/i<",
        "Lcom/swedbank/mobile/app/overview/a/n;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/ao<",
        "Lcom/swedbank/mobile/app/overview/a/n;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private e:Lcom/swedbank/mobile/core/ui/widget/t;

.field private f:Lcom/swedbank/mobile/core/ui/widget/t;

.field private final g:Lcom/swedbank/mobile/core/ui/aj;

.field private final h:Lcom/swedbank/mobile/business/navigation/i;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "overviewList"

    const-string v4, "getOverviewList()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/a/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/business/navigation/i;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/aj;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/navigation/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/aj;",
            "Lcom/swedbank/mobile/business/navigation/i;",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transitionAwareRenderer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationItemReselectStream"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProviders"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/k;->g:Lcom/swedbank/mobile/core/ui/aj;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/a/k;->h:Lcom/swedbank/mobile/business/navigation/i;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/a/k;->i:Ljava/util/List;

    .line 38
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/k;->b:Lkotlin/f/c;

    .line 39
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_combined_refresh:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/k;->c:Lkotlin/f/c;

    .line 40
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_combined_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/k;->d:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/a/k;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->f()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/a/k;)Landroid/content/Context;
    .locals 0

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/a/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/a/k;->f:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez p0, :cond_0

    const-string v0, "olderItemLoadErrorRenderer"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/a/k;->e:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez p0, :cond_0

    const-string v0, "mainErrorRenderer"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method private final f()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/a/n;)V
    .locals 18
    .param p1    # Lcom/swedbank/mobile/app/overview/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const-string v1, "viewState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/a/n;->b()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 74
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/a/n;->a()Z

    move-result v1

    .line 137
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->b(Lcom/swedbank/mobile/app/overview/a/k;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v3

    .line 138
    invoke-virtual {v3, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    const/4 v1, 0x1

    .line 139
    invoke-virtual {v3, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 76
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/a/n;->c()Lcom/swedbank/mobile/business/util/e;

    move-result-object v3

    .line 77
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/a/n;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object v4

    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/overview/a/n;->e()Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    const/4 v5, 0x0

    if-eqz v3, :cond_3

    .line 149
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->c(Lcom/swedbank/mobile/app/overview/a/k;)Landroid/content/Context;

    move-result-object v6

    .line 150
    sget v7, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_general_loading_error:I

    .line 153
    instance-of v8, v3, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v8, :cond_1

    check-cast v3, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 155
    move-object v8, v3

    check-cast v8, Ljava/util/Collection;

    invoke-interface {v8}, Ljava/util/Collection;->isEmpty()Z

    move-result v8

    xor-int/2addr v8, v1

    if-eqz v8, :cond_0

    move-object v9, v3

    check-cast v9, Ljava/lang/Iterable;

    const-string v3, "\n"

    move-object v10, v3

    check-cast v10, Ljava/lang/CharSequence;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x3e

    const/16 v17, 0x0

    invoke-static/range {v9 .. v17}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 156
    :cond_0
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 158
    :cond_1
    instance-of v7, v3, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v7, :cond_2

    check-cast v3, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    .line 159
    invoke-static {v3}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v6, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v3, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const-string v6, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 160
    invoke-static {v3, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    goto :goto_2

    .line 159
    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_3
    if-eqz v2, :cond_4

    .line 164
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->c(Lcom/swedbank/mobile/app/overview/a/k;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    move-object v2, v5

    :goto_1
    move-object v3, v2

    check-cast v3, Ljava/lang/CharSequence;

    :goto_2
    if-eqz v3, :cond_5

    .line 166
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->d(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 167
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->e(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    .line 168
    new-instance v2, Lcom/swedbank/mobile/app/overview/a/k$d;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/overview/a/k$d;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 169
    new-instance v4, Lcom/swedbank/mobile/app/overview/a/k$e;

    invoke-direct {v4, v0, v3}, Lcom/swedbank/mobile/app/overview/a/k$e;-><init>(Lcom/swedbank/mobile/app/overview/a/k;Ljava/lang/CharSequence;)V

    check-cast v4, Lkotlin/e/a/b;

    .line 167
    invoke-virtual {v1, v2, v4}, Lcom/swedbank/mobile/core/ui/widget/t;->a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V

    goto/16 :goto_5

    .line 176
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->e(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    if-eqz v4, :cond_9

    .line 178
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->c(Lcom/swedbank/mobile/app/overview/a/k;)Landroid/content/Context;

    move-result-object v2

    .line 179
    sget v3, Lcom/swedbank/mobile/app/overview/i$f;->overview_statement_general_loading_error:I

    .line 182
    instance-of v5, v4, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v5, :cond_7

    check-cast v4, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 184
    move-object v5, v4

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v1, v5

    if-eqz v1, :cond_6

    move-object v5, v4

    check-cast v5, Ljava/lang/Iterable;

    const-string v1, "\n"

    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 185
    :cond_6
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 187
    :cond_7
    instance-of v1, v4, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_8

    check-cast v4, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 188
    invoke-static {v1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    const-string v2, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 189
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v1

    check-cast v5, Ljava/lang/CharSequence;

    goto :goto_4

    .line 188
    :cond_8
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_9
    :goto_4
    if-eqz v5, :cond_a

    .line 192
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->d(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    .line 193
    new-instance v2, Lcom/swedbank/mobile/app/overview/a/k$f;

    invoke-direct {v2, v5}, Lcom/swedbank/mobile/app/overview/a/k$f;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 194
    new-instance v3, Lcom/swedbank/mobile/app/overview/a/k$g;

    invoke-direct {v3, v0, v5}, Lcom/swedbank/mobile/app/overview/a/k$g;-><init>(Lcom/swedbank/mobile/app/overview/a/k;Ljava/lang/CharSequence;)V

    check-cast v3, Lkotlin/e/a/b;

    .line 192
    invoke-virtual {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/t;->a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V

    goto :goto_5

    .line 201
    :cond_a
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/overview/a/k;->d(Lcom/swedbank/mobile/app/overview/a/k;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :goto_5
    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/swedbank/mobile/app/overview/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/a/k;->b(Lcom/swedbank/mobile/app/overview/a/n;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->f()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 65
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/a/k;->e:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v1, :cond_0

    const-string v2, "mainErrorRenderer"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 63
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/app/overview/a/n;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->g:Lcom/swedbank/mobile/core/ui/aj;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/ui/aj;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/swedbank/mobile/app/overview/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/a/k;->a(Lcom/swedbank/mobile/app/overview/a/n;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->c()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 69
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/a/k;->f:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v1, :cond_0

    const-string v2, "olderItemLoadErrorRenderer"

    invoke-static {v2}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 67
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026observeActionClicks()\n  )"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected e()V
    .locals 8

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->g:Lcom/swedbank/mobile/core/ui/aj;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v2, v4}, Lcom/swedbank/mobile/core/ui/aj$a;->a(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/architect/a/b/a;ZILjava/lang/Object;)V

    .line 129
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 130
    new-instance v1, Lcom/swedbank/mobile/app/overview/a/k$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/overview/a/k$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v4, Lcom/swedbank/mobile/app/overview/a/l;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/app/overview/a/l;-><init>(Lkotlin/e/a/a;)V

    check-cast v4, Lio/reactivex/c/a;

    invoke-static {v4}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v4, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 131
    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->e:Lcom/swedbank/mobile/core/ui/widget/t;

    .line 132
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 133
    new-instance v1, Lcom/swedbank/mobile/app/overview/a/k$b;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/overview/a/k$b;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v4, Lcom/swedbank/mobile/app/overview/a/l;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/app/overview/a/l;-><init>(Lkotlin/e/a/a;)V

    check-cast v4, Lio/reactivex/c/a;

    invoke-static {v4}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v4, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 134
    iput-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->f:Lcom/swedbank/mobile/core/ui/widget/t;

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->f()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 50
    new-array v1, v2, [I

    sget v2, Lcom/swedbank/mobile/app/overview/i$a;->brand_orange:I

    aput v2, v1, v3

    sget v2, Lcom/swedbank/mobile/app/overview/i$a;->brand_turqoise:I

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 51
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewStartOffset()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v2

    float-to-int v2, v4

    invoke-virtual {v0, v3, v1, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZII)V

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/a/k;->i:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Ljava/util/List;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/a/k;->g()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/k;->h:Lcom/swedbank/mobile/business/navigation/i;

    const-string v1, "feature_overview"

    .line 56
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/navigation/i;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v2

    .line 57
    new-instance v0, Lcom/swedbank/mobile/app/overview/a/k$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/overview/a/k$c;-><init>(Lcom/swedbank/mobile/app/overview/a/k;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 135
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void
.end method
