.class public final Lcom/swedbank/mobile/app/overview/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "OverviewCombinedPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/overview/a/j;",
        "Lcom/swedbank/mobile/app/overview/a/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/business/overview/combined/a;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/swedbank/mobile/business/overview/combined/a;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/overview/combined/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;",
            "Lcom/swedbank/mobile/business/overview/combined/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataMappers"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/c;->b:Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/a/c;->c:Lcom/swedbank/mobile/business/overview/combined/a;

    .line 24
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/c;->a:Lcom/swedbank/mobile/core/ui/x;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/a/c;)Lcom/swedbank/mobile/business/overview/combined/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/a/c;->c:Lcom/swedbank/mobile/business/overview/combined/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/a/c;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/a/c;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/c;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 28
    invoke-interface {v0}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v0

    .line 29
    sget-object v1, Lcom/swedbank/mobile/app/overview/a/c$j;->a:Lcom/swedbank/mobile/app/overview/a/c$j;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/overview/a/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/a/f;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/a/c;->c:Lcom/swedbank/mobile/business/overview/combined/a;

    .line 32
    invoke-interface {v1}, Lcom/swedbank/mobile/business/overview/combined/a;->c()Lio/reactivex/o;

    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/a/c;->b:Ljava/util/Map;

    .line 136
    new-instance v3, Lcom/swedbank/mobile/app/overview/a/c$a;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/a/c$a;-><init>(Ljava/util/Map;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 137
    new-instance v2, Lcom/swedbank/mobile/app/overview/a/c$b;

    invoke-direct {v2}, Lcom/swedbank/mobile/app/overview/a/c$b;-><init>()V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "map { it.toMappedData(da\u2026t, hasMoreToLoad) }\n    }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    sget-object v2, Lcom/swedbank/mobile/app/overview/a/c$m;->a:Lcom/swedbank/mobile/app/overview/a/c$m;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 47
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 48
    sget-object v4, Lcom/swedbank/mobile/app/overview/a/c$f;->a:Lcom/swedbank/mobile/app/overview/a/c$f;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/overview/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    sget-object v5, Lcom/swedbank/mobile/app/overview/a/c$g;->a:Lcom/swedbank/mobile/app/overview/a/c$g;

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    check-cast v4, Lio/reactivex/s;

    .line 49
    iget-object v5, p0, Lcom/swedbank/mobile/app/overview/a/c;->c:Lcom/swedbank/mobile/business/overview/combined/a;

    invoke-interface {v5}, Lcom/swedbank/mobile/business/overview/combined/a;->e()Lio/reactivex/o;

    move-result-object v5

    sget-object v6, Lcom/swedbank/mobile/app/overview/a/c$h;->a:Lcom/swedbank/mobile/app/overview/a/c$h;

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v5

    check-cast v5, Lio/reactivex/s;

    .line 46
    invoke-static {v3, v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v3

    .line 50
    new-instance v4, Lcom/swedbank/mobile/app/overview/a/c$i;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/overview/a/c$i;-><init>(Lcom/swedbank/mobile/app/overview/a/c;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 69
    sget-object v4, Lcom/swedbank/mobile/app/overview/a/c$k;->a:Lcom/swedbank/mobile/app/overview/a/c$k;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/overview/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 70
    new-instance v5, Lcom/swedbank/mobile/app/overview/a/c$l;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/overview/a/c$l;-><init>(Lcom/swedbank/mobile/app/overview/a/c;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    .line 84
    sget-object v5, Lcom/swedbank/mobile/app/overview/a/c$d;->a:Lcom/swedbank/mobile/app/overview/a/c$d;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/overview/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 85
    new-instance v6, Lcom/swedbank/mobile/app/overview/a/c$e;

    iget-object v7, p0, Lcom/swedbank/mobile/app/overview/a/c;->c:Lcom/swedbank/mobile/business/overview/combined/a;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/overview/a/c$e;-><init>(Lcom/swedbank/mobile/business/overview/combined/a;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/overview/a/e;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/overview/a/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 86
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 87
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    const/4 v6, 0x5

    .line 89
    new-array v6, v6, [Lio/reactivex/s;

    .line 90
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v6, v2

    const/4 v0, 0x1

    .line 91
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v6, v0

    const/4 v0, 0x2

    .line 92
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v6, v0

    const/4 v0, 0x3

    .line 93
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v6, v0

    const/4 v0, 0x4

    .line 94
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v6, v0

    .line 89
    invoke-static {v6}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026m,\n        actionsStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v1, Lcom/swedbank/mobile/app/overview/a/n;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/swedbank/mobile/app/overview/a/n;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/overview/a/c$c;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/overview/a/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/overview/a/c$c;-><init>(Lcom/swedbank/mobile/app/overview/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 142
    new-instance v3, Lcom/swedbank/mobile/app/overview/a/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 143
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
