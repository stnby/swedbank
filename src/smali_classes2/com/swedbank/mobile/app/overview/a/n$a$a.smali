.class public final Lcom/swedbank/mobile/app/overview/a/n$a$a;
.super Lcom/swedbank/mobile/app/overview/a/n$a;
.source "OverviewCombinedViewState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/a/n$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Landroidx/recyclerview/widget/f$b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/f$b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/plugins/list/d;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            "Z)V"
        }
    .end annotation

    const-string v0, "dataset"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/overview/a/n$a;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/n$a$a;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/a/n$a$a;->b:Landroidx/recyclerview/widget/f$b;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/overview/a/n$a$a;->c:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/n$a$a;->a:Ljava/util/List;

    return-object v0
.end method

.method public final b()Landroidx/recyclerview/widget/f$b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/n$a$a;->b:Landroidx/recyclerview/widget/f$b;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 29
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/a/n$a$a;->c:Z

    return v0
.end method
