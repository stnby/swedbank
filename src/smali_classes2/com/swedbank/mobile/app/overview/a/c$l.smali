.class final Lcom/swedbank/mobile/app/overview/a/c$l;
.super Ljava/lang/Object;
.source "OverviewCombinedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/a/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/c$l;->a:Lcom/swedbank/mobile/app/overview/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/overview/a/n$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/a/c$l;->a:Lcom/swedbank/mobile/app/overview/a/c;

    .line 71
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/a/c$l;->a:Lcom/swedbank/mobile/app/overview/a/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/overview/a/c;->a(Lcom/swedbank/mobile/app/overview/a/c;)Lcom/swedbank/mobile/business/overview/combined/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/overview/combined/a;->b()Lio/reactivex/w;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 73
    sget-object v0, Lcom/swedbank/mobile/app/overview/a/c$l$1;->a:Lcom/swedbank/mobile/app/overview/a/c$l$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 80
    sget-object v0, Lcom/swedbank/mobile/app/overview/a/n$a$g;->a:Lcom/swedbank/mobile/app/overview/a/n$a$g;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "interactor.queryOlderDat\u2026lState.OlderItemsLoading)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/swedbank/mobile/app/overview/a/c$m;->a:Lcom/swedbank/mobile/app/overview/a/c$m;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/a/c$l;->a(Lkotlin/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
