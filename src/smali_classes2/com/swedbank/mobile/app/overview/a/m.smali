.class public final Lcom/swedbank/mobile/app/overview/a/m;
.super Ljava/lang/Object;
.source "OverviewCombinedViewImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/overview/a/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/ui/aj;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/ui/aj;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/a/m;->a:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/a/m;->b:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/a/m;->c:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/overview/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/ui/aj;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/navigation/i;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;>;)",
            "Lcom/swedbank/mobile/app/overview/a/m;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/swedbank/mobile/app/overview/a/m;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/overview/a/m;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/overview/a/k;
    .locals 4

    .line 29
    new-instance v0, Lcom/swedbank/mobile/app/overview/a/k;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/a/m;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/core/ui/aj;

    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/a/m;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/navigation/i;

    iget-object v3, p0, Lcom/swedbank/mobile/app/overview/a/m;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/overview/a/k;-><init>(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/business/navigation/i;Ljava/util/List;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/a/m;->a()Lcom/swedbank/mobile/app/overview/a/k;

    move-result-object v0

    return-object v0
.end method
