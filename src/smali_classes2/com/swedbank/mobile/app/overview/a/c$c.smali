.class final synthetic Lcom/swedbank/mobile/app/overview/a/c$c;
.super Lkotlin/e/b/i;
.source "OverviewCombinedPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/overview/a/n;",
        "Lcom/swedbank/mobile/app/overview/a/n$a;",
        "Lcom/swedbank/mobile/app/overview/a/n;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/a/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/a/n;Lcom/swedbank/mobile/app/overview/a/n$a;)Lcom/swedbank/mobile/app/overview/a/n;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/overview/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/overview/a/n$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/a/c$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/overview/a/c;

    .line 137
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/a/n$a$d;

    if-eqz v0, :cond_0

    .line 138
    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$d;->a()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p1

    .line 137
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    goto/16 :goto_2

    .line 139
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/a/n$a$e;

    if-eqz v0, :cond_3

    .line 140
    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$e;->a()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/a/n;->a()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, 0x1

    const/4 v1, 0x1

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v0, p1

    .line 139
    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    goto/16 :goto_2

    .line 144
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/overview/a/n$a$g;->a:Lcom/swedbank/mobile/app/overview/a/n$a$g;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 145
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/a/n;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object p2

    if-eqz p2, :cond_8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    goto :goto_2

    .line 149
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/a/n$a$a;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    .line 150
    new-instance v3, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 151
    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$a;->a()Ljava/util/List;

    move-result-object v0

    .line 152
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$a;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object v1

    .line 153
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$a;->c()Z

    move-result p2

    .line 150
    invoke-direct {v3, v0, v1, p2}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v1, p1

    .line 149
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    goto :goto_2

    .line 154
    :cond_5
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/a/n$a$c;

    if-eqz v0, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 156
    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1a

    const/4 v8, 0x0

    move-object v1, p1

    .line 154
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    goto :goto_2

    .line 157
    :cond_6
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/a/n$a$f;

    if-eqz v0, :cond_7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 158
    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$f;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    move-object v1, p1

    .line 157
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    goto :goto_2

    .line 159
    :cond_7
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/a/n$a$b;

    if-eqz v0, :cond_9

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 161
    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/a/n$a$b;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v1, p1

    .line 159
    invoke-static/range {v1 .. v8}, Lcom/swedbank/mobile/app/overview/a/n;->a(Lcom/swedbank/mobile/app/overview/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    :cond_8
    :goto_2
    return-object p1

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/overview/a/n;

    check-cast p2, Lcom/swedbank/mobile/app/overview/a/n$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/a/c$c;->a(Lcom/swedbank/mobile/app/overview/a/n;Lcom/swedbank/mobile/app/overview/a/n$a;)Lcom/swedbank/mobile/app/overview/a/n;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/overview/a/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/overview/combined/OverviewCombinedViewState;Lcom/swedbank/mobile/app/overview/combined/OverviewCombinedViewState$PartialState;)Lcom/swedbank/mobile/app/overview/combined/OverviewCombinedViewState;"

    return-object v0
.end method
