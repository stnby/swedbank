.class public final Lcom/swedbank/mobile/app/overview/i$d;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/overview/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# static fields
.field public static final action0:I = 0x7f0a002b

.field public static final action_bar:I = 0x7f0a002c

.field public static final action_bar_activity_content:I = 0x7f0a002d

.field public static final action_bar_container:I = 0x7f0a002e

.field public static final action_bar_root:I = 0x7f0a002f

.field public static final action_bar_spinner:I = 0x7f0a0030

.field public static final action_bar_subtitle:I = 0x7f0a0031

.field public static final action_bar_title:I = 0x7f0a0032

.field public static final action_container:I = 0x7f0a0033

.field public static final action_context_bar:I = 0x7f0a0034

.field public static final action_divider:I = 0x7f0a0036

.field public static final action_image:I = 0x7f0a0037

.field public static final action_menu_divider:I = 0x7f0a0038

.field public static final action_menu_presenter:I = 0x7f0a0039

.field public static final action_mode_bar:I = 0x7f0a003a

.field public static final action_mode_bar_stub:I = 0x7f0a003b

.field public static final action_mode_close_button:I = 0x7f0a003c

.field public static final action_text:I = 0x7f0a003d

.field public static final actions:I = 0x7f0a003e

.field public static final activity_chooser_view_content:I = 0x7f0a003f

.field public static final add:I = 0x7f0a0040

.field public static final adjust_height:I = 0x7f0a0041

.field public static final adjust_width:I = 0x7f0a0042

.field public static final alertTitle:I = 0x7f0a004c

.field public static final architect_node_id:I = 0x7f0a0054

.field public static final async:I = 0x7f0a0055

.field public static final auto:I = 0x7f0a0056

.field public static final automatic:I = 0x7f0a0057

.field public static final balance_item_all_currencies:I = 0x7f0a0058

.field public static final balance_item_available_funds:I = 0x7f0a0059

.field public static final balance_item_credit_limit:I = 0x7f0a005a

.field public static final blocking:I = 0x7f0a0071

.field public static final bottom:I = 0x7f0a0072

.field public static final bottom_sheet_action_list:I = 0x7f0a0073

.field public static final bottom_sheet_action_title:I = 0x7f0a0074

.field public static final bottom_sheet_subtitle:I = 0x7f0a0075

.field public static final bottom_sheet_title:I = 0x7f0a0076

.field public static final browser_actions_header_text:I = 0x7f0a0077

.field public static final browser_actions_menu_item_icon:I = 0x7f0a0078

.field public static final browser_actions_menu_item_text:I = 0x7f0a0079

.field public static final browser_actions_menu_items:I = 0x7f0a007a

.field public static final browser_actions_menu_view:I = 0x7f0a007b

.field public static final buttonPanel:I = 0x7f0a007c

.field public static final cancel_action:I = 0x7f0a007d

.field public static final center:I = 0x7f0a00c3

.field public static final challenge_info:I = 0x7f0a00c7

.field public static final challenge_status:I = 0x7f0a00c8

.field public static final checkbox:I = 0x7f0a00c9

.field public static final chronometer:I = 0x7f0a00ca

.field public static final contact_app_bar_layout:I = 0x7f0a00ce

.field public static final contact_list_business:I = 0x7f0a00cf

.field public static final contact_list_private:I = 0x7f0a00d0

.field public static final contact_method_divider:I = 0x7f0a00d1

.field public static final contact_method_icon:I = 0x7f0a00d2

.field public static final contact_method_root_view:I = 0x7f0a00d3

.field public static final contact_method_subtitle:I = 0x7f0a00d4

.field public static final contact_method_title:I = 0x7f0a00d5

.field public static final contact_root_view:I = 0x7f0a00d6

.field public static final contact_tabs:I = 0x7f0a00d7

.field public static final contact_view_pager:I = 0x7f0a00d8

.field public static final container:I = 0x7f0a00d9

.field public static final content:I = 0x7f0a00da

.field public static final contentPanel:I = 0x7f0a00db

.field public static final coordinator:I = 0x7f0a00dc

.field public static final credit_limit_details_below_content_bg:I = 0x7f0a00dd

.field public static final credit_limit_details_bottom_divider:I = 0x7f0a00de

.field public static final credit_limit_details_content_group:I = 0x7f0a00df

.field public static final credit_limit_details_credit_limit_subtitle:I = 0x7f0a00e0

.field public static final credit_limit_details_divider:I = 0x7f0a00e1

.field public static final credit_limit_details_error_group:I = 0x7f0a00e2

.field public static final credit_limit_details_interest_accruing_amount:I = 0x7f0a00e3

.field public static final credit_limit_details_interest_accruing_amount_loading:I = 0x7f0a00e4

.field public static final credit_limit_details_interest_accruing_amount_subtitle:I = 0x7f0a00e5

.field public static final credit_limit_details_interest_accruing_amount_title:I = 0x7f0a00e6

.field public static final credit_limit_details_repay_interest_accruing_amount_btn:I = 0x7f0a00e7

.field public static final credit_limit_details_repay_used_credit_limit_btn:I = 0x7f0a00e8

.field public static final credit_limit_details_retry_background:I = 0x7f0a00e9

.field public static final credit_limit_details_retry_btn:I = 0x7f0a00ea

.field public static final credit_limit_details_retry_title:I = 0x7f0a00eb

.field public static final credit_limit_details_used_credit_limit:I = 0x7f0a00ec

.field public static final credit_limit_details_used_credit_limit_loading:I = 0x7f0a00ed

.field public static final credit_limit_details_used_credit_limit_title:I = 0x7f0a00ee

.field public static final credit_limit_details_view_root:I = 0x7f0a00ef

.field public static final credit_limit_group:I = 0x7f0a00f0

.field public static final currencies_balance_available_funds:I = 0x7f0a00f1

.field public static final currencies_balance_end_guideline:I = 0x7f0a00f2

.field public static final currencies_balance_reserved:I = 0x7f0a00f3

.field public static final currencies_balance_start_guideline:I = 0x7f0a00f4

.field public static final currencies_balance_title:I = 0x7f0a00f5

.field public static final currencies_buttons_exchange:I = 0x7f0a00f6

.field public static final currencies_buttons_view_rates:I = 0x7f0a00f7

.field public static final custom:I = 0x7f0a00f9

.field public static final customPanel:I = 0x7f0a00fa

.field public static final dark:I = 0x7f0a0104

.field public static final decor_content_parent:I = 0x7f0a0105

.field public static final default_activity_button:I = 0x7f0a0106

.field public static final design_bottom_sheet:I = 0x7f0a0107

.field public static final design_menu_item_action_area:I = 0x7f0a0108

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0109

.field public static final design_menu_item_text:I = 0x7f0a010a

.field public static final design_navigation_view:I = 0x7f0a010b

.field public static final edit_query:I = 0x7f0a0111

.field public static final enable_setting_title:I = 0x7f0a0112

.field public static final end:I = 0x7f0a0113

.field public static final end_padder:I = 0x7f0a0114

.field public static final error:I = 0x7f0a0117

.field public static final expand_activities_button:I = 0x7f0a0119

.field public static final expanded_menu:I = 0x7f0a011a

.field public static final fill:I = 0x7f0a0127

.field public static final filled:I = 0x7f0a012a

.field public static final fixed:I = 0x7f0a0130

.field public static final forever:I = 0x7f0a0138

.field public static final ghost_view:I = 0x7f0a0139

.field public static final gone:I = 0x7f0a013a

.field public static final group_divider:I = 0x7f0a013b

.field public static final hardware:I = 0x7f0a013d

.field public static final home:I = 0x7f0a013e

.field public static final icon:I = 0x7f0a0145

.field public static final icon_group:I = 0x7f0a0146

.field public static final icon_only:I = 0x7f0a0147

.field public static final image:I = 0x7f0a0149

.field public static final info:I = 0x7f0a014a

.field public static final information_setting_content:I = 0x7f0a014b

.field public static final information_setting_description:I = 0x7f0a014c

.field public static final information_setting_title:I = 0x7f0a014d

.field public static final invisible:I = 0x7f0a014e

.field public static final italic:I = 0x7f0a014f

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0155

.field public static final item_transaction_detail_content:I = 0x7f0a0156

.field public static final item_transaction_detail_loading_content:I = 0x7f0a0157

.field public static final item_transaction_detail_loading_end_guideline:I = 0x7f0a0158

.field public static final item_transaction_detail_loading_title:I = 0x7f0a0159

.field public static final item_transaction_detail_title:I = 0x7f0a015a

.field public static final labeled:I = 0x7f0a015b

.field public static final largeLabel:I = 0x7f0a015d

.field public static final left:I = 0x7f0a015e

.field public static final light:I = 0x7f0a015f

.field public static final line1:I = 0x7f0a0160

.field public static final line3:I = 0x7f0a0161

.field public static final listMode:I = 0x7f0a0162

.field public static final list_item:I = 0x7f0a0163

.field public static final list_item_separator_title:I = 0x7f0a0164

.field public static final lottie_layer_name:I = 0x7f0a0189

.field public static final masked:I = 0x7f0a018a

.field public static final media_actions:I = 0x7f0a018b

.field public static final message:I = 0x7f0a018e

.field public static final mini:I = 0x7f0a0190

.field public static final mtrl_child_content_container:I = 0x7f0a0197

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0198

.field public static final multiply:I = 0x7f0a0199

.field public static final navigation_bar:I = 0x7f0a019a

.field public static final navigation_header_container:I = 0x7f0a019b

.field public static final navigation_item_icon:I = 0x7f0a019c

.field public static final navigation_item_text:I = 0x7f0a019d

.field public static final navigation_root_view:I = 0x7f0a019e

.field public static final none:I = 0x7f0a01aa

.field public static final normal:I = 0x7f0a01ab

.field public static final not_authenticated_illustration:I = 0x7f0a01ac

.field public static final not_authenticated_log_in_btn:I = 0x7f0a01ad

.field public static final not_authenticated_message:I = 0x7f0a01ae

.field public static final not_authenticated_title:I = 0x7f0a01af

.field public static final not_authenticated_view_root:I = 0x7f0a01b0

.field public static final notification_background:I = 0x7f0a01b1

.field public static final notification_main_column:I = 0x7f0a01b2

.field public static final notification_main_column_container:I = 0x7f0a01b3

.field public static final off:I = 0x7f0a01b4

.field public static final on:I = 0x7f0a01b5

.field public static final on_off_setting_state:I = 0x7f0a01b6

.field public static final on_off_setting_subtitle:I = 0x7f0a01b7

.field public static final on_off_setting_title:I = 0x7f0a01b8

.field public static final onboarding_cancel_btn:I = 0x7f0a01ba

.field public static final onboarding_content_views:I = 0x7f0a01bb

.field public static final onboarding_done_btn:I = 0x7f0a01bc

.field public static final onboarding_error_handler_cancel_btn:I = 0x7f0a01bd

.field public static final onboarding_error_handler_message:I = 0x7f0a01be

.field public static final onboarding_error_handler_retry_btn:I = 0x7f0a01bf

.field public static final onboarding_error_handler_title:I = 0x7f0a01c0

.field public static final onboarding_loading:I = 0x7f0a01c7

.field public static final onboarding_loading_views:I = 0x7f0a01c8

.field public static final onboarding_plugin_container:I = 0x7f0a01c9

.field public static final onboarding_root_view:I = 0x7f0a01cb

.field public static final onboarding_text:I = 0x7f0a01cc

.field public static final onboarding_title:I = 0x7f0a01cd

.field public static final outline:I = 0x7f0a01d8

.field public static final overview_app_bar_layout:I = 0x7f0a01d9

.field public static final overview_balance_list:I = 0x7f0a01da

.field public static final overview_balance_loading:I = 0x7f0a01db

.field public static final overview_combined_list:I = 0x7f0a01dc

.field public static final overview_combined_refresh:I = 0x7f0a01dd

.field public static final overview_combined_root:I = 0x7f0a01de

.field public static final overview_content_container:I = 0x7f0a01df

.field public static final overview_currencies_error_group:I = 0x7f0a01e0

.field public static final overview_currencies_list:I = 0x7f0a01e1

.field public static final overview_currencies_loading:I = 0x7f0a01e2

.field public static final overview_currencies_retry_background:I = 0x7f0a01e3

.field public static final overview_currencies_retry_btn:I = 0x7f0a01e4

.field public static final overview_currencies_retry_title:I = 0x7f0a01e5

.field public static final overview_currencies_view_root:I = 0x7f0a01e6

.field public static final overview_detailed_list:I = 0x7f0a01e7

.field public static final overview_detailed_list_refresh:I = 0x7f0a01e8

.field public static final overview_detailed_root:I = 0x7f0a01e9

.field public static final overview_error_group:I = 0x7f0a01ea

.field public static final overview_history_icon:I = 0x7f0a01eb

.field public static final overview_history_item_root:I = 0x7f0a01ec

.field public static final overview_history_keyword:I = 0x7f0a01ed

.field public static final overview_loading_app_bar_layout:I = 0x7f0a01ee

.field public static final overview_loading_title:I = 0x7f0a01ef

.field public static final overview_loan_offer_icon:I = 0x7f0a01f0

.field public static final overview_loan_offer_item_root:I = 0x7f0a01f1

.field public static final overview_loan_offer_title:I = 0x7f0a01f2

.field public static final overview_retry_btn:I = 0x7f0a01f3

.field public static final overview_retry_title:I = 0x7f0a01f4

.field public static final overview_search_clear_btn:I = 0x7f0a01f5

.field public static final overview_search_input:I = 0x7f0a01f6

.field public static final overview_search_input_loading:I = 0x7f0a01f7

.field public static final overview_search_results:I = 0x7f0a01f8

.field public static final overview_search_view_root:I = 0x7f0a01f9

.field public static final overview_statement_container:I = 0x7f0a01fa

.field public static final overview_statement_empty:I = 0x7f0a01fb

.field public static final overview_statement_end_guideline:I = 0x7f0a01fc

.field public static final overview_statement_error_group:I = 0x7f0a01fd

.field public static final overview_statement_header_search:I = 0x7f0a01fe

.field public static final overview_statement_header_title:I = 0x7f0a01ff

.field public static final overview_statement_retry_btn:I = 0x7f0a0200

.field public static final overview_statement_retry_title:I = 0x7f0a0201

.field public static final overview_statement_start_guideline:I = 0x7f0a0202

.field public static final overview_toolbar_layout:I = 0x7f0a0203

.field public static final packed:I = 0x7f0a0204

.field public static final parallax:I = 0x7f0a0205

.field public static final parent:I = 0x7f0a0206

.field public static final parentPanel:I = 0x7f0a0207

.field public static final parent_matrix:I = 0x7f0a0208

.field public static final percent:I = 0x7f0a0242

.field public static final pin:I = 0x7f0a0243

.field public static final progress_circular:I = 0x7f0a0261

.field public static final progress_horizontal:I = 0x7f0a0262

.field public static final radio:I = 0x7f0a0264

.field public static final recurring_login_alternative_login_btn:I = 0x7f0a0265

.field public static final recurring_login_plugin_barrier:I = 0x7f0a0266

.field public static final recurring_login_plugin_min_bottom_guideline:I = 0x7f0a0267

.field public static final recurring_login_plugin_root_view:I = 0x7f0a0268

.field public static final recurring_login_view_root:I = 0x7f0a0269

.field public static final recurring_login_welcome_title:I = 0x7f0a026a

.field public static final restart:I = 0x7f0a026b

.field public static final retry_btn:I = 0x7f0a026c

.field public static final retry_title:I = 0x7f0a026d

.field public static final reverse:I = 0x7f0a026e

.field public static final right:I = 0x7f0a026f

.field public static final right_icon:I = 0x7f0a0270

.field public static final right_side:I = 0x7f0a0271

.field public static final root_layout:I = 0x7f0a0272

.field public static final save_image_matrix:I = 0x7f0a0273

.field public static final save_non_transition_alpha:I = 0x7f0a0274

.field public static final save_scale_type:I = 0x7f0a0275

.field public static final screen:I = 0x7f0a0276

.field public static final scrollIndicatorDown:I = 0x7f0a0278

.field public static final scrollIndicatorUp:I = 0x7f0a0279

.field public static final scrollView:I = 0x7f0a027a

.field public static final scrollable:I = 0x7f0a027b

.field public static final search_badge:I = 0x7f0a027c

.field public static final search_bar:I = 0x7f0a027d

.field public static final search_button:I = 0x7f0a027e

.field public static final search_close_btn:I = 0x7f0a027f

.field public static final search_edit_frame:I = 0x7f0a0280

.field public static final search_go_btn:I = 0x7f0a0281

.field public static final search_mag_icon:I = 0x7f0a0282

.field public static final search_plate:I = 0x7f0a0283

.field public static final search_src_text:I = 0x7f0a028a

.field public static final search_voice_btn:I = 0x7f0a028b

.field public static final select_dialog_listview:I = 0x7f0a028d

.field public static final selected:I = 0x7f0a028e

.field public static final shortcut:I = 0x7f0a0298

.field public static final slide_to_confirm_circle_bg:I = 0x7f0a029c

.field public static final slide_to_confirm_confirm_button_image:I = 0x7f0a029d

.field public static final slide_to_confirm_end_bg:I = 0x7f0a029e

.field public static final slide_to_confirm_indicator:I = 0x7f0a029f

.field public static final slide_to_confirm_inner_button:I = 0x7f0a02a0

.field public static final slide_to_confirm_main_bg:I = 0x7f0a02a1

.field public static final slide_to_confirm_root:I = 0x7f0a02a2

.field public static final slide_to_confirm_text:I = 0x7f0a02a3

.field public static final smallLabel:I = 0x7f0a02a4

.field public static final snackbar_action:I = 0x7f0a02a5

.field public static final snackbar_action_btn:I = 0x7f0a02a6

.field public static final snackbar_bottom_barrier:I = 0x7f0a02a7

.field public static final snackbar_bottom_guideline:I = 0x7f0a02a8

.field public static final snackbar_buttons_barrier:I = 0x7f0a02a9

.field public static final snackbar_close_btn:I = 0x7f0a02aa

.field public static final snackbar_subtitle:I = 0x7f0a02ab

.field public static final snackbar_text:I = 0x7f0a02ac

.field public static final snackbar_title:I = 0x7f0a02ad

.field public static final snackbar_top_guideline:I = 0x7f0a02ae

.field public static final software:I = 0x7f0a02b1

.field public static final spacer:I = 0x7f0a02b2

.field public static final split_action_bar:I = 0x7f0a02b3

.field public static final spread:I = 0x7f0a02b4

.field public static final spread_inside:I = 0x7f0a02b5

.field public static final src_atop:I = 0x7f0a02b6

.field public static final src_in:I = 0x7f0a02b7

.field public static final src_over:I = 0x7f0a02b8

.field public static final standard:I = 0x7f0a02b9

.field public static final start:I = 0x7f0a02ba

.field public static final status_bar_latest_event_content:I = 0x7f0a02bb

.field public static final stretch:I = 0x7f0a02bc

.field public static final submenuarrow:I = 0x7f0a02bd

.field public static final submit_area:I = 0x7f0a02be

.field public static final tabMode:I = 0x7f0a02c6

.field public static final tag_transition_group:I = 0x7f0a02cd

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a02ce

.field public static final tag_unhandled_key_listeners:I = 0x7f0a02cf

.field public static final text:I = 0x7f0a02d0

.field public static final text2:I = 0x7f0a02d1

.field public static final textSpacerNoButtons:I = 0x7f0a02d2

.field public static final textSpacerNoTitle:I = 0x7f0a02d3

.field public static final text_input_password_toggle:I = 0x7f0a02d5

.field public static final textinput_counter:I = 0x7f0a02d6

.field public static final textinput_error:I = 0x7f0a02d7

.field public static final textinput_helper_text:I = 0x7f0a02d8

.field public static final time:I = 0x7f0a02d9

.field public static final title:I = 0x7f0a02da

.field public static final titleDividerNoCustom:I = 0x7f0a02db

.field public static final title_template:I = 0x7f0a02dc

.field public static final toolbar:I = 0x7f0a02dd

.field public static final top:I = 0x7f0a02de

.field public static final topPanel:I = 0x7f0a02df

.field public static final touch_outside:I = 0x7f0a02e0

.field public static final transaction_account_available_funds:I = 0x7f0a02e1

.field public static final transaction_account_item_root:I = 0x7f0a02e2

.field public static final transaction_account_name:I = 0x7f0a02e3

.field public static final transaction_amount:I = 0x7f0a02e4

.field public static final transaction_bottom_guideline:I = 0x7f0a02e5

.field public static final transaction_date:I = 0x7f0a02e6

.field public static final transaction_description:I = 0x7f0a02e7

.field public static final transaction_details_error_background:I = 0x7f0a02e8

.field public static final transaction_details_error_group:I = 0x7f0a02e9

.field public static final transaction_details_list:I = 0x7f0a02ea

.field public static final transaction_details_new_payment_btn:I = 0x7f0a02eb

.field public static final transaction_details_retry_btn:I = 0x7f0a02ec

.field public static final transaction_details_retry_title:I = 0x7f0a02ed

.field public static final transaction_details_root:I = 0x7f0a02ee

.field public static final transaction_end_guideline:I = 0x7f0a02ef

.field public static final transaction_item_root:I = 0x7f0a02f0

.field public static final transaction_start_guideline:I = 0x7f0a02f1

.field public static final transaction_title:I = 0x7f0a02f2

.field public static final transaction_top_guideline:I = 0x7f0a02f3

.field public static final transaction_type:I = 0x7f0a02f4

.field public static final transition_current_scene:I = 0x7f0a0312

.field public static final transition_layout_save:I = 0x7f0a0313

.field public static final transition_position:I = 0x7f0a0314

.field public static final transition_scene_layoutid_cache:I = 0x7f0a0315

.field public static final transition_transform:I = 0x7f0a0316

.field public static final uniform:I = 0x7f0a0317

.field public static final unlabeled:I = 0x7f0a0318

.field public static final up:I = 0x7f0a0319

.field public static final view_offset_helper:I = 0x7f0a031d

.field public static final visible:I = 0x7f0a031f

.field public static final wide:I = 0x7f0a0362

.field public static final wrap:I = 0x7f0a0382

.field public static final wrap_content:I = 0x7f0a0383
