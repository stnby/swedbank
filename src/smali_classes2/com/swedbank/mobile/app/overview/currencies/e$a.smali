.class final synthetic Lcom/swedbank/mobile/app/overview/currencies/e$a;
.super Lkotlin/e/b/i;
.source "OverviewCurrenciesPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/currencies/e;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/overview/currencies/m;",
        "Lcom/swedbank/mobile/app/overview/currencies/m$a;",
        "Lcom/swedbank/mobile/app/overview/currencies/m;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/currencies/e;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/overview/currencies/m;Lcom/swedbank/mobile/app/overview/currencies/m$a;)Lcom/swedbank/mobile/app/overview/currencies/m;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/overview/currencies/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/overview/currencies/m$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/e$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/overview/currencies/e;

    .line 94
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/currencies/m$a$a;

    if-eqz v0, :cond_0

    .line 95
    check-cast p2, Lcom/swedbank/mobile/app/overview/currencies/m$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/currencies/m$a$a;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p1

    .line 94
    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/overview/currencies/m;->a(Lcom/swedbank/mobile/app/overview/currencies/m;Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p1

    goto :goto_0

    .line 97
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/app/overview/currencies/m$a$e;->a:Lcom/swedbank/mobile/app/overview/currencies/m$a$e;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x5

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/overview/currencies/m;->a(Lcom/swedbank/mobile/app/overview/currencies/m;Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p1

    goto :goto_0

    .line 101
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/currencies/m$a$b;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 102
    check-cast p2, Lcom/swedbank/mobile/app/overview/currencies/m$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/currencies/m$a$b;->a()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object v1, p1

    .line 101
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/overview/currencies/m;->a(Lcom/swedbank/mobile/app/overview/currencies/m;Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p1

    goto :goto_0

    .line 104
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/app/overview/currencies/m$a$d;->a:Lcom/swedbank/mobile/app/overview/currencies/m$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/overview/currencies/m;->a(Lcom/swedbank/mobile/app/overview/currencies/m;Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p1

    goto :goto_0

    .line 107
    :cond_3
    instance-of v0, p2, Lcom/swedbank/mobile/app/overview/currencies/m$a$c;

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 109
    check-cast p2, Lcom/swedbank/mobile/app/overview/currencies/m$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/overview/currencies/m$a$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    const/4 v6, 0x5

    const/4 v7, 0x0

    move-object v1, p1

    .line 107
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/overview/currencies/m;->a(Lcom/swedbank/mobile/app/overview/currencies/m;Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/app/overview/currencies/m;

    check-cast p2, Lcom/swedbank/mobile/app/overview/currencies/m$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/currencies/e$a;->a(Lcom/swedbank/mobile/app/overview/currencies/m;Lcom/swedbank/mobile/app/overview/currencies/m$a;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/overview/currencies/e;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/overview/currencies/OverviewCurrenciesViewState;Lcom/swedbank/mobile/app/overview/currencies/OverviewCurrenciesViewState$PartialState;)Lcom/swedbank/mobile/app/overview/currencies/OverviewCurrenciesViewState;"

    return-object v0
.end method
