.class public final Lcom/swedbank/mobile/app/overview/currencies/c;
.super Ljava/lang/Object;
.source "OverviewCurrenciesBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/swedbank/mobile/business/overview/currencies/c;

.field private final d:Lcom/swedbank/mobile/a/u/c/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/u/c/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/u/c/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/c;->d:Lcom/swedbank/mobile/a/u/c/a$a;

    .line 17
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/c;->b:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/overview/currencies/c;)Lcom/swedbank/mobile/app/overview/currencies/c;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/currencies/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/currencies/c;

    .line 29
    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/currencies/c;->c:Lcom/swedbank/mobile/business/overview/currencies/c;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/currencies/c;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/currencies/c;

    .line 21
    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/currencies/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/c;->d:Lcom/swedbank/mobile/a/u/c/a$a;

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/c;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/c/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/u/c/a$a;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/c;->b:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/c/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/u/c/a$a;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/c;->c:Lcom/swedbank/mobile/business/overview/currencies/c;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/u/c/a$a;->b(Lcom/swedbank/mobile/business/overview/currencies/c;)Lcom/swedbank/mobile/a/u/c/a$a;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/c/a$a;->a()Lcom/swedbank/mobile/a/u/c/a;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/swedbank/mobile/a/u/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 37
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display currencies without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 33
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display currencies without account id"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final b(Ljava/lang/String;)Lcom/swedbank/mobile/app/overview/currencies/c;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/overview/currencies/c;

    .line 25
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/overview/currencies/c;->b:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method
