.class final Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;
.super Lkotlin/e/b/k;
.source "CurrenciesLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;->b:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;
    .locals 22
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v8, p2

    move-object/from16 v1, p0

    const-string v3, "resources"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "metrics"

    invoke-static {v8, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v3, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;->b:Landroid/content/Context;

    sget v4, Lcom/swedbank/mobile/app/overview/i$a;->background_color:I

    .line 131
    invoke-static {v3, v4}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v17

    move/from16 v15, v17

    .line 34
    new-instance v4, Landroid/graphics/Paint;

    move-object v3, v4

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    new-instance v5, Landroid/graphics/Paint;

    move-object v6, v5

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/16 v4, 0x28

    invoke-static {v4}, Lcom/swedbank/mobile/core/ui/ag;->a(I)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    sget v4, Lcom/swedbank/mobile/app/overview/i$b;->list_item_separator_horizontal_padding:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v9, v4

    move v4, v9

    .line 37
    new-instance v10, Lcom/swedbank/mobile/core/ui/m;

    move-object v7, v10

    invoke-direct {v10, v2, v9, v5}, Lcom/swedbank/mobile/core/ui/m;-><init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V

    .line 41
    sget v5, Lcom/swedbank/mobile/app/overview/i$b;->overview_currencies_content_horizontal_margin:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v9, v5

    const/4 v5, 0x4

    int-to-float v5, v5

    const/4 v11, 0x1

    .line 132
    invoke-static {v11, v5, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    .line 43
    sget v5, Lcom/swedbank/mobile/app/overview/i$b;->overview_currencies_balance_item_min_height:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v14, v2

    move v2, v14

    int-to-float v5, v11

    .line 133
    invoke-static {v11, v5, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    const/16 v10, 0x14

    int-to-float v12, v10

    .line 134
    invoke-static {v11, v12, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v16

    move/from16 v10, v16

    .line 135
    invoke-static {v11, v12, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v18

    move/from16 v12, v18

    const/16 v0, 0xa

    int-to-float v0, v0

    .line 136
    invoke-static {v11, v0, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    move v11, v14

    move v14, v0

    sub-float v8, v11, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v8, v8, v16

    add-float v19, v18, v13

    add-float v19, v19, v0

    sub-float v0, v11, v19

    div-float v0, v0, v16

    move v11, v0

    add-float v0, v0, v18

    add-float/2addr v13, v0

    .line 53
    new-instance v18, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;

    move-object/from16 v20, v1

    move-object/from16 v0, v18

    move-object/from16 v21, v0

    iget-object v0, v1, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;->a:Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView;

    move-object/from16 v16, v0

    check-cast v16, Landroid/view/View;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v17}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;FLandroid/graphics/Paint;FFLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFFFILandroid/view/View;I)V

    return-object v18
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
