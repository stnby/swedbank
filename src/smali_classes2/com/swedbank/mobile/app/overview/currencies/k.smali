.class public final Lcom/swedbank/mobile/app/overview/currencies/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "OverviewCurrenciesViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/overview/currencies/j;
.implements Lcom/swedbank/mobile/architect/a/b/i;
.implements Lcom/swedbank/mobile/core/ui/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/b/a;",
        "Lcom/swedbank/mobile/app/overview/currencies/j;",
        "Lcom/swedbank/mobile/architect/a/b/i<",
        "Lcom/swedbank/mobile/app/overview/currencies/m;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/ao<",
        "Lcom/swedbank/mobile/app/overview/currencies/m;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lcom/swedbank/mobile/core/ui/aj;

.field private final i:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/currencies/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/currencies/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/currencies/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "currenciesList"

    const-string v4, "getCurrenciesList()Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/currencies/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/currencies/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/overview/currencies/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/business/util/l;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/aj;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_overview_currencies"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/aj;",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transitionAwareRenderer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialAccountName"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->h:Lcom/swedbank/mobile/core/ui/aj;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->i:Lcom/swedbank/mobile/business/util/l;

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->j:Lio/reactivex/o;

    .line 32
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->b:Lkotlin/f/c;

    .line 33
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_currencies_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->c:Lkotlin/f/c;

    .line 34
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_currencies_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->d:Lkotlin/f/c;

    .line 35
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_currencies_retry_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->e:Lkotlin/f/c;

    .line 36
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_currencies_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->f:Lkotlin/f/c;

    .line 37
    sget p1, Lcom/swedbank/mobile/app/overview/i$d;->overview_currencies_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->g:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/view/View;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->g()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/currencies/k;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->h()Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/view/View;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->k()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/widget/TextView;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->i()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/content/Context;
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final g()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final h()Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    return-object v0
.end method

.method private final i()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final j()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final k()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->j:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/overview/currencies/m;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/app/overview/currencies/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/currencies/m;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->o()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    check-cast v0, Landroid/view/ViewGroup;

    .line 113
    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->c()Landroidx/l/r;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Landroidx/l/r;->clone()Landroidx/l/n;

    move-result-object v1

    .line 118
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->b(Lcom/swedbank/mobile/app/overview/currencies/k;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    .line 119
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->a(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    .line 111
    invoke-static {v0, v1}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 55
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/currencies/m;->b()Z

    move-result v0

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->a(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 57
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/currencies/m;->c()Ljava/util/List;

    move-result-object v0

    .line 58
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/currencies/m;->b()Z

    move-result v1

    .line 59
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/currencies/m;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-nez v1, :cond_2

    if-nez v4, :cond_2

    .line 131
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->b(Lcom/swedbank/mobile/app/overview/currencies/k;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->setVisibility(I)V

    .line 132
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->b(Lcom/swedbank/mobile/app/overview/currencies/k;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->a(Ljava/util/List;)V

    goto :goto_2

    .line 134
    :cond_2
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->b(Lcom/swedbank/mobile/app/overview/currencies/k;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->setVisibility(I)V

    .line 60
    :goto_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/overview/currencies/m;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 140
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->c(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 141
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->d(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/widget/TextView;

    move-result-object v0

    .line 142
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->e(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/content/Context;

    move-result-object v1

    .line 143
    sget v2, Lcom/swedbank/mobile/app/overview/i$f;->overview_currencies_general_loading_error:I

    .line 146
    instance-of v3, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v3, :cond_4

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 148
    move-object v3, p1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v5

    if-eqz v3, :cond_3

    move-object v4, p1

    check-cast v4, Ljava/lang/Iterable;

    const-string p1, "\n"

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 149
    :cond_3
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 151
    :cond_4
    instance-of v2, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_5

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 152
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    const-string v1, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 153
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 154
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 152
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 155
    :cond_6
    invoke-static {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->c(Lcom/swedbank/mobile/app/overview/currencies/k;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    return-void

    .line 112
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/app/overview/currencies/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/currencies/k;->b(Lcom/swedbank/mobile/app/overview/currencies/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->j()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 110
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public b(Lcom/swedbank/mobile/app/overview/currencies/m;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/overview/currencies/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->h:Lcom/swedbank/mobile/core/ui/aj;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/ui/aj;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/app/overview/currencies/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/currencies/k;->a(Lcom/swedbank/mobile/app/overview/currencies/m;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->h()Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->h()Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()V
    .locals 5

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->h:Lcom/swedbank/mobile/core/ui/aj;

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/core/ui/aj$a;->a(Lcom/swedbank/mobile/core/ui/aj;Lcom/swedbank/mobile/architect/a/b/a;ZILjava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/k;->i:Lcom/swedbank/mobile/business/util/l;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/overview/currencies/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    .line 107
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    sget-object v0, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    goto :goto_0

    .line 108
    :cond_0
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/n;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v0

    .line 105
    check-cast v0, Ljava/lang/CharSequence;

    .line 41
    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v0

    .line 109
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/util/l;

    return-void

    .line 41
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
