.class public final Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "CurrenciesLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;

.field final synthetic b:F

.field final synthetic c:Landroid/graphics/Paint;

.field final synthetic d:F

.field final synthetic e:F

.field final synthetic f:Landroid/graphics/Paint;

.field final synthetic g:Lcom/swedbank/mobile/core/ui/m;

.field final synthetic h:F

.field final synthetic i:F

.field final synthetic j:F

.field final synthetic k:F

.field final synthetic l:F

.field final synthetic m:F

.field final synthetic n:F

.field final synthetic o:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;FLandroid/graphics/Paint;FFLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFFFILandroid/view/View;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Landroid/graphics/Paint;",
            "FF",
            "Landroid/graphics/Paint;",
            "Lcom/swedbank/mobile/core/ui/m;",
            "FFFFFFFI",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    .line 53
    iput-object v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->a:Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1;

    move v1, p2

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->b:F

    move-object v1, p3

    iput-object v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->c:Landroid/graphics/Paint;

    move v1, p4

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->d:F

    move v1, p5

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->e:F

    move-object v1, p6

    iput-object v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->f:Landroid/graphics/Paint;

    move-object v1, p7

    iput-object v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->g:Lcom/swedbank/mobile/core/ui/m;

    move v1, p8

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->h:F

    move v1, p9

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->i:F

    move/from16 v1, p10

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->j:F

    move/from16 v1, p11

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->k:F

    move/from16 v1, p12

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->l:F

    move/from16 v1, p13

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->m:F

    move/from16 v1, p14

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->n:F

    move/from16 v1, p15

    iput v1, v0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->o:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x1c

    const/4 v5, 0x0

    move-object p1, p0

    move-object/from16 p2, p16

    move/from16 p3, p17

    move p4, v1

    move p5, v2

    move p6, v3

    move p7, v4

    move-object p8, v5

    invoke-direct/range {p1 .. p8}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;)V
    .locals 9
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p2, "canvas"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p2, p1

    .line 56
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x5

    if-ge v3, v5, :cond_1

    .line 61
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->b:F

    add-float/2addr v5, v4

    invoke-virtual {v0, v2, v4, p2, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 62
    iget-object v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->c:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v5, 0x4

    if-ge v3, v5, :cond_0

    .line 66
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 67
    iget v6, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->d:F

    iget v7, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->d:F

    sub-float v7, p2, v7

    iget v8, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->e:F

    add-float/2addr v8, v5

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 68
    iget-object v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->f:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 69
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->b:F

    iget v6, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->e:F

    add-float/2addr v5, v6

    add-float/2addr v4, v5

    goto :goto_1

    .line 72
    :cond_0
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->b:F

    add-float/2addr v4, v5

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    :cond_1
    iget-object p2, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->g:Lcom/swedbank/mobile/core/ui/m;

    float-to-int v0, v4

    invoke-virtual {p2, v1, v0, p1, p3}, Lcom/swedbank/mobile/core/ui/m;->b(IIILandroid/graphics/Canvas;)V

    return-void
.end method

.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 9
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p2, "canvas"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "itemPaint"

    invoke-static {p4, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    int-to-float p1, p1

    const v0, 0x3df5c28f    # 0.12f

    mul-float v0, v0, p1

    const v1, 0x3e4ccccd    # 0.2f

    mul-float v1, v1, p1

    const v2, 0x3e8f5c29    # 0.28f

    mul-float v2, v2, p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x5

    if-ge v4, v5, :cond_0

    .line 93
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->h:F

    add-float/2addr v5, v3

    .line 94
    iget v6, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->i:F

    iget v7, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->i:F

    add-float/2addr v7, v0

    iget v8, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->j:F

    add-float/2addr v8, v5

    invoke-virtual {p2, v6, v5, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 95
    invoke-virtual {p3, p2, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 98
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->i:F

    sub-float v5, p1, v5

    sub-float/2addr v5, v1

    .line 99
    iget v6, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->k:F

    add-float/2addr v6, v3

    add-float v7, v5, v1

    .line 100
    iget v8, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->l:F

    add-float/2addr v8, v6

    invoke-virtual {p2, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 101
    invoke-virtual {p3, p2, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 104
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->i:F

    sub-float v5, p1, v5

    sub-float/2addr v5, v2

    .line 105
    iget v6, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->m:F

    add-float/2addr v6, v3

    add-float v7, v5, v2

    .line 106
    iget v8, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->n:F

    add-float/2addr v8, v6

    invoke-virtual {p2, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 107
    invoke-virtual {p3, p2, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 109
    iget v5, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->b:F

    iget v6, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesLoadingView$1$1;->e:F

    add-float/2addr v5, v6

    add-float/2addr v3, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
