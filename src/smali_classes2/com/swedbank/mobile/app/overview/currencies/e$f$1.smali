.class final Lcom/swedbank/mobile/app/overview/currencies/e$f$1;
.super Ljava/lang/Object;
.source "OverviewCurrenciesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/currencies/e$f;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/overview/currencies/e$f$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/overview/currencies/e$f$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/overview/currencies/e$f$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/overview/currencies/e$f$1;->a:Lcom/swedbank/mobile/app/overview/currencies/e$f$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lcom/swedbank/mobile/app/overview/currencies/m$a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/overview/currencies/m$a$d;->a:Lcom/swedbank/mobile/app/overview/currencies/m$a$d;

    check-cast p1, Lcom/swedbank/mobile/app/overview/currencies/m$a;

    goto :goto_0

    .line 38
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/app/overview/currencies/m$a$c;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/overview/currencies/m$a$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/overview/currencies/m$a;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/currencies/e$f$1;->a(Lcom/swedbank/mobile/business/util/p;)Lcom/swedbank/mobile/app/overview/currencies/m$a;

    move-result-object p1

    return-object p1
.end method
