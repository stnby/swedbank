.class public final Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "CurrenciesListView.kt"


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/core/ui/widget/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p2

    const-string p3, "PublishRelay.create<Unit>()"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->a:Lcom/b/c/c;

    .line 30
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p2

    const-string p3, "PublishRelay.create<Unit>()"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->b:Lcom/b/c/c;

    .line 31
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/k;

    const/4 p3, 0x2

    .line 32
    new-array p3, p3, [Lkotlin/k;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 34
    sget v2, Lcom/swedbank/mobile/app/overview/i$e;->item_currencies_balance:I

    .line 35
    new-instance v3, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView$a;

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView$a;-><init>(Landroid/content/Context;)V

    check-cast v3, Lkotlin/e/a/m;

    .line 33
    invoke-static {v2, v3}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    aput-object v1, p3, v0

    const/4 v1, 0x1

    .line 51
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 52
    sget v3, Lcom/swedbank/mobile/app/overview/i$e;->item_currencies_buttons:I

    .line 53
    new-instance v4, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView$b;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView$b;-><init>(Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;)V

    check-cast v4, Lkotlin/e/a/m;

    .line 51
    invoke-static {v3, v4}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, p3, v1

    .line 32
    invoke-static {p3}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object p3

    const/4 v2, 0x0

    .line 31
    invoke-direct {p2, v2, p3, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->c:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 63
    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p2, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 64
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/f;

    new-instance p3, Lcom/swedbank/mobile/core/ui/widget/g$c;

    new-array v2, v1, [I

    aput v1, v2, v0

    invoke-direct {p3, v2}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v6, p3

    check-cast v6, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 65
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->c:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 26
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 27
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;)Lcom/b/c/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->b:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;)Lcom/b/c/c;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/currencies/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "balances"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->c:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 70
    check-cast p1, Ljava/lang/Iterable;

    .line 79
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 80
    move-object v4, v1

    check-cast v4, Ljava/util/Collection;

    check-cast v2, Lcom/swedbank/mobile/app/overview/currencies/a;

    .line 71
    new-instance v5, Lcom/swedbank/mobile/core/ui/widget/i;

    const/4 v6, 0x0

    invoke-direct {v5, v2, v6}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_0
    move-object p1, v1

    check-cast p1, Ljava/util/Collection;

    .line 73
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/i;

    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-direct {p1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    check-cast v1, Ljava/util/List;

    const/4 p1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, v2}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    return-void
.end method

.method public final b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/CurrenciesListView;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method
