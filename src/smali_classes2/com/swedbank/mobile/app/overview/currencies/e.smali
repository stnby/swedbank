.class public final Lcom/swedbank/mobile/app/overview/currencies/e;
.super Lcom/swedbank/mobile/architect/a/d;
.source "OverviewCurrenciesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/overview/currencies/j;",
        "Lcom/swedbank/mobile/app/overview/currencies/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/overview/currencies/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/overview/currencies/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/overview/currencies/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/e;->a:Lcom/swedbank/mobile/business/overview/currencies/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/overview/currencies/e;)Lcom/swedbank/mobile/business/overview/currencies/a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/app/overview/currencies/e;->a:Lcom/swedbank/mobile/business/overview/currencies/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/e;->a:Lcom/swedbank/mobile/business/overview/currencies/a;

    .line 19
    invoke-interface {v0}, Lcom/swedbank/mobile/business/overview/currencies/a;->a()Lio/reactivex/o;

    move-result-object v0

    .line 20
    sget-object v1, Lcom/swedbank/mobile/app/overview/currencies/e$b;->a:Lcom/swedbank/mobile/app/overview/currencies/e$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/e;->a:Lcom/swedbank/mobile/business/overview/currencies/a;

    .line 23
    invoke-interface {v1}, Lcom/swedbank/mobile/business/overview/currencies/a;->c()Lio/reactivex/o;

    move-result-object v1

    .line 24
    sget-object v2, Lcom/swedbank/mobile/app/overview/currencies/e$g;->a:Lcom/swedbank/mobile/app/overview/currencies/e$g;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 30
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 31
    sget-object v3, Lcom/swedbank/mobile/app/overview/currencies/e$e;->a:Lcom/swedbank/mobile/app/overview/currencies/e$e;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/overview/currencies/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 29
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 32
    new-instance v3, Lcom/swedbank/mobile/app/overview/currencies/e$f;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/overview/currencies/e$f;-><init>(Lcom/swedbank/mobile/app/overview/currencies/e;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 44
    sget-object v3, Lcom/swedbank/mobile/app/overview/currencies/e$c;->a:Lcom/swedbank/mobile/app/overview/currencies/e$c;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/overview/currencies/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 45
    new-instance v4, Lcom/swedbank/mobile/app/overview/currencies/e$d;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/overview/currencies/e$d;-><init>(Lcom/swedbank/mobile/app/overview/currencies/e;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 46
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 47
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 49
    sget-object v4, Lcom/swedbank/mobile/app/overview/currencies/e$h;->a:Lcom/swedbank/mobile/app/overview/currencies/e$h;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/overview/currencies/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 50
    new-instance v5, Lcom/swedbank/mobile/app/overview/currencies/e$i;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/overview/currencies/e$i;-><init>(Lcom/swedbank/mobile/app/overview/currencies/e;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 51
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 52
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    .line 54
    sget-object v5, Lcom/swedbank/mobile/app/overview/currencies/e$j;->a:Lcom/swedbank/mobile/app/overview/currencies/e$j;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/overview/currencies/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 55
    new-instance v6, Lcom/swedbank/mobile/app/overview/currencies/e$k;

    invoke-direct {v6, p0}, Lcom/swedbank/mobile/app/overview/currencies/e$k;-><init>(Lcom/swedbank/mobile/app/overview/currencies/e;)V

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 56
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 57
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    const/4 v6, 0x6

    .line 60
    new-array v6, v6, [Lio/reactivex/s;

    .line 61
    check-cast v0, Lio/reactivex/s;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    .line 62
    check-cast v1, Lio/reactivex/s;

    const/4 v0, 0x1

    aput-object v1, v6, v0

    .line 63
    check-cast v2, Lio/reactivex/s;

    const/4 v0, 0x2

    aput-object v2, v6, v0

    .line 64
    check-cast v3, Lio/reactivex/s;

    const/4 v0, 0x3

    aput-object v3, v6, v0

    .line 65
    check-cast v4, Lio/reactivex/s;

    const/4 v0, 0x4

    aput-object v4, v6, v0

    .line 66
    check-cast v5, Lio/reactivex/s;

    const/4 v0, 0x5

    aput-object v5, v6, v0

    .line 60
    invoke-static {v6}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n        .merg\u2026currencyRatesClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v1, Lcom/swedbank/mobile/app/overview/currencies/m;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/overview/currencies/m;-><init>(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/overview/currencies/e$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/overview/currencies/e;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/overview/currencies/e$a;-><init>(Lcom/swedbank/mobile/app/overview/currencies/e;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 93
    new-instance v3, Lcom/swedbank/mobile/app/overview/currencies/f;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/overview/currencies/f;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 94
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
