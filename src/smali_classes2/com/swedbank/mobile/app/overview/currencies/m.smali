.class public final Lcom/swedbank/mobile/app/overview/currencies/m;
.super Ljava/lang/Object;
.source "OverviewCurrenciesViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/overview/currencies/m$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/currencies/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/overview/currencies/m;-><init>(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/currencies/a;",
            ">;",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "accountName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balances"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const-string p1, ""

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 10
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 11
    check-cast p4, Lcom/swedbank/mobile/business/util/e;

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/currencies/m;-><init>(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/overview/currencies/m;Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/overview/currencies/m;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-boolean p2, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/currencies/m;->a(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/overview/currencies/m;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/overview/currencies/m;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/currencies/a;",
            ">;",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/overview/currencies/m;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accountName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balances"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/app/overview/currencies/m;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/overview/currencies/m;-><init>(Ljava/lang/String;ZLjava/util/List;Lcom/swedbank/mobile/business/util/e;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/overview/currencies/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/overview/currencies/m;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/overview/currencies/m;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    iget-object p1, p1, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OverviewCurrenciesViewState(accountName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", balances="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", balancesQueryError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/currencies/m;->d:Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
