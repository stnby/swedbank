.class final Lcom/swedbank/mobile/app/overview/g$e;
.super Lkotlin/e/b/k;
.source "OverviewRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/overview/g;->a(Lcom/swedbank/mobile/business/util/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/overview/g;

.field final synthetic b:Lcom/swedbank/mobile/business/util/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/overview/g;Lcom/swedbank/mobile/business/util/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/overview/g$e;->a:Lcom/swedbank/mobile/app/overview/g;

    iput-object p2, p0, Lcom/swedbank/mobile/app/overview/g$e;->b:Lcom/swedbank/mobile/business/util/e;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 14
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    check-cast p1, Ljava/lang/Iterable;

    .line 103
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 102
    instance-of v0, v0, Lcom/swedbank/mobile/business/overview/retry/d;

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    if-nez v2, :cond_6

    .line 94
    iget-object p1, p0, Lcom/swedbank/mobile/app/overview/g$e;->a:Lcom/swedbank/mobile/app/overview/g;

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/app/overview/g$e;->a:Lcom/swedbank/mobile/app/overview/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/overview/g;->e(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/app/overview/f/a;

    move-result-object v0

    .line 90
    iget-object v2, p0, Lcom/swedbank/mobile/app/overview/g$e;->b:Lcom/swedbank/mobile/business/util/e;

    .line 91
    iget-object v3, p0, Lcom/swedbank/mobile/app/overview/g$e;->a:Lcom/swedbank/mobile/app/overview/g;

    invoke-static {v3}, Lcom/swedbank/mobile/app/overview/g;->g(Lcom/swedbank/mobile/app/overview/g;)Landroid/app/Application;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 92
    sget v4, Lcom/swedbank/mobile/app/overview/i$f;->overview_general_loading_error:I

    .line 109
    instance-of v5, v2, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v5, :cond_4

    check-cast v2, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 111
    move-object v5, v2

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v1, v5

    if-eqz v1, :cond_3

    move-object v5, v2

    check-cast v5, Ljava/lang/Iterable;

    const-string v1, "\n"

    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 112
    :cond_3
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 114
    :cond_4
    instance-of v1, v2, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_5

    check-cast v2, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 115
    invoke-static {v1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    const-string v2, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 116
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 89
    new-instance v2, Lcom/swedbank/mobile/app/overview/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/overview/e;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/swedbank/mobile/app/overview/f/c;

    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/overview/f/a;->a(Lcom/swedbank/mobile/app/overview/f/c;)Lcom/swedbank/mobile/app/overview/f/a;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/swedbank/mobile/app/overview/g$e;->a:Lcom/swedbank/mobile/app/overview/g;

    invoke-static {v1}, Lcom/swedbank/mobile/app/overview/g;->f(Lcom/swedbank/mobile/app/overview/g;)Lcom/swedbank/mobile/business/overview/retry/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/overview/f/a;->a(Lcom/swedbank/mobile/business/overview/retry/c;)Lcom/swedbank/mobile/app/overview/f/a;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/overview/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 118
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_2

    .line 115
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_6
    :goto_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/overview/g$e;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
