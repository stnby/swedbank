.class public final Lcom/swedbank/mobile/app/transfer/m;
.super Lcom/swedbank/mobile/business/i/f;
.source "TransferRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/h;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/transfer/payment/form/j;

.field private final f:Lcom/swedbank/mobile/app/transfer/search/a;

.field private final g:Lcom/swedbank/mobile/app/transfer/a/a;

.field private final h:Lcom/swedbank/mobile/app/transfer/c/b;

.field private final i:Lcom/swedbank/mobile/app/transfer/c/a/b;

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/swedbank/mobile/business/transfer/payment/form/g;

.field private final l:Lcom/swedbank/mobile/business/transfer/search/c;

.field private final m:Lcom/swedbank/mobile/business/transfer/detailed/c;

.field private final n:Lcom/swedbank/mobile/business/transfer/request/d;

.field private final o:Lcom/swedbank/mobile/business/transfer/request/opening/f;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/j;Lcom/swedbank/mobile/app/transfer/search/a;Lcom/swedbank/mobile/app/transfer/a/a;Lcom/swedbank/mobile/app/transfer/c/b;Lcom/swedbank/mobile/app/transfer/c/a/b;Ljava/util/List;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/transfer/search/c;Lcom/swedbank/mobile/business/transfer/detailed/c;Lcom/swedbank/mobile/business/transfer/request/d;Lcom/swedbank/mobile/business/transfer/request/opening/f;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/form/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/search/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/transfer/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/transfer/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/transfer/c/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/transfer/payment/form/g;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/transfer/search/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/transfer/detailed/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/business/transfer/request/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/business/transfer/request/opening/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p13    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p14    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/payment/form/j;",
            "Lcom/swedbank/mobile/app/transfer/search/a;",
            "Lcom/swedbank/mobile/app/transfer/a/a;",
            "Lcom/swedbank/mobile/app/transfer/c/b;",
            "Lcom/swedbank/mobile/app/transfer/c/a/b;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            "Lcom/swedbank/mobile/business/transfer/search/c;",
            "Lcom/swedbank/mobile/business/transfer/detailed/c;",
            "Lcom/swedbank/mobile/business/transfer/request/d;",
            "Lcom/swedbank/mobile/business/transfer/request/opening/f;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentFormBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferSearchBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferDetailedBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestBuilder"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestOpeningBuilder"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferPlugins"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentFormListener"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferSearchListener"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferDetailedListener"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestListener"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestOpeningListener"

    invoke-static {p11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p14, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0, p12, p14, p13}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/m;->e:Lcom/swedbank/mobile/app/transfer/payment/form/j;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/m;->f:Lcom/swedbank/mobile/app/transfer/search/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/m;->g:Lcom/swedbank/mobile/app/transfer/a/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/m;->h:Lcom/swedbank/mobile/app/transfer/c/b;

    iput-object p5, p0, Lcom/swedbank/mobile/app/transfer/m;->i:Lcom/swedbank/mobile/app/transfer/c/a/b;

    iput-object p6, p0, Lcom/swedbank/mobile/app/transfer/m;->j:Ljava/util/List;

    iput-object p7, p0, Lcom/swedbank/mobile/app/transfer/m;->k:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    iput-object p8, p0, Lcom/swedbank/mobile/app/transfer/m;->l:Lcom/swedbank/mobile/business/transfer/search/c;

    iput-object p9, p0, Lcom/swedbank/mobile/app/transfer/m;->m:Lcom/swedbank/mobile/business/transfer/detailed/c;

    iput-object p10, p0, Lcom/swedbank/mobile/app/transfer/m;->n:Lcom/swedbank/mobile/business/transfer/request/d;

    iput-object p11, p0, Lcom/swedbank/mobile/app/transfer/m;->o:Lcom/swedbank/mobile/business/transfer/request/opening/f;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/app/transfer/payment/form/j;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->e:Lcom/swedbank/mobile/app/transfer/payment/form/j;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/m;Lkotlin/e/a/b;)V
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/m;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/business/transfer/payment/form/g;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->k:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/app/transfer/search/a;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->f:Lcom/swedbank/mobile/app/transfer/search/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/business/transfer/search/c;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->l:Lcom/swedbank/mobile/business/transfer/search/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/app/transfer/a/a;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->g:Lcom/swedbank/mobile/app/transfer/a/a;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/business/transfer/detailed/c;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->m:Lcom/swedbank/mobile/business/transfer/detailed/c;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/app/transfer/c/b;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->h:Lcom/swedbank/mobile/app/transfer/c/b;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/business/transfer/request/d;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/m;->n:Lcom/swedbank/mobile/business/transfer/request/d;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/transfer/payment/form/h;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m;->e:Lcom/swedbank/mobile/app/transfer/payment/form/j;

    .line 70
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/m;->k:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/app/transfer/payment/form/j;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/app/transfer/payment/form/j;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 145
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->b(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    .line 73
    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/form/h;

    .line 146
    new-instance v1, Lcom/swedbank/mobile/app/transfer/m$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/transfer/m$a;-><init>(Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {p0, v1}, Lcom/swedbank/mobile/app/transfer/m;->a(Lcom/swedbank/mobile/app/transfer/m;Lkotlin/e/a/b;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/request/opening/g;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m;->i:Lcom/swedbank/mobile/app/transfer/c/a/b;

    .line 81
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/m;->o:Lcom/swedbank/mobile/business/transfer/request/opening/f;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/c/a/b;->a(Lcom/swedbank/mobile/business/transfer/request/opening/f;)Lcom/swedbank/mobile/app/transfer/c/a/b;

    move-result-object v0

    .line 82
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/transfer/c/a/b;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/transfer/c/a/b;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/c/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 155
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->b(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/request/opening/g;

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 153
    sget-object v0, Lcom/swedbank/mobile/app/transfer/m$c;->a:Lcom/swedbank/mobile/app/transfer/m$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Z)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 58
    new-instance v0, Lcom/swedbank/mobile/app/transfer/m$h;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/m$h;-><init>(Lcom/swedbank/mobile/app/transfer/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Z)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/m;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 156
    sget-object v0, Lcom/swedbank/mobile/app/transfer/m$e;->a:Lcom/swedbank/mobile/app/transfer/m$e;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "router"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m;->j:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 160
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/transfer/f;

    .line 100
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/transfer/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    new-instance p1, Lcom/swedbank/mobile/app/transfer/m$j;

    invoke-direct {p1, v1, p0}, Lcom/swedbank/mobile/app/transfer/m$j;-><init>(Lcom/swedbank/mobile/app/transfer/f;Lcom/swedbank/mobile/app/transfer/m;)V

    check-cast p1, Lkotlin/e/a/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/m;->b(Lkotlin/e/a/b;)V

    return-void

    .line 161
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public c()V
    .locals 1

    .line 88
    new-instance v0, Lcom/swedbank/mobile/app/transfer/m$k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/m$k;-><init>(Lcom/swedbank/mobile/app/transfer/m;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/m;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 158
    sget-object v0, Lcom/swedbank/mobile/app/transfer/m$g;->a:Lcom/swedbank/mobile/app/transfer/m$g;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public e()V
    .locals 1

    .line 162
    sget-object v0, Lcom/swedbank/mobile/app/transfer/m$f;->a:Lcom/swedbank/mobile/app/transfer/m$f;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public f()V
    .locals 1

    .line 118
    new-instance v0, Lcom/swedbank/mobile/app/transfer/m$i;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/m$i;-><init>(Lcom/swedbank/mobile/app/transfer/m;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/m;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public g()V
    .locals 1

    .line 164
    sget-object v0, Lcom/swedbank/mobile/app/transfer/m$d;->a:Lcom/swedbank/mobile/app/transfer/m$d;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public h()V
    .locals 2

    const/4 v0, 0x0

    .line 166
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 167
    new-instance v1, Lcom/swedbank/mobile/app/transfer/m$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/m$a;-><init>(Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {p0, v1}, Lcom/swedbank/mobile/app/transfer/m;->a(Lcom/swedbank/mobile/app/transfer/m;Lkotlin/e/a/b;)V

    return-void
.end method
