.class public final Lcom/swedbank/mobile/app/transfer/n;
.super Ljava/lang/Object;
.source "TransferRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/transfer/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/j;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/c;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/detailed/c;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/request/d;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/request/opening/f;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/detailed/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/request/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/request/opening/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/n;->a:Ljavax/inject/Provider;

    .line 64
    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/n;->b:Ljavax/inject/Provider;

    .line 65
    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/n;->c:Ljavax/inject/Provider;

    .line 66
    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/n;->d:Ljavax/inject/Provider;

    .line 67
    iput-object p5, p0, Lcom/swedbank/mobile/app/transfer/n;->e:Ljavax/inject/Provider;

    .line 68
    iput-object p6, p0, Lcom/swedbank/mobile/app/transfer/n;->f:Ljavax/inject/Provider;

    .line 69
    iput-object p7, p0, Lcom/swedbank/mobile/app/transfer/n;->g:Ljavax/inject/Provider;

    .line 70
    iput-object p8, p0, Lcom/swedbank/mobile/app/transfer/n;->h:Ljavax/inject/Provider;

    .line 71
    iput-object p9, p0, Lcom/swedbank/mobile/app/transfer/n;->i:Ljavax/inject/Provider;

    .line 72
    iput-object p10, p0, Lcom/swedbank/mobile/app/transfer/n;->j:Ljavax/inject/Provider;

    .line 73
    iput-object p11, p0, Lcom/swedbank/mobile/app/transfer/n;->k:Ljavax/inject/Provider;

    .line 74
    iput-object p12, p0, Lcom/swedbank/mobile/app/transfer/n;->l:Ljavax/inject/Provider;

    .line 75
    iput-object p13, p0, Lcom/swedbank/mobile/app/transfer/n;->m:Ljavax/inject/Provider;

    .line 76
    iput-object p14, p0, Lcom/swedbank/mobile/app/transfer/n;->n:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/transfer/n;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/j;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/search/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/a/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/transfer/c/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/f;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/payment/form/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/search/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/detailed/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/request/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/transfer/request/opening/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/transfer/n;"
        }
    .end annotation

    .line 98
    new-instance v15, Lcom/swedbank/mobile/app/transfer/n;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/swedbank/mobile/app/transfer/n;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/transfer/m;
    .locals 17

    move-object/from16 v0, p0

    .line 81
    new-instance v16, Lcom/swedbank/mobile/app/transfer/m;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/swedbank/mobile/app/transfer/payment/form/j;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->b:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/swedbank/mobile/app/transfer/search/a;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->c:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/swedbank/mobile/app/transfer/a/a;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->d:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/swedbank/mobile/app/transfer/c/b;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->e:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/swedbank/mobile/app/transfer/c/a/b;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->f:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/util/List;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->g:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/swedbank/mobile/business/transfer/payment/form/g;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->h:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/swedbank/mobile/business/transfer/search/c;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->i:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/swedbank/mobile/business/transfer/detailed/c;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->j:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/swedbank/mobile/business/transfer/request/d;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->k:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/swedbank/mobile/business/transfer/request/opening/f;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->l:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/swedbank/mobile/architect/business/c;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->m:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/swedbank/mobile/architect/a/b/f;

    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/n;->n:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/swedbank/mobile/architect/a/b/g;

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/swedbank/mobile/app/transfer/m;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/j;Lcom/swedbank/mobile/app/transfer/search/a;Lcom/swedbank/mobile/app/transfer/a/a;Lcom/swedbank/mobile/app/transfer/c/b;Lcom/swedbank/mobile/app/transfer/c/a/b;Ljava/util/List;Lcom/swedbank/mobile/business/transfer/payment/form/g;Lcom/swedbank/mobile/business/transfer/search/c;Lcom/swedbank/mobile/business/transfer/detailed/c;Lcom/swedbank/mobile/business/transfer/request/d;Lcom/swedbank/mobile/business/transfer/request/opening/f;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v16
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/n;->a()Lcom/swedbank/mobile/app/transfer/m;

    move-result-object v0

    return-object v0
.end method
