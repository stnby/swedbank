.class final Lcom/swedbank/mobile/app/transfer/m$j;
.super Lkotlin/e/b/k;
.source "TransferRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/m;->b(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/f;

.field final synthetic b:Lcom/swedbank/mobile/app/transfer/m;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/f;Lcom/swedbank/mobile/app/transfer/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/m$j;->a:Lcom/swedbank/mobile/app/transfer/f;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/m$j;->b:Lcom/swedbank/mobile/app/transfer/m;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    check-cast p1, Ljava/lang/Iterable;

    .line 145
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 146
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 144
    instance-of v0, v0, Lcom/swedbank/mobile/business/transfer/detailed/d;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_4

    .line 110
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/m$j;->b:Lcom/swedbank/mobile/app/transfer/m;

    .line 104
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m$j;->b:Lcom/swedbank/mobile/app/transfer/m;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/m;->e(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/app/transfer/a/a;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/m$j;->a:Lcom/swedbank/mobile/app/transfer/f;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/transfer/f;->h()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_3

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/a/a;->a(I)Lcom/swedbank/mobile/app/transfer/a/a;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/m$j;->a:Lcom/swedbank/mobile/app/transfer/f;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/transfer/f;->c()Lcom/swedbank/mobile/app/plugins/list/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/a/a;->a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/app/transfer/a/a;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/m$j;->b:Lcom/swedbank/mobile/app/transfer/m;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/m;->f(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/business/transfer/detailed/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/a/a;->a(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/app/transfer/a/a;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 149
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 105
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Detailed title res must be defined in TransferPluginDetails"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_4
    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/m$j;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
