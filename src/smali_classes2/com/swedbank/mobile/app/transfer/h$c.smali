.class final synthetic Lcom/swedbank/mobile/app/transfer/h$c;
.super Lkotlin/e/b/i;
.source "TransferPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/h;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/transfer/s;",
        "Lcom/swedbank/mobile/app/transfer/s$a;",
        "Lcom/swedbank/mobile/app/transfer/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/h;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/s;Lcom/swedbank/mobile/app/transfer/s$a;)Lcom/swedbank/mobile/app/transfer/s;
    .locals 11
    .param p1    # Lcom/swedbank/mobile/app/transfer/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/s$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/h$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/transfer/h;

    .line 182
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/s$a$g;

    if-eqz v0, :cond_2

    .line 184
    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a$g;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/s;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 185
    :cond_0
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$g;->a()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7e

    const/4 v9, 0x0

    move-object v0, p1

    .line 189
    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto/16 :goto_0

    .line 192
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/s$a$b;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 193
    new-instance v5, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 194
    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$b;->a()Ljava/util/List;

    move-result-object v0

    .line 195
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$b;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object v1

    .line 196
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$b;->c()Z

    move-result p2

    .line 193
    invoke-direct {v5, v0, v1, p2}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x77

    const/4 v10, 0x0

    move-object v1, p1

    .line 192
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto/16 :goto_0

    .line 197
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/transfer/s$a$d;->a:Lcom/swedbank/mobile/app/transfer/s$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 198
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/s;->e()Z

    move-result p2

    if-eqz p2, :cond_4

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x6e

    const/4 v9, 0x0

    move-object v0, p1

    .line 203
    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto/16 :goto_0

    .line 207
    :cond_5
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/s$a$c;

    if-eqz v0, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 209
    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x5b

    const/4 v10, 0x0

    move-object v1, p1

    .line 207
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto/16 :goto_0

    .line 210
    :cond_6
    sget-object v0, Lcom/swedbank/mobile/app/transfer/s$a$e;->a:Lcom/swedbank/mobile/app/transfer/s$a$e;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 214
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/s;->e()Z

    move-result p2

    if-nez p2, :cond_7

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v0, p1

    .line 215
    invoke-static/range {v0 .. v9}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto :goto_0

    .line 219
    :cond_8
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/s$a$a;

    if-eqz v0, :cond_9

    const/4 v2, 0x0

    .line 220
    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$a;->a()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7d

    const/4 v10, 0x0

    move-object v1, p1

    .line 219
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto :goto_0

    .line 221
    :cond_9
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/s$a$h;

    if-eqz v0, :cond_a

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 222
    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a$h;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$h;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1b

    const/4 v10, 0x0

    move-object v1, p1

    .line 221
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    goto :goto_0

    .line 225
    :cond_a
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/s$a$f;

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 226
    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/s$a$f;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v8

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/16 v9, 0x1b

    const/4 v10, 0x0

    move-object v1, p1

    .line 225
    invoke-static/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/s;->a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/app/transfer/s;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/s$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/h$c;->a(Lcom/swedbank/mobile/app/transfer/s;Lcom/swedbank/mobile/app/transfer/s$a;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/h;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/transfer/TransferViewState;Lcom/swedbank/mobile/app/transfer/TransferViewState$PartialState;)Lcom/swedbank/mobile/app/transfer/TransferViewState;"

    return-object v0
.end method
