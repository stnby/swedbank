.class final Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;
.super Lkotlin/e/b/k;
.source "PaymentFormLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->b:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;
    .locals 23
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v3, p0

    move-object/from16 v0, p1

    move-object/from16 v7, p2

    move-object/from16 v1, p0

    const-string v2, "resources"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "metrics"

    invoke-static {v7, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0x48

    int-to-float v2, v2

    const/4 v9, 0x1

    .line 142
    invoke-static {v9, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    const/16 v2, 0xc

    int-to-float v2, v2

    const/4 v5, 0x2

    .line 143
    invoke-static {v5, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v11

    const/16 v2, 0x24

    int-to-float v2, v2

    .line 144
    invoke-static {v9, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v13

    const/16 v2, 0x12

    int-to-float v2, v2

    .line 145
    invoke-static {v9, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v14

    const/16 v2, 0x8

    int-to-float v2, v2

    .line 146
    invoke-static {v9, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v10

    const/16 v2, 0x10

    int-to-float v2, v2

    .line 147
    invoke-static {v9, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    const/16 v5, 0x60

    int-to-float v5, v5

    .line 148
    invoke-static {v9, v5, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v12

    .line 49
    iget-object v5, v3, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->b:Landroid/content/Context;

    sget v6, Lcom/swedbank/mobile/app/transfer/a$b;->background_color:I

    .line 150
    invoke-static {v5, v6}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v17

    move/from16 v15, v17

    .line 50
    new-instance v5, Landroid/graphics/Paint;

    move-object v8, v5

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    new-instance v6, Landroid/graphics/Paint;

    move-object v5, v6

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    const/16 v16, 0x28

    invoke-static/range {v16 .. v16}, Lcom/swedbank/mobile/core/ui/ag;->a(I)I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    sget v9, Lcom/swedbank/mobile/app/transfer/a$c;->list_item_separator_horizontal_padding:I

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    move-object/from16 v19, v1

    move-object v1, v6

    move v6, v9

    move/from16 v21, v2

    move/from16 v20, v9

    const/4 v9, 0x1

    int-to-float v2, v9

    .line 151
    invoke-static {v9, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 54
    new-instance v2, Lcom/swedbank/mobile/core/ui/m;

    move/from16 v22, v4

    move/from16 v4, v20

    move-object v9, v2

    invoke-direct {v2, v0, v4, v1}, Lcom/swedbank/mobile/core/ui/m;-><init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V

    .line 59
    new-instance v18, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;

    move-object/from16 v0, v18

    iget-object v1, v3, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView;

    move-object/from16 v16, v1

    check-cast v16, Landroid/view/View;

    const/4 v1, 0x0

    move v3, v1

    move-object/from16 v1, v19

    move/from16 v2, v21

    move/from16 v4, v22

    invoke-direct/range {v0 .. v17}, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;FFFLandroid/graphics/Paint;FFLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFILandroid/view/View;I)V

    return-object v18
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
