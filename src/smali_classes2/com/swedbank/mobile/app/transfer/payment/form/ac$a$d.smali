.class public final Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;
.super Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;
.source "PaymentFormViewState.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Lcom/swedbank/mobile/app/w/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZZLcom/swedbank/mobile/app/w/b;)V
    .locals 1
    .param p4    # Lcom/swedbank/mobile/app/w/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;-><init>(Lkotlin/e/b/g;)V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->a:Z

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->b:Z

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->d:Lcom/swedbank/mobile/app/w/b;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->b:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->c:Z

    return v0
.end method

.method public final d()Lcom/swedbank/mobile/app/w/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->d:Lcom/swedbank/mobile/app/w/b;

    return-object v0
.end method
