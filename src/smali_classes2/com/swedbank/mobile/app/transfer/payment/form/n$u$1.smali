.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->a(Lkotlin/k;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n$u;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n$u;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$u;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    const-string v0, "executionResultStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    check-cast p1, Lio/reactivex/s;

    .line 194
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1$1;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/o;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 200
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$u;

    iget-object v1, v1, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->b(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/app/transfer/payment/form/d;

    move-result-object v1

    .line 201
    invoke-interface {v1}, Lcom/swedbank/mobile/app/transfer/payment/form/d;->b()Lio/reactivex/o;

    move-result-object v1

    .line 202
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$u;

    iget-object v2, v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->b(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/app/transfer/payment/form/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/d;->a()Lcom/swedbank/mobile/business/general/a;

    move-result-object v2

    .line 396
    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1$a;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1$a;-><init>(Lcom/swedbank/mobile/business/general/a;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->l(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 397
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/y;->a()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/general/a;->b()Lcom/swedbank/mobile/business/util/y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/y;->b()Ljava/util/concurrent/TimeUnit;

    move-result-object v2

    invoke-virtual {v1, v3, v4, v2}, Lio/reactivex/o;->c(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "repeatWhen { it.flatMap \u2026ime, pollData.delay.unit)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    sget-object v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1$2;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1$2;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/form/q;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/q;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 204
    invoke-virtual {v1, p1}, Lio/reactivex/o;->h(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 192
    invoke-static {p1, v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
