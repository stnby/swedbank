.class public final Lcom/swedbank/mobile/app/transfer/payment/b/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PinChallengePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/payment/b/j;",
        "Lcom/swedbank/mobile/app/transfer/payment/b/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/b/c;)Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 19
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;

    .line 20
    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;->b()Lio/reactivex/w;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/b/c$b;->a:Lcom/swedbank/mobile/app/transfer/payment/b/c$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/b/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/payment/b/f;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/b/c$e;->a:Lcom/swedbank/mobile/app/transfer/payment/b/c$e;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 25
    sget-object v2, Lcom/swedbank/mobile/app/transfer/payment/b/c$f;->a:Lcom/swedbank/mobile/app/transfer/payment/b/c$f;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 27
    sget-object v2, Lcom/swedbank/mobile/app/transfer/payment/b/c$g;->a:Lcom/swedbank/mobile/app/transfer/payment/b/c$g;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 28
    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/b/c$h;

    iget-object v4, p0, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/transfer/payment/b/c$h;-><init>(Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/a;)V

    check-cast v3, Lkotlin/e/a/b;

    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/b/e;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/transfer/payment/b/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 29
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/b/c$i;->a:Lcom/swedbank/mobile/app/transfer/payment/b/c$i;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 31
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/b/c$c;->a:Lcom/swedbank/mobile/app/transfer/payment/b/c$c;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/transfer/payment/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 32
    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/b/c$d;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/transfer/payment/b/c$d;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/c;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 33
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 34
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 37
    check-cast v0, Lio/reactivex/s;

    .line 38
    check-cast v1, Lio/reactivex/s;

    .line 39
    check-cast v2, Lio/reactivex/s;

    .line 40
    check-cast v3, Lio/reactivex/s;

    .line 36
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026      declineClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/b/m;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/app/transfer/payment/b/m;-><init>(Ljava/lang/String;ZZILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/b/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/transfer/payment/b/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/transfer/payment/b/c$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 55
    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/b/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/payment/b/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 56
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
