.class public final Lcom/swedbank/mobile/app/transfer/payment/a/g;
.super Lcom/swedbank/mobile/architect/a/h;
.source "PaymentExecutionRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/payment/execution/p;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/d/a;

.field private final f:Lcom/swedbank/mobile/app/transfer/payment/b/a;

.field private final g:Lcom/swedbank/mobile/app/c/a/g;

.field private final h:Lcom/swedbank/mobile/business/challenge/d;

.field private final i:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/d/a;Lcom/swedbank/mobile/app/transfer/payment/b/a;Lcom/swedbank/mobile/app/c/a/g;Lcom/swedbank/mobile/business/challenge/d;Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/payment/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/c/a/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/challenge/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_execution"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_execution"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_execution"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/d/a;",
            "Lcom/swedbank/mobile/app/transfer/payment/b/a;",
            "Lcom/swedbank/mobile/app/c/a/g;",
            "Lcom/swedbank/mobile/business/challenge/d;",
            "Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "challengeBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinChallengeBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "biometricAuthenticationPromptBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challengeListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinChallengeListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    .line 35
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->e:Lcom/swedbank/mobile/app/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->f:Lcom/swedbank/mobile/app/transfer/payment/b/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->g:Lcom/swedbank/mobile/app/c/a/g;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->h:Lcom/swedbank/mobile/business/challenge/d;

    iput-object p5, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->i:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/a/g;)Lcom/swedbank/mobile/app/d/a;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->e:Lcom/swedbank/mobile/app/d/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/a/g;)Lcom/swedbank/mobile/business/challenge/d;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->h:Lcom/swedbank/mobile/business/challenge/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/a/g;)Lcom/swedbank/mobile/app/transfer/payment/b/a;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->f:Lcom/swedbank/mobile/app/transfer/payment/b/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/payment/a/g;)Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->i:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/payment/a/g;)Lcom/swedbank/mobile/app/c/a/g;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g;->g:Lcom/swedbank/mobile/app/c/a/g;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 81
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/a/g$b;->a:Lcom/swedbank/mobile/app/transfer/payment/a/g$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/challenge/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/challenge/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "challengeInfo"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/a/g$e;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/a/g$e;-><init>(Lcom/swedbank/mobile/app/transfer/payment/a/g;Lcom/swedbank/mobile/business/challenge/a;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/a/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/biometric/authentication/h;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/biometric/authentication/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;-><init>(Lcom/swedbank/mobile/app/transfer/payment/a/g;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/biometric/authentication/h;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/a/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "challengeCode"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/a/g$f;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/a/g$f;-><init>(Lcom/swedbank/mobile/app/transfer/payment/a/g;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/a/g;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 83
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/a/g$c;->a:Lcom/swedbank/mobile/app/transfer/payment/a/g$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 85
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/a/g$a;->a:Lcom/swedbank/mobile/app/transfer/payment/a/g$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
