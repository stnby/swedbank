.class public final Lcom/swedbank/mobile/app/transfer/payment/form/s;
.super Lcom/swedbank/mobile/business/i/f;
.source "PaymentFormRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/payment/form/i;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/transfer/payment/a/b;

.field private final f:Lcom/swedbank/mobile/app/f/a/b;

.field private final g:Lcom/swedbank/mobile/app/f/a;

.field private final h:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

.field private final i:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

.field private final j:Lcom/swedbank/mobile/business/general/confirmation/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/a/b;Lcom/swedbank/mobile/app/f/a/b;Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/business/transfer/payment/execution/m;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/transfer/payment/execution/m;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_form"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/payment/a/b;",
            "Lcom/swedbank/mobile/app/f/a/b;",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/m;",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentExecutionBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentExecutionListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogListener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p7, p9, p8}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->e:Lcom/swedbank/mobile/app/transfer/payment/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->f:Lcom/swedbank/mobile/app/f/a/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->g:Lcom/swedbank/mobile/app/f/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->h:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    iput-object p5, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->i:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    iput-object p6, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->j:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/app/transfer/payment/a/b;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->e:Lcom/swedbank/mobile/app/transfer/payment/a/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/s;Lkotlin/e/a/b;)V
    .locals 0

    .line 27
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/s;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/business/transfer/payment/execution/m;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->h:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->f:Lcom/swedbank/mobile/app/f/a/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->i:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->g:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s;->j:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 88
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "paymentExecutionUuid"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/s;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/s;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/s$d;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/s;)V

    check-cast v0, Lio/reactivex/z;

    invoke-static {v0}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.create { emitter \u2026cutionAttached)\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()V
    .locals 1

    .line 63
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$f;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/s$f;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/s;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/s;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 90
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$b;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public e()V
    .locals 1

    .line 75
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/s$e;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/s;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/s;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public f()V
    .locals 1

    .line 92
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/s$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
