.class public final Lcom/swedbank/mobile/app/transfer/payment/form/v$c;
.super Lkotlin/e/b/k;
.source "PaymentFormViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/Boolean;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/v;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/v;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 6

    .line 510
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v0

    .line 527
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-string v1, "nextField.text"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x5

    const/4 v4, 0x6

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    goto :goto_1

    :cond_1
    const/4 v0, 0x5

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 511
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-string v5, "nextField.text"

    invoke-static {v0, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    const/4 v0, 0x6

    goto :goto_3

    :cond_3
    const/4 v0, 0x5

    :goto_3
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 512
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v0

    .line 531
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-string v5, "nextField.text"

    invoke-static {v0, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    if-eqz v1, :cond_5

    const/4 v3, 0x6

    :cond_5
    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 513
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->F(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;->a(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
