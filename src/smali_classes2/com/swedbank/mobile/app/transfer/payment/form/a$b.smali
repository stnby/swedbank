.class public final synthetic Lcom/swedbank/mobile/app/transfer/payment/form/a$b;
.super Lkotlin/e/b/i;
.source "AccountSelectionAddon.kt"

# interfaces
.implements Lkotlin/e/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/a<",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/payment/form/a;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "closeAccountSelection"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "closeAccountSelection()V"

    return-object v0
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/a;

    .line 102
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v1, 0x0

    .line 103
    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Z)V

    .line 104
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$d;->ic_arrow_drop_down:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public synthetic f_()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a$b;->d()V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    return-object v0
.end method
