.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$e;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$e;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
    .locals 18
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "it"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget-object v1, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;-><init>(Z)V

    .line 396
    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.just(this)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p0

    goto :goto_0

    .line 88
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v1, :cond_1

    move-object/from16 v1, p0

    iget-object v2, v1, Lcom/swedbank/mobile/app/transfer/payment/form/n$e;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 89
    new-instance v10, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 91
    check-cast v0, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v8, 0x6

    const/4 v9, 0x0

    move-object v3, v10

    .line 89
    invoke-direct/range {v3 .. v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;-><init>(ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    move-object v12, v10

    check-cast v12, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    .line 397
    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v11

    .line 399
    sget-object v13, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    const-wide/16 v14, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    .line 397
    invoke-static/range {v11 .. v17}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    move-object/from16 v1, p0

    .line 399
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$e;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
