.class public final Lcom/swedbank/mobile/app/transfer/payment/form/b;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "AccountSelectionOverlay.kt"


# instance fields
.field private a:Lkotlin/e/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Lcom/swedbank/mobile/core/ui/widget/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/k;

    .line 47
    sget p3, Lcom/swedbank/mobile/app/transfer/a$g;->item_account_selectable:I

    .line 48
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/b$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/b;)V

    check-cast v0, Lkotlin/e/a/m;

    .line 46
    invoke-static {p3, v0}, Lcom/swedbank/mobile/core/ui/widget/l;->b(ILkotlin/e/a/m;)Landroid/util/SparseArray;

    move-result-object p3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 45
    invoke-direct {p2, v0, p3, v1, v0}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->d:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 63
    sget p2, Lcom/swedbank/mobile/app/transfer/a$e;->account_selection_overlay:I

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setId(I)V

    .line 64
    new-instance p2, Landroidx/recyclerview/widget/RecyclerView$j;

    const/4 p3, -0x1

    const/4 v0, 0x0

    invoke-direct {p2, p3, v0}, Landroidx/recyclerview/widget/RecyclerView$j;-><init>(II)V

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/swedbank/mobile/app/transfer/a$c;->toolbar_elevation:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setElevation(F)V

    .line 66
    sget p2, Lcom/swedbank/mobile/app/transfer/a$b;->transfer_account_selection_overlay:I

    .line 118
    invoke-static {p1, p2}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result p2

    .line 66
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setBackgroundColor(I)V

    .line 67
    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p2, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 68
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/f;

    .line 71
    sget-object p3, Lcom/swedbank/mobile/core/ui/widget/g$b;->a:Lcom/swedbank/mobile/core/ui/widget/g$b;

    move-object v4, p3

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, p2

    move-object v1, p1

    .line 68
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 74
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->d:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 30
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 31
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/transfer/payment/form/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/b;)Lkotlin/e/a/b;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->a:Lkotlin/e/a/b;

    return-object p0
.end method


# virtual methods
.method public final a(Landroidx/constraintlayout/widget/ConstraintLayout;I)V
    .locals 4
    .param p1    # Landroidx/constraintlayout/widget/ConstraintLayout;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "constraintLayout"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->addView(Landroid/view/View;)V

    .line 84
    new-instance v0, Landroidx/constraintlayout/widget/b;

    invoke-direct {v0}, Landroidx/constraintlayout/widget/b;-><init>()V

    .line 85
    invoke-virtual {v0, p1}, Landroidx/constraintlayout/widget/b;->a(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 86
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getId()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v3, p2, v2}, Landroidx/constraintlayout/widget/b;->a(IIII)V

    .line 87
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getId()I

    move-result p2

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v2, v1, v2}, Landroidx/constraintlayout/widget/b;->a(IIII)V

    .line 88
    invoke-virtual {v0, p1}, Landroidx/constraintlayout/widget/b;->b(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->d:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast p1, Ljava/lang/Iterable;

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 114
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 78
    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/widget/l;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/core/ui/widget/i;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_0
    check-cast v1, Ljava/util/List;

    const/4 p1, 0x2

    const/4 v2, 0x0

    .line 78
    invoke-static {v0, v1, v2, p1, v2}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 93
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 94
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 95
    iput-boolean v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->c:Z

    .line 97
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 98
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getTranslationX()F

    move-result v2

    .line 99
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getTranslationY()F

    move-result v3

    .line 105
    iget-boolean v4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->c:Z

    if-eqz v4, :cond_1

    .line 101
    invoke-virtual {p0, v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpl-float v4, v0, v4

    if-ltz v4, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getTop()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->getBottom()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->b:Lkotlin/e/a/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/s;

    :cond_1
    const/4 v0, 0x0

    .line 108
    iput-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->c:Z

    .line 110
    :cond_2
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final setCancelListener(Lkotlin/e/a/a;)V
    .locals 1
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->b:Lkotlin/e/a/a;

    return-void
.end method

.method public final setListener(Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/b<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b;->a:Lkotlin/e/a/b;

    return-void
.end method
