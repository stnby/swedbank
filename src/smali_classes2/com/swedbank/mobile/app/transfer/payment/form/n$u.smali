.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$u;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

.field final synthetic b:Lio/reactivex/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;Lio/reactivex/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->b:Lio/reactivex/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "+",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    .line 191
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->b:Lio/reactivex/o;

    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$u$1;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n$u;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;->a(Lkotlin/k;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
