.class public final Lcom/swedbank/mobile/app/transfer/payment/form/c;
.super Lcom/swedbank/mobile/app/f/a/d$b;
.source "InstantPaymentRetryDialogInformation.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/payment/form/c;

.field private static final b:I

.field private static final c:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/c;

    .line 10
    sget v0, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_instant_payment_retry_title:I

    sput v0, Lcom/swedbank/mobile/app/transfer/payment/form/c;->b:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/app/f/a/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 10
    sget v0, Lcom/swedbank/mobile/app/transfer/payment/form/c;->b:I

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 11
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/c;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/f/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x3

    .line 13
    new-array v0, v0, [Lcom/swedbank/mobile/app/f/a/a;

    .line 14
    new-instance v7, Lcom/swedbank/mobile/app/f/a/a;

    .line 15
    sget-object v2, Lcom/swedbank/mobile/business/transfer/payment/form/j;->a:Lcom/swedbank/mobile/business/transfer/payment/form/j;

    .line 16
    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_instant_payment_retry_btn:I

    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    .line 14
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;ILkotlin/e/b/g;)V

    const/4 v1, 0x0

    aput-object v7, v0, v1

    .line 17
    new-instance v1, Lcom/swedbank/mobile/app/f/a/a;

    .line 18
    sget-object v9, Lcom/swedbank/mobile/business/transfer/payment/form/j;->b:Lcom/swedbank/mobile/business/transfer/payment/form/j;

    .line 19
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_instant_payment_retry_as_regular_btn:I

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v8, v1

    .line 17
    invoke-direct/range {v8 .. v13}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;ILkotlin/e/b/g;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 20
    new-instance v1, Lcom/swedbank/mobile/app/f/a/a;

    .line 21
    sget-object v4, Lcom/swedbank/mobile/business/transfer/payment/form/j;->c:Lcom/swedbank/mobile/business/transfer/payment/form/j;

    .line 22
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_instant_payment_retry_cancel_btn:I

    invoke-static {v2}, Lcom/swedbank/mobile/core/ui/ap;->a(I)Lkotlin/e/a/b;

    move-result-object v5

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v1

    .line 20
    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/app/f/a/a;-><init>(Ljava/lang/Object;Lkotlin/e/a/b;Ljava/lang/Integer;ILkotlin/e/b/g;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 13
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
