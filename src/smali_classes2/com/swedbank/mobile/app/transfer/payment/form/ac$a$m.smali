.class public final Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;
.super Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;
.source "PaymentFormViewState.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "m"
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZZLcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p4    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "error"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 80
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;-><init>(Lkotlin/e/b/g;)V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->a:Z

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->b:Z

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->d:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method

.method public synthetic constructor <init>(ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    const/4 p3, 0x0

    .line 78
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;-><init>(ZZZLcom/swedbank/mobile/business/util/e;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->b:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->c:Z

    return v0
.end method

.method public final d()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->d:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method
