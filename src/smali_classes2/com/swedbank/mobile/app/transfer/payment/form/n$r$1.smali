.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n$r;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/f;)Lio/reactivex/o;
    .locals 20
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/f;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "it"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;

    if-eqz v2, :cond_8

    .line 133
    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    iget-object v2, v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->e()Ljava/util/List;

    move-result-object v2

    .line 396
    new-instance v3, Landroidx/c/a;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Landroidx/c/a;-><init>(I)V

    .line 397
    new-instance v4, Landroidx/c/g;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Landroidx/c/g;-><init>(I)V

    .line 398
    check-cast v2, Ljava/lang/Iterable;

    .line 399
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/g;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/g;->c()Lcom/swedbank/mobile/business/transfer/payment/a;

    move-result-object v5

    .line 401
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/form/z;->a()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v7, v3

    check-cast v7, Landroidx/c/g;

    .line 402
    invoke-virtual {v7, v5, v6}, Landroidx/c/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 404
    :cond_0
    invoke-virtual {v4, v5, v6}, Landroidx/c/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 409
    :cond_1
    invoke-virtual {v4}, Landroidx/c/g;->size()I

    move-result v2

    .line 410
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v2, :cond_2

    .line 412
    invoke-virtual {v4, v7}, Landroidx/c/g;->c(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 414
    :cond_2
    check-cast v5, Ljava/util/List;

    .line 408
    invoke-static {v3, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v3

    .line 133
    move-object v9, v3

    check-cast v9, Ljava/util/Map;

    invoke-virtual {v2}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v17, v2

    check-cast v17, Ljava/util/List;

    .line 135
    iget-object v8, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    const-string v2, "paymentInput"

    invoke-static {v8, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->a()Ljava/lang/String;

    move-result-object v10

    .line 138
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->c()Ljava/lang/String;

    move-result-object v11

    .line 139
    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    iget-object v2, v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 415
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->b()Lcom/swedbank/mobile/business/transfer/payment/execution/q;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_4

    .line 417
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/q;->a()Ljava/math/BigDecimal;

    move-result-object v7

    sget-object v12, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v7, v12}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v7

    if-lez v7, :cond_3

    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->c(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Landroid/content/res/Resources;

    move-result-object v2

    sget v7, Lcom/swedbank/mobile/app/transfer/a$h;->payment_service_fee:I

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    .line 418
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/q;->a()Ljava/math/BigDecimal;

    move-result-object v13

    const/4 v14, 0x3

    invoke-static {v13, v6, v6, v14, v4}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v6

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/execution/q;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v12, v5

    .line 417
    invoke-virtual {v2, v7, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 419
    :cond_3
    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->c(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/swedbank/mobile/app/transfer/a$h;->payment_service_fee_free:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object v12, v2

    goto :goto_3

    :cond_4
    move-object v12, v4

    .line 140
    :goto_3
    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    iget-object v2, v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 423
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 424
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->d()Ljava/util/List;

    move-result-object v3

    sget-object v6, Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;->INSTANT:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$b;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v3, "Swedbank"

    .line 425
    invoke-static {v1, v3, v5}, Lkotlin/j/n;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v5, :cond_5

    goto :goto_4

    .line 426
    :cond_5
    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->c(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_non_instant_payment_execution_info:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v13, v1

    goto :goto_5

    :cond_6
    :goto_4
    move-object v13, v4

    :goto_5
    const/4 v14, 0x0

    const/16 v15, 0x40

    const/16 v16, 0x0

    .line 134
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;

    move-object v7, v1

    invoke-direct/range {v7 .. v16}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/e/b/g;)V

    .line 142
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 428
    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.just(this)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 143
    :cond_7
    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    iget-object v2, v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x3f

    const/16 v19, 0x0

    move-object v10, v1

    invoke-static/range {v10 .. v19}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    .line 429
    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v3

    .line 431
    sget-object v5, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    const-wide/16 v6, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    .line 429
    invoke-static/range {v3 .. v9}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    goto :goto_6

    .line 147
    :cond_8
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    iget-object v2, v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 148
    new-instance v10, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 150
    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/f$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v8, 0x5

    const/4 v9, 0x0

    move-object v3, v10

    .line 148
    invoke-direct/range {v3 .. v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;-><init>(ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    move-object v12, v10

    check-cast v12, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    .line 432
    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v11

    .line 434
    sget-object v13, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    const-wide/16 v14, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    .line 432
    invoke-static/range {v11 .. v17}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    :goto_6
    return-object v1

    .line 434
    :cond_9
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/f;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
