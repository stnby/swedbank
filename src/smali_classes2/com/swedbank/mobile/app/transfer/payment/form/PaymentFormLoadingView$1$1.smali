.class public final Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "PaymentFormLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Landroid/graphics/Paint;

.field final synthetic f:F

.field final synthetic g:F

.field final synthetic h:Landroid/graphics/Paint;

.field final synthetic i:Lcom/swedbank/mobile/core/ui/m;

.field final synthetic j:F

.field final synthetic k:F

.field final synthetic l:F

.field final synthetic m:F

.field final synthetic n:F

.field final synthetic o:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;FFFLandroid/graphics/Paint;FFLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFILandroid/view/View;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFF",
            "Landroid/graphics/Paint;",
            "FF",
            "Landroid/graphics/Paint;",
            "Lcom/swedbank/mobile/core/ui/m;",
            "FFFFFI",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    .line 59
    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;

    move v1, p2

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->b:F

    move v1, p3

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->c:F

    move v1, p4

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->d:F

    move-object v1, p5

    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->e:Landroid/graphics/Paint;

    move v1, p6

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->f:F

    move v1, p7

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->g:F

    move-object v1, p8

    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->h:Landroid/graphics/Paint;

    move-object v1, p9

    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    move/from16 v1, p10

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->j:F

    move/from16 v1, p11

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->k:F

    move/from16 v1, p12

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->l:F

    move/from16 v1, p13

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->m:F

    move/from16 v1, p14

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->n:F

    move/from16 v1, p15

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->o:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x1c

    const/4 v5, 0x0

    move-object p1, p0

    move-object/from16 p2, p16

    move/from16 p3, p17

    move p4, v1

    move p5, v2

    move p6, v3

    move p7, v4

    move-object p8, v5

    invoke-direct/range {p1 .. p8}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;)V
    .locals 16
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    const-string v3, "canvas"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v3, v1

    .line 62
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 67
    iget v5, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->b:F

    .line 68
    iget v6, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->c:F

    .line 70
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->d:F

    .line 73
    iget-object v8, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->e:Landroid/graphics/Paint;

    .line 74
    iget v9, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->f:F

    .line 75
    iget v10, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->g:F

    .line 76
    iget-object v11, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->h:Landroid/graphics/Paint;

    .line 77
    iget-object v12, v0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    add-float/2addr v5, v6

    .line 142
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v6

    add-float/2addr v6, v5

    add-float v13, v6, v7

    const/4 v14, 0x0

    .line 143
    invoke-virtual {v4, v14, v6, v3, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 144
    invoke-virtual {v2, v4, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v13, 0x0

    :goto_0
    const/4 v15, 0x3

    if-ge v13, v15, :cond_0

    .line 147
    iget v15, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v15, v10

    add-float v6, v15, v7

    .line 148
    invoke-virtual {v4, v14, v15, v3, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 149
    invoke-virtual {v2, v4, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_0
    float-to-int v6, v5

    const/4 v11, 0x0

    .line 156
    invoke-virtual {v12, v11, v6, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->a(IIILandroid/graphics/Canvas;)V

    .line 163
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v6

    add-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v15, :cond_1

    .line 165
    iget v6, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v6, v7

    sub-float v11, v3, v9

    add-float v13, v6, v10

    .line 166
    invoke-virtual {v4, v9, v6, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 167
    invoke-virtual {v2, v4, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 171
    :cond_1
    iget v3, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v7

    float-to-int v3, v3

    const/4 v4, 0x0

    .line 172
    invoke-virtual {v12, v4, v3, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->b(IIILandroid/graphics/Canvas;)V

    .line 178
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->b()F

    return-void
.end method

.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 10
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string p2, "canvas"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "itemPaint"

    invoke-static {p4, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float p1, p1

    const p2, 0x3dcccccd    # 0.1f

    mul-float p2, p2, p1

    const v0, 0x3e2e147b    # 0.17f

    mul-float v1, p1, v0

    .line 83
    iget v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->j:F

    iget v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->b:F

    add-float/2addr v2, v3

    .line 84
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 86
    iget v4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->b:F

    iget v5, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->c:F

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    invoke-virtual {v5}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v5

    add-float/2addr v4, v5

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x2

    const/4 v7, 0x4

    if-ge v5, v7, :cond_1

    .line 90
    iget v7, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->d:F

    iget v8, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->k:F

    sub-float/2addr v7, v8

    const/high16 v8, 0x3f400000    # 0.75f

    mul-float v7, v7, v8

    add-float/2addr v7, v4

    .line 91
    iget-object v8, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;

    iget-object v8, v8, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1;->a:Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView;

    packed-switch v5, :pswitch_data_0

    const v8, 0x3e851eb8    # 0.26f

    goto :goto_1

    :pswitch_0
    const v8, 0x3e570a3d    # 0.21f

    goto :goto_1

    :pswitch_1
    const v8, 0x3e2e147b    # 0.17f

    goto :goto_1

    :pswitch_2
    const v8, 0x3ed1eb85    # 0.41f

    :goto_1
    mul-float v8, v8, p1

    add-float/2addr v8, v2

    iget v9, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->k:F

    add-float/2addr v9, v7

    invoke-virtual {v3, v2, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 92
    invoke-virtual {p3, v3, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    if-ne v5, v6, :cond_0

    sub-float v6, p1, v2

    sub-float/2addr v6, p2

    add-float v8, v6, p2

    .line 97
    iget v9, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->k:F

    add-float/2addr v9, v7

    invoke-virtual {v3, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 98
    invoke-virtual {p3, v3, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 101
    :cond_0
    iget v6, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->d:F

    iget v7, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->g:F

    add-float/2addr v6, v7

    add-float/2addr v4, v6

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 105
    :cond_1
    iget p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->b:F

    add-float/2addr p2, v4

    sub-float v0, p1, v2

    sub-float/2addr v0, v1

    add-float/2addr v1, v0

    .line 107
    iget v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->k:F

    add-float/2addr v2, p2

    invoke-virtual {v3, v0, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 108
    invoke-virtual {p3, v3, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 111
    iget p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->b:F

    iget v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->l:F

    add-float/2addr p2, v0

    add-float/2addr v4, p2

    const p2, 0x3f266666    # 0.65f

    mul-float p2, p2, p1

    sub-float/2addr p1, p2

    int-to-float v0, v6

    div-float/2addr p1, v0

    add-float/2addr p2, p1

    .line 114
    iget v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->m:F

    add-float/2addr v0, v4

    invoke-virtual {v3, p1, v4, p2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 115
    iget p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->n:F

    iget p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormLoadingView$1$1;->n:F

    invoke-virtual {p3, v3, p1, p2, p4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
