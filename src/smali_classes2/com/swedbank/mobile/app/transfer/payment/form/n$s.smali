.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$s;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;)Lio/reactivex/o;
    .locals 18
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "it"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 165
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$e;

    invoke-direct {v1, v3}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$e;-><init>(Z)V

    .line 396
    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.just(this)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 167
    :cond_0
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    if-eqz v2, :cond_5

    .line 168
    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$a;->b()Ljava/util/List;

    move-result-object v1

    .line 397
    new-instance v2, Landroidx/c/a;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v4}, Landroidx/c/a;-><init>(I)V

    .line 398
    new-instance v4, Landroidx/c/g;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Landroidx/c/g;-><init>(I)V

    .line 399
    check-cast v1, Ljava/lang/Iterable;

    .line 400
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/transfer/payment/execution/g;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/g;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/execution/g;->c()Lcom/swedbank/mobile/business/transfer/payment/a;

    move-result-object v5

    .line 402
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/form/z;->a()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v7, v2

    check-cast v7, Landroidx/c/g;

    .line 403
    invoke-virtual {v7, v5, v6}, Landroidx/c/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 405
    :cond_1
    invoke-virtual {v4, v5, v6}, Landroidx/c/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 410
    :cond_2
    invoke-virtual {v4}, Landroidx/c/g;->size()I

    move-result v1

    .line 411
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    if-ge v3, v1, :cond_3

    .line 413
    invoke-virtual {v4, v3}, Landroidx/c/g;->c(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 415
    :cond_3
    check-cast v5, Ljava/util/List;

    .line 409
    invoke-static {v2, v5}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    .line 168
    check-cast v2, Ljava/util/Map;

    invoke-virtual {v1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 170
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 171
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3, v4}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;-><init>(Ljava/util/Map;Ljava/util/List;ILkotlin/e/b/g;)V

    .line 416
    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.just(this)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 173
    :cond_4
    iget-object v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;

    invoke-direct {v4, v2, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;-><init>(Ljava/util/Map;Ljava/util/List;)V

    move-object v6, v4

    check-cast v6, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    .line 417
    invoke-static {v3}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v5

    .line 419
    sget-object v7, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    .line 417
    invoke-static/range {v5 .. v11}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    goto :goto_2

    .line 178
    :cond_5
    instance-of v2, v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 179
    new-instance v10, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 181
    check-cast v1, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object v3, v10

    .line 179
    invoke-direct/range {v3 .. v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;-><init>(ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    move-object v12, v10

    check-cast v12, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    .line 420
    invoke-static {v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v11

    .line 422
    sget-object v13, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    const-wide/16 v14, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    .line 420
    invoke-static/range {v11 .. v17}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    :goto_2
    return-object v1

    .line 422
    :cond_6
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
