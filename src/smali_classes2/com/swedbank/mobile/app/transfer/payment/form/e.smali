.class public final Lcom/swedbank/mobile/app/transfer/payment/form/e;
.super Ljava/lang/Object;
.source "PaymentExecutionLoadingMessagesProvider.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/payment/form/d;


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/e;->a:Landroid/app/Application;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/e;)Landroid/app/Application;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/e;->a:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/business/general/a;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    new-instance v0, Lcom/swedbank/mobile/business/general/a;

    .line 23
    new-instance v1, Lcom/swedbank/mobile/business/util/y;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x5

    invoke-direct {v1, v3, v4, v2}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 24
    new-instance v2, Lcom/swedbank/mobile/business/util/y;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v2, v3, v4, v5}, Lcom/swedbank/mobile/business/util/y;-><init>(JLjava/util/concurrent/TimeUnit;)V

    .line 22
    invoke-direct {v0, v1, v2}, Lcom/swedbank/mobile/business/general/a;-><init>(Lcom/swedbank/mobile/business/util/y;Lcom/swedbank/mobile/business/util/y;)V

    return-object v0
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/e$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/e$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/e;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/o;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.fromCallable \u2026es)\n        .random()\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
