.class public final Lcom/swedbank/mobile/app/transfer/payment/b/a;
.super Ljava/lang/Object;
.source "PinChallengeBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

.field private final c:Lcom/swedbank/mobile/a/ac/d/c/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/ac/d/c/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/ac/d/c/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/a;->c:Lcom/swedbank/mobile/a/ac/d/c/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;)Lcom/swedbank/mobile/app/transfer/payment/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pinChallengeListener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/b/a;

    .line 20
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/b/a;->b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/transfer/payment/b/a;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "challengeCode"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/b/a;

    .line 16
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/b/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/a;->c:Lcom/swedbank/mobile/a/ac/d/c/a$a;

    .line 24
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/c/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/ac/d/c/a$a;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/a;->b:Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/c/a$a;->b(Lcom/swedbank/mobile/business/transfer/payment/pinchallenge/c;)Lcom/swedbank/mobile/a/ac/d/c/a$a;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/d/c/a$a;->a()Lcom/swedbank/mobile/a/ac/d/c/a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/d/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display PIN challenge without parent node"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 24
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display PIN challenge without challenge code"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
