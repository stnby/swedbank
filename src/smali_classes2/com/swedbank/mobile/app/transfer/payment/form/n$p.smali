.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$p;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$p;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/a/a;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
    .locals 12
    .param p1    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$p;->a:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfe

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;ILjava/lang/Object;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/a/a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$p;->a(Lcom/swedbank/mobile/business/a/a;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    return-object p1
.end method
