.class public final Lcom/swedbank/mobile/app/transfer/payment/a/d$b;
.super Ljava/lang/Object;
.source "PaymentExecutionResultNotificationProviderImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/a/d;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/e/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/a/d;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/execution/n;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            "Lcom/swedbank/mobile/business/transfer/payment/execution/n;",
            ")V"
        }
    .end annotation

    .line 38
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->a:Lcom/swedbank/mobile/app/transfer/payment/a/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p3, "payment_execution_result_notification_channel_id"

    .line 39
    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->d:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;

    move-result-object p3

    sget v0, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_payment_execution_notification_channel_name:I

    invoke-virtual {p3, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string v0, "application.getString(R.\u2026otification_channel_name)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->e:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;

    move-result-object p3

    sget v0, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_payment_execution_notification_success_title:I

    invoke-virtual {p3, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string v0, "application.getString(R.\u2026tification_success_title)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->f:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;

    move-result-object p1

    sget p3, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_payment_execution_notification_success_text:I

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p1, p3, v0}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "application.getString(R.\u2026ext, input.recipientName)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public e()Landroid/os/Bundle;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x2

    .line 44
    new-array v0, v0, [Lkotlin/k;

    .line 45
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/a/e;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->a:Lcom/swedbank/mobile/app/transfer/payment/a/d;

    invoke-static {v3}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->a(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Lcom/squareup/moshi/n;

    move-result-object v3

    .line 83
    const-class v4, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {v3, v4}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jsonMapper.adapter(T::class.java).toJson(this)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 46
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/a/e;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;->c:Lcom/swedbank/mobile/business/transfer/payment/execution/n;

    if-eqz v2, :cond_0

    check-cast v2, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/execution/n$c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 44
    invoke-static {v0}, Landroidx/core/os/b;->a([Lkotlin/k;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0

    .line 46
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.swedbank.mobile.business.transfer.payment.execution.PaymentExecutionResult.Success"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
