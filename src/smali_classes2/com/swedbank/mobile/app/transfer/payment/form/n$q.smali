.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$q;
.super Lkotlin/e/b/k;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/app/transfer/payment/form/u;",
        "Lio/reactivex/o<",
        "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/payment/form/n$q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$q;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$q;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$q;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$q;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/payment/form/u;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/form/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/payment/form/u;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-interface {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/u;->f()Lio/reactivex/o;

    move-result-object v0

    .line 123
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/n$q$1;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$q$1;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/u;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object p1

    .line 125
    sget-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$q$2;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$q$2;

    check-cast v0, Lkotlin/e/a/b;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/q;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/q;-><init>(Lkotlin/e/a/b;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/app/transfer/payment/form/u;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$q;->a(Lcom/swedbank/mobile/app/transfer/payment/form/u;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
