.class final synthetic Lcom/swedbank/mobile/app/transfer/payment/form/n$b;
.super Lkotlin/e/b/i;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/transfer/payment/form/ac;",
        "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
        "Lcom/swedbank/mobile/app/transfer/payment/form/ac;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;
    .locals 28
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/form/ac;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p2

    const-string v1, "p1"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "p2"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p0

    iget-object v3, v1, Lcom/swedbank/mobile/app/transfer/payment/form/n$b;->b:Ljava/lang/Object;

    check-cast v3, Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 397
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$l;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_2

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$l;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v6

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    if-eqz v6, :cond_0

    .line 401
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    .line 400
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->l()Z

    move-result v4

    :cond_1
    :goto_0
    move/from16 v25, v4

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xff7f7

    const/16 v24, 0x0

    move-object/from16 v2, p1

    move-object v4, v0

    move-object v5, v7

    move v7, v8

    move-object v8, v9

    move v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move/from16 v14, v25

    .line 398
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 402
    :cond_2
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$q;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$q;

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 403
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->l()Z

    move-result v14

    xor-int/2addr v14, v5

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xff7ff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    move-object v5, v0

    .line 402
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 404
    :cond_3
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;

    if-eqz v3, :cond_5

    .line 405
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0x3ffef

    const/16 v24, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    :cond_4
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xfffef

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 409
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 412
    :cond_5
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$i;

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 413
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$i;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$i;->a()Z

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xffbff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 412
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 414
    :cond_6
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$b;

    if-eqz v3, :cond_7

    .line 415
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$b;->a()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xffffe

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 414
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 416
    :cond_7
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    .line 417
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;->a()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xffffd

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 416
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 418
    :cond_8
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$p;

    if-eqz v3, :cond_9

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 419
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$p;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$p;->a()Lcom/swedbank/mobile/business/util/l;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xffffb

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 418
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 420
    :cond_9
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$e;

    if-eqz v3, :cond_b

    .line 421
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$e;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$e;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0x19fff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    :cond_a
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xf1fff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 427
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 432
    :cond_b
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;

    const/4 v6, 0x0

    if-eqz v3, :cond_d

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 435
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;->a()Ljava/util/Map;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->isEmpty()Z

    move-result v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    .line 436
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;->a()Ljava/util/Map;

    move-result-object v21

    .line 437
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$k;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {v0}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    goto :goto_1

    :cond_c
    move-object v0, v6

    :goto_1
    const/16 v22, 0x0

    const v23, 0x98fff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    move-object v6, v7

    move v7, v8

    move-object v8, v9

    move v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v18

    move/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v0

    .line 432
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 438
    :cond_d
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$f;

    if-eqz v3, :cond_e

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 439
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$f;->a()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xfbfff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    .line 438
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 440
    :cond_e
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$g;

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xcffff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 443
    :cond_f
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;

    if-eqz v3, :cond_17

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v19, 0x0

    .line 445
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->c()Ljava/lang/String;

    move-result-object v11

    .line 447
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->g()Z

    move-result v12

    if-nez v12, :cond_11

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->c()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_10

    goto :goto_2

    :cond_10
    const/4 v12, 0x0

    goto :goto_3

    :cond_11
    :goto_2
    const/4 v12, 0x1

    .line 448
    :goto_3
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->d()Ljava/lang/String;

    move-result-object v13

    .line 449
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->e()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    .line 450
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->f()Ljava/lang/String;

    move-result-object v16

    .line 453
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->e()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_12

    .line 452
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->b()Ljava/util/Map;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_12

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->g()Ljava/util/List;

    move-result-object v17

    if-nez v17, :cond_12

    const/16 v17, 0x1

    goto :goto_4

    :cond_12
    const/16 v17, 0x0

    :goto_4
    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    .line 454
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->b()Ljava/util/Map;

    move-result-object v25

    .line 455
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->g()Ljava/util/List;

    move-result-object v22

    if-eqz v22, :cond_13

    invoke-static/range {v22 .. v22}, Lcom/swedbank/mobile/business/util/f;->b(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v6

    :cond_13
    move-object/from16 v26, v6

    .line 457
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->a()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v6

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->b()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 458
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$h;->b()Ljava/util/Map;

    move-result-object v0

    sget-object v6, Lcom/swedbank/mobile/business/transfer/payment/a;->g:Lcom/swedbank/mobile/business/transfer/payment/a;

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    goto :goto_5

    :cond_14
    const/4 v0, 0x0

    :goto_5
    if-nez v0, :cond_16

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->l()Z

    move-result v0

    if-eqz v0, :cond_15

    goto :goto_6

    :cond_15
    const/4 v0, 0x0

    goto :goto_7

    :cond_16
    :goto_6
    const/4 v0, 0x1

    :goto_7
    const/16 v22, 0x0

    const v23, 0x8e41f

    const/16 v24, 0x0

    move-object/from16 v2, p1

    move-object v4, v7

    move-object v5, v8

    move-object v6, v9

    move v7, v10

    move-object v8, v11

    move v9, v12

    move-object v10, v13

    move-object v11, v14

    move-object/from16 v12, v16

    move v13, v15

    move v14, v0

    move/from16 v15, v17

    move/from16 v16, v18

    move-object/from16 v17, v20

    move/from16 v18, v21

    move-object/from16 v20, v25

    move-object/from16 v21, v26

    .line 443
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 459
    :cond_17
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$o;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$o;

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 460
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->g()Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0xfff9f

    const/16 v24, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    :cond_18
    move-object v0, v2

    goto/16 :goto_e

    .line 465
    :cond_19
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;

    if-eqz v3, :cond_1d

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 466
    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;

    .line 467
    invoke-interface {v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;->a()Z

    move-result v10

    if-nez v10, :cond_1a

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->e()Z

    move-result v10

    if-eqz v10, :cond_1a

    const/4 v10, 0x1

    goto :goto_8

    :cond_1a
    const/4 v10, 0x0

    :goto_8
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v19, 0x0

    const/16 v25, 0x0

    .line 469
    invoke-interface {v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;->b()Z

    move-result v17

    if-nez v17, :cond_1b

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->q()Z

    move-result v17

    if-eqz v17, :cond_1b

    const/16 v26, 0x1

    goto :goto_9

    :cond_1b
    const/16 v26, 0x0

    .line 471
    :goto_9
    invoke-interface {v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;->c()Z

    move-result v9

    if-nez v9, :cond_1c

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->n()Z

    move-result v9

    if-eqz v9, :cond_1c

    const/16 v27, 0x1

    goto :goto_a

    :cond_1c
    const/16 v27, 0x0

    :goto_a
    const/16 v18, 0x0

    const/16 v17, 0x0

    const/16 v20, 0x0

    .line 473
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$m;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object v21

    const/16 v22, 0x0

    const v23, 0xa9fef

    const/16 v24, 0x0

    move-object/from16 v2, p1

    move-object v4, v6

    move-object v5, v7

    move-object v6, v8

    move v7, v10

    move-object v8, v11

    move v9, v12

    move-object v10, v13

    move-object v11, v14

    move-object v12, v15

    move/from16 v13, v16

    move/from16 v14, v19

    move/from16 v15, v25

    move/from16 v16, v27

    move/from16 v19, v26

    .line 465
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 474
    :cond_1d
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const v23, 0x3ffff

    const/16 v24, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    goto/16 :goto_e

    .line 477
    :cond_1e
    instance-of v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;

    if-eqz v3, :cond_22

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 478
    move-object v9, v0

    check-cast v9, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;

    invoke-virtual {v9}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;->d()Lcom/swedbank/mobile/app/w/b;

    move-result-object v22

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v21, 0x0

    const/16 v20, 0x0

    const/16 v18, 0x0

    .line 481
    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;

    .line 482
    invoke-interface {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;->a()Z

    move-result v17

    if-nez v17, :cond_1f

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->e()Z

    move-result v17

    if-eqz v17, :cond_1f

    const/16 v19, 0x1

    goto :goto_b

    :cond_1f
    const/16 v19, 0x0

    .line 484
    :goto_b
    invoke-interface {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;->b()Z

    move-result v17

    if-nez v17, :cond_20

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->q()Z

    move-result v17

    if-eqz v17, :cond_20

    const/16 v25, 0x1

    goto :goto_c

    :cond_20
    const/16 v25, 0x0

    .line 486
    :goto_c
    invoke-interface {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$n;->c()Z

    move-result v0

    if-nez v0, :cond_21

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->n()Z

    move-result v0

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    goto :goto_d

    :cond_21
    const/4 v0, 0x0

    :goto_d
    const/16 v17, 0x0

    const v23, 0x9fef

    const/16 v24, 0x0

    move-object/from16 v2, p1

    move-object v4, v6

    move-object v5, v7

    move-object v6, v8

    move/from16 v7, v19

    move-object v8, v9

    move v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v0

    move/from16 v19, v25

    .line 477
    invoke-static/range {v2 .. v24}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_22
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/n$b;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;)Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormViewState;Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormViewState$PartialState;)Lcom/swedbank/mobile/app/transfer/payment/form/PaymentFormViewState;"

    return-object v0
.end method
