.class final Lcom/swedbank/mobile/app/transfer/payment/form/s$g;
.super Lkotlin/e/b/k;
.source "PaymentFormRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/s;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/s;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/s;Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    check-cast p1, Ljava/lang/Iterable;

    .line 89
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 90
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 88
    instance-of v0, v0, Lcom/swedbank/mobile/business/transfer/payment/execution/p;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 49
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s;

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/s;->a(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/app/transfer/payment/a/b;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/transfer/payment/a/b;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->c:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/app/transfer/payment/a/b;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Lcom/swedbank/mobile/app/transfer/payment/a/b;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/s;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/payment/form/s;->b(Lcom/swedbank/mobile/app/transfer/payment/form/s;)Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)Lcom/swedbank/mobile/app/transfer/payment/a/b;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 93
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/s$g;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
