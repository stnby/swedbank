.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$r;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentInput"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    .line 128
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/business/transfer/payment/form/d;

    move-result-object v1

    .line 129
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/w;

    move-result-object v1

    .line 130
    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$r$1;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n$r;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 153
    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$g;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v1, "interactor\n             \u2026tate.FormCheckInProgress)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v0, v2, v3, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;ZZZ)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026xecutingPayment))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
