.class public final Lcom/swedbank/mobile/app/transfer/payment/form/a$a;
.super Lkotlin/e/b/k;
.source "AccountSelectionAddon.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/lang/String;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    .line 102
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v1, 0x0

    .line 103
    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Z)V

    .line 104
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$d;->ic_arrow_drop_down:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->d(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/b/c/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a$a;->a(Ljava/lang/String;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
