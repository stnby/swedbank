.class public final Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/s<",
        "+",
        "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Z


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;ZZZ)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->b:Z

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->c:Z

    iput-boolean p4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/o;
    .locals 12
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;

    .line 373
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    .line 374
    iget-boolean v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->b:Z

    .line 375
    iget-boolean v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->c:Z

    .line 376
    iget-boolean v4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->d:Z

    .line 372
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$d;-><init>(ZZZLcom/swedbank/mobile/app/w/b;)V

    move-object v6, v1

    check-cast v6, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a;

    .line 396
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v5

    .line 398
    sget-object v7, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$c;

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    .line 396
    invoke-static/range {v5 .. v11}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;->a(Ljava/lang/Throwable;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
