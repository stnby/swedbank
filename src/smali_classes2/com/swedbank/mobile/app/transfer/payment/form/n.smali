.class public final Lcom/swedbank/mobile/app/transfer/payment/form/n;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PaymentFormPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/payment/form/u;",
        "Lcom/swedbank/mobile/app/transfer/payment/form/ac;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/swedbank/mobile/core/ui/ad;

.field private final c:Lcom/swedbank/mobile/app/transfer/payment/form/d;

.field private final d:Lcom/swedbank/mobile/business/transfer/payment/form/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/d;Lcom/swedbank/mobile/business/transfer/payment/form/d;Landroid/app/Application;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/form/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/form/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "executionLoadingMessagesProvider"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->c:Lcom/swedbank/mobile/app/transfer/payment/form/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    .line 39
    invoke-virtual {p3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a:Landroid/content/res/Resources;

    .line 40
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/business/transfer/payment/form/d;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/app/transfer/payment/form/d;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->c:Lcom/swedbank/mobile/app/transfer/payment/form/d;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Landroid/content/res/Resources;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 40

    move-object/from16 v0, p0

    .line 43
    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->c()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v1

    .line 45
    sget-object v2, Lcom/swedbank/mobile/app/transfer/payment/form/n$h;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$h;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 46
    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/form/n$i;

    invoke-direct {v3, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$i;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 48
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 50
    iget-object v3, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-interface {v3}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->g()Lio/reactivex/o;

    move-result-object v3

    .line 51
    invoke-static {v3}, Lcom/swedbank/mobile/business/util/m;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v3

    .line 52
    sget-object v4, Lcom/swedbank/mobile/app/transfer/payment/form/n$o;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$o;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 55
    sget-object v4, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 56
    iget-object v4, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-interface {v4}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->g()Lio/reactivex/o;

    move-result-object v4

    .line 57
    iget-object v5, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-interface {v5}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->f()Lio/reactivex/o;

    move-result-object v5

    .line 58
    iget-object v6, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-interface {v6}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->i()Lio/reactivex/o;

    move-result-object v6

    .line 396
    check-cast v4, Lio/reactivex/s;

    check-cast v5, Lio/reactivex/s;

    check-cast v6, Lio/reactivex/s;

    .line 397
    new-instance v7, Lcom/swedbank/mobile/app/transfer/payment/form/n$a;

    invoke-direct {v7}, Lcom/swedbank/mobile/app/transfer/payment/form/n$a;-><init>()V

    check-cast v7, Lio/reactivex/c/i;

    .line 396
    invoke-static {v4, v5, v6, v7}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/i;)Lio/reactivex/o;

    move-result-object v4

    .line 65
    new-instance v5, Lcom/swedbank/mobile/app/transfer/payment/form/n$f;

    invoke-direct {v5, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$f;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    .line 66
    sget-object v5, Lcom/swedbank/mobile/app/transfer/payment/form/n$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$g;

    check-cast v5, Lkotlin/e/a/b;

    if-eqz v5, :cond_0

    new-instance v6, Lcom/swedbank/mobile/app/transfer/payment/form/q;

    invoke-direct {v6, v5}, Lcom/swedbank/mobile/app/transfer/payment/form/q;-><init>(Lkotlin/e/a/b;)V

    move-object v5, v6

    :cond_0
    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    .line 68
    sget-object v5, Lcom/swedbank/mobile/app/transfer/payment/form/n$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$c;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {v0, v5}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 69
    new-instance v6, Lcom/swedbank/mobile/app/transfer/payment/form/n$d;

    iget-object v7, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/transfer/payment/form/n$d;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/d;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/transfer/payment/form/p;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/transfer/payment/form/p;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 70
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 71
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 398
    invoke-static {v1}, Lio/reactivex/w;->b(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v6

    const-string v7, "Single.just(this)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    sget-object v7, Lcom/swedbank/mobile/app/transfer/payment/form/n$x;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$x;

    check-cast v7, Lkotlin/e/a/b;

    if-eqz v7, :cond_1

    new-instance v8, Lcom/swedbank/mobile/app/transfer/payment/form/q;

    invoke-direct {v8, v7}, Lcom/swedbank/mobile/app/transfer/payment/form/q;-><init>(Lkotlin/e/a/b;)V

    move-object v7, v8

    :cond_1
    check-cast v7, Lio/reactivex/c/h;

    invoke-virtual {v6, v7}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v6

    .line 76
    invoke-virtual {v6}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v6

    .line 78
    sget-object v7, Lcom/swedbank/mobile/app/transfer/payment/form/n$v;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$v;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {v0, v7}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    .line 79
    sget-object v8, Lcom/swedbank/mobile/app/transfer/payment/form/n$w;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$w;

    check-cast v8, Lio/reactivex/c/h;

    invoke-virtual {v7, v8}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v7

    .line 81
    iget-object v8, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    .line 82
    invoke-interface {v8}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->e()Lio/reactivex/w;

    move-result-object v8

    .line 83
    new-instance v9, Lcom/swedbank/mobile/app/transfer/payment/form/n$e;

    invoke-direct {v9, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$e;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V

    check-cast v9, Lio/reactivex/c/h;

    invoke-virtual {v8, v9}, Lio/reactivex/w;->c(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v8

    .line 94
    new-instance v9, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$j;-><init>(Z)V

    invoke-virtual {v8, v9}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v8

    const-string v9, "interactor\n        .quer\u2026Accounts(loading = true))"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 402
    new-instance v9, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;

    const/4 v11, 0x0

    invoke-direct {v9, v0, v10, v11, v11}, Lcom/swedbank/mobile/app/transfer/payment/form/n$ac;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;ZZZ)V

    check-cast v9, Lio/reactivex/c/h;

    invoke-virtual {v8, v9}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v8

    const-string v9, "onErrorResumeNext { e: T\u2026xecutingPayment))\n      }"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v9, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    .line 98
    invoke-interface {v9}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->j()Lio/reactivex/o;

    move-result-object v9

    .line 99
    sget-object v12, Lcom/swedbank/mobile/app/transfer/payment/form/n$j;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$j;

    check-cast v12, Lio/reactivex/c/h;

    invoke-virtual {v9, v12}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v9

    .line 101
    iget-object v12, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    .line 102
    invoke-interface {v12}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->h()Lio/reactivex/o;

    move-result-object v12

    .line 103
    sget-object v13, Lcom/swedbank/mobile/app/transfer/payment/form/n$aa;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$aa;

    check-cast v13, Lkotlin/e/a/b;

    if-eqz v13, :cond_2

    new-instance v14, Lcom/swedbank/mobile/app/transfer/payment/form/q;

    invoke-direct {v14, v13}, Lcom/swedbank/mobile/app/transfer/payment/form/q;-><init>(Lkotlin/e/a/b;)V

    move-object v13, v14

    :cond_2
    check-cast v13, Lio/reactivex/c/h;

    invoke-virtual {v12, v13}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v12

    .line 105
    sget-object v13, Lcom/swedbank/mobile/app/transfer/payment/form/n$k;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$k;

    check-cast v13, Lkotlin/e/a/b;

    invoke-virtual {v0, v13}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v13

    .line 106
    new-instance v14, Lcom/swedbank/mobile/app/transfer/payment/form/n$l;

    iget-object v15, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-direct {v14, v15}, Lcom/swedbank/mobile/app/transfer/payment/form/n$l;-><init>(Lcom/swedbank/mobile/business/transfer/payment/form/d;)V

    check-cast v14, Lkotlin/e/a/b;

    new-instance v15, Lcom/swedbank/mobile/app/transfer/payment/form/p;

    invoke-direct {v15, v14}, Lcom/swedbank/mobile/app/transfer/payment/form/p;-><init>(Lkotlin/e/a/b;)V

    check-cast v15, Lio/reactivex/c/g;

    invoke-virtual {v13, v15}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v13

    .line 107
    invoke-virtual {v13}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v13

    .line 108
    invoke-virtual {v13}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v13

    .line 113
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->a()Z

    move-result v14

    const-wide/16 v10, 0x1

    if-eqz v14, :cond_3

    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object v1

    goto :goto_0

    .line 114
    :cond_3
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->e()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    invoke-static {v1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    goto :goto_0

    .line 115
    :cond_4
    iget-object v14, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    .line 116
    invoke-interface {v14}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->g()Lio/reactivex/o;

    move-result-object v14

    .line 117
    invoke-static {v14}, Lcom/swedbank/mobile/business/util/m;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v14

    .line 118
    new-instance v15, Lcom/swedbank/mobile/app/transfer/payment/form/n$p;

    invoke-direct {v15, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$p;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast v15, Lio/reactivex/c/h;

    invoke-virtual {v14, v15}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 119
    invoke-virtual {v1, v10, v11}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v1

    .line 112
    :goto_0
    check-cast v1, Lio/reactivex/s;

    .line 121
    sget-object v14, Lcom/swedbank/mobile/app/transfer/payment/form/n$q;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$q;

    check-cast v14, Lkotlin/e/a/b;

    invoke-virtual {v0, v14}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v14

    check-cast v14, Lio/reactivex/s;

    .line 111
    invoke-static {v1, v14}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 127
    new-instance v14, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;

    invoke-direct {v14, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$r;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V

    check-cast v14, Lio/reactivex/c/h;

    invoke-virtual {v1, v14}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 157
    sget-object v14, Lcom/swedbank/mobile/app/transfer/payment/form/n$y;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$y;

    check-cast v14, Lkotlin/e/a/b;

    invoke-virtual {v0, v14}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v14

    .line 158
    sget-object v15, Lcom/swedbank/mobile/app/transfer/payment/form/n$z;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$z;

    check-cast v15, Lio/reactivex/c/h;

    invoke-virtual {v14, v15}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v14

    .line 160
    iget-object v15, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    .line 161
    invoke-interface {v15}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->k()Lio/reactivex/o;

    move-result-object v15

    .line 162
    new-instance v10, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;

    invoke-direct {v10, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$s;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V

    check-cast v10, Lio/reactivex/c/h;

    invoke-virtual {v15, v10}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v10

    .line 186
    sget-object v11, Lcom/swedbank/mobile/app/transfer/payment/form/n$t;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$t;

    check-cast v11, Lkotlin/e/a/b;

    invoke-virtual {v0, v11}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v11

    check-cast v11, Lio/reactivex/s;

    .line 189
    iget-object v15, v0, Lcom/swedbank/mobile/app/transfer/payment/form/n;->d:Lcom/swedbank/mobile/business/transfer/payment/form/d;

    invoke-interface {v15}, Lcom/swedbank/mobile/business/transfer/payment/form/d;->l()Lio/reactivex/o;

    move-result-object v15

    check-cast v15, Lio/reactivex/s;

    .line 185
    invoke-static {v11, v15}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v11

    .line 190
    new-instance v15, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;

    invoke-direct {v15, v0, v10}, Lcom/swedbank/mobile/app/transfer/payment/form/n$u;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;Lio/reactivex/o;)V

    check-cast v15, Lio/reactivex/c/h;

    invoke-virtual {v11, v15}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v10

    .line 209
    sget-object v11, Lcom/swedbank/mobile/app/transfer/payment/form/n$m;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$m;

    check-cast v11, Lkotlin/e/a/b;

    invoke-virtual {v0, v11}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v11

    .line 210
    sget-object v15, Lcom/swedbank/mobile/app/transfer/payment/form/n$n;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$n;

    check-cast v15, Lio/reactivex/c/h;

    invoke-virtual {v11, v15}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v11

    const/16 v15, 0xe

    .line 212
    new-array v15, v15, [Lio/reactivex/s;

    .line 213
    check-cast v6, Lio/reactivex/s;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    .line 214
    check-cast v7, Lio/reactivex/s;

    const/4 v6, 0x1

    aput-object v7, v15, v6

    const/4 v6, 0x2

    .line 215
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v15, v6

    const/4 v3, 0x3

    .line 216
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v15, v3

    const/4 v3, 0x4

    .line 217
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v15, v3

    const/4 v3, 0x5

    .line 218
    check-cast v8, Lio/reactivex/s;

    aput-object v8, v15, v3

    const/4 v3, 0x6

    .line 219
    check-cast v9, Lio/reactivex/s;

    aput-object v9, v15, v3

    const/4 v3, 0x7

    .line 220
    check-cast v12, Lio/reactivex/s;

    aput-object v12, v15, v3

    const/16 v3, 0x8

    .line 221
    check-cast v13, Lio/reactivex/s;

    aput-object v13, v15, v3

    const/16 v3, 0x9

    .line 222
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v15, v3

    const/16 v1, 0xa

    .line 223
    check-cast v14, Lio/reactivex/s;

    aput-object v14, v15, v1

    const/16 v1, 0xb

    .line 224
    check-cast v10, Lio/reactivex/s;

    aput-object v10, v15, v1

    const/16 v1, 0xc

    .line 225
    check-cast v11, Lio/reactivex/s;

    aput-object v11, v15, v1

    const/16 v1, 0xd

    .line 226
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v15, v1

    .line 212
    invoke-static {v15}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.mergeArray(\n \u2026\n        backClickStream)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    move-object/from16 v17, v2

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const v38, 0xfffff

    const/16 v39, 0x0

    invoke-direct/range {v17 .. v39}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;-><init>(Ljava/util/List;Ljava/util/List;Lcom/swedbank/mobile/business/util/l;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;ZZLjava/util/Map;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/form/n$b;

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/transfer/payment/form/n$b;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;)V

    check-cast v3, Lkotlin/e/a/m;

    .line 409
    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/form/o;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/transfer/payment/form/o;-><init>(Lkotlin/e/a/m;)V

    check-cast v4, Lio/reactivex/c/c;

    invoke-virtual {v1, v2, v4}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 410
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.mergeArray(\n \u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 411
    invoke-static {v0, v1}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
