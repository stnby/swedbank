.class public final Lcom/swedbank/mobile/app/transfer/payment/form/j;
.super Ljava/lang/Object;
.source "PaymentFormBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/transfer/payment/form/g;

.field private b:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Lcom/swedbank/mobile/a/ac/d/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/ac/d/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/ac/d/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->d:Lcom/swedbank/mobile/a/ac/d/b/a$a;

    .line 17
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->b:Lcom/swedbank/mobile/business/util/l;

    const/4 p1, 0x1

    .line 18
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/app/transfer/payment/form/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/j;

    .line 25
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->b:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/app/transfer/payment/form/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/form/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/j;

    .line 21
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    return-object v0
.end method

.method public final a(Z)Lcom/swedbank/mobile/app/transfer/payment/form/j;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/form/j;

    .line 29
    iput-boolean p1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->c:Z

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->d:Lcom/swedbank/mobile/a/ac/d/b/a$a;

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a:Lcom/swedbank/mobile/business/transfer/payment/form/g;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/b/a$a;->b(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/a/ac/d/b/a$a;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->b:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/b/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/ac/d/b/a$a;

    move-result-object v0

    .line 37
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/j;->c:Z

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/b/a$a;->b(Z)Lcom/swedbank/mobile/a/ac/d/b/a$a;

    move-result-object v0

    .line 38
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/d/b/a$a;->a()Lcom/swedbank/mobile/a/ac/d/b/a;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/d/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Payment form node cannot be built without a listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
