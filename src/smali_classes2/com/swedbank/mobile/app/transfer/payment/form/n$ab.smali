.class public final Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/n;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->b:Ljava/util/List;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->c:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selectedCurrency"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 396
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 397
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 398
    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/app/transfer/b;

    .line 240
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->c:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lcom/swedbank/mobile/business/a/d;

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/a/d;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/transfer/b;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/a/d;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_0

    goto :goto_2

    :cond_2
    move-object v4, v6

    :goto_2
    check-cast v4, Lcom/swedbank/mobile/business/a/d;

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    if-eqz v4, :cond_3

    .line 242
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/a/d;->d()Ljava/math/BigDecimal;

    move-result-object v11

    const/4 v12, 0x3

    invoke-static {v11, v7, v7, v12, v6}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v6, 0x20

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/a/d;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_3

    .line 243
    :cond_3
    iget-object v4, p0, Lcom/swedbank/mobile/app/transfer/payment/form/n$ab;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n;

    invoke-static {v4}, Lcom/swedbank/mobile/app/transfer/payment/form/n;->c(Lcom/swedbank/mobile/app/transfer/payment/form/n;)Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_no_funds:I

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-virtual {v4, v6, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_3
    move-object v7, v4

    const/4 v10, 0x0

    const/16 v11, 0x17

    const/4 v12, 0x0

    move-object v4, v2

    move-object v5, v8

    move-object v6, v9

    move v8, v10

    move v9, v11

    move-object v10, v12

    .line 241
    invoke-static/range {v3 .. v10}, Lcom/swedbank/mobile/app/transfer/b;->a(Lcom/swedbank/mobile/app/transfer/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/b;

    move-result-object v2

    .line 243
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 399
    :cond_4
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
