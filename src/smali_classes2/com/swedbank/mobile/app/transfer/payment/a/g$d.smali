.class final Lcom/swedbank/mobile/app/transfer/payment/a/g$d;
.super Lkotlin/e/b/k;
.source "PaymentExecutionRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/a/g;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/biometric/authentication/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/a/g;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic c:Lcom/swedbank/mobile/business/biometric/authentication/h;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/a/g;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/biometric/authentication/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->a:Lcom/swedbank/mobile/app/transfer/payment/a/g;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->c:Lcom/swedbank/mobile/business/biometric/authentication/h;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 4
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    check-cast p1, Ljava/lang/Iterable;

    .line 82
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 83
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 81
    instance-of v0, v0, Lcom/swedbank/mobile/business/biometric/authentication/i;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_5

    .line 73
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->a:Lcom/swedbank/mobile/app/transfer/payment/a/g;

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->a:Lcom/swedbank/mobile/app/transfer/payment/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/a/g;->e(Lcom/swedbank/mobile/app/transfer/payment/a/g;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    check-cast v1, Lcom/swedbank/mobile/business/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/c;)Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {v2}, Lcom/swedbank/mobile/app/customer/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_4

    .line 66
    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/a/a;

    invoke-direct {v3, v1, v2}, Lcom/swedbank/mobile/app/transfer/payment/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v3, Lcom/swedbank/mobile/app/c/a/d;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/app/c/a/d;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->c:Lcom/swedbank/mobile/business/biometric/authentication/h;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/c/a/g;->a(Lcom/swedbank/mobile/business/biometric/authentication/h;)Lcom/swedbank/mobile/app/c/a/g;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/c/a/g;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 86
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_2

    .line 68
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Beneficiary name cannot be missing"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_5
    :goto_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/a/g$d;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
