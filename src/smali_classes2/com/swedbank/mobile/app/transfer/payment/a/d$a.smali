.class public final Lcom/swedbank/mobile/app/transfer/payment/a/d$a;
.super Ljava/lang/Object;
.source "PaymentExecutionResultNotificationProviderImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/e/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/a/d;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")V"
        }
    .end annotation

    .line 53
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->a:Lcom/swedbank/mobile/app/transfer/payment/a/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "payment_execution_result_notification_channel_id"

    .line 54
    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->c:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_payment_execution_notification_channel_name:I

    invoke-virtual {v0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "application.getString(R.\u2026otification_channel_name)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->d:Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_payment_execution_notification_fail_title:I

    invoke-virtual {v0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "application.getString(R.\u2026_notification_fail_title)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->e:Ljava/lang/String;

    .line 57
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_payment_execution_notification_fail_text:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object p2

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "application.getString(R.\u2026ext, input.recipientName)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public e()Landroid/os/Bundle;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x1

    .line 59
    new-array v0, v0, [Lkotlin/k;

    .line 60
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/a/e;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;->a:Lcom/swedbank/mobile/app/transfer/payment/a/d;

    invoke-static {v3}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->a(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Lcom/squareup/moshi/n;

    move-result-object v3

    .line 83
    const-class v4, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {v3, v4}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jsonMapper.adapter(T::class.java).toJson(this)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 59
    invoke-static {v0}, Landroidx/core/os/b;->a([Lkotlin/k;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
