.class public final Lcom/swedbank/mobile/app/transfer/payment/form/v;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PaymentFormViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/payment/form/u;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final A:Lkotlin/f/c;

.field private final B:Lkotlin/f/c;

.field private final C:Lkotlin/f/c;

.field private final D:Lkotlin/f/c;

.field private final E:Lkotlin/f/c;

.field private final F:Lkotlin/f/c;

.field private final G:Lkotlin/f/c;

.field private final H:Lkotlin/f/c;

.field private final I:Lkotlin/f/c;

.field private final J:Lcom/swedbank/mobile/core/ui/u;

.field private final K:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final L:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private M:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final N:Lkotlin/d;

.field private final O:Lkotlin/d;

.field private P:Lcom/swedbank/mobile/app/transfer/payment/form/a;

.field private final Q:Lorg/threeten/bp/format/DateTimeFormatter;

.field private final R:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private final m:Lkotlin/f/c;

.field private final n:Lkotlin/f/c;

.field private final o:Lkotlin/f/c;

.field private final p:Lkotlin/f/c;

.field private final q:Lkotlin/f/c;

.field private final r:Lkotlin/f/c;

.field private final s:Lkotlin/f/c;

.field private final t:Lkotlin/f/c;

.field private final u:Lkotlin/f/c;

.field private final v:Lkotlin/f/c;

.field private final w:Lkotlin/f/c;

.field private final x:Lkotlin/f/c;

.field private final y:Lkotlin/f/c;

.field private final z:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x23

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "scrollContainer"

    const-string v4, "getScrollContainer()Landroid/widget/ScrollView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentContainer"

    const-string v4, "getContentContainer()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "creditServiceInfoMessage"

    const-string v4, "getCreditServiceInfoMessage()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientNameInput"

    const-string v4, "getRecipientNameInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientNameLoading"

    const-string v4, "getRecipientNameLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientNameExtra"

    const-string v4, "getRecipientNameExtra()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientIbanInput"

    const-string v4, "getRecipientIbanInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientIbanLoading"

    const-string v4, "getRecipientIbanLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientIbanError"

    const-string v4, "getRecipientIbanError()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientBank"

    const-string v4, "getRecipientBank()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "amountInput"

    const-string v4, "getAmountInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "amountLoading"

    const-string v4, "getAmountLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "amountError"

    const-string v4, "getAmountError()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "currencyInput"

    const-string v4, "getCurrencyInput()Landroid/widget/Spinner;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "currencySingleInput"

    const-string v4, "getCurrencySingleInput()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "availableFunds"

    const-string v4, "getAvailableFunds()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "availableFundsLoading"

    const-string v4, "getAvailableFundsLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "descriptionLayout"

    const-string v4, "getDescriptionLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "descriptionLoading"

    const-string v4, "getDescriptionLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "descriptionInput"

    const-string v4, "getDescriptionInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "serviceFee"

    const-string v4, "getServiceFee()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "extraFieldsToggle"

    const-string v4, "getExtraFieldsToggle()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "extraFieldsGroup"

    const-string v4, "getExtraFieldsGroup()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "referenceNumberLayout"

    const-string v4, "getReferenceNumberLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "referenceNumberLoading"

    const-string v4, "getReferenceNumberLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "referenceNumberInput"

    const-string v4, "getReferenceNumberInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "paymentDateLayout"

    const-string v4, "getPaymentDateLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "paymentDateInput"

    const-string v4, "getPaymentDateInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "paymentInfoMessageView"

    const-string v4, "getPaymentInfoMessageView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "executePaymentBtn"

    const-string v4, "getExecutePaymentBtn()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "executePaymentMessage"

    const-string v4, "getExecutePaymentMessage()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingViews"

    const-string v4, "getLoadingViews()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "recipientInputStream"

    const-string v4, "getRecipientInputStream()Lio/reactivex/Observable;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/payment/form/v;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentContainerClickStream"

    const-string v4, "getContentContainerClickStream()Lio/reactivex/Observable;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 2
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->R:Lio/reactivex/o;

    .line 52
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->c:Lkotlin/f/c;

    .line 53
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_scroll_container:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d:Lkotlin/f/c;

    .line 54
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_content_container:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->e:Lkotlin/f/c;

    .line 55
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_credit_service_info_message:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f:Lkotlin/f/c;

    .line 56
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_name:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->g:Lkotlin/f/c;

    .line 57
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_name_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->h:Lkotlin/f/c;

    .line 58
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_name_extra:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i:Lkotlin/f/c;

    .line 59
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_iban:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->j:Lkotlin/f/c;

    .line 60
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_iban_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->k:Lkotlin/f/c;

    .line 61
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_iban_error:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->l:Lkotlin/f/c;

    .line 62
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_bank:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->m:Lkotlin/f/c;

    .line 63
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_amount:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->n:Lkotlin/f/c;

    .line 64
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_amount_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o:Lkotlin/f/c;

    .line 65
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_amount_error:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->p:Lkotlin/f/c;

    .line 66
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_currency_selector:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->q:Lkotlin/f/c;

    .line 67
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_currency:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->r:Lkotlin/f/c;

    .line 68
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_available_funds:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->s:Lkotlin/f/c;

    .line 69
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_available_funds_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t:Lkotlin/f/c;

    .line 70
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_description_layout:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->u:Lkotlin/f/c;

    .line 71
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_description_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->v:Lkotlin/f/c;

    .line 72
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_description:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w:Lkotlin/f/c;

    .line 73
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_service_fee:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x:Lkotlin/f/c;

    .line 74
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_extra_fields_toggle:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->y:Lkotlin/f/c;

    .line 75
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_extra_fields_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z:Lkotlin/f/c;

    .line 76
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_reference_number_layout:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A:Lkotlin/f/c;

    .line 77
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_reference_number_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->B:Lkotlin/f/c;

    .line 78
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_reference_number:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C:Lkotlin/f/c;

    .line 79
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_payment_date_layout:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->D:Lkotlin/f/c;

    .line 80
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_payment_date:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->E:Lkotlin/f/c;

    .line 81
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_payment_info_message:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->F:Lkotlin/f/c;

    .line 82
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_pay_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->G:Lkotlin/f/c;

    .line 83
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_payment_execution_message:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->H:Lkotlin/f/c;

    const/4 p1, 0x5

    .line 84
    new-array p1, p1, [I

    .line 85
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_name_loading:I

    const/4 v1, 0x0

    aput v0, p1, v1

    .line 86
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_iban_loading:I

    const/4 v1, 0x1

    aput v0, p1, v1

    .line 87
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_amount_loading:I

    const/4 v1, 0x2

    aput v0, p1, v1

    .line 88
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_description_loading:I

    const/4 v1, 0x3

    aput v0, p1, v1

    .line 89
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_reference_number_loading:I

    const/4 v1, 0x4

    aput v0, p1, v1

    .line 84
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;[I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->I:Lkotlin/f/c;

    .line 91
    new-instance p1, Lcom/swedbank/mobile/core/ui/u;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/u;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    .line 92
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->K:Lcom/b/c/c;

    .line 93
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<String>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->L:Lcom/b/c/c;

    .line 95
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->M:Lcom/swedbank/mobile/business/util/l;

    .line 96
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/v$n;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$n;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/v;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->N:Lkotlin/d;

    .line 108
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/v$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/v;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->O:Lkotlin/d;

    const-string p1, "dd/MM/yyyy"

    .line 114
    invoke-static {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->Q:Lorg/threeten/bp/format/DateTimeFormatter;

    return-void
.end method

.method private final A()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->n:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public static final synthetic A(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->y()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final B()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic B(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->H()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic C(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final C()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->p:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final D()Landroid/widget/Spinner;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->q:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    return-object v0
.end method

.method public static final synthetic D(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->N()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method private final E()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->r:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic E(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/b/c/c;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->K:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic F(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final F()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->s:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final G()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic G(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->Q()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final H()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->u:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method public static final synthetic H(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->Q:Lorg/threeten/bp/format/DateTimeFormatter;

    return-object p0
.end method

.method private final I()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->v:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic I(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/swedbank/mobile/core/ui/u;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    return-object p0
.end method

.method public static final synthetic J(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->r()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final J()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final K()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x15

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final L()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->y:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final M()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final N()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final O()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->B:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final P()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x1a

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final Q()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->E:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x1c

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final R()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->F:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x1d

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final S()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->G:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x1e

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    return-object v0
.end method

.method private final T()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->H:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x1f

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final U()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->I:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x20

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final V()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->N:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x21

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method private final W()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->O:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x22

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/swedbank/mobile/app/transfer/payment/form/a;
    .locals 1

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    if-nez p0, :cond_0

    const-string v0, "accountSelection"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->s()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/Spinner;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->D()Landroid/widget/Spinner;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/b/c/c;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->L:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->E()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->L()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->M()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->v()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->K()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->R()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->G()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic o(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->F()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final p()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic p(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->S()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object p0

    return-object p0
.end method

.method private final q()Landroid/widget/ScrollView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method public static final synthetic q(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->T()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final r()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic r(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->u()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic s(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final s()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic t(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->B()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final t()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final u()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic u(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->I()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic v(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->O()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final v()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final w()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public static final synthetic w(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Ljava/util/List;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->U()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final x()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final y()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->l:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic y(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method private final z()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->m:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a:[Lkotlin/h/g;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/transfer/payment/form/g;
    .locals 12
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->M:Lcom/swedbank/mobile/business/util/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/l;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 144
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 608
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 613
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 612
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 611
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v5, 0x0

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v5

    .line 145
    :goto_1
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 614
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 619
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 618
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 617
    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-lez v6, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_3

    move-object v6, v1

    goto :goto_3

    :cond_3
    move-object v6, v5

    .line 146
    :goto_3
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 620
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 625
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 624
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 623
    move-object v7, v1

    check-cast v7, Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-lez v7, :cond_4

    const/4 v7, 0x1

    goto :goto_4

    :cond_4
    const/4 v7, 0x0

    :goto_4
    if-eqz v7, :cond_5

    move-object v7, v1

    goto :goto_5

    :cond_5
    move-object v7, v5

    .line 627
    :goto_5
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->c(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7

    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->c(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    instance-of v8, v1, Ljava/lang/String;

    if-nez v8, :cond_6

    move-object v1, v5

    :cond_6
    check-cast v1, Ljava/lang/String;

    :goto_6
    move-object v8, v1

    goto :goto_8

    .line 628
    :cond_7
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_b

    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v1

    .line 632
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 637
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 636
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 635
    move-object v8, v1

    check-cast v8, Ljava/lang/CharSequence;

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-lez v8, :cond_8

    const/4 v8, 0x1

    goto :goto_7

    :cond_8
    const/4 v8, 0x0

    :goto_7
    if-eqz v8, :cond_9

    goto :goto_6

    :cond_9
    move-object v1, v5

    goto :goto_6

    .line 636
    :cond_a
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    move-object v8, v5

    .line 148
    :goto_8
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 640
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 645
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 644
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 643
    move-object v9, v1

    check-cast v9, Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-lez v9, :cond_c

    const/4 v9, 0x1

    goto :goto_9

    :cond_c
    const/4 v9, 0x0

    :goto_9
    if-eqz v9, :cond_d

    move-object v9, v1

    goto :goto_a

    :cond_d
    move-object v9, v5

    .line 149
    :goto_a
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 646
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 651
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 650
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 649
    move-object v10, v1

    check-cast v10, Ljava/lang/CharSequence;

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-lez v10, :cond_e

    const/4 v3, 0x1

    :cond_e
    if-eqz v3, :cond_f

    move-object v10, v1

    goto :goto_b

    :cond_f
    move-object v10, v5

    .line 142
    :goto_b
    new-instance v11, Lcom/swedbank/mobile/app/transfer/payment/form/g;

    move-object v1, v11

    move-object v3, v7

    move-object v4, v8

    move-object v5, v0

    move-object v7, v9

    move-object v8, v10

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/app/transfer/payment/form/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v11

    .line 650
    :cond_10
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 644
    :cond_11
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 624
    :cond_12
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618
    :cond_13
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 612
    :cond_14
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;)V
    .locals 26
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/form/ac;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const-string v1, "viewState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    iget-object v1, v0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    const/4 v3, 0x1

    .line 658
    invoke-virtual {v1, v3}, Lcom/swedbank/mobile/core/ui/u;->a(Z)V

    .line 205
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 660
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/swedbank/mobile/app/transfer/b;

    .line 206
    invoke-virtual {v7}, Lcom/swedbank/mobile/app/transfer/b;->e()Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_0

    :cond_1
    move-object v5, v6

    .line 661
    :goto_0
    check-cast v5, Lcom/swedbank/mobile/app/transfer/b;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/swedbank/mobile/app/transfer/b;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    move-object v4, v6

    .line 208
    :goto_1
    invoke-static {v4}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v4

    iput-object v4, v0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->M:Lcom/swedbank/mobile/business/util/l;

    .line 209
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->r()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_2

    .line 662
    :cond_3
    invoke-static {}, Lkotlin/a/x;->a()Ljava/util/Map;

    move-result-object v4

    .line 664
    :goto_2
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/form/z;->a()Ljava/util/Set;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 668
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/Collection;

    .line 669
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/swedbank/mobile/business/transfer/payment/a;

    .line 667
    invoke-interface {v4, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    xor-int/2addr v9, v3

    if-eqz v9, :cond_4

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 670
    :cond_5
    check-cast v7, Ljava/util/List;

    check-cast v7, Ljava/lang/Iterable;

    .line 671
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v7, 0x4

    const/4 v8, 0x0

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/transfer/payment/a;

    .line 672
    sget-object v9, Lcom/swedbank/mobile/app/transfer/payment/form/w;->b:[I

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/a;->ordinal()I

    move-result v5

    aget v5, v9, v5

    packed-switch v5, :pswitch_data_0

    goto :goto_4

    .line 779
    :pswitch_0
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->D(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    move-object v7, v6

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v5, v7}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 778
    :pswitch_1
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->B(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    move-object v7, v6

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v5, v7}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 744
    :pswitch_2
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v5

    .line 745
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v9

    .line 746
    move-object v10, v6

    check-cast v10, Ljava/lang/CharSequence;

    .line 752
    invoke-static {v5, v8}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Z)V

    .line 753
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 774
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 775
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 709
    :pswitch_3
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v5

    .line 710
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->j(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v9

    .line 711
    move-object v10, v6

    check-cast v10, Ljava/lang/CharSequence;

    .line 717
    invoke-static {v5, v8}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Z)V

    .line 718
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 739
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 740
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 674
    :pswitch_4
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v5

    .line 675
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->y(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v9

    .line 676
    move-object v10, v6

    check-cast v10, Ljava/lang/CharSequence;

    .line 682
    invoke-static {v5, v8}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Z)V

    .line 683
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 704
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 705
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 211
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->c()Lcom/swedbank/mobile/business/util/l;

    move-result-object v4

    .line 212
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->b()Ljava/util/List;

    move-result-object v5

    .line 787
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->c(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/Spinner;

    move-result-object v9

    .line 788
    check-cast v5, Ljava/lang/Iterable;

    .line 789
    new-instance v10, Ljava/util/ArrayList;

    const/16 v11, 0xa

    invoke-static {v5, v11}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v12

    invoke-direct {v10, v12}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v10, Ljava/util/Collection;

    .line 790
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    .line 791
    check-cast v12, Lcom/swedbank/mobile/app/transfer/payment/form/h;

    .line 788
    invoke-virtual {v12}, Lcom/swedbank/mobile/app/transfer/payment/form/h;->b()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 792
    :cond_7
    check-cast v10, Ljava/util/List;

    .line 793
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v3, :cond_8

    const/4 v5, 0x1

    goto :goto_6

    :cond_8
    const/4 v5, 0x0

    :goto_6
    if-eqz v5, :cond_b

    .line 794
    invoke-virtual {v9}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v12

    instance-of v13, v12, Lcom/swedbank/mobile/app/transfer/payment/form/m;

    if-nez v13, :cond_9

    move-object v12, v6

    :cond_9
    check-cast v12, Lcom/swedbank/mobile/app/transfer/payment/form/m;

    if-eqz v12, :cond_a

    invoke-virtual {v12}, Lcom/swedbank/mobile/app/transfer/payment/form/m;->a()Ljava/util/List;

    move-result-object v12

    goto :goto_7

    :cond_a
    move-object v12, v6

    :goto_7
    invoke-static {v12, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    xor-int/2addr v12, v3

    if-eqz v12, :cond_b

    .line 795
    new-instance v12, Lcom/swedbank/mobile/app/transfer/payment/form/m;

    .line 800
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v13

    .line 802
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->e(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/b/c/c;

    move-result-object v14

    .line 795
    invoke-direct {v12, v13, v10, v14}, Lcom/swedbank/mobile/app/transfer/payment/form/m;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/b/c/c;)V

    .line 799
    invoke-virtual {v12, v9}, Lcom/swedbank/mobile/app/transfer/payment/form/m;->a(Landroid/widget/Spinner;)V

    .line 805
    :cond_b
    instance-of v12, v4, Lcom/swedbank/mobile/business/util/n;

    const/16 v13, 0x8

    if-eqz v12, :cond_d

    if-eqz v5, :cond_c

    .line 808
    invoke-virtual {v9, v8}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 809
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 810
    check-cast v4, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 811
    invoke-virtual {v9}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v5

    if-eq v4, v5, :cond_e

    .line 812
    invoke-virtual {v9, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_8

    .line 816
    :cond_c
    invoke-virtual {v9, v13}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 817
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v5

    .line 818
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 819
    check-cast v4, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 820
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_8

    .line 824
    :cond_d
    invoke-virtual {v9, v13}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 825
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 214
    :cond_e
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->f()Ljava/lang/String;

    move-result-object v4

    .line 215
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->g()Z

    move-result v5

    .line 216
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->h()Ljava/lang/String;

    move-result-object v9

    .line 217
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->c()Lcom/swedbank/mobile/business/util/l;

    move-result-object v10

    .line 218
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->e()Z

    move-result v12

    .line 219
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->b()Ljava/util/List;

    move-result-object v14

    .line 220
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->i()Ljava/lang/String;

    move-result-object v15

    .line 221
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->j()Ljava/lang/String;

    move-result-object v16

    if-eqz v4, :cond_10

    .line 829
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v11

    .line 830
    move-object v3, v4

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v11, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 831
    invoke-virtual {v11}, Landroid/widget/EditText;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 832
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v11, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 834
    :cond_f
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    :cond_10
    if-eqz v5, :cond_13

    .line 838
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v3

    .line 839
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->j(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v4

    .line 840
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v5

    sget v11, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_recipient_name_corrected:I

    invoke-virtual {v5, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    if-eqz v5, :cond_11

    const/4 v11, 0x1

    goto :goto_9

    :cond_11
    const/4 v11, 0x0

    .line 846
    :goto_9
    invoke-static {v3, v6}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Ljava/lang/Integer;)V

    .line 847
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v11, :cond_12

    .line 849
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 851
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 864
    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    const-string v11, "append(text)"

    invoke-static {v5, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 867
    new-instance v5, Landroid/text/SpannedString;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v5, v3}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 868
    :cond_12
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 869
    move-object v3, v6

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 872
    :cond_13
    :goto_a
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->k(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v9, Ljava/lang/CharSequence;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 873
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->l(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v15, Ljava/lang/CharSequence;

    invoke-virtual {v3, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 874
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->m(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    move-object/from16 v4, v16

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v12, :cond_14

    .line 877
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->n(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 878
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 880
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 881
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    .line 883
    :cond_14
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->n(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 884
    invoke-virtual {v3, v13}, Landroid/view/View;->setVisibility(I)V

    .line 886
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 887
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 891
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    .line 889
    check-cast v14, Ljava/lang/Iterable;

    .line 897
    instance-of v4, v10, Lcom/swedbank/mobile/business/util/n;

    if-eqz v4, :cond_15

    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/form/v$d;

    invoke-direct {v4, v10}, Lcom/swedbank/mobile/app/transfer/payment/form/v$d;-><init>(Lcom/swedbank/mobile/business/util/l;)V

    check-cast v4, Lkotlin/e/a/b;

    goto :goto_b

    .line 898
    :cond_15
    sget-object v4, Lcom/swedbank/mobile/app/transfer/payment/form/y;->a:Lkotlin/h/i;

    check-cast v4, Lkotlin/e/a/b;

    .line 900
    :goto_b
    invoke-interface {v14}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_16
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_17

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v4, v9}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_16

    goto :goto_c

    :cond_17
    move-object v9, v6

    .line 901
    :goto_c
    check-cast v9, Lcom/swedbank/mobile/app/transfer/payment/form/h;

    if-eqz v9, :cond_18

    .line 891
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_available_balance:I

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/swedbank/mobile/app/transfer/payment/form/h;->a()Ljava/math/BigDecimal;

    move-result-object v11

    const/4 v12, 0x3

    invoke-static {v11, v8, v8, v12, v6}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    invoke-virtual {v9}, Lcom/swedbank/mobile/app/transfer/payment/form/h;->b()Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x1

    aput-object v9, v10, v11

    invoke-virtual {v4, v5, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_d

    :cond_18
    move-object v4, v6

    :goto_d
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :goto_e
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->r()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_25

    .line 905
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_24

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/business/transfer/payment/a;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 906
    sget-object v9, Lcom/swedbank/mobile/app/transfer/payment/form/w;->a:[I

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/transfer/payment/a;->ordinal()I

    move-result v10

    aget v9, v9, v10

    const/16 v10, 0x11

    packed-switch v9, :pswitch_data_1

    .line 1017
    sget-object v9, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received not handled input field ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ") error w/ message="

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_f

    .line 1016
    :pswitch_5
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->D(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    goto :goto_f

    .line 1015
    :pswitch_6
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->B(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    const-string v10, "descriptionInput.text"

    invoke-static {v9, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v9, Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-lez v9, :cond_19

    const/4 v9, 0x1

    goto :goto_10

    :cond_19
    const/4 v9, 0x0

    :goto_10
    if-eqz v9, :cond_1a

    goto :goto_11

    :cond_1a
    move-object v4, v6

    :goto_11
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    goto :goto_f

    .line 980
    :pswitch_7
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v5

    .line 981
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v9

    .line 987
    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_1c

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    const-string v12, "inputView.text"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Ljava/lang/CharSequence;

    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    move-result v11

    if-lez v11, :cond_1b

    const/4 v11, 0x1

    goto :goto_12

    :cond_1b
    const/4 v11, 0x0

    :goto_12
    if-eqz v11, :cond_1c

    const/4 v11, 0x1

    goto :goto_13

    :cond_1c
    const/4 v11, 0x0

    .line 989
    :goto_13
    invoke-static {v5, v11}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Z)V

    .line 990
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v11, :cond_1d

    .line 992
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 994
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 997
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v11

    sget v12, Lcom/swedbank/mobile/app/transfer/a$b;->label_red:I

    .line 999
    invoke-static {v11, v12}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v11

    .line 1000
    new-instance v12, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v12, v11}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1001
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    .line 1003
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1005
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v5, v12, v11, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1010
    new-instance v4, Landroid/text/SpannedString;

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v4, v5}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_f

    .line 1011
    :cond_1d
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1012
    move-object v4, v6

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_f

    .line 944
    :pswitch_8
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v5

    .line 945
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->j(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v9

    .line 951
    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_1f

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    const-string v12, "inputView.text"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Ljava/lang/CharSequence;

    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    move-result v11

    if-lez v11, :cond_1e

    const/4 v11, 0x1

    goto :goto_14

    :cond_1e
    const/4 v11, 0x0

    :goto_14
    if-eqz v11, :cond_1f

    const/4 v11, 0x1

    goto :goto_15

    :cond_1f
    const/4 v11, 0x0

    .line 953
    :goto_15
    invoke-static {v5, v11}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Z)V

    .line 954
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v11, :cond_20

    .line 956
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 958
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 961
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v11

    sget v12, Lcom/swedbank/mobile/app/transfer/a$b;->label_red:I

    .line 963
    invoke-static {v11, v12}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v11

    .line 964
    new-instance v12, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v12, v11}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 965
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    .line 967
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 969
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v5, v12, v11, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 974
    new-instance v4, Landroid/text/SpannedString;

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v4, v5}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_f

    .line 975
    :cond_20
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 976
    move-object v4, v6

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_f

    .line 908
    :pswitch_9
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v5

    .line 909
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->y(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v9

    .line 915
    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_22

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    const-string v12, "inputView.text"

    invoke-static {v11, v12}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Ljava/lang/CharSequence;

    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    move-result v11

    if-lez v11, :cond_21

    const/4 v11, 0x1

    goto :goto_16

    :cond_21
    const/4 v11, 0x0

    :goto_16
    if-eqz v11, :cond_22

    const/4 v11, 0x1

    goto :goto_17

    :cond_22
    const/4 v11, 0x0

    .line 917
    :goto_17
    invoke-static {v5, v11}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;Z)V

    .line 918
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v11, :cond_23

    .line 920
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 922
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 925
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v11

    sget v12, Lcom/swedbank/mobile/app/transfer/a$b;->label_red:I

    .line 927
    invoke-static {v11, v12}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v11

    .line 928
    new-instance v12, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v12, v11}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 929
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    .line 931
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 933
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v5, v12, v11, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 938
    new-instance v4, Landroid/text/SpannedString;

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v4, v5}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_f

    .line 939
    :cond_23
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 940
    move-object v4, v6

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_f

    .line 1024
    :cond_24
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 223
    :cond_25
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->q()Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 1027
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2d

    .line 1028
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    .line 1029
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_name:I

    if-ne v3, v4, :cond_26

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->r(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 1030
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18

    .line 1032
    :cond_26
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_recipient_iban:I

    if-ne v3, v4, :cond_27

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->s(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 1033
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18

    .line 1035
    :cond_27
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_amount:I

    if-ne v3, v4, :cond_28

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 1036
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18

    .line 1038
    :cond_28
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_description:I

    if-ne v3, v4, :cond_29

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->u(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 1039
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18

    .line 1041
    :cond_29
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->payment_form_reference_number:I

    if-ne v3, v4, :cond_2a

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->v(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v3

    .line 1042
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1045
    :cond_2a
    :goto_18
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_1a

    .line 1046
    :cond_2b
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Ljava/util/List;

    move-result-object v3

    .line 1047
    check-cast v3, Ljava/lang/Iterable;

    .line 1048
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v4, v9}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 1049
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_19
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1050
    check-cast v5, Landroid/view/View;

    .line 1047
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 1051
    :cond_2c
    check-cast v4, Ljava/util/List;

    .line 225
    :cond_2d
    :goto_1a
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->p()Z

    move-result v3

    .line 226
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->m()Z

    move-result v4

    if-eqz v4, :cond_2e

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->e()Z

    move-result v4

    if-nez v4, :cond_2e

    const/4 v4, 0x1

    goto :goto_1b

    :cond_2e
    const/4 v4, 0x0

    .line 227
    :goto_1b
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->n()Z

    move-result v5

    .line 230
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->r()Ljava/util/Map;

    move-result-object v7

    if-nez v7, :cond_30

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->s()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    if-nez v7, :cond_30

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->t()Lcom/swedbank/mobile/app/w/b;

    move-result-object v7

    if-eqz v7, :cond_2f

    goto :goto_1c

    :cond_2f
    const/4 v7, 0x0

    goto :goto_1d

    :cond_30
    :goto_1c
    const/4 v7, 0x1

    .line 1054
    :goto_1d
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->p(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object v9

    .line 1055
    invoke-virtual {v9, v4}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->setEnabled(Z)V

    if-eqz v5, :cond_31

    .line 1056
    invoke-virtual {v9}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->a()V

    :cond_31
    if-eqz v3, :cond_32

    .line 1058
    invoke-virtual {v9}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->b()V

    goto :goto_1e

    :cond_32
    if-eqz v7, :cond_33

    .line 1059
    invoke-virtual {v9}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->c()V

    .line 1061
    :cond_33
    :goto_1e
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 231
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->o()Ljava/lang/String;

    move-result-object v3

    .line 1064
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->q(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v4

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->s()Lcom/swedbank/mobile/business/util/e;

    move-result-object v3

    .line 234
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->t()Lcom/swedbank/mobile/app/w/b;

    move-result-object v4

    if-eqz v3, :cond_37

    .line 1068
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v4

    .line 1069
    move-object v5, v6

    check-cast v5, Ljava/lang/Integer;

    .line 1072
    instance-of v5, v3, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v5, :cond_35

    check-cast v3, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 1074
    move-object v5, v3

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    const/4 v7, 0x1

    xor-int/2addr v5, v7

    if-eqz v5, :cond_34

    move-object/from16 v17, v3

    check-cast v17, Ljava/lang/Iterable;

    const-string v3, "\n"

    move-object/from16 v18, v3

    check-cast v18, Ljava/lang/CharSequence;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x3e

    const/16 v25, 0x0

    invoke-static/range {v17 .. v25}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1f

    .line 1075
    :cond_34
    sget v3, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1f

    .line 1077
    :cond_35
    instance-of v5, v3, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v5, :cond_36

    check-cast v3, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    .line 1078
    invoke-static {v3}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1f
    const-string v4, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 1079
    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 1081
    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    const/4 v5, 0x1

    invoke-direct {v4, v6, v5, v6}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 1067
    invoke-virtual {v0, v3, v4}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_20

    .line 1078
    :cond_36
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_37
    if-eqz v4, :cond_38

    .line 1083
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context().getString(fata\u2026ror.userDisplayedMessage)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 1084
    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    const/4 v5, 0x1

    invoke-direct {v4, v6, v5, v6}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 1082
    invoke-virtual {v0, v3, v4}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_20

    .line 1085
    :cond_38
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 235
    :goto_20
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->d()Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object v3

    if-eqz v3, :cond_42

    .line 1091
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->E(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/b/c/c;

    move-result-object v4

    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {v4, v5}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    .line 1093
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v5

    if-eqz v5, :cond_39

    invoke-static {v5, v8, v8}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZ)Ljava/lang/String;

    move-result-object v5

    goto :goto_21

    :cond_39
    move-object v5, v6

    :goto_21
    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1098
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v4

    .line 1099
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v5

    .line 1100
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->c(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/Spinner;

    move-result-object v7

    .line 1101
    check-cast v5, Ljava/lang/Iterable;

    .line 1102
    new-instance v9, Ljava/util/ArrayList;

    const/16 v10, 0xa

    invoke-static {v5, v10}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v9, Ljava/util/Collection;

    .line 1103
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_22
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 1104
    check-cast v10, Lcom/swedbank/mobile/app/transfer/payment/form/h;

    .line 1101
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/transfer/payment/form/h;->b()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_22

    .line 1105
    :cond_3a
    check-cast v9, Ljava/util/List;

    .line 1106
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    const/4 v10, 0x1

    if-le v5, v10, :cond_3b

    const/4 v5, 0x1

    goto :goto_23

    :cond_3b
    const/4 v5, 0x0

    :goto_23
    if-eqz v5, :cond_3e

    .line 1107
    invoke-virtual {v7}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v10

    instance-of v11, v10, Lcom/swedbank/mobile/app/transfer/payment/form/m;

    if-nez v11, :cond_3c

    move-object v10, v6

    :cond_3c
    check-cast v10, Lcom/swedbank/mobile/app/transfer/payment/form/m;

    if-eqz v10, :cond_3d

    invoke-virtual {v10}, Lcom/swedbank/mobile/app/transfer/payment/form/m;->a()Ljava/util/List;

    move-result-object v6

    :cond_3d
    invoke-static {v6, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    const/4 v10, 0x1

    xor-int/2addr v6, v10

    if-eqz v6, :cond_3e

    .line 1108
    new-instance v6, Lcom/swedbank/mobile/app/transfer/payment/form/m;

    .line 1113
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->d(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/content/Context;

    move-result-object v10

    .line 1115
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->e(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/b/c/c;

    move-result-object v11

    .line 1108
    invoke-direct {v6, v10, v9, v11}, Lcom/swedbank/mobile/app/transfer/payment/form/m;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/b/c/c;)V

    .line 1112
    invoke-virtual {v6, v7}, Lcom/swedbank/mobile/app/transfer/payment/form/m;->a(Landroid/widget/Spinner;)V

    .line 1118
    :cond_3e
    instance-of v6, v4, Lcom/swedbank/mobile/business/util/n;

    if-eqz v6, :cond_40

    if-eqz v5, :cond_3f

    .line 1121
    invoke-virtual {v7, v8}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1122
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1123
    check-cast v4, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v9, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 1124
    invoke-virtual {v7}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v5

    if-eq v4, v5, :cond_41

    .line 1125
    invoke-virtual {v7, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_24

    .line 1129
    :cond_3f
    invoke-virtual {v7, v13}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1130
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v5

    .line 1131
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1132
    check-cast v4, Lcom/swedbank/mobile/business/util/n;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/n;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1133
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_24

    .line 1137
    :cond_40
    invoke-virtual {v7, v13}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1138
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->f(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1141
    :cond_41
    :goto_24
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1142
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->g()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1143
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->h()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1144
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->F(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->i()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1145
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->G(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->H(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/threeten/bp/LocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1146
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 236
    :cond_42
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->a()Ljava/util/List;

    move-result-object v3

    .line 1148
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Lcom/swedbank/mobile/app/transfer/payment/form/a;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Ljava/util/List;)V

    .line 1149
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_46

    check-cast v3, Landroid/view/ViewGroup;

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->d()Landroidx/l/n;

    move-result-object v4

    invoke-static {v3, v4}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 240
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->l()Z

    move-result v3

    .line 1151
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->g(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v3, :cond_43

    .line 1153
    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_extra_fields_hide:I

    goto :goto_25

    .line 1154
    :cond_43
    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_extra_fields_show:I

    .line 1151
    :goto_25
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1157
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->h(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/view/View;

    move-result-object v4

    if-eqz v3, :cond_44

    const/4 v3, 0x0

    goto :goto_26

    :cond_44
    const/16 v3, 0x8

    .line 1158
    :goto_26
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 241
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/payment/form/ac;->k()Z

    move-result v2

    .line 1161
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->b(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v2, :cond_45

    const/4 v13, 0x0

    .line 1162
    :cond_45
    invoke-virtual {v3, v13}, Landroid/view/View;->setVisibility(I)V

    .line 1165
    invoke-virtual {v1, v8}, Lcom/swedbank/mobile/core/ui/u;->a(Z)V

    return-void

    .line 1149
    :cond_46
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 49
    check-cast p1, Lcom/swedbank/mobile/app/transfer/payment/form/ac;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a(Lcom/swedbank/mobile/app/transfer/payment/form/ac;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->L:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->K:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 10

    .line 527
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 528
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/v$i;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$i;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/form/x;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/x;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 529
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 118
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$d;->ic_close:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(I)V

    .line 119
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v3}, Landroid/text/InputFilter$AllCaps;-><init>()V

    check-cast v3, Landroid/text/InputFilter;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;[Landroid/text/InputFilter;)V

    .line 120
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w()Landroid/widget/EditText;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v3}, Landroid/text/InputFilter$AllCaps;-><init>()V

    check-cast v3, Landroid/text/InputFilter;

    aput-object v3, v2, v4

    new-instance v3, Lcom/swedbank/mobile/core/ui/q;

    invoke-direct {v3}, Lcom/swedbank/mobile/core/ui/q;-><init>()V

    check-cast v3, Landroid/text/InputFilter;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;[Landroid/text/InputFilter;)V

    .line 121
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->S()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->q()Landroid/widget/ScrollView;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;->setScrollParent(Landroid/view/ViewGroup;)V

    .line 122
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o()Landroid/view/View;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "rootView.context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 532
    :goto_0
    instance-of v2, v1, Landroid/content/ContextWrapper;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 533
    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 534
    check-cast v1, Landroid/app/Activity;

    goto :goto_1

    .line 536
    :cond_0
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context.baseContext"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_1
    if-eqz v1, :cond_3

    .line 539
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const-string v2, "window"

    .line 540
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const-string v4, "window.decorView"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 541
    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/form/v$k;

    invoke-direct {v4, v2, v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/v$k;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/Window;)V

    check-cast v4, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 542
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 544
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 v5, 0x10

    if-eq v0, v5, :cond_2

    .line 545
    invoke-virtual {v1, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 548
    :cond_2
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/v$l;

    invoke-direct {v0, v2, v4, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/v$l;-><init>(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/Window;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction {\u2026INPUT_ADJUST_NOTHING)\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 530
    :cond_3
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 549
    :goto_2
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 123
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o()Landroid/view/View;

    move-result-object v0

    .line 551
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/v$j;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$j;-><init>(Landroid/view/View;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/x;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/x;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction(rootView::hideKeyboard)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 552
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 124
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->W()Lio/reactivex/o;

    move-result-object v4

    const-string v0, "contentContainerClickStream"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 125
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/v$m;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$m;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/v;)V

    move-object v7, v0

    check-cast v7, Lkotlin/e/a/b;

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 554
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 128
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->o()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v1, :cond_4

    move-object v0, v3

    :cond_4
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    if-eqz v0, :cond_9

    .line 130
    sget v1, Lcom/swedbank/mobile/app/transfer/a$e;->toolbar:I

    .line 127
    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/form/a;

    invoke-direct {v2, v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;I)V

    iput-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    .line 560
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->i(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v0

    .line 561
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 566
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 565
    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v$b;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$b;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_5

    new-instance v2, Lcom/swedbank/mobile/app/transfer/payment/form/aa;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/aa;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_5
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 564
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "textChanges()\n      .ski\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/s;

    .line 567
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->z(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v1

    .line 568
    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v1

    .line 573
    invoke-virtual {v1}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v1

    .line 572
    sget-object v2, Lcom/swedbank/mobile/app/transfer/payment/form/v$b;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$b;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_6

    new-instance v3, Lcom/swedbank/mobile/app/transfer/payment/form/aa;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/aa;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_6
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 571
    invoke-virtual {v1}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v1

    const-string v2, "textChanges()\n      .ski\u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/s;

    .line 574
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->x(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v2

    .line 584
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v2

    .line 589
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    .line 588
    sget-object v3, Lcom/swedbank/mobile/app/transfer/payment/form/v$b;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$b;

    check-cast v3, Lkotlin/e/a/b;

    if-eqz v3, :cond_7

    new-instance v4, Lcom/swedbank/mobile/app/transfer/payment/form/aa;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/transfer/payment/form/aa;-><init>(Lkotlin/e/a/b;)V

    move-object v3, v4

    :cond_7
    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 587
    invoke-virtual {v2}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v2

    const-string v3, "textChanges()\n      .ski\u2026  .distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/s;

    .line 590
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->C(Lcom/swedbank/mobile/app/transfer/payment/form/v;)Landroid/widget/EditText;

    move-result-object v3

    .line 599
    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v3

    .line 604
    invoke-virtual {v3}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v3

    .line 603
    sget-object v4, Lcom/swedbank/mobile/app/transfer/payment/form/v$b;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$b;

    check-cast v4, Lkotlin/e/a/b;

    if-eqz v4, :cond_8

    new-instance v5, Lcom/swedbank/mobile/app/transfer/payment/form/aa;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/transfer/payment/form/aa;-><init>(Lkotlin/e/a/b;)V

    move-object v4, v5

    :cond_8
    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 602
    invoke-virtual {v3}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v3

    const-string v4, "textChanges()\n      .ski\u2026  .distinctUntilChanged()"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/s;

    .line 559
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v4

    const-string v0, "Observable\n      .merge(\u2026ut.distinctTextChanges())"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 558
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$c;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/v;)V

    move-object v7, v0

    check-cast v7, Lkotlin/e/a/b;

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 605
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void

    .line 128
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RootView must be ConstraintLayout for AccountSelectionAddon"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public f()Lio/reactivex/o;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x6

    .line 155
    new-array v1, v0, [Lio/reactivex/s;

    .line 156
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->t()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 157
    invoke-static {v2}, Lcom/b/b/d/d;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object v2

    .line 158
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 159
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->w()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 160
    invoke-static {v2}, Lcom/b/b/d/d;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 162
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 163
    invoke-static {v2}, Lcom/b/b/d/d;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object v2

    .line 164
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    .line 165
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 166
    invoke-static {v2}, Lcom/b/b/d/d;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    .line 168
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 169
    invoke-static {v2}, Lcom/b/b/d/d;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object v2

    .line 170
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    const/4 v7, 0x4

    aput-object v2, v1, v7

    .line 171
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->Q()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 172
    invoke-static {v2}, Lcom/b/b/d/d;->a(Landroid/view/View;)Lcom/b/b/a;

    move-result-object v2

    .line 173
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    const/4 v8, 0x5

    aput-object v2, v1, v8

    .line 155
    invoke-static {v1}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 174
    sget-object v2, Lcom/swedbank/mobile/app/transfer/payment/form/v$f;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$f;

    check-cast v2, Lio/reactivex/c/k;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 175
    new-array v0, v0, [Lio/reactivex/s;

    .line 176
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->V()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    aput-object v2, v0, v3

    .line 177
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->j()Lio/reactivex/o;

    move-result-object v2

    .line 178
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    if-eqz v3, :cond_4

    .line 652
    check-cast v3, Lio/reactivex/c/k;

    .line 178
    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    aput-object v2, v0, v4

    .line 179
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->A()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 180
    invoke-static {v2}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v2

    .line 181
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    .line 182
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    if-eqz v3, :cond_3

    .line 653
    check-cast v3, Lio/reactivex/c/k;

    .line 182
    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    aput-object v2, v0, v5

    .line 183
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->L:Lcom/b/c/c;

    const-wide/16 v3, 0x1

    .line 184
    invoke-virtual {v2, v3, v4}, Lcom/b/c/c;->c(J)Lio/reactivex/o;

    move-result-object v2

    .line 185
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    if-eqz v3, :cond_2

    .line 654
    check-cast v3, Lio/reactivex/c/k;

    .line 185
    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    aput-object v2, v0, v6

    .line 186
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 187
    invoke-static {v2}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v2

    .line 188
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    .line 189
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    if-eqz v3, :cond_1

    .line 655
    check-cast v3, Lio/reactivex/c/k;

    .line 189
    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    aput-object v2, v0, v7

    .line 190
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 191
    invoke-static {v2}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v2

    .line 192
    invoke-virtual {v2}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v2

    .line 193
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->J:Lcom/swedbank/mobile/core/ui/u;

    if-eqz v3, :cond_0

    .line 656
    check-cast v3, Lio/reactivex/c/k;

    .line 193
    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    aput-object v2, v0, v8

    .line 175
    invoke-static {v0}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    .line 194
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Lio/reactivex/o;->b(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 195
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->W()Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 154
    invoke-static {v1, v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 196
    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v$g;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$g;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026ream)\n      .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 656
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type io.reactivex.functions.Predicate<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 655
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type io.reactivex.functions.Predicate<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type io.reactivex.functions.Predicate<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 653
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type io.reactivex.functions.Predicate<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 652
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type io.reactivex.functions.Predicate<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 198
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->V()Lio/reactivex/o;

    move-result-object v0

    .line 199
    sget-object v1, Lcom/swedbank/mobile/app/transfer/payment/form/v$h;->a:Lcom/swedbank/mobile/app/transfer/payment/form/v$h;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "recipientInputStream\n      .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->R:Lio/reactivex/o;

    .line 135
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 137
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/v$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v$e;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/v;)V

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeW\u2026Selection.closeIfOpen() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 140
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->S()Lcom/swedbank/mobile/core/ui/widget/SlideToConfirmButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 607
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public j()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/v;->P:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    if-nez v0, :cond_0

    const-string v1, "accountSelection"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 152
    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public k()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 201
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/v;->L()Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 657
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
