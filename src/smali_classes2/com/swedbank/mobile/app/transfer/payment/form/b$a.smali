.class final Lcom/swedbank/mobile/app/transfer/payment/form/b$a;
.super Lkotlin/e/b/k;
.source "AccountSelectionOverlay.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/transfer/b;",
        ">;",
        "Lcom/swedbank/mobile/app/transfer/b;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/b;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/b;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/b$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/b;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/payment/form/b$a;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/transfer/b;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/transfer/b;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectableAccount"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->account_title:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/b;->f()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->account_subtitle:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 52
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/b;->d()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/b;->d()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    xor-int/2addr v1, v2

    const/16 v2, 0x8

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    .line 113
    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 55
    sget v0, Lcom/swedbank/mobile/app/transfer/a$e;->account_selected:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/b;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    .line 115
    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 56
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object v0

    .line 117
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/b$a$a;

    invoke-direct {v1, p2, p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/b$a$a;-><init>(Lcom/swedbank/mobile/app/transfer/b;Lcom/swedbank/mobile/app/transfer/payment/form/b$a;Lcom/swedbank/mobile/core/ui/widget/j;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
