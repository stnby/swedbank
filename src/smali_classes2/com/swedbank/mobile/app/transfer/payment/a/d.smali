.class public final Lcom/swedbank/mobile/app/transfer/payment/a/d;
.super Ljava/lang/Object;
.source "PaymentExecutionResultNotificationProviderImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/payment/execution/o;
.implements Lcom/swedbank/mobile/data/device/h;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/squareup/moshi/n;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/architect/business/a/c;Lcom/squareup/moshi/n;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/squareup/moshi/n;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flowManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d;->c:Lcom/squareup/moshi/n;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Lcom/squareup/moshi/n;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d;->c:Lcom/squareup/moshi/n;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Landroid/app/Application;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d;->a:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Lcom/swedbank/mobile/architect/business/a/c;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/d;->b:Lcom/swedbank/mobile/architect/business/a/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/e/m;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "executionResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;

    invoke-direct {v0, p0, p2, p1}, Lcom/swedbank/mobile/app/transfer/payment/a/d$b;-><init>(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Lcom/swedbank/mobile/business/transfer/payment/execution/n;)V

    check-cast v0, Lcom/swedbank/mobile/business/e/m;

    return-object v0
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/a/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/a/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 85
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->a(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Lcom/squareup/moshi/n;

    move-result-object v1

    .line 86
    const-class v2, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {v1, v2}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    if-eqz v0, :cond_1

    .line 88
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/a/d;->c(Lcom/swedbank/mobile/app/transfer/payment/a/d;)Lcom/swedbank/mobile/architect/business/a/c;

    move-result-object v1

    .line 89
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/payment/a/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/business/overview/statement/details/b;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/overview/statement/details/b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast p1, Lcom/swedbank/mobile/architect/business/a/b;

    goto :goto_0

    .line 90
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/business/transfer/payment/form/b;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/business/transfer/payment/form/b;-><init>(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast p1, Lcom/swedbank/mobile/architect/business/a/b;

    .line 88
    :goto_0
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public b(Lcom/swedbank/mobile/business/transfer/payment/execution/n;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/business/e/m;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "executionResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "input"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance p1, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;

    invoke-direct {p1, p0, p2}, Lcom/swedbank/mobile/app/transfer/payment/a/d$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/a/d;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    check-cast p1, Lcom/swedbank/mobile/business/e/m;

    return-object p1
.end method
