.class public final Lcom/swedbank/mobile/app/transfer/payment/form/a;
.super Ljava/lang/Object;
.source "AccountSelectionAddon.kt"


# instance fields
.field private final a:Landroidx/appcompat/widget/Toolbar;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Lcom/swedbank/mobile/app/transfer/payment/form/b;

.field private final f:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private final i:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final j:I


# direct methods
.method public constructor <init>(Landroidx/constraintlayout/widget/ConstraintLayout;I)V
    .locals 2
    .param p1    # Landroidx/constraintlayout/widget/ConstraintLayout;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->i:Landroidx/constraintlayout/widget/ConstraintLayout;

    iput p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->j:I

    .line 26
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->i:Landroidx/constraintlayout/widget/ConstraintLayout;

    iget p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->j:I

    invoke-virtual {p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "rootView.findViewById(toolbarRes)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a:Landroidx/appcompat/widget/Toolbar;

    .line 31
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<AccountId>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->f:Lcom/b/c/b;

    .line 32
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g:Ljava/util/List;

    .line 36
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "toolbar.context"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/swedbank/mobile/app/transfer/a$g;->item_current_account:I

    const/4 v0, 0x0

    .line 144
    check-cast v0, Landroid/view/ViewGroup;

    .line 147
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 146
    invoke-virtual {p1, p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b:Landroid/view/ViewGroup;

    .line 37
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b:Landroid/view/ViewGroup;

    sget p2, Lcom/swedbank/mobile/app/transfer/a$e;->current_account:I

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "currentAccountView.findV\u2026yId(R.id.current_account)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c:Landroid/widget/TextView;

    .line 38
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b:Landroid/view/ViewGroup;

    sget p2, Lcom/swedbank/mobile/app/transfer/a$e;->drop_down_indicator:I

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "currentAccountView.findV\u2026R.id.drop_down_indicator)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->d:Landroid/widget/ImageView;

    .line 40
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a:Landroidx/appcompat/widget/Toolbar;

    iget-object p2, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b:Landroid/view/ViewGroup;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/Toolbar;->addView(Landroid/view/View;)V

    return-void

    .line 146
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Lcom/swedbank/mobile/app/transfer/payment/form/b;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->e:Lcom/swedbank/mobile/app/transfer/payment/form/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Z)V
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->h:Z

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Z
    .locals 0

    .line 22
    iget-boolean p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->h:Z

    return p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->e:Lcom/swedbank/mobile/app/transfer/payment/form/b;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->i:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/b/c/b;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->f:Lcom/b/c/b;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/TextView;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/payment/form/a;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->j:I

    return p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/ImageView;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->d:Landroid/widget/ImageView;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->f:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g:Ljava/util/List;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_5

    .line 102
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 104
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/app/transfer/b;

    .line 102
    invoke-virtual {v4}, Lcom/swedbank/mobile/app/transfer/b;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_1
    move-object v2, v3

    .line 105
    :goto_0
    check-cast v2, Lcom/swedbank/mobile/app/transfer/b;

    if-eqz v2, :cond_2

    .line 106
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->e(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/b;->f()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_2

    goto :goto_1

    .line 107
    :cond_2
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->e(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/TextView;

    move-result-object v0

    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->payment_form_toolbar_title:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 48
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    .line 49
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g:Ljava/util/List;

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/b;

    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v1, "rootView.context"

    invoke-static {v2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/transfer/payment/form/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    .line 111
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/a;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setListener(Lkotlin/e/a/b;)V

    .line 115
    new-instance v1, Lcom/swedbank/mobile/app/transfer/payment/form/a$b;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/app/transfer/payment/form/a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/a$b;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/a;)V

    check-cast v1, Lkotlin/e/a/a;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->setCancelListener(Lkotlin/e/a/a;)V

    .line 116
    invoke-static {p0, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Lcom/swedbank/mobile/app/transfer/payment/form/b;)V

    .line 52
    :goto_2
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->a(Ljava/util/List;)V

    .line 53
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    .line 118
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/a$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a$c;-><init>(Lcom/swedbank/mobile/app/transfer/payment/form/a;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 58
    :cond_4
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->d:Landroid/widget/ImageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 59
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b:Landroid/view/ViewGroup;

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    :goto_3
    return-void
.end method

.method public final b()Z
    .locals 3

    .line 65
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a;->h:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeView(Landroid/view/View;)V

    .line 123
    :cond_0
    invoke-static {p0, v1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Z)V

    .line 124
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$d;->ic_arrow_drop_down:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
