.class public final Lcom/swedbank/mobile/app/transfer/payment/form/a$c;
.super Lcom/swedbank/mobile/core/ui/h;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    .line 113
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    .line 302
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 303
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    :goto_0
    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_1

    .line 306
    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 307
    check-cast v1, Landroid/app/Activity;

    goto :goto_1

    .line 309
    :cond_0
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context.baseContext"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    .line 312
    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    new-instance v2, Landroid/view/View;

    move-object v3, v1

    check-cast v3, Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_2
    const-string v3, "activity.currentFocus ?: View(activity)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    check-cast v1, Landroid/content/Context;

    const-string v3, "input_method"

    .line 317
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 315
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_3

    .line 317
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_3
    const/4 v0, 0x1

    .line 319
    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Z)V

    .line 320
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->f(Lcom/swedbank/mobile/app/transfer/payment/form/a;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/app/transfer/payment/form/b;->a(Landroidx/constraintlayout/widget/ConstraintLayout;I)V

    .line 321
    :cond_5
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/ImageView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/transfer/a$d;->ic_arrow_drop_up:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    .line 323
    :cond_6
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/a$c;->a:Lcom/swedbank/mobile/app/transfer/payment/form/a;

    .line 324
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->b(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Lcom/swedbank/mobile/app/transfer/payment/form/b;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->c(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeView(Landroid/view/View;)V

    .line 325
    :cond_7
    invoke-static {p1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->a(Lcom/swedbank/mobile/app/transfer/payment/form/a;Z)V

    .line 326
    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/a;->g(Lcom/swedbank/mobile/app/transfer/payment/form/a;)Landroid/widget/ImageView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/transfer/a$d;->ic_arrow_drop_down:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_4
    return-void
.end method
