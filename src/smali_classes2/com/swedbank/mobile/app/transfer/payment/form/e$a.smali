.class final Lcom/swedbank/mobile/app/transfer/payment/form/e$a;
.super Ljava/lang/Object;
.source "PaymentExecutionLoadingMessagesProvider.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/e;->b()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/payment/form/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/payment/form/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/form/e$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/form/e$a;->a:Lcom/swedbank/mobile/app/transfer/payment/form/e;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/e;->a(Lcom/swedbank/mobile/app/transfer/payment/form/e;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 29
    sget v1, Lcom/swedbank/mobile/app/transfer/a$a;->transfer_payment_execution_loading_messages:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "application\n        .res\u2026ecution_loading_messages)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 35
    array-length v2, v0

    if-lez v2, :cond_0

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/form/e$a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
