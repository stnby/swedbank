.class public final Lcom/swedbank/mobile/app/transfer/payment/a/b;
.super Ljava/lang/Object;
.source "PaymentExecutionBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

.field private b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field private c:Ljava/lang/String;

.field private d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

.field private final e:Lcom/swedbank/mobile/a/ac/d/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/ac/d/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/ac/d/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->e:Lcom/swedbank/mobile/a/ac/d/a/a$a;

    .line 19
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "UUID.randomUUID().toString()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/app/transfer/payment/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;

    .line 27
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;)Lcom/swedbank/mobile/app/transfer/payment/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;

    .line 35
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)Lcom/swedbank/mobile/app/transfer/payment/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/execution/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;

    .line 23
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/transfer/payment/a/b;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "paymentExecutionUuid"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;

    .line 31
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->e:Lcom/swedbank/mobile/a/ac/d/a/a$a;

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->a:Lcom/swedbank/mobile/business/transfer/payment/execution/m;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/a/a$a;->b(Lcom/swedbank/mobile/business/transfer/payment/execution/m;)Lcom/swedbank/mobile/a/ac/d/a/a$a;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/a/a$a;->b(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/a/ac/d/a/a$a;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/a/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/ac/d/a/a$a;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/payment/a/b;->d:Lcom/swedbank/mobile/business/transfer/payment/PaymentPriority;

    invoke-static {v1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/d/a/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/ac/d/a/a$a;

    move-result-object v0

    .line 47
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/d/a/a$a;->a()Lcom/swedbank/mobile/a/ac/d/a/a;

    move-result-object v0

    .line 48
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/d/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build payment execution node without input"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 39
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build payment execution node without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
