.class final Lcom/swedbank/mobile/app/transfer/payment/form/n$j;
.super Ljava/lang/Object;
.source "PaymentFormPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/payment/form/n;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/payment/form/n$j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$j;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/payment/form/n$j;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/payment/form/n$j;->a:Lcom/swedbank/mobile/app/transfer/payment/form/n$j;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/d;",
            ">;)",
            "Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 396
    check-cast p1, Ljava/lang/Iterable;

    .line 397
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 398
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 399
    check-cast v1, Lcom/swedbank/mobile/business/a/d;

    .line 396
    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/payment/form/i;->a(Lcom/swedbank/mobile/business/a/d;)Lcom/swedbank/mobile/app/transfer/payment/form/h;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 400
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 99
    new-instance p1, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;-><init>(Ljava/util/List;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/form/n$j;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/transfer/payment/form/ac$a$a;

    move-result-object p1

    return-object p1
.end method
