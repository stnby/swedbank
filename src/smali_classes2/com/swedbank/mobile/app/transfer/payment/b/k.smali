.class public final Lcom/swedbank/mobile/app/transfer/payment/b/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PinChallengeViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/payment/b/j;
.implements Lcom/swedbank/mobile/core/ui/i;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/app/Dialog;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private e:Lio/reactivex/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/EditText;

.field private h:Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 27
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a:Lcom/b/c/c;

    .line 28
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Int>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->b:Lcom/b/c/c;

    .line 29
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<String>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->c:Lcom/b/c/c;

    .line 32
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->e:Lio/reactivex/b/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/content/Context;
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/b/k;Landroid/widget/EditText;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->g:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/b/k;Landroid/widget/TextView;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->f:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/payment/b/k;Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->h:Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/TextView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->f:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/EditText;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->g:Landroid/widget/EditText;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->h:Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/b/c/c;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/b/c/c;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->b:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/b/c/c;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->c:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;
    .locals 1
    .param p1    # Landroidx/appcompat/app/b$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/i$a;->a(Lcom/swedbank/mobile/core/ui/i;Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;

    move-result-object p1

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->b:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 31
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->d:Landroid/app/Dialog;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/payment/b/m;)V
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/transfer/payment/b/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->d()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_6

    .line 45
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/b/m;->a()Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/b/m;->b()Z

    move-result v1

    .line 47
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/b/m;->c()Z

    move-result p1

    .line 122
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/content/Context;

    move-result-object v2

    .line 123
    sget v3, Lcom/swedbank/mobile/app/transfer/a$g;->view_pin_challenge:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 125
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const-string v7, "LayoutInflater.from(this)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v6, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_5

    check-cast v3, Landroid/view/ViewGroup;

    .line 126
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->pin_challenge_code:I

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {p0, v4}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a(Lcom/swedbank/mobile/app/transfer/payment/b/k;Landroid/widget/TextView;)V

    .line 127
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->pin_challenge_entry_input:I

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    invoke-static {p0, v4}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a(Lcom/swedbank/mobile/app/transfer/payment/b/k;Landroid/widget/EditText;)V

    .line 128
    sget v4, Lcom/swedbank/mobile/app/transfer/a$e;->pin_challenge_sign:I

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    invoke-static {p0, v4}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a(Lcom/swedbank/mobile/app/transfer/payment/b/k;Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;)V

    .line 133
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->b(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->c(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_1

    xor-int/lit8 v4, p1, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 135
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->d(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 136
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 137
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setShimmering(Z)V

    .line 140
    :cond_2
    new-instance p1, Landroidx/appcompat/app/b$a;

    invoke-direct {p1, v2}, Landroidx/appcompat/app/b$a;-><init>(Landroid/content/Context;)V

    .line 149
    check-cast v3, Landroid/view/View;

    invoke-virtual {p1, v3}, Landroidx/appcompat/app/b$a;->b(Landroid/view/View;)Landroidx/appcompat/app/b$a;

    move-result-object p1

    .line 148
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/b/k$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k$e;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/k;)V

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/b$a;->a(Landroid/content/DialogInterface$OnCancelListener;)Landroidx/appcompat/app/b$a;

    move-result-object p1

    const/4 v0, 0x1

    .line 147
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/b$a;->a(Z)Landroidx/appcompat/app/b$a;

    move-result-object p1

    const-string v1, "AlertDialog.Builder(cont\u2026     .setCancelable(true)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a(Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;

    move-result-object p1

    .line 154
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/b;->setCanceledOnTouchOutside(Z)V

    .line 155
    invoke-virtual {p1}, Landroidx/appcompat/app/b;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_3

    const v0, 0x20008

    .line 157
    invoke-virtual {p1, v0}, Landroid/view/Window;->clearFlags(I)V

    const/4 v0, 0x5

    .line 158
    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 162
    :cond_3
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->c(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/EditText;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 163
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 167
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/b/k$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k$a;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/k;)V

    move-object v4, v0

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 169
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 171
    invoke-static {p1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/widget/EditText;)Lio/reactivex/o;

    move-result-object p1

    .line 176
    new-instance v0, Lcom/swedbank/mobile/app/transfer/payment/b/k$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k$b;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/k;)V

    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v1

    const-string p1, "keyboardDoneClicks()\n   \u2026ton?.isEnabled ?: false }"

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    new-instance p1, Lcom/swedbank/mobile/app/transfer/payment/b/k$c;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k$c;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/k;)V

    move-object v4, p1

    check-cast v4, Lkotlin/e/a/b;

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 177
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 179
    :cond_4
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->d(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object p1

    if-eqz p1, :cond_9

    .line 180
    check-cast p1, Landroid/view/View;

    .line 184
    new-instance v0, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    move-object v1, v0

    check-cast v1, Lio/reactivex/o;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 183
    new-instance p1, Lcom/swedbank/mobile/app/transfer/payment/b/k$d;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k$d;-><init>(Lcom/swedbank/mobile/app/transfer/payment/b/k;)V

    move-object v4, p1

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 185
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    goto :goto_0

    .line 124
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 50
    :cond_6
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/b/m;->a()Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/b/m;->b()Z

    move-result v1

    .line 52
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/b/m;->c()Z

    move-result p1

    .line 188
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->b(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    :cond_7
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->c(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_8

    xor-int/lit8 v2, p1, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 190
    :cond_8
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->d(Lcom/swedbank/mobile/app/transfer/payment/b/k;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 191
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 192
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setShimmering(Z)V

    :cond_9
    :goto_0
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->e:Lio/reactivex/b/c;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/app/transfer/payment/b/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a(Lcom/swedbank/mobile/app/transfer/payment/b/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->c:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public d()Landroid/app/Dialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->d:Landroid/app/Dialog;

    return-object v0
.end method

.method public f()Lio/reactivex/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/payment/b/k;->e:Lio/reactivex/b/c;

    return-object v0
.end method
