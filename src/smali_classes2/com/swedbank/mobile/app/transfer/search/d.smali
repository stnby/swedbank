.class public final Lcom/swedbank/mobile/app/transfer/search/d;
.super Lcom/swedbank/mobile/architect/a/d;
.source "TransferSearchPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/search/n;",
        "Lcom/swedbank/mobile/app/transfer/search/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/swedbank/mobile/business/transfer/search/a;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/transfer/search/a;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/search/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/d;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/search/d;->b:Lcom/swedbank/mobile/business/transfer/search/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/search/d;)Lcom/swedbank/mobile/business/transfer/search/a;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/search/d;->b:Lcom/swedbank/mobile/business/transfer/search/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/search/d;)Landroid/app/Application;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/search/d;->a:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 6

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/transfer/search/d$b;->a:Lcom/swedbank/mobile/app/transfer/search/d$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/search/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/swedbank/mobile/app/transfer/search/d$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/transfer/search/d$c;-><init>(Lcom/swedbank/mobile/app/transfer/search/d;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/d$d;->a:Lcom/swedbank/mobile/app/transfer/search/d$d;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/transfer/search/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    new-instance v2, Lcom/swedbank/mobile/app/transfer/search/d$e;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/transfer/search/d$e;-><init>(Lcom/swedbank/mobile/app/transfer/search/d;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/search/d;->b:Lcom/swedbank/mobile/business/transfer/search/a;

    .line 36
    invoke-interface {v2}, Lcom/swedbank/mobile/business/transfer/search/a;->a()Lio/reactivex/o;

    move-result-object v2

    .line 37
    invoke-static {}, Lcom/swedbank/mobile/app/transfer/search/f;->a()Lkotlin/e/a/m;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v2, v4, v3, v5, v4}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    .line 38
    sget-object v3, Lcom/swedbank/mobile/app/transfer/search/d$h;->a:Lcom/swedbank/mobile/app/transfer/search/d$h;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 43
    sget-object v3, Lcom/swedbank/mobile/app/transfer/search/d$f;->a:Lcom/swedbank/mobile/app/transfer/search/d$f;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/transfer/search/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 44
    new-instance v5, Lcom/swedbank/mobile/app/transfer/search/d$g;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/transfer/search/d$g;-><init>(Lcom/swedbank/mobile/app/transfer/search/d;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v3, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 49
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 50
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 53
    check-cast v0, Lio/reactivex/s;

    .line 54
    check-cast v1, Lio/reactivex/s;

    .line 55
    check-cast v2, Lio/reactivex/s;

    .line 56
    check-cast v3, Lio/reactivex/s;

    .line 52
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026       resultClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v1, Lcom/swedbank/mobile/app/transfer/search/s;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-direct {v1, v4, v2, v3, v4}, Lcom/swedbank/mobile/app/transfer/search/s;-><init>(Lkotlin/k;ZILkotlin/e/b/g;)V

    .line 59
    new-instance v2, Lcom/swedbank/mobile/app/transfer/search/d$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/transfer/search/d;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/transfer/search/d$a;-><init>(Lcom/swedbank/mobile/app/transfer/search/d;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 73
    new-instance v3, Lcom/swedbank/mobile/app/transfer/search/e;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/search/e;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 74
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
