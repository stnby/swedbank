.class public final Lcom/swedbank/mobile/app/transfer/search/j;
.super Landroidx/recyclerview/widget/RecyclerView$x;
.source "TransferSearchResultsAdapter.kt"


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "avatarView"

    const-string v4, "getAvatarView()Lcom/swedbank/mobile/core/ui/widget/AvatarView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "iconView"

    const-string v4, "getIconView()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "largeTitleView"

    const-string v4, "getLargeTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "titleView"

    const-string v4, "getTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "subtitleView"

    const-string v4, "getSubtitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/search/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$x;-><init>(Landroid/view/View;)V

    .line 81
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->search_result_avatar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/j;->b:Lkotlin/f/c;

    .line 82
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->search_result_icon:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/j;->c:Lkotlin/f/c;

    .line 83
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->search_result_large_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/j;->d:Lkotlin/f/c;

    .line 84
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->search_result_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/j;->e:Lkotlin/f/c;

    .line 85
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->search_result_subtitle:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Landroidx/recyclerview/widget/RecyclerView$x;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/j;->f:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/ImageView;
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->b()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method private final a()Lcom/swedbank/mobile/core/ui/widget/AvatarView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/j;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    return-object v0
.end method

.method private final b()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->c()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/search/j;)Lcom/swedbank/mobile/core/ui/widget/AvatarView;
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->a()Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->d()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final e()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->e()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/search/h;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/app/transfer/search/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "searchResult"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    instance-of v0, p1, Lcom/swedbank/mobile/app/transfer/search/h$b;

    const/4 v1, 0x0

    if-eqz v0, :cond_c

    .line 173
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->a(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 174
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->b(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;

    move-result-object v0

    .line 175
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 180
    check-cast p1, Lcom/swedbank/mobile/app/transfer/search/h$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$b;->c()Ljava/math/BigDecimal;

    move-result-object v3

    const/16 v4, 0x11

    const/4 v5, 0x1

    if-eqz v3, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/c;

    .line 183
    invoke-static {p1}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/c;)Ljava/lang/String;

    move-result-object p1

    .line 186
    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_search_start_new_amount:I

    new-array v7, v5, [Ljava/lang/Object;

    aput-object p1, v7, v1

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "resources.getString(R.st\u2026new_amount, formatAmount)"

    invoke-static {v3, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    move-object v7, v3

    check-cast v7, Ljava/lang/CharSequence;

    new-array v8, v5, [Ljava/lang/String;

    aput-object p1, v8, v1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x6

    const/4 v12, 0x0

    .line 191
    invoke-static/range {v7 .. v12}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 192
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 194
    check-cast v6, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x1

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_1

    .line 196
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 197
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 199
    move-object v8, p1

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v2, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 201
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-virtual {v2, v6, v7, v8, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 203
    :cond_1
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    const-string v7, "append(textPart)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_2
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 209
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$b;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$b;->b()Ljava/lang/String;

    move-result-object p1

    .line 215
    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_search_start_new_recipient_name:I

    new-array v7, v5, [Ljava/lang/Object;

    aput-object p1, v7, v1

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "resources.getString(R.st\u2026ient_name, recipientName)"

    invoke-static {v3, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    move-object v7, v3

    check-cast v7, Ljava/lang/CharSequence;

    new-array v8, v5, [Ljava/lang/String;

    aput-object p1, v8, v1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x6

    const/4 v12, 0x0

    .line 220
    invoke-static/range {v7 .. v12}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 221
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 223
    check-cast v6, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-nez v7, :cond_4

    const/4 v7, 0x1

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    :goto_3
    if-eqz v7, :cond_5

    .line 225
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 226
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 228
    move-object v8, p1

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v2, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 230
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-virtual {v2, v6, v7, v8, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    .line 232
    :cond_5
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    const-string v7, "append(textPart)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 237
    :cond_6
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 238
    :cond_7
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$b;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$b;->e()Ljava/lang/String;

    move-result-object p1

    .line 244
    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_search_start_new_recipient_iban:I

    new-array v7, v5, [Ljava/lang/Object;

    aput-object p1, v7, v1

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "resources.getString(R.st\u2026ient_iban, recipientIban)"

    invoke-static {v3, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    move-object v7, v3

    check-cast v7, Ljava/lang/CharSequence;

    new-array v8, v5, [Ljava/lang/String;

    aput-object p1, v8, v1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x6

    const/4 v12, 0x0

    .line 249
    invoke-static/range {v7 .. v12}, Lkotlin/j/n;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 250
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 252
    check-cast v6, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-nez v7, :cond_8

    const/4 v7, 0x1

    goto :goto_5

    :cond_8
    const/4 v7, 0x0

    :goto_5
    if-eqz v7, :cond_9

    .line 254
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 255
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 257
    move-object v8, p1

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v2, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 259
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-virtual {v2, v6, v7, v8, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4

    .line 261
    :cond_9
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    const-string v7, "append(textPart)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    .line 266
    :cond_a
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_6

    .line 267
    :cond_b
    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_search_start_new_empty:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    const-string v1, "append(resources.getStri\u2026_search_start_new_empty))"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    :goto_6
    new-instance p1, Landroid/text/SpannedString;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {p1, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_8

    .line 90
    :cond_c
    instance-of v0, p1, Lcom/swedbank/mobile/app/transfer/search/h$c;

    if-eqz v0, :cond_e

    .line 273
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->c(Lcom/swedbank/mobile/app/transfer/search/j;)Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    move-result-object v0

    .line 274
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->setVisibility(I)V

    .line 275
    check-cast p1, Lcom/swedbank/mobile/app/transfer/search/h$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$c;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    goto :goto_7

    :cond_d
    const-string v2, ""

    :goto_7
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v2, v1, v3, v4}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Lcom/swedbank/mobile/core/ui/widget/AvatarView;Ljava/lang/String;ZILjava/lang/Object;)V

    .line 276
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 277
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->d(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;

    move-result-object v0

    .line 278
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$c;->b()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 281
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->e(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;

    move-result-object v0

    .line 282
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 283
    check-cast p1, Lcom/swedbank/mobile/business/b;

    invoke-static {p1}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/b;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_8

    .line 91
    :cond_e
    instance-of v0, p1, Lcom/swedbank/mobile/app/transfer/search/h$a;

    if-eqz v0, :cond_f

    .line 286
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->a(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/ImageView;

    move-result-object v0

    .line 287
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 288
    sget v2, Lcom/swedbank/mobile/app/transfer/a$d;->ic_access_time:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 289
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 290
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/j;->b(Lcom/swedbank/mobile/app/transfer/search/j;)Landroid/widget/TextView;

    move-result-object v0

    .line 291
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    check-cast p1, Lcom/swedbank/mobile/app/transfer/search/h$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/h$a;->b()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    :cond_f
    :goto_8
    return-void
.end method
