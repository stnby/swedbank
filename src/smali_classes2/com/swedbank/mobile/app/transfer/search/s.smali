.class public final Lcom/swedbank/mobile/app/transfer/search/s;
.super Ljava/lang/Object;
.source "TransferSearchViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/transfer/search/s$a;
    }
.end annotation


# instance fields
.field private final a:Lkotlin/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/k<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/search/h;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2, v0}, Lcom/swedbank/mobile/app/transfer/search/s;-><init>(Lkotlin/k;ZILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/k;Z)V
    .locals 0
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/search/h;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;Z)V"
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/k;ZILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    .line 8
    check-cast p1, Lkotlin/k;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    .line 9
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/search/s;-><init>(Lkotlin/k;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/transfer/search/s;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/search/s;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/search/s;->a(Lkotlin/k;Z)Lcom/swedbank/mobile/app/transfer/search/s;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Lkotlin/k;Z)Lcom/swedbank/mobile/app/transfer/search/s;
    .locals 1
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/search/h;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;Z)",
            "Lcom/swedbank/mobile/app/transfer/search/s;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lcom/swedbank/mobile/app/transfer/search/s;

    invoke-direct {v0, p1, p2}, Lcom/swedbank/mobile/app/transfer/search/s;-><init>(Lkotlin/k;Z)V

    return-object v0
.end method

.method public final a()Lkotlin/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/k<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/search/h;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/transfer/search/s;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/transfer/search/s;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    iget-object v3, p1, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    iget-boolean p1, p1, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    if-ne v1, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransferSearchViewState(searchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->a:Lkotlin/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/search/s;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
