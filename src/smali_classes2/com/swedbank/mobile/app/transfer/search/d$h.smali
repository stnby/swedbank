.class final Lcom/swedbank/mobile/app/transfer/search/d$h;
.super Ljava/lang/Object;
.source "TransferSearchPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/search/d;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/search/d$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/transfer/search/d$h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/search/d$h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/search/d$h;->a:Lcom/swedbank/mobile/app/transfer/search/d$h;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/transfer/search/s$a$b;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/core/ui/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/j<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;)",
            "Lcom/swedbank/mobile/app/transfer/search/s$a$b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->d()Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    .line 39
    check-cast v0, Ljava/lang/Iterable;

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 74
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 75
    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/business/transfer/search/f;

    .line 76
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->b()Lcom/swedbank/mobile/business/transfer/search/f$a;

    move-result-object v2

    sget-object v3, Lcom/swedbank/mobile/app/transfer/search/i;->a:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/transfer/search/f$a;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 88
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 90
    :pswitch_0
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 88
    new-instance v3, Lcom/swedbank/mobile/app/transfer/search/h$a;

    invoke-direct {v3, v4, v2}, Lcom/swedbank/mobile/app/transfer/search/h$a;-><init>(Lcom/swedbank/mobile/business/transfer/search/f;Ljava/lang/String;)V

    check-cast v3, Lcom/swedbank/mobile/app/transfer/search/h;

    goto :goto_1

    .line 90
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 82
    :pswitch_1
    new-instance v2, Lcom/swedbank/mobile/app/transfer/search/h$b;

    .line 84
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->e()Ljava/lang/String;

    move-result-object v5

    .line 85
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->r_()Ljava/lang/String;

    move-result-object v6

    .line 86
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->c()Ljava/math/BigDecimal;

    move-result-object v7

    .line 87
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->d()Ljava/lang/String;

    move-result-object v8

    move-object v3, v2

    .line 82
    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/app/transfer/search/h$b;-><init>(Lcom/swedbank/mobile/business/transfer/search/f;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V

    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/app/transfer/search/h;

    goto :goto_1

    .line 77
    :pswitch_2
    new-instance v2, Lcom/swedbank/mobile/app/transfer/search/h$c;

    .line 79
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->e()Ljava/lang/String;

    move-result-object v3

    .line 80
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->r_()Ljava/lang/String;

    move-result-object v5

    .line 81
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/transfer/search/f;->s_()Ljava/lang/String;

    move-result-object v6

    .line 77
    invoke-direct {v2, v4, v3, v5, v6}, Lcom/swedbank/mobile/app/transfer/search/h$c;-><init>(Lcom/swedbank/mobile/business/transfer/search/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/app/transfer/search/h;

    .line 91
    :goto_1
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 40
    new-instance v0, Lcom/swedbank/mobile/app/transfer/search/s$a$b;

    invoke-static {v1, p1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/transfer/search/s$a$b;-><init>(Lkotlin/k;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/core/ui/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/search/d$h;->a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/transfer/search/s$a$b;

    move-result-object p1

    return-object p1
.end method
