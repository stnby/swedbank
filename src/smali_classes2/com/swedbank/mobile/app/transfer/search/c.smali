.class public final Lcom/swedbank/mobile/app/transfer/search/c;
.super Lcom/swedbank/mobile/architect/a/b/a/a;
.source "TransferSearchEnterTransition.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)Landroidx/l/n;
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string p2, "container"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    .line 25
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 26
    sget p2, Lcom/swedbank/mobile/app/transfer/a$f;->anim_default_dur:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    int-to-long p2, p2

    .line 27
    sget p4, Lcom/swedbank/mobile/app/transfer/a$f;->anim_short_dur:I

    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p4

    int-to-long v0, p4

    .line 28
    sget p4, Lcom/swedbank/mobile/app/transfer/a$h;->tag_transfer_search_transition_target:I

    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    const-string v2, "resources.getString(R.st\u2026search_transition_target)"

    invoke-static {p4, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->tag_transfer_search_toolbar_transition_target:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "resources.getString(R.st\u2026oolbar_transition_target)"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v2, Landroidx/l/r;

    invoke-direct {v2}, Landroidx/l/r;-><init>()V

    .line 32
    new-instance v3, Lcom/swedbank/mobile/core/ui/a/a;

    invoke-direct {v3}, Lcom/swedbank/mobile/core/ui/a/a;-><init>()V

    .line 33
    invoke-virtual {v3, p2, p3}, Lcom/swedbank/mobile/core/ui/a/a;->setDuration(J)Landroidx/l/n;

    move-result-object v3

    .line 34
    invoke-virtual {v3, p1}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p1

    .line 32
    invoke-virtual {v2, p1}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 35
    new-instance v2, Lcom/swedbank/mobile/core/ui/a/b;

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v3, v4}, Lcom/swedbank/mobile/core/ui/a/b;-><init>(ZZILkotlin/e/b/g;)V

    .line 36
    invoke-virtual {v2, p2, p3}, Lcom/swedbank/mobile/core/ui/a/b;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 37
    invoke-virtual {p2, p4}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 35
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 38
    new-instance p2, Landroidx/l/d;

    const/4 p3, 0x2

    invoke-direct {p2, p3}, Landroidx/l/d;-><init>(I)V

    const-wide/16 p3, 0x14

    .line 39
    invoke-virtual {p2, p3, p4}, Landroidx/l/d;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 40
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_app_bar_layout:I

    invoke-virtual {p2, p3}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    move-result-object p2

    .line 38
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 41
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/c;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/c;-><init>()V

    .line 42
    invoke-virtual {p2, v0, v1}, Lcom/swedbank/mobile/core/ui/a/c;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 43
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_search_results:I

    invoke-virtual {p2, p3}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    move-result-object p2

    .line 41
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    const-string p2, "TransitionSet()\n        \u2026transfer_search_results))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/l/n;

    return-object p1

    .line 21
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "TransferSearchEnterTransition can only be used for push transitions"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
