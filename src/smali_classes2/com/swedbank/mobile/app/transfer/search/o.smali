.class public final Lcom/swedbank/mobile/app/transfer/search/o;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "TransferSearchViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/search/n;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lcom/swedbank/mobile/app/transfer/search/k;

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "input"

    const-string v4, "getInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "inputLoading"

    const-string v4, "getInputLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "clearBtn"

    const-string v4, "getClearBtn()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/search/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "resultsView"

    const-string v4, "getResultsView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/search/o;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->h:Lio/reactivex/o;

    .line 30
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->b:Lkotlin/f/c;

    .line 31
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_search_input:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->c:Lkotlin/f/c;

    .line 32
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_search_input_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->d:Lkotlin/f/c;

    .line 33
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_search_clear_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->e:Lkotlin/f/c;

    .line 34
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_search_results:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->f:Lkotlin/f/c;

    .line 36
    new-instance p1, Lcom/swedbank/mobile/app/transfer/search/k;

    invoke-direct {p1}, Lcom/swedbank/mobile/app/transfer/search/k;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->g:Lcom/swedbank/mobile/app/transfer/search/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/search/o;)Landroid/widget/EditText;
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->f()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/search/o;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->i()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/search/o;)Lcom/swedbank/mobile/app/transfer/search/k;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->g:Lcom/swedbank/mobile/app/transfer/search/k;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/search/o;)Landroid/view/View;
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->h()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final f()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final g()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final h()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final i()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->h:Lio/reactivex/o;

    .line 72
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeW\u2026olbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/search/s;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/transfer/search/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/s;->a()Lkotlin/k;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/search/o;->g:Lcom/swedbank/mobile/app/transfer/search/k;

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/app/transfer/search/k;->a(Lkotlin/k;)V

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->g()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/search/s;->b()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    const/4 p1, 0x4

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 130
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/app/transfer/search/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/search/o;->a(Lcom/swedbank/mobile/app/transfer/search/s;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 74
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->f()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/b/b/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 77
    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o$d;->a:Lcom/swedbank/mobile/app/transfer/search/o$d;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/transfer/search/q;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/search/q;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/swedbank/mobile/app/transfer/search/o$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/transfer/search/o$e;-><init>(Lcom/swedbank/mobile/app/transfer/search/o;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    .line 81
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lio/reactivex/o;->b(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "input\n      .textChanges\u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/transfer/search/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/o;->g:Lcom/swedbank/mobile/app/transfer/search/k;

    .line 62
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/search/k;->a()Lio/reactivex/o;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/swedbank/mobile/app/transfer/search/o$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/transfer/search/o$f;-><init>(Lcom/swedbank/mobile/app/transfer/search/o;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 69
    sget-object v1, Lcom/swedbank/mobile/app/transfer/search/o$g;->a:Lcom/swedbank/mobile/app/transfer/search/o$g;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "resultsAdapter\n      .ob\u2026pe !== HISTORICAL_INPUT }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected e()V
    .locals 8

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->o()Landroid/view/View;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "rootView.context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    :goto_0
    instance-of v2, v1, Landroid/content/ContextWrapper;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 92
    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 93
    check-cast v1, Landroid/app/Activity;

    goto :goto_1

    .line 95
    :cond_0
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context.baseContext"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_1
    if-eqz v1, :cond_3

    .line 98
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const-string v2, "window"

    .line 99
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const-string v4, "window.decorView"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v4, Lcom/swedbank/mobile/app/transfer/search/o$b;

    invoke-direct {v4, v2, v0, v1}, Lcom/swedbank/mobile/app/transfer/search/o$b;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/Window;)V

    check-cast v4, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 101
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 103
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    const/16 v6, 0x10

    if-eq v5, v6, :cond_2

    .line 104
    invoke-virtual {v1, v6}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 107
    :cond_2
    new-instance v5, Lcom/swedbank/mobile/app/transfer/search/o$c;

    invoke-direct {v5, v2, v4, v1}, Lcom/swedbank/mobile/app/transfer/search/o$c;-><init>(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/Window;)V

    check-cast v5, Lio/reactivex/c/a;

    invoke-static {v5}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction {\u2026INPUT_ADJUST_NOTHING)\n  }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 89
    :cond_3
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.disposed()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    :goto_2
    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 110
    new-instance v1, Lcom/swedbank/mobile/app/transfer/search/o$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/search/o$a;-><init>(Landroid/view/View;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v0, Lcom/swedbank/mobile/app/transfer/search/p;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/transfer/search/p;-><init>(Lkotlin/e/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction(rootView::hideKeyboard)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->f()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 113
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "input_method"

    .line 117
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 119
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->b(Lcom/swedbank/mobile/app/transfer/search/o;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    .line 120
    new-instance v4, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    check-cast v4, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 121
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->c(Lcom/swedbank/mobile/app/transfer/search/o;)Lcom/swedbank/mobile/app/transfer/search/k;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 122
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/e;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "context"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x2

    invoke-direct {v1, v4, v2, v5, v3}, Lcom/swedbank/mobile/core/ui/widget/e;-><init>(Landroid/content/Context;IILkotlin/e/b/g;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 124
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$f;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Landroidx/recyclerview/widget/e;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/e;->a(Z)V

    .line 46
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/search/o;->h()Landroid/view/View;

    move-result-object v0

    .line 127
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    move-object v2, v1

    check-cast v2, Lio/reactivex/o;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 47
    new-instance v0, Lcom/swedbank/mobile/app/transfer/search/o$h;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/search/o$h;-><init>(Lcom/swedbank/mobile/app/transfer/search/o;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 128
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void

    .line 124
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_5
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
