.class final Lcom/swedbank/mobile/app/transfer/search/d$g;
.super Ljava/lang/Object;
.source "TransferSearchPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/search/d;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Lcom/swedbank/mobile/business/transfer/search/f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/search/d;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/search/d;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/search/d$g;->a:Lcom/swedbank/mobile/app/transfer/search/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/search/f;)V
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/d$g;->a:Lcom/swedbank/mobile/app/transfer/search/d;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/search/d;->a(Lcom/swedbank/mobile/app/transfer/search/d;)Lcom/swedbank/mobile/business/transfer/search/a;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/search/d$g;->a:Lcom/swedbank/mobile/app/transfer/search/d;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/search/d;->b(Lcom/swedbank/mobile/app/transfer/search/d;)Landroid/app/Application;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_search_prefilled_description:I

    invoke-virtual {v1, v2}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/business/transfer/search/f;->a(Ljava/lang/String;)Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    move-result-object p1

    .line 45
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/transfer/search/a;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/business/transfer/search/f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/search/d$g;->a(Lcom/swedbank/mobile/business/transfer/search/f;)V

    return-void
.end method
