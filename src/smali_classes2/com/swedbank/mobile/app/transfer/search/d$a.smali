.class final synthetic Lcom/swedbank/mobile/app/transfer/search/d$a;
.super Lkotlin/e/b/i;
.source "TransferSearchPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/search/d;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/transfer/search/s;",
        "Lcom/swedbank/mobile/app/transfer/search/s$a;",
        "Lcom/swedbank/mobile/app/transfer/search/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/search/d;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/search/s;Lcom/swedbank/mobile/app/transfer/search/s$a;)Lcom/swedbank/mobile/app/transfer/search/s;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/transfer/search/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/search/s$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/search/d$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/transfer/search/d;

    .line 74
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/search/s$a$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p2, Lcom/swedbank/mobile/app/transfer/search/s$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/search/s$a$a;->a()Z

    move-result p2

    const/4 v0, 0x1

    invoke-static {p1, v1, p2, v0, v1}, Lcom/swedbank/mobile/app/transfer/search/s;->a(Lcom/swedbank/mobile/app/transfer/search/s;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/search/s;

    move-result-object p1

    goto :goto_0

    .line 75
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/search/s$a$b;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/swedbank/mobile/app/transfer/search/s$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/search/s$a$b;->a()Lkotlin/k;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v2, 0x2

    invoke-static {p1, p2, v0, v2, v1}, Lcom/swedbank/mobile/app/transfer/search/s;->a(Lcom/swedbank/mobile/app/transfer/search/s;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/search/s;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/swedbank/mobile/app/transfer/search/s;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/search/s$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/search/d$a;->a(Lcom/swedbank/mobile/app/transfer/search/s;Lcom/swedbank/mobile/app/transfer/search/s$a;)Lcom/swedbank/mobile/app/transfer/search/s;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/search/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/transfer/search/TransferSearchViewState;Lcom/swedbank/mobile/app/transfer/search/TransferSearchViewState$PartialState;)Lcom/swedbank/mobile/app/transfer/search/TransferSearchViewState;"

    return-object v0
.end method
