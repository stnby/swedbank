.class final Lcom/swedbank/mobile/app/transfer/search/o$g;
.super Ljava/lang/Object;
.source "TransferSearchViewImpl.kt"

# interfaces
.implements Lio/reactivex/c/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/search/o;->c()Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/k<",
        "Lcom/swedbank/mobile/business/transfer/search/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/search/o$g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/transfer/search/o$g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/search/o$g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/search/o$g;->a:Lcom/swedbank/mobile/app/transfer/search/o$g;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/search/f;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/search/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/search/f;->b()Lcom/swedbank/mobile/business/transfer/search/f$a;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/business/transfer/search/f$a;->c:Lcom/swedbank/mobile/business/transfer/search/f$a;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/business/transfer/search/f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/search/o$g;->a(Lcom/swedbank/mobile/business/transfer/search/f;)Z

    move-result p1

    return p1
.end method
