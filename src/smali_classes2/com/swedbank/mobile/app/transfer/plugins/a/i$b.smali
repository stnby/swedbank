.class final Lcom/swedbank/mobile/app/transfer/plugins/a/i$b;
.super Lkotlin/e/b/k;
.source "TransferOwnAccountsPreviewRenderer.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/plugins/a/i;->a(Lcom/swedbank/mobile/app/transfer/plugins/a/g;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lkotlin/k<",
        "+",
        "Landroid/view/View;",
        "+",
        "Lcom/swedbank/mobile/app/transfer/plugins/a/d;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/c/g;


# direct methods
.method constructor <init>(Lio/reactivex/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/i$b;->a:Lio/reactivex/c/g;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)V
    .locals 8
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Landroid/view/View;",
            "Lcom/swedbank/mobile/app/transfer/plugins/a/d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/app/transfer/plugins/a/d;

    .line 40
    iget-object v7, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/i$b;->a:Lio/reactivex/c/g;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_account_avatar:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.transf\u2026_overview_account_avatar)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_account_title:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.transfer_overview_account_title)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p1

    check-cast v5, Landroid/widget/TextView;

    .line 46
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_account_subtitle:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.transf\u2026verview_account_subtitle)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, p1

    check-cast v6, Landroid/widget/TextView;

    .line 50
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->d()Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->f()Z

    move-result v1

    .line 49
    invoke-virtual {v4, p1, v1}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Ljava/lang/String;Z)V

    .line 52
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->h()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v5, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->g()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    new-instance p1, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;

    move-object v1, p1

    move-object v3, v0

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/a/d;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/AvatarView;Landroid/widget/TextView;Landroid/widget/TextView;Lio/reactivex/c/g;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/plugins/a/i$b;->a(Lkotlin/k;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
