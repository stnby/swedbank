.class public final Lcom/swedbank/mobile/app/transfer/plugins/d/e;
.super Ljava/lang/Object;
.source "TransferSuggestedItem.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/transfer/suggested/a;)Lcom/swedbank/mobile/app/transfer/plugins/d/d;
    .locals 12
    .param p0    # Lcom/swedbank/mobile/business/transfer/suggested/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toTransferSuggestedItem"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/d/d;

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/suggested/a;->b()Ljava/lang/String;

    move-result-object v2

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/suggested/a;->e()Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    move-result-object v1

    sget-object v3, Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;->INVOICE:Lcom/swedbank/mobile/business/transfer/suggested/SuggestedPaymentType;

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 28
    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/suggested/a;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_1
    move-object v4, v1

    goto :goto_2

    :cond_1
    const-string v1, ""

    goto :goto_1

    .line 29
    :goto_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/suggested/a;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_3
    move-object v5, v1

    goto :goto_4

    :cond_2
    const-string v1, ""

    goto :goto_3

    .line 30
    :goto_4
    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/business/b;

    invoke-static {v1}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/b;)Ljava/lang/String;

    move-result-object v6

    .line 31
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/transfer/suggested/a;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    :goto_5
    move-object v7, v1

    goto :goto_6

    :cond_3
    const-string v1, ""

    goto :goto_5

    .line 32
    :goto_6
    check-cast p0, Lcom/swedbank/mobile/business/c;

    invoke-static {p0}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/c;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x80

    const/4 v11, 0x0

    move-object v1, v0

    .line 25
    invoke-direct/range {v1 .. v11}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    return-object v0
.end method
