.class public final Lcom/swedbank/mobile/app/transfer/plugins/d/h;
.super Ljava/lang/Object;
.source "TransferSuggestedPreviewItem.kt"


# direct methods
.method public static final a(Ljava/util/List;Z)Ljava/util/List;
    .locals 9
    .param p0    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/suggested/a;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/plugins/d/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toTransferSuggestedPreviewItem"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    check-cast p0, Ljava/lang/Iterable;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 22
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 23
    check-cast v1, Lcom/swedbank/mobile/business/transfer/suggested/a;

    .line 19
    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/plugins/d/e;->a(Lcom/swedbank/mobile/business/transfer/suggested/a;)Lcom/swedbank/mobile/app/transfer/plugins/d/d;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 24
    :cond_0
    move-object v3, v0

    check-cast v3, Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 18
    new-instance p0, Lcom/swedbank/mobile/app/transfer/plugins/d/g;

    move-object v2, p0

    move v4, p1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/transfer/plugins/d/g;-><init>(Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-static {p0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method
