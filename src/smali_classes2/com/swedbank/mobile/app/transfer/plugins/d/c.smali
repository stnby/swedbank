.class public final Lcom/swedbank/mobile/app/transfer/plugins/d/c;
.super Ljava/lang/Object;
.source "TransferSuggestedDataMapper.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/e/a/b<",
        "Ljava/util/List<",
        "+",
        "Lcom/swedbank/mobile/business/transfer/suggested/a;",
        ">;",
        "Lcom/swedbank/mobile/app/plugins/list/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/plugins/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/d/c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/plugins/d/c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/d/c;->a:Lcom/swedbank/mobile/app/transfer/plugins/d/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Lcom/swedbank/mobile/app/plugins/list/g;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/transfer/suggested/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/plugins/list/g;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "unmappedData"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    check-cast p1, Ljava/lang/Iterable;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 15
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 16
    check-cast v1, Lcom/swedbank/mobile/business/transfer/suggested/a;

    .line 12
    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/plugins/d/e;->a(Lcom/swedbank/mobile/business/transfer/suggested/a;)Lcom/swedbank/mobile/app/transfer/plugins/d/d;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 17
    :cond_0
    check-cast v0, Ljava/util/List;

    const/4 p1, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 11
    new-instance v3, Lcom/swedbank/mobile/app/plugins/list/g;

    invoke-direct {v3, v0, p1, v1, v2}, Lcom/swedbank/mobile/app/plugins/list/g;-><init>(Ljava/util/List;ZILkotlin/e/b/g;)V

    return-object v3
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/c;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/plugins/list/g;

    move-result-object p1

    return-object p1
.end method
