.class public final Lcom/swedbank/mobile/app/transfer/plugins/a/k;
.super Ljava/lang/Object;
.source "TransferOwnAccountsRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/transfer/plugins/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/plugins/a/k;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/a/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/plugins/a/k;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->a:Lcom/swedbank/mobile/app/transfer/plugins/a/k;

    .line 19
    const-class v0, Lcom/swedbank/mobile/app/transfer/plugins/a/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->b:Lkotlin/h/b;

    .line 20
    sget v0, Lcom/swedbank/mobile/app/transfer/a$g;->item_transfer_overview_account:I

    sput v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->c:I

    const/4 v0, 0x1

    .line 21
    sput-boolean v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/a/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    sget-object v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/plugins/a/d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/transfer/plugins/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/plugins/a/d;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object p2, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string p3, "holder.itemView"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_account_avatar:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.transf\u2026_overview_account_avatar)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p3

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    .line 54
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_account_title:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.transfer_overview_account_title)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p3

    check-cast v4, Landroid/widget/TextView;

    .line 55
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_account_subtitle:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.transf\u2026verview_account_subtitle)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p3

    check-cast v5, Landroid/widget/TextView;

    .line 59
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->d()Ljava/lang/String;

    move-result-object p3

    .line 60
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->f()Z

    move-result v0

    .line 58
    invoke-virtual {v3, p3, v0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Ljava/lang/String;Z)V

    .line 61
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->h()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v4, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->g()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    new-instance p3, Lcom/swedbank/mobile/app/transfer/plugins/a/j$b;

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/transfer/plugins/a/j$b;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/a/d;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/AvatarView;Landroid/widget/TextView;Landroid/widget/TextView;Lio/reactivex/c/g;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/transfer/plugins/a/d;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->a(Lcom/swedbank/mobile/app/transfer/plugins/a/d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 20
    sget v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 21
    sget-boolean v0, Lcom/swedbank/mobile/app/transfer/plugins/a/k;->d:Z

    return v0
.end method
