.class public final Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;
.super Lcom/swedbank/mobile/core/ui/h;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/plugins/a/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/plugins/a/d;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Lcom/swedbank/mobile/core/ui/widget/AvatarView;

.field final synthetic d:Landroid/widget/TextView;

.field final synthetic e:Landroid/widget/TextView;

.field final synthetic f:Lio/reactivex/c/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/plugins/a/d;Landroid/view/View;Lcom/swedbank/mobile/core/ui/widget/AvatarView;Landroid/widget/TextView;Landroid/widget/TextView;Lio/reactivex/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->a:Lcom/swedbank/mobile/app/transfer/plugins/a/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->c:Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->d:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->e:Landroid/widget/TextView;

    iput-object p6, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->f:Lio/reactivex/c/g;

    .line 113
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->f:Lio/reactivex/c/g;

    new-instance v7, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;

    .line 302
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->a:Lcom/swedbank/mobile/app/transfer/plugins/a/d;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->d()Ljava/lang/String;

    move-result-object v1

    .line 303
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->a:Lcom/swedbank/mobile/app/transfer/plugins/a/d;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/plugins/a/d;->b()Ljava/lang/String;

    move-result-object v2

    .line 304
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/plugins/a/j$a;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/swedbank/mobile/app/transfer/a$h;->payment_between_own_accounts_prefilled_description:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "resources.getString(R.st\u2026ts_prefilled_description)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    .line 301
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/business/transfer/plugins/accounts/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/e/b/g;)V

    invoke-interface {p1, v7}, Lio/reactivex/c/g;->b(Ljava/lang/Object;)V

    return-void
.end method
