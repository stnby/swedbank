.class final Lcom/swedbank/mobile/app/transfer/plugins/d/i$b;
.super Lkotlin/e/b/k;
.source "TransferSuggestedPreviewRenderer.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/plugins/d/i;->a(Lcom/swedbank/mobile/app/transfer/plugins/d/g;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lkotlin/k<",
        "+",
        "Landroid/view/View;",
        "+",
        "Lcom/swedbank/mobile/app/transfer/plugins/d/d;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/c/g;


# direct methods
.method constructor <init>(Lio/reactivex/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/plugins/d/i$b;->a:Lio/reactivex/c/g;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)V
    .locals 12
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Landroid/view/View;",
            "Lcom/swedbank/mobile/app/transfer/plugins/d/d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/app/transfer/plugins/d/d;

    .line 40
    iget-object v10, p0, Lcom/swedbank/mobile/app/transfer/plugins/d/i$b;->a:Lio/reactivex/c/g;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_avatar:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.suggested_payment_avatar)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p1

    check-cast v5, Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_icon:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.suggested_payment_icon)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Landroid/widget/ImageView;

    .line 46
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_title:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.suggested_payment_title)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, p1

    check-cast v6, Landroid/widget/TextView;

    .line 47
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_subtitle:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.suggested_payment_subtitle)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, p1

    check-cast v7, Landroid/widget/TextView;

    .line 48
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_description:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.suggested_payment_description)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, p1

    check-cast v8, Landroid/widget/TextView;

    .line 49
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_amount:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "findViewById(R.id.suggested_payment_amount)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v9, p1

    check-cast v9, Landroid/widget/TextView;

    .line 53
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->b()Z

    move-result p1

    const/4 v1, 0x4

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 55
    invoke-virtual {v5, v1}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->setVisibility(I)V

    goto :goto_0

    .line 58
    :cond_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 59
    invoke-virtual {v5, v3}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->setVisibility(I)V

    .line 60
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->c()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x2

    const/4 v11, 0x0

    invoke-static {v5, p1, v3, v1, v11}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Lcom/swedbank/mobile/core/ui/widget/AvatarView;Ljava/lang/String;ZILjava/lang/Object;)V

    .line 63
    :goto_0
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->d()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->f()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v7, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->g()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v8, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->h()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v9, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    new-instance p1, Lcom/swedbank/mobile/app/transfer/plugins/d/j$a;

    move-object v1, p1

    move-object v3, v0

    invoke-direct/range {v1 .. v10}, Lcom/swedbank/mobile/app/transfer/plugins/d/j$a;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/d/d;Landroid/view/View;Landroid/widget/ImageView;Lcom/swedbank/mobile/core/ui/widget/AvatarView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lio/reactivex/c/g;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/i$b;->a(Lkotlin/k;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
