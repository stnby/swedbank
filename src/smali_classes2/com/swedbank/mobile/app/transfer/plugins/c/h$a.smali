.class final Lcom/swedbank/mobile/app/transfer/plugins/c/h$a;
.super Lkotlin/e/b/k;
.source "TransferServicesPreviewRenderer.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/plugins/c/h;->a(Lcom/swedbank/mobile/app/transfer/plugins/c/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lkotlin/k<",
        "+",
        "Landroid/view/View;",
        "+",
        "Lcom/swedbank/mobile/app/transfer/plugins/c/d;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/c/g;


# direct methods
.method constructor <init>(Lio/reactivex/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/plugins/c/h$a;->a:Lio/reactivex/c/g;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/k;)V
    .locals 5
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Landroid/view/View;",
            "Lcom/swedbank/mobile/app/transfer/plugins/c/d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/transfer/plugins/c/d;

    .line 36
    sget v1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_service_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "rootView.findViewById(R.id.transfer_service_icon)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/ImageView;

    .line 37
    sget v2, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_service_title:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "rootView.findViewById(R.id.transfer_service_title)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/TextView;

    .line 39
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/c/d;->b()Lcom/swedbank/mobile/business/services/a/a;

    move-result-object p1

    .line 40
    sget-object v3, Lcom/swedbank/mobile/app/transfer/plugins/c/i;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/services/a/a;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 49
    new-instance v0, Lkotlin/j;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rendering of the requested service is not implemented: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 46
    :pswitch_0
    sget v3, Lcom/swedbank/mobile/app/transfer/a$d;->ic_payments_list:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 47
    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_service_payments_list:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 42
    :pswitch_1
    sget v3, Lcom/swedbank/mobile/app/transfer/a$d;->ic_e_invoice:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 43
    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_service_e_invoices:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 57
    :goto_0
    new-instance v1, Lcom/swedbank/mobile/app/transfer/plugins/c/h$a$a;

    invoke-direct {v1, p0, p1}, Lcom/swedbank/mobile/app/transfer/plugins/c/h$a$a;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/c/h$a;Lcom/swedbank/mobile/business/services/a/a;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/plugins/c/h$a;->a(Lkotlin/k;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
