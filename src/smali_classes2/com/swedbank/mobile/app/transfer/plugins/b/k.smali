.class public final Lcom/swedbank/mobile/app/transfer/plugins/b/k;
.super Ljava/lang/Object;
.source "TransferPredefinedRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/transfer/plugins/b/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/plugins/b/k;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/b/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/plugins/b/k;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->a:Lcom/swedbank/mobile/app/transfer/plugins/b/k;

    .line 18
    const-class v0, Lcom/swedbank/mobile/app/transfer/plugins/b/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->b:Lkotlin/h/b;

    .line 19
    sget v0, Lcom/swedbank/mobile/app/transfer/a$g;->item_predefined_payment:I

    sput v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->c:I

    const/4 v0, 0x1

    .line 20
    sput-boolean v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/b/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    sget-object v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/plugins/b/d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/transfer/plugins/b/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/plugins/b/d;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object p2, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string p3, "holder.itemView"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->predefined_payment_title:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.predefined_payment_title)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p3

    check-cast v3, Landroid/widget/TextView;

    .line 48
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->predefined_payment_subtitle:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.predefined_payment_subtitle)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p3

    check-cast v4, Landroid/widget/TextView;

    .line 49
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->predefined_payment_amount:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.predefined_payment_amount)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p3

    check-cast v5, Landroid/widget/TextView;

    .line 52
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/b/d;->b()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/b/d;->c()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v4, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/b/d;->d()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    new-instance p3, Lcom/swedbank/mobile/app/transfer/plugins/b/j$b;

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/transfer/plugins/b/j$b;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/b/d;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lio/reactivex/c/g;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/transfer/plugins/b/d;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->a(Lcom/swedbank/mobile/app/transfer/plugins/b/d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 19
    sget v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 20
    sget-boolean v0, Lcom/swedbank/mobile/app/transfer/plugins/b/k;->d:Z

    return v0
.end method
