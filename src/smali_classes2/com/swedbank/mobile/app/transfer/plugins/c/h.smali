.class public final Lcom/swedbank/mobile/app/transfer/plugins/c/h;
.super Ljava/lang/Object;
.source "TransferServicesPreviewRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/transfer/plugins/c/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/plugins/c/h;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/c/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/plugins/c/h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h;->a:Lcom/swedbank/mobile/app/transfer/plugins/c/h;

    .line 21
    const-class v0, Lcom/swedbank/mobile/app/transfer/plugins/c/f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h;->b:Lkotlin/h/b;

    .line 22
    sget v0, Lcom/swedbank/mobile/app/transfer/a$g;->item_transfer_preview:I

    sput v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h;->c:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/c/f;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    sget-object v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/plugins/c/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 6
    .param p1    # Lcom/swedbank/mobile/app/transfer/plugins/c/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/plugins/c/f;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object p2, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    if-eqz p2, :cond_0

    check-cast p2, Lcom/swedbank/mobile/app/transfer/plugins/TransferPreviewLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p2

    .line 31
    invoke-static/range {v0 .. v5}, Lcom/swedbank/mobile/app/transfer/plugins/TransferPreviewLayout;->a(Lcom/swedbank/mobile/app/transfer/plugins/TransferPreviewLayout;ZLjava/lang/Integer;Lkotlin/e/a/b;ILjava/lang/Object;)V

    .line 33
    sget p3, Lcom/swedbank/mobile/app/transfer/a$g;->item_transfer_service:I

    .line 34
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/c/f;->b()Ljava/util/List;

    move-result-object p1

    .line 35
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h$a;

    invoke-direct {v0, p4}, Lcom/swedbank/mobile/app/transfer/plugins/c/h$a;-><init>(Lio/reactivex/c/g;)V

    check-cast v0, Lkotlin/e/a/b;

    .line 32
    invoke-virtual {p2, p3, p1, v0}, Lcom/swedbank/mobile/app/transfer/plugins/TransferPreviewLayout;->a(ILjava/util/List;Lkotlin/e/a/b;)V

    return-void

    .line 30
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.app.transfer.plugins.TransferPreviewLayout"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/transfer/plugins/c/f;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/transfer/plugins/c/h;->a(Lcom/swedbank/mobile/app/transfer/plugins/c/f;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 22
    sget v0, Lcom/swedbank/mobile/app/transfer/plugins/c/h;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 20
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/f$a;->a(Lcom/swedbank/mobile/app/plugins/list/f;)Z

    move-result v0

    return v0
.end method
