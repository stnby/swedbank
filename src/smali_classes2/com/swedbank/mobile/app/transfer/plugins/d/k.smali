.class public final Lcom/swedbank/mobile/app/transfer/plugins/d/k;
.super Ljava/lang/Object;
.source "TransferSuggestedRenderer.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/plugins/list/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/swedbank/mobile/app/plugins/list/f<",
        "Lcom/swedbank/mobile/app/transfer/plugins/d/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/transfer/plugins/d/k;

.field private static final b:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/d/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final c:I

# The value of this static final field might be set in the static constructor
.field private static final d:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/transfer/plugins/d/k;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->a:Lcom/swedbank/mobile/app/transfer/plugins/d/k;

    .line 20
    const-class v0, Lcom/swedbank/mobile/app/transfer/plugins/d/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    sput-object v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->b:Lkotlin/h/b;

    .line 21
    sget v0, Lcom/swedbank/mobile/app/transfer/a$g;->item_suggested_payment:I

    sput v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->c:I

    const/4 v0, 0x1

    .line 22
    sput-boolean v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "Lcom/swedbank/mobile/app/transfer/plugins/d/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    sget-object v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->b:Lkotlin/h/b;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/plugins/d/d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/transfer/plugins/d/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/plugins/d/d;",
            "Lcom/swedbank/mobile/app/plugins/list/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;",
            "Lio/reactivex/c/g<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "actionConsumer"

    invoke-static {p4, p3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object p2, p2, Lcom/swedbank/mobile/app/plugins/list/h;->itemView:Landroid/view/View;

    const-string p3, "holder.itemView"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_avatar:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.suggested_payment_avatar)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p3

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/AvatarView;

    .line 65
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_icon:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.suggested_payment_icon)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p3

    check-cast v3, Landroid/widget/ImageView;

    .line 66
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_title:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.suggested_payment_title)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p3

    check-cast v5, Landroid/widget/TextView;

    .line 67
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_subtitle:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.suggested_payment_subtitle)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, p3

    check-cast v6, Landroid/widget/TextView;

    .line 68
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_description:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.suggested_payment_description)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, p3

    check-cast v7, Landroid/widget/TextView;

    .line 69
    sget p3, Lcom/swedbank/mobile/app/transfer/a$e;->suggested_payment_amount:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "findViewById(R.id.suggested_payment_amount)"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, p3

    check-cast v8, Landroid/widget/TextView;

    .line 73
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->b()Z

    move-result p3

    const/4 v0, 0x4

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    .line 74
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    invoke-virtual {v4, v0}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->setVisibility(I)V

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 79
    invoke-virtual {v4, v1}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->setVisibility(I)V

    .line 80
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->c()Ljava/lang/String;

    move-result-object p3

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-static {v4, p3, v1, v0, v2}, Lcom/swedbank/mobile/core/ui/widget/AvatarView;->a(Lcom/swedbank/mobile/core/ui/widget/AvatarView;Ljava/lang/String;ZILjava/lang/Object;)V

    .line 83
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->d()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->f()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v6, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->g()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v7, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/plugins/d/d;->h()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v8, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    new-instance p3, Lcom/swedbank/mobile/app/transfer/plugins/d/j$b;

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/app/transfer/plugins/d/j$b;-><init>(Lcom/swedbank/mobile/app/transfer/plugins/d/d;Landroid/view/View;Landroid/widget/ImageView;Lcom/swedbank/mobile/core/ui/widget/AvatarView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lio/reactivex/c/g;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/swedbank/mobile/app/transfer/plugins/d/d;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->a(Lcom/swedbank/mobile/app/transfer/plugins/d/d;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void
.end method

.method public b()I
    .locals 1

    .line 21
    sget v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->c:I

    return v0
.end method

.method public c()Z
    .locals 1

    .line 22
    sget-boolean v0, Lcom/swedbank/mobile/app/transfer/plugins/d/k;->d:Z

    return v0
.end method
