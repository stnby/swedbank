.class public final Lcom/swedbank/mobile/app/transfer/a$e;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final account_selected:I = 0x7f0a0027

.field public static final account_selection_overlay:I = 0x7f0a0028

.field public static final account_subtitle:I = 0x7f0a0029

.field public static final account_title:I = 0x7f0a002a

.field public static final action0:I = 0x7f0a002b

.field public static final action_bar:I = 0x7f0a002c

.field public static final action_bar_activity_content:I = 0x7f0a002d

.field public static final action_bar_container:I = 0x7f0a002e

.field public static final action_bar_root:I = 0x7f0a002f

.field public static final action_bar_spinner:I = 0x7f0a0030

.field public static final action_bar_subtitle:I = 0x7f0a0031

.field public static final action_bar_title:I = 0x7f0a0032

.field public static final action_container:I = 0x7f0a0033

.field public static final action_context_bar:I = 0x7f0a0034

.field public static final action_divider:I = 0x7f0a0036

.field public static final action_image:I = 0x7f0a0037

.field public static final action_menu_divider:I = 0x7f0a0038

.field public static final action_menu_presenter:I = 0x7f0a0039

.field public static final action_mode_bar:I = 0x7f0a003a

.field public static final action_mode_bar_stub:I = 0x7f0a003b

.field public static final action_mode_close_button:I = 0x7f0a003c

.field public static final action_text:I = 0x7f0a003d

.field public static final actions:I = 0x7f0a003e

.field public static final activity_chooser_view_content:I = 0x7f0a003f

.field public static final add:I = 0x7f0a0040

.field public static final adjust_height:I = 0x7f0a0041

.field public static final adjust_width:I = 0x7f0a0042

.field public static final alertTitle:I = 0x7f0a004c

.field public static final architect_node_id:I = 0x7f0a0054

.field public static final async:I = 0x7f0a0055

.field public static final auto:I = 0x7f0a0056

.field public static final automatic:I = 0x7f0a0057

.field public static final biometric_auth_embedded_icon:I = 0x7f0a005d

.field public static final biometric_auth_embedded_instruction_text:I = 0x7f0a005e

.field public static final biometric_auth_embedded_root_view:I = 0x7f0a005f

.field public static final biometric_auth_prompt_cancel:I = 0x7f0a0060

.field public static final biometric_auth_prompt_icon:I = 0x7f0a0061

.field public static final biometric_auth_prompt_identity:I = 0x7f0a0062

.field public static final biometric_auth_prompt_instruction_text:I = 0x7f0a0063

.field public static final biometric_auth_prompt_secondary_text:I = 0x7f0a0064

.field public static final biometric_auth_prompt_title:I = 0x7f0a0065

.field public static final biometric_login_method_cancel_btn:I = 0x7f0a0066

.field public static final biometric_login_method_loading:I = 0x7f0a0067

.field public static final biometric_login_method_login_btn:I = 0x7f0a0068

.field public static final biometric_login_method_login_container:I = 0x7f0a0069

.field public static final biometric_onboarding_error_handler_btn:I = 0x7f0a006a

.field public static final biometric_onboarding_error_handler_cancel_btn:I = 0x7f0a006b

.field public static final biometric_onboarding_error_handler_message:I = 0x7f0a006c

.field public static final biometric_onboarding_error_handler_title:I = 0x7f0a006d

.field public static final blocking:I = 0x7f0a0071

.field public static final bottom:I = 0x7f0a0072

.field public static final bottom_sheet_action_list:I = 0x7f0a0073

.field public static final bottom_sheet_action_title:I = 0x7f0a0074

.field public static final bottom_sheet_subtitle:I = 0x7f0a0075

.field public static final bottom_sheet_title:I = 0x7f0a0076

.field public static final browser_actions_header_text:I = 0x7f0a0077

.field public static final browser_actions_menu_item_icon:I = 0x7f0a0078

.field public static final browser_actions_menu_item_text:I = 0x7f0a0079

.field public static final browser_actions_menu_items:I = 0x7f0a007a

.field public static final browser_actions_menu_view:I = 0x7f0a007b

.field public static final buttonPanel:I = 0x7f0a007c

.field public static final cancel_action:I = 0x7f0a007d

.field public static final center:I = 0x7f0a00c3

.field public static final challenge_info:I = 0x7f0a00c7

.field public static final challenge_status:I = 0x7f0a00c8

.field public static final checkbox:I = 0x7f0a00c9

.field public static final chronometer:I = 0x7f0a00ca

.field public static final contact_app_bar_layout:I = 0x7f0a00ce

.field public static final contact_list_business:I = 0x7f0a00cf

.field public static final contact_list_private:I = 0x7f0a00d0

.field public static final contact_method_divider:I = 0x7f0a00d1

.field public static final contact_method_icon:I = 0x7f0a00d2

.field public static final contact_method_root_view:I = 0x7f0a00d3

.field public static final contact_method_subtitle:I = 0x7f0a00d4

.field public static final contact_method_title:I = 0x7f0a00d5

.field public static final contact_root_view:I = 0x7f0a00d6

.field public static final contact_tabs:I = 0x7f0a00d7

.field public static final contact_view_pager:I = 0x7f0a00d8

.field public static final container:I = 0x7f0a00d9

.field public static final content:I = 0x7f0a00da

.field public static final contentPanel:I = 0x7f0a00db

.field public static final coordinator:I = 0x7f0a00dc

.field public static final current_account:I = 0x7f0a00f8

.field public static final custom:I = 0x7f0a00f9

.field public static final customPanel:I = 0x7f0a00fa

.field public static final dark:I = 0x7f0a0104

.field public static final decor_content_parent:I = 0x7f0a0105

.field public static final default_activity_button:I = 0x7f0a0106

.field public static final design_bottom_sheet:I = 0x7f0a0107

.field public static final design_menu_item_action_area:I = 0x7f0a0108

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0109

.field public static final design_menu_item_text:I = 0x7f0a010a

.field public static final design_navigation_view:I = 0x7f0a010b

.field public static final drop_down_indicator:I = 0x7f0a0110

.field public static final edit_query:I = 0x7f0a0111

.field public static final enable_setting_title:I = 0x7f0a0112

.field public static final end:I = 0x7f0a0113

.field public static final end_padder:I = 0x7f0a0114

.field public static final error:I = 0x7f0a0117

.field public static final expand_activities_button:I = 0x7f0a0119

.field public static final expanded_menu:I = 0x7f0a011a

.field public static final external_biometric_authentication_action_btn:I = 0x7f0a011b

.field public static final external_biometric_authentication_cancel_btn:I = 0x7f0a011c

.field public static final external_biometric_authentication_control_code:I = 0x7f0a011d

.field public static final external_biometric_authentication_guideline_end:I = 0x7f0a011e

.field public static final external_biometric_authentication_guideline_start:I = 0x7f0a011f

.field public static final external_biometric_authentication_message:I = 0x7f0a0120

.field public static final external_biometric_authentication_title:I = 0x7f0a0121

.field public static final fill:I = 0x7f0a0127

.field public static final filled:I = 0x7f0a012a

.field public static final fixed:I = 0x7f0a0130

.field public static final forever:I = 0x7f0a0138

.field public static final ghost_view:I = 0x7f0a0139

.field public static final gone:I = 0x7f0a013a

.field public static final group_divider:I = 0x7f0a013b

.field public static final hardware:I = 0x7f0a013d

.field public static final home:I = 0x7f0a013e

.field public static final icon:I = 0x7f0a0145

.field public static final icon_group:I = 0x7f0a0146

.field public static final icon_only:I = 0x7f0a0147

.field public static final image:I = 0x7f0a0149

.field public static final info:I = 0x7f0a014a

.field public static final information_setting_content:I = 0x7f0a014b

.field public static final information_setting_description:I = 0x7f0a014c

.field public static final information_setting_title:I = 0x7f0a014d

.field public static final invisible:I = 0x7f0a014e

.field public static final italic:I = 0x7f0a014f

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0155

.field public static final labeled:I = 0x7f0a015b

.field public static final largeLabel:I = 0x7f0a015d

.field public static final left:I = 0x7f0a015e

.field public static final light:I = 0x7f0a015f

.field public static final line1:I = 0x7f0a0160

.field public static final line3:I = 0x7f0a0161

.field public static final listMode:I = 0x7f0a0162

.field public static final list_item:I = 0x7f0a0163

.field public static final list_item_separator_title:I = 0x7f0a0164

.field public static final login_biometric_cancel_btn:I = 0x7f0a0165

.field public static final login_biometric_loading:I = 0x7f0a0167

.field public static final login_biometric_login_btn:I = 0x7f0a0168

.field public static final lottie_layer_name:I = 0x7f0a0189

.field public static final masked:I = 0x7f0a018a

.field public static final media_actions:I = 0x7f0a018b

.field public static final message:I = 0x7f0a018e

.field public static final mini:I = 0x7f0a0190

.field public static final mtrl_child_content_container:I = 0x7f0a0197

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0198

.field public static final multiply:I = 0x7f0a0199

.field public static final navigation_bar:I = 0x7f0a019a

.field public static final navigation_header_container:I = 0x7f0a019b

.field public static final navigation_item_icon:I = 0x7f0a019c

.field public static final navigation_item_text:I = 0x7f0a019d

.field public static final navigation_root_view:I = 0x7f0a019e

.field public static final nearby_accounts_informative_views:I = 0x7f0a019f

.field public static final nearby_accounts_list:I = 0x7f0a01a0

.field public static final nearby_accounts_loading:I = 0x7f0a01a1

.field public static final nearby_accounts_loading_cancel:I = 0x7f0a01a2

.field public static final nearby_accounts_loading_views:I = 0x7f0a01a3

.field public static final nearby_accounts_search_btn:I = 0x7f0a01a4

.field public static final nearby_accounts_search_loading_text:I = 0x7f0a01a5

.field public static final nearby_accounts_search_subtitle:I = 0x7f0a01a6

.field public static final nearby_accounts_search_title:I = 0x7f0a01a7

.field public static final nearby_accounts_shared_account:I = 0x7f0a01a8

.field public static final none:I = 0x7f0a01aa

.field public static final normal:I = 0x7f0a01ab

.field public static final not_authenticated_illustration:I = 0x7f0a01ac

.field public static final not_authenticated_log_in_btn:I = 0x7f0a01ad

.field public static final not_authenticated_message:I = 0x7f0a01ae

.field public static final not_authenticated_title:I = 0x7f0a01af

.field public static final not_authenticated_view_root:I = 0x7f0a01b0

.field public static final notification_background:I = 0x7f0a01b1

.field public static final notification_main_column:I = 0x7f0a01b2

.field public static final notification_main_column_container:I = 0x7f0a01b3

.field public static final off:I = 0x7f0a01b4

.field public static final on:I = 0x7f0a01b5

.field public static final on_off_setting_state:I = 0x7f0a01b6

.field public static final on_off_setting_subtitle:I = 0x7f0a01b7

.field public static final on_off_setting_title:I = 0x7f0a01b8

.field public static final onboarding_biometric_switch:I = 0x7f0a01b9

.field public static final onboarding_cancel_btn:I = 0x7f0a01ba

.field public static final onboarding_content_views:I = 0x7f0a01bb

.field public static final onboarding_done_btn:I = 0x7f0a01bc

.field public static final onboarding_error_handler_cancel_btn:I = 0x7f0a01bd

.field public static final onboarding_error_handler_message:I = 0x7f0a01be

.field public static final onboarding_error_handler_retry_btn:I = 0x7f0a01bf

.field public static final onboarding_error_handler_title:I = 0x7f0a01c0

.field public static final onboarding_loading:I = 0x7f0a01c7

.field public static final onboarding_loading_views:I = 0x7f0a01c8

.field public static final onboarding_plugin_container:I = 0x7f0a01c9

.field public static final onboarding_root_view:I = 0x7f0a01cb

.field public static final onboarding_text:I = 0x7f0a01cc

.field public static final onboarding_title:I = 0x7f0a01cd

.field public static final outline:I = 0x7f0a01d8

.field public static final packed:I = 0x7f0a0204

.field public static final parallax:I = 0x7f0a0205

.field public static final parent:I = 0x7f0a0206

.field public static final parentPanel:I = 0x7f0a0207

.field public static final parent_matrix:I = 0x7f0a0208

.field public static final payment_form_amount:I = 0x7f0a0209

.field public static final payment_form_amount_error:I = 0x7f0a020a

.field public static final payment_form_amount_loading:I = 0x7f0a020b

.field public static final payment_form_available_funds:I = 0x7f0a020c

.field public static final payment_form_available_funds_loading:I = 0x7f0a020d

.field public static final payment_form_content_container:I = 0x7f0a020e

.field public static final payment_form_credit_service_info_message:I = 0x7f0a020f

.field public static final payment_form_currency:I = 0x7f0a0210

.field public static final payment_form_currency_barrier:I = 0x7f0a0211

.field public static final payment_form_currency_selector:I = 0x7f0a0212

.field public static final payment_form_description:I = 0x7f0a0213

.field public static final payment_form_description_layout:I = 0x7f0a0214

.field public static final payment_form_description_loading:I = 0x7f0a0215

.field public static final payment_form_extra_fields_group:I = 0x7f0a0216

.field public static final payment_form_extra_fields_toggle:I = 0x7f0a0217

.field public static final payment_form_guideline_end:I = 0x7f0a0218

.field public static final payment_form_guideline_start:I = 0x7f0a0219

.field public static final payment_form_pay_btn:I = 0x7f0a021a

.field public static final payment_form_payment_date:I = 0x7f0a021b

.field public static final payment_form_payment_date_layout:I = 0x7f0a021c

.field public static final payment_form_payment_execution_message:I = 0x7f0a021d

.field public static final payment_form_payment_info_message:I = 0x7f0a021e

.field public static final payment_form_recipient_bank:I = 0x7f0a021f

.field public static final payment_form_recipient_iban:I = 0x7f0a0220

.field public static final payment_form_recipient_iban_error:I = 0x7f0a0221

.field public static final payment_form_recipient_iban_layout:I = 0x7f0a0222

.field public static final payment_form_recipient_iban_loading:I = 0x7f0a0223

.field public static final payment_form_recipient_name:I = 0x7f0a0224

.field public static final payment_form_recipient_name_extra:I = 0x7f0a0225

.field public static final payment_form_recipient_name_layout:I = 0x7f0a0226

.field public static final payment_form_recipient_name_loading:I = 0x7f0a0227

.field public static final payment_form_reference_number:I = 0x7f0a0228

.field public static final payment_form_reference_number_layout:I = 0x7f0a0229

.field public static final payment_form_reference_number_loading:I = 0x7f0a022a

.field public static final payment_form_scroll_container:I = 0x7f0a022b

.field public static final payment_form_service_fee:I = 0x7f0a022c

.field public static final payment_form_view_root:I = 0x7f0a022d

.field public static final payment_request_account:I = 0x7f0a022e

.field public static final payment_request_account_layout:I = 0x7f0a022f

.field public static final payment_request_account_root:I = 0x7f0a0230

.field public static final payment_request_account_spinner:I = 0x7f0a0231

.field public static final payment_request_action_btn:I = 0x7f0a0232

.field public static final payment_request_amount:I = 0x7f0a0233

.field public static final payment_request_close_btn:I = 0x7f0a0234

.field public static final payment_request_currency:I = 0x7f0a0235

.field public static final payment_request_currency_caret:I = 0x7f0a0236

.field public static final payment_request_description:I = 0x7f0a0237

.field public static final payment_request_description_layout:I = 0x7f0a0238

.field public static final payment_request_guideline_end:I = 0x7f0a0239

.field public static final payment_request_guideline_start:I = 0x7f0a023a

.field public static final payment_request_opening_error_background:I = 0x7f0a023b

.field public static final payment_request_opening_error_btn:I = 0x7f0a023c

.field public static final payment_request_opening_error_group:I = 0x7f0a023d

.field public static final payment_request_opening_error_title:I = 0x7f0a023e

.field public static final payment_request_opening_loading_view:I = 0x7f0a023f

.field public static final payment_request_success_image:I = 0x7f0a0240

.field public static final payment_request_view_root:I = 0x7f0a0241

.field public static final percent:I = 0x7f0a0242

.field public static final pin:I = 0x7f0a0243

.field public static final pin_challenge_code:I = 0x7f0a0254

.field public static final pin_challenge_entry_input:I = 0x7f0a0255

.field public static final pin_challenge_entry_layout:I = 0x7f0a0256

.field public static final pin_challenge_sign:I = 0x7f0a0257

.field public static final predefined_payment_amount:I = 0x7f0a025b

.field public static final predefined_payment_icon:I = 0x7f0a025c

.field public static final predefined_payment_item_root:I = 0x7f0a025d

.field public static final predefined_payment_subtitle:I = 0x7f0a025e

.field public static final predefined_payment_title:I = 0x7f0a025f

.field public static final progress_circular:I = 0x7f0a0261

.field public static final progress_horizontal:I = 0x7f0a0262

.field public static final radio:I = 0x7f0a0264

.field public static final recurring_login_alternative_login_btn:I = 0x7f0a0265

.field public static final recurring_login_plugin_barrier:I = 0x7f0a0266

.field public static final recurring_login_plugin_min_bottom_guideline:I = 0x7f0a0267

.field public static final recurring_login_plugin_root_view:I = 0x7f0a0268

.field public static final recurring_login_view_root:I = 0x7f0a0269

.field public static final recurring_login_welcome_title:I = 0x7f0a026a

.field public static final restart:I = 0x7f0a026b

.field public static final retry_btn:I = 0x7f0a026c

.field public static final retry_title:I = 0x7f0a026d

.field public static final reverse:I = 0x7f0a026e

.field public static final right:I = 0x7f0a026f

.field public static final right_icon:I = 0x7f0a0270

.field public static final right_side:I = 0x7f0a0271

.field public static final root_layout:I = 0x7f0a0272

.field public static final save_image_matrix:I = 0x7f0a0273

.field public static final save_non_transition_alpha:I = 0x7f0a0274

.field public static final save_scale_type:I = 0x7f0a0275

.field public static final screen:I = 0x7f0a0276

.field public static final scrollIndicatorDown:I = 0x7f0a0278

.field public static final scrollIndicatorUp:I = 0x7f0a0279

.field public static final scrollView:I = 0x7f0a027a

.field public static final scrollable:I = 0x7f0a027b

.field public static final search_badge:I = 0x7f0a027c

.field public static final search_bar:I = 0x7f0a027d

.field public static final search_button:I = 0x7f0a027e

.field public static final search_close_btn:I = 0x7f0a027f

.field public static final search_edit_frame:I = 0x7f0a0280

.field public static final search_go_btn:I = 0x7f0a0281

.field public static final search_mag_icon:I = 0x7f0a0282

.field public static final search_plate:I = 0x7f0a0283

.field public static final search_result_avatar:I = 0x7f0a0284

.field public static final search_result_icon:I = 0x7f0a0285

.field public static final search_result_item_root:I = 0x7f0a0286

.field public static final search_result_large_title:I = 0x7f0a0287

.field public static final search_result_subtitle:I = 0x7f0a0288

.field public static final search_result_title:I = 0x7f0a0289

.field public static final search_src_text:I = 0x7f0a028a

.field public static final search_voice_btn:I = 0x7f0a028b

.field public static final select_dialog_listview:I = 0x7f0a028d

.field public static final selected:I = 0x7f0a028e

.field public static final shortcut:I = 0x7f0a0298

.field public static final slide_to_confirm_circle_bg:I = 0x7f0a029c

.field public static final slide_to_confirm_confirm_button_image:I = 0x7f0a029d

.field public static final slide_to_confirm_end_bg:I = 0x7f0a029e

.field public static final slide_to_confirm_indicator:I = 0x7f0a029f

.field public static final slide_to_confirm_inner_button:I = 0x7f0a02a0

.field public static final slide_to_confirm_main_bg:I = 0x7f0a02a1

.field public static final slide_to_confirm_root:I = 0x7f0a02a2

.field public static final slide_to_confirm_text:I = 0x7f0a02a3

.field public static final smallLabel:I = 0x7f0a02a4

.field public static final snackbar_action:I = 0x7f0a02a5

.field public static final snackbar_action_btn:I = 0x7f0a02a6

.field public static final snackbar_bottom_barrier:I = 0x7f0a02a7

.field public static final snackbar_bottom_guideline:I = 0x7f0a02a8

.field public static final snackbar_buttons_barrier:I = 0x7f0a02a9

.field public static final snackbar_close_btn:I = 0x7f0a02aa

.field public static final snackbar_subtitle:I = 0x7f0a02ab

.field public static final snackbar_text:I = 0x7f0a02ac

.field public static final snackbar_title:I = 0x7f0a02ad

.field public static final snackbar_top_guideline:I = 0x7f0a02ae

.field public static final software:I = 0x7f0a02b1

.field public static final spacer:I = 0x7f0a02b2

.field public static final split_action_bar:I = 0x7f0a02b3

.field public static final spread:I = 0x7f0a02b4

.field public static final spread_inside:I = 0x7f0a02b5

.field public static final src_atop:I = 0x7f0a02b6

.field public static final src_in:I = 0x7f0a02b7

.field public static final src_over:I = 0x7f0a02b8

.field public static final standard:I = 0x7f0a02b9

.field public static final start:I = 0x7f0a02ba

.field public static final status_bar_latest_event_content:I = 0x7f0a02bb

.field public static final stretch:I = 0x7f0a02bc

.field public static final submenuarrow:I = 0x7f0a02bd

.field public static final submit_area:I = 0x7f0a02be

.field public static final suggested_payment_amount:I = 0x7f0a02bf

.field public static final suggested_payment_avatar:I = 0x7f0a02c0

.field public static final suggested_payment_description:I = 0x7f0a02c1

.field public static final suggested_payment_icon:I = 0x7f0a02c2

.field public static final suggested_payment_item_root:I = 0x7f0a02c3

.field public static final suggested_payment_subtitle:I = 0x7f0a02c4

.field public static final suggested_payment_title:I = 0x7f0a02c5

.field public static final tabMode:I = 0x7f0a02c6

.field public static final tag_transition_group:I = 0x7f0a02cd

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a02ce

.field public static final tag_unhandled_key_listeners:I = 0x7f0a02cf

.field public static final text:I = 0x7f0a02d0

.field public static final text2:I = 0x7f0a02d1

.field public static final textSpacerNoButtons:I = 0x7f0a02d2

.field public static final textSpacerNoTitle:I = 0x7f0a02d3

.field public static final text_input_password_toggle:I = 0x7f0a02d5

.field public static final textinput_counter:I = 0x7f0a02d6

.field public static final textinput_error:I = 0x7f0a02d7

.field public static final textinput_helper_text:I = 0x7f0a02d8

.field public static final time:I = 0x7f0a02d9

.field public static final title:I = 0x7f0a02da

.field public static final titleDividerNoCustom:I = 0x7f0a02db

.field public static final title_template:I = 0x7f0a02dc

.field public static final toolbar:I = 0x7f0a02dd

.field public static final top:I = 0x7f0a02de

.field public static final topPanel:I = 0x7f0a02df

.field public static final touch_outside:I = 0x7f0a02e0

.field public static final transfer_detailed_list:I = 0x7f0a02f5

.field public static final transfer_detailed_refresh:I = 0x7f0a02f6

.field public static final transfer_detailed_root:I = 0x7f0a02f7

.field public static final transfer_overview_account_avatar:I = 0x7f0a02f8

.field public static final transfer_overview_account_root:I = 0x7f0a02f9

.field public static final transfer_overview_account_subtitle:I = 0x7f0a02fa

.field public static final transfer_overview_account_title:I = 0x7f0a02fb

.field public static final transfer_overview_app_bar_layout:I = 0x7f0a02fc

.field public static final transfer_overview_error_group:I = 0x7f0a02fd

.field public static final transfer_overview_loading:I = 0x7f0a02fe

.field public static final transfer_overview_refresh:I = 0x7f0a02ff

.field public static final transfer_overview_retry_btn:I = 0x7f0a0300

.field public static final transfer_overview_retry_title:I = 0x7f0a0301

.field public static final transfer_overview_start_payment_btn_loading:I = 0x7f0a0302

.field public static final transfer_overview_start_payment_search_btn:I = 0x7f0a0303

.field public static final transfer_payment_request_btn:I = 0x7f0a0304

.field public static final transfer_plugin_list:I = 0x7f0a0305

.field public static final transfer_preview_child_container:I = 0x7f0a0306

.field public static final transfer_preview_see_all:I = 0x7f0a0307

.field public static final transfer_preview_title:I = 0x7f0a0308

.field public static final transfer_root:I = 0x7f0a0309

.field public static final transfer_search_clear_btn:I = 0x7f0a030a

.field public static final transfer_search_input:I = 0x7f0a030b

.field public static final transfer_search_input_loading:I = 0x7f0a030c

.field public static final transfer_search_results:I = 0x7f0a030d

.field public static final transfer_search_view_root:I = 0x7f0a030e

.field public static final transfer_service_icon:I = 0x7f0a030f

.field public static final transfer_service_item_root:I = 0x7f0a0310

.field public static final transfer_service_title:I = 0x7f0a0311

.field public static final transition_current_scene:I = 0x7f0a0312

.field public static final transition_layout_save:I = 0x7f0a0313

.field public static final transition_position:I = 0x7f0a0314

.field public static final transition_scene_layoutid_cache:I = 0x7f0a0315

.field public static final transition_transform:I = 0x7f0a0316

.field public static final uniform:I = 0x7f0a0317

.field public static final unlabeled:I = 0x7f0a0318

.field public static final up:I = 0x7f0a0319

.field public static final view_offset_helper:I = 0x7f0a031d

.field public static final visible:I = 0x7f0a031f

.field public static final wide:I = 0x7f0a0362

.field public static final wrap:I = 0x7f0a0382

.field public static final wrap_content:I = 0x7f0a0383
