.class public final Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "TransferOverviewLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Landroid/graphics/Paint;

.field final synthetic f:F

.field final synthetic g:F

.field final synthetic h:Landroid/graphics/Paint;

.field final synthetic i:Lcom/swedbank/mobile/core/ui/m;

.field final synthetic j:F

.field final synthetic k:F

.field final synthetic l:F

.field final synthetic m:F

.field final synthetic n:F

.field final synthetic o:F

.field final synthetic p:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1;FFFLandroid/graphics/Paint;FFLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFFILandroid/view/View;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFF",
            "Landroid/graphics/Paint;",
            "FF",
            "Landroid/graphics/Paint;",
            "Lcom/swedbank/mobile/core/ui/m;",
            "FFFFFFI",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    .line 57
    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->a:Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1;

    move v1, p2

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    move v1, p3

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->c:F

    move v1, p4

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    move-object v1, p5

    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->e:Landroid/graphics/Paint;

    move v1, p6

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->f:F

    move v1, p7

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->g:F

    move-object v1, p8

    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->h:Landroid/graphics/Paint;

    move-object v1, p9

    iput-object v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    move/from16 v1, p10

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->j:F

    move/from16 v1, p11

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->k:F

    move/from16 v1, p12

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->l:F

    move/from16 v1, p13

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    move/from16 v1, p14

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->n:F

    move/from16 v1, p15

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->o:F

    move/from16 v1, p16

    iput v1, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->p:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x1c

    const/4 v5, 0x0

    move-object p1, p0

    move-object/from16 p2, p17

    move/from16 p3, p18

    move p4, v1

    move p5, v2

    move p6, v3

    move p7, v4

    move-object p8, v5

    invoke-direct/range {p1 .. p8}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;)V
    .locals 16
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    const-string v3, "canvas"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v3, v1

    .line 60
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 65
    iget v5, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    .line 66
    iget v6, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->c:F

    .line 68
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    .line 71
    iget-object v8, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->e:Landroid/graphics/Paint;

    .line 72
    iget v9, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->f:F

    .line 73
    iget v10, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->g:F

    .line 74
    iget-object v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->h:Landroid/graphics/Paint;

    .line 75
    iget-object v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    add-float/2addr v5, v6

    .line 183
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v6

    add-float/2addr v6, v5

    add-float v13, v6, v7

    const/4 v14, 0x0

    .line 184
    invoke-virtual {v4, v14, v6, v3, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 185
    invoke-virtual {v2, v4, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v13, 0x0

    :goto_0
    const/4 v15, 0x1

    if-ge v13, v15, :cond_0

    .line 188
    iget v15, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v15, v10

    add-float v6, v15, v7

    .line 189
    invoke-virtual {v4, v14, v15, v3, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 190
    invoke-virtual {v2, v4, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_0
    float-to-int v6, v5

    const/4 v11, 0x0

    .line 197
    invoke-virtual {v12, v11, v6, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->a(IIILandroid/graphics/Canvas;)V

    .line 204
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v6

    add-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v15, :cond_1

    .line 206
    iget v6, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v6, v7

    sub-float v11, v3, v9

    add-float v13, v6, v10

    .line 207
    invoke-virtual {v4, v9, v6, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 208
    invoke-virtual {v2, v4, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 212
    :cond_1
    iget v5, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v5, v7

    float-to-int v6, v5

    const/4 v7, 0x0

    .line 213
    invoke-virtual {v12, v7, v6, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->b(IIILandroid/graphics/Canvas;)V

    .line 219
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->b()F

    move-result v6

    add-float/2addr v5, v6

    .line 80
    iget v6, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    add-float/2addr v6, v5

    iget v5, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->j:F

    add-float/2addr v6, v5

    .line 81
    iget v5, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->c:F

    .line 83
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    .line 86
    iget-object v8, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->e:Landroid/graphics/Paint;

    .line 87
    iget v9, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->f:F

    .line 88
    iget v10, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->g:F

    .line 89
    iget-object v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->h:Landroid/graphics/Paint;

    .line 90
    iget-object v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    add-float/2addr v6, v5

    .line 220
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v5

    add-float/2addr v5, v6

    add-float v13, v5, v7

    .line 221
    invoke-virtual {v4, v14, v5, v3, v13}, Landroid/graphics/RectF;->set(FFFF)V

    .line 222
    invoke-virtual {v2, v4, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v5, 0x2

    const/4 v13, 0x0

    :goto_2
    if-ge v13, v5, :cond_2

    .line 225
    iget v15, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v15, v10

    add-float v5, v15, v7

    .line 226
    invoke-virtual {v4, v14, v15, v3, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 227
    invoke-virtual {v2, v4, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v13, v13, 0x1

    const/4 v5, 0x2

    goto :goto_2

    :cond_2
    float-to-int v5, v6

    const/4 v11, 0x0

    .line 234
    invoke-virtual {v12, v11, v5, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->a(IIILandroid/graphics/Canvas;)V

    .line 241
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v5

    add-float/2addr v6, v5

    iput v6, v4, Landroid/graphics/RectF;->bottom:F

    const/4 v5, 0x0

    const/4 v6, 0x2

    :goto_3
    if-ge v5, v6, :cond_3

    .line 243
    iget v11, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v11, v7

    sub-float v13, v3, v9

    add-float v14, v11, v10

    .line 244
    invoke-virtual {v4, v9, v11, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 245
    invoke-virtual {v2, v4, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 249
    :cond_3
    iget v3, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v7

    float-to-int v3, v3

    const/4 v4, 0x0

    .line 250
    invoke-virtual {v12, v4, v3, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->b(IIILandroid/graphics/Canvas;)V

    .line 256
    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->b()F

    return-void
.end method

.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 17
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    const-string v3, "canvas"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "itemPaint"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v3, p1

    int-to-float v3, v3

    const v4, 0x3e9eb852    # 0.31f

    mul-float v4, v4, v3

    const v5, 0x3e4ccccd    # 0.2f

    mul-float v5, v5, v3

    const v6, 0x3e19999a    # 0.15f

    mul-float v6, v6, v3

    const v7, 0x3e6147ae    # 0.22f

    mul-float v7, v7, v3

    const v8, 0x3e2e147b    # 0.17f

    mul-float v8, v8, v3

    .line 100
    iget v9, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->k:F

    iget v10, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    add-float/2addr v9, v10

    .line 101
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 103
    iget v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->c:F

    add-float/2addr v11, v12

    iget-object v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->i:Lcom/swedbank/mobile/core/ui/m;

    invoke-virtual {v12}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v12

    add-float/2addr v11, v12

    move v13, v11

    const/4 v11, 0x0

    :goto_0
    const/4 v14, 0x2

    const/high16 v15, 0x40000000    # 2.0f

    if-ge v11, v14, :cond_0

    .line 107
    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    div-float/2addr v14, v15

    add-float/2addr v14, v13

    .line 108
    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->l:F

    add-float/2addr v12, v9

    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->l:F

    invoke-virtual {v1, v12, v14, v15, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 111
    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    sub-float/2addr v12, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v12, v14

    add-float/2addr v12, v13

    .line 112
    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    mul-float v15, v15, v14

    add-float/2addr v15, v9

    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->n:F

    add-float/2addr v15, v14

    add-float v14, v15, v4

    move/from16 v16, v7

    .line 113
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v7, v12

    invoke-virtual {v10, v15, v12, v14, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 114
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 116
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->g:F

    add-float/2addr v7, v12

    add-float/2addr v13, v7

    add-int/lit8 v11, v11, 0x1

    move/from16 v7, v16

    goto :goto_0

    :cond_0
    move/from16 v16, v7

    .line 121
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    int-to-float v11, v14

    mul-float v7, v7, v11

    add-float/2addr v7, v13

    add-float v12, v9, v5

    .line 123
    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v14, v7

    invoke-virtual {v10, v9, v7, v12, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 124
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 127
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    mul-float v7, v7, v11

    add-float/2addr v7, v13

    sub-float/2addr v3, v9

    sub-float v11, v3, v8

    add-float/2addr v8, v11

    .line 129
    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v12, v7

    invoke-virtual {v10, v11, v7, v8, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 130
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 133
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    iget v8, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->j:F

    add-float/2addr v7, v8

    add-float/2addr v13, v7

    const/4 v7, 0x3

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v7, :cond_1

    .line 136
    iget v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    add-float/2addr v11, v13

    .line 137
    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->l:F

    add-float/2addr v14, v9

    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->l:F

    invoke-virtual {v1, v14, v11, v15, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 140
    iget v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    mul-float v14, v14, v12

    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->o:F

    add-float/2addr v14, v15

    sub-float/2addr v11, v14

    div-float/2addr v11, v12

    add-float/2addr v11, v13

    .line 141
    iget v14, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->b:F

    mul-float v14, v14, v12

    add-float/2addr v14, v9

    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->n:F

    add-float/2addr v14, v12

    add-float v12, v14, v4

    .line 142
    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v15, v11

    invoke-virtual {v10, v14, v11, v12, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 143
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 146
    iget v11, v10, Landroid/graphics/RectF;->bottom:F

    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->o:F

    add-float/2addr v11, v12

    .line 147
    iget v12, v10, Landroid/graphics/RectF;->left:F

    iget v14, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v14, v5

    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v15, v11

    invoke-virtual {v10, v12, v11, v14, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 148
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 151
    iget v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    iget v12, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    const/high16 v14, 0x40000000    # 2.0f

    mul-float v12, v12, v14

    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->o:F

    add-float/2addr v12, v15

    sub-float/2addr v11, v12

    div-float/2addr v11, v14

    add-float/2addr v11, v13

    sub-float v12, v3, v6

    add-float v15, v12, v6

    .line 153
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v7, v11

    invoke-virtual {v10, v12, v11, v15, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 154
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 157
    iget v7, v10, Landroid/graphics/RectF;->bottom:F

    iget v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->o:F

    add-float/2addr v7, v11

    sub-float v11, v3, v16

    add-float v12, v11, v16

    .line 159
    iget v15, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->m:F

    add-float/2addr v15, v7

    invoke-virtual {v10, v11, v7, v12, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 160
    invoke-virtual {v1, v10, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 162
    iget v7, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->d:F

    iget v11, v0, Lcom/swedbank/mobile/app/transfer/loading/TransferOverviewLoadingView$1$1;->g:F

    add-float/2addr v7, v11

    add-float/2addr v13, v7

    add-int/lit8 v8, v8, 0x1

    const/4 v7, 0x3

    goto :goto_1

    :cond_1
    return-void
.end method
