.class public final Lcom/swedbank/mobile/app/transfer/p;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "TransferViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/o;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private m:F

.field private final n:Lcom/swedbank/mobile/business/navigation/i;

.field private final o:Z

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0xa

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "appBar"

    const-string v4, "getAppBar()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenLoadingView"

    const-string v4, "getFullScreenLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "transferSearchBtn"

    const-string v4, "getTransferSearchBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "transferSearchBtnLoading"

    const-string v4, "getTransferSearchBtnLoading()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "paymentRequestBtn"

    const-string v4, "getPaymentRequestBtn()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/p;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "transferList"

    const-string v4, "getTransferList()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/navigation/i;ZLjava/util/List;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/navigation/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_payment_request_enabled"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/navigation/i;",
            "Z",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "navigationItemReselectStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProviders"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->n:Lcom/swedbank/mobile/business/navigation/i;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/p;->o:Z

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/p;->p:Ljava/util/List;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_app_bar_layout:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->c:Lkotlin/f/c;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_refresh:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->d:Lkotlin/f/c;

    .line 43
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->e:Lkotlin/f/c;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_retry_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->f:Lkotlin/f/c;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->g:Lkotlin/f/c;

    .line 46
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->h:Lkotlin/f/c;

    .line 47
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_start_payment_search_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->i:Lkotlin/f/c;

    .line 48
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_overview_start_payment_btn_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->j:Lkotlin/f/c;

    .line 49
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_payment_request_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->k:Lkotlin/f/c;

    .line 50
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_plugin_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->l:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/p;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->u()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->r()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/p;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->i()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->s()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->j()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;
    .locals 0

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/p;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->q()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final h()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/TextView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->k()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->p()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final i()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final j()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->h()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/app/transfer/p;)F
    .locals 0

    .line 36
    iget p0, p0, Lcom/swedbank/mobile/app/transfer/p;->m:F

    return p0
.end method

.method private final k()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->t()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final p()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final q()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final r()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final s()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final t()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final u()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->l:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/p;->a:[Lkotlin/h/g;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 69
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->r()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 215
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/s;)V
    .locals 22
    .param p1    # Lcom/swedbank/mobile/app/transfer/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const-string v1, "viewState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->e()Z

    move-result v1

    .line 83
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->b()Z

    move-result v3

    .line 84
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->d()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v4

    .line 218
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->b(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const/4 v6, 0x4

    const/4 v7, 0x0

    if-eqz v3, :cond_0

    const/4 v8, 0x4

    goto :goto_0

    :cond_0
    const/4 v8, 0x0

    .line 219
    :goto_0
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    xor-int/lit8 v5, v3, 0x1

    .line 222
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g()Z

    move-result v8

    const/16 v9, 0x8

    if-eqz v8, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->l(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v8

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    const/16 v5, 0x8

    .line 223
    :goto_1
    invoke-virtual {v8, v5}, Landroid/view/View;->setVisibility(I)V

    .line 226
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->c(Lcom/swedbank/mobile/app/transfer/p;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const/4 v8, 0x1

    if-eqz v1, :cond_4

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_5

    const/4 v1, 0x4

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    .line 227
    :goto_4
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 229
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->a(Lcom/swedbank/mobile/app/transfer/p;)Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 86
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->e()Z

    move-result v1

    .line 87
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->b()Z

    move-result v3

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->a()Z

    move-result v4

    .line 237
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->d(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v5

    if-eqz v3, :cond_6

    const/4 v10, 0x0

    goto :goto_5

    :cond_6
    const/16 v10, 0x8

    .line 238
    :goto_5
    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    if-eqz v4, :cond_8

    if-eqz v1, :cond_7

    if-eqz v3, :cond_8

    :cond_7
    const/4 v1, 0x1

    goto :goto_6

    :cond_8
    const/4 v1, 0x0

    .line 241
    :goto_6
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->e(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v3

    if-eqz v1, :cond_9

    const/4 v5, 0x0

    goto :goto_7

    :cond_9
    const/16 v5, 0x8

    .line 242
    :goto_7
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 244
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->c(Lcom/swedbank/mobile/app/transfer/p;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v3

    if-eqz v1, :cond_a

    .line 247
    invoke-virtual {v3, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 248
    invoke-virtual {v3, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_8

    :cond_a
    if-eqz v4, :cond_b

    .line 251
    invoke-virtual {v3, v8}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 252
    invoke-virtual {v3, v8}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_8

    .line 255
    :cond_b
    invoke-virtual {v3, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 256
    invoke-virtual {v3, v8}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 259
    :goto_8
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->c()Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->e()Z

    move-result v3

    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v4

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->g()Lcom/swedbank/mobile/app/w/b;

    move-result-object v5

    const/4 v10, 0x0

    if-eqz v1, :cond_c

    .line 270
    check-cast v1, Ljava/lang/CharSequence;

    .line 271
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$c;

    invoke-direct {v3, v10, v8, v10}, Lcom/swedbank/mobile/core/ui/widget/s$c$c;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 269
    invoke-virtual {v0, v1, v3}, Lcom/swedbank/mobile/app/transfer/p;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto/16 :goto_a

    :cond_c
    if-eqz v3, :cond_d

    if-eqz v5, :cond_d

    .line 273
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v5}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "context().getString(fata\u2026ror.userDisplayedMessage)"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 274
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_overview_retry_btn:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 272
    invoke-virtual {v0, v1, v3}, Lcom/swedbank/mobile/app/transfer/p;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto/16 :goto_a

    :cond_d
    if-eqz v3, :cond_11

    if-eqz v4, :cond_11

    .line 276
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v1

    .line 277
    move-object v3, v10

    check-cast v3, Ljava/lang/Integer;

    .line 280
    instance-of v3, v4, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v3, :cond_f

    check-cast v4, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 282
    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v4, v8

    if-eqz v4, :cond_e

    move-object v11, v3

    check-cast v11, Ljava/lang/Iterable;

    const-string v1, "\n"

    move-object v12, v1

    check-cast v12, Ljava/lang/CharSequence;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x3e

    const/16 v19, 0x0

    invoke-static/range {v11 .. v19}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_9

    .line 283
    :cond_e
    sget v3, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_9

    .line 285
    :cond_f
    instance-of v3, v4, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v3, :cond_10

    check-cast v4, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    .line 286
    invoke-static {v3}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_9
    const-string v3, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 287
    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 289
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_overview_retry_btn:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 275
    invoke-virtual {v0, v1, v3}, Lcom/swedbank/mobile/app/transfer/p;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_a

    .line 286
    :cond_10
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 290
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 95
    :goto_a
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->a()Z

    move-result v1

    .line 96
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->e()Z

    move-result v3

    .line 97
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->b()Z

    move-result v4

    .line 98
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    .line 99
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/s;->g()Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    .line 301
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const/4 v12, 0x0

    if-nez v1, :cond_14

    if-eqz v4, :cond_14

    .line 304
    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_overview_no_accounts:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 305
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_overview_retry_btn:I

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 310
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v3

    .line 311
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_12

    const/4 v3, 0x1

    goto :goto_b

    :cond_12
    const/4 v3, 0x0

    .line 313
    :goto_b
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->b(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 314
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 317
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g()Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->l(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v4

    .line 318
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 321
    :cond_13
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->c(Lcom/swedbank/mobile/app/transfer/p;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setVisibility(I)V

    .line 322
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 323
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->h(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->i(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    if-eq v3, v8, :cond_1f

    .line 329
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->j(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/View;->setElevation(F)V

    goto/16 :goto_f

    :cond_14
    if-nez v3, :cond_17

    if-eqz v2, :cond_17

    .line 336
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 337
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_overview_retry_btn:I

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 350
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v3

    .line 351
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_15

    const/4 v3, 0x1

    goto :goto_c

    :cond_15
    const/4 v3, 0x0

    .line 353
    :goto_c
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->b(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 354
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 357
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->l(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v4

    .line 358
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 361
    :cond_16
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->c(Lcom/swedbank/mobile/app/transfer/p;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setVisibility(I)V

    .line 362
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 363
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->h(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->i(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    if-eq v3, v8, :cond_1f

    .line 369
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->j(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/View;->setElevation(F)V

    goto/16 :goto_f

    :cond_17
    if-nez v3, :cond_1d

    if-eqz v5, :cond_1d

    .line 376
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->f(Lcom/swedbank/mobile/app/transfer/p;)Landroid/content/Context;

    move-result-object v1

    .line 377
    check-cast v10, Ljava/lang/Integer;

    .line 380
    instance-of v2, v5, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_19

    check-cast v5, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 382
    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v8

    if-eqz v3, :cond_18

    move-object v13, v2

    check-cast v13, Ljava/lang/Iterable;

    const-string v1, "\n"

    move-object v14, v1

    check-cast v14, Ljava/lang/CharSequence;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3e

    const/16 v21, 0x0

    invoke-static/range {v13 .. v21}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_d

    .line 383
    :cond_18
    sget v2, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_d

    .line 385
    :cond_19
    instance-of v2, v5, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_1c

    check-cast v5, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    .line 386
    invoke-static {v2}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_d
    const-string v2, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 387
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 389
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->transfer_overview_retry_btn:I

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 399
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v3

    .line 400
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1a

    const/4 v3, 0x1

    goto :goto_e

    :cond_1a
    const/4 v3, 0x0

    .line 402
    :goto_e
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->b(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 403
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g()Z

    move-result v4

    if-eqz v4, :cond_1b

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->l(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v4

    .line 407
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 410
    :cond_1b
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->c(Lcom/swedbank/mobile/app/transfer/p;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setVisibility(I)V

    .line 411
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 412
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->h(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->i(Lcom/swedbank/mobile/app/transfer/p;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    if-eq v3, v8, :cond_1f

    .line 418
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->j(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/View;->setElevation(F)V

    goto :goto_f

    .line 386
    :cond_1c
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 430
    :cond_1d
    check-cast v10, Ljava/lang/CharSequence;

    .line 433
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v1

    .line 434
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1e

    const/4 v7, 0x1

    .line 449
    :cond_1e
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->g(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    if-eqz v7, :cond_1f

    .line 452
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->j(Lcom/swedbank/mobile/app/transfer/p;)Landroid/view/View;

    move-result-object v1

    .line 454
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/p;->k(Lcom/swedbank/mobile/app/transfer/p;)F

    move-result v2

    .line 452
    invoke-virtual {v1, v2}, Landroid/view/View;->setElevation(F)V

    .line 100
    :cond_1f
    :goto_f
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/p;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/swedbank/mobile/app/transfer/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/p;->a(Lcom/swedbank/mobile/app/transfer/s;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 72
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->i()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 73
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->p()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 216
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 74
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/p;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 71
    invoke-static {v0, v2, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 76
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->u()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 8

    .line 210
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 211
    new-instance v1, Lcom/swedbank/mobile/app/transfer/p$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/p$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/transfer/q;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/q;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 212
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/p;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 57
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->i()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x2

    .line 58
    new-array v1, v1, [I

    sget v2, Lcom/swedbank/mobile/app/transfer/a$b;->brand_orange:I

    const/4 v3, 0x0

    aput v2, v1, v3

    sget v2, Lcom/swedbank/mobile/app/transfer/a$b;->brand_turqoise:I

    const/4 v4, 0x1

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 59
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewStartOffset()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f8ccccd    # 1.1f

    mul-float v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v2

    int-to-float v2, v2

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float v2, v2, v4

    float-to-int v2, v2

    invoke-virtual {v0, v3, v1, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZII)V

    .line 61
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/p;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$c;->toolbar_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/swedbank/mobile/app/transfer/p;->m:F

    .line 62
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->u()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/p;->p:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Ljava/util/List;)V

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/p;->n:Lcom/swedbank/mobile/business/navigation/i;

    const-string v1, "feature_transfer"

    .line 64
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/business/navigation/i;->a(Ljava/lang/String;)Lio/reactivex/o;

    move-result-object v2

    .line 65
    new-instance v0, Lcom/swedbank/mobile/app/transfer/p$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/p$b;-><init>(Lcom/swedbank/mobile/app/transfer/p;)V

    move-object v5, v0

    check-cast v5, Lkotlin/e/a/b;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 213
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void
.end method

.method public f()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/p;->t()Landroid/view/View;

    move-result-object v0

    .line 217
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public final g()Z
    .locals 1

    .line 38
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/p;->o:Z

    return v0
.end method
