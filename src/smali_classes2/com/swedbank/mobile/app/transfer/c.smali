.class public final Lcom/swedbank/mobile/app/transfer/c;
.super Ljava/lang/Object;
.source "SelectableAccount.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/a/a;ZLjava/lang/String;)Lcom/swedbank/mobile/app/transfer/b;
    .locals 7
    .param p0    # Lcom/swedbank/mobile/business/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toSelectableAccount"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/swedbank/mobile/app/transfer/b;

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->a()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->b()Ljava/lang/String;

    move-result-object v3

    .line 20
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/a;->c()Ljava/lang/String;

    move-result-object v4

    move-object v1, v0

    move-object v5, p2

    move v6, p1

    .line 17
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/transfer/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static synthetic a(Lcom/swedbank/mobile/business/a/a;ZLjava/lang/String;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/b;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 17
    check-cast p2, Ljava/lang/String;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/c;->a(Lcom/swedbank/mobile/business/a/a;ZLjava/lang/String;)Lcom/swedbank/mobile/app/transfer/b;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/business/a/c;Z)Lcom/swedbank/mobile/app/transfer/b;
    .locals 7
    .param p0    # Lcom/swedbank/mobile/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toSelectableAccount"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/swedbank/mobile/app/transfer/b;

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/c;->a()Ljava/lang/String;

    move-result-object v2

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/c;->b()Ljava/lang/String;

    move-result-object v3

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/c;->c()Ljava/lang/String;

    move-result-object v4

    .line 29
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/a/c;->g()Ljava/lang/String;

    move-result-object v5

    move-object v1, v0

    move v6, p1

    .line 25
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/transfer/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method
