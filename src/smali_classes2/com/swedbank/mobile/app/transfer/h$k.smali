.class final Lcom/swedbank/mobile/app/transfer/h$k;
.super Ljava/lang/Object;
.source "TransferPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/h;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/h;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/h$k;->a:Lcom/swedbank/mobile/app/transfer/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 5
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "forceShowLoading"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/h$k;->a:Lcom/swedbank/mobile/app/transfer/h;

    .line 53
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/h$k;->a:Lcom/swedbank/mobile/app/transfer/h;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/h;->a(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/business/transfer/d;

    move-result-object v1

    .line 54
    invoke-interface {v1}, Lcom/swedbank/mobile/business/transfer/d;->a()Lio/reactivex/w;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v1

    .line 56
    new-instance v2, Lcom/swedbank/mobile/app/transfer/h$k$1;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/transfer/h$k$1;-><init>(Lcom/swedbank/mobile/app/transfer/h$k;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 66
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/h$k;->a:Lcom/swedbank/mobile/app/transfer/h;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/h;->b(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-static {p1, v4, v3, v2}, Lcom/swedbank/mobile/core/ui/x$a;->b(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 67
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/h$k;->a:Lcom/swedbank/mobile/app/transfer/h;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/h;->b(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-static {p1, v4, v3, v2}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 65
    :goto_0
    check-cast p1, Lio/reactivex/s;

    invoke-virtual {v1, p1}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v1, "interactor\n             \u2026oading()\n              })"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    new-instance v1, Lcom/swedbank/mobile/app/transfer/h$r;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/h$r;-><init>(Lcom/swedbank/mobile/app/transfer/h;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/h$k;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
