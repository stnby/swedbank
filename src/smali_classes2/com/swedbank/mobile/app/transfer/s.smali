.class public final Lcom/swedbank/mobile/app/transfer/s;
.super Ljava/lang/Object;
.source "TransferViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/transfer/s$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/app/plugins/list/b;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Z

.field private final f:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/app/w/b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/swedbank/mobile/app/transfer/s;-><init>(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/app/w/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/app/plugins/list/b;",
            "Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/app/w/b;",
            ")V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    iput-boolean p5, p0, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    iput-object p6, p0, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    iput-object p7, p0, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    return-void
.end method

.method public synthetic constructor <init>(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V
    .locals 5

    and-int/lit8 p9, p8, 0x1

    const/4 v0, 0x0

    if-eqz p9, :cond_0

    const/4 p9, 0x0

    goto :goto_0

    :cond_0
    move p9, p1

    :goto_0
    and-int/lit8 p1, p8, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    and-int/lit8 p1, p8, 0x4

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    .line 15
    move-object p3, p2

    check-cast p3, Ljava/lang/String;

    :cond_2
    move-object v2, p3

    and-int/lit8 p1, p8, 0x8

    if-eqz p1, :cond_3

    .line 16
    new-instance p4, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 17
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    .line 16
    invoke-direct {p4, p1, p2, v0}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    :cond_3
    move-object v3, p4

    and-int/lit8 p1, p8, 0x10

    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    move v0, p5

    :goto_2
    and-int/lit8 p1, p8, 0x20

    if-eqz p1, :cond_5

    .line 21
    move-object p6, p2

    check-cast p6, Lcom/swedbank/mobile/business/util/e;

    :cond_5
    move-object v4, p6

    and-int/lit8 p1, p8, 0x40

    if-eqz p1, :cond_6

    .line 22
    move-object p7, p2

    check-cast p7, Lcom/swedbank/mobile/app/w/b;

    :cond_6
    move-object p8, p7

    move-object p1, p0

    move p2, p9

    move p3, v1

    move-object p4, v2

    move-object p5, v3

    move p6, v0

    move-object p7, v4

    invoke-direct/range {p1 .. p8}, Lcom/swedbank/mobile/app/transfer/s;-><init>(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/transfer/s;ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/s;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/swedbank/mobile/app/transfer/s;->a(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)Lcom/swedbank/mobile/app/transfer/s;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)Lcom/swedbank/mobile/app/transfer/s;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/app/w/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/app/plugins/list/b;",
            "Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/swedbank/mobile/app/w/b;",
            ")",
            "Lcom/swedbank/mobile/app/transfer/s;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "data"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/app/transfer/s;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/swedbank/mobile/app/transfer/s;-><init>(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/swedbank/mobile/app/plugins/list/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_4

    instance-of v1, p1, Lcom/swedbank/mobile/app/transfer/s;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    check-cast p1, Lcom/swedbank/mobile/app/transfer/s;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    iget-object v3, p1, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    iget-object v3, p1, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    iget-object p1, p1, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    return v2

    :cond_4
    :goto_3
    return v0
.end method

.method public final f()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public final g()Lcom/swedbank/mobile/app/w/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    if-eqz v2, :cond_4

    goto :goto_2

    :cond_4
    move v1, v2

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_6
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransferViewState(loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/s;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", noAccounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/s;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", paymentExecutionSuccessMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->d:Lcom/swedbank/mobile/app/plugins/list/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", firstDataQuerySuccessful="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/s;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", dataQueryError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->f:Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/s;->g:Lcom/swedbank/mobile/app/w/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
